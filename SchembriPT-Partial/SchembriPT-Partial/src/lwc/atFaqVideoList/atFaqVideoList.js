import {LightningElement} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import FORM_FACTOR from '@salesforce/client/formFactor';


export default class AtFaqVideoList extends NavigationMixin(LightningElement) {

    get isMobile() {
        return FORM_FACTOR === 'Medium' || FORM_FACTOR === 'Small';
    }

}