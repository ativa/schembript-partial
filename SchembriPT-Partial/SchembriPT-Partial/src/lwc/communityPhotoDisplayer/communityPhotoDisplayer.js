import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';


export default class CommunityPhotoDisplayer extends LightningElement {

	/**
	 * @description The url of the photo we want to display
	 */
	@api photoUrl = '';

	@track _showImg = false;

	@api get showImg() {
		return this._showImg;
	}

	set showImg(newState) {
		this._showImg = newState;

		if(this.showImg === true) {
			let mainSelector	= this.template.querySelector(".img-holder__desktop");
			let boundingBox		= mainSelector.getBoundingClientRect();
			let width			= boundingBox.width;
			let height			= boundingBox.height;

			this._isHorizontal = width > height;
		}

	}

	/**
	 * @description
	 */
	@track _isHorizontal = false;

	/**
	 * @description
	 */
	@track _isVertical = false;

	/**
	 * @description Fires when the Close button is clicked
	 * @param {*} event The event of pressing the close button
	 */
	onCloseButtonClick(event) {
		// Prevents the anchor element from navigating to a URL.
        event.preventDefault();
		//Create a Close Event
		const closeEvent = new CustomEvent('close', {});
        // Dispatches the close event
        this.dispatchEvent(closeEvent);
	}

	debugDisplayer(event) {
		console.log("Clicked Button");

		let mainSelector = this.template.querySelector(".img-holder__desktop");



		console.log(JSON.parse(JSON.stringify(mainSelector.getBoundingClientRect())));

	}

	get isMobile() {
		return FORM_FACTOR === "Small";
	}

}