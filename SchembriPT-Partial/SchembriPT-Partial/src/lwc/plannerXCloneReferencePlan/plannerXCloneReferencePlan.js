import { LightningElement, api, track } from 'lwc';

import QueryPlanData	from '@salesforce/apex/AT_LWCC_PlannerCloneReferencePlan.QueryPlanData';
import ClonePlan		from '@salesforce/apex/AT_LWCC_PlannerCloneReferencePlan.ClonePlan';

export default class PlannerXCloneReferencePlan extends LightningElement {

	/* ---------- VARIABLES ---------- */

	@track _planId;

	@track _planData;

	@track _newPlanName = "";

	@track _isLoading = false;

	@track _isSaving = false;
	
	/* ---------- GETTERS ---------- */

	@api get planId() {
		return this._planId;
	}

	get isSavingOrLoading() {
		return this._isLoading === true || this._isSaving === true;
	}
	
	/* ---------- SETTERS ---------- */

	set planId(newPlanId) {
		console.log("[plannerXCloneReferencePlan] Updated Plan Id: ", newPlanId);
		this._planId	= newPlanId;
		this._isLoading = true;
		this.refreshPlanData();
	}

	/* ---------- CUSTOM METHODS ---------- */
	
	/**
	 * @description Clone the plan
	 */
	@api Clone(onSuccess, onError) {
		ClonePlan({
			planId:			this._planId,
			newPlanName:	this._newPlanName
		}).then(result => {
			console.log("SAVED: ", JSON.parse(JSON.stringify(result)));
			this.fireSaveEvent();
			if(onSuccess) {
				onSuccess(result);
			}
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});

	}

	/**
	 * @description Refresh the Plan Data
	 */
	refreshPlanData() {
	
		QueryPlanData({
			planId: this._planId
		}).then(result => {
			console.log("RESULT: ", JSON.parse(JSON.stringify(result)));
			this._newPlanName		= result.PlanName;
			this._isLoading			= false;
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

	fireSaveEvent() {
        this.dispatchEvent(new CustomEvent('save'));
	}

	/* ---------- CUSTOM EVENTS ---------- */

	onSaveClonedPlan(event) {
		
		ClonePlan({
			planId:			this._planId,
			newPlanName:	this._newPlanName
		}).then(result => {
			console.log("SAVED: ", JSON.parse(JSON.stringify(result)));
			this.fireSaveEvent();
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});

	} 

	onUpdatedPlanName(event) {
		this._newPlanName = event.target.value;
		console.log("New Plan Name: ", this._newPlanName);
	}



}