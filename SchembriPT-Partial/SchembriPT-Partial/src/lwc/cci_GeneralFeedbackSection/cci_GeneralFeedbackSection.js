import { LightningElement, api, track } from 'lwc';

export default class Cci_GeneralFeedbackSection extends LightningElement {

	/* --------------- VARIABLES --------------- */

	@api recordId;

	onSuccessAction;
	 
	onErrorAction;
	
	/* --------------- GETTERS --------------- */

	/* --------------- SETTERS --------------- */
	
	/* --------------- CUSTOM METHODS --------------- */

	@api Submit(onSuccessAction, onErrorAction) {
		console.log("Submitting General Feedback Section");
		const element = this.template.querySelector('[data-id="feedback"]');
		element.submit();

		if(onSuccessAction) {
			this.onSuccessAction = onSuccessAction;
		}
		
		if(onErrorAction) {
			this.onErrorAction = onErrorAction;
		}

	}

	/**
	 * @description Are all the values set
	 */
	@api allValuesSet() {

		let fields = [
			this.template.querySelector('[data-id="feedback-general"]'),
			this.template.querySelector('[data-id="feedback-nutrition"]'),
			this.template.querySelector('[data-id="feedback-training"]')
		];

		let returnValue = {
			valuesSet:		true,
			notSetFields:	[]
		};
		
		for(let i = 0; i < fields.length; i++) {
			if(fields[i].value === null || fields[i].value === '') {
				returnValue.valuesSet = false;
				returnValue.notSetFields.push(fields[i].outerText);
			}
		}
		
		return returnValue;

	}
	
	/* --------------- CUSTOM EVENTS --------------- */
	
	onSuccess(event) {
		console.log("Succesfully Saved General Feedback");

		if(this.onSuccessAction) {
			this.onSuccessAction(event);
		}

	}

	onError(event) {
		console.log("General Feedback Errored");
	
		if(this.onErrorAction) {
			this.onErrorAction(event);
		}
	}


}