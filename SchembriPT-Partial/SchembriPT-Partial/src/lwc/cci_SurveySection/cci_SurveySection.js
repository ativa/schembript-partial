import { LightningElement, api, track } from 'lwc';

export default class Cci_SurveySection extends LightningElement {

	/* --------------- VARIABLES --------------- */

	@api recordId;

	@api surveyFields;
	
	onSuccessAction;
	 
	onErrorAction;

	/* --------------- GETTERS --------------- */

	get sFields() {

		let newSurveyFields = [];

		for(let i = 0; i < this.surveyFields.length; i++) {
			
			let copiedField = JSON.parse(JSON.stringify(this.surveyFields[i]));

			if(copiedField.type === "slider n/a") {
				copiedField.isSlider = true;
			} else {
				copiedField.isSlider = false;
			}

			newSurveyFields.push(copiedField);
		}

		return newSurveyFields;

	}

	/* --------------- SETTERS --------------- */
	
	/* --------------- CUSTOM METHODS --------------- */

	
	@api Submit(onSuccessAction, onErrorAction) {

		const element = this.template.querySelector('[data-id="survey"]');
		element.submit();

		if(onSuccessAction) {
			this.onSuccessAction = onSuccessAction;
		}
		
		if(onErrorAction) {
			this.onErrorAction = onErrorAction;
		}

	}

	/**
	 * @description Are all the values set
	 */
	@api allValuesSet() {

		let returnValue = {
			valuesSet:		true,
			notSetFields:	[]
		};
	
		let surveyInputFields = this.template.querySelectorAll(".survey-input-field");

		for(let i = 0; i < surveyInputFields.length; i++) {
			if(surveyInputFields[i].value === null || surveyInputFields[i].value === '') {
				returnValue.notSetFields.push(surveyInputFields[i].dataset.label);
				returnValue.valuesSet = false;
			}
		}

		return returnValue;

	}




	/* --------------- CUSTOM EVENTS --------------- */

	onSliderChange(event) {

		let value			= event.target.value;
		let fieldName		= event.target.dataset.name;
		let allSurveyFields = JSON.parse(JSON.stringify(this.surveyFields));
		
		for(let i = 0; i < allSurveyFields.length; i++) {
			
			let surveyField = allSurveyFields[i];
			if(surveyField.name === fieldName) {
				surveyField.value =  '' + value;
			}
		}
		this.surveyFields = allSurveyFields;
	}
	
	onSuccess(event) {
		console.log("Succesfully Saved Survey");

		if(this.onSuccessAction) {
			this.onSuccessAction(event);
		}

	}

	onError(event) {
		console.log("Survey Errored");
	
		if(this.onErrorAction) {
			this.onErrorAction(event);
		}
	}


}