import { LightningElement, api, track } from 'lwc';
import { 
	FlowAttributeChangeEvent, 
	FlowNavigationBackEvent,
	FlowNavigationNextEvent,
	FlowNavigationPauseEvent,
	FlowNavigationFinishEvent
} from 'lightning/flowSupport';


export default class AfcFieldSetAndNavigation extends LightningElement {

	/* --------------- CUSTOM VARIABLES --------------- */

	/**
	 * @description Available Flow Actions
	 */
	@api availableActions = [];

	@api fieldSetRecordIdOne	= '';
	@api fieldSetRecordIdTwo	= '';
	@api fieldSetRecordIdThree	= '';
	@api fieldSetRecordIdFour	= '';
	@api fieldSetRecordIdFive	= '';
	
	/**
	 * @description A list of states to display the titles of the fieldsets
	 */
	@api fieldSetShowTitles		= '';
	
	/**
	 * @description A list of Titles for the fieldsets
	 */
	@api fieldSetTitleTexts		= '';

	/**
	 * @description A list of columns for the fieldsets
	 */
	@api fieldSetColumns		= '';

	/**
	 * @description A list to track the editability of the forms
	 */
	@api fieldSetEditables		= '';

	/**
	 * @description A list of Salesforce Object Developer names for each fieldset
	 */
	@api fieldSetObjectApiNames = '';

	/**
	 * @description A list of FieldSet Names for each fieldset
	 */
	@api fieldSetNames			= '';

	/**
	 * @description The Position of the Label relative to the field on the fieldset
	 */
	@api fieldSetLabelPositions = '';

	/**
	 * @description The Labels on the flow buttons
	 */
	@api navigationLabels		= '';
	
	/**
	 * @description The Actions on the flow buttons
	 */	
	@api navigationActions		= '';
	
	/**
	 * @description The Values of the Flow Buttons
	 */
	@api navigationValues		= '';

	/**
	 * @description The Submissions of the Flow Buttons
	 */
	@api navigationSubmissions	= '';
	
	/**
	 * @description The Button Variants of the Flow Buttons
	 */
	@api navigationVariants		= '';

	/**
	 * @description The Value for the navigation
	 */
	@api buttonValue;

	/* --------------- GETTERS --------------- */

	get fieldSets() {

		let fsTitleTexts			= this.fieldSetTitleTexts.split(';');

		let fsShowTitles		= this.fieldSetShowTitles.split(';');
		let fsColumns			= this.fieldSetColumns.split(';');
		let fsEditables			= this.fieldSetEditables.split(';');
		let fsObjectApiNames	= this.fieldSetObjectApiNames.split(';');
		let fsNames				= this.fieldSetNames.split(';');
		let fsLabelPositions	= this.fieldSetLabelPositions.split(';');

		let newFieldSets = [];

		for(let i = 0; i < fsTitleTexts.length; i++) {

			let newFieldSet = {
				order:			i,
				objectApiName:	fsObjectApiNames[i],
				fieldSetName:	fsNames[i],
				labelPosition:	fsLabelPositions[i],
				editable:		(fsEditables[i] === 'true'),
				title: {
					text:	fsTitleTexts[i]
				},
				showTitle:		(fsShowTitles[i] === 'true'),
				columns:		fsColumns[i]
			};

			if(i === 0) {
				newFieldSet.recordId = this.fieldSetRecordIdOne;
			} else if(i === 1) {
				newFieldSet.recordId = this.fieldSetRecordIdTwo;
			} else if(i === 2) {
				newFieldSet.recordId = this.fieldSetRecordIdThree;
			} else if(i === 3) {
				newFieldSet.recordId = this.fieldSetRecordIdFour;
			} else if(i === 4) {
				newFieldSet.recordId = this.fieldSetRecordIdFive;
			}

			newFieldSets.push(newFieldSet);
		}


		return newFieldSets;


	}

	get flowButtons() {
	
		let buttons = [];

		if(this.navigationLabels && this.navigationActions && this.navigationValues && this.navigationSubmissions && this.navigationVariants) {

			let navLabels		= this.navigationLabels.split(';');
			let navActions		= this.navigationActions.split(';');
			let navValues		= this.navigationValues.split(';');
			let navSubmissions	= this.navigationSubmissions.split(';');
			let navVariants		= this.navigationVariants.split(';');
			for(let i = 0; i < navLabels.length; i++) {
				let newButton = {
					order:		i,
					label:		navLabels[i],
					action:		navActions[i],
					value:		navValues[i],
					submit:		(navSubmissions[i] === 'true'),
					variant:	navVariants[i]
				};
				buttons.push(newButton);
			}
		
		}

		return buttons;

	}

	/* --------------- SETTERS --------------- */

	/* --------------- CUSTOM EVENTS --------------- */

	onNavigationSelect(event) {

		let action	= event.target.dataset.action;
		let value	= event.target.dataset.value;
		let submit	= (event.target.dataset.submit == 'true');

		this.buttonValue = value;

		let navigateEvent = null;
		
		

		if(action === 'NEXT' && this.availableActions.find(act => act === 'NEXT')) {
            navigateEvent = new FlowNavigationNextEvent(); // Navigate to the next screen
		} else if(action === 'NEXT' && this.availableActions.find(act => act === 'FINISH')) {
            navigateEvent = new FlowNavigationFinishEvent(); // Navigate to the finish screen
		} else if(action === 'BACK' && this.availableActions.find(act => act === 'BACK')) {
            navigateEvent = new FlowNavigationBackEvent(); // Navigate to the previous screen
		}
		
		if(submit) {
		
			let allFieldSets = this.template.querySelectorAll('.field-set'); // [<div>First</div>, <div>Second</div>]
			
			let fieldSetsSubmitted = 0;
			
			for(let i = 0; i < allFieldSets.length; i++) {

				allFieldSets[i].Submit((result) => {
					fieldSetsSubmitted += 1;

					if(fieldSetsSubmitted === allFieldSets.length) {
						this.dispatchEvent(navigateEvent);
					}

				},(error) => {
					console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
				});

			}


			/*
			let fieldSet = this.template.querySelector('.field-set');

			fieldSet.Submit(() => {
				this.dispatchEvent(navigateEvent);
			},() => {
				console.error("Err-or");
			});
			*/

		} else {
            this.dispatchEvent(navigateEvent);
		}

		console.log('At the End');

	}


}