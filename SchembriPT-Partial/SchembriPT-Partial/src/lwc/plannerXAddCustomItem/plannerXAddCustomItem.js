import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import Template_Item__c from '@salesforce/schema/Template_Item__c';

/**
 * @description       : Add A Custom Item
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : Ativa
 * @last modified on  : 12-15-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   12-15-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
export default class PlannerXAddCustomItem extends LightningElement {
    
    /* --------------- VARIABLES --------------- */

	/**
	 * @description The record type assigned to the template item
	 */
	@api recordType = "Nutrition";

	/**
	 * @description Tracks if the data is loading
	 */
	@track _isLoading = false;
	
	/**
	 * @description Holds the 'Nutrition' Record Type data
	 */
    @track _nutritionRecordType;
    
    /**
     * @description The Method to call when the Form has been successfully submitted
     */
    _onSuccessMethod;

    /**
     * @description The Method to call when the Form has errored
     */
    _onErrorMethod;

    /* --------------- GETTERS --------------- */

	get hasRecordTypeInfo() {
		return typeof(this._nutritionRecordType) !== 'undefined';
	}
    /* --------------- SETTERS --------------- */

    /* --------------- CUSTOM METHODS --------------- */

    /**
     * @description Submit the form
     * @param {Function} onSuccess The Function to call when the form has been successfully submitted
     * @param {Function} onError The Function to call when the form has failed to submit
     */
    @api Submit(onSuccess, onError) {

        if(onSuccess) {
            this._onSuccessMethod = onSuccess;
        }
        if(onError) {
            this._onErrorMethod = onError;
        }

        let form = this.template.querySelector('.add-custom-item-form');
        
        form.submit();

    }
    
    /* --------------- CUSTOM EVENTS --------------- */

    /**
     * @description Executes when the form is successfully submitted
     * @param {Event} event The event that triggers this
     */
    onFormSuccess(event) {
        if(this._onSuccessMethod) {
            this._onSuccessMethod(event.detail);
        }
    }

    /**
     * @description Executes when the form has failed to be submitted
     * @param {Event} event The event that triggers this
     */
    onFormError(event) {
        if(this._onErrorMethod) {
            this._onErrorMethod();
        }
    }

    /* --------------- WIRE EVENTS --------------- */

	@wire(getObjectInfo, {objectApiName: Template_Item__c})
	getRecordTypeData({ data, error }) {

		if(data) {
			for(let recId in data.recordTypeInfos) {
                let recType = data.recordTypeInfos[recId];
				console.log("Record Type: ", JSON.parse(JSON.stringify(recType)));
				if(recType.name === this.recordType) {
					this._nutritionRecordType = recType;
                }
			}
			this._isLoading = false;

		} else if(error) {
			console.error("[trackerXAddCustomItem] ERROR: ", JSON.parse(JSON.stringify(error)) );
		}

	}

    /* --------------- LWC EVENTS --------------- */

}