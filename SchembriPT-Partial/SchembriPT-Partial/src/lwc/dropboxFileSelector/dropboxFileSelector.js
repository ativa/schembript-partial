import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class DropboxFileSelector extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	* @description The Authorization Key used to connect with dropbox
	*/
	@api authorizationKey = 'KnfIiR9B6bAAAAAAAAA9v9yb2ThYQTqQxkhAxgFbTnPmroxTwwX95g4tcbtRZZd9';

	/**
	 * @description The label of the upload button
	 */
	@api buttonLabel = "Upload Files";

	/**
	* @description The folder path
	*/
	@api folderPath;

	@api hideProgressBar = false;

	/**
	* @description The name of the files to use (If multiple is true, is a list, otherwise it's a string
	*/
	@api fileNames = [];

	@api selectedFiles = [];

	/** 
	 * @description All the Formats that can be uploaded
	 */
	@api validFormats;


	/**
	 * @description Can upload multiple files
	 */
	@api multiple = false;

	/**
	 * @description Tracks if we are currently uploading files
	 */
	@track _uploadingFiles = false;

	/**
	 * @description The current file index we are uploading
	 */
	@track fileIndex = 0;

	/**
	 * @description The upload results of the files
	 */
	@track uploadResults = [];

	/* ---------- GETTERS ---------- */

	get loadingBarProgress() {
		return (this.fileIndex / this.selectedFiles.length) * 100;
	}

	get displayProgressBar() {
		return this.hideProgressBar === false && this._uploadingFiles === true;
	}
	
	/* ---------- CUSTOM METHODS ---------- */
	
	readAndUploadFiles(onComplete) {

		if(this._uploadingFiles === false) {
			console.log("Cancelled Uploading Files");
			return;
		}

		//Start uploading the files
		const fileReader = new FileReader();
		fileReader.onloadend = (() => {  
			//When you get the file contents, upload it to Dropbox
			let fileContents = fileReader.result;
			this.uploadToDropbox(
				fileContents, 
				this.selectedFiles[this.fileIndex], 
				(result) => {

				
					console.log('File Uploaded: ', JSON.parse(JSON.stringify(result)));


					//Get the Sharing Link
					this.createSharingLink(
						result.path_display, 
						(result2) => {
							this.uploadResults.push({
								fileName:		this.selectedFiles[this.fileIndex].name,
								uploadResult:	result,
								result:			result2
							});
							this.progressToNextPhoto(onComplete);
						}, (error) => {

							this.uploadResults.push({
								fileName:		this.selectedFiles[this.fileIndex].name,
								uploadResult:	result,
								error:			error
							});
							this.progressToNextPhoto(onComplete);
						}
					);


				}, 
				(error) => {
					console.error("Failed to upload to Dropbox: ", JSON.parse(JSON.stringify(error)));
				}
			);

		});  
		fileReader.readAsDataURL(this.selectedFiles[this.fileIndex]); 
	} 
	
	progressToNextPhoto(onComplete) {
		this.fileIndex += 1;
		if(this.fileIndex == this.selectedFiles.length) {
			if(onComplete) {
				onComplete();
			}
		} else {
			this.readAndUploadFiles(onComplete);
		}
	}
	
	uploadToDropbox(fileContents, file, onSuccess, onError) {
	
		//If the number of files is one, use the custom file name, if however it's greater than 1 then use it's file name
		let fileName = this.fileNames[this.fileIndex];

		let fileType = file.name.substr(file.name.indexOf('.') + 1);

		let path = this.folderPath + fileName + "." + fileType;
		path = path.replace('’','_');


		fetch(
			"https://content.dropboxapi.com/2/files/upload",
			{
				method:		"POST",
				body:		file,
				headers:	{
					"Authorization":	"Bearer " + this.authorizationKey,
					"Dropbox-API-Arg":	'{"path": "' + path + '","mode": "overwrite","autorename": true,"mute": false,"strict_conflict": false}',
					"Content-Type":		"application/octet-stream"
				}
			}
		).then((response) => {
            if (!response.ok) {
				if(onError) {
					onError(response);
					return;
				}
            }
            return response.json();
        }).then((response) => {
			if(onSuccess) {
				onSuccess(response);
			}
		})
	}

	createSharingLink(path, onSuccess, onError) {
		fetch(
			'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
			{
				method:		"POST",
				body:		'{"path": "' + path + '"}',
				headers:	{
					"Authorization":	"Bearer " + this.authorizationKey,
					"Content-Type":		"application/json"
				}
			}
		).then((response) => {
            return response.json();
        }).then((response) => {
			if(response.error) {
				if(onError) {
					onError(response);
				}
			} else {
				if(onSuccess) {
					onSuccess(response);
				}
			}
		})

	}

	/**
	 * @description Fires when the file is selected
	 */
	fireFileSelectedEvent() {

		let selectedDetails = {
			selectedFileCount: this.selectedFiles.length,
			selectedFileNames: []
		};

		for(let i = 0; i < this.selectedFiles.length; i++) {
			selectedDetails.selectedFileNames.push(this.selectedFiles[i].name);
		}

		console.log("SELECTED DETAILS: ",selectedDetails);


		const selectedEvent = new CustomEvent('select', { detail: { fileDetails:selectedDetails }  });
		this.dispatchEvent(selectedEvent);
			
	}

	/**
	 * @description Fires when the files have been successfully uploaded
	 */
	fireUploadSuccessEvent() {
        const uploadEvent = new CustomEvent('uploadfinished', { detail: { uploadResults: this.uploadResults } });
        this.dispatchEvent(uploadEvent);
	}

	fireErrorToast(title, message) {
        const event = new ShowToastEvent({
            title:		title,
            message:	message,
			variant:	'error'
        });
        this.dispatchEvent(event);
	}

	fireSuccessToast(title, message) {
        const event = new ShowToastEvent({
            title:		title,
            message:	message,
			variant:	'success'
        });
        this.dispatchEvent(event);

	}

	/**
	 * @description Upload the files to Dropbox
	 */
	@api Upload(fileNames, onSuccess) {

		if(this.multiple === true) {
			this.fileNames = fileNames;
		} else {
			this.fileNames = [ fileNames ];
		}

		this.fileIndex			= 0;
		this._uploadingFiles	= true;
		this.uploadResults		= [];

		this.readAndUploadFiles(() => {
			console.log("Successfully uploaded all files");
			this.fireSuccessToast('Success','All files have been uploaded');
			this.fireUploadSuccessEvent();

			if(onSuccess) {
				onSuccess({ 
					uploadResults: this.uploadResults 
				});
			}

		});

	}

	isValidFormat(fileName) {

		if(typeof this.validFormats === "undefined") {
			return true;
		}

		let fileSegments = fileName.split(".");
		let fileFormat = "." + fileSegments[fileSegments.length - 1];
		return this.validFormats.toLowerCase().includes(fileFormat.toLowerCase());
	}

	/* ---------- CUSTOM EVENTS ---------- */

	/**
	 * @description Executes when files get selected
	 * @param event {Event} The event that triggers this method
	 */
	onFilesSelection(event) {

		if (event.target.files.length > 0) {  

			let allFilesValid = true;

			for(let i = 0; i < event.target.files.length; i++) {
				console.log("FileName: ", event.target.files[i].name);

				if(this.isValidFormat(event.target.files[i].name) === false) {
					allFilesValid = false;
				}

			}

			if(allFilesValid === true) {
				console.log("All Files are Valid");
				//Select the files, and fire an onselection event
				this.selectedFiles = event.target.files;
				this.fireFileSelectedEvent();
			} else {
				console.log("All Files are not Valid");
				this.fireErrorToast("One or more files have an invalid format", "Unsupported file type - please upload one of the following: " + this.validFormats);
			}


		}

	}

	onCloseUploadingFilesModal(event) {
		this._uploadingFiles	= false;
	}

}