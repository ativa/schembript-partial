import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';

import GetFrontSideBackPhotosForCheckIn 	from '@salesforce/apex/CommunityHome.GetFrontSideBackPhotosForCheckIn'; //Just Get the CheckIn Info, and pull the photos from there
import AddDropboxPhoto						from '@salesforce/apex/CommunityHome.AddDropboxPhoto';
import recordPhoto 							from '@salesforce/apex/CommunityHome.recordPhoto';
import DeletePhoto							from '@salesforce/apex/CommunityHome.DeletePhoto';

const MONTHS = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

export default class CciXPhotosSection extends LightningElement {

	/* --------------- VARIABLES --------------- */

	@track _validFormats = ".jpeg, .jpg, .png";

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;

	/**
	 * @description The name of the Check-In
	 */
	@track _checkInName;

	/**
	 * @description The Id of the Client
	 */
	@track _clientId;

	/**
	 * @description The URL of the Front Photo
	 */
	@track frontPhotoURL;

	/**
	 * @description The URL of the Side Photo
	 */
	@track sidePhotoURL;

	/**
	 * @description The URL of the Back Photo
	 */
	@track backPhotoURL;

	/**
	 * @description The Gender of the client
	 */
	@track clientGender;

	/**
	 * @description The name of the client
	 */
	@track _clientName;

	/**
	 * @description The Authorization code for dropbox
	 */
	@track _dropboxAuthCode;

	/**
	 * @description The folder that is used to upload the photo
	 */
	@track _dropboxReferencePath;

	/**
	 * @description Uploading Front
	 */
	@track _uploadingFront	= false;
	
	/**
	 * @description Uploading Side
	 */
	@track _uploadingSide	= false;
	
	/**
	 * @description Uploading Back
	 */
	@track _uploadingBack	= false;

	formatDate(date) {

		const todaysDate	= date.getDate();
		const todaysMonth	= MONTHS[date.getMonth()];
		const todaysYear	= date.getFullYear();

		let dateStr = '';

		if(todaysDate < 10) {
			dateStr = '0' + todaysDate;
		} else {
			dateStr = '' + todaysDate;
		}

		return todaysMonth + dateStr + '-' + todaysYear;

	}


	/* --------------- GETTERS --------------- */
	
	/**
	 * @description The File name of the front photo
	 */
	get _frontPhotoFileName() {
		return this._checkInName + '_' + this.formatDate(new Date()) + '_Front';
	}
	
	get _sidePhotoFileName() {
		return this._checkInName + '_' + this.formatDate(new Date()) + '_Side';
	}

	get _backPhotoFileName() {
		return this._checkInName + '_' + this.formatDate(new Date()) + '_Back';
	}

	/**
	 * @description Returns the Check-In Id
	 * @returns The Check-In Id
	 */
	@api get checkInId() {
		return this._checkInId;
	}

	/**
	 * @description Is this client Male?
	 * @returns Returns true if the client is Male, false otherwise
	 */
	get isMaleClient() {
		return this.clientGender === 'Male';
	}

	/**
	 * @description Is this client Female
	 * @returns Returns true if the client is Female, false otherwise
	 */
	get isFemaleClient() {
		return this.clientGender === 'Female';
	}

	/**
	 * @description Is The Component being used on mobile
	 * @returns Returns true if being used by the mobile, false otherwise
	 */
	get isMobile() {
		return FORM_FACTOR === 'Small';
	}

	/**
	 * @description Has the Front Photo been uploaded
	 * @returns Returns true if the front photo has been uploaded, false otherwise
	 */
	get isFrontPhotoUploaded() {
		return typeof(this.frontPhotoURL) != "undefined";
	}

	/**
	 * @description Has the Side Photo been uploaded
	 * @returns Returns true if the side photo has been uploaded, false otherwise
	 */
	get isSidePhotoUploaded() {
		return typeof(this.sidePhotoURL) != "undefined";
	}

	/**
	 * @description Has the Back Photo been uploaded
	 * @returns Returns true if the back photo has been uploaded, false otherwise
	 */
	get isBackPhotoUploaded() {
		return typeof(this.backPhotoURL) != "undefined";
	}

	/* --------------- SETTERS --------------- */

	set checkInId(newCheckInId) {

		this._checkInId = newCheckInId;

		GetFrontSideBackPhotosForCheckIn({
			checkInId: this._checkInId
		}).then(result => {

			console.log("GetFrontSideBackPhotosForCheckIn: ", JSON.parse(JSON.stringify(result)));

			this._clientName	= result.ClientName; 
			this.clientGender	= result.ClientGender;
			this._checkInName	= result.CheckInName;

			if(result.Front) {
				this.frontPhotoURL 	= result.Front;
			}
			if(result.Side) {
				this.sidePhotoURL 	= result.Side;
			}
			if(result.Back) {
				this.backPhotoURL 	= result.Back;
			}

			this._dropboxAuthCode		= result.DropboxAuthorizationKey;
			this._dropboxReferencePath	= result.DropboxReferencePath;

			console.log("[cci_PhotosSection] Get CheckIn Info: ", JSON.parse(JSON.stringify(result)));
		}).catch(error => {
			console.error("[cci_PhotosSection] Get CheckIn Info: ", JSON.parse(JSON.stringify(error)));
		});

	}

	/* --------------- CUSTOM METHODS --------------- */

	/**
	 * @description Have all the photos been submitted
	 * @returns Have the photos been set?
	 */
	@api allValuesSet() {
	
		let frontSet 	= typeof(this.frontPhotoURL) != "undefined";
		let sideSet 	= typeof(this.sidePhotoURL) != "undefined";
		let backSet 	= typeof(this.backPhotoURL) != "undefined";
		
		let returnValue = {
			valuesSet:		frontSet === true && sideSet === true && backSet === true,
			notSetFields:	[]
		};
		
		return returnValue;

	}

	/**
	 * @description Add a Document Photo, as a Photo Object
	 * @param {String} photoType The Type of photo we are uploading (Front, Side, Back)
	 * @param {String} documentId The Id of the Document we are uploading
	 */
	addPhotoRecord(photoType, documentId) {

		recordPhoto({
			checkInID:	this.checkInId,
			name: 		photoType,
			documentID: documentId
		}).then(result => {
			console.log("Updated Photo: ", JSON.parse(JSON.stringify(result)));

			if(photoType === 'front') {
				console.log("Updating Front Photo URL");
				this.frontPhotoURL = result;
			} else if(photoType === 'side') {
				console.log("Updating Side Photo URL");
				this.sidePhotoURL = result;
			} else if(photoType === 'back') {
				console.log("Updating Back Photo URL");
				this.backPhotoURL = result;
			}

			//Fire an Upload Event
			this.fireOnPhotoUpload(photoType);

		}).catch(error => {
			console.error("Updated Photo Error: ", JSON.parse(JSON.stringify(error)));
		});

	}

	/**
	 * @description Fires a Photo Upload Event
	 * @param {String} photoType The Type of Photo that was just uploaded
	 */
	fireOnPhotoUpload(photoType) {
		
		let data = {
			photoType: 			photoType,
			url: 				'',
			allPhotosUploaded:	this.allValuesSet().valuesSet
		};

		if(photoType === 'front') {
			data.url = this.frontPhotoURL;
		} else if(photoType === 'side') {
			data.url = this.sidePhotoURL;
		} else if(photoType === 'back') {
			data.url = this.backPhotoURL;
		}

        // Creates the event with the contact ID data.
        const uploadPhotoEvent = new CustomEvent('photoupload', { detail: data });

		console.log("Fire Photo Upload");

        // Dispatches the event.
		this.dispatchEvent(uploadPhotoEvent);
		
	}

	/* --------------- CUSTOM EVENTS --------------- */

	addDropboxPhoto(photoType, referencePath, dropboxURL) {
		AddDropboxPhoto({
			checkInID:		this._checkInId,
			fileType:		photoType,
			referencePath:	referencePath,
			dropboxURL:		dropboxURL,
		}).then(result => {
			
			if(photoType === 'front') {
				console.log("Updating Front Photo URL");
				this.frontPhotoURL = result.Dropbox_Path__c;
			} else if(photoType === 'side') {
				console.log("Updating Side Photo URL");
				this.sidePhotoURL = result.Dropbox_Path__c;
			} else if(photoType === 'back') {
				console.log("Updating Back Photo URL");
				this.backPhotoURL = result.Dropbox_Path__c;
			}

			//Fire an Upload Event
			this.fireOnPhotoUpload(photoType);

		}).catch(error => {
			console.error("Updated Photo Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Triggers when the Front Photo has been successfully uploaded
	 * @param {Event} event The Event that triggers this method
	 */
	onFrontPhotoUploaded(event) {
		this._uploadingFront = false;
		let uploadResults = event.detail.uploadResults;
		let refPath = uploadResults[0].uploadResult.path_display;
		if(uploadResults[0].result) {
			this.addDropboxPhoto('front', refPath, uploadResults[0].result.url);
		} else if(uploadResults[0].error) {
			this.addDropboxPhoto('front', refPath, uploadResults[0].error.error.shared_link_already_exists.metadata.url);
		}
	}

	/**
	 * @description Triggers when the Front Photo has been selected
	 * @param {Event} event The Event that triggers this method
	 */
	onFrontPhotoSelected(event) {
		this._uploadingFront = true;
        let frontPhotoFileSelector = this.template.querySelector('.front-photo-file-selector');
		frontPhotoFileSelector.Upload(this._checkInName + '_' + this.formatDate(new Date()) + '_Front');
	}

	/**
	 * @description Triggers when the Front Photo is going to be deleted
	 * @param {Event} event The Event that triggers this method
	 */
	onFrontPhotoDelete(event) {
		DeletePhoto({
			checkInId: this._checkInId,
			photoType: 'Front'
		}).then(() => {
			this.frontPhotoURL = undefined;
		}).catch((error) => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Triggers when the Side Photo has been successfully uploaded
	 * @param {Event} event The Event that triggers this method
	 */
	onSidePhotoUploaded(event) {
		this._uploadingSide = false;
		let uploadResults = event.detail.uploadResults;
		let refPath = uploadResults[0].uploadResult.path_display;
		if(uploadResults[0].result) {
			this.addDropboxPhoto('side', refPath, uploadResults[0].result.url);
		} else if(uploadResults[0].error) {
			this.addDropboxPhoto('side', refPath, uploadResults[0].error.error.shared_link_already_exists.metadata.url);
		}
	}
	
	/**
	 * @description Triggers when the Side Photo has been selected
	 * @param {Event} event The Event that triggers this method
	 */
	onSidePhotoSelected(event) {
		this._uploadingSide = true;
        let sidePhotoFileSelector = this.template.querySelector('.side-photo-file-selector');
		sidePhotoFileSelector.Upload(this._checkInName + '_' + this.formatDate(new Date()) + '_Side');
	}

	/**
	 * @description Triggers when the Side Photo is going to be deleted
	 * @param {Event} event The Event that triggers this method
	 */
	onSidePhotoDelete(event) {
		DeletePhoto({
			checkInId: this._checkInId,
			photoType: 'Side'
		}).then(() => {
			this.sidePhotoURL = undefined;
		}).catch((error) => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Triggers when the Back Photo has been successfully uploaded
	 * @param {Event} event The Event that triggers this method
	 */
	onBackPhotoUploaded(event) {
		this._uploadingBack = false;
		let uploadResults = event.detail.uploadResults;
		let refPath = uploadResults[0].uploadResult.path_display;
		if(uploadResults[0].result) {
			this.addDropboxPhoto('back', refPath, uploadResults[0].result.url);
		} else if(uploadResults[0].error) {
			this.addDropboxPhoto('back', refPath, uploadResults[0].error.error.shared_link_already_exists.metadata.url);
		}
	}
	
	/**
	 * @description Triggers when the Back Photo has been selected
	 * @param {Event} event The Event that triggers this method
	 */
	onBackPhotoSelected(event) {
		this._uploadingBack = true;
        let sidePhotoFileSelector = this.template.querySelector('.back-photo-file-selector');
		sidePhotoFileSelector.Upload(this._checkInName + '_' + this.formatDate(new Date()) + '_Back');
	}

	/**
	 * @description Triggers when the Back Photo is going to be deleted
	 * @param {Event} event The Event that triggers this method
	 */
	onBackPhotoDelete(event) {
		DeletePhoto({
			checkInId: this._checkInId,
			photoType: 'Back'
		}).then(() => {
			this.backPhotoURL = undefined;
		}).catch((error) => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

}