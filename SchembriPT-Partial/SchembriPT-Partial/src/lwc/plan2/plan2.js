import { LightningElement, track, api } from 'lwc';
import FORM_FACTOR						from '@salesforce/client/formFactor'; //Tracks if Mobile or Desktop
import { ShowToastEvent }               from 'lightning/platformShowToastEvent';

import getNutritionalItems	from '@salesforce/apex/Planner.getNutritionalItems';
import getFilters			from '@salesforce/apex/Planner.getFilters';

import addBlock		from '@salesforce/apex/Planner.addBlock';
import renameBlock	from '@salesforce/apex/Planner.renameBlock';
import deleteBlock	from '@salesforce/apex/Planner.deleteBlock';
import deleteItem	from '@salesforce/apex/Planner.deleteItem';
import moveBlocks	from '@salesforce/apex/Planner.moveBlocks';
import moveItems	from '@salesforce/apex/Planner.moveItems';

import getTemplateBlocks from '@salesforce/apex/Planner.getTemplateBlocks';

import addBlockItems from '@salesforce/apex/Planner.addBlockItems';

export default class Plan2 extends LightningElement {

	// --------------- VARIABLES ---------------

	/** 
	 * @description Tracks if we are either moving the blocks or the items
	 */
    @track _movingBlocksOrItems = false;
    
    /**
     * @description Tracks the Add Item tab being displayed
     */
    @track _addItemCurrentTab;

    // --------------- GETTERS ---------------

    // --------------- SETTERS ---------------

    // --------------- CUSTOM METHODS ---------------

    fireSuccessEvent(title, message) {
        const event = new ShowToastEvent({
            title:      title,
            message:    message,
            variant:    'success'
        });
        this.dispatchEvent(event);
    }

    fireErrorEvent(title, message) {
        const event = new ShowToastEvent({
            title:      title,
            message:    message,
            variant:    'error'
        });
        this.dispatchEvent(event);
    }

    addCustomItem() {

        let customItemForm = this.template.querySelector('.custom-item-form');

        customItemForm.Submit((submitSuccess) => {
            console.log("Success Add Item: ", JSON.parse(JSON.stringify(submitSuccess)))
            this.fireSuccessEvent('Added New Custom Item', submitSuccess.fields.Name.value + 'has been added');

            //Add the New Item to the Block
            const index = this.selectedBlockIndex;
            const block = this.blocks.filter(b => (b.index == index))[0];

            addBlockItems({
                blockID:        block.id, 
                templateItems:  JSON.stringify([{ id: submitSuccess.id, name: submitSuccess.fields.Name.value }])
            }).then(addedItems => {
                console.log('Added items: ', addedItems);
                addedItems.forEach(i => this.setOriginals(i));
                block.items = block.items.concat(addedItems);
                this.addingItem = false;
                this.recalculateTotals();
                this.dispatchEvent(new CustomEvent('planchanged'));
                this.refreshView = false;
            }).catch(error => {
                console.log(JSON.stringify(error));
                this.refreshView = false;
            });

        }, () => {
            this.fireErrorEvent('Failed to Add Item', 'An Unexpected Error Occurred')
        });
        
    }

    /**
     * @description Add Standard Items to Block
     */
    addStandardItems() {
        
        this.refreshView = true;
        const index = this.selectedBlockIndex;
        if (!this.newItems || (this.newItems.length == 0)) return;
        const block = this.blocks.filter(b => (b.index == index))[0];

        addBlockItems({
			blockID:        block.id, 
			templateItems:  JSON.stringify(this.newItems)
		}).then(addedItems => {
            console.log('Added items: ', addedItems);

            addedItems.forEach(i => this.setOriginals(i));

            block.items = block.items.concat(addedItems);

            this.addingItem = false;

            this.recalculateTotals();
            this.dispatchEvent(new CustomEvent('planchanged'));
            this.refreshView = false;
        }).catch(error => {
            console.log(JSON.stringify(error));
            this.refreshView = false;
        });
    }

    // --------------- CUSTOM EVENTS ---------------

    onAddItemToBlock(event) {
        if(this._addItemCurrentTab === 'custom-item') {
            this.addCustomItem();
        } else if(this._addItemCurrentTab === 'standard-item') {
            this.addStandardItems();
        }
    }

    /*
    
    addItem(event) {
        if (this.customItem == true) {
            this.addCustomItem();
            return;
        }

        this.refreshView = true;

        const index = this.selectedBlockIndex;

        if (!this.newItems || (this.newItems.length == 0)) return;

        const block = this.blocks.filter(b => (b.index == index))[0];

        let itemIndex = 0;

        if (block.items.length > 0) {
            itemIndex = block.items[block.items.length - 1].index + 1;
        }

        console.log('Block: ', block.id, block.index, block.name, block.items.length);

        addBlockItems({
			blockID: block.id, 
			templateItems: JSON.stringify(this.newItems)
		}).then(addedItems => {
            console.log('Added items: ', addedItems);

            addedItems.forEach(i => this.setOriginals(i));

            block.items = block.items.concat(addedItems);

            this.addingItem = false;

            this.recalculateTotals();
            this.dispatchEvent(new CustomEvent('planchanged'));
            this.refreshView = false;
        }).catch(error => {
            console.log(JSON.stringify(error));
            this.refreshView = false;
        });
    }

    */


    /**
     * @description Handles when the new item tab is selected
     * @param {Event} event The event that triggers this function 
     */
    onSelectItemTab(event) {
        this._addItemCurrentTab = event.target.value;
    }

    // --------------- LWC EVENTS ---------------

    
    // ----

    @track refreshView = false;

    @track _showTotals = false;

    @api resetTotals = false;

    @api editing = false;

    @api get showTotals() {
        return true; //this._showTotals;
    }

    set showTotals(show) {
        console.log('Set show totals: ' + show);
        console.log('Type: ' + (typeof show));

        if (typeof show == 'string') this._showTotals = (show == 'true');
        if (typeof show == 'boolean') this._showTotals = show;
    }

    @api planID;

    @track nutritionalItems;
    @track filteredItems;

    @track view				= true;
    @track edit				= false;
    @track daily			= true;
    @track editOrTracking	= false;

    @track adjusting = false;
    @track adjustProperties = [{value: 'Protein', label: 'Protein'}, {value: 'Carbs', label: 'Carbs'}, {value: 'Fat', label: 'Fat'}, {value: 'Calories', label: 'Calories'}];
    @track adjustProperty;
    @track adjustTypes = [{value: 'Amount', label: 'Amount'}, {value: 'Percent', label: 'Percent'}];
    @track adjustType;
    @track adjustValue;
    @track adjustShow = false;
    @track adjustPlanData;
    @track adjustBlocks;
    @track adjustTotal;
    @track adjustReference;
    @track adjustMap;
    @track adjustAllowExceed = false;

    @track labelColumns = 3;
    @track blockColumns = 6;
    @track itemColumns = 7;

    @api referenceTotals;
    @api currentTotals;

    @track differenceValue;
    @track differencePercent;
    @track differenceClass;

    @track totalsChanged = false;

    @track searchName;

    @track name;

    @api blocks;

    @track templateBlocks;
    @track templateBlockTotals;
    @track templateBlockSelected;
    @track templateBlockItems;
    @track selectedTemplateBlockID;

    @track blockIndex = 0;
    @track itemIndex  = 0;

    @api deletedBlocks = [];
    @api deletedItems  = [];

    @track addingItem = false;
    @api selectedBlockIndex;
    @api selectedBlock;
    @api newItems;
    @track selectedItems = [];

    @track allergies;
    @track allergyList = [];
    @track selectedAllergies = [];
    @track restrictions;
    @track restrictionList = [];
    @track selectedRestrictions = [];

    @track renamingBlock = false;
    @track newName;

    @track addingBlock = false;

    @track customItem = false;
    @track newCustomItem;
    @track customValues = {};

	/**
	 *	@description Used with the Add Block Modal, All the Template Blocks filted by the name
	 */
	@track addBlock_filteredBlocks = [];

	/*
	 * @description Is the component being displayed on a mobile device
	 */
	get isMobile() {
		return (FORM_FACTOR === 'Medium' || FORM_FACTOR === 'Small');
	}

	addStylingToBlocks(blocks) {
		console.log('ADDING STYLING', JSON.parse(blocks));
	}

    constructor() {
        super();
        console.log('Plan');
        getNutritionalItems()
            .then(items => {
                this.nutritionalItems = JSON.parse(items);
                this.filteredItems    = JSON.parse(items);
            })
            .catch(error => {
                console.log(JSON.stringify(error));
            });

        getTemplateBlocks()
            .then(blocks => {


				this.addStylingToBlocks(blocks);

                this.templateBlocks = JSON.parse(blocks);
            })
            .catch(error => {
                console.error("ERROR: ", JSON.stringify(error));
            });

        getFilters()
            .then(filters => {
                if (filters.allergies) {
                    this.allergies = filters.allergies.map(a => {return {name: a, checked: false}}); //.map(f => {return {value: f, label: f};});
                }
                if (filters.restrictions) {
                    this.restrictions = filters.restrictions.map(r => {return {name: r, checked: false};});
                }

                console.log('Allergies', JSON.stringify(this.allergies));
                console.log('restrictions', JSON.stringify(this.restrictions));
            })
            .catch(error => {
                console.log(JSON.stringify(error));
            });
    }

    @api get clientAllergies() {
        return [];
    }

    set clientAllergies(list) {
        console.log(list);
        if (list && list.length > 0) {
            list.forEach(a => {
                this.allergyList.push({name: a, checked: (this.allergies && this.allergies.indexOf(a) >= 0)});
            });
        }
    }

    @api get clientRestrictions() {
        return [];
    }

    set clientRestrictions(list) {
        console.log(list);
    }

    get allergyOptions() {
        return [{label: 'test', value: 'test'}];
    }

    get restrictionOptions() {
        return this.restrictions;
    }

    @api get mode() {
        if (this.view) return 'view';
        if (this.edit) return 'edit';
        return 'daily';

    }

	@track labelWidth = '';

    set mode(newMode) {
        this.view  = (newMode == 'view');
        this.edit  = (newMode == 'edit');
        this.daily  = (newMode == 'daily');

        this.editOrTracking = this.edit;

        if (this.edit) {
            this.labelColumns = 4;
            this.blockColumns = 7;
            this.itemColumns = 8;
			this.labelWidth = 'width: 550px; !important';
        } else {
            this.labelColumns = 3;
            this.blockColumns = 6;
            this.itemColumns = 7;
			this.labelWidth = 'width: 481px;';
        }
    }

    @api get planData() {
        return {
            id:		this.planID,
            name:	this.name,
            blocks: this.blocks
        }
    }

	@track planCompleted = false;

    set planData(planData) {
        const data = JSON.parse(JSON.stringify(planData));

        console.log('[plan2] Plan Data:', data);

        this.planID			= data.id;
        this.name			= data.name;
        this.blocks			= data.blocks;
		this.planCompleted	= data.completed;

        this.referenceTotals = {
            carbs: 0,
            fat: 0,
            protein: 0,
            calories: 0
        };

        this.currentTotals = {
            carbs: 0,
            fat: 0,
            protein: 0,
            calories: 0
        };

        this.rowFormats();

        this.calculateReferences();

        this.recalculateTotals();
    }

    @api get existingTotals() {
        return this.referenceTotals;
    }

    set existingTotals(totals) {
        console.log('Existing totals:', JSON.parse(JSON.stringify(totals)));

        this.referenceTotals = JSON.parse(JSON.stringify(totals));

        this.calculateReferences();
        this.recalculateTotals();
    }

    rowFormats() {


        if (this.blocks) {
            this.blocks.forEach(block => {



                if (block.items) {

					let rowNum = 0;

                    block.items.forEach(item => {

                        item.rowClasses = 'item slds-hint-parent ';

						if(rowNum % 2 == 0) {
							item.rowClasses += 'even-row';
						} else {
							item.rowClasses += 'odd-row';
						}

                        if (item.added) item.rowClasses += ' added';
                        if (item.removed) item.rowClasses += ' removed';
                        if (item.changed) item.rowClasses += ' changed';

						rowNum += 1;
                    });
                }
            });
        }
    }

    round(value) {
        if (isNaN(value)) return 0;

        if (value == undefined || value == null) return 0;

        return Math.round(value); //Number(value.toFixed(2)); //
    }

    round(value, places) {
        if (isNaN(value)) return 0;

        if (value == undefined || value == null) return 0;

        return Math.round(value);// Number(value.toFixed(places));
    }

    calculateReferences() {
        if (this.resetTotals || this.referenceTotals == undefined || this.referenceTotals == null) {
            this.referenceTotals = {
                carbs: 0,
                fat: 0,
                protein: 0,
                calories: 0
            };
        }

        //if (this.currentTotals == undefined || this.currentTotals == null) {
            this.currentTotals = {
                carbs: 0,
                fat: 0,
                protein: 0,
                calories: 0
            };
        //}



        if (this.blocks && (this.blocks.length > 0)) {
            this.blocks.forEach(b => {
                //b.index = this.blockIndex++;

                b.items.forEach(i => {
                    //i.index = this.itemIndex++;

                    if (i.original == undefined || i.original == null) {
                        i.original = {};

                        i.original.quantity = i.quantity;
                        i.original.carbs    = i.carbs;
                        i.original.fat      = i.fat;
                        i.original.protein  = i.protein;
                        i.original.calories = i.calories;

						/* ROUND ACTION
                        this.referenceTotals.carbs    += this.round(i.carbs * i.quantity);
                        this.referenceTotals.fat      += this.round(i.fat * i.quantity);
                        this.referenceTotals.protein  += this.round(i.protein * i.quantity);
                        this.referenceTotals.calories += this.round(i.calories * i.quantity);
						*/
                        this.referenceTotals.carbs    += i.carbs * i.quantity;
                        this.referenceTotals.fat      += i.fat * i.quantity;
                        this.referenceTotals.protein  += i.protein * i.quantity;
                        this.referenceTotals.calories += i.calories * i.quantity;

                        if (i.changed) {
							
							/* ROUND ACTION
                            this.currentTotals.carbs    += this.round(i.carbs * i.changedQuantity);
                            this.currentTotals.fat      += this.round(i.fat * i.changedQuantity);
                            this.currentTotals.protein  += this.round(i.protein * i.changedQuantity);
                            this.currentTotals.calories += this.round(i.calories * i.changedQuantity);

                            i.carbs						= this.round(i.carbs * i.changedQuantity, 2);
                            i.fat						= this.round(i.fat * i.changedQuantity, 2);
                            i.protein					= this.round(i.protein * i.changedQuantity, 2);
                            i.calories					= this.round(i.calories * i.changedQuantity);
							*/

                            this.currentTotals.carbs    += i.carbs * i.changedQuantity;
                            this.currentTotals.fat      += i.fat * i.changedQuantity;
                            this.currentTotals.protein  += i.protein * i.changedQuantity;
                            this.currentTotals.calories += i.calories * i.changedQuantity;

                            i.carbs						= i.carbs * i.changedQuantity;
                            i.fat						= i.fat * i.changedQuantity;
                            i.protein					= i.protein * i.changedQuantity;
                            i.calories					= i.calories * i.changedQuantity;

                        } else {
							/* ROUND ACTION
                            this.currentTotals.carbs    += this.round(i.carbs * i.quantity);
                            this.currentTotals.fat      += this.round(i.fat * i.quantity);
                            this.currentTotals.protein  += this.round(i.protein * i.quantity);
                            this.currentTotals.calories += this.round(i.calories * i.quantity);

                            i.carbs = this.round(i.carbs * i.quantity, 2);
                            i.fat = this.round(i.fat * i.quantity, 2);
                            i.protein = this.round(i.protein * i.quantity, 2);
                            i.calories = this.round(i.calories * i.quantity);
							*/
                            this.currentTotals.carbs    += i.carbs * i.quantity;
                            this.currentTotals.fat      += i.fat * i.quantity;
                            this.currentTotals.protein  += i.protein * i.quantity;
                            this.currentTotals.calories += i.calories * i.quantity;

                            i.carbs						= i.carbs * i.quantity;
                            i.fat						= i.fat * i.quantity;
                            i.protein					= i.protein * i.quantity;
                            i.calories					= i.calories * i.quantity;
                        }

                        if (i.changed && i.changedQuantity) {
                            console.log('CRef: ', JSON.stringify(this.referenceTotals));
                            console.log('CCurr: ', JSON.stringify(this.currentTotals));

                            i.quantity = i.changedQuantity;
                        }
                    }
                });
            });
        }

        this.referenceTotals = {
            carbs: Math.round(this.referenceTotals.carbs),
            fat: Math.round(this.referenceTotals.fat),
            protein: Math.round(this.referenceTotals.protein),
            calories: Math.round(this.referenceTotals.calories),
        };

        this.currentTotals = {
            carbs: Math.round(this.currentTotals.carbs),
            fat: Math.round(this.currentTotals.fat),
            protein: Math.round(this.currentTotals.protein),
            calories: Math.round(this.currentTotals.calories),
        };

        this.difference = {
            carbs: 0,
            fat: 0,
            protein: 0,
            calories: 0
        };

        this.totalsChanged = false;
    }

    recalculateTotals() {
        this.currentTotals = {
            carbs:		0,
            fat:		0,
            protein:	0,
            calories:	0
        };

        if (this.blocks && (this.blocks.length > 0)) {
            this.blocks.forEach(b => {
                b.items.forEach(i => {
                    if (i.removed == undefined || i.removed != true) {
                        this.currentTotals.carbs    += i.carbs;
                        this.currentTotals.fat      += i.fat;
                        this.currentTotals.protein  += i.protein;
                        this.currentTotals.calories += i.calories;
                    }
                });
            });
        }

        this.currentTotals = {
            carbs: Math.round(this.currentTotals.carbs),
            fat: Math.round(this.currentTotals.fat),
            protein: Math.round(this.currentTotals.protein),
            calories: Math.round(this.currentTotals.calories),
        };

        this.differenceValue = {
            carbs: this.formatDifferenceValue(this.currentTotals.carbs - this.referenceTotals.carbs),
            fat: this.formatDifferenceValue(this.currentTotals.fat - this.referenceTotals.fat),
            protein: this.formatDifferenceValue(this.currentTotals.protein - this.referenceTotals.protein),
            calories: this.formatDifferenceValue(this.currentTotals.calories - this.referenceTotals.calories)
        };

        this.differencePercent = {
            carbs: (this.referenceTotals.carbs != 0) ? this.currentTotals.carbs / this.referenceTotals.carbs : 0,
            fat: (this.referenceTotals.fat != 0) ? this.currentTotals.fat / this.referenceTotals.fat : 0,
            protein: (this.referenceTotals.protein != 0) ? this.currentTotals.protein / this.referenceTotals.protein : 0,
            calories: (this.referenceTotals.calories != 0) ? this.currentTotals.calories / this.referenceTotals.calories : 0
        };

        this.differenceClass = {
            carbs: 'carbs ' + this.getDifferenceClass(this.differenceValue.carbs),
            fat: 'fat ' + this.getDifferenceClass(this.differenceValue.fat),
            protein: 'protein ' + this.getDifferenceClass(this.differenceValue.protein),
            calories: 'calories ' + this.getDifferenceClass(this.differenceValue.calories)
        };

        console.log('Value: ' + JSON.stringify(this.differenceValue));
        console.log('Percent: ' + JSON.stringify(this.differencePercent));
        console.log('Class: ' + JSON.stringify(this.differenceClass));

        this.totalsChanged = (this.differenceValue.carbs != 0) || (this.differenceValue.fat != 0) || (this.differenceValue.protein != 0) || (this.differenceValue.calories != 0);

        console.log('Totals updated');
		console.log('[plan2] Reference Totals:', this.referenceTotals);
		console.log('[plan2] Current Totals:', this.currentTotals);

        this.dispatchEvent(new CustomEvent('totalsupdated', {detail: {reference: this.referenceTotals, current: this.currentTotals}}));
    }

    formatDifferenceValue(value) {
		/* ROUND ACTION
        const rounded = this.round(value);
		*/
        const rounded = value;


        if (rounded == 0) {
            return '0';
        } else if (value > 0) {
            return '+' + value;
        } else {
            return value;
        }
    }
    getDifferenceClass(value) {
        if (value == 0) {
            return 'zero';
        } else if (value > 0) {
            return 'increase';
        } else {
            return 'decrease';
        }
    }

    addBlockItem(event) {

        this.selectedBlockIndex = Number(event.target.name);

        this.newItems = [];

        this.selectedItems = [];

        this.newCustomItem = {name: '', carbs: 0, fat: 0, protein: 0, calories: 0};

        this.addingItem = true;
		
		//Clear the filter
        this.searchName = '';
        this.filterItems();
    }

    selectExistingItem(event) {
        this.customItem = false;
    }

    selectCustomItem(event) {
        this.customItem = true;
    }

    setOriginals(item) {
        console.log('Setting originals: ', item.id);

        if (item.original == undefined || item.original == null) {
            item['original'] = {};

            item.original.quantity = item.quantity;
            item.original.carbs    = item.carbs;
            item.original.fat      = item.fat;
            item.original.protein  = item.protein;
            item.original.calories = item.calories;
			
            item.carbs    = item.carbs * item.quantity;
            item.fat      = item.fat * item.quantity;
            item.protein  = item.protein * item.quantity;
            item.calories = item.calories * item.quantity;

            console.log('Originals: ', item.original);
        } else {
            console.log('Existing originals');
        }
    }

    cancelAddItem(event) {
        this.addingItem = false;
    }

    deleteBlock(event) {
        const index = Number(event.target.name);

        const block = this.blocks.filter(b => (b.index == index))[0];

        this.refreshView = true;

        deleteBlock({blockID: block.id})
            .then(() => {
                if (block.id) this.deletedBlocks.push(block.id);

                this.blocks = this.blocks.filter(b => (b.index != index));

                this.recalculateTotals();

                this.refreshView = false;
            })
            .catch(error => {
                console.log(JSON.stringify(error));
            });
    }

    deleteItem(event) {
        this.refreshView = true;

        const key = event.target.name;

        const item = this.blocks.map(b => b.items.filter(i => (i.key == key))).flat()[0];

        if (item.id) {
            this.deletedItems.push(item.id);

            deleteItem({itemID: item.id})
                .then(() => {
                    this.blocks.forEach(b => {
                        let deleted = -1;

                        for (let i = 0; i < b.items.length; i++) {
                            if (b.items[i].key == key) {
                                deleted = i;
                                break;
                            }
                        }

                        if (deleted >= 0) b.items.splice(deleted, 1);
                    });

                    this.recalculateTotals();

                    this.refreshView = false;
                })
                .catch(error => {
                    console.log(JSON.stringify(error));
                });
        }
    }

    removeItem(event) {
        this.refreshView = true;

        const key = event.target.name;

        console.log('Removing: ' + key);

        this.blocks.forEach(block => {
            block.items.forEach(item => {
                if (item.key == key) {
                    console.log('Removing: ' + item.id + ', ' + item.name + ', ' + item.key + ', ' + item.index);

                    if (item.removed == undefined || item.removed == null) {
                        item.removed = true;
                    } else {
                        item.removed = !item.removed;
                    }

                    deleteItem({itemID: item.id, removeStatus: item.removed})
                        .then(() => {
                            this.rowFormats();

                            this.recalculateTotals();

                            this.refreshView = false;
                        })
                        .catch(error => {
                            console.log(JSON.stringify(error));
                        });
                }
            });
        });

        //if (item.id) item.removed = true; //this.deletedItems.push(item.id);

        /*this.blocks.forEach(b => {
            b.items = b.items.filter(i => (i.index != index))
        });*/
    }

    changeQuantity(event) {
	
        const key         = event.target.name;
        const newQuantity = parseFloat(event.target.value);

        const parts = key.split('-');

        const blockIndex = Number(parts[0]);
        const itemIndex = Number(parts[1]);

        if (isNaN(newQuantity)) return;

        const block = this.blocks.filter(b => b.index == blockIndex)[0];

        const item = this.blocks.map(b => b.items.filter(i => (i.key == key))).flat()[0];

        if (!item) return;

        item.quantity = newQuantity;

        const ratio = newQuantity / item.original.quantity;
		
        item.carbs    = newQuantity * item.original.carbs;
        item.fat      = newQuantity * item.original.fat;
        item.protein  = newQuantity * item.original.protein;
        item.calories = newQuantity * item.original.calories;

        item.changed = true;

        this.recalculateTotals();

        block.dummy++; //HC: What??

        this.dispatchEvent(new CustomEvent('planchanged'));

    }

	changeQuantityV2(event) {

        const key         = event.detail.name;
        const newQuantity = event.detail.value;
		
		console.log("Key: " + key);
		console.log("New Quantity: " + newQuantity);
		
        const parts = key.split('-');

        const blockIndex = Number(parts[0]);
        const itemIndex = Number(parts[1]);

        if (isNaN(newQuantity)) return;

        const block = this.blocks.filter(b => b.index == blockIndex)[0];

        const item = this.blocks.map(b => b.items.filter(i => (i.key == key))).flat()[0];

        if (!item) return;

        item.quantity = newQuantity;

        const ratio = newQuantity / item.original.quantity;
		
        item.carbs    = newQuantity * item.original.carbs;
        item.fat      = newQuantity * item.original.fat;
        item.protein  = newQuantity * item.original.protein;
        item.calories = newQuantity * item.original.calories;

        item.changed = true;

        this.recalculateTotals();

        block.dummy++; //HC: What??

        this.dispatchEvent(new CustomEvent('planchanged'));

    }

    adjustPlan(event) {
        this.adjustProperty = this.adjustProperties[3].value;
        this.adjustType = this.adjustTypes[0].value;
        this.adjusting = true;
        this.adjustAllowExceed = false;

        this.adjustValue = this.currentTotals.calories;
    }

    setAdjustAllowExceed(event) {
        this.adjustAllowExceed = event.target.checked;
    }

    selectAdjustProperty(event) {
        this.adjustProperty = event.target.value;

        if (this.adjustType == 'Amount') {
            if (this.adjustProperty == 'Calories') this.adjustValue = this.currentTotals.calories;
            if (this.adjustProperty == 'Carbs') this.adjustValue = this.currentTotals.carbs;
            if (this.adjustProperty == 'Fat') this.adjustValue = this.currentTotals.fat;
            if (this.adjustProperty == 'Protein') this.adjustValue = this.currentTotals.protein;
        }
        if (this.adjustType == 'Percent') {
            this.adjustValue = 100;
        }

        console.log(this.adjustProperty + ': ' + this.adjustValue);
    }

    selectAdjustType(event) {
        this.adjustType = event.target.value;

        if (this.adjustType == 'Amount') {
            if (this.adjustProperty == 'Calories') this.adjustValue = this.currentTotals.calories;
            if (this.adjustProperty == 'Carbs') this.adjustValue = this.currentTotals.carbs;
            if (this.adjustProperty == 'Fat') this.adjustValue = this.currentTotals.fat;
            if (this.adjustProperty == 'Protein') this.adjustValue = this.currentTotals.protein;
        }
        if (this.adjustType == 'Percent') {
            this.adjustValue = 100;
        }

        console.log(this.adjustType + ': ' + this.adjustValue);
    }

    setAdjustValue(event) {
        const value = event.target.value;

        this.adjustValue = Number(value);
    }
	
    adjustCalculate(event) {
        this.adjustShow = false;

        this.adjustMap = {};

        this.adjustBlocks = JSON.parse(JSON.stringify(this.blocks));

        this.adjustPlanData = {
            id: 'adjusting',
            name: 'Adjusting Plan',
            blocks: this.adjustBlocks
        }

        let adjustTarget = {
            carbs: this.currentTotals.carbs,
            fat: this.currentTotals.fat,
            protein: this.currentTotals.protein,
            calories: this.currentTotals.calories
        };

        this.adjustTotal = {
            carbs: this.currentTotals.carbs,
            fat: this.currentTotals.fat,
            protein: this.currentTotals.protein,
            calories: this.currentTotals.calories
        };

        this.adjustReference = {
            carbs: this.currentTotals.carbs,
            fat: this.currentTotals.fat,
            protein: this.currentTotals.protein,
            calories: this.currentTotals.calories
        }

        console.log(`Type: ${this.adjustType}, Value: ${this.adjustProperty}`);

        if (this.adjustType == 'Amount') {
            if (this.adjustProperty == 'Carbs') adjustTarget.carbs = this.adjustValue;
            if (this.adjustProperty == 'Fat') adjustTarget.fat = this.adjustValue;
            if (this.adjustProperty == 'Protein') adjustTarget.protein = this.adjustValue;
            if (this.adjustProperty == 'Calories') adjustTarget.calories = this.adjustValue;
        }
        if (this.adjustType == 'Percent') {
            if (this.adjustProperty == 'Carbs') adjustTarget.carbs *= (this.adjustValue / 100);
            if (this.adjustProperty == 'Fat') adjustTarget.fat *= (this.adjustValue / 100);
            if (this.adjustProperty == 'Protein') adjustTarget.protein *= (this.adjustValue / 100);
            if (this.adjustProperty == 'Calories') adjustTarget.calories *= (this.adjustValue / 100);
        }

        console.log('Targets: ', adjustTarget);

        const upOrDown = (adjustTarget.carbs > this.currentTotals.carbs) ||
                         (adjustTarget.fat > this.currentTotals.fat) ||
                         (adjustTarget.protein > this.currentTotals.protein) ||
                         (adjustTarget.calories > this.currentTotals.calories);

        console.log('UpOrDown:', upOrDown);

        adjustLoop:
        for (;;) {

			//Track if an item was updated
			let updatedItem = false;

            let metOrExceeded = false;

            for (let b = 0; b < this.adjustBlocks.length; b++) {
                const block = this.adjustBlocks[b];

                for (let i = 0; i < block.items.length; i++) {
                    const item = block.items[i];

                    const useItem = (this.adjustProperty == 'Carbs' && item.original.carbs > 0) ||
                                    (this.adjustProperty == 'Fat' && item.original.fat > 0) ||
                                    (this.adjustProperty == 'Protein' && item.original.protein > 0) ||
                                    (this.adjustProperty == 'Calories' && item.original.calories > 0);

					console.log("[plan2] At Item: ", JSON.parse(JSON.stringify(item)));

                    if (item.divisible && useItem) {
                        console.log('Item', JSON.stringify(item));

						let newQuantity = item.quantity + (item.increment * (upOrDown ? 1 : -1));

						//If the new quantity is above the minimum quantity allowed, then update it
						if(item.minimumQuantity <= newQuantity) {

						    item.quantity = newQuantity;
							updatedItem = true;

							const adjusted = {
								carbs:    item.original.carbs * item.quantity,
								fat:      item.original.fat * item.quantity,
								protein:  item.original.protein * item.quantity,
								calories: item.original.calories * item.quantity
							}

							if (adjusted.calories == 0) continue;

							metOrExceeded = this.checkAdjustment(this.adjustProperty, upOrDown, this.adjustTotal, adjustTarget, item, adjusted);

							console.log('Returned: ', JSON.stringify(metOrExceeded));

							item.changed = true;
							item.changedQuantity = item.quantity;
							item.carbs = adjusted.carbs;
							item.fat = adjusted.fat;
							item.protein = adjusted.protein;
							item.calories = adjusted.calories;

							this.adjustMap[item.id] = item;

							console.log('New Item: ', JSON.stringify(item));

							if (metOrExceeded) {
								console.log('done');
								break adjustLoop;
							}

						}


                    }
                }
            }

            if (!metOrExceeded) {
                console.log('Looped through all blocks and items without reaching the target, starting again.');
                //b = 0;
                //i = 0;
            }

			//If we haven't updated an item, then break the loop
			if(updatedItem === false) {
				break adjustLoop;
			}


        }

        this.adjustShow = true;

        this.dispatchEvent(new CustomEvent('planchanged'));
    }

    checkAdjustment(property, upOrDown, total, target, item, adjusted) {
        const check = {
            carbs: total.carbs - item.carbs + adjusted.carbs,
            fat: total.fat - item.fat + adjusted.fat,
            protein: total.protein - item.protein + adjusted.protein,
            calories: total.calories - item.calories + adjusted.calories
        };

        console.log('Property', JSON.stringify(property));
        console.log('Check', JSON.stringify(check));

        const metOrExceeded = (property == 'Carbs') ? (upOrDown ? (check.carbs >= target.carbs) : (check.carbs <= target.carbs)):
                              (property == 'Fat') ? (upOrDown ? (check.fat >= target.fat) : (check.fat <= target.fat)):
                              (property == 'Protein') ? (upOrDown ? (check.protein >= target.protein) : (check.protein <= target.protein)):
                              (property == 'Calories') ? (upOrDown ? (check.calories >= target.calories) : (check.calories <= target.calories)): false;

        console.log('Met or exceeded:', JSON.stringify(metOrExceeded));

        total.carbs = check.carbs;
        total.fat = check.fat;
        total.protein = check.protein;
        total.calories = check.calories;

        console.log('New total:', JSON.stringify(total));

        return metOrExceeded;
    }

    cancelAdjust(event) {
        this.adjusting = false;
    }

    saveAdjust(event) {
        this.adjusting = false;

        if (this.adjustMap == undefined && this.adjustMap == null) return;

        this.blocks.forEach(block => {
            block.items.forEach(item => {
                const adjusted = this.adjustMap[item.id];

                if (adjusted != undefined && adjusted != null) {
                    item.changed = adjusted.changed;
                    item.changedQuantity = adjusted.quantity;
                    item.quantity = adjusted.quantity;
                    item.carbs = adjusted.carbs;
                    item.fat = adjusted.fat;
                    item.protein = adjusted.protein;
                    item.calories = adjusted.calories;
                }
            });
        });

        this.recalculateTotals();

        this.dispatchEvent(new CustomEvent('planchanged'));

    }

    filterItems() {
        let filtered = [];

        this.nutritionalItems.forEach(item => {
            let select = true;

            const itemName = item.name.toLowerCase();

            if (this.searchName && (this.searchName != '') && (itemName.indexOf(this.searchName.toLowerCase()) < 0)) select = false;

            if (this.selectedAllergies && (this.selectedAllergies.length > 0) && (item.allergies != '')) {
                console.log('Checking A, ' + JSON.stringify(item.name), JSON.stringify(this.selectedAllergies), item.allergies.split(';'));
                console.log(this.hasOverlap(this.selectedAllergies, item.allergies.split(';')));
                if (this.hasOverlap(this.selectedAllergies, item.allergies.split(';'))) select = false;
            }

            if (this.selectedRestrictions && (this.selectedRestrictions.length > 0)) {
                if ((item.restrictions === null) || (item.restrictions == '')) {
                    select = false;
                } else if (!this.hasOverlap(this.selectedRestrictions, item.restrictions.split(';'))) {
                    select = false;
                }
            }

            if (select) filtered.push(JSON.parse(JSON.stringify(item)));
        });

        this.filteredItems = filtered;
    }

    hasOverlap(values, checks) {
        let matched = false;

        values.forEach(v => {
            checks.forEach(c => {
                console.log('CH:', JSON.stringify(c), JSON.stringify(v));
                if (c == v) matched = true;
            });
        });

        return matched;
    }

    setSearchName(event) {
        this.searchName = event.target.value;
        //console.log(JSON.stringify(this.searchName));

        this.filterItems();
    }

    selectAddItem(event) {
        const itemID   = event.target.name;
        const selected = event.target.checked;

        if (this.selectedItems === null) {
            if (selected) this.selectedItems = [itemID];
        } else {
            if (selected) this.selectedItems.push(itemID);
            if (!selected) this.selectedItems = this.selectedItems.filter(i => (i != itemID));
        }

        this.newItems = [];

        this.nutritionalItems.forEach(item => {
            if (this.selectedItems.includes(item.id)) {
                this.newItems.push(JSON.parse(JSON.stringify(item)));
            }
        });

        console.log('SELECT Add Items:');
        console.log(JSON.stringify(this.newItems));
        console.log(JSON.stringify(this.selectedItems));
    }

    selectAllergies(event) {
        const allergy = event.target.name;
        const selected = event.target.checked;

        if (this.selectedAllergies === null) {
            if (selected) this.selectedAllergies = [allergy];
        } else {
            if (selected) this.selectedAllergies.push(allergy);
            if (!selected) this.selectedAllergies = this.selectedAllergies.filter(i => (i != allergy));
        }

        this.filterItems();
    }

    selectRestrictions(event) {
        const restriction = event.target.name;
        const selected = event.target.checked;

        if (this.selectedRestrictions === null) {
            if (selected) this.selectedRestrictions = [restriction];
        } else {
            if (selected) this.selectedRestrictions.push(restriction);
            if (!selected) this.selectedRestrictions= this.selectedRestrictions.filter(i => (i != restriction));
        }

        this.filterItems();
    }

    addBlock(event) {
        this.addingBlock = true;
        this.newName = '';
        this.selectedTemplateBlockID = null;
        this.templateBlockItems = [];
        this.templateBlockSelected = false;
        this.templateBlockTotals = {protein: 0, carbs: 0, fat: 0, calories: 0};
		
		/*this.newName = event.target.value;*/

		this.addBlock_filterTemplateBlocks();



    }

	addBlock_filterTemplateBlocks() {

		let filteredBlocks = [];

	
		this.templateBlocks.forEach(tmpBlock => {
			if(tmpBlock.name.toUpperCase().includes(this.newName.toUpperCase())) {
				filteredBlocks.push(tmpBlock);
			}

		});

		this.addBlock_filteredBlocks = filteredBlocks;

	
	} 




    cancelAddBlock(event) {
        this.addingBlock = false;
    }

    selectTemplateBlock(event) {
        console.log('Selected template block ID: ', event.target.value);

        this.selectedTemplateBlockID = event.target.value;

        this.templateBlockSelected = this.selectedTemplateBlockID && (this.selectedTemplateBlockID != '');

        console.log('template block selected: ', this.templateBlockSelected);

        this.templateBlockItems = [];

        if (this.templateBlockSelected) {
            const block = this.templateBlocks.filter(b => b.id == this.selectedTemplateBlockID)[0];

            this.templateBlockItems = block.items;

            this.templateBlockTotals = {protein: 0, carbs: 0, fat: 0, calories: 0};

            this.templateBlockItems.forEach(item => {
                this.templateBlockTotals.protein += (item.protein);
                this.templateBlockTotals.carbs += (item.carbs);
                this.templateBlockTotals.fat += (item.fat);
                this.templateBlockTotals.calories += (item.calories);
            });
        }
    }

    saveAddBlock(event) {
        let newIndex = 0;

        this.blocks.forEach(block => {
            if (block.index > newIndex) {
                console.log('Block: ' + block.name + ', ' + block.index);

                newIndex = block.index;
            }
        });

        newIndex++;

        console.log('New index: ' + newIndex);

        addBlock({
			planID: this.planID, 
			name: this.newName, 
			index: newIndex, 
			templateID: this.selectedTemplateBlockID
		}).then(block => {
            block.items.forEach(i => {
                i.original = {};

                i.original.quantity = i.quantity;
                i.original.carbs    = i.carbs / i.quantity;
                i.original.fat      = i.fat / i.quantity;
                i.original.protein  = i.protein / i.quantity;
                i.original.calories = i.calories / i.quantity;

            });

            this.blocks.push(block);

            this.addingBlock = false;

            this.recalculateTotals();
        }).catch(error => {
            console.log(JSON.stringify(error));
        });
    }

    moveBlockUp(event) {
	
		//If we are already moving the blocks then don't move the block down
		if(this._movingBlocksOrItems === true) {
			return;
		}
		
        let index = Number(event.target.name);
		
		//Get the placement of the block in the list
		let blockPlacement = -1;
		for(let i = 0; i < this.blocks.length; i++) {
			let currentBlock = this.blocks[i];
			if(currentBlock.index === index) {
				blockPlacement = i;
			}
		};

        if (blockPlacement > 0 && blockPlacement != -1) {
			
			this._movingBlocksOrItems = true;
		
			for(let i = 0; i < this.blocks.length; i++) {
				
				let currentBlock = this.blocks[i];
				
				if(currentBlock.index === index) {
					
					let aboveBlock = this.blocks[i-1];
					
					moveBlocks({
						blockID1:	currentBlock.id,  
						index1:		aboveBlock.index,
						blockID2:	aboveBlock.id,	
						index2:		currentBlock.index
					}).then(() => {

						let currBlockIndex		= currentBlock.index;
						let aboveBlockIndex		= aboveBlock.index;
						
						this.blocks[i-1]		= currentBlock;
						this.blocks[i-1].index	= aboveBlockIndex;
						this.rekeyItems(this.blocks[i-1]);

						this.blocks[i]			= aboveBlock;
						this.blocks[i].index	= currBlockIndex;
						this.rekeyItems(this.blocks[i]);
						
						this._movingBlocksOrItems = false; //Set to not moving the item or block

					}).catch(error => {
						this._movingBlocksOrItems = false; //Set to not moving the item or block
						console.error("[plan2] ERROR: ", JSON.parse(JSON.stringify(error)));
					});

				}
				
			}
			
        }

    }

    moveBlockDown(event) {

		//If we are already moving the blocks then don't move the block down
		if(this._movingBlocksOrItems === true) {
			return;
		}
		
        let index = Number(event.target.name);
		
		//Get the placement of the block in the list
		let blockPlacement = -1;
		for(let i = 0; i < this.blocks.length; i++) {
			let currentBlock = this.blocks[i];
			if(currentBlock.index === index) {
				blockPlacement = i;
			}
		};

        if (blockPlacement < (this.blocks.length - 1) && blockPlacement != -1) {
		
			this._movingBlocksOrItems = true;
		
			for(let i = 0; i < this.blocks.length; i++) {
				
				let currentBlock = this.blocks[i];
				
				if(currentBlock.index === index) {
				
					let belowBlock = this.blocks[i+1];
					
					moveBlocks({
						blockID1:	currentBlock.id,  
						index1:		belowBlock.index,
						blockID2:	belowBlock.id,	
						index2:		currentBlock.index
					}).then(() => {

						
					
						let currBlockIndex		= currentBlock.index;
						let belowBlockIndex		= belowBlock.index;
						
						this.blocks[i+1]		= currentBlock;
						this.blocks[i+1].index	= belowBlockIndex;
						this.rekeyItems(this.blocks[i+1]);

						this.blocks[i]			= belowBlock;
						this.blocks[i].index	= currBlockIndex;
						this.rekeyItems(this.blocks[i]);
						
						this._movingBlocksOrItems = false; //Set to not moving the item or block

					}).catch(error => {
						this._movingBlocksOrItems = false; //Set to not moving the item or block
						console.error("[plan2] ERROR: ", JSON.parse(JSON.stringify(error)));
					});
					
				}
				
			}

        }
    }

    moveItemUp(event) {

		if(this._movingBlocksOrItems === true) {
			return;
		}



        const key = event.target.name;
		
		//Get the order of the item, and the order of the block
        const parts			= key.split('-');
        const itemOrder		= Number(parts[1]);
        const blockOrder	= Number(parts[0]);
		
		//Get the Block
        let block = this.blocks.filter(b => b.index == blockOrder)[0];
		
		//Don't move the top item up
		
		for(let i = 0; i < block.items.length; i++) {
		
			let currItem = block.items[i];

			//If we have found the current item move it up (and it's not the top of the list)
			if(currItem.key === key) {

				if(i === 0) {
					this._movingBlocksOrItems = false;
				} else {

					this._movingBlocksOrItems = true;

					let aboveItem = block.items[i - 1];
					moveItems({
						itemID1: currItem.id,		index1: aboveItem.index,
						itemID2: aboveItem.id,		index2: currItem.index
					}).then(() => {
						//Swap the item
						const swap = aboveItem.index;
						aboveItem.index				= currItem.index;
						currItem.index				= swap;
						aboveItem.key				= String(blockOrder) + '-' + String(aboveItem.index);
						currItem.key				= String(blockOrder) + '-' + String(currItem.index);
						block.items[i-1]			= currItem;
						block.items[i]				= aboveItem;

						
						this._movingBlocksOrItems = false;

					}).catch(error => {
						console.log(JSON.stringify(error));

						
						this._movingBlocksOrItems = false;

					});
				}
			}
		}

    }

    moveItemDown(event) {
	
		if(this._movingBlocksOrItems === true) {
			return;
		}

        const key = event.target.name;
		
		//Get the order of the item, and the order of the block
        const parts			= key.split('-');
        const itemOrder		= Number(parts[1]);
        const blockOrder	= Number(parts[0]);
		
		//Get the Block
        let block = this.blocks.filter(b => b.index == blockOrder)[0];
		

		for(let i = 0; i < block.items.length; i++) {
		
			let currItem = block.items[i];

			//If we have found the current item move it up (and it's not the top of the list)
			if(currItem.key === key) {

				if(i === block.items.length - 1) {
					this._movingBlocksOrItems = false;
				} else {
					let belowItem = block.items[i + 1];
					
					this._movingBlocksOrItems = true;

					moveItems({
						itemID1: currItem.id,		index1: belowItem.index,
						itemID2: belowItem.id,		index2: currItem.index
					}).then(() => {
						//Swap the item
						const swap = belowItem.index;
						belowItem.index				= currItem.index;
						currItem.index				= swap;
						belowItem.key				= String(blockOrder) + '-' + String(belowItem.index);
						currItem.key				= String(blockOrder) + '-' + String(currItem.index);
						block.items[i + 1]			= currItem;
						block.items[i]				= belowItem;
						
						this._movingBlocksOrItems = false;

					}).catch(error => {
						console.log(JSON.stringify(error));
						
						this._movingBlocksOrItems = false;

					});
				}
			}
		}

    }

    rekeyItems(block) {
        if (block.items) {
            let index = 0;

            block.items.forEach(item => {
                item.key = (block.index + '-' + index);

                index++;
            });
        }
    }

    selectAllItems(event) {
        console.log('selecting all');
        this.blocks.forEach(block => {
            block.items.forEach(item => {
                console.log('selecting');
                item.selected = true;
            });
        });
    }

    renameBlock(event) {
        this.selectedBlockIndex = event.target.name;

        this.selectedBlock = this.blocks.filter(b => (b.index == this.selectedBlockIndex))[0];
        this.newName = this.selectedBlock.name;
        console.log(this.selectedBlock);
        this.renamingBlock = true;
    }

    setBlockName(event) {
        this.newName = event.target.value;
		this.addBlock_filterTemplateBlocks();
    }

    saveBlockName(event) {
        this.selectedBlock.name = this.newName;

        renameBlock({blockID: this.selectedBlock.id, name: this.selectedBlock.name})
            .then(() => {
               this.renamingBlock = false;
            })
            .catch(error => {
                console.log(JSON.stringify(error));
            });
    }

    toggleCustomItem(event) {
        //const checked = event.target.checked;
        //console.log('Checked: ' + checked);

        this.customItem = !this.customItem;
    }

    setCustomName(event) {
        this.newCustomItem.name = event.target.value;
    }

    setCustomCarb(event) {
        this.newCustomItem.carbs = Number(event.target.value);
    }

    setCustomFat(event) {
        this.newCustomItem.fat = Number(event.target.value);
    }

    setCustomProtein(event) {
        this.newCustomItem.protein = Number(event.target.value);
    }

    setCustomCalories(event) {
        this.newCustomItem.calories = Number(event.target.value);
    }
}