import { LightningElement, api, track, wire }	from 'lwc';
import { getObjectInfo }						from 'lightning/uiObjectInfoApi';
import Photo__c									from '@salesforce/schema/Photo__c';
import UploadFileToDropbox						from '@salesforce/apex/CommunityCheckInController.UploadFileToDropbox';
import GetAllMultiMediaFiles					from '@salesforce/apex/CommunityCheckInController.GetAllMultiMediaFiles';
import DeleteMultiMediaFile						from '@salesforce/apex/CommunityCheckInController.DeleteMultiMediaFile';
import GetDropboxDetails						from '@salesforce/apex/CommunityCheckInController.GetDropboxDetails';

const MONTHS = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

/**
 * @description Add MultiMedia files for the Client
 */
export default class CciXAddMultiMediaFiles extends LightningElement {

	/* --------------- VARIABLES --------------- */

	@track _isLoading = false;
	
	/**
	 * @description Tracks if a new file is being saved
	 */
	@track _isSavingFile = false;

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;

	/**
	 * @description Holds the Record Type for the Other Photos
	 */ 
	@track _otherPhotoRecType;

	/**
	 * @description The Id of the New file
	 */ 
	@track _newFileId;

	/**
	 * @description The Id of the file that we want to delete
	 */
	@track _fileToDelete;

	/**
	 * @description Are we displaying the Add Item Panel 
	 */
	@track _displayAddItemPanel = false;

	/**
	 * @description All MultiMedia Files that relate to the Check-In
	 */
	@track _allMultiMediaFiles = [];

	/**
	 * @description The Authorization code for dropbox
	 */
	@track _dropboxAuthCode = '';

	/**
	 * @description The folder that is used to upload the photo
	 */
	@track _dropboxReferencePath = '';


	/**
	 * @description A list of files that were uploaded
	 */
	@track _dropboxFilesSelected = [];

	/** 
	 * @description A list of Dropbox File Names
	 */
	@track _dropboxFileNames = [];

	/**
	 * @description The name of the Check-In
	 */
	@track _checkInName = '';

	@track _photosSubmitted = 0;


	/* --------------- GETTERS --------------- */
	
	/**
	 * @description The Id of the Check-In
	 */
	@api get checkInId() {
		return this._checkInId;
	}
	
	/**
	 * @description Tracks if the Delete Item Panel is open
	 */
	get _displayDeleteItemPanel() {
		return typeof this._fileToDelete !== 'undefined'
	}

	/**
	 * @description Is all the data loaded
	 */
	get allDataIsLoaded() {
		return typeof this._otherPhotoRecType !== 'undefined' && typeof this._checkInId !== 'undefined';
	}

	/**
	 * @description Tracks if a new file an be submitted or not
	 */
	get cantSubmitNewFile() {
		return this._dropboxFilesSelected.length === 0;
	}

	get isFileUploaded() {
		return typeof this._newFileId !== 'undefined';
	}

	/* --------------- SETTERS --------------- */

	/**
	 * @description The Id of the Check-In
	 * @param {String} newCheckInId The new Id of the Check-In
	 */
	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;

		
		GetDropboxDetails({
			checkInId: this._checkInId
		}).then(result => {
			this._checkInName			= result.CheckInName;
			this._dropboxAuthCode		= result.DropboxAuthorizationKey;
			this._dropboxReferencePath	= result.DropboxReferencePath;

			//Refresh Data
			this.refreshData(); 
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		}); 

	}

	/* --------------- CUSTOM METHODS --------------- */
	
	formatDate(date) {

		const todaysDate	= date.getDate();
		const todaysMonth	= MONTHS[date.getMonth()];
		const todaysYear	= date.getFullYear();

		let dateStr = '';

		if(todaysDate < 10) {
			dateStr = '0' + todaysDate;
		} else {
			dateStr = '' + todaysDate;
		}

		return todaysMonth + dateStr + '-' + todaysYear;

	}

	/**
	 * @description Refresh the Multimedia Files
	 */
	@api refreshData(onSuccess, onError) {
	
		GetAllMultiMediaFiles({
			checkInId: this._checkInId
		}).then(result => {
			this._allMultiMediaFiles = JSON.parse(JSON.stringify(result));
			console.log("MultiMedia Files: ", JSON.parse(JSON.stringify(this._allMultiMediaFiles)));

			if(onSuccess) {
				onSuccess(this._allMultiMediaFiles);
			}

		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)) );
			
			if(onError) {
				onError();
			}
		});

	}

	/**
	 * @description Delete File
	 */
	deleteFile(fileId) {
	
		this._isLoading = true;

		DeleteMultiMediaFile({
			fileId: fileId
		}).then(result => {
			this._fileToDelete = undefined;
			this.refreshData(() => {
				this._isLoading = false;
			});
			
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}
	
	/* --------------- CUSTOM EVENTS --------------- */

	onMultiMediaPanelClose() {
        this.dispatchEvent(new CustomEvent('close'));
	}

	onCancelDeleteFile() {
		this._fileToDelete = undefined;
	}

	onConfirmDeleteFile() {
		this.deleteFile(this._fileToDelete);
	}	

	/**
	 * @description Remove File
	 */
	onRemoveFile(event) {
		let fileId			= event.target.dataset.itemId;
		this._fileToDelete	= fileId;
	}

	onDisplayAddMultiMediaItemPanel(event) {
		this._displayAddItemPanel	= true;
	} 

	onCloseAddMultiMediaItemPanel(event) {
		this._displayAddItemPanel	= false;
	}
	
	onFilesSelection(event) {

		let selectedDetails = event.detail.fileDetails;
		
		console.log("Details: ", JSON.parse(JSON.stringify(selectedDetails)));
		
		let processedResults = [];

		for(let i = 0; i < selectedDetails.selectedFileNames.length; i++) {
		
			processedResults.push({
				fileName:	selectedDetails.selectedFileNames[i],
				type:		null,
				order:		i
			});

		}
		
		this._dropboxFilesSelected = processedResults;

	}

	onFieldTypeChange(event) {
		this._dropboxFilesSelected[event.target.dataset.order].type = event.target.value;
		console.log("Updated Type");
	}

	/**
	 * @description Submit selected files into the form
	 */
	onSubmitNewFiles(event) {
		//this._isSavingFile = true;

		//Count the different types for this check-In
		let mutliTypesInDatabase = {};

		for(let i = 0; i < this._allMultiMediaFiles.length; i++) { 
			let multiFile = this._allMultiMediaFiles[i];
			if(typeof mutliTypesInDatabase[multiFile.Type__c] === "undefined") {
				mutliTypesInDatabase[multiFile.Type__c] = 0;
			}
			mutliTypesInDatabase[multiFile.Type__c] += 1;
		}


		console.log(JSON.parse(JSON.stringify(this._dropboxFilesSelected)));

		let dropboxForms = this.template.querySelectorAll(".drop-box-form");

	

		this._dropboxFileNames = [];
		for(let i = 0; i < this._dropboxFilesSelected.length; i++) {

		
			if(typeof mutliTypesInDatabase[this._dropboxFilesSelected[i].type] === "undefined") {
				mutliTypesInDatabase[this._dropboxFilesSelected[i].type] = 0;
			}

			this._dropboxFileNames.push(this._checkInName + '_' + this.formatDate(new Date()) + '_' + this._dropboxFilesSelected[i].type + '_' + mutliTypesInDatabase[this._dropboxFilesSelected[i].type]);

			mutliTypesInDatabase[this._dropboxFilesSelected[i].type] += 1;
		}

		let dropboxFileSelector = this.template.querySelector(".dropbox-file-selector");
		dropboxFileSelector.Upload(this._dropboxFileNames, (success) => {
			console.log("Successfully Uploaded: ", JSON.parse(JSON.stringify(success)) );

			for(let i = 0; i < success.uploadResults.length; i++) {
			
				this._dropboxFilesSelected[i].referencePath		= success.uploadResults[i].uploadResult.path_display;
				
				if(success.uploadResults[i].result) {
					this._dropboxFilesSelected[i].dropboxPath		= success.uploadResults[i].result.url;
				} else if(success.uploadResults[i].error) {
					this._dropboxFilesSelected[i].dropboxPath		= success.uploadResults[i].error.error.shared_link_already_exists.metadata.url;
				}

				this._dropboxFilesSelected[i].dropboxPath = this._dropboxFilesSelected[i].dropboxPath.substring(0, this._dropboxFilesSelected[i].dropboxPath.length - ("dl=0").length) + "raw=1";

			}

			setTimeout(function(){ 
				//After uploading, update the dropbox file forms
				for(let i = 0; i < dropboxForms.length; i++) { 
					dropboxForms[i].submit();
				}
			}, 1000);


			
		});

	}

	onFileUploadCompleted(event) {
		
	}

	onPhotoSuccessfulSubmission(event) {

		console.log("Photo Successfully Submitted")

		this._photosSubmitted += 1;

		if(this._photosSubmitted === this._dropboxFilesSelected.length) {
			console.log("All Photos Submitted");
			this._displayAddItemPanel	= false;
			this.refreshData();
		}

	}

	@wire(getObjectInfo, { objectApiName: Photo__c })
	getRecordTypeData({ data, error }) {

		if(data) {
			
			for(let recId in data.recordTypeInfos) {
				let recType = data.recordTypeInfos[recId];
				if(recType.name === "Client Other Photos") {
					console.log("Client Other Photos Record Type: ", JSON.parse(JSON.stringify(recType)));
					this._otherPhotoRecType = recType;
				}
			}

		} else if(error) {
			console.error("[cci_AddMultiMediaFiles] ERROR: ", JSON.parse(JSON.stringify(error)) );
		}

	}


}