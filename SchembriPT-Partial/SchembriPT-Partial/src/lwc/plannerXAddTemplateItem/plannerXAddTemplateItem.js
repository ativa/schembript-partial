import { LightningElement, api, track } from 'lwc';
import GetNutritionalTemplateItemInformation from '@salesforce/apex/AT_LWCC_PlannerAddTemplateItem.GetNutritionalTemplateItemInformation';

export default class PlannerXAddTemplateItem extends LightningElement {

	/* ---------- VARIABLES ---------- */

	@track _isLoading = false;

	@track _blockId;
	
	@track _allAllergies				= [];

	@track _selectedAllergies			= [];
	
	@track _allDietRestrictions			= [];

	@track _selectedDietRestrictions	= [];

	@track _allTemplateItems			= [];

	@track _selectedTemplateItems		= [];

	@track _searchName					= "";
	
	/* ---------- GETTERS ---------- */

	@api get blockId() {
		return this._blockId;
	}

	get templateItems() {

		let viewedTemplateItems = [];
		
		mainLoop: for(let i = 0; i < this._allTemplateItems.length; i++) {

			//If the name is not a part of the search name, or the template contains the selected allergy, or dietary restriction then ignore it
			if(this._searchName != "" && this._allTemplateItems[i].itemName.toUpperCase().includes(this._searchName.toUpperCase()) === false) {
				continue mainLoop;
			}
			if(this._selectedAllergies.length > 0 && this._allTemplateItems[i].allergies.length > 0) {
				allergyLoop: for(let j = 0; j < this._selectedAllergies.length; j++) {
					if(this._allTemplateItems[i].allergies.includes(this._selectedAllergies[j])) {
						continue mainLoop;
					}
				}
			}
			if(this._selectedDietRestrictions.length > 0 && this._allTemplateItems[i].dietRestrictions.length > 0) {
				allergyLoop: for(let j = 0; j < this._selectedDietRestrictions.length; j++) {
					if(this._allTemplateItems[i].dietRestrictions.includes(this._selectedDietRestrictions[j])) {
						continue mainLoop;
					}
				}
			}

			let tmpItem			= JSON.parse(JSON.stringify(this._allTemplateItems[i]));
			tmpItem.isSelected	= this._selectedTemplateItems.includes(tmpItem.itemId);
			viewedTemplateItems.push(tmpItem);
		}

		return viewedTemplateItems;

	}

	get allergyCheckboxes() {
		let c = [];
		for(let i = 0; i < this._allAllergies.length; i++) {
			c.push({
				label: this._allAllergies[i].label,
				value: this._allAllergies[i].value,
				checked: this._selectedAllergies.includes(this._allAllergies[i].value),
			});
		}
		return c;
	}

	get dietaryRestrictionsCheckboxes() {
		let c = [];
		for(let i = 0; i < this._allDietRestrictions.length; i++) {
			c.push({
				label: this._allDietRestrictions[i].label,
				value: this._allDietRestrictions[i].value,
				checked: this._allDietRestrictions.includes(this._allDietRestrictions[i].value),
			});
		}
		return c;
	}
	
	/* ---------- SETTERS ---------- */

	set blockId(newId) {
		this._blockId = newId;
	}

	/* ---------- CUSTOM METHODS ---------- */

	queryTemplateItemInformation() {
		GetNutritionalTemplateItemInformation({
		}).then(result => {
			console.log("SUCCESS: ", JSON.parse(JSON.stringify(result)));
			this._allAllergies			= result.allAllergies;
			this._allDietRestrictions	= result.allDietRestrictions;
			this._allTemplateItems		= result.allTemplateItems;
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

	@api saveItems() {
		let savedItems = [];
		for(let i = 0; i < this._allTemplateItems.length; i++) {
			if(this._selectedTemplateItems.includes(this._allTemplateItems[i].itemId)) {
				savedItems.push(JSON.parse(JSON.stringify(this._allTemplateItems[i])))
			}
		}
		return savedItems;
	}

	/* ---------- CUSTOM EVENTS ---------- */

	onItemSelect(event) {
		if(event.target.checked === true) {
			this._selectedTemplateItems.push(event.target.dataset.itemId);
		} else if(event.target.checked === false) {
			for(let i = 0; i < this._selectedTemplateItems.length; i++) {
				if(this._selectedTemplateItems[i] === event.target.dataset.itemId) {
					this._selectedTemplateItems.splice(i,1);
				}
			}
		}
	}

	onAllergySelection(event) {
		let value	= event.target.dataset.value;
		let checked = event.target.checked;
		if(checked === true) {
			this._selectedAllergies.push(value);
		} else {
			for(var i = 0; i < this._selectedAllergies.length; i++){ 
				if (this._selectedAllergies[i] === value) {
					this._selectedAllergies.splice(i, 1); 
					break;
				}
			}
		}
	}

	onDietrySelection(event) {
		let value	= event.target.dataset.value;
		let checked = event.target.checked;
		if(checked === true) {
			this._selectedDietRestrictions.push(value);
		} else {
			for(var i = 0; i < this._selectedDietRestrictions.length; i++){ 
				if (this._selectedDietRestrictions[i] === value) {
					this._selectedDietRestrictions.splice(i, 1); 
					break;
				}
			}
		}
	}

	/**
	 * @description Executes when the Search Name is updated
	 *
	 * @param {Event} event The event that executes this
	 */
	onSearchNameUpdate(event) {
		this._searchName = event.target.value;
	}


	/* ---------- LWC EVENTS ---------- */

	connectedCallback() {
		this.queryTemplateItemInformation();
	} 


}