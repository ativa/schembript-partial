import { LightningElement, track, api } from 'lwc';
import GetNutritionReferencePlans from '@salesforce/apex/AT_LWCC_PlannerAssignToDays.GetNutritionReferencePlans';
import AssignDaysToReferencePlans from '@salesforce/apex/AT_LWCC_PlannerAssignToDays.AssignDaysToReferencePlans';

export default class PlannerXAssignToDays extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The Check-In Id
	 */
	@track _checkInId;

	/**
	 * @description The Plans Data
	 */
	@track _plansData = [];

	/**
	 * @description Are the Plans being loaded
	 */
	@track _isLoading = false;

	/**
	 * @description Is the Assign to days being saved
	 */
	@track _isSaving = false;
	
	/* ---------- GETTERS ---------- */

	/**
	 * @description The Check-In Id
	 */
	@api get checkInId() {
		return this._checkInId;
	}

	/**
	 * @description Are the Plans being loaded in, or are the Plans being saved?
	 */
	get isLoadingOrSaving() {
		return this._isLoading === true || this._isSaving === true;
	}

	/**
	 * @description Can the user submit days
	 */
	get canSubmitDays() {
		//If all days have assigned plan Ids, the days can be submitted
		let selDays = this.getSelectedDays();
		return !(selDays.Monday && selDays.Tuesday && selDays.Wednesday && selDays.Thursday && selDays.Friday && selDays.Saturday && selDays.Sunday);

	}

	/* ---------- SETTERS ---------- */
	
	/**
	 * @description Set the Check-In Id
	 */
	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		this.refresh();
	}

	/* ---------- CUSTOM METHODS ---------- */
	
	/**
	 * @description Refresh the Plans being used
	 */
	@api refresh() {
		this._isLoading = true;
		GetNutritionReferencePlans({
			checkInId: this._checkInId
		}).then(result => {
			this._plansData = this.processPlanData(JSON.parse(JSON.stringify(result)));
			console.log("SUCCESS: ", JSON.parse(JSON.stringify(this._plansData)));
			this._isLoading = false;
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			this._isLoading = false;
		});
	};

	/** 
	 * @description Process the Plan data
	 */
	processPlanData(rawPlanData) {
		let allPlanData = [];
		for(let i = 0; i < rawPlanData.length; i++) { 
			let rawPlan = rawPlanData[i];
			let newPlanData = {
				planId:					rawPlan.Id,
				planName:				rawPlan.Name,
				mondayActive:			rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Monday") : false,
				tuesdayActive:			rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Tuesday") : false,
				wednesdayActive:		rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Wednesday") : false,
				thursdayActive:			rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Thursday") : false,
				fridayActive:			rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Friday") : false,
				saturdayActive:			rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Saturday") : false,
				sundayActive:			rawPlan.Weekdays__c ? rawPlan.Weekdays__c.includes("Sunday") : false,
				planButtonRowClass:		""
			}
			if(i === 0) {
				newPlanData.planButtonRowClass = 'button-row button-row_first';
			} else if(i === rawPlanData.length - 1) {
				newPlanData.planButtonRowClass = 'button-row button-row_last';
			} else {
				newPlanData.planButtonRowClass = 'button-row button-row_middle';
			}
			allPlanData.push(newPlanData);
		}
		return allPlanData;
	}

	/**
	 * @description Get the plan Ids that associate with the selected days
	 */
	getSelectedDays() {

		let selectedDays = {
		};

		for(let i = 0; i < this._plansData.length; i++) {
			let plan = this._plansData[i];

			if(plan.mondayActive === true) { selectedDays.Monday = plan.planId; }
			if(plan.tuesdayActive === true) { selectedDays.Tuesday = plan.planId; }
			if(plan.wednesdayActive === true) { selectedDays.Wednesday = plan.planId; }
			if(plan.thursdayActive === true) { selectedDays.Thursday = plan.planId; }
			if(plan.fridayActive === true) { selectedDays.Friday = plan.planId; }
			if(plan.saturdayActive === true) { selectedDays.Saturday = plan.planId; }
			if(plan.sundayActive === true) { selectedDays.Sunday = plan.planId; }

		}

		return selectedDays;

	}

	/* ---------- CUSTOM EVENTS ---------- */

	/**
	 * @description Executes when the Modal is closed
	 */
	onCloseModal() {
		this.dispatchEvent(new CustomEvent('close'));
	}

	/** 
	 * @description Executes when a day is slelected
	 */
	onSelectDay(event) {
		let dataset = event.target.dataset;

		let planId	= dataset.planId;
		let day		= dataset.day;

		for(let i = 0; i < this._plansData.length; i++) {

			//If this plan is selected, set the day selected to active
			if(this._plansData[i].planId === planId) {
				if(day === "Monday") {
					this._plansData[i].mondayActive		= true;
				} else if(day === "Tuesday") {
					this._plansData[i].tuesdayActive	= true;
				} else if(day === "Wednesday") {
					this._plansData[i].wednesdayActive	= true;
				} else if(day === "Thursday") {
					this._plansData[i].thursdayActive	= true;
				} else if(day === "Friday") {
					this._plansData[i].fridayActive		= true;
				} else if(day === "Saturday") {
					this._plansData[i].saturdayActive	= true;
				} else if(day === "Sunday") {
					this._plansData[i].sundayActive		= true;
				}
			} else {
				//Otherwise set the day to inactive
				if(day === "Monday") {
					this._plansData[i].mondayActive		= false;
				} else if(day === "Tuesday") {
					this._plansData[i].tuesdayActive	= false;
				} else if(day === "Wednesday") {
					this._plansData[i].wednesdayActive	= false;
				} else if(day === "Thursday") {
					this._plansData[i].thursdayActive	= false;
				} else if(day === "Friday") {
					this._plansData[i].fridayActive		= false;
				} else if(day === "Saturday") {
					this._plansData[i].saturdayActive	= false;
				} else if(day === "Sunday") {
					this._plansData[i].sundayActive		= false;
				}
			}
		}

	}

	onSaveAssignDays(event) {
		this._isSaving = true;
		AssignDaysToReferencePlans({
			checkInId:		this._checkInId,
			selectedDays:	this.getSelectedDays()
		}).then(result => {
            this._isSaving = false;
            this.dispatchEvent(new CustomEvent('update'));
		}).catch(error => {
			this._isSaving = false;
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

}