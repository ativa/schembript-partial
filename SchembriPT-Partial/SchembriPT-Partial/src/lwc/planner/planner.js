import { LightningElement, api, track }	from 'lwc';
import { ShowToastEvent }						from 'lightning/platformShowToastEvent';
import { NavigationMixin }						from 'lightning/navigation';

import getCheckInInfo				from '@salesforce/apex/Planner.getCheckInInfo';
import getFilters					from '@salesforce/apex/Planner.getFilters';
import setNutritionToWIP			from '@salesforce/apex/Planner.setNutritionToWIP';

import QueryCheckInPlans			from '@salesforce/apex/PlannerController.QueryCheckInPlans';
import AddTargetAndPoolPlanVersion	from '@salesforce/apex/PlannerController.AddTargetAndPoolPlanVersion';
import UpdateTargetName				from '@salesforce/apex/PlannerController.UpdateTargetName';
import DeletePlan					from '@salesforce/apex/PlannerController.DeletePlan';
import UpdateTargetValues			from '@salesforce/apex/PlannerController.UpdateTargetValues';

export default class Planner extends NavigationMixin(LightningElement) {

	// --------------- VARIABLES ---------------
	
    /**
	 * @description Display the assign to days modal
	 */
	@track displayAssignToDaysModal = false;

	/** 
	 * @description The Id of the Plan to Clone
	 */
	@track _planIdToClone;

	/**
	 * @description Tracks if we want to display the modal that allows the user to clone the plan
	 */
	@track _displayClonePlan = false;

	/**
	 * @description The Id of the Reference Plan that we want to save as a Template Plan
	 */
	@track _referenceIdForTemplatePlan;

	/**
	 * @description Tracks if we are displaying the Save as Template Plan Modal
	 */
	@track _isSavingReferenceAsTemplatePlan = false;

	/**
	 * @description Tracks if we are capable of adding a new template plan
	 */
	@track _cantAddNewTemplatePlan = false;

	/**
	 * @description The planning mode for the Check-In
	 */
	@track _nutritionPlanningMode;

	/**
	 * @description Is the Plan being saved
	 */
	@track _isSavingPlan = false;
	
	// --------------- GETTERS ---------------

	get _isDisplayingDailyPlans() {
		return this.planMode === "Daily Plans";
	}

	get _isDisplayingReferencePlans() {
		return this.planMode === "Reference";
	}

	/**
	 * @description Is the Check-In using the Carb Cycle Planning Mode
	 */
	get isCarbCycleCheckIn() {
		return this._nutritionPlanningMode === "Carb-Cycle";
	}
	
	/**
	 * @description Is the Check-In using the Flex-Plans Planning Mode
	 */
	get isFlexiPlansCheckIn() {
		return this._nutritionPlanningMode === "Flexi Plans";
	}
	
	/**
	 * @description Is the Check-In using the Guided Plans Planning Mode
	 */
	get isGuidedPlansCheckIn() {
		return this._nutritionPlanningMode === "Guided Plans";
	}

	/**
	 * @description The active allergy filters used for the Add Template component
	 */
	get addTemplateActiveAllergyFilters() {
		let activeFilters = [];
		for(let i = 0; i < this.allergies.length; i++) {
			if(this.allergies[i].checked === true) {
				activeFilters.push(this.allergies[i].name);
			}
		}
		return activeFilters;
	}

	/**
	 * @description The active restriction filters used for the Add Template component
	 */
	get addTemplateActiveRestrictionFilters() {
		let activeFilters = [];
		for(let i = 0; i < this.restrictions.length; i++) {
			if(this.restrictions[i].checked === true) {
				activeFilters.push(this.restrictions[i].name);
			}
		}
		return activeFilters;
	}

	// --------------- SETTERS ---------------

	// --------------- CUSTOM METHODS ---------------

	/**
	 * @description Display an error toast message
	 */
	displayErrorToastEvent(error) {
		const event = new ShowToastEvent({
            title:		error.exceptionType,
            message:	error.message,
			variant:	'error',
			mode:		'sticky'
        });
        this.dispatchEvent(event);
	}
	
	/**
	 * @description Tag the Check-In as WIP (Work in Progress)
	 */
	tagAsWIP(onSuccess) {

		setNutritionToWIP({
			checkInId: this._checkInID
		}).then(() => {
			console.log("[planner] Tagged as WIP SUCCESS", this._checkInID);
			if(onSuccess) {
				onSuccess();
			}
		}).catch(error => {
            console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			this.displayErrorToastEvent(error);
        });

	}	

	// --------------- CUSTOM EVENTS ---------------
	
	/**
	 * @description Executes when the save button is pressed for the template plan
	 */
	onSaveTemplatePlanSuccess(event) {
		let savePlanAsTemplate = this.template.querySelector(".save-plan-as-template");
		savePlanAsTemplate.Save(
			() => {
				this._isSavingReferenceAsTemplatePlan = false;
			},
			(error) => {
				this._isSavingReferenceAsTemplatePlan = false;
			}
		);
	}
	
	/**
	 * @description Executes when the Template Plan Modal is closed
	 */
	onCloseSaveTemplatePlanModal(event) {
		this._isSavingReferenceAsTemplatePlan = false;
	}


	/**
	 * @description Executes the we want to stop editing the carb cycle plan
	 *
	 * @param {Event} event The events that executes this
	 */
    onCancelEditingCarbCyclePlan(event) {
        this.editingPlan = false;
    }

	/**
	 * @description Executes when the nutrition planning mode has been updated
	 */
	onNutritionPlanningModeUpdate(event) {
		console.log("Planning Mode Updated...");
		console.log(JSON.parse(JSON.stringify(event.detail)));
		this._nutritionPlanningMode = event.detail.newMode;
	}

	onClonePlan(event) {
		this._planIdToClone		= event.target.name;
		this._displayClonePlan	= true;
	}

	onCloseCloneReferencePlanModal(event) {
		this._displayClonePlan	= false;
	}

	onSavedClonePlan(event) {
		this._displayClonePlan	= false;
		
        this.loading = true;
		//Query the CheckIn Plans Related
		QueryCheckInPlans({
			checkInId: this._checkInID
		}).then(plans => {
			this.processPlans(plans);
            this.loading = false;
		}).catch(error => {
			console.error('[planner] ERROR: ',JSON.stringify(error));
			this.displayErrorToastEvent(error);
		});


	}

	/**
	 * @description Executes when the assign to days component is updated
	 */
	onAssignToDaysUpdate(event) {
        this.displayAssignToDaysModal = false;
	}
	
	/**
	 * @description Executes when the assign to days modal is closed
	 */
	onAssignToDaysClose(event) {
        this.displayAssignToDaysModal = false;
	}

	
	/**
	 * @description Executes when we want to close the Add template Plan modal
	 */
	onAddTemplatePlanModalClose(event) {
		this.addingTemplate = false;
	}

	onNewTemplateItemChange(event) {
		this._cantAddNewTemplatePlan = event.detail.cantCreateNewTemplate;
	}

	onSaveAsTemplatePlan(event) {
		this._referenceIdForTemplatePlan		= event.target.name;
		this._isSavingReferenceAsTemplatePlan	= true;
	}

	onSaveNewTemplatePlan(event) {
		let addTmpPlanModal = this.template.querySelector('.addTemplatePlan');
		addTmpPlanModal.SaveAsReferencePlan(this._checkInID, (result) => {
			console.log("Saved New Plan...", JSON.parse(JSON.stringify(result)));

			//import AddTargetAndPoolPlanVersion	from '@salesforce/apex/PlannerController.AddTargetAndPoolPlanVersion';
			//Add Target and Pool Plan Version for the Plan
			AddTargetAndPoolPlanVersion({
				refPlanId: result.newPlanId, 
				checkInId: this._checkInID
			}).then(result2 => {
				QueryCheckInPlans({
					checkInId: this._checkInID
				}).then(plans => {
					this.processPlans(plans);
					this.dispatchEvent(new ShowToastEvent({
						title:		'Plan Added',
						message:	result.newPlanName + ' has been added to reference plans',
						mode:		'dismissable',
						variant:	'success'
					}));
					this.addingTemplate = false;
					let target = this.template.querySelector('[data-id="reference-client-info"]');
					target.refreshData();

				}).catch(error => {
					console.error('[planner] ERROR: ',JSON.stringify(error));
					this.displayErrorToastEvent(error);
				});
			}).catch(error => {
				console.error('[planner] ERROR: ',JSON.stringify(error));
				this.displayErrorToastEvent(error);
			});

		});
	}

	onSaveAndAddNewTemplatePlan(event) {
		let addTmpPlanModal = this.template.querySelector('.addTemplatePlan');
		addTmpPlanModal.SaveAsReferencePlan(this._checkInID, (result) => {

			AddTargetAndPoolPlanVersion({
				refPlanId: result.newPlanId, 
				checkInId: this._checkInID
			}).then(result2 => {
				QueryCheckInPlans({
					checkInId: this._checkInID
				}).then(plans => {
					this.processPlans(plans);
					this.dispatchEvent(new ShowToastEvent({
						title:		'Plan Added',
						message:	result.newPlanName + ' has been added to reference plans',
						mode:		'dismissable',
						variant:	'success'
					}));
					let target = this.template.querySelector('[data-id="reference-client-info"]');
					target.refreshData();
				}).catch(error => {
					console.error('[planner] ERROR: ',JSON.stringify(error));
					this.displayErrorToastEvent(error);
				});
			}).catch(error => {
				console.error('[planner] ERROR: ',JSON.stringify(error));
				this.displayErrorToastEvent(error);
			});

		});
	}

	/** 
	 * @description Executes when an allergy is selected on the Add Template Modal
	 */
	onAddTemplateAllergiesSelect(event) {
        const allergy	= event.target.name;
        const selected	= event.target.checked;
		for(let i = 0; i < this.allergies.length; i++) {
			if(this.allergies[i].name === allergy) {
				this.allergies[i].checked = selected;
			}
		}
	}
	
	/** 
	 * @description Executes when a restriction is selected on the Add Template Modal
	 */
	onAddTemplateRestrictionsSelect(event) {
        const restriction	= event.target.name;
        const selected		= event.target.checked;
		for(let i = 0; i < this.restrictions.length; i++) {
			if(this.restrictions[i].name === restriction) {
				this.restrictions[i].checked = selected;
			}
		}
	}
	
	// ---------------------------------------------


    @track refreshView = false;

    @track selectedTab;

    @track showReferencePlans = true;

    @track _checkInID;

    @track clientName;
    @track programName;
    @track checkInName;
    @track clientTargets;

    @track editable = false;

    @track totals;

    @track planMode = 'Reference';

    @track disableAssignDays = true;
    @track assignedWeekdays = [];

    @track trackingPlan = false;

    @track selectedDailyPlanID;
    @track selectedDailyPlan;
    @track dailyPlanSelected = false;

    @track templateID;
    @track selectedTemplate;
    @track showSelectedTemplate = false;

    @track addingTemplate = false;

    @track referencePlans;
    @track dailyPlans;

    @track hasPlans = false;
    @track hasDailyPlans = false;
    @track showAddButton = true;

    @track dailyPlanList;

    @track editingPlan = false;
    @track deletingPlan = false;
    @track planID;
    @track selectedPlan;
    @track changedBlocks;

    @track renamingPlan = false;
    @track newName;

    @track allergies;
    @track selectedAllergies = [];
    @track restrictions;
    @track selectedRestrictions = [];

    @track likes = [];
    @track dislikes = [];

    @track clientAllergies = [];
    @track clientRestrictions = [];

    @track otherAllergies = [];
    @track otherRestrictions = [];

    @track hasOtherAllergies = false;
    @track hasOtherRestrictions = false;

    @track loading = true;

    @track assigning = false;

    ///////////////////////////

    constructor() {
        super();

        getFilters().then(filters => {
            if (filters.allergies) {
                this.allergies = filters.allergies.map(a => {return {name: a, checked: false}});
            }
            if (filters.restrictions) {
                this.restrictions = filters.restrictions.map(r => {return {name: r, checked: false};});
            }

        }).catch(error => {
            console.log(JSON.stringify(error));
        });
    }


    //////////////////////////

    @api get checkInID() {
        return this._checkInID;
    }

    set checkInID(newID) {
        this._checkInID = newID;

        console.log('CID:', this._checkInID);

        getCheckInInfo({
			checkInID: newID
		}).then(info => {
            console.log('Info:', info);
			this._nutritionPlanningMode = info.nutritionPlanningMode;
            this.clientName				= info.clientName;
            this.programName			= info.programName;
            this.checkInName			= info.checkInName;
            this.editable				= true;
            this.clientAllergies		= info.allergies;
            this.clientRestrictions		= info.restrictions;

            this.otherAllergies			= info.otherAllergies.join(', ');
            this.otherRestrictions		= info.otherRestrictions.join(', ');

            this.hasOtherAllergies		= info.otherAllergies.length > 0;
            this.hasOtherRestrictions	= info.otherRestrictions.length > 0;

            this.selectedAllergies		= JSON.parse(JSON.stringify(info.allergies));
            this.selectedRestrictions	= JSON.parse(JSON.stringify(info.restrictions));


            this.likes    = (info.likes == undefined || info.likes == null || info.likes.length == 0) ? '(none)' : info.likes.join(', ');
            this.dislikes = (info.dislikes == undefined || info.dislikes == null || info.dislikes.length == 0) ? '(none)' : info.dislikes.join(', ');

            //this.clientTargets = info.targets;

            this.allergies.forEach(a => {
                if (this.clientAllergies.indexOf(a.name) >= 0) a.checked = true;
            });

            this.restrictions.forEach(r => {
                if (this.clientRestrictions.indexOf(r.name) >= 0) r.checked = true;
            });

        }).catch(error => {
            console.log(JSON.stringify(error));
        });

		//Query the CheckIn Plans Related
		QueryCheckInPlans({
			checkInId: this._checkInID
		}).then(plans => {
			this.processPlans(plans);
            this.loading = false;
		}).catch(error => {
			console.error('[planner] ERROR: ',JSON.stringify(error));
			this.displayErrorToastEvent(error);
		});

    }

    get planModes() {
        return [
            {
				label: 'Reference Plans', 
				value: 'Reference'
			},
            {
				label: 'Daily Plans', 
				value: 'Daily Plans'
			}
        ];
    }

    closePlans(event) {
		
		this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            }
        });
    }

    changePlanMode(event) {
        const newMode = event.target.value;
        this.planMode = newMode;
        if (this.selectedDailyPlanID) {
            this.selectedDailyPlan = this.dailyPlans.filter(p => p.id == this.selectedDailyPlanID)[0];
        }
    }

    onOpenAddTemplatePlanModal(event) {

		console.log("ADDING NEW TEMPLATE PLAN");
        this.addingTemplate = true;
		this._cantAddNewTemplatePlan	= true;
    }

	//plannerXAddTemplatePlan

	/**
	 * @description Handle a Save Event for the Add Template Plan Panel
	 */
	handleAddTemplatePlanSaveEvent(event) {

        const saveData = event.detail;

		//If we have a normal save event, then close the new template panel, and requery the plans
		if(saveData.type === "SAVE") {
			
			QueryCheckInPlans({
				checkInId: this._checkInID
			}).then(plans => {
				this.processPlans(plans);
				this.dispatchEvent(new ShowToastEvent({
					title:		'Plan Added',
					message:	saveData.newPlanName + ' has been added to reference plans',
					mode:		'dismissable',
					variant:	'success'
				}));
				this.addingTemplate = false;
				let target = this.template.querySelector('[data-id="reference-client-info"]');
				target.refreshData();

			}).catch(error => {
				console.error('[planner] ERROR: ',JSON.stringify(error));
				this.displayErrorToastEvent(error);
			});

		} else if(saveData.type === "SAVE_AND_ADD") {

			QueryCheckInPlans({
				checkInId: this._checkInID
			}).then(plans => {
				this.processPlans(plans);
				this.dispatchEvent(new ShowToastEvent({
					title:		'Plan Added',
					message:	saveData.newPlanName + ' has been added to reference plans',
					mode:		'dismissable',
					variant:	'success'
				}));
				let target = this.template.querySelector('[data-id="reference-client-info"]');
				target.refreshData();

			}).catch(error => {
				console.error('[planner] ERROR: ',JSON.parse(JSON.stringify(error)));
				this.displayErrorToastEvent(JSON.parse(JSON.stringify(error)));
			});

		}

		

	}

    editPlan(event) {
        this.planID = event.target.name;
        this.selectedPlan = JSON.parse(JSON.stringify(this.referencePlans.filter(p => (p.id == this.planID))[0]));
        if (this.selectedPlan.currentTotals) {
            this.selectedPlan.referenceTotals = JSON.parse(JSON.stringify(this.selectedPlan.currentTotals));
        }
        this.newName = this.selectedPlan.name;
        this.editingPlan = true;
    }

    deletePlan(event) {
        this.planID = event.target.name;
        this.selectedPlan = JSON.parse(JSON.stringify(this.referencePlans.filter(p => (p.id == this.planID))[0]));
        this.deletingPlan = true;
        this.refreshView = true;
    }

    cancelDelete(event) {
        this.deletingPlan = false;
        this.refreshView = false;
    }

    confirmDelete(event) {
        this.loading = true;

		
		this.tagAsWIP(() => {
		
			DeletePlan({
				refPlanId: this.selectedPlan.id
			}).then(result => {

				QueryCheckInPlans({
					checkInId: this._checkInID
				}).then(plans => {
				
					this.showReferencePlans = false;
					this.selectedTab        = null;
					this.processPlans(plans);
					this.deletingPlan       = false;
					this.refreshView        = false;
					let target = this.template.querySelector('[data-id="reference-client-info"]');
					target.refreshData();
					this.dispatchEvent(new ShowToastEvent({
						title:		'Plan Added',
						message:	result.newPlanName + ' has been added to reference plans',
						mode:		'dismissable',
						variant:	'success'
					}));
				}).catch(error => {
					console.error('[planner] ERROR: ',JSON.stringify(error));
					this.displayErrorToastEvent(error);
				});

			}).catch(error => {
				console.log(JSON.stringify(error));

				this.refreshView = false;

				this.loading = false;
			});
		});


    }

    planChanged(event) {
        const planID = event.target.planID;

        this.selectedPlan.blocks = JSON.parse(JSON.stringify(event.target.blocks));

        this.selectedPlan.blocks.forEach(block => {
            block.items.forEach(item => {
                item.newQuantity = null;
                item.changed = null;
                item.changedQuantity = null;
            });
        });
    }

    totalsChanged(event) {
        this.totals = event.detail.current;
    }

    referenceTotalsChanged(event) {
        this.totals = event.detail.reference;
    }

    onSavePlan(event) {

		this._isSavingPlan = true;

		let nutCarbCyclePlan = this.template.querySelector(".nutrition-carb-cycle-plan");

		nutCarbCyclePlan.Save(() => {
			console.log("Saved Successfully");
			
			UpdateTargetName({
				refPlanId: this.planID
			}).then(result => {

				UpdateTargetValues({
					refPlanId: this.planID
				}).then(result2 => {
					this.tagAsWIP(() => {
						this.editingPlan = false;
						
						this._isSavingPlan = false;

						/*
						//Refresh the Client Info, and the Current Plan
						let target = this.template.querySelector('[data-id="reference-client-info"]');
						target.refreshData();
						let currentPlan = this.template.querySelector('[data-plan-id="' + this.planID + '"]');
						currentPlan.RefreshPlan();
						*/

						this.loading = true;
						
						//Query the CheckIn Plans Related
						QueryCheckInPlans({
							checkInId: this._checkInID
						}).then(plans => {
							this.processPlans(plans);
							
							let target = this.template.querySelector('[data-id="reference-client-info"]');
							target.refreshData();
							let currentPlan = this.template.querySelector('[data-plan-id="' + this.planID + '"]');
							currentPlan.RefreshPlan();

							this.loading = false;
						}).catch(error => {
							console.error('[planner] ERROR: ',JSON.stringify(error));
							this.displayErrorToastEvent(error);
						});

					});
				}).catch(error => {
					console.error("Error:", JSON.parse(JSON.stringify(error)));
				});

			}).catch(error => {
				console.error("Error:", JSON.parse(JSON.stringify(error)));
			});


		}, () => {
			console.error("Error");
		});
		
    }

    cancelTracking(event) {
        this.trackingPlan = false;
    }

    saveTracking(event) {
        this.selectedPlan.blocks = this.changedBlocks;
        this.trackingPlan = false;
    }

    assignToWeekdays(event) {
        this.disableAssignDays = true;
        this.assignedWeekdays = [];
        this.displayAssignToDaysModal = true;

        if (this.referencePlans) {
            this.referencePlans.forEach(plan => {
                Object.keys(plan.days).forEach(day => {
                    if (plan.days[day] == 'brand') {
                        if (!this.assignedWeekdays.includes(day)) this.assignedWeekdays.push(day);
                    }
                });
            });
        }

        this.disableAssignDays = (this.assignedWeekdays.length != 7);
    }

    selectWeekday(event) {
        const planID = event.target.name;
        const day    = event.target.label;

        const plan = this.referencePlans.filter(p => (p.id == planID))[0];

        let selected = false;

        if (plan.days[day] == 'neutral') {
            plan.days[day] = 'brand';

            selected = true;

            if (!this.assignedWeekdays.includes(day)) this.assignedWeekdays.push(day);
        } else {
            plan.days[day] = 'neutral';

            this.assignedWeekdays = this.assignedWeekdays.filter(d => d != day);
        }

        this.disableAssignDays = (this.assignedWeekdays.length != 7);

        if (selected) {
            const otherPlans = this.referencePlans.filter(p => (p.id != planID));

            otherPlans.forEach(plan => {
                plan.days[day] = 'neutral';
            });
        }
    }

    selectDailyPlan(event) {
        this.selectedDailyPlanID = event.target.value;
        this.selectedDailyPlan = this.dailyPlans.filter(p => p.id == this.selectedDailyPlanID)[0];
        this.dailyPlanSelected = true;
    }

    ///////////////////////////////

    processPlans(planData) {
        const plans = JSON.parse(planData);


        const today = this.formatDate(new Date());

        if (plans) {
            this.referencePlans = plans.filter(p => (p.type == 'Nutrition_Reference_Plan'));
            this.dailyPlans		= plans.filter(p => (p.type == 'Nutrition_Session_Plan'));
            this.hasPlans		= (this.referencePlans.length) > 0;
            this.hasDailyPlans	= (this.dailyPlans.length > 0);
            this.referencePlans.forEach(plan => {
                this.setPlanDayMap(plan);
            });

            if (this.selectedTab == null && this.referencePlans.length > 0) {
                this.selectedTab = this.referencePlans[0].id;
            }

            this.dailyPlanList = [];

            this.selectedDailyPlanID = null;

            this.dailyPlans.forEach(plan => {
                plan['displayDate'] = this.formatDate(new Date(plan.trackDate));

                if (today == plan.displayDate) {
                    this.selectedDailyPlanID = plan.id;
                    this.selectedDailyPlan = plan;

                    console.log('Today\'s plan', JSON.parse(JSON.stringify(plan.blocks)));
                }

                this.dailyPlanList.push({label: plan.displayDate, value: plan.id});
            });

            if (!this.selectedDailyPlanID && this.dailyPlans.length > 0) {
                this.selectedDailyPlanID = this.dailyPlans[0].id;
                this.selectedDailyPlan = this.dailyPlans[0];

                this.dailyPlanSelected = true;
            }

            if (this.referencePlans.length > 0) {
                console.log('Showing reference plans');
                this.showReferencePlans = true;
            }
        }

        this.loading = false;
    }

    setPlanDayMap(plan) {
            plan.days = {
                Mon: this.weekdaySelected('Monday', plan.weekdays),
                Tue: this.weekdaySelected('Tuesday', plan.weekdays),
                Wed: this.weekdaySelected('Wednesday', plan.weekdays),
                Thu: this.weekdaySelected('Thursday', plan.weekdays),
                Fri: this.weekdaySelected('Friday', plan.weekdays),
                Sat: this.weekdaySelected('Saturday', plan.weekdays),
                Sun: this.weekdaySelected('Sunday', plan.weekdays)
            }
    }

    formatDate(date) {
        var year     = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
        var month    = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date);
        var day      = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
        var weekday  = new Intl.DateTimeFormat('en', { weekday: 'long' }).format(date);

        return weekday + ' ' + day + '/' + month + '/' + year;
    }

    weekdaySelected(day, weekdays) {
        if (weekdays) {
            return (weekdays.indexOf(day) >= 0) ? 'brand' : 'neutral';
        } else {
            return 'neutral';
        }
    }

}