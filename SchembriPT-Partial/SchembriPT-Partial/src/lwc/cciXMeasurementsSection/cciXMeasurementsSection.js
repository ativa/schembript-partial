import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';

/**
 * Used with the 'CommunityCheckIn' Lightning Component
 */
export default class CciXMeasurementsSection extends LightningElement {
	
	/* --------------- VARIABLES --------------- */

	@track today;

	@api checkInId;

	onSuccessAction;
	
	/* --------------- GETTERS --------------- */
	
	get isMobile() {
		return FORM_FACTOR === "Small";
	}



	@api get areValuesUpdated() {
		
		//Mobile
		if(FORM_FACTOR === "Small") {
			let weightField = this.template.querySelector('.weight-field__mobile');
			let chestField	= this.template.querySelector('.chest-field__mobile');
			let waistField	= this.template.querySelector('.waist-field__mobile');
			let hipField	= this.template.querySelector('.hip-field__mobile');
			let thighField	= this.template.querySelector('.thigh-field__mobile');

			return true;

		//Desktop
		} else {

			let weightField = this.template.querySelector('.weight-field__desktop');
			let chestField	= this.template.querySelector('.chest-field__desktop');
			let waistField	= this.template.querySelector('.waist-field__desktop');
			let hipField	= this.template.querySelector('.hip-field__desktop');
			let thighField	= this.template.querySelector('.thigh-field__desktop');

		}

		return true;

	}

	/* --------------- SETTERS --------------- */

	
	/* --------------- OTHER --------------- */

	@api allValuesSet() {
	
		let fields = [];

		//Mobile
		if(FORM_FACTOR === "Small") {
			fields = [
				this.template.querySelector('.weight-field__mobile'),
				this.template.querySelector('.chest-field__mobile'),
				this.template.querySelector('.waist-field__mobile'),
				this.template.querySelector('.hip-field__mobile'),
				this.template.querySelector('.thigh-field__mobile'),
			];

		//Desktop
		} else {
			fields = [
				this.template.querySelector('.weight-field__desktop'),
				this.template.querySelector('.chest-field__desktop'),
				this.template.querySelector('.waist-field__desktop'),
				this.template.querySelector('.hip-field__desktop'),
				this.template.querySelector('.thigh-field__desktop'),
			];

		}
		
		let returnValue = {
			valuesSet:		true,
			notSetFields:	[]
		};
		
		for(let i = 0; i < fields.length; i++) {
		
			if(fields[i].value === null || fields[i].value === '') {
				returnValue.valuesSet = false;
				returnValue.notSetFields.push(fields[i].outerText);
			}
			
		}
		
		return returnValue;

	}

	@api Submit(onSuccessAction) {
	
		if(onSuccessAction) {
			this.onSuccessAction = onSuccessAction;
		}

		console.log("Submitting Measurements Section");
		const element = this.template.querySelector('[data-id="measurements"]');
		element.submit();


	}

	connectedCallback(){
		this.today = new Date();
	}  

	onSuccess(event) {
		console.log("Succesfully Saved Measurements");

		if(this.onSuccessAction) {
			let values = this.allValuesSet();
			this.onSuccessAction(values);
		}

	}

	onError(event) {
		console.error("Error in Measurements");
	}

	get isMobile() {
		return FORM_FACTOR === 'Small';
	}

}