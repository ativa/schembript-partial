import { LightningElement, api, track } from 'lwc';

export default class AtCciMeasurementsSection extends LightningElement {

	@track today;

	@api checkInId;

	connectedCallback(){
		this.today = new Date();
	}  

	onSuccess(event) {
		console.log("SUCCESS: ")
	}

	onError(event) {
		console.error("ERROR: ")
	}

}