import { LightningElement, api, track } from 'lwc';

export default class AtSldsCombobox extends LightningElement {

	/* ----- VARIABLES ----- */

	@api label = "???";
	
	@api value = "G2 Value 1";

	@api variant = "label-hidden";
	
	@track _isOpen = false;

	@track _onMouseOverDropdown = false;

	@track _isFocused = false;

	@track _options = [
		{
			label: "Group 1",
			value: "Group 1",
			options: [
				{ label: "G1 Value 1", value: "G1 Value 1" },
				{ label: "G1 Value 2", value: "G1 Value 2" },
				{ label: "G1 Value 3", value: "G1 Value 3" },
			]
		},
		{
			label: "Group 2",
			value: "Group 2",
			options: [
				{ label: "G2 Value 1", value: "G2 Value 1" },
				{ label: "G2 Value 2", value: "G2 Value 2" },
				{ label: "G2 Value 3", value: "G2 Value 3" },
			]
		}
	];

	/* ----- GETTERS ----- */

	get valueLabel() {

		if(typeof(this.value) !== "undefined") {
			for(let i = 0; i < this._options.length; i++) {
				for(let j = 0; j < this._options[i].options.length; j++) {
					if(this._options[i].options[j].value === this.value) {
						return this._options[i].options[j].label;
					}
				}
			}
		}

		return "Select an Option…";

	}

	get _inputClass() {

		let inputCls = "slds-input slds-combobox__input";

		if(this._isFocused === true) {
			inputCls += " slds-has-focus";
		}

		return inputCls;
	}


	@api get options() {
		return this._options;
	}

	get _comboboxClass() {
		let classStr = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
		if(this._isOpen === true) {
			classStr += " slds-is-open";
		}
		return classStr;
	}

	get displayedOptions() {

		let updatedOptions = JSON.parse(JSON.stringify(this._options));

		for(let i = 0; i < updatedOptions.length; i++) {
			for(let j = 0; j < updatedOptions[i].options.length; j++) {
				updatedOptions[i].options[j].states = {
					isSelected: this.value === updatedOptions[i].options[j].value
				};
			}
		}

		return updatedOptions;

	}


	/* ----- SETTERS ----- */

	set options(newOptions) {
		this._options = newOptions;
	}

	/* ----- CUSTOM METHODS ----- */

	fireChangeEvent() {
        this.dispatchEvent(new CustomEvent('change'));
	}

	/* ----- CUSTOM EVENTS ----- */

	onComboboxFocus(event) {
		console.log("Combobox Focus");
		this._isFocused = true;
		this._isOpen = true;
	}

	onComboboxBlur(event) {
		console.log("Combobox Blur");
		this._isFocused = false;
		if(this._onMouseOverDropdown === false) {
			this._isOpen = false;
		}
	}

	onSelectOption(event) {
		console.log("Selected Option");
		this.value = event.target.dataset.value;
		this.fireChangeEvent();
		this._isFocused = false;
		this._isOpen = false;
	}

	onMouseOverDropdown(event) {
		this._onMouseOverDropdown = true;
	}

	onMouseOutDropdown(event) {
		this._onMouseOverDropdown = false;
	}
	
	/* ----- LWC EVENTS ----- */

}