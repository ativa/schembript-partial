import { LightningElement, api, track, wire } from 'lwc';
import FlowNavRequestChannel from '@salesforce/messageChannel/FlowNavRequestChannel__c';
import FlowNavResponseChannel from '@salesforce/messageChannel/FlowNavResponseChannel__c';
import { subscribe, publish, MessageContext } from 'lightning/messageService';

/**
 * @description A Flow Component that Displays a field set
 */
export default class AfcFieldSet extends LightningElement {

	subscription = null;

    @wire(MessageContext)
	messageContext;
	
	/**
	 * @description The Id of the record that is using the field set
	 */
	@api recordId;

	/**
	 * @description The name of the object we are querying
	 */
	@api objectApiName; 

	/**
	 * @description The Name of the Fieldset which contains the fields we want to display
	 */
	@api fieldSetName;

	/**
	 * @description The title to display at the top
	 */
	@api title;

	/**
	 * @description If true display the title
	 */
	@api showTitle = false;

	/**
	 * @description The number of columns to use
	 */
	@api columns = 1;
	
	/**
	 * @description Is the Field Set Editable
	 */
	@api editable = false;

	/**
	 * @description Display the Save Button for the Form
	 */
	@api displaySaveButton = false;

	/**
	 * @description A Unique key that identitifes the the channel to use for this field set
	 */
	@api channelKey = '';

	/**
	 * @description The Position of the Label
	 */
	@api labelPosition = '';

	connectedCallback() {

		if (this.subscription) {
            return;
		}
		
        this.subscription = subscribe(this.messageContext, FlowNavRequestChannel, (message) => {

			//If the channel key is the same, receive the submission
			if(message.channelKey === this.channelKey) {
			
				console.log("Received Submission Response");

				let fieldset = this.template.querySelector('.afc-field-set');
				
				let messagePacket = {
					channelKey: this.channelKey
				};

				if(this.editable === true) {
					fieldset.Submit((success) => {
						//Publish Changes
						publish(this.messageContext, FlowNavResponseChannel, messagePacket);
					}, (error) => {
						console.error("Error Occured", JSON.parse(JSON.stringify(error)));
					});
				} else {
					//Publish Changes
					publish(this.messageContext, FlowNavResponseChannel, messagePacket);
				}


			}


        });
	}


}