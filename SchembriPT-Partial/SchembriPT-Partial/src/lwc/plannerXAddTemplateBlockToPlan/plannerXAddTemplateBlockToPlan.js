import { LightningElement, api, track } from 'lwc';
import GetNutrionalTemplateBlocks from '@salesforce/apex/AT_LWCC_PlannerAddTemplateBlockToPlan.GetNutrionalTemplateBlocks';

export default class PlannerXAddTemplateBlockToPlan extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The Id of the Plan that we want to add the block too
	 */
	@track _planId;

	/**
	 * @description All of the Template Blocks that we can add to the plan
	 */
	@track _allTemplateBlocks = [];

	/**
	 * @description The currently selected template block
	 */
	@track _selectedTemplateBlock;

	/**
	 * @description The name of the block when created
	 */
	@track _blockName = "";

	/**
	 * @description The current block name filter
	 */
	@track _blockNameFilter = "";

	/**
	 * @description The current block calories filter
	 */
	@track _blockCaloriesFilter;

	/**
	 * @description The current item name filter
	 */
	@track _blockItemNameFilter;
	
	/* ---------- GETTERS ---------- */

	/**
	 * @description Returns the selected template block
	 */
	get selectedTemplateBlock() {
		return this._selectedTemplateBlock;
	}

	/**
	 * @description The Plan Id
	 */
	@api get planId() {
		return this._planId;
	}

	/**
	 * @description Has a block been selected
	 */
	get hasSelectedBlock() {
		return typeof(this._selectedTemplateBlock) !== "undefined";
	}

	/**
	 * @description The list of template blocks that can be selected
	 */
	get viewableTemplateBlocks() {
		let v = [];
		let calorieMinRange = 0;
		let calorieMaxRange = 1000000;
		if(this._blockCaloriesFilter) {
			calorieMinRange = this._blockCaloriesFilter * 0.95;
			calorieMaxRange = this._blockCaloriesFilter * 1.05;
		}
		for(let i = 0; i < this._allTemplateBlocks.length; i++) {

			//If the block was not in the calorie range then ignore it
			if((calorieMinRange <= this._allTemplateBlocks[i].templateCalories === false) || (this._allTemplateBlocks[i].templateCalories <= calorieMaxRange === false)) {
				continue;
			}
			//if the item is not a part of the block group then ignore it
			if(this._blockItemNameFilter) {
				let includesItem = false;
				templateItemLoop: for(let j = 0; j < this._allTemplateBlocks[i].templateItems.length; j++) {
					if(this._allTemplateBlocks[i].templateItems[j].templateName.toLowerCase().includes(this._blockItemNameFilter.toLowerCase())) {
						includesItem = true;
						break templateItemLoop;
					}
				}
				if(includesItem === false) {
					continue;
				}
			}

			if(this._blockNameFilter && this._allTemplateBlocks[i].templateName.toLowerCase().includes(this._blockNameFilter.toLowerCase()) === false) {
				continue;
			}

			let tmpBlock = JSON.parse(JSON.stringify(this._allTemplateBlocks[i]));

			if(this._selectedTemplateBlock) {
				tmpBlock.selected = tmpBlock.templateId === this._selectedTemplateBlock.templateId;
			} else {
				tmpBlock.selected = false;
			}

			v.push(tmpBlock);

		}
		return v;
	}

	/* ---------- SETTERS ---------- */

	/**
	 * @description Set the plan Id
	 */
	set planId(newPlanId) {
		console.log("Plan Id: ", newPlanId);
		this._planId = newPlanId;
		this.getAllTemplateBlocks();
	}

	/* ---------- CUSTOM METHODS ---------- */
	
	/**
	 * @description Returns the currently selected block, and if a valid name has been given
	 */
	@api getSelectedBlock() {
		let packet = {
			hasValidName:			this._blockName != "",
			hasSelectedBlock:		typeof(this._selectedTemplateBlock) != "undefined",
		}
		if(packet.hasSelectedBlock === true) {
			let tmpBlock			= JSON.parse(JSON.stringify(this._selectedTemplateBlock));
			tmpBlock.templateName	= this._blockName;
			packet.selectedTemplateBlock = tmpBlock;
		}
		return packet;
	}

	/**
	 * @description Queries all of the Template Nutritional Blocks in the database 
	 */
	getAllTemplateBlocks() {
		GetNutrionalTemplateBlocks({
			planId: this._planId
		}).then(result => {
			this._allTemplateBlocks = JSON.parse(JSON.stringify(result));
			console.log("Returned Template Blocks: ", JSON.parse(JSON.stringify(this._allTemplateBlocks)));
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Finds the Template Block within the list by the template id
	 *
	 * @param {String} templateId The Id of the template block
	 */
	findTemplateBlock(templateId) {
		for(let i = 0; i < this._allTemplateBlocks.length; i++) {
			if(this._allTemplateBlocks[i].templateId === templateId) {
				this._selectedTemplateBlock = JSON.parse(JSON.stringify(this._allTemplateBlocks[i]));
				break;
			}
		}
	}

	/* ---------- CUSTOM EVENTS ---------- */

	/**
	 * @description Executes when the block name filter has been updated
	 */
	onBlockNameFilterUpdate(event) {
		this._blockNameFilter = event.target.value;
	}

	/**
	 * @description Executes when the block calories filter has been updated
	 */
	onBlockCaloriesFilterUpdate(event) {
		this._blockCaloriesFilter = event.target.value;
	}

	/**
	 * @description Executes when the block item name filter has been updated
	 */
	onBlockItemNameFilterUpdate(event) {
		this._blockItemNameFilter = event.target.value;
	}

	/**
	 * @description Executes when the block name is updated
	 */
	onUpdateBlockName(event) {
		this._blockName = event.target.value;
	}

	/**
	 * @description Executes when the block is selected
	 */
	onBlockSelect(event) {
		let templateId = event.target.value;
		if(templateId != "") {
			this.findTemplateBlock(templateId);
		} else {
			this._selectedTemplateBlock = undefined;
		}
	}

	/* ---------- LWC EVENTS ---------- */

}