import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR						from '@salesforce/client/formFactor';

import GetAssignPlanInformation from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.GetAssignPlanInformation';
import GetPlanOptions			from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.GetPlanOptions';
import GetPlanOptions2			from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.GetPlanOptions2';
import SaveFlexiPlanTargets     from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.SaveFlexiPlanTargets';
import SaveGuidedPlanTargets    from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.SaveGuidedPlanTargets';
import SaveCarbCycleTargets		from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.SaveCarbCycleTargets';
import GetTargetAssignToDays	from '@salesforce/apex/AT_LWCC_TrackerAssignPlansToTargets.GetTargetAssignToDays';

export default class TrackerXAssignPlansToTargets extends LightningElement {

	/* ---------- VARIABLES ---------- */
	
	/**
	 * @description All Targets
	 */
	@track _targets = [];

	@track _nutritionTargets = {};

    /**
     * @description A list of all the targets that have been updated by indexes
     */
    @track _updatedTargetIndexes = [];

	@track _clientPlanOptions = [];

	@track _targetPlanOptions;

	@track _targetAssign;

	@track _checkInInformation;

	@track _fromValue;

	@track _toValue;

    /**
     * @description The indexes of the weeks opened
     */
    @track _openWeeks = [0]

    /**
     * @description Is the data being loaded
     */
    @track _isLoading = false;

    /**
     * @description Is the data being saved
     */
    @track _isSaving = false;

	@track _displayAssignToDaysModal = false;

	@track _selectedWeekGroup;

	@track _assignedPlanDayWeeks = {};

	@track _assignedApplyToAllWeeks = false;
	
	/* ---------- GETTERS ---------- */

	get disableAssignButton() {
		if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans") {
			return	typeof(this._assignedPlanDayWeeks["Monday"]) === "undefined" || 
					typeof(this._assignedPlanDayWeeks["Tuesday"]) === "undefined" || 
					typeof(this._assignedPlanDayWeeks["Wednesday"]) === "undefined" || 
					typeof(this._assignedPlanDayWeeks["Thursday"]) === "undefined" || 
					typeof(this._assignedPlanDayWeeks["Friday"]) === "undefined" || 
					typeof(this._assignedPlanDayWeeks["Saturday"]) === "undefined" || 
					typeof(this._assignedPlanDayWeeks["Sunday"]) === "undefined";
		} else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
			let weekCount = this._targets.length;

			let isDisabled = false;

			for(let i = 1; i <= weekCount; i++) {
				if(typeof(this._assignedPlanDayWeeks["Week " + i]) === "undefined") {
					isDisabled = true;
					break;
				}
			}

			return isDisabled;

		}

	}

	get assignToDaysHeaderOptions() {
		let options = [];
		if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans" || this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {
			options = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];
		} else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
			for(let i = 1; i <= this._targets.length; i++) { options.push("W" + i); }
		}
		return options;
	}

	get assignToDaysOptions_GuidedPlans() {
	
		let targetOptions = [];
  
		for(const [targetId, poolPlans] of Object.entries(this._targetPlanOptions)) {
		
            let selectedTarget;

            for(let i = 0; i < this._targets.length; i++) {
                if(this._targets[i].targetId === targetId) {
                    selectedTarget = this._targets[i];
                }
            }


			let targetOption = {
				name:			selectedTarget.targetName,
				id:				selectedTarget.targetId,
				poolPlans:		[],
				assignToDays:	[]
			}

			let assignToDaysWeeks = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
			
			let spareDaysLabels = 0;
			for(let i = 0; i < 7; i++) {
				let newAssignToOption = {};
				if(i < assignToDaysWeeks.length) {
					newAssignToOption = {
						label:		assignToDaysWeeks[i],
						value:		assignToDaysWeeks[i],
						isBlank:	false
					};
				} else {
					newAssignToOption = {
						label:		"",
						value:		"Blank " + spareDaysLabels,
						isBlank:	true,
					};
					spareDaysLabels += 1;
				}
				newAssignToOption.label = newAssignToOption.label.replace("Monday","Mon");
				newAssignToOption.label = newAssignToOption.label.replace("Tuesday","Tue");
				newAssignToOption.label = newAssignToOption.label.replace("Wednesday","Wed");
				newAssignToOption.label = newAssignToOption.label.replace("Thursday","Thu");
				newAssignToOption.label = newAssignToOption.label.replace("Friday","Fri");
				newAssignToOption.label = newAssignToOption.label.replace("Saturday","Sat");
				newAssignToOption.label = newAssignToOption.label.replace("Sunday","Sun");

				targetOption.assignToDays.push(newAssignToOption);

			}

			//Add the pool Plans
			for(let i = 0; i < poolPlans.length; i++) {
				let newPoolPlanOption = {
					planName:		poolPlans[i].label,
					planId:			poolPlans[i].relatedPlanId,
					poolPlanId:		poolPlans[i].poolPlanId,
					assignToDays:	[]
				};

				//Add Assign to Days Labels for the plan rows
				let spareDays = 0;
				let assignToOptions = [];
				for(let i = 0; i < 7; i++) {
					let newAssignToOption = {};

					if(i < assignToDaysWeeks.length) {
						newAssignToOption = {
							label:		assignToDaysWeeks[i],
							value:		assignToDaysWeeks[i],
							isBlank:	false,
							variant:	(this._assignedPlanDayWeeks[assignToDaysWeeks[i]] && this._assignedPlanDayWeeks[assignToDaysWeeks[i]] === newPoolPlanOption.poolPlanId) ? "brand" : "neutral" 
						};

						newAssignToOption.buttonClass = "slds-button slds-button_stretch";
						if(this._assignedPlanDayWeeks[assignToDaysWeeks[i]] && this._assignedPlanDayWeeks[assignToDaysWeeks[i]] === newPoolPlanOption.poolPlanId) {
							newAssignToOption.buttonClass += " slds-button_brand";
						} else {
							newAssignToOption.buttonClass += " slds-button_neutral";
						}

						if(i === 0) {
							newAssignToOption.buttonClass += " assign-to__first-button";
						}
						if(i !== 0 && i !== assignToDaysWeeks.length - 1) {
							newAssignToOption.buttonClass += " assign-to__middle-button";
						}
						if(i === assignToDaysWeeks.length - 1) {
							newAssignToOption.buttonClass += " assign-to__last-button";
						}

					} else {
						newAssignToOption = {
							label:		"",
							value:		"Blank " + spareDays,
							isBlank:	true,
						};
						spareDays += 1;
					}
					newAssignToOption.label = newAssignToOption.label.replace("Monday","Mon");
					newAssignToOption.label = newAssignToOption.label.replace("Tuesday","Tue");
					newAssignToOption.label = newAssignToOption.label.replace("Wednesday","Wed");
					newAssignToOption.label = newAssignToOption.label.replace("Thursday","Thu");
					newAssignToOption.label = newAssignToOption.label.replace("Friday","Fri");
					newAssignToOption.label = newAssignToOption.label.replace("Saturday","Sat");
					newAssignToOption.label = newAssignToOption.label.replace("Sunday","Sun");
					assignToOptions.push(newAssignToOption);
				}

				newPoolPlanOption.assignToDays = assignToOptions;

				targetOption.poolPlans.push(newPoolPlanOption);
			}


			console.log("Pool Plans: ",		JSON.parse(JSON.stringify(poolPlans)));
			console.log("Assign To: ",		JSON.parse(JSON.stringify(assignToDaysWeeks)));

			targetOptions.push(targetOption);

		}

		return targetOptions;

	}

	get assignToDaysOptions_FlexiPlans() {
	
		let targetOptions = [];
  
		console.log("Target Plan Options3: ", JSON.parse(JSON.stringify(this._targetPlanOptions)));

		for(const [targetId, poolPlans] of Object.entries(this._targetPlanOptions)) {
		
            let selectedTarget;

            for(let i = 0; i < this._targets.length; i++) {
                if(this._targets[i].targetId === targetId) {
                    selectedTarget = this._targets[i];
                }
            }


			let targetOption = {
				name:			selectedTarget.targetName,
				id:				selectedTarget.targetId,
				poolPlans:		[],
				assignToDays:	[]
			}

			let assignToDaysWeeks;
			
			if(this._checkInInformation.flexiPlansLocked === true) {
				assignToDaysWeeks = this._targetAssign[targetId];
			} else {
				assignToDaysWeeks = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
			}

			let spareDaysLabels = 0;
			for(let i = 0; i < 7; i++) {
				let newAssignToOption = {};
				if(i < assignToDaysWeeks.length) {
					newAssignToOption = {
						label:		assignToDaysWeeks[i],
						value:		assignToDaysWeeks[i],
						isBlank:	false
					};
				} else {
					newAssignToOption = {
						label:		"",
						value:		"Blank " + spareDaysLabels,
						isBlank:	true,
					};
					spareDaysLabels += 1;
				}
				newAssignToOption.label = newAssignToOption.label.replace("Monday","Mon");
				newAssignToOption.label = newAssignToOption.label.replace("Tuesday","Tue");
				newAssignToOption.label = newAssignToOption.label.replace("Wednesday","Wed");
				newAssignToOption.label = newAssignToOption.label.replace("Thursday","Thu");
				newAssignToOption.label = newAssignToOption.label.replace("Friday","Fri");
				newAssignToOption.label = newAssignToOption.label.replace("Saturday","Sat");
				newAssignToOption.label = newAssignToOption.label.replace("Sunday","Sun");

				targetOption.assignToDays.push(newAssignToOption);

			}



			//Add the pool Plans
			for(let i = 0; i < poolPlans.length; i++) {
				let newPoolPlanOption = {
					planName:		poolPlans[i].label,
					planId:			poolPlans[i].relatedPlanId,
					poolPlanId:		poolPlans[i].poolPlanId,
					assignToDays:	[]
				};

				//Add Assign to Days Labels for the plan rows
				let spareDays = 0;
				let assignToOptions = [];
				for(let i = 0; i < 7; i++) {
					let newAssignToOption = {};

					if(i < assignToDaysWeeks.length) {
						newAssignToOption = {
							label:		assignToDaysWeeks[i],
							value:		assignToDaysWeeks[i],
							isBlank:	false,
							variant:	(this._assignedPlanDayWeeks[assignToDaysWeeks[i]] && this._assignedPlanDayWeeks[assignToDaysWeeks[i]] === newPoolPlanOption.poolPlanId) ? "brand" : "neutral" 
						};

						newAssignToOption.buttonClass = "slds-button slds-button_stretch";
						if(this._assignedPlanDayWeeks[assignToDaysWeeks[i]] && this._assignedPlanDayWeeks[assignToDaysWeeks[i]] === newPoolPlanOption.poolPlanId) {
							newAssignToOption.buttonClass += " slds-button_brand";
						} else {
							newAssignToOption.buttonClass += " slds-button_neutral";
						}

						if(i === 0) {
							newAssignToOption.buttonClass += " assign-to__first-button";
						}
						if(i !== 0 && i !== assignToDaysWeeks.length - 1) {
							newAssignToOption.buttonClass += " assign-to__middle-button";
						}
						if(i === assignToDaysWeeks.length - 1) {
							newAssignToOption.buttonClass += " assign-to__last-button";
						}

					} else {
						newAssignToOption = {
							label:		"",
							value:		"Blank " + spareDays,
							isBlank:	true,
						};
						spareDays += 1;
					}
					newAssignToOption.label = newAssignToOption.label.replace("Monday","Mon");
					newAssignToOption.label = newAssignToOption.label.replace("Tuesday","Tue");
					newAssignToOption.label = newAssignToOption.label.replace("Wednesday","Wed");
					newAssignToOption.label = newAssignToOption.label.replace("Thursday","Thu");
					newAssignToOption.label = newAssignToOption.label.replace("Friday","Fri");
					newAssignToOption.label = newAssignToOption.label.replace("Saturday","Sat");
					newAssignToOption.label = newAssignToOption.label.replace("Sunday","Sun");
					assignToOptions.push(newAssignToOption);
				}

				newPoolPlanOption.assignToDays = assignToOptions;

				targetOption.poolPlans.push(newPoolPlanOption);
			}


			console.log("Pool Plans: ",		JSON.parse(JSON.stringify(poolPlans)));
			console.log("Assign To: ",		JSON.parse(JSON.stringify(assignToDaysWeeks)));

			targetOptions.push(targetOption);

		}

		return targetOptions;

	}
	
	get assignToDaysOptions_CarbCyclePlans() {
	
		let targetOptions = [];
  
		console.log("Target Plan Options3: ", JSON.parse(JSON.stringify(this._targetPlanOptions)));

		for(const [targetId, poolPlans] of Object.entries(this._targetPlanOptions)) {
		
            let selectedTarget;

            for(let i = 0; i < this._targets.length; i++) {
                if(this._targets[i].targetId === targetId) {
                    selectedTarget = this._targets[i];
                }
            }


			let targetOption = {
				name:			selectedTarget.targetName,
				id:				selectedTarget.targetId,
				poolPlans:		[],
				assignToDays:	[
					{ label: "Mon", value: "Monday", isBlank: false },
					{ label: "Tue", value: "Tuesday", isBlank: false },
					{ label: "Wed", value: "Wednesday", isBlank: false },
					{ label: "Thu", value: "Thursday", isBlank: false },
					{ label: "Fri", value: "Friday", isBlank: false },
					{ label: "Sat", value: "Saturday", isBlank: false },
					{ label: "Sun", value: "Sunday", isBlank: false },
				]
			}

			let assignToDaysWeeks = this._targetAssign[targetId];

			let daysInWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

			//Add the pool Plans
			for(let i = 0; i < poolPlans.length; i++) {
				let newPoolPlanOption = {
					planName:		poolPlans[i].label,
					planId:			poolPlans[i].relatedPlanId,
					poolPlanId:		poolPlans[i].poolPlanId,
					assignToDays:	[]
				};

				//Add Assign to Days Labels for the plan rows
				let spareDays = 0;
				let assignToOptions = [];

				let assignToDaysIndex = 0;

				for(let i = 0; i < daysInWeek.length; i++) {
				
					let newAssignToOption = {};

					if(daysInWeek[i] === assignToDaysWeeks[assignToDaysIndex]) {
						newAssignToOption = {
							label:			assignToDaysWeeks[assignToDaysIndex],
							value:			assignToDaysWeeks[assignToDaysIndex],
							isBlank:		false,
							variant:		(this._assignedPlanDayWeeks[assignToDaysWeeks[assignToDaysIndex]] && this._assignedPlanDayWeeks[assignToDaysWeeks[assignToDaysIndex]] === newPoolPlanOption.poolPlanId) ? "brand" : "neutral" 
						};
						
						newAssignToOption.buttonClass = "slds-button slds-button_stretch";
						if(this._assignedPlanDayWeeks[assignToDaysWeeks[assignToDaysIndex]] && this._assignedPlanDayWeeks[assignToDaysWeeks[assignToDaysIndex]] === newPoolPlanOption.poolPlanId) {
							newAssignToOption.buttonClass += " slds-button_brand";
						} else {
							newAssignToOption.buttonClass += " slds-button_neutral";
						}

						/*
						
						if(i === 0) {
							newAssignToOption.buttonClass += " assign-to__first-button";
						}
						if(i !== 0 && i !== assignToDaysWeeks.length - 1) {
							newAssignToOption.buttonClass += " assign-to__middle-button";
						}
						if(i === assignToDaysWeeks.length - 1) {
							newAssignToOption.buttonClass += " assign-to__last-button";
						}
						*/

						assignToDaysIndex += 1;
					} else {
						newAssignToOption = {
							label:		"",
							value:		"Blank " + spareDays,
							isBlank:	true,
						};
						spareDays += 1;
					}

					newAssignToOption.label = newAssignToOption.label.replace("Monday","Mon");
					newAssignToOption.label = newAssignToOption.label.replace("Tuesday","Tue");
					newAssignToOption.label = newAssignToOption.label.replace("Wednesday","Wed");
					newAssignToOption.label = newAssignToOption.label.replace("Thursday","Thu");
					newAssignToOption.label = newAssignToOption.label.replace("Friday","Fri");
					newAssignToOption.label = newAssignToOption.label.replace("Saturday","Sat");
					newAssignToOption.label = newAssignToOption.label.replace("Sunday","Sun");
					assignToOptions.push(newAssignToOption);

				}

				//Loop through the options again to get the button groups
				for(let i = 0; i < daysInWeek.length; i++) {
					if(assignToOptions[i].isBlank === false) {
						if(i === 0) {
							if(assignToOptions[i+1].isBlank === true) {
								assignToOptions[i].buttonClass += " assign-to__single-button";
							} else {
								assignToOptions[i].buttonClass += " assign-to__first-button";
							}
						} else if(i !== 0 && i !== daysInWeek.length - 1) {
							let prevAssign = assignToOptions[i - 1];
							let nextAssign = assignToOptions[i + 1];

							//Start
							if(prevAssign.isBlank === true && nextAssign.isBlank === false) {
								assignToOptions[i].buttonClass += " assign-to__first-button";
							//Middle
							} else if(prevAssign.isBlank === false && nextAssign.isBlank === false) {
								assignToOptions[i].buttonClass += " assign-to__middle-button";
							//Last
							} else if(prevAssign.isBlank === false && nextAssign.isBlank === true) {
								assignToOptions[i].buttonClass += " assign-to__last-button";
							}

						} else if(i === daysInWeek.length - 1) {
							if(assignToOptions[i-1].isBlank === true) {
								assignToOptions[i].buttonClass += " assign-to__single-button";
							} else {
								assignToOptions[i].buttonClass += " assign-to__last-button";
							}
						}
					}
				}

				newPoolPlanOption.assignToDays = assignToOptions;

				targetOption.poolPlans.push(newPoolPlanOption);
			}

			targetOptions.push(targetOption);

		}

		return targetOptions;

	}

	get assignToDaysOptions() {

		let options = [];
  
		console.log("Target Plan Options: ", this._targetPlanOptions);

		for(const [targetId, poolPlans] of Object.entries(this._targetPlanOptions)) {

			let assignToDaysWeeks = this._targetAssign[targetId];

			console.log("targetId: ",		JSON.parse(JSON.stringify(targetId)));
			console.log("Pool Plans: ",		JSON.parse(JSON.stringify(poolPlans)));
			console.log("Assign To: ",		JSON.parse(JSON.stringify(assignToDaysWeeks)));

			for(let i = 0; i < poolPlans.length; i++) {
				let newOption = {
					planName:	poolPlans[i].label,
					planId:		poolPlans[i].relatedPlanId,
					poolPlanId: poolPlans[i].poolPlanId,
					assign:		[]
				};

				if(this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {
					let weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
					for(let i = 0; i < weekdays.length; i++) {
						newOption.assign.push({ 
							label:		weekdays[i],	
							disabled:	assignToDaysWeeks.includes(weekdays[i]) === false,
							value:		weekdays[i],
							checked:	this._assignedPlanDayWeeks[weekdays[i]] && this._assignedPlanDayWeeks[weekdays[i]] === newOption.poolPlanId,
							variant:	(this._assignedPlanDayWeeks[weekdays[i]] && this._assignedPlanDayWeeks[weekdays[i]] === newOption.poolPlanId) ? "brand" : "neutral" 
						});
					}
				} else if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans" || this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {
					
					let weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
					for(let i = 0; i < weekdays.length; i++) {
						newOption.assign.push({ 
							label:		weekdays[i],	
							disabled:	false,
							value:		weekdays[i],
							checked:	this._assignedPlanDayWeeks[weekdays[i]] && this._assignedPlanDayWeeks[weekdays[i]] === newOption.poolPlanId,
							variant:	(this._assignedPlanDayWeeks[weekdays[i]] && this._assignedPlanDayWeeks[weekdays[i]] === newOption.poolPlanId) ? "brand" : "neutral" 
						});
					}

				} else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
				
					let weekCount = this._targets.length;

					for(let i = 1; i <= weekCount; i++) {
						newOption.assign.push({ 
							label:		"Week " + i,	
							value:		"Week " + i,
							disabled:	assignToDaysWeeks.includes("Week " + i) === false,
							checked:	this._assignedPlanDayWeeks["Week " + i] && this._assignedPlanDayWeeks["Week " + i] === newOption.poolPlanId,
							variant:	(this._assignedPlanDayWeeks["Week " + i] && this._assignedPlanDayWeeks["Week " + i] === newOption.poolPlanId) ? "brand" : "neutral" 
						});
					}

				}

				options.push(newOption);

			}

		}

		return options;

	}

	get isMobile() {
		return FORM_FACTOR !== "Large";
	}

    get isLoadingOrSaving() {
        return this._isLoading === true || this._isSaving === true;
    }

    /**
     * @description Is the Guided Plans
     */
    get isGuidedPlans() {
        if(this._checkInInformation) {
            return this._checkInInformation.nutritionPlanningMode === "Guided Plans";
        } else {
            return false;
        }
    }

    /**
     * @description Is the Flexi Plans
     */
    get isFlexiPlans() {
        if(this._checkInInformation) {
            return this._checkInInformation.nutritionPlanningMode === "Flexi Plans";
        } else {
            return false;
        }
    }

	get isCarbCyclePlans() {
        if(this._checkInInformation) {
            return this._checkInInformation.nutritionPlanningMode === "Carb-Cycle";
        } else {
            return false;
        }
	}

    get displayedTargets2() {

        let targetGroups = [];

        if(this._checkInInformation) {

            if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
                targetGroups.push({
                    order:      0,
                    targets:    [],
                    states: {
                        isOpen: this._openWeeks.includes(0)
                    }
                });
            }

            console.log("Target Plan Options: ", JSON.parse(JSON.stringify(this._targetPlanOptions)));

            for(let i = 0; i < this._targets.length; i++) {

                let updatedTarget = JSON.parse(JSON.stringify(this._targets[i]));

                updatedTarget.states = {
                    hasRelatedPlan: typeof(updatedTarget.selectedPlanId) !== "undefined"
                };
				updatedTarget.values = {
					planOptions: this._targetPlanOptions[updatedTarget.targetId],
					planOptionGroups: []
				};

                let myPlanGroup = {
                    label: "My Plans",
                    value: "My Plans",
                    options: []

                };

                let addedClientPoolPlans = [];
				
				for(const [targetId, poolPlans] of Object.entries(this._targetPlanOptions)) {
					let group = {
						label: this._nutritionTargets[targetId].name,
						value: targetId,
						options: []
					};
					for(let i = 0; i < poolPlans.length; i++) {
						let newOption = {
							label:	poolPlans[i].label,
							value:	poolPlans[i].poolPlanId
						};

                        if(poolPlans[i].relatedPlanRecordType === "Nutrition_Client_Plan" && addedClientPoolPlans.includes(poolPlans[i].poolPlanId) === false) {
                            addedClientPoolPlans.push(poolPlans[i].poolPlanId);
                            myPlanGroup.options.push(newOption);
                        } else if(poolPlans[i].relatedPlanRecordType === "Nutrition_Reference_Plan") {
                            group.options.push(newOption);
                        }
                        
					}
					updatedTarget.values.planOptionGroups.push(group)
				}

                updatedTarget.values.planOptionGroups.push(myPlanGroup);

                if(updatedTarget.selectedPlanId) {
                    let relatedPlan = this.findPlan(updatedTarget.selectedPlanId);
                    updatedTarget.plan = {
                        protein:    relatedPlan.protein,
                        carbs:      relatedPlan.carbs,
                        fat:        relatedPlan.fat,
                        calories:   relatedPlan.calories
                    };
                }

                if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans" || this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {

                    let targetGroupIndex = parseInt(i / 7);

                    //If this is the first day in the week
                    if(i % 7 === 0) {
                        targetGroups.push({
                            order:      targetGroupIndex,
                            week:       targetGroupIndex + 1,
                            startDate:  updatedTarget.targetDate,
                            targets:    [],
                            states: {
                                isOpen: this._openWeeks.includes(targetGroupIndex)
                            }
                        });
                    }
                    if(i % 7 === 6) {
                        targetGroups[targetGroupIndex].endDate = updatedTarget.targetDate;
                    }
                    targetGroups[targetGroupIndex].targets.push(updatedTarget);
                } else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
                    targetGroups[0].targets.push(updatedTarget);
                }
              


            }

        }


        return targetGroups;
    }

	get fromPlanOptions() {
		let options = [];
		let selectedPoolPlanIds = [];
		for(let i = 0; i < this._targets.length; i++) {
			if(this._targets[i].selectedPlanId) {
				selectedPoolPlanIds.push(this._targets[i].selectedPlanId);
			}
		}
		for(let i = 0; i < this._clientPlanOptions.length; i++) {
			if(selectedPoolPlanIds.includes(this._clientPlanOptions[i].poolPlanId) === true) {
				options.push({
					label: this._clientPlanOptions[i].label,
					value: this._clientPlanOptions[i].poolPlanId
				})
			}
		}
		return options;
	}

	/* ---------- SETTERS ---------- */
    
	/* ---------- CUSTOM METHODS ---------- */

	/**
	 * @description Returns a list of selected template pool ids
	 */
	@api GetSelectedTemplatePoolIds() {
		let tmpPoolItemIds = [];
		for(let i = 0; i < this._targets.length; i++) {
			if(this._targets[i].selectedPlanId) {
				if(tmpPoolItemIds.includes(this._targets[i].selectedPlanId) === false) {
					tmpPoolItemIds.push(this._targets[i].selectedPlanId);
				}
			}
		}
		return tmpPoolItemIds;
	}

	/**
	 * @description Convert Plans
	 *
	 * @param {String} fromTemplatePoolId
	 * @param {String} toTemplatePoolId
	 */
	@api ConvertPlans(fromTemplatePoolId, toTemplatePoolId) {
		console.log("Converting...");
		let updatedTargets = JSON.parse(JSON.stringify(this._targets));
		for(let i = 0; i < updatedTargets.length; i++) {
			if(updatedTargets[i].selectedPlanId === fromTemplatePoolId) {
				updatedTargets[i].selectedPlanId = toTemplatePoolId;
			}
		}
		this._targets = JSON.parse(JSON.stringify(updatedTargets));
		console.log("Targets: ", JSON.parse(JSON.stringify(this._targets)));
		this.fireUpdateEvent();
	}


	/**
	 * @description Assign Plans to days
	 * 
	 * @param {Object} dayOptions The day options
	 * @param {Integer} selectedWeek The selected week (If not set then apply to all weeks)
	 */
	@api AssignDays(dayOptions, selectedWeek) {
		console.log("Assigning...");
		console.log("Selected Week: ", selectedWeek);
		let updatedTargets = JSON.parse(JSON.stringify(this._targets));
		for(let i = 0; i < updatedTargets.length; i++) {

			//If selected week is not the current week
			if(typeof(selectedWeek) !== "undefined" && parseInt(i / 7) !== selectedWeek) {
				continue;
			}

			let tDate	= new Date(updatedTargets[i].targetDate);
			let day		= tDate.getDay();
			if(dayOptions["Sunday"] && day === 0) {
				updatedTargets[i].selectedPlanId = dayOptions["Sunday"];
			} else if(dayOptions["Monday"] && day === 1) {
				updatedTargets[i].selectedPlanId = dayOptions["Monday"];
			} else if(dayOptions["Tuesday"] && day === 2) {
				updatedTargets[i].selectedPlanId = dayOptions["Tuesday"];
			} else if(dayOptions["Wednesday"] && day === 3) {
				updatedTargets[i].selectedPlanId = dayOptions["Wednesday"];
			} else if(dayOptions["Thursday"] && day === 4) {
				updatedTargets[i].selectedPlanId = dayOptions["Thursday"];
			} else if(dayOptions["Friday"] && day === 5) {
				updatedTargets[i].selectedPlanId = dayOptions["Friday"];
			} else if(dayOptions["Saturday"] && day === 6) {
				updatedTargets[i].selectedPlanId = dayOptions["Saturday"];
			}

            this._updatedTargetIndexes.push(i);
            
		}
		this._targets = JSON.parse(JSON.stringify(updatedTargets));
		console.log("Targets: ", JSON.parse(JSON.stringify(this._targets)));
		this.fireUpdateEvent();
	}

    @api AssignWeeks(weekOptions) {
        console.log("Assigning...");
        let updatedTargets = JSON.parse(JSON.stringify(this._targets));
        for(let i = 0; i < updatedTargets.length; i++) {
			if(weekOptions[updatedTargets[i].assignedWeek]) {
				updatedTargets[i].selectedPlanId = weekOptions[updatedTargets[i].assignedWeek];
			}
        }
        this._targets = JSON.parse(JSON.stringify(updatedTargets));
        console.log("Targets: ", JSON.parse(JSON.stringify(this._targets)));
		this.fireUpdateEvent();
    }

	findPlan(planId) {
		for(let i = 0; i < this._clientPlanOptions.length; i++) {
			if(planId === this._clientPlanOptions[i].value) {
				return this._clientPlanOptions[i];
			}
		}
	}

	fireUpdateEvent() {
		this.dispatchEvent(new CustomEvent('update'));
	}

	/**
	 * @description Refresh the Plan Options
	 */
	@api RefreshPlanOptions(onSuccess, onError) {
		GetPlanOptions2({
			checkInId: this._checkInInformation.id
		}).then(result => {
			this._targetPlanOptions		= JSON.parse(JSON.stringify(result));
			if(onSuccess) {
				onSuccess(result);
			}         
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});
	}

	_saveCarbCyclePlans(onSuccess, onError) {

        let carbCyclePlans = [];
		
        for(let i = 0; i < this._targets.length; i++) {
            if(this._updatedTargetIndexes.includes(i) === true) {
                carbCyclePlans.push({
                    poolPlanId:         this._targets[i].selectedPlanId,
                    targetId:           this._targets[i].targetId,
                    assignedWeek:       this._targets[i].assignedWeek,
                    planDate:           this._targets[i].targetDate
                });
            }
        }
		
        console.log("Saving Carb Cycle Plans: ", JSON.parse(JSON.stringify(carbCyclePlans)));
       
        SaveCarbCycleTargets({
            checkInId:      this._checkInInformation.id,
            carbCycleDays:  carbCyclePlans
        }).then(result => {
            console.log("Successfully saved Carb-Cycle targets");
            this._updatedTargetIndexes = [];
            if(onSuccess) {
                onSuccess();
            }
			this._isSaving = false;
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
            if(onError) {
                onError(error);
            }
        });
    }

    /**
     * @description Save the Flexi Plans
     *
     * @param {Function} onSuccess Executes when the Flexi-Plans were successfully saved
     * @param {Function} onError Executes when the Flexi-Plans were not saved successfully
     */
    _saveFlexiPlans(onSuccess, onError) {
        let flexiPlans = [];

        for(let i = 0; i < this._targets.length; i++) {
            if(this._updatedTargetIndexes.includes(i) === true) {
                flexiPlans.push({
                    poolPlanId:         this._targets[i].selectedPlanId,
                    targetId:           this._targets[i].targetId,
                    assignedWeek:       this._targets[i].assignedWeek,
                    planDate:           this._targets[i].targetDate
                });
            }
        }

        console.log("Saving Flexi Plans: ", JSON.parse(JSON.stringify(flexiPlans)));
        


        SaveFlexiPlanTargets({
            checkInId:      this._checkInInformation.id,
            flexiPlanDays:  flexiPlans
        }).then(result => {
            console.log("Successfully saved flexi targets");
            this._updatedTargetIndexes = [];
            if(onSuccess) {
                onSuccess();
            }
			this._isSaving = false;
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
            if(onError) {
                onError(error);
            }
        });
    }

    _saveGuidedPlans(onSuccess, onError) {
        let guidedPlans = [];
        for(let i = 0; i < this._targets.length; i++) {
            guidedPlans.push({
                poolPlanId:				this._targets[i].selectedPlanId,
                targetId:               this._targets[i].targetId,
                assignedWeek:           this._targets[i].assignedWeek,
                startDate:              this._targets[i].targetStartDate,
                endDate:                this._targets[i].targetEndDate
            });
        }
        console.log("GUIDED PLANS: ", JSON.parse(JSON.stringify(guidedPlans)));
        SaveGuidedPlanTargets({
            checkInId:          this._checkInInformation.id,
            guidedPlanDays:     guidedPlans
        }).then(result => {
            console.log("Successfully saved guided targets");
            if(onSuccess) {
                onSuccess();
            }
            this._isSaving = false;
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
            if(onError) {
                onError(error);
            }
            this._isSaving = false;
        });
    }

    /**
     * @description Saves the assigned plans
     * 
     * @param {Function} onSuccess
     * @param {Function} onError
     */
    @api Save(onSuccess, onError) {
        this._isSaving = true;
        if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans") {
            this._saveFlexiPlans(onSuccess, onError);
        } else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
            this._saveGuidedPlans(onSuccess, onError);
        } else if(this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {
			console.log("A");
			this._saveCarbCyclePlans(onSuccess, onError);
			console.log("B");
		}
    }

    toggleTargetGroup(groupOrder) {
        let index = -1;
        for(let i = 0; i < this._openWeeks.length; i++) {
            if(this._openWeeks[i] === groupOrder) {
                index = i;
                break;
            }
        }
        if(index != -1) {
            this._openWeeks.splice(index, 1);
        } else {
            this._openWeeks.push(groupOrder);
        }
    }

    tagAsUpdated(order) {
        if(this._updatedTargetIndexes.includes(order) === false) {
            this._updatedTargetIndexes.push(order);
        }
    }

	/* ---------- CUSTOM EVENTS ---------- */

    onOpenTargetGroup(event) {
        let order = parseInt(event.target.dataset.order);
        this.toggleTargetGroup(order);
    }
    
	/**
	 * @description Executes when a plan is selected for a target
	 */
	onSelectPlanForTarget(event) {
		let selectedPlan	= event.target.value;
		let targetDate		= event.target.dataset.targetDate;

		console.log("Selected Plan: ", selectedPlan);
		console.log("Target Date: ", targetDate);

		for(let i = 0; i < this._targets.length; i++) {

			console.log("Target: ", JSON.parse(JSON.stringify(this._targets[i])));

            if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
                if(this._targets[i].targetStartDate === targetDate) {
                    this._targets[i].selectedPlanId = selectedPlan;
                    this.tagAsUpdated(i);
                }
            } else if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans" || this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {
                if(this._targets[i].targetDate === targetDate) {
                    this._targets[i].selectedPlanId = selectedPlan;
                    this.tagAsUpdated(i);
                }
            }
		}
		this._targets = JSON.parse(JSON.stringify(this._targets));
		this.fireUpdateEvent();
	}

	onUpdateFromValue(event) {
		this._fromValue = event.target.value;
	}

	onUpdateToValue(event) {
		this._toValue	= event.target.value;
	}

	onConvertTargetPlans(event) {
		for(let i = 0; i < this._targets.length; i++) {
			console.log(JSON.parse(JSON.stringify(this._targets[i])));
			if(this._targets[i].selectedPlanId === this._fromValue) {
				this._targets[i].selectedPlanId = this._toValue;
			}
		}		
		this.fireUpdateEvent();
	}

    onSaveTest(event) {
        this.Save();
    }

	onOpenAssignToDaysModal(event) {
		this._displayAssignToDaysModal	= true;
        let order = parseInt(event.target.dataset.order);
		this._selectedWeekGroup			= order;
        this.toggleTargetGroup(order);
		this._assignedPlanDayWeeks		= {};
	}

	onCloseAssignToDaysModal(event) {
		this._displayAssignToDaysModal = false;
	}
    
	onAssignSelectDayWeek(event) {
		let poolPlanId				= event.target.dataset.poolPlanId;
		let weekdayWeekValue		= event.target.value;

		if(this._assignedPlanDayWeeks[weekdayWeekValue] === poolPlanId) {
			//delete this._assignedPlanDayWeeks[weekdayWeekValue];
		} else {
			this._assignedPlanDayWeeks[weekdayWeekValue] = poolPlanId;
		}

	}


    assignDaysOrWeeks() {
        if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans" || this._checkInInformation.nutritionPlanningMode === "Carb-Cycle") {
            if(this._assignedApplyToAllWeeks === true) {
                this.AssignDays(this._assignedPlanDayWeeks);
                this._displayAssignToDaysModal = false;
            } else {
                this.AssignDays(this._assignedPlanDayWeeks, this._selectedWeekGroup);
                this._displayAssignToDaysModal = false;
            }
        } else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {
            this.AssignWeeks(this._assignedPlanDayWeeks);
            this._displayAssignToDaysModal = false;
        }
    }

    onAssignFlexiPlansForWeek(event) {
        this._assignedApplyToAllWeeks = false;
        this.assignDaysOrWeeks();
    }

    onAssignFlexiPlansForAllWeeks(event) {
        this._assignedApplyToAllWeeks = true;
        this.assignDaysOrWeeks();
    }

	onAssignGuidedPlans(event) {
		this.assignDaysOrWeeks();
	}
    
	/* ---------- LWC EVENTS ---------- */

	connectedCallback() {

        this._isLoading = true;
            
		GetAssignPlanInformation().then(result => {
			console.log("Result: ", JSON.parse(JSON.stringify(result)));
			console.log("Client Plan Options: ", JSON.parse(JSON.stringify(result.clientPlanOptions)));

            if(result.guidedPlanTargets) {
                this._targets = JSON.parse(JSON.stringify(result.guidedPlanTargets));
            } else if(result.flexiPlanTargets) {
                this._targets = JSON.parse(JSON.stringify(result.flexiPlanTargets));
            } else if(result.carbCycleTargets) {
                this._targets = JSON.parse(JSON.stringify(result.carbCycleTargets));
            }
           
			this._nutritionTargets		= JSON.parse(JSON.stringify(result.nutritionTargets));
			this._targetPlanOptions		= JSON.parse(JSON.stringify(result.trackerPlanOptions));
			this._targetAssign			= JSON.parse(JSON.stringify(result.targetAssignToDays));
			this._clientPlanOptions		= JSON.parse(JSON.stringify(result.clientPlanOptions));
			this._checkInInformation	= JSON.parse(JSON.stringify(result.checkInInformation));
   
            this._isLoading = false;

        }).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}


}