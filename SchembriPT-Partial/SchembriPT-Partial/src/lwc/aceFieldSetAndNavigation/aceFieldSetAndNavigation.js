import { LightningElement, api, track } from 'lwc';

import getAllObjectTypes		from '@salesforce/apex/FieldSetView.getAllObjectTypes';
import getFieldSets				from '@salesforce/apex/FieldSetView.getFieldSets';
import getAllObjectFieldSets	from '@salesforce/apex/FieldSetView.getAllObjectFieldSets';

export default class AceFieldSetAndNavigation extends LightningElement {
	    
	/* --------------- CUSTOM VARIABLES --------------- */
	
	/**
	 * Variables related to the Lightning Web Component
	 */
	_inputVariables = [];
	
	/**
	 * @description Context variables realted to the flow
	 */
    _builderContext = {};

	/**
	 * @description Object Options available for the fieldset
	 */
	_objectOptions = [];
	
	/**
	 * @description Fieldset Options available for the fieldset (relative to the object selected)
	 */
	_fieldsetOptions = [];

	/**
	 * @description All Buttons 
	 */
	_flowButtons = [];

	_objectFieldSets = {};

	@track _initLoading = false;
	
	/**
	 * @description All available Label Options
	 */
	labelOptions = [
		{ label: 'Label Inline',	value: 'label-inline' },
		{ label: 'Label Stacked',	value: 'label-stacked' }
	];

	_variantOptions = [
		{ label: 'Base',				value: 'base' },
		{ label: 'Neutral',				value: 'neutral' },
		{ label: 'Brand',				value: 'brand' },
		{ label: 'Brand Outline',		value: 'brand-outline' },
		{ label: 'Destructive',			value: 'destructive' },
		{ label: 'Destructive Text',	value: 'destructive-text' },
		{ label: 'Inverse',				value: 'inverse' },
		{ label: 'Success',				value: 'success' }
	];

	_flowActionOptions = [
		{ label: 'Next',	value: 'NEXT' },
		{ label: 'Previous',	value: 'BACK' }
	
	];

	_css_fontWeights = [
		{ label: 'Normal',	value: 'normal' },
		{ label: 'Bold',	value: 'bold' },
		{ label: 'Bolder',	value: 'bolder' },
		{ label: 'Lighter',	value: 'lighter' },
		{ label: '100',		value: '100' },
		{ label: '200',		value: '200' },
		{ label: '300',		value: '300' },
		{ label: '400',		value: '400' },
		{ label: '500',		value: '500' },
		{ label: '600',		value: '600' },
		{ label: '700',		value: '700' },
		{ label: '800',		value: '800' },
		{ label: '900',		value: '900' },
		{ label: 'Initial',	value: 'initial' },
	];

	/* --------------- GETTERS --------------- */
	
    @api get inputVariables() {
        return this._inputVariables;
    }
	
	@api get builderContext() {
		return this._builderContext;
	}
	
	get getRecordIdOptions() {

		let options = [];

		for(let scrVariable of this._builderContext.variables) {
			if(scrVariable.dataType === "String" && scrVariable.isCollection == false) {
				options.push({
					label: scrVariable.name,
					value: scrVariable.name
				});
			}
		}
		return options;
	}

	get fieldSetForms() {
	
		let fieldSetShowTitles		= this.getInputValue('fieldSetShowTitles');
		
		let fieldSetTitleTexts			= this.getInputValue('fieldSetTitleTexts');

		let fieldSetColumns			= this.getInputValue('fieldSetColumns');
		let fieldSetEditables		= this.getInputValue('fieldSetEditables');
		let fieldSetObjectApiNames	= this.getInputValue('fieldSetObjectApiNames');
		let fieldSetNames			= this.getInputValue('fieldSetNames');
		let fieldSetLabelPositions	= this.getInputValue('fieldSetLabelPositions');
		
		let forms = [];
		
		if(fieldSetTitleTexts && fieldSetShowTitles && fieldSetColumns && fieldSetEditables && fieldSetObjectApiNames && fieldSetNames && fieldSetLabelPositions) {
		
			let fsTitleTexts		= fieldSetTitleTexts.split(';');

			let fsShowTitles		= fieldSetShowTitles.split(';');
			let fsColumns			= fieldSetColumns.split(';');
			let fsEditables			= fieldSetEditables.split(';');
			let fsObjectApiNames	= fieldSetObjectApiNames.split(';');
			let fsNames				= fieldSetNames.split(';');
			let fsLabelPositions	= fieldSetLabelPositions.split(';');
			
			for(let i = 0; i < fsTitleTexts.length; i++) {
			
				let newForm = {
					order:			i,
					title: {
						text:	fsTitleTexts[i],
					},
					showTitle:		(fsShowTitles[i] === 'true'),
					columns:		parseInt(fsColumns[i]),
					editable:		(fsEditables[i] === 'true'),
					objectApiName:	fsObjectApiNames[i],
					fieldSetName:	fsNames[i],
					labelPosition:	fsLabelPositions[i],
					states: {},
					picklistOptions: {}
				};

				if(i == 0) {
					newForm.recordId = this.getInputValue('fieldSetRecordIdOne');
				} else if(i == 1) {
					newForm.recordId = this.getInputValue('fieldSetRecordIdTwo');
				} else if(i == 2) {
					newForm.recordId = this.getInputValue('fieldSetRecordIdThree');
				} else if(i == 3) {
					newForm.recordId = this.getInputValue('fieldSetRecordIdFour');
				} else if(i == 4) {
					newForm.recordId = this.getInputValue('fieldSetRecordIdFive');
				}


				newForm.states.hasSelectedObject = (newForm.objectApiName !== " ");
				
				if(newForm.states.hasSelectedObject === true) {
					newForm.picklistOptions.fieldset = this._objectFieldSets[newForm.objectApiName];
				}

				forms.push(newForm);
				
			}
			
		}

		console.log("FORMS: ", JSON.parse(JSON.stringify(forms)))
		
		return forms;

	}
	
	get flowButtons() {

		let navigationLabels		= this.getInputValue('navigationLabels');
		let navigationActions		= this.getInputValue('navigationActions');
		let navigationValues		= this.getInputValue('navigationValues');
		let navigationSubmissions	= this.getInputValue('navigationSubmissions');
		let navigationVariants		= this.getInputValue('navigationVariants');

		let navButtons = [];

		if(navigationLabels && navigationActions && navigationValues && navigationSubmissions && navigationVariants) {

			let navLabels		= navigationLabels.split(';');
			let navActions		= navigationActions.split(';');
			let navValues		= navigationValues.split(';');
			let navSubmissions	= navigationSubmissions.split(';');
			let navVariants		= navigationVariants.split(';');
		
			for(let i = 0; i < navLabels.length; i++) {
			
				navButtons.push({
					order:		i,
					label:		navLabels[i],
					action:		navActions[i],
					value:		navValues[i],
					submit:		(navSubmissions[i] === 'true'),
					variant:	navVariants[i]
				});

			}

		}

		return navButtons;

	}

	/* --------------- SETTERS --------------- */
	
    // Set a field with the data that was stored from the flow.
    // This data includes the public volume property of the custom volume      
    // component.
    set inputVariables(variables) {
		console.log("Input Variables:", JSON.parse(JSON.stringify(variables)));
        this._inputVariables = variables || [];
		
		//If we have both the input variabeles and the builder context set, then init the component
		if(this._inputVariables && this._builderContext) {
			this.initComponent();
		}

    }

	set builderContext(context) {
		console.log("Builder Context:", JSON.parse(JSON.stringify(context)));
        this._builderContext = context || {};
		
		//If we have both the input variabeles and the builder context set, then init the component
		if(this._inputVariables && this._builderContext) {
			this.initComponent();
		}
	}
	
	/* --------------- CUSTOM METHODS --------------- */
	
	initComponent() {

		//Load the object types
		this.loadObjectTypes(
			(success) => {
		
				getAllObjectFieldSets({
				}).then(result => {
					console.log("SUCCESS: ", JSON.parse(JSON.stringify(result)));
					this._objectFieldSets = JSON.parse(JSON.stringify(result));
					this._initLoading = false;
				}).catch(error => {
					console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
					this._initLoading = false;
				});


			},
			(error) => {
		
			}
		);

	}
	
	loadObjectTypes(onSuccess, onError) {
		getAllObjectTypes().then(result => {
			this._objectOptions = JSON.parse(JSON.stringify(result));
			onSuccess(this._objectOptions);
		}).catch(error => {
			onError(error);
		});
	}
	

	loadFieldsets(objectApiName, onSuccess, onError) {
		getFieldSets({
			objectName: objectApiName
		}).then(result => {
			this._fieldsetOptions = JSON.parse(JSON.stringify(result));

			if(onSuccess) {
				onSuccess(result);
			}

		}).catch(error => {

			if(onError) {
				onError(error);
			}

		});
	}
		


	/**
	 * @description Get the value 
	 */
	getInputValue(fieldName) {
        const param = this.inputVariables.find(({name}) => name === fieldName);
        return param && param.value;
	}
	
	fireInputValueChangedEvent(name, newValue, newValueDataType) {
		const valueChangedEvent = new CustomEvent(
			'configuration_editor_input_value_changed', {
					bubbles: true,
					cancelable: false,
					composed: true,
					detail: {
						name,
						newValue,
						newValueDataType
					}
			}
		);
		this.dispatchEvent(valueChangedEvent);
	}

	/* --------------- CUSTOM EVENTS --------------- */

	onUpdateFieldSetRecordId(event) {
		
		let newRecordId = event.target.value;
		let order		= parseInt(event.target.dataset.order);

		let inputFieldName	= '';
		if(order === 0) {
			inputFieldName = 'fieldSetRecordIdOne';
		} else if(order === 1) {
			inputFieldName = 'fieldSetRecordIdTwo';
		} else if(order === 2) {
			inputFieldName = 'fieldSetRecordIdThree';
		} else if(order === 3) {
			inputFieldName = 'fieldSetRecordIdFour';
		} else if(order === 4) {
			inputFieldName = 'fieldSetRecordIdFive';
		}

		this.fireInputValueChangedEvent(inputFieldName, '{!' +  newRecordId + '}', 'reference');


	}

	/**
	 * @description Update one of the values on a list
	 * @param {String} valueListName The name of the list
	 * @param {Number} order The Index in the list that we are updating
	 * @param {String} newValue The New Value to set the list
	 */
	updateListValue(valueListName, order, newValue) {
		let values		= (this.getInputValue(valueListName)).split(';');
		values[order]	= newValue;
		this.fireInputValueChangedEvent(valueListName, values.join(';'), 'String');
	}

	onUpdateFieldSetTitleText(event) {
		this.updateListValue('fieldSetTitleTexts', parseInt(event.target.dataset.order), event.target.value);
	}
	
	onUpdateFieldSetShowTitle(event) {
		this.updateListValue('fieldSetShowTitles', parseInt(event.target.dataset.order), event.target.checked.toString());
	}

	onUpdateFieldColumns(event) {
		this.updateListValue('fieldSetColumns', parseInt(event.target.dataset.order), parseInt(event.target.value));
	}

	onUpdateFieldEditables(event) {
		this.updateListValue('fieldSetEditables', parseInt(event.target.dataset.order), event.target.checked.toString());
	}
	
	onUpdateObjectApiNames(event) {
		this.updateListValue('fieldSetObjectApiNames', parseInt(event.target.dataset.order), event.target.value);
	}

	onUpdateFieldSetName(event) {
		this.updateListValue('fieldSetNames', parseInt(event.target.dataset.order), event.target.value);
	}
	
	onDeleteFieldset(event) {
		
		let order = parseInt(event.target.dataset.order);
		
		let fieldSetTitleTexts		= (this.getInputValue('fieldSetTitleTexts')).split(';');
		let fieldSetShowTitles		= (this.getInputValue('fieldSetShowTitles')).split(';');
		let fieldSetColumns			= (this.getInputValue('fieldSetColumns')).split(';');
		let fieldSetEditables		= (this.getInputValue('fieldSetEditables')).split(';');
		let fieldSetObjectApiNames	= (this.getInputValue('fieldSetObjectApiNames')).split(';');
		let fieldSetNames			= (this.getInputValue('fieldSetNames')).split(';');
		let fieldSetLabelPositions	= (this.getInputValue('fieldSetLabelPositions')).split(';');
		
		fieldSetTitleTexts.splice(order, 1);
		fieldSetShowTitles.splice(order, 1);
		fieldSetColumns.splice(order, 1);
		fieldSetEditables.splice(order, 1);
		fieldSetObjectApiNames.splice(order, 1);
		fieldSetNames.splice(order, 1);
		fieldSetLabelPositions.splice(order, 1);
		
		this.fireInputValueChangedEvent('fieldSetTitleTexts',		fieldSetTitleTexts.join(';'),		'String');
		this.fireInputValueChangedEvent('fieldSetShowTitles',		fieldSetShowTitles.join(';'),		'String');
		this.fireInputValueChangedEvent('fieldSetColumns',			fieldSetColumns.join(';'),			'String');
		this.fireInputValueChangedEvent('fieldSetEditables',		fieldSetEditables.join(';'),		'String');
		this.fireInputValueChangedEvent('fieldSetObjectApiNames',	fieldSetObjectApiNames.join(';'),	'String');
		this.fireInputValueChangedEvent('fieldSetNames',			fieldSetNames.join(';'),			'String');
		this.fireInputValueChangedEvent('fieldSetLabelPositions',	fieldSetLabelPositions.join(';'),	'String');

		if(order === 0) {
			let recordId2 = this.getInputValue('fieldSetRecordIdTwo');
			let recordId3 = this.getInputValue('fieldSetRecordIdThree');
			let recordId4 = this.getInputValue('fieldSetRecordIdFour');
			let recordId5 = this.getInputValue('fieldSetRecordIdFive');
			
			this.fireInputValueChangedEvent('fieldSetRecordIdOne',	recordId2, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdTwo',	recordId3, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdThree', recordId4, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFour', recordId5, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFive', '', 'String');
		} else if(order === 1) {
			let recordId3 = this.getInputValue('fieldSetRecordIdThree');
			let recordId4 = this.getInputValue('fieldSetRecordIdFour');
			let recordId5 = this.getInputValue('fieldSetRecordIdFive');
			
			this.fireInputValueChangedEvent('fieldSetRecordIdTwo',	recordId3, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdThree', recordId4, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFour', recordId5, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFive', '', 'String');
		} else if(order === 2) {
			let recordId4 = this.getInputValue('fieldSetRecordIdFour');
			let recordId5 = this.getInputValue('fieldSetRecordIdFive');
			
			this.fireInputValueChangedEvent('fieldSetRecordIdThree', recordId4, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFour', recordId5, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFive', '', 'String');
		} else if(order === 3) {
			let recordId5 = this.getInputValue('fieldSetRecordIdFive');
			
			this.fireInputValueChangedEvent('fieldSetRecordIdFour', recordId5, 'String');
			this.fireInputValueChangedEvent('fieldSetRecordIdFive', '', 'String');

		} else if(order === 4) {
			this.fireInputValueChangedEvent('fieldSetRecordIdFive', '', 'String');
		}


	}


	onAddFieldSet(event) {
	
		let fieldSetTitleTexts		= this.getInputValue('fieldSetTitleTexts');
		let fieldSetShowTitles		= this.getInputValue('fieldSetShowTitles');
		let fieldSetColumns			= this.getInputValue('fieldSetColumns');
		let fieldSetEditables		= this.getInputValue('fieldSetEditables');
		let fieldSetObjectApiNames	= this.getInputValue('fieldSetObjectApiNames');
		let fieldSetNames			= this.getInputValue('fieldSetNames');
		let fieldSetLabelPositions	= this.getInputValue('fieldSetLabelPositions');

		if(fieldSetTitleTexts) { fieldSetTitleTexts = fieldSetTitleTexts.split(';'); } else { fieldSetTitleTexts = []; }
		if(fieldSetShowTitles) { fieldSetShowTitles = fieldSetShowTitles.split(';'); } else { fieldSetShowTitles = []; }
		if(fieldSetColumns) { fieldSetColumns = fieldSetColumns.split(';'); } else { fieldSetColumns = []; }
		if(fieldSetEditables) { fieldSetEditables = fieldSetEditables.split(';'); } else { fieldSetEditables = []; }
		if(fieldSetObjectApiNames) { fieldSetObjectApiNames = fieldSetObjectApiNames.split(';'); } else { fieldSetObjectApiNames = []; }
		if(fieldSetNames) { fieldSetNames = fieldSetNames.split(';'); } else { fieldSetNames = []; }
		if(fieldSetLabelPositions) { fieldSetLabelPositions = fieldSetLabelPositions.split(';'); } else { fieldSetLabelPositions = []; }

		fieldSetTitleTexts.push('...');
		fieldSetShowTitles.push('false');
		fieldSetColumns.push('1');
		fieldSetEditables.push('true');
		fieldSetObjectApiNames.push(' ');
		fieldSetNames.push(' ');
		fieldSetLabelPositions.push('label-stacked');
		
		this.fireInputValueChangedEvent('fieldSetTitleTexts',		fieldSetTitleTexts.join(';'), 'String');
		this.fireInputValueChangedEvent('fieldSetShowTitles',		fieldSetShowTitles.join(';'), 'String');
		this.fireInputValueChangedEvent('fieldSetColumns',			fieldSetColumns.join(';'), 'String');
		this.fireInputValueChangedEvent('fieldSetEditables',		fieldSetEditables.join(';'), 'String');
		this.fireInputValueChangedEvent('fieldSetObjectApiNames',	fieldSetObjectApiNames.join(';'), 'String');
		this.fireInputValueChangedEvent('fieldSetNames',			fieldSetNames.join(';'), 'String');
		this.fireInputValueChangedEvent('fieldSetLabelPositions',	fieldSetLabelPositions.join(';'), 'String');

		let length = fieldSetTitleTexts.length;

		if(length === 1) {
			this.fireInputValueChangedEvent('fieldSetRecordIdOne', '{!}', 'reference');
		} else if(length === 2) {
			this.fireInputValueChangedEvent('fieldSetRecordIdTwo', '{!}', 'reference');
		} else if(length === 3) {
			this.fireInputValueChangedEvent('fieldSetRecordIdThree', '{!}', 'reference');
		} else if(length === 4) {
			this.fireInputValueChangedEvent('fieldSetRecordIdFour', '{!}', 'reference');
		} else if(length === 5) {
			this.fireInputValueChangedEvent('fieldSetRecordIdFive', '{!}', 'reference');
		}


		
	}
	
	onUpdateFieldLabelPosition(event) {
	
		let newLabelPosition		= event.target.value;
		let order					= parseInt(event.target.dataset.order);

		let fieldSetLabelPositions		= (this.getInputValue('fieldSetLabelPositions')).split(';');
		fieldSetLabelPositions[order]	= newLabelPosition;
		this.fireInputValueChangedEvent('fieldSetLabelPositions', fieldSetLabelPositions.join(';'), 'String');

	}

	/**
	 * @description Execures when a Flow Button Label is updated
	 */
	onUpdateFlowLabel(event) {
		let newLabel = event.target.value;
		let order = parseInt(event.target.dataset.order);

		let navigationLabels	= this.getInputValue('navigationLabels');
		if(navigationLabels) {	navigationLabels = navigationLabels.split(';');		} else { navigationLabels = []; }

		navigationLabels[order] = newLabel;

		this.fireInputValueChangedEvent('navigationLabels',		navigationLabels.join(';'),		'String');
	}
	
	/**
	 * @description Execures when a Flow Button Action is updated
	 */
	onUpdateFlowAction(event) {
		let newAction = event.target.value;
		let order = parseInt(event.target.dataset.order);

		let navigationActions	= this.getInputValue('navigationActions');
		if(navigationActions) { navigationActions = navigationActions.split(';');	} else { navigationActions = []; }
		
		navigationActions[order] = newAction;

		this.fireInputValueChangedEvent('navigationActions',	navigationActions.join(';'),	'String');
	}
	
	/**
	 * @description Execures when a Flow Button Value is updated
	 */
	onUpdateFlowValue(event) {
		let newValue = event.target.value;
		let order = parseInt(event.target.dataset.order);
		
		let navigationValues	= this.getInputValue('navigationValues');
		if(navigationValues) {	navigationValues = navigationValues.split(';');		} else { navigationValues = []; }

		navigationValues[order] = newValue;

		this.fireInputValueChangedEvent('navigationValues',		navigationValues.join(';'),		'String');

	}

	onUpdateFlowVariant(event) {
		let newVariant	= event.target.value;
		let order		= parseInt(event.target.dataset.order);
		
		let navigationVariants	= this.getInputValue('navigationVariants');
		if(navigationVariants) {	navigationVariants = navigationVariants.split(';');		} else { navigationVariants = []; }
		
		navigationVariants[order] = newVariant;
		this.fireInputValueChangedEvent('navigationVariants', navigationVariants.join(';'), 'String');

	}

	onUpdateFlowSubmit(event) {
		let newSubmit	= event.target.checked;
		let order		= parseInt(event.target.dataset.order);
		let navigationSubmissions	= this.getInputValue('navigationSubmissions');
		if(navigationSubmissions) {	navigationSubmissions = navigationSubmissions.split(';');		} else { navigationSubmissions = []; }
		navigationSubmissions[order] = (newSubmit) ? 'true' : 'false';
		this.fireInputValueChangedEvent('navigationSubmissions', navigationSubmissions.join(';'), 'String');
	}
	
	onDeleteFlowButton(event) {
		let order = parseInt(event.target.dataset.order);

		let navigationLabels		= this.getInputValue('navigationLabels');
		let navigationActions		= this.getInputValue('navigationActions');
		let navigationValues		= this.getInputValue('navigationValues');
		let navigationSubmissions	= this.getInputValue('navigationSubmissions');
		let navigationVariants		= this.getInputValue('navigationVariants');

		if(navigationLabels) {	navigationLabels = navigationLabels.split(';');		} else { navigationLabels = []; }
		if(navigationActions) { navigationActions = navigationActions.split(';');	} else { navigationActions = []; }
		if(navigationValues) {	navigationValues = navigationValues.split(';');		} else { navigationValues = []; }
		if(navigationSubmissions) { navigationSubmissions = navigationSubmissions.split(';'); } else { navigationSubmissions = []; }
		if(navigationVariants) { navigationVariants = navigationVariants.split(';'); } else { navigationVariants = []; }

		navigationLabels.splice(order, 1);
		navigationActions.splice(order, 1);
		navigationValues.splice(order, 1);
		navigationSubmissions.splice(order, 1);
		navigationVariants.splice(order, 1);

		this.fireInputValueChangedEvent('navigationLabels',			navigationLabels.join(';'),			'String');
		this.fireInputValueChangedEvent('navigationActions',		navigationActions.join(';'),		'String');
		this.fireInputValueChangedEvent('navigationValues',			navigationValues.join(';'),			'String');
		this.fireInputValueChangedEvent('navigationSubmissions',	navigationSubmissions.join(';'),	'String');
		this.fireInputValueChangedEvent('navigationVariants',		navigationVariants.join(';'),		'String');

	}

	
	onAddFlowButton(event) {
	
		let navigationLabels		= this.getInputValue('navigationLabels');
		let navigationActions		= this.getInputValue('navigationActions');
		let navigationValues		= this.getInputValue('navigationValues');
		let navigationSubmissions	= this.getInputValue('navigationSubmissions');
		let navigationVariants		= this.getInputValue('navigationVariants');

		if(navigationLabels) { navigationLabels = navigationLabels.split(';'); } else { navigationLabels = []; }
		if(navigationActions) { navigationActions = navigationActions.split(';'); } else { navigationActions = []; }
		if(navigationValues) { navigationValues = navigationValues.split(';'); } else { navigationValues = []; }
		if(navigationSubmissions) { navigationSubmissions = navigationSubmissions.split(';'); } else { navigationSubmissions = []; }
		if(navigationVariants) { navigationVariants = navigationVariants.split(';'); } else { navigationVariants = []; }

		navigationLabels.push('New Label');
		navigationActions.push('NEXT');
		navigationValues.push('-');
		navigationSubmissions.push('true');
		navigationVariants.push('neutral');
		
		this.fireInputValueChangedEvent('navigationLabels',			navigationLabels.join(';'),			'String');
		this.fireInputValueChangedEvent('navigationActions',		navigationActions.join(';'),		'String');
		this.fireInputValueChangedEvent('navigationValues',			navigationValues.join(';'),			'String');
		this.fireInputValueChangedEvent('navigationSubmissions',	navigationSubmissions.join(';'),	'String');
		this.fireInputValueChangedEvent('navigationVariants',		navigationVariants.join(';'),		'String');

	}

	connectedCallback() {
		this._initLoading = true;
	}

}