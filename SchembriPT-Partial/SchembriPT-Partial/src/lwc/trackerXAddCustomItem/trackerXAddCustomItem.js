import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import Template_Item__c from '@salesforce/schema/Template_Item__c';
import Id from '@salesforce/user/Id';


export default class TrackerXAddCustomItem extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The Id of the User
	 */
    _userId = Id;

	/**
	 * @description Tracks if the data is loading
	 */
	@track _isLoading = false;
	
	/**
	 * @description Holds the 'Custom Nutrition' Record Type data
	 */
	@track _customNutritionRecordType;

	/* ---------- GETTERS ---------- */

	get hasRecordTypeInfo() {
		return this._customNutritionRecordType !== undefined;
	}

	/* ---------- SETTERS ---------- */

	/* ---------- CUSTOM METHODS ---------- */

	/* ---------- CUSTOM EVENTS ---------- */

	onSaveItem(event) {
		let tempItemForm = this.template.querySelector('.template-item-form');
		tempItemForm.submit();
	}

	onCancelItem(event) {
        const cancelEvent		= new CustomEvent('cancel');
        this.dispatchEvent(cancelEvent);
	}

	onSaveFormSuccess(event) {
		//Fire a 'save' event and include the new template item data
		let newTemplateItemData = event.detail;
        const saveEvent			= new CustomEvent('save', { detail: newTemplateItemData });
        this.dispatchEvent(saveEvent);
	}

	onSaveFormError(event) {
		console.error("Form Error: ", JSON.parse(JSON.stringify(event.detail)));
	}

	/* ---------- WIRE EVENTS ---------- */

	@wire(getObjectInfo, {objectApiName: Template_Item__c})
	getRecordTypeData({ data, error }) {

		if(data) {
			for(let recId in data.recordTypeInfos) {
				let recType = data.recordTypeInfos[recId];
				if(recType.name === "Custom Nutrition") {
					this._customNutritionRecordType = recType;
				}
			}

			this.isLoading = false;

		} else if(error) {
			console.error("[trackerXAddCustomItem] ERROR: ", JSON.parse(JSON.stringify(error)) );
		}

	}


	
	/* ---------- LWC FUNCTIONS ---------- */
	
	connectedCallback() {
		this.isLoading = true;
	}

}