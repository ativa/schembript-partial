import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';

import getCheckInInfo 	from '@salesforce/apex/CommunityHome.getCheckInInfo'; //Just Get the CheckIn Info, and pull the photos from there
import recordPhoto 		from '@salesforce/apex/CommunityHome.recordPhoto';

export default class Cci_PhotosSection extends LightningElement {

	/* --------------- VARIABLES --------------- */

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;

	/**
	 * @description The Id of the Client
	 */
	@track _clientId;

	/**
	 * @description The URL of the Front Photo
	 */
	@track frontPhotoURL;

	/**
	 * @description The URL of the Side Photo
	 */
	@track sidePhotoURL;

	/**
	 * @description The URL of the Back Photo
	 */
	@track backPhotoURL;

	/**
	 * @description The Gender of the client
	 */
	@track clientGender;

	/* --------------- GETTERS --------------- */

	/**
	 * @description Returns the Check-In Id
	 * @returns The Check-In Id
	 */
	@api get checkInId() {
		return this._checkInId;
	}

	/**
	 * @description Is this client Male?
	 * @returns Returns true if the client is Male, false otherwise
	 */
	get isMaleClient() {
		return this.clientGender === 'Male';
	}

	/**
	 * @description Is this client Female
	 * @returns Returns true if the client is Female, false otherwise
	 */
	get isFemaleClient() {
		return this.clientGender === 'Female';
	}

	/**
	 * @description Is The Component being used on mobile
	 * @returns Returns true if being used by the mobile, false otherwise
	 */
	get isMobile() {
		return FORM_FACTOR === 'Small';
	}

	/**
	 * @description Has the Front Photo been uploaded
	 * @returns Returns true if the front photo has been uploaded, false otherwise
	 */
	get isFrontPhotoUploaded() {
		return typeof(this.frontPhotoURL) != "undefined";
	}

	/**
	 * @description Has the Side Photo been uploaded
	 * @returns Returns true if the side photo has been uploaded, false otherwise
	 */
	get isSidePhotoUploaded() {
		return typeof(this.sidePhotoURL) != "undefined";
	}

	/**
	 * @description Has the Back Photo been uploaded
	 * @returns Returns true if the back photo has been uploaded, false otherwise
	 */
	get isBackPhotoUploaded() {
		return typeof(this.backPhotoURL) != "undefined";
	}

	/* --------------- SETTERS --------------- */

	set checkInId(newCheckInId) {

		this._checkInId = newCheckInId;

		getCheckInInfo({

		}).then(result => {

			this.clientGender = result.clientGender;

			this.frontPhotoURL 	= result.frontPhotoURL;
			this.sidePhotoURL 	= result.sidePhotoURL;
			this.backPhotoURL 	= result.backPhotoURL;

			console.log("[cci_PhotosSection] Get CheckIn Info: ", JSON.parse(JSON.stringify(result)));
		}).catch(error => {
			console.error("[cci_PhotosSection] Get CheckIn Info: ", JSON.parse(JSON.stringify(error)));
		});

	}

	/* --------------- CUSTOM METHODS --------------- */

	/**
	 * @description Have all the photos been submitted
	 * @returns Have the photos been set?
	 */
	@api allValuesSet() {
	
		let frontSet 	= typeof(this.frontPhotoURL) != "undefined";
		let sideSet 	= typeof(this.sidePhotoURL) != "undefined";
		let backSet 	= typeof(this.backPhotoURL) != "undefined";
		
		let returnValue = {
			valuesSet:		frontSet === true && sideSet === true && backSet === true,
			notSetFields:	[]
		};
		
		return returnValue;

	}

	/**
	 * @description Add a Document Photo, as a Photo Object
	 * @param {String} photoType The Type of photo we are uploading (Front, Side, Back)
	 * @param {String} documentId The Id of the Document we are uploading
	 */
	addPhotoRecord(photoType, documentId) {

		recordPhoto({
			checkInID:	this.checkInId,
			name: 		photoType,
			documentID: documentId
		}).then(result => {
			console.log("Updated Photo: ", JSON.parse(JSON.stringify(result)));

			if(photoType === 'front') {
				console.log("Updating Front Photo URL");
				this.frontPhotoURL = result;
			} else if(photoType === 'side') {
				console.log("Updating Side Photo URL");
				this.sidePhotoURL = result;
			} else if(photoType === 'back') {
				console.log("Updating Back Photo URL");
				this.backPhotoURL = result;
			}

			//Fire an Upload Event
			this.fireOnPhotoUpload(photoType);

		}).catch(error => {
			console.error("Updated Photo Error: ", JSON.parse(JSON.stringify(error)));
		});

	}

	/**
	 * @description Fires a Photo Upload Event
	 * @param {String} photoType The Type of Photo that was just uploaded
	 */
	fireOnPhotoUpload(photoType) {
		
		let data = {
			photoType: 			photoType,
			url: 				'',
			allPhotosUploaded:	this.allValuesSet().valuesSet
		};

		if(photoType === 'front') {
			data.url = this.frontPhotoURL;
		} else if(photoType === 'side') {
			data.url = this.sidePhotoURL;
		} else if(photoType === 'back') {
			data.url = this.backPhotoURL;
		}

        // Creates the event with the contact ID data.
        const uploadPhotoEvent = new CustomEvent('photoupload', { detail: data });

		console.log("Fire Photo Upload");

        // Dispatches the event.
		this.dispatchEvent(uploadPhotoEvent);
		
	}

	/* --------------- CUSTOM EVENTS --------------- */

	/**
	 * @description Triggers when the Front Photo has been successfully uploaded
	 * @param {Event} event The Event that triggers this method
	 */
	onFrontPhotoUploaded(event) {
		this.addPhotoRecord('front', event.detail.files[0].documentId);
	}

	/**
	 * @description Triggers when the Front Photo is going to be deleted
	 * @param {Event} event The Event that triggers this method
	 */
	onFrontPhotoDelete(event) {

	}

	/**
	 * @description Triggers when the Side Photo has been successfully uploaded
	 * @param {Event} event The Event that triggers this method
	 */
	onSidePhotoUploaded(event) {
		this.addPhotoRecord('side', event.detail.files[0].documentId);
	}

	/**
	 * @description Triggers when the Side Photo is going to be deleted
	 * @param {Event} event The Event that triggers this method
	 */
	onSidePhotoDelete(event) {

	}

	/**
	 * @description Triggers when the Back Photo has been successfully uploaded
	 * @param {Event} event The Event that triggers this method
	 */
	onBackPhotoUploaded(event) {
		this.addPhotoRecord('back', event.detail.files[0].documentId);
	}

	/**
	 * @description Triggers when the Back Photo is going to be deleted
	 * @param {Event} event The Event that triggers this method
	 */
	onBackPhotoDelete(event) {

	}

}