/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-13-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-13-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';

/* ---------- TrackerController Class Actions ---------- */
import QuerySessionPlanData			from '@salesforce/apex/TrackerController.QuerySessionPlanData';
import CompletePlan					from '@salesforce/apex/TrackerController.CompletePlan';
import UpdateBlockCompletedState	from '@salesforce/apex/TrackerController.UpdateBlockCompletedState';
import AddTemplateItemToBlock		from '@salesforce/apex/TrackerController.AddTemplateItemToBlock';
import UpdateTrackerData			from '@salesforce/apex/TrackerController.UpdateTrackerData';
import UpdateBlockTrackerData		from '@salesforce/apex/TrackerController.UpdateBlockTrackerData';
import GetNutritionalItems			from '@salesforce/apex/TrackerController.GetNutritionalItems';
import DeleteItems					from '@salesforce/apex/TrackerController.DeleteItems';
import AddTemplateItems				from '@salesforce/apex/TrackerController.AddTemplateItems';
import AddNewSessionItems			from '@salesforce/apex/TrackerController.AddNewSessionItems';

/**
 * @description The valid modes for the Tracker
 */
const MODES = [ 'PASSIVE', 'ACTIVE' ];

export default class TrackerXPlan extends LightningElement {

	/* ---------- VARIABLES ---------- */

	@track _mode = 'PASSIVE';

	/**
	 * @description Is the Plan being loaded
	 */
	@track _isLoading = false;

	/** 
	 * @description Stores the Plan__c id
	 */
	@track _planId;

	/**
	 * @description The Original Plan Data for the Plan
	 */
	@track _origPlanData;

	/**
	 * @description The Updated Plan Data for the Plan
	 */
	@track _updatedPlanData;

	/**
	 * @description The Nutritional Items that we can add
	 */
	@track _nutritionalItems = [];

	/**
	 * @description The Filtered Nutritional Items
	 */
	@track _filteredItems = [];

	/**
	 * @description The Name Filter Parameter
	 */
	@track _filteredNameSearchParameter = "";

	/**
	 * @description Tracks block and item changes we want to save
	 */
	@track _changesToSave = {
		blocks: {}
	};

	/**
	 * @description Tracks the Items that need to be deleted
	 */
	@track _itemsToDelete = []

	/**
	 * @description Tracks if the user is currently adding a custom item
	 */
	@track _addCustomItem = false;

	/**
	 * @description Tracks all of the blocks that are open
	 */
	@track _openBlocks = [];

	/* ---------- GETTERS ---------- */

	/**
	 * @description Return the Plan__c Id
	 */
	@api get planId() {
		return this._planId;
	}
	
	@api get mode() {
		return this._mode;
	}

	get isActiveMode() {
		return this.mode === 'ACTIVE';
	}

	get planTableClass() {

		if(this.mode === 'PASSIVE') {
			return 'tracker-plan__table tracker-plan__passive';
		} else {
			return 'tracker-plan__table tracker-plan__active';
		}

		return 
	}
	
	get planChanges() {
		return this.getPlanDifferences();
	}

	get isPhone() {
		return FORM_FACTOR === "Small";
	}


	get hasPlanId() {
		return typeof(this._planId) != "undefined";
	}

	get haveAnyTargetsChanged() {
		return (this._origPlanData.macroTargetsChanged === true);
	}

	/* ---------- SETTERS ---------- */

	/**
	 * @description Set the Plan Id
	 */
	set planId(newPlanId) {
		this._planId	= newPlanId;

		console.log("Plan Id: ", this._planId);

		if(typeof(this._planId) != "undefined") {
			this._isLoading = true;
			this.resetValues();
			this.refreshData(() => {
				
				for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
					let sessBlock = this._updatedPlanData.blocks[i];
					if(this._openBlocks.includes(sessBlock.id) === false) {
						sessBlock.states.isDisplayingItems = true;
						this._openBlocks.push(sessBlock.id);
					}
				}

			});
		}
	}

	/**
	 * @description Set the Mode of the Tracker Plan, can be passive or active
	 */
	set mode(newMode) {
		if(MODES.includes(newMode)) {
			this._mode = newMode;

			//Fire a mode change event
			this.fireModeChangeEvent();

		} else {
			console.error("[trackerXPlan] " + newMode + " is not a valid mode");
		}
	}
	
	/* ---------- CUSTOM METHODS ---------- */

	/**
	 * @description Fix up the original data
	 */
	HACK_originalPlanData() {

		//Get the Block Ids for from the original plan
		let blockIds = [];
		for(let i = 0; i < this._origPlanData.blocks.length; i++) {
			blockIds.push(this._origPlanData.blocks[i].id);
		}

		//Update block totals
		for(let i = 0; i < blockIds.length; i++) {
			this.updateBlockQuantity(this._origPlanData, blockIds[i]);
		}

		//Update Plan data
		this.updatePlanQuantity(this._origPlanData);

	}

	/**
	 * @description Update Block Totals in Database
	 * @param {String} blockId The Id of the block we want to update
	 */
	 HACK_pushBlockTotal(blockId) {

		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessBlock = this._updatedPlanData.blocks[i];

			if(sessBlock.blockId === blockId) {
				for(let j = 0; j < sessBlock.items.length; j++) {
					let sessItem = sessBlock.items[j];

					if(this._changesToSave.blocks[sessBlock.id] === undefined) {
						this._changesToSave.blocks[sessBlock.id] = {
							items : {}
						};	
					}
					if(this._changesToSave.blocks[sessBlock.id].items[sessItem.id] === undefined && sessItem.states.isTemplateItem == false) {
						this._changesToSave.blocks[sessBlock.id].items[sessItem.id] = {};
						this.updateItemQuantity(this._updatedPlanData, sessItem.quantity, sessBlock.id, sessItem.id);
					}

				}
				this.updateBlockQuantity(this._updatedPlanData, sessBlock.blockId);
			}
		}
	 }

	/**
	 * @description Push up all value changes
	 */
	HACK_pushAllChanges() {
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessBlock = this._updatedPlanData.blocks[i];
			for(let j = 0; j < sessBlock.items.length; j++) {
				let sessItem = sessBlock.items[j];

				if(this._changesToSave.blocks[sessBlock.id] === undefined) {
					this._changesToSave.blocks[sessBlock.id] = {
						items : {}
					};	
				}
				if(this._changesToSave.blocks[sessBlock.id].items[sessItem.id] === undefined && sessItem.states.isTemplateItem == false) {
					this._changesToSave.blocks[sessBlock.id].items[sessItem.id] = {};
					this.updateItemQuantity(this._updatedPlanData, sessItem.quantity, sessBlock.id, sessItem.id);
				}

			}
			this.updateBlockQuantity(this._updatedPlanData, sessBlock.id);
		}
		this.updatePlanQuantity(this._updatedPlanData);

	}

	/** 
	 * @description Get the Differences between the original and updated plans
	 * @return {Object} Returns the differences in percentages and values for protein, carbs, fats, and calories
	 */
	getPlanDifferences() {
	
		let changes = {
			protein:	{},
			carb:		{},
			fat:		{},
			calories:	{}
		}

		let updateTotalProtein	= parseFloat(parseFloat(this._updatedPlanData.protein).toFixed(1));
		let updateTotalCarbs	= parseFloat(parseFloat(this._updatedPlanData.carbs).toFixed(1));
		let updateTotalFat		= parseFloat(parseFloat(this._updatedPlanData.fat).toFixed(1));
		let updateTotalCalories = parseFloat(parseFloat(this._updatedPlanData.calories).toFixed(1));
		
		let origTotalProtein	= parseFloat(parseFloat(this._origPlanData.protein).toFixed(1));
		let origTotalCarbs		= parseFloat(parseFloat(this._origPlanData.carbs).toFixed(1));
		let origTotalFat		= parseFloat(parseFloat(this._origPlanData.fat).toFixed(1));
		let origTotalCalories	= parseFloat(parseFloat(this._origPlanData.calories).toFixed(1));

		changes.protein.value		= (updateTotalProtein - origTotalProtein).toFixed(0);
		changes.carb.value			= (updateTotalCarbs - origTotalCarbs).toFixed(0);
		changes.fat.value			= (updateTotalFat - origTotalFat).toFixed(0);
		changes.calories.value		= (updateTotalCalories - origTotalCalories).toFixed(0);
		
		if(origTotalProtein === 0) {
			changes.protein.percent		= (updateTotalProtein / 1 * 100).toFixed(0);
		} else {
			changes.protein.percent		= (updateTotalProtein / origTotalProtein * 100).toFixed(0);
		}

		if(origTotalCarbs === 0) {
			changes.carb.percent		= (updateTotalCarbs / 1 * 100).toFixed(0);
		} else {
			changes.carb.percent		= (updateTotalCarbs / origTotalCarbs * 100).toFixed(0);
		}

		if(origTotalFat === 0) {
			changes.fat.percent			= (updateTotalFat / 1 * 100).toFixed(0);
		} else {
			changes.fat.percent			= (updateTotalFat / origTotalFat * 100).toFixed(0);
		}

		if(origTotalCalories === 0) {
			changes.calories.percent	= (updateTotalCalories / 1 * 100).toFixed(0);
		} else {
			changes.calories.percent	= (updateTotalCalories / origTotalCalories * 100).toFixed(0);
		}


		if(updateTotalProtein > origTotalProtein) {
			changes.protein.value = '+' + changes.protein.value;
			changes.protein.classValue = 'above-value';
		} else if(updateTotalProtein < origTotalProtein) {
			changes.protein.classValue = 'below-value';
		} else {
			changes.protein.classValue = 'equal-value';
		}

		if(updateTotalCarbs > origTotalCarbs) {
			changes.carb.value = '+' + changes.carb.value;
			changes.carb.classValue = 'above-value';
		} else if(updateTotalCarbs < origTotalCarbs) {
			changes.carb.classValue = 'below-value';
		} else {
			changes.carb.classValue = 'equal-value';
		}

		if(updateTotalFat > origTotalFat) {
			changes.fat.value = '+' + changes.fat.value;
			changes.fat.classValue = 'above-value';
		} else if(updateTotalFat < origTotalFat) {
			changes.fat.value = changes.fat.value;
			changes.fat.classValue = 'below-value';
		} else {
			changes.fat.classValue = 'equal-value';
		}

		if(updateTotalCalories > origTotalCalories) {
			changes.calories.value = '+' + changes.calories.value;
			changes.calories.classValue = 'above-value';
		} else if(updateTotalCalories < origTotalCalories) {
			changes.calories.classValue = 'below-value';
		} else {
			changes.calories.classValue = 'equal-value';
		}

		//Set if the changes are different
		if(changes.protein.value == 0 && changes.carb.value == 0 && changes.fat.value == 0 && changes.calories.value == 0) {
			changes.differences = false;
		} else {
			changes.differences = true;
		}

		return changes;
	}


	/**
	 * @description Fire a plan Mode change event
	 */
	fireModeChangeEvent() {
		//Fire a Mode Change Event
		let detail = { currentMode: this.mode };
        const modechangeEvent = new CustomEvent('modechange', { detail: detail });
        this.dispatchEvent(modechangeEvent);
	}

	/**
	 * @description Refresh the data for the Plan
	 */
	@api refreshData(onSuccess, onError) {

		this._isLoading = true;
		QuerySessionPlanData({
			planId: this._planId
		}).then(result => {
			this._origPlanData		= JSON.parse(JSON.stringify(result));
			this._updatedPlanData	= this.processPlan(this._origPlanData);
			console.log("[trackerXPlan] QuerySessionPlanData SUCCESS: ", JSON.parse(JSON.stringify(this._updatedPlanData)));
			GetNutritionalItems().then(result => {
				this._nutritionalItems	= this.processNutritionalItems(result);
				this._filteredItems		= JSON.parse(JSON.stringify(this._nutritionalItems));
				this._isLoading			= false;
				if(onSuccess) {
					onSuccess();
				}
			}).catch(error => {
				console.error("[trackerXPlan] GetNutritionalItems ERROR: ", JSON.parse( JSON.stringify(error) ) );
				
				if(onError) {
					onError(error);
				}

			});

		}).catch(error => {
			this._isLoading = false;
			console.error("[trackerXPlan] QuerySessionPlanData ERROR: ", JSON.parse(JSON.stringify(error)));
			
			if(onError) {
				onError(error);
			}

		});
	}

	processNutritionalItems(nutritionalItems) {
		let nutItems = nutritionalItems;
		let processedItems = [];
		for(let i = 0; i < nutItems.length; i++) {
			let itm = nutItems[i];
			processedItems.push(itm);
		}
		return processedItems;
	}

	/**
	 * @description Update the Filtered Items list
	 */
	updateFilteredItems() {

		//Get the BlockId that we are adding the items for
		let blockId = null;
		let templateIds = [];
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessionBlock = this._updatedPlanData.blocks[i];
			if(sessionBlock.states.isCreatingItem === true) {
				blockId = sessionBlock.id;

				for(let j = 0; j < sessionBlock.items.length; j++) {

					let sessionItem = sessionBlock.items[j];

					if(!templateIds.includes(sessionItem.templateItemId)) {
						templateIds.push(sessionItem.templateItemId);
					}
				}
				break;
			}
		}

		console.log("Template Ids: ", templateIds);


		let filterdItems = [];

		this._nutritionalItems.forEach(item => {

			let selected = true;
	
			//If the filter name is not correct, then remove it from the list
            if (this._filteredNameSearchParameter && (this._filteredNameSearchParameter != '') && (item.name.toLowerCase().indexOf(this._filteredNameSearchParameter.toLowerCase()) < 0)) {
				selected = false;
			} 

			if(templateIds.includes(item.id)) {
				selected = false;
			}


			if(selected === true) {
				filterdItems.push(item);
			}


		});

		this._filteredItems = filterdItems;

	}

	processPlan(originalData) {
		let processedPlan = JSON.parse(JSON.stringify(originalData));
		for(let i = 0; i < processedPlan.blocks.length; i++) {
			let sessBlk = processedPlan.blocks[i];
			//Set up some states that we can use to create items, etc.
			sessBlk.states = {
				isCreatingItem:		false,
				isDisplayingItems:	false
			};
			//If the block was previously displaying items, keep it open
			if(this._openBlocks.includes(sessBlk.id)) {
				sessBlk.states.isDisplayingItems = true;
			}
			for(let j = 0; j < sessBlk.items.length; j++) {
				let sessItem = sessBlk.items[j];
				sessItem.states = {
					isTemplateItem:		false
				};
				sessItem.values = {
					proteinPerUnit:		sessItem.referenceProtein	/ sessItem.referenceQuantity,
					carbsPerUnit:		sessItem.referenceCarbs		/ sessItem.referenceQuantity,
					fatPerUnit:			sessItem.referenceFat		/ sessItem.referenceQuantity,
					caloriesPerUnit:	sessItem.referenceCalories	/ sessItem.referenceQuantity
				}
			}
		}
		return processedPlan;
	}

	updateItemQuantityV2(planData, newQuantity, blockOrder, itemOrder) {
	
		console.log("1");

		for(let i = 0; i < planData.blocks.length; i++) {
			console.log("1-1");
			let sessionBlk = planData.blocks[i];
			console.log("1-2");
			if(sessionBlk.order === blockOrder) {
				console.log("1-2-a-1");
				for(let j = 0; j < sessionBlk.items.length; j++) {
					console.log("1-2-a-1-1");
					let sessionItm = sessionBlk.items[j];
					console.log("1-2-a-1-2");
					if(sessionItm.order === itemOrder) {
						console.log("1-2-a-1-2-a-1");
						sessionItm.quantity = newQuantity;
						sessionItm.carbs	= newQuantity * sessionItm.values.carbsPerUnit;
						sessionItm.fat		= newQuantity * sessionItm.values.fatPerUnit;
						sessionItm.protein	= newQuantity * sessionItm.values.proteinPerUnit;
						sessionItm.calories = newQuantity * sessionItm.values.caloriesPerUnit;
						console.log("1-2-a-1-2-a-2");
						
						//Save changes for session items, not template items
						if(sessionItm.states.isTemplateItem === false) {
							if(this._changesToSave.blocks[sessionBlk.id] === undefined) {
								this._changesToSave.blocks[sessionBlk.id] = {
									items : {}
								};	
							}

							if(this._changesToSave.blocks[sessionBlk.id].items[sessionItm.id] === undefined) {
								this._changesToSave.blocks[sessionBlk.id].items[sessionItm.id] = {};
							}

							this._changesToSave.blocks[sessionBlk.id].items[sessionItm.id] = {
								quantity:	newQuantity,
								carbs:		sessionItm.carbs,
								fat:		sessionItm.fat,
								protein:	sessionItm.protein,
								calories:	sessionItm.calories
							};

						}
						console.log("1-2-a-1-2-a-3");

					}
					console.log("1-2-a-1-3");
				}
				console.log("1-2-a-2");
			}
			console.log("1-3");
		}

		console.log("2");

	}

	updateItemQuantity(planData, newQuantity, blockId, itemId) {

		for(let i = 0; i < planData.blocks.length; i++) {
			let sessionBlk = planData.blocks[i];
			if(sessionBlk.id === blockId) {
				for(let j = 0; j < sessionBlk.items.length; j++) {
					let sessionItm = sessionBlk.items[j];
					if(sessionItm.id === itemId) {

						sessionItm.quantity = newQuantity;
						sessionItm.carbs	= newQuantity * sessionItm.values.carbsPerUnit;
						sessionItm.fat		= newQuantity * sessionItm.values.fatPerUnit;
						sessionItm.protein	= newQuantity * sessionItm.values.proteinPerUnit;
						sessionItm.calories = newQuantity * sessionItm.values.caloriesPerUnit;
						
						//Save changes for session items, not template items
						if(sessionItm.states.isTemplateItem === false) {
							if(this._changesToSave.blocks[blockId] === undefined) {
								this._changesToSave.blocks[blockId] = {
									items : {}
								};	
							}
							if(this._changesToSave.blocks[blockId].items[itemId] === undefined) {
								this._changesToSave.blocks[blockId].items[itemId] = {};
							}
							this._changesToSave.blocks[blockId].items[itemId] = {
								quantity:	newQuantity,
								carbs:		sessionItm.carbs,
								fat:		sessionItm.fat,
								protein:	sessionItm.protein,
								calories:	sessionItm.calories
							};
						}

					}
				}
			}
		}
	}

	/**
	 * @description Update the Block Quantity for the calories, fats, protein, and carbs
	 * 
	 * @param {Object} planData The Data we are updating the quantities for
	 * @param {String} blockId The Id of the block to identify it
	 */
	updateBlockQuantity(planData, blockId) {
		
		//Find the block we want to update
		for(let i = 0; i < planData.blocks.length; i++) {
		
			let sessionBlk = planData.blocks[i];
			
			//If we find the block, then update the protein, carbs, fat, and calories 
			if(sessionBlk.id === blockId) {
			
				let blockProtein	= 0;
				let blockCarbs		= 0;
				let blockFat		= 0;
				let blockCalories	= 0;
				
				//Use the session items in the block to update the calories
				for(let j = 0; j < sessionBlk.items.length; j++) {
					let sessionItm = sessionBlk.items[j];

					if(sessionItm.protein) {
						blockProtein += parseFloat(sessionItm.protein);
					}
					if(sessionItm.carbs) {
						blockCarbs += parseFloat(sessionItm.carbs);
					}
					if(sessionItm.fat) {
						blockFat += parseFloat(sessionItm.fat);
					}
					if(sessionItm.calories) {
						blockCalories += parseFloat(sessionItm.calories);
					}
				}
				
				sessionBlk.protein		= blockProtein;
				sessionBlk.carbs		= blockCarbs;
				sessionBlk.fat			= blockFat;
				sessionBlk.calories		= blockCalories;

				//Track these new changes, cause we might want to push the updates to the database
				if(this._changesToSave.blocks[blockId] === undefined) {
					this._changesToSave.blocks[blockId] = {
						items : {}
					};	
				}
				this._changesToSave.blocks[blockId].protein		= blockProtein;
				this._changesToSave.blocks[blockId].carbs		= blockCarbs;
				this._changesToSave.blocks[blockId].fat			= blockFat;
				this._changesToSave.blocks[blockId].calories	= blockCalories;
				
			}
			
		}
	}

	/**
	 * @description Update the Plan Quantity for the calories, fats, protein, and carbs
	 *
	 * @param {Object} planData The Plan data we want to update
	 */
	updatePlanQuantity(planData) {
		
		let planProtein		= 0;
		let planCarbs		= 0;
		let planFat			= 0;
		let planCalories	= 0;
		
		for(let i = 0; i < planData.blocks.length; i++) {
			let sessionBlk = planData.blocks[i];
		
			if(sessionBlk.protein) {
				planProtein += parseFloat(sessionBlk.protein);
			}
			if(sessionBlk.carbs) {
				planCarbs += parseFloat(sessionBlk.carbs);
			}
			if(sessionBlk.fat) {
				planFat += parseFloat(sessionBlk.fat);
			}
			if(sessionBlk.calories) {
				planCalories += parseFloat(sessionBlk.calories);
			}

		}
		
		planData.protein	= planProtein;
		planData.carbs		= planCarbs;
		planData.fat		= planFat;
		planData.calories	= planCalories;
		
		this._changesToSave.protein		= planProtein;
		this._changesToSave.carbs		= planCarbs;
		this._changesToSave.fat			= planFat;
		this._changesToSave.calories	= planCalories;

	}

	resetValues() {
		this._changesToSave = {
			blocks: {}
		};
		this._itemsToDelete = [];
	}

	
	addTemplateItem(templateItemId, blockId) {
	
		for(let i = 0; i < this._filteredItems.length; i++) {
			
			let filtItem = this._filteredItems[i];
			
			if(filtItem.id === templateItemId) {

				/*
				this.quantity	                = item.Client_Quantity__c;
				this.protein	                = item.Client_Protein__c.setScale(2);
				this.carbs		                = item.Client_Carbs__c.setScale(2);
				this.fat		                = item.Client_Fat__c.setScale(2);
				this.calories	                = item.Client_Calories__c.setScale(2);
				
				this.actualQuantity				= item.Actual_Quantity__c;
				this.actualProtein				= item.Actual_Protein__c;
				this.actualCarbs				= item.Actual_Carbs__c;
				this.actualFat					= item.Actual_Fat__c;
				this.actualCalories				= item.Actual_Calories__c;

                this.referenceId                = item.Reference_Plan_Item__c;
                this.referenceItemName          = item.Reference_Plan_Item__r.Name;
                this.referenceQuantity          = item.Trainer_Quantity__c;
                this.referenceCalories          = item.Trainer_Calories__c.setScale(2);
                this.referenceCarbs             = item.Trainer_Carbs__c.setScale(2);
                this.referenceFat               = item.Trainer_Fat__c.setScale(2);
                this.referenceProtein           = item.Trainer_Protein__c.setScale(2);
                this.referenceMeasurement       = item.Template_Measurement__c;
                this.referenceQuantityIncrement = item.Template_Quantity_Increment__c != null ? item.Template_Quantity_Increment__c : 1;
                this.referenceMinimumQuantity   = item.Template_Minimum_Quantity__c != null ? item.Template_Minimum_Quantity__c : 0;

				*/

				console.log("Selected Item: ", JSON.parse(JSON.stringify(filtItem)));

				let ratio = 1 / filtItem.quantity;

				let newItem = {
					
					blockId:					blockId,
					name:						filtItem.name,
					referenceMeasurement:		filtItem.measure,
					referenceQuantityIncrement: filtItem.quantityIncrement,
					referenceMinimumQuantity:	filtItem.minimumQuantity,

					quantity:					filtItem.quantity,
					protein:					filtItem.protein,
					carbs:						filtItem.carbs,
					fat:						filtItem.fat,
					calories:					filtItem.calories,
					
					referenceQuantity:			filtItem.quantity,
					referenceProtein:			filtItem.protein,
					referenceCarbs:				filtItem.carbs,
					referenceFat:				filtItem.fat,
					referenceCalories:			filtItem.calories,

					states: {
						isTemplateItem:		true,
					},
					values: {
						proteinPerUnit:		filtItem.protein * ratio,
						carbsPerUnit:		filtItem.carbs * ratio,
						fatPerUnit:			filtItem.fat * ratio,
						caloriesPerUnit:	filtItem.calories * ratio
					}

				};

				/*
				let newItem = {
					itemId:				filtItem.id,
					templateItemId:		filtItem.id,
					calories:			filtItem.calories,
					caloriesPerUnit:	filtItem.calories * ratio,
					carbs:				filtItem.carbs,
					carbsPerUnit:		filtItem.carbs * ratio,
					fat:				filtItem.fat,
					fatPerUnit:			filtItem.fat * ratio,
					protein:			filtItem.protein,
					proteinPerUnit:		filtItem.protein * ratio,
					measurement:		filtItem.measure,
					quantity:			filtItem.quantity,
					itemName:			filtItem.name,
					states: {
						isTemplateItem: true,
					}
				};
				*/

				for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
					let sessionBlock = this._updatedPlanData.blocks[i];
					if(sessionBlock.id === blockId) {
						newItem.order = sessionBlock.items.length;
						sessionBlock.items.push(newItem);
					}
				}

			}

		}
	}


	/* ---------- CUSTOM EVENTS ---------- */
	
	onMoveBlockUp(event) {
		console.log("Move Block Up");
	}

	onMoveBlockDown(event) {
		console.log("Move Block Down");
	}

	onMoveItemUp(event) {
		console.log("Move Item Up");
	}

	onMoveItemDown(event) {
		console.log("Move Item Down");
	}

	/**
	 * @description Executes whe the Session Block Header is Clicked
	 */
	onSessionBlockHeaderClick(event) {

		//Get the Block Id
		let blockId = event.target.dataset.blockId;
		
		//Toggle the display of items for a block
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessionBlock = this._updatedPlanData.blocks[i];
			if(sessionBlock.id === blockId) {
				if(sessionBlock.states.isDisplayingItems === true) {
					sessionBlock.states.isDisplayingItems = false;
					let removeIndex = this._openBlocks.indexOf(sessionBlock.blockId);
					this._openBlocks.splice(removeIndex, 1);
				} else {
					sessionBlock.states.isDisplayingItems = true;
					this._openBlocks.push(sessionBlock.id);
				}
			}
		}

	}

	onSaveCustomTemplateItem(event) {
		let newCustomItem	= event.detail;
		let newCustomItemId = newCustomItem.id;

		console.log("[trackerXPlan] SAVED: ", JSON.parse(JSON.stringify(newCustomItem)));

		this._isLoading			= true;

		//Find the Block id
		let blockId = null;
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessBlock = this._updatedPlanData.blocks[i];
			if(sessBlock.states.isCreatingItem === true) {
				blockId = sessBlock.id;
				sessBlock.states.isCreatingItem = false;
			}
		}

		//Refresh the filterd Items
		GetNutritionalItems().then(result => {
			this._nutritionalItems	= this.processNutritionalItems(result);
			this._filteredItems		= JSON.parse(JSON.stringify(this._nutritionalItems));

			this.addTemplateItem(newCustomItemId, blockId);

			
			this.updateBlockQuantity(this._updatedPlanData, blockId);
			this.updatePlanQuantity(this._updatedPlanData);
			
			this._isLoading			= false;
			this._addCustomItem		= false;

		}).catch(error => {
			console.error("[trackerXPlan] GetNutritionalItems ERROR: ", JSON.parse( JSON.stringify(error) ) );
		});

	}

	onCancelCustomTemplateItem(event) {
		this._addCustomItem = false;
	}

	onDeleteItem(event) {

		//Set the Mode to active
		this.mode = 'ACTIVE';

		let blockId = event.target.dataset.blockId;
		let itemId	= event.target.dataset.itemId;
		
		//Remove the Item from the Block, and tag it to be deleted in the server
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessionBlock = this._updatedPlanData.blocks[i];
			if(sessionBlock.id === blockId) {
				for(let j = 0; j < sessionBlock.items.length; j++) {
					let sessionItem = sessionBlock.items[j];
					if(sessionItem.id === itemId) {
						sessionBlock.items.splice(j, 1);

						//If the Item is still a template item, then don't worry about deleting it
						if(sessionItem.states.isTemplateItem === false) {
							this._itemsToDelete.push(sessionItem.id);
						}

					}
				}
			}
		}

		//Update the Block and Plan Quantity
		this.updateBlockQuantity(this._updatedPlanData, blockId);
		this.updatePlanQuantity(this._updatedPlanData);

		console.log("[trackerXPlan] Deleted Items: ", JSON.parse(JSON.stringify(this._itemsToDelete)));

	}

	onSetActiveAddItem(event) {
	
		//Clear all Creating Item views
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			this._updatedPlanData.blocks[i].states.isCreatingItem = false;
		}

		this.updateFilteredItems();

		//Set the Mode to active
		this.mode = 'ACTIVE';

		let blockId = event.target.dataset.blockId;
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			if(this._updatedPlanData.blocks[i].id === blockId) {
				this._updatedPlanData.blocks[i].states.isCreatingItem = true;
			}
		}
	}

	onSetInactiveAddItem(event) {
	
		//Set the Mode to active
		this.mode = 'ACTIVE';

		let blockId = event.target.dataset.blockId;
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			if(this._updatedPlanData.blocks[i].id === blockId) {
				this._updatedPlanData.blocks[i].states.isCreatingItem = false;
			}
		}
	}

	onBlockCompletedToggle(event) {

		//Get the Block Id
		let blockId = event.target.dataset.blockId;
		let state;
		
		//Toggle the display of items for a block
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessionBlock = this._updatedPlanData.blocks[i];
			if(sessionBlock.id === blockId) {

				if(sessionBlock.isCompleted === true) {
					sessionBlock.isCompleted	= false;
					state						= false;
				} else {
					sessionBlock.isCompleted	= true;
					state						= true;
				}

			}
		}
		
		this._isLoading = true;

		//If the mode is passive, set the block completed state, and then refresh all of the data
		if(this.mode === 'PASSIVE') {
			//HACK: Push the Block Totals, then update the data
			//this.HACK_pushBlockTotal(blockId);
			
			UpdateTrackerData({
				trackerData: JSON.stringify(this._changesToSave),
				planId: this._planId
			}).then(result => {
				console.log("Successfully updated Tracker data");

				this.resetValues();
				
				console.log("Block: " + blockId + " | " + state); 


				UpdateBlockCompletedState({
					blockId: blockId,
					state: state
				}).then(() => {
					console.log("Updated Block Completed State");
					//If the mode is in Passive then refresh the whole plan, however if it's active, then update just the actuals
					this.refreshData();
				}).catch(error => {
					console.error("[trackerXPlan] ERROR: ", JSON.parse(JSON.stringify(error)));
				});

			}).catch(error => {
				console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			});

		//If the mode is active, set the block completed state, and then refresh only the block that was updated
		} else if(this.mode === 'ACTIVE') {
		
			//HACK: Push the Block Totals,
			this.HACK_pushBlockTotal(blockId);
			
			//Take out just the block we want to update
			let planData	= JSON.parse(JSON.stringify(this._changesToSave));
			planData.blocks = {}
			planData.blocks[blockId] = JSON.parse(JSON.stringify(this._changesToSave.blocks[blockId]));

			console.log("Plan Data We are Updating: ", JSON.parse(JSON.stringify(this._changesToSave.blocks[blockId])))
			
			//Update the Block Data
			UpdateBlockTrackerData({
				blockId:	blockId,
				blockData:	JSON.stringify(this._changesToSave.blocks[blockId]),
			}).then(result => {
				console.log("Successfully Updated The Block " + blockId + " Data");

				//So if successful, set the state of completed
				UpdateBlockCompletedState({
					blockId:	blockId,
					state:		state
				}).then(() => {
					console.log("Updated Block " + blockId + " Completed State");

					//Query the New Plan Data
					QuerySessionPlanData({
						planId: this._planId
					}).then(result => {
					
						console.log("Queried the Plan Data: ", JSON.parse(JSON.stringify(result)));

						this._origPlanData.totalProtein			= result.totalProtein;
						this._origPlanData.totalCarbs			= result.totalCarbs;
						this._origPlanData.totalFat				= result.totalFat;
						this._origPlanData.totalCalories		= result.totalCalories;
						
						//Update the Actuals...
						this._origPlanData.actualProtein		= result.actualProtein;
						this._origPlanData.actualCarbs			= result.actualCarbs;
						this._origPlanData.actualFat			= result.actualFat;
						this._origPlanData.actualCalories		= result.actualCalories;

						//Get the plan differences, 
						let planDifferences = this.getPlanDifferences();

						console.log("Update Plan Data: ", JSON.parse(JSON.stringify(this._updatedPlanData)) );

						//If there are no more differences, and all blocks are completed, then refresh the data
						if(planDifferences.differences === false) {

							let allBlocksCompleted = true;
							for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
								let sessBlock = this._updatedPlanData.blocks[i];
								if(sessBlock.isCompleted === false) {
									allBlocksCompleted = false;
								}
							}

							if(allBlocksCompleted === true) {
								this.mode = 'PASSIVE';
								this._openBlocks = []; //Close all of the open blocks
								this.refreshData();
							} else {
								this._isLoading = false;
							}

						} else {
						
							this._isLoading = false;

						}

						

					}).catch(error => {
						console.error("[trackerXPlan] ERROR: ", JSON.parse(JSON.stringify(error)));
					});

				}).catch(error => {
					console.error("[trackerXPlan] ERROR: ", JSON.parse(JSON.stringify(error)));
				});

			}).catch(error => {
				console.error("[trackerXPlan] ERROR: ", JSON.parse(JSON.stringify(error)));
			});

			

			/* Update only the Block data*/
			/*
			UpdateTrackerData({
				trackerData: JSON.stringify(this._changesToSave),
				planId: this._planId
			}).then(result => {
				console.log("Successfully updated Tracker data");
			}
			*/


		}

		
		
		/* REAL IMPLEMENTATION HERE
		UpdateBlockCompletedState({
			blockId: blockId,
			state: state
		}).then(() => {
			console.log("Updated Block Completed State");

			this.refreshData();
		}).catch(error => {
            console.error("[trackerXPlan] ERROR: ", JSON.parse(JSON.stringify(error)));
		});
		*/
	}

	clearAddingTemplateItemViews() {

	}

	onAddTemplateItem(event) {

		//Set the Mode to active
		this.mode = 'ACTIVE';

		let templateItemId	= event.target.dataset.itemId;
		let blockId			= event.target.dataset.blockId;

		//Add the template item to the block
		this.addTemplateItem(templateItemId, blockId);
		
		this.updateBlockQuantity(this._updatedPlanData, blockId);
		this.updatePlanQuantity(this._updatedPlanData);

		//Update the Filtered Items, so we can exclude the item we just added
		this.updateFilteredItems();

	}

	onAddCustomItem(event) {
	
		//Set the Mode to active
		this.mode = 'ACTIVE';

		this._addCustomItem = true;
	}

	onItemQuantityChange(event) {
	
		//Set the Mode to active
		this.mode = 'ACTIVE';
		
		let itemId		= event.target.dataset.itemId;
		let blockId		= event.target.dataset.blockId;
		let itemOrder	= parseInt(event.target.dataset.itemOrder);
		let blockOrder	= parseInt(event.target.dataset.blockOrder);
		let newQuantity = parseFloat(event.detail.value);
		
		//Update the Item Quantity, Block Quantity, and the Plan Quantity
		this.updateItemQuantityV2(this._updatedPlanData, newQuantity, blockOrder, itemOrder);
		this.updateBlockQuantity(this._updatedPlanData, blockId);
		this.updatePlanQuantity(this._updatedPlanData);
		console.log("Changes to Save: ", JSON.parse(JSON.stringify(this._changesToSave)));

	}

	onSaveTrackerData(event) {



		//this._isLoading = true;

		//Get the New Template Items
		let newItems = [];
		for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
			let sessionBlock = this._updatedPlanData.blocks[i];
			for(let j = 0; j < sessionBlock.items.length; j++) {
				let sessionItem = sessionBlock.items[j];
				if(sessionItem.states.isTemplateItem === true) {
					newItems.push(sessionItem);
				}
			}
		}




		//AddNewSessionItems


		AddNewSessionItems({
			newItems: newItems
		}).then(result => {

			console.log("Added Template Items Successfully");

			//Update the Tracker Data
			UpdateTrackerData({
				trackerData:	JSON.stringify(this._changesToSave),
				planId:			this._planId
			}).then(result => {
				console.log("Successfully updated Tracker data");

				//Delete the Items
				DeleteItems({
					itemIds: this._itemsToDelete
				}).then(result => {
				
					console.log("Deleted Items Successfully");

					//Set the Mode to active
					this.mode = 'PASSIVE';

					//Reset the Changes, so we can make new changes
					this.resetValues();
					this.refreshData();
				}).catch(error => {
					console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
				});


			}).catch(error => {
				console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			});

		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		})

	}

	/**
	 * @description Clear the Tracker Data
	 */
	onClearTrackerData(event) {
		//Set the Mode to active
		this.mode = 'PASSIVE';
		this.resetValues();
		this.refreshData();
	}

	/**
	 * @description Filter for new Items
	 */
	onNewItemNameFilterChange(event) {
	
		//Set the Mode to active
		this.mode = 'ACTIVE';

		let searchParam						= event.target.value;
		this._filteredNameSearchParameter	= searchParam;
		this.updateFilteredItems();
	}

	/* ---------- LWC FUNCTIONS ---------- */

	connectedCallback() {
		this.isLoading = true;
	}

}