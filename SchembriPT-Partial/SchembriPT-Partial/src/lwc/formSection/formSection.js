import { LightningElement, api, track } from 'lwc';

import GetFormSectionData from '@salesforce/apex/FormController.GetFormSectionData';

export default class FormSection extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The field section name
	 */
	@track _fieldSectionName;

	/**
	 * @description If populated update this record, otherwise create the record
	 */
	@track _recordId;

	/**
	 * @description The form data for the section
	 */
	@track _formSectionData;

	@track _formFields;

	/**
	 * @description Is the section currently loading
	 */
	@track _isLoading = false;

	/* ---------- GETTERS ---------- */

	/**
	 * @description Get the Field Section Name
	 */
	@api get fieldSectionName() {
		return this._fieldSectionName;
	}

	/**
	 * @description Get the Record Id
	 */
	@api get recordId() {
		return this._recordId;
	}

	/**
	 * @description Does the form section data loaded
	 */
	get canDisplayForm() {
		return typeof this._formSectionData !== 'undefined' && typeof this._recordId !== 'undefined';
	}

	get formSectionFields() {

		let fields = [];

		for(let i = 0; i < this._formSectionData.fields.length; i++) {

			let currField = this._formSectionData.fields[i];

			if(this._formFields && currField.controllingValue && currField.controllingField && currField.controllingType && currField.controllingComparison) {
				if(this.canDisplayField(currField) === false) {
					continue;
				}
			}


			let newField = {
				label:				currField.label,
				apiName:			currField.apiName,
				editable:			currField.editable,
				required:			currField.required,
				columnWidthClass:	'slds-col slds-size_' + currField.columnWidth + '-of-12',
				fieldClass:			(currField.apiName === 'Name') ? 'hide-legend' : 'hide-label',
				fieldVariation:		(currField.apiName === 'Name') ? 'standard' : 'label-hidden'
			};

			if(currField.apiName !== 'Name' && this._formFields && this._formFields[currField.apiName] && this._formFields[currField.apiName].value) {
				newField.value = this._formFields[currField.apiName].value;
			}


			fields.push(newField);

		}

		return fields;
	}
	
	/* ---------- SETTERS ---------- */

	/**
	 * @description Set the Field Section Name
	 */
	set fieldSectionName(newValue) {

		this._isLoading = true;

		this._fieldSectionName = newValue;
		
        GetFormSectionData({
			formSectionName: this._fieldSectionName
		}).then(result => {
			console.log("Result: ", JSON.parse(JSON.stringify(result)));
			this._formSectionData	= JSON.parse(JSON.stringify(result));
			this._isLoading			= false;
        }).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			this._isLoading			= false;
        });

	}

	/**
	 * @description Set the Record Id
	 */
	set recordId(newRecordId) {
		this._recordId = newRecordId;
	}
	
	/* ---------- CUSTOM EVENTS ---------- */

	onFormSectionLoad(event) {
		console.log("Loaded Form");
		let recordDetails	= JSON.parse(JSON.stringify(event.detail.records[this._recordId]));
		if(typeof(this._formFields) === "undefined") {
			this._formFields	= JSON.parse(JSON.stringify(recordDetails.fields));
		}
	}

	onFieldUpdate(event) {
		this._formFields[event.target.dataset.apiName].value = event.target.value;
		console.log(JSON.parse(JSON.stringify(this._formFields)));

	}

	/* ---------- CUSTOM FUNCTIONS ---------- */

	canDisplayField(currField) {
		if(currField.controllingType === "String") {
			if(currField.controllingComparison === "Equal") {
				return this._formFields[currField.controllingField].value === currField.controllingValue;
			} else if(currField.controllingComparison === "Not equal") {
				return this._formFields[currField.controllingField].value !== currField.controllingValue;
			} else if(currField.controllingComparison === "Includes") {
				return this._formFields[currField.controllingField].value.includes(currField.controllingValue);
			}
		} else if(currField.controllingType === "Number") {
			if(currField.controllingComparison === "Equal") {
				return this._formFields[currField.controllingField].value === parseFloat(currField.controllingValue);
			} else if(currField.controllingComparison === "Greater than") {
				return this._formFields[currField.controllingField].value > parseFloat(currField.controllingValue);
			} else if(currField.controllingComparison === "Less than") {
				return this._formFields[currField.controllingField].value < parseFloat(currField.controllingValue);
			} else if(currField.controllingComparison === "Greater than or equal too") {
				return this._formFields[currField.controllingField].value >= parseFloat(currField.controllingValue);
			} else if(currField.controllingComparison === "Less than or equal too") {
				return this._formFields[currField.controllingField].value <= parseFloat(currField.controllingValue);
			} else if(currField.controllingComparison === "Not equal") {
				return this._formFields[currField.controllingField].value !== parseFloat(currField.controllingValue);
			}

		} else if(currField.controllingType === "Boolean") {
			if(currField.controllingComparison === "Equal") {
				return String(this._formFields[currField.controllingField].value) === currField.controllingValue;
			} else if(currField.controllingComparison === "Not equal") {
				return String(this._formFields[currField.controllingField].value) !== currField.controllingValue;
			}
		}
		return false;

	}

	/**
	 * @description Submit the Form
	 * @return {Boolean} Returns if all the field are valid
	 */
	@api Submit() {
		console.log("Submitting form");

		let allFieldsValid = this.areAllFieldsValid();

		if(allFieldsValid === true) {
			let recordEditForm = this.template.querySelector(".record-edit-form");
			recordEditForm.submit();
		}

		return allFieldsValid;

	}

	/**
	 * @description Validate the fields of a form
	 * @return {Boolean} Returns true if all fields are valid
	 */
	areAllFieldsValid() {

		let fieldsAreValid = true;

        this.template.querySelectorAll('lightning-input-field').forEach(element => {
			if(element.reportValidity() === false) {
				fieldsAreValid = false;
			}
        });

		return fieldsAreValid;

    }

}