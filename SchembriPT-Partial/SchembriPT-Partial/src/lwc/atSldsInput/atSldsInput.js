import { LightningElement, api, track } from 'lwc';

export default class AtSldsInput extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The label for the input field
	 */
	@api label = "";

	/**
	 * @description Is this field required?
	 */
	@api required = false;

	/**
	 * @description The input type
	 */
	@api type = "text";

	/**
	 * @description The variant of the input type
	 */
	@api variant = "standard";

	/**
	 * @description The value of the input field
	 */
	@api value;

	/**
	 * @description Is the Input field checked
	 */
	@api checked = false;

	/**
	 * @description The placeholder value of the input field
	 */
	@api placeholder;
	
	/**
	 * @description How is the text aligned in the input field
	 */
	@track _textAlign = "left";

	/* ---------- GETTERS ---------- */

	/**
	 * @description Returns how the text is aligned on the input field
	 */
	@api get textAlign() {
		return this._textAlign;
	}

	get _isRadioType() {
		return this.type === "radio";
	}

	/**
	 * @description Is the input field of standard variant type
	 */
	get _isStandardVariant() {
		return this.variant === "standard";
	}
	
	/**
	 * @description Is the input field of label hidden variant type
	 */
	get _isLabelHiddenVariant() {
		return this.variant === "label-hidden";
	}

    /**
     * @description Is the input field of the label inline
     */
    get _isLabelInlineVariant() {
        return this.variant === "label-inline";
    }

	get _inputClass() {
		let inputClass = "slds-input";

		if(this._textAlign === "left") {
			inputClass += " slds-text-align_left";
		} else if(this._textAlign === "middle") {
			inputClass += " slds-text-align_center";
		} else if(this._textAlign === "right") {
			inputClass += " slds-text-align_right";
		}
		return inputClass;

	}

	/* ---------- SETTERS ---------- */
	
	/**
	 * @description Returns how the text is aligned on the input field
	 */
	set textAlign(newTextAlign) {

		let textAlignOptions = [ "left", "middle", "right" ];

		//If the text align value is not valid then default to left
		if(textAlignOptions.includes(newTextAlign) === false) {
			this._textAlign = "left";
		} else {
			this._textAlign = newTextAlign;
		}

	}

	/* ---------- CUSTOM METHODS ---------- */

	/**
	 * @description Executes when the value is changed
	 */
	onValueChange(event) {
		event.preventDefault();
		this.value = event.target.value;
		this.dispatchEvent(new CustomEvent('change'));
	}

	onCheckedChange(event) {
		event.preventDefault();
		this.checked = !this.checked;
		this.dispatchEvent(new CustomEvent('change'));
	}
	
	/* ---------- CUSTOM EVENTS ---------- */

	/* ---------- LWC EVENTS ---------- */

}