import { LightningElement, api, track } from 'lwc';

import getClientSummary				from '@salesforce/apex/Planner.getClientSummary';
import GetVideoCollection			from '@salesforce/apex/AT_LC_CMSCollection.GetVideoCollection';


export default class AtCmsCollection extends LightningElement {

	/* ---------- VARIABLES ---------- */

	@track _nameSearch = "";

	@track _trainerVideos = [];

	@track _isLoading = false;

	@api primaryCategory;

	@api secondaryCategory;

	@api tertiaryCategory;

	
	/* ---------- GETTERS ---------- */
	
	/* ---------- SETTERS ---------- */
	
	/* ---------- CUSTOM METHODS ---------- */

	@api Refresh() {
		this._isLoading = true;
		GetVideoCollection({
			nameSearch:			this._nameSearch,
			primaryCategory:	this.primaryCategory,
			secondaryCategory:	this.secondaryCategory,
			tertiaryCategory:	this.tertiaryCategory
		}).then(result => {
			console.log("Result: ", JSON.parse(JSON.stringify(result)));
			this._trainerVideos = JSON.parse(JSON.stringify(result.trainingVideos));
			this._isLoading = false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/* ---------- CUSTOM EVENTS ---------- */

	/**
	 * @description Executes when the name search is updated
	 */
	onNameSearchUpdate(event) {
		this._nameSearch = event.target.value;
		this._pageNumber = 0; //If we are updating the search then start at the beginning
		this.Refresh();
	}

	/* ---------- LWC EVENTS ---------- */

	connectedCallback() {
		this.Refresh();
	}

}