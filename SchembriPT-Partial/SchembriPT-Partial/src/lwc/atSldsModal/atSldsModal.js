import { LightningElement, api, track } from 'lwc';

/**
 * @author Harrison Campbell
 * @description A Custom Ativa Component that mimics the SLDS Modal
 * @href https://www.lightningdesignsystem.com/components/modals/#Base
 */
export default class AtSldsModal extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The label of the modal header
	 */
	@api headerLabel = "???";

	/**
	 * @description The Size of the Modal
	 */
	@api size = "medium"

	/**
	 * @description Display the Header of the Modal
	 */
	@api displayHeader = false;

	/**
	 * @description Display the Footer of the Modal
	 */
	@api displayFooter = false;

	/* ---------- GETTERS ---------- */

	/**
	 * @description The class for the Modal
	 */
	get modalClass() {

		//Add the standard modal classes
		let mClass = "slds-modal slds-fade-in-open";

		//Add the correct size class
		if(this.size === "small") {
			mClass += " slds-modal_small";
		} else if(this.size === "medium") {
			mClass += " slds-modal_medium";
		} else if(this.size === "large") {
			mClass += " slds-modal_large";
		} else {
			//Default to medium size modal if the size is not valid
			mClass += " slds-modal_medium";
		}

		return mClass;
		

	}

	/**
	 * @description The class for the Modal Header
	 */
	get modalHeaderClass() {

		let mClass = "slds-modal__header";

		if(this.displayHeader === false) {
			mClass += " slds-modal__header_empty";
		}

		return mClass;
	}

	/* ---------- SETTERS ---------- */
	
	/* ---------- CUSTOM METHODS ---------- */

	/* ---------- CUSTOM EVENTS ---------- */

	/**
	 * @description Closes the modal
	 */
	onClose(event) {
        this.dispatchEvent(new CustomEvent('close', { bubbles: true, composed: true }));
	}
	 
}