import { LightningElement, api, track } from 'lwc';

export default class QuantityInput extends LightningElement {

	@track _value;

	@api measurement;

	@api disabled = false;
	
	@api get value() {
		return this._value;
	}

	set value(newValue) {
		if(this.measurement !== 'Grams') {
			this._value = parseFloat(parseFloat(newValue).toFixed(1));
		} else {
			this._value = newValue;
		}
	}

	@api name;

	@api get isGrams() {
		return this.measurement === 'Grams';
	}

	onGramInput(event) {
		this.value = parseInt(event.target.value);
	}

	fireNonGramChangeEvent(event) {

		event.preventDefault();
		
		this.value = Math.round(event.target.value * 10) / 10
		
		let detail = {
			disabled :	this.disabled,
			value :		this.value,
			name :		this.name
		};
		
		const keyupEvent = new CustomEvent('keyupupdate', { detail: detail });
		
        // Dispatches the event.
        this.dispatchEvent(keyupEvent);

	}


	fireGramChangeEvent(event) {
	
		event.preventDefault();
		
		this.value = parseInt(event.target.value);
		
		let detail = {
			disabled :	this.disabled,
			value :		this.value,
			name :		this.name
		};
		
		const keyupEvent = new CustomEvent('keyupupdate', { detail: detail });
		
        // Dispatches the event.
        this.dispatchEvent(keyupEvent);


	}


}