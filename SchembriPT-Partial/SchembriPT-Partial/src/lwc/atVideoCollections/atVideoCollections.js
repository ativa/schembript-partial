import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import FORM_FACTOR from '@salesforce/client/formFactor';

export default class AtVideoCollections extends NavigationMixin(LightningElement) {

	/* ---------- VARIABLES ---------- */

	@track _isFAQsSectionDisplayed						= false;
	@track _isExerciseProgressionSectionDisplayed		= false;
	@track _isGymDemonstrationVideosSectionDisplayed	= false;
	@track _isHomeDemonstrationVideosSectionDisplayed	= false;
	
	/* ---------- GETTERS ---------- */

	get isMobile() {
		return FORM_FACTOR === 'Medium' || FORM_FACTOR === 'Small';
	}
	
	/* ---------- SETTERS ---------- */

	/* ---------- CUSTOM METHODS ---------- */

	/* ---------- CUSTOM EVENTS ---------- */

	onToggleFaqsSection(event) {
		this._isFAQsSectionDisplayed					= !this._isFAQsSectionDisplayed;
	}

	onToggleExerciseProgressionSection(event) {
		this._isExerciseProgressionSectionDisplayed		= !this._isExerciseProgressionSectionDisplayed;
	}

	onToggleGymDemonstrationVideosSection(event) {
		this._isGymDemonstrationVideosSectionDisplayed	= !this._isGymDemonstrationVideosSectionDisplayed;
	}

	onToggleHomeDemonstrationVideosSection(event) {
		this._isHomeDemonstrationVideosSectionDisplayed = !this._isHomeDemonstrationVideosSectionDisplayed;
	}
	
	
	/* ---------- LWC EVENTS ---------- */


	onNavigateToHome(event) {
		this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: 'Home'
            },
        });
	}

}