import { api, track, LightningElement } from 'lwc';

import { NavigationMixin } from 'lightning/navigation';

import FORM_FACTOR from '@salesforce/client/formFactor'; //Tracks if Mobile or Desktop

import getFilters			from '@salesforce/apex/Planner.getFilters';

/* -------------------- TrackerController -------------------- */
import CompletePlan				from '@salesforce/apex/TrackerController.CompletePlan';
import SetPlanCompleteState		from '@salesforce/apex/TrackerController.SetPlanCompleteState';
import GetTrackerInformation	from '@salesforce/apex/TrackerController.GetTrackerInformation';

export default class Tracker extends NavigationMixin(LightningElement) {

	/* ---------- VARIABLES ---------- */
	
	/**
	 * @description Drives which tool to use when setting up modes
	 */
	@track _modeType = 'Planner';

	/**
	 * @description Has the Plan been set to completed
	 */
	@track planCompleted;

	/**
	 * @description The mode of the tracker plan ('ACTIVE', 'PASSIVE'); 
	 */
	@track trackerPlanMode = 'PASSIVE';

	/** 
	 * @description Is new plan data being loaded
	 */
    @track isLoading = true;

	/**
	 * @description Tracks if Nutrition Session Plans were not found
	 */
	@track noPlansFound = false;

	noPlanFoundOption = [ { label: 'No Plans Available', value: 'no-plans-available' } ];

	/* ---------- GETTERS ---------- */

	/**
	 * @description Is the Tracker currently in active mode
	 */
	get isInActiveMode() {
		return this.trackerPlanMode === 'ACTIVE';
	}	

	/**
	 * @description Is the current mode in planner mode
	 */
	get isInPlannerMode() {
		return this._modeType === "Planner";
	}

	/**
	 * @description Is the current mode in tracker mode
	 */
	get isInTrackerMode() {
		return this._modeType === "Tracker";
	}
	
	/* ---------- SETTERS ---------- */
	
	/* ---------- CUSTOM METHODS ---------- */
	
	/**
	 * @description Format the Date Value
	 */
    formatDate(date) {
        var year     = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
        var month    = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date);
        var day      = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
        var weekday  = new Intl.DateTimeFormat('en', { weekday: 'long' }).format(date);

        return weekday + ' ' + day + '/' + month + '/' + year;
    }

	/* ---------- CUSTOM EVENTS ---------- */
	
	/**
	 * @description Executes when a plan is selected
	 *
	 * @param {Event} event The event that triggers this
	 */
	onPlanSelection(event) {
		this.planID		= event.detail.value;
		this.isLoading	= true;
        GetTrackerInformation({
			planId: this.planID
		}).then(tracker => {
            this.processTracker(tracker);
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });
	}

	/**
	 * @description Return to the home page
	 */
    onReturnToHome(event) {
        this[NavigationMixin.Navigate](
            {
                type: 'comm__namedPage',
                attributes: {
                    name: 'Home'
                }
            }
        );
    }
	/**
	 * @description Executes when the tracker plan has the mode changed
	 */
	onTrackerModeChange(event) {
		let detail				= event.detail;
		this.trackerPlanMode	= detail.currentMode;
	}
	
	/**
	 * @description Executes when the user clicks the complete day button
	 */
	onClickCompleteDay(event) {

		this.isLoading = true;
		//Complete the currently active plan
		SetPlanCompleteState({
			planId: this.planID,
			state:	true
		}).then(result => {
			this.planCompleted 	= true;
			this.isLoading 		= false;
			this.template.querySelector(".tracker-plan").refreshData();
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Executes when the user clicks the completed button
	 */
	onClickUnCompleteDay(event) {
	
		this.isLoading = true;

		//UnComplete the currently active plan
		SetPlanCompleteState({
			planId: this.planID,
			state:	false
		}).then(result => {
			this.planCompleted 	= false;
			this.isLoading 		= false;
			this.template.querySelector(".tracker-plan").refreshData();
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});

	}

	/**
	 * @description Executes when the mode is selected
	 */
	onModeSelect(event) {
		let newMode = event.target.dataset.mode;
		console.log("New Mode: ", newMode);
		this._modeType = newMode;
	}

	/* ---------- LWC FUNCTIONS ---------- */



    @track _checkInID;
    @track editing = false;

    @track planID;
    @track planDate;

    @track nextID;
    @track previousID;

    @track hasNext = false;
    @track hasPrevious = false;

    @track dailyPlans;
    @track dailyPlanList;
    @track planSelected = false;
    @track selectedDailyPlanID;
    @track selectedDailyPlan;

    @track clientAllergies = [];
    @track clientRestrictions = [];

    @track allergies;
    @track selectedAllergies = [];
    @track restrictions;
    @track selectedRestrictions = [];

    @track totals;

	/**
	 * @description A list of Plans that the user can navigate too
	 */
	@track planOptions = [];


    constructor() {
        super();
        getFilters().then(filters => {
            if (filters.allergies) {
                this.allergies = filters.allergies.map(a => {return {name: a, checked: false}});
            }
            if (filters.restrictions) {
                this.restrictions = filters.restrictions.map(r => {return {name: r, checked: false};});
            }
        }).catch(error => {
            console.log(JSON.stringify(error));
        });
}

    get isMobile() {
		return (FORM_FACTOR === 'Medium' || FORM_FACTOR === 'Small');
	}

    @api get checkInID() {
        return this._checkInID;
    }

    /**
     *
     */
    @api Refresh() {

        //Get the Starting plan for the tracker
        GetTrackerInformation().then(tracker => {
            console.log("[tracker] Tracker Data: ", JSON.parse(JSON.stringify(tracker)));
            this.processTracker(tracker);
        }).catch(error => {
            console.error("ERROR:", JSON.parse(JSON.stringify(error)));
        });

    }

    onTrackerPlannerUpdate() {
        console.log("Updating Tracker Information");
        this.Refresh();
    }

	/**
	 * @description Set the Check-In Id
	 * @param {String} id The Check-In Id
	 */
    set checkInID(id) {
		
		//Set the Check-In Id
        this._checkInID = id;

        this.Refresh();
       
        getCheckInInfo({
            checkInID: this._checkInID
        }).then(info => {

            console.log('Info:', info);
            this.clientAllergies        = info.allergies;
            this.clientRestrictions     = info.restrictions;

            this.selectedAllergies      = JSON.parse(JSON.stringify(info.allergies));
            this.selectedRestrictions   = JSON.parse(JSON.stringify(info.restrictions));

            this.allergies.forEach(a => {
                if (this.clientAllergies.indexOf(a.name) >= 0) a.checked = true;
            });

            this.restrictions.forEach(r => {
                if (this.clientRestrictions.indexOf(r.name) >= 0) r.checked = true;
            });

        }).catch(error => {
            console.log(JSON.stringify(error));
        });
    }

    previousPlan(event) {
        if (this.previousID) {
            this.isLoading = true;
            GetTrackerInformation({
				planId: this.previousID
			}).then(tracker => {
                this.processTracker(tracker);
			}).catch(error => {
                console.log(JSON.stringify(error));
            });
        } else {
            console.log('No previous plan');
        }
    }

    nextPlan(event) {
        if (this.nextID) {
            this.isLoading = true;
            GetTrackerInformation({
				planId: this.nextID
			}).then(tracker => {
                this.processTracker(tracker);
            }).catch(error => {
                console.log(JSON.stringify(error));
            });

        } else {
            console.log('No next plan');
        }
    }

	/**
	 * @description Process the tracker data returned, including the current plan information
	 * @param {*} tracker The Tracker information
	 */
    processTracker(tracker) {

		console.log("Process Tracker: ", JSON.parse(JSON.stringify(tracker)));

		this.noPlansFound = tracker.noPlansFound;

		//this.noPlansFound = true;
        if (this.noPlansFound === false) {

			this.planID			= tracker.currentPlanId;
			this.planSelected	= true;
			this.planDate		= tracker.currentPlanDate;
            this.hasNext		= (tracker.nextPlanId != undefined && tracker.nextPlanId != null);
            this.hasPrevious	= (tracker.previousPlanId != undefined && tracker.previousPlanId != null);
            this.nextID			= tracker.nextPlanId;
            this.previousID		= tracker.previousPlanId;
			
			this.planCompleted	= tracker.currentPlanIsCompleted;
			
			//Set up the navigation
			let newPlanNavigation = [];
			for(let i = 0; i < tracker.availablePlans.length; i++) {
				let pNav = tracker.availablePlans[i];
				newPlanNavigation.push({
					label: this.formatDate(new Date(pNav.planDate)),
					value: pNav.planId
				});
			}
			this.planOptions = newPlanNavigation;
        }
        this.isLoading = false;
    }

}