import { LightningElement, api } from 'lwc';

export default class FormatNumber extends LightningElement {

	@api value;

	@api digits = 0;

	@api get isZero() {
		return this.value === 0;
	}

}