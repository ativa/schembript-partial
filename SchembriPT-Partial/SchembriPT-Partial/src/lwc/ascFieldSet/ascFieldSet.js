import { LightningElement, api, track } from 'lwc';
import getData from '@salesforce/apex/FieldSetView.getData';

export default class AscFieldSet extends LightningElement {

	/* --------------- CUSTOM VARIABLES --------------- */

	/**
	 * @description The Id of the record that is using the field set
	 */
	@api recordId;

	/**
	 * @description The name of the object we are querying
	 */
	@api objectApiName; 

	/**
	 * @description The name of the fieldset
	 */
	@api fieldSetName;

	/**
	 * @description The title to display at the top
	 */
	@api title;

	@api titleColor = '#000000';

	@api titleWeight = 'normal';

	@api titleSize = 18;

	/**
	 * @description If true display the title
	 */
	@api showTitle = false;

	/**
	 * @description The number of columns to use
	 */
	@api columns = 1;

	/**
	 * @description Is the Field Set Editable
	 */
	@api editable = false;

	@api labelPosition = 'label-stacked';

	/**
	 * @description If true display the Save button at the bottom of the form
	 */
	@api displaySaveButton = false;

	/**
	 * @description Tracks if we are saving the Form
	 */
	@track _isSaving = false;

	@track _fields = [];

	/**
	 * @description Code to execute when the submission was a success
	 */
	_onSuccess;

	/**
	 * @description Code to execute when the submission was unsuccessful
	 */
	_onError;

	
	/* --------------- GETTERS --------------- */

	get displayForm() {

		console.log("Display Form: ", {
			recordId:		this.recordId,
			objectApiName:	this.objectApiName,
			fieldSetName:	this.fieldSetName
		});

		return this.recordId && this.objectApiName && this.fieldSetName;
	}
	
	get gridColumnClass() {
		return "slds-col slds-size_1-of-" + this.columns;
	}

	/* --------------- SETTERS --------------- */
	
	/* --------------- CUSTOM METHODS --------------- */
	
	/**
	 * @description Submit the Form
	 */
	@api Submit(onSuccess, onError) {

		let form = this.template.querySelector('.field-set__form');

		if(onSuccess) {
			this._onSuccess = onSuccess;
		}
		if(onError) {
			this._onError = onError;
		}
		
		this._isSaving = true;
		form.submit();

	}

	loadFields() {
		getData({
            recordID:     this.recordId,
            fieldSetName: this.fieldSetName
        }).then(result => {
			console.log("[ascFieldSet] Load Fields RESULT", JSON.parse(JSON.stringify(result)));

			//this._fields = result.fields;
			this._fields = result.fieldData;

		}).catch(error => {
			console.error("[ascFieldSet] Load Fields ERROR", JSON.parse(JSON.stringify(error)));
		});
	}

	/* --------------- CUSTOM EVENTS --------------- */
	
	onFormSubmit(event) {

		this._isSaving = true;
	}
	
	onFormSuccess(event) {
		if(this._onSuccess) {
			this._onSuccess(event.detail);
		}

		this._isSaving = false;

	}
	
	onFormError(event) {
		if(this._onError) {
			this._onError(event.detail);
		}
		this._isSaving = false;
	}

	/* --------------- LWC EVENTS --------------- */

	connectedCallback() {
		this.loadFields();
	}

}