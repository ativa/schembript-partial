import { LightningElement, api, track }		from 'lwc';
import { ShowToastEvent }					from 'lightning/platformShowToastEvent';

import GetPlanDetails						from '@salesforce/apex/AT_LWCC_PlannerSavePlanAsTemplate.GetPlanDetails';
import SaveAsTemplatePlan					from '@salesforce/apex/AT_LWCC_PlannerSavePlanAsTemplate.SaveAsTemplatePlan';
import SaveAsTemplateBlocks					from '@salesforce/apex/AT_LWCC_PlannerSavePlanAsTemplate.SaveAsTemplateBlocks';

export default class PlannerXSaveReferenceAsTemplate extends LightningElement {

	/* --------------- VARIABLES --------------- */
	
	/**
	 * @description The Save options for the Plan/Blocks
	 */
	saveOptions = [
        { 
			'label': 'Save Plan with Blocks', 
			'value': 'PlanAndBlocks' 
		},
        { 
			'label': 'Specific Blocks', 
			'value': 'Blocks' 
		}
    ];
	
	/**
	 * @description The Id of the plan
	 */
	@track _planId;

	/**
	 * @description How is the plan being saved
	 */
	@track saveType = 'PlanAndBlocks';

	/**
	 * @description Is the data being saved
	 */
	@track _isSaving = false;

	/**
	 * @description Is the data being loaded
	 */
	@track _isLoading = false;

	/**
	 * @description The plan data
	 */
	@track _planData;

	/**
	 * @description A list of selected blocks
	 */
	@track _selectedBlocks = [];

	/* --------------- GETTERS --------------- */

	/**
	 * @description Is the data being saved or loaded
	 */
	get _isLoadingOrSaving() {
		return this._isLoading === true || this._isSaving === true;
	}

	/**
	 * @description The Id of the Plan
	 */
	@api get planId() {
		return this._planId;
	}

	/**
	 * @description Returns the blocks displayed in the plan data
	 */
	get displayedBlocks() {
		let refBlocks = [];
		for(let i = 0; i < this._planData.blocks.length; i++) {
			let newRefBlk = {
				blockId:			this._planData.blocks[i].id,
				blockName:			this._planData.blocks[i].name,
				templateBlockName:	this._planData.blocks[i].templateBlockName,
				templateShortName:	this._planData.blocks[i].templateShortName
			}
			newRefBlk.selected = this._selectedBlocks.includes(newRefBlk.id);
			refBlocks.push(newRefBlk);
		}
		return refBlocks;
	}

	/**
	 * @description If we are saving blocks, then display the select option for the blocks
	 */
	get displaySelectOption() {
		return this.saveType === "Blocks";
	}
	
	/* --------------- SETTERS --------------- */

	/**
	 * @description Set the Plan Id
	 *
	 * @param {String} newPlanId The new plan Id
	 */
	set planId(newPlanId) {

		this._planId	= newPlanId;
		this._isLoading = true;

		//Refresh the data when the plan Id is set
		this.refreshData(
			(result) => {
				this._isLoading = false;
			},(error) => {
				console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
				this.displayErrorToastMessage("Error Occured", "Couldn't retrieve Plan Data");
			}
		);

	}

	/*  --------------- CUSTOM METHODS --------------- */

	/**
	 * @description Display an error message to the user
	 */
	displayErrorToastMessage(title, message) {
		const event = new ShowToastEvent({
			title:		title,
			message:	message,
			variant:	'error'
		});
		this.dispatchEvent(event);
	}

	/**
	 * @description Refresh the Data
	 *
	 * @param {Function} onSuccess Executes if the data is returned successfully
	 * @param {Function} onError Executes if the data has errored
	 */
	@api refreshData(onSuccess, onError) {
		GetPlanDetails({
			planId: this._planId
		}).then(result => {
			this._planData = this.processData(JSON.parse(JSON.stringify(result)));
			console.log("Plan Details: ", JSON.parse(JSON.stringify(this._planData)));
			if(onSuccess) {
				onSuccess(this._planData);
			}
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});


	}

	/**
	 * @description Process data
	 */
	processData(data) {
		let newData = JSON.parse(JSON.stringify(data));
		for(let i = 0; i < newData.blocks.length; i++) {
			newData.blocks[i].templateBlockName	= newData.blocks[i].name;
			//If there is no short name, use the block name
			if(newData.blocks[i].shortName) {
				newData.blocks[i].templateShortName = newData.blocks[i].shortName;
			} else {
				newData.blocks[i].templateShortName = newData.blocks[i].name;
			}
		}
		return newData;
	}

	/**
	 * @description Setup the Template Plan Data packet
	 */
	_setupTemplatePlanData() {
		
		let tmpPlanData = {
			nutritionId:	this._planId,
			templateName:	this._planData.name,
			templateBlocks: []
		};

		for(let i = 0; i < this._planData.blocks.length; i++) {
			tmpPlanData.templateBlocks.push({
				nutritionId:		this._planData.blocks[i].id,
				templateName:		this._planData.blocks[i].templateBlockName,
				templateShortName:	this._planData.blocks[i].templateShortName
			});
		}

		return tmpPlanData;

	}

	/**
	 * @description Setup the Template Blocks Data packet
	 */
	_setupTemplateBlockData() {
		let  tmpPlanBlockData = [];
		for(let i = 0; i < this._planData.blocks.length; i++) {
			if(this._selectedBlocks.includes(this._planData.blocks[i].id) === true) {
				tmpPlanBlockData.push({
					nutritionId:		this._planData.blocks[i].id,
					templateName:		this._planData.blocks[i].templateBlockName,
					templateShortName:	this._planData.blocks[i].templateShortName
				});
			}
		}
		return tmpPlanBlockData;
	}
	
	_savePlanAsTemplate(onSuccess, onError) {
		let templatePlanData = this._setupTemplatePlanData();

		console.log("Template Plan Data: ", JSON.parse(JSON.stringify(templatePlanData)));

		SaveAsTemplatePlan({
			templatePlan: templatePlanData
		}).then(result => {
			console.log("Successfully saved as Template Plan");
			if(onSuccess) {
				onSuccess();
			}
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});
	}

	_saveBlocksAsTemplate(onSuccess, onError) {
		let templateBlockData = this._setupTemplateBlockData();
		SaveAsTemplateBlocks({
			templateBlocks: templateBlockData
		}).then(result => {
			console.log("Successfully saved Template Blocks");
			if(onSuccess) {
				onSuccess();
			}
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});
	}


	/**
	 * @description Save Plan as Template Plan
	 *
	 * @param {Function} onSuccess Executes when the save was successful
	 * @param {Function} onError Executes when the plan save failed
	 */
	@api Save(onSuccess, onError) {
	
		this._isSaving = true;

		console.log("Started Save");

		if(this.saveType === "PlanAndBlocks") {

			this._savePlanAsTemplate(() => {
				this._isSaving = false;
				if(onSuccess) {
					onSuccess();
				}
			}, (error) => {
				this._isSaving = false;
				console.error("ERROR [plannerXSavePlanAsTemplate] : ", JSON.parse(JSON.stringify(error)) );
				this.displayErrorToastMessage('Error Occured', 'Could not Save Successfully');
				if(onError) {
					onError(error);
				}
			});

		} else if(this.saveType === "Blocks") {
			this._saveBlocksAsTemplate(() => {
				this._isSaving = false;
				if(onSuccess) {
					onSuccess();
				}
			}, (error) => {
				this._isSaving = false;
				console.error("ERROR [plannerXSavePlanAsTemplate] : ", JSON.parse(JSON.stringify(error)) );
				this.displayErrorToastMessage('Error Occured', 'Could not Save Successfully');
				if(onError) {
					onError(error);
				}
			});
		}

	}
	
	/*  --------------- CUSTOM EVENTS --------------- */

	onPlanNameChange(event) {
		this._planData.name = event.target.value;
		console.log("Plan Data: ", JSON.parse(JSON.stringify(this._planData)));
	}

	onPlanSaveTypeChange(event) {
		this.saveType = event.target.value;
		console.log("Plan Data: ", JSON.parse(JSON.stringify(this._planData)));
	}

	onBlockSelect(event) {
		if(event.target.checked) {
			this._selectedBlocks.push(event.target.name);
		} else {
			let oldSelectedBlocks = JSON.parse(JSON.stringify(this._selectedBlocks));
			for(let i = 0; i < oldSelectedBlocks.length; i++) {
				if(oldSelectedBlocks[i] === event.target.name) {
					oldSelectedBlocks.splice(i, 1);
				}
			}
			this._selectedBlocks = JSON.parse(JSON.stringify(oldSelectedBlocks));
		}
		console.log("Plan Data: ", JSON.parse(JSON.stringify(this._planData)));
		console.log("Selected Blocks: ", JSON.parse(JSON.stringify(this._selectedBlocks)));
	}

	onTemplateBlockNameChange(event) {
		let blockId					= event.target.name;
		let newTemplateBlockName	= event.target.value;
		for(let i = 0; i < this._planData.blocks.length; i++) {
			if(this._planData.blocks[i].id === blockId) {
				this._planData.blocks[i].templateBlockName = newTemplateBlockName;
			}
		}
		console.log("Plan Data: ", JSON.parse(JSON.stringify(this._planData)));
	}

	onTemplateShortNameChange(event) {
		let blockId					= event.target.name;
		let newTemplateShortName	= event.target.value;
		for(let i = 0; i < this._planData.blocks.length; i++) {
			if(this._planData.blocks[i].id === blockId) {
				this._planData.blocks[i].templateShortName = newTemplateShortName;
			}
		}
		console.log("Plan Data: ", JSON.parse(JSON.stringify(this._planData)));
	}

}