import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR						from '@salesforce/client/formFactor';

import SaveNewVersion				from '@salesforce/apex/AT_LWCC_TrackerViewTemplatePool.SaveNewVersion';
import RefreshLatestPoolPlanVersion from '@salesforce/apex/AT_LWCC_TrackerViewTemplatePool.RefreshLatestPoolPlanVersion';
import DeletePoolPlan               from '@salesforce/apex/AT_LWCC_TrackerViewTemplatePool.DeletePoolPlan';
import SaveAsNewPoolPlan			from '@salesforce/apex/AT_LWCC_TrackerViewTemplatePool.SaveAsNewPoolPlan';

import GetData						from '@salesforce/apex/AT_LWCC_TrackerViewTemplatePool.GetData';

export default class TrackerXViewTemplatePool extends LightningElement {

	/* ---------- VARIABLES ---------- */

    /**
     * @description The Id of the Check-In
     */
	@track _checkInId;

	/**
	 * @description Holds the Check-In information
	 */
	@track _checkInInformation;

	/**
	 * @description The list of the latest pool plan versions
	 */
	@track _latestPoolPlanVersions = [];

	/**
	 * @description The Id of the selected version
	 */
    @track _selectedVersionId;

	@track _newPlanName;
	
	@track _assignedValues = [];

	/**
	 * @description Display the Edit Plan Modal
	 */
    @track _displayEditPlanModal = false;

	/**
	 * @description Display the Manage Versions Modal
	 */
    @track _displayManageVersionsModal = false;

	/**
	 * @description Display the Save new Pool Plan Modal
	 */
	@track _displaySaveNewPoolPlan = false;

	/**
	 * @description Display the delete pool plan modal
	 */
	@track _displayDeletePoolPlan = false;

	/**
	 * @description Display the assign to days for the pool plan
	 */
	@track _displayAssignToDaysPlan = false;

    @track _displayViewPlanModal = false;

	@track _selectedPlanDetails;

	/* ---------- GETTERS ---------- */

	get isMobile() {
		return FORM_FACTOR !== "Large";
	}

	@api get checkInId() {
		return this._checkInId;
	}

    get selectedPlanDetails() {
        return this.getPoolPlanVersionDetails(this._selectedVersionId);
    }

	get assignToDayOptions() {
		let options = [];

		if(this._checkInInformation.nutritionPlanningMode === "Flexi Plans") {
			options.push({ label: "Monday",		variant: this._assignedValues.includes("Monday") ? "brand" : "neutral",		value: "Monday" });
			options.push({ label: "Tuesday",	variant: this._assignedValues.includes("Tuesday") ? "brand" : "neutral",	value: "Tuesday" });
			options.push({ label: "Wednesday",	variant: this._assignedValues.includes("Wednesday") ? "brand" : "neutral",	value: "Wednesday" });
			options.push({ label: "Thursday",	variant: this._assignedValues.includes("Thursday") ? "brand" : "neutral",	value: "Thursday" });
			options.push({ label: "Friday",		variant: this._assignedValues.includes("Friday") ? "brand" : "neutral",		value: "Friday" });
			options.push({ label: "Saturday",	variant: this._assignedValues.includes("Saturday") ? "brand" : "neutral",	value: "Saturday" });
			options.push({ label: "Sunday",		variant: this._assignedValues.includes("Sunday") ? "brand" : "neutral",		value: "Sunday" });
		} else if(this._checkInInformation.nutritionPlanningMode === "Guided Plans") {			
			let weekCount = parseInt(this._checkInInformation.guidedPlanWeek.replace("Week ", ""));
			for(let i = 1; i <= weekCount; i++) {
				options.push({ label: "Week " + i, variant: this._assignedValues.includes("Week " + i) ? "brand" : "neutral", value: "Week " + i });
			}
		}

		return options;
	}

	get clientPoolPlanVersions() {
		
		let poolPlanVersions = [];

		for(let i = 0; i < this._latestPoolPlanVersions.length; i++) {
			if(this._latestPoolPlanVersions[i].states.isNutritionClientPlan === true) {
				poolPlanVersions.push(JSON.parse(JSON.stringify(this._latestPoolPlanVersions[i])));
			}
		}

		return poolPlanVersions;

	}

	get referencePoolPlanVersions() {
	
		let poolPlanVersions = [];

		for(let i = 0; i < this._latestPoolPlanVersions.length; i++) {
			if(this._latestPoolPlanVersions[i].states.isNutritionReferencePlan === true) {
				poolPlanVersions.push(JSON.parse(JSON.stringify(this._latestPoolPlanVersions[i])));
			}
		}

		return poolPlanVersions;

	}

    
	/* ---------- SETTERS ---------- */

	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		this.Refresh();
	}
	
	/* ---------- CUSTOM METHODS ---------- */

    processPoolPlanVersions(poolPlanVersions) {

        let latestVersions = [];

        for(let i = 0; i < poolPlanVersions.length; i++) {

            let updatedVersion = JSON.parse(JSON.stringify(poolPlanVersions[i]));
            updatedVersion.states = {
                isNutritionReferencePlan:   updatedVersion.planRecordTypeName === "Nutrition_Reference_Plan",
                isNutritionClientPlan:      updatedVersion.planRecordTypeName === "Nutrition_Client_Plan"

            };
            latestVersions.push(updatedVersion);


        }

        return latestVersions;

    }

	@api Refresh(onSuccess, onError) {

		console.log("Refreshing...");

		GetData({
			checkInId: this._checkInId
		}).then(result => {
			this._latestPoolPlanVersions	= this.processPoolPlanVersions(result.latestPoolPlanVersions);
			this._checkInInformation		= JSON.parse(JSON.stringify(result.checkInInformation));
            console.log("Pool Plan Versions: ", JSON.parse(JSON.stringify(this._latestPoolPlanVersions)));

			if(onSuccess) {
				onSuccess(result);
			}
			
			//Fire a refresh event
			this.dispatchEvent(new CustomEvent('refresh'));

		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});
	}

    getPoolPlanVersionDetails(poolPlanVersionId) {
        for(let i = 0; i < this._latestPoolPlanVersions.length; i++) {
            if(this._latestPoolPlanVersions[i].id === poolPlanVersionId) {
                return this._latestPoolPlanVersions[i];
            }
        }
    }

	/* ---------- CUSTOM EVENTS ---------- */

    onOpenEditPlanModal(event) {
        this._selectedVersionId		= event.target.dataset.poolPlanVersionId;
		this._selectedPlanDetails	= this.getPoolPlanVersionDetails(this._selectedVersionId);
        console.log("Selected Plan Id: ", this._selectedVersionId);
        this._displayEditPlanModal = true;
    }

    onCloseEditPlanModal(event) {
        this._displayEditPlanModal = false;
    }

    onSavePlan(event) {
        let nutCarbCyclePlan = this.template.querySelector(".nutrition-carb-cycle-plan");
        nutCarbCyclePlan.Save(() => {
            this.Refresh();
            this._displayEditPlanModal = false;
        }, (error) => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });
    }

    onActivateNewPlan(event) {

        let nutCarbCyclePlan = this.template.querySelector(".nutrition-carb-cycle-plan");
        nutCarbCyclePlan.SaveAs((result) => {

            SaveNewVersion({
                poolPlanId: this._selectedPlanDetails.poolPlanId,
                newPlanId:  result.newPlanId
            }).then(result => {

                this.Refresh();
                this._displayEditPlanModal = false;

            }).catch(error2 => {
                console.error("Error: ", JSON.parse(JSON.stringify(error2)));
            });

        }, (error) => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });

    }

    onPoolPlanManageVersions(event) {
        this._selectedVersionId             = event.target.dataset.poolPlanVersionId;
		this._selectedPlanDetails			= this.getPoolPlanVersionDetails(this._selectedVersionId);
        this._displayManageVersionsModal    = true;
    }

    onOpenSaveAsNewPoolPlanModal(event) {   
		this._selectedVersionId             = event.target.dataset.poolPlanVersionId;
		this._selectedPlanDetails			= this.getPoolPlanVersionDetails(this._selectedVersionId);
		this._newPlanName					= this._selectedPlanDetails.name;
		this._displaySaveNewPoolPlan		= true;
        console.log("Save as a new Plan: " + poolPlanVersionId);
    }

	onUpdateNewPlanName(event) {
		this._newPlanName = event.target.value;
	}

	onCloseSaveAsNewPoolPlanModal(event) {
		this._displaySaveNewPoolPlan		= false;
	}

	onSaveNewPoolPlan(event) { 
		console.log("Save new Pool Plan");
		SaveAsNewPoolPlan({
			poolPlanVersionId:	this._selectedVersionId,
			newPlanName:		this._newPlanName
		}).then(result => {
			this._displaySaveNewPoolPlan		= false;
            this.Refresh();
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	onOpenDeletePoolPlanModal(event) {
		this._displayDeletePoolPlan			= true;
		this._selectedVersionId				= event.target.dataset.poolPlanVersionId;
		this._selectedPlanDetails			= this.getPoolPlanVersionDetails(this._selectedVersionId);
	}

	onCloseDeletePoolPlanModal(event) {
		this._displayDeletePoolPlan = false;
	}

    onDeletePoolPlan(event) {
        let poolPlanVersion = this._selectedPlanDetails;
        DeletePoolPlan({
            poolPlanId: poolPlanVersion.poolPlanId
        }).then(result => {
            console.log("Deleted Plan");
            this.Refresh();
			this._displayDeletePoolPlan = false;
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });

    }

    onCloseManageVersionsModal(event) {
        this._displayManageVersionsModal    = false;
    }

	onOpenAssignToDaysModal(event) {
		this._displayAssignToDaysPlan	= true;
		this._selectedVersionId			= event.target.dataset.poolPlanVersionId;
		this._selectedPlanDetails		= this.getPoolPlanVersionDetails(this._selectedVersionId);
		this._assignedValues = [];
	}

	onCloseAssignToDaysModal(event) {
		this._displayAssignToDaysPlan	= false;
		this._assignedValues = [];
	}

	onAssignDaysToPlan(event) {
		
		let poolPlanVersion = this._selectedPlanDetails;

		let packet = {
			poolPlanId: poolPlanVersion.poolPlanId,
			assign:		JSON.parse(JSON.stringify(this._assignedValues))
		}
		
        // Creates the event with the contact ID data.
        const assignEvent = new CustomEvent('assign', { detail: packet });
        // Dispatches the event.
        this.dispatchEvent(assignEvent);
		this._displayAssignToDaysPlan	= false;
		this._assignedValues = [];

	}

	onPoolPlanVersionRefresh(event) {
        this.Refresh();
	}

    onOpenViewPlanModal(event) {
        this._displayViewPlanModal  = true;
        this._selectedVersionId     = event.target.dataset.poolPlanVersionId;
        this._selectedPlanDetails   = this.getPoolPlanVersionDetails(this._selectedVersionId);
    }

    onCloseViewPlanModal(event) {
        this._displayViewPlanModal  = false;
    }

	/* ---------- LWC EVENTS ---------- */

	onSelectAssignToDay(event) {
		let value = event.target.value;
		if(this._assignedValues.includes(value) === false) {
			this._assignedValues.push(value);
		} else {
			for(let i = 0; i < this._assignedValues.length; i++) { 
				if(this._assignedValues[i] === value) {
					this._assignedValues.splice(i, 1);
				}
			}
		}
	}

}