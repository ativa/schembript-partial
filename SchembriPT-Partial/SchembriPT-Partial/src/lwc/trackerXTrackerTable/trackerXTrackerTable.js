import { LightningElement, api, track } from 'lwc';
import FORM_FACTOR						from '@salesforce/client/formFactor';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

import SaveClientComments			from '@salesforce/apex/AT_LWCC_TrackerTargetTable.SaveClientComments';
import GetData                      from '@salesforce/apex/AT_LWCC_TrackerTargetTable.GetData';
import SaveAsNewPoolPlan	        from '@salesforce/apex/AT_LWCC_TrackerTargetTable.SaveAsNewPoolPlan';
import DeletePoolPlan		        from '@salesforce/apex/AT_LWCC_TrackerTargetTable.DeletePoolPlan';
import SaveNewVersion		        from '@salesforce/apex/AT_LWCC_TrackerTargetTable.SaveNewVersion';
import AssignPoolPlanToTargets      from '@salesforce/apex/AT_LWCC_TrackerTargetTable.AssignPoolPlanToTargets';
import UnassignPoolPlanFromTarget   from '@salesforce/apex/AT_LWCC_TrackerTargetTable.UnassignPoolPlanFromTarget';
import IsPlanNameUsed				from '@salesforce/apex/AT_LWCC_TrackerTargetTable.IsPlanNameUsed';

export default class TrackerXTrackerTable extends LightningElement {

    /* ---------- VARIABLES ---------- */

	/**
	 * @description Is data being saved
	 */
	@track _isSaving = false;

	/**
	 * @description Is data being loaded
	 */
	@track _isLoading = false;

    /**
     * @description A list of nutritional targets
     */
    @track _targets = [];

    /**
     * @description The Id of the Check-In
     */
    @track _checkInId;

    @track _checkInNutritionPlanningMode;

	@track _newPlanName;

	@track _displaySaveAsNewPoolPlanModal = false;

	@track _displayViewPlanModal = false;
	
	@track _displayEditPlanModal = false;

	@track _displayDeletePoolPlanModal = false;

	@track _displayManageVersionsModal = false;

    @track _displayAssignPoolPlanToTargetsModal = false;

	@track _selectedReferencePlanId;

	@track _selectedVersionId;

	@track _selectedPoolPlanId;

    @track _selectedTargetId;

	@track _selectedTargetIds;
   
    /**
     * @description A list of all the Client Pool Plans
     */
    @track _clientPoolPlans = [];

    /**
     * @description A list of Targets that new Pool Plans can be assigned too
     */
    @track _assignedTargetOptions = [];

    /**
     * @description A list of Targets Ids for when creating a new pool plan
     */
    @track _selectedAssignedTargetIds = [];

    /**
     * @description A list of unassigned plans
     */
    @track _unassignedPlans = [];
	
    @track _displayUnassignPoolPlanFromTargetModal = false;
    
    /* ---------- GETTERS ---------- */
	
	get isSavingOrLoading() {
		return this._isLoading === true || this._isSaving === true;
	}

	get isMobile() {
		return FORM_FACTOR !== "Large";
	}

    @api get checkInId() {
        return this._checkInId;
    }

    get isFlexiPlanMode() {
        return this._checkInNutritionPlanningMode === "Flexi Plans";
    }

    get isGuidedPlanMode() {
        return this._checkInNutritionPlanningMode === "Guided Plans";
    }

    get _selectedTargetOptions() {
        let options = [];

        for(let i = 0; i < this._assignedTargetOptions.length; i++) {
            let nOption         = JSON.parse(JSON.stringify(this._assignedTargetOptions[i]));
            nOption.selected    = this._selectedAssignedTargetIds.includes(nOption.value);
            options.push(nOption);
        }

        return options;
    }

    /* ---------- SETTERS ---------- */

    set checkInId(newCheckInId) {
        this._checkInId = newCheckInId;
        this.Refresh();
    }

    /* ---------- CUSTOM METHODS ---------- */

    @api Refresh(onSuccess, onError) {

		this._isLoading = true;

        GetData({
            checkInId: this._checkInId
        }).then(result => {

            console.log("RESULT: ", JSON.parse(JSON.stringify(result)))

            this._targets                       = this._processTargets(result.targets);
            this._unassignedPlans               = this._processUnassignedPlans(result.unassignedPoolPlans);
            this._assignedTargetOptions = [];

            console.log("Targets: ", JSON.parse(JSON.stringify(this._targets)));

            for(let i = 0 ; i < this._targets.length; i++) {
                this._assignedTargetOptions.push({
                    label:          this._targets[i].name,
                    assignTo:       this._targets[i].values.assignedToString,
                    value:          this._targets[i].id,
                    protein:        this._targets[i].protein,
                    carbs:          this._targets[i].carbs,
                    fat:            this._targets[i].fat,
                    calories:       this._targets[i].calories
                });
            }
            this._checkInNutritionPlanningMode  = result.nutritionPlanningMode;
            if(onSuccess) {
                onSuccess();
            }

			this._isLoading = false;

        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));

            if(onError) {
                onError(error);
            }

        });

    }

    _processUnassignedPlans(plans) {
        return JSON.parse(JSON.stringify(plans));
    }

    _processTargets(targets) {
        let retTargets = [];

        this._clientPoolPlans = [];
        let addedPoolPlans = [];

        for(let i = 0; i < targets.length; i++) {
            let updatedTarget = JSON.parse(JSON.stringify(targets[i]));
            updatedTarget.values = {
                assignedToString:           updatedTarget.assignedTo ? updatedTarget.assignedTo.join(", ") : "N/A",
                activeClientPoolPlans:      [],
                activeReferencePoolPlans:   []
            };

            for(let j = 0; j < updatedTarget.poolPlans.length; j++) {
                updatedTarget.poolPlans[j].activePoolPlanVersion.referencePlan.states = {
                    isNutritionReferencePlan:   updatedTarget.poolPlans[j].activePoolPlanVersion.referencePlan.recordTypeDeveloperName === "Nutrition_Reference_Plan",
                    isNutritionClientPlan:      updatedTarget.poolPlans[j].activePoolPlanVersion.referencePlan.recordTypeDeveloperName === "Nutrition_Client_Plan"
                };

                if(updatedTarget.poolPlans[j].activePoolPlanVersion.referencePlan.states.isNutritionReferencePlan === true) {
                    updatedTarget.values.activeReferencePoolPlans.push(updatedTarget.poolPlans[j].activePoolPlanVersion);
                }
                if(updatedTarget.poolPlans[j].activePoolPlanVersion.referencePlan.states.isNutritionClientPlan === true) {
                    updatedTarget.values.activeClientPoolPlans.push(updatedTarget.poolPlans[j].activePoolPlanVersion);
                    if(!addedPoolPlans.includes(updatedTarget.poolPlans[j].id)) {
                        addedPoolPlans.push(updatedTarget.poolPlans[j].id);
                        this._clientPoolPlans.push(updatedTarget.poolPlans[j]);
                    }
                }

            }
            retTargets.push(updatedTarget);
        }

		console.log("Client Active Pool Plan Versions: ", JSON.parse(JSON.stringify(this._clientPoolPlans)));
        
        return retTargets;
    }

    /* ---------- CUSTOM EVENTS ---------- */


    @track _selectedPlanDetails = {
        name:       "???",
        protein:    0,
        carbs:      1,
        fat:        2,
        calories:   3
    }

    onAssignPoolPlanToTargets(event) {

        this._selectedPlanDetails = {
            name:       event.target.dataset.planName,
            protein:    event.target.dataset.planProtein,
            carbs:      event.target.dataset.planCarbs,
            fat:        event.target.dataset.planFat,
            calories:   event.target.dataset.planCalories
        };

        this._displayAssignPoolPlanToTargetsModal = true;
        this._selectedPoolPlanId = event.target.dataset.poolPlanId;
        this._selectedAssignedTargetIds = [];
        for(let i = 0; i < this._clientPoolPlans.length; i++) {
            if(this._clientPoolPlans[i].id === this._selectedPoolPlanId) {
                this._selectedAssignedTargetIds = this._clientPoolPlans[i].parentTargetIds;
            }
        }

        this.Refresh();

    }

    onSaveNewAssignedPoolPlans(event) {

        AssignPoolPlanToTargets({
            poolPlanId: this._selectedPoolPlanId,
            targetIds:  this._selectedAssignedTargetIds
        }).then(result => {
            this._displayAssignPoolPlanToTargetsModal = false;

            this.Refresh();

        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });
        

    }

    onCloseAssignPoolPlanToTargetsModal(event) {
        this._displayAssignPoolPlanToTargetsModal = false;
    }

    onToggleTargetSelection(event) {
        let targetId = event.target.value;
        console.log("Selected Target: ", targetId);

        if(this._selectedAssignedTargetIds.includes(targetId)) {
            for(let i = 0; i < this._selectedAssignedTargetIds.length; i++) {
                if(this._selectedAssignedTargetIds[i] == targetId) {
                    this._selectedAssignedTargetIds.splice(i, 1);
                }
            } 
        } else {
            this._selectedAssignedTargetIds.push(targetId);
        }
    }

	onOpenViewReferencePlanModal(event) {
		let referencePlanId				= event.target.dataset.referencePlanId;
		let poolPlanVersionId			= event.target.dataset.poolPlanVersionId;
		this._selectedReferencePlanId	= referencePlanId;
		this._displayViewPlanModal = true;
	}

	onCloseViewReferencePlanModal(event) {
		this._displayViewPlanModal = false;
	}

	onOpenSaveAsNewPoolPlanModal(event) {
		this._displaySaveAsNewPoolPlanModal = true;


        this._selectedPlanDetails = {
            name:       event.target.dataset.planName,
            protein:    event.target.dataset.planProtein,
            carbs:      event.target.dataset.planCarbs,
            fat:        event.target.dataset.planFat,
            calories:   event.target.dataset.planCalories
        };

        let targetId                    = event.target.dataset.targetId;
        let poolPlanId                  = event.target.dataset.poolPlanId;
		let referencePlanId				= event.target.dataset.referencePlanId;
		let referencePlanName			= event.target.dataset.referencePlanName;
		let poolPlanVersionId			= event.target.dataset.poolPlanVersionId;
        

		this._selectedReferencePlanId	= referencePlanId;
		this._newPlanName				= referencePlanName;
		this._selectedVersionId			= poolPlanVersionId;
	}

	onCloseSaveAsNewPoolPlanModal(event) {
		this._displaySaveAsNewPoolPlanModal = false;
	}

	onChangeNewPlanName(event) {
		this._newPlanName = event.target.value;
	}

	
	onSaveNewPoolPlan(event) { 

		this._isSaving = true;

		IsPlanNameUsed({
			planName: this._newPlanName,
			checkInId: this._checkInId
		}).then(planNameUsed => {

			if(planNameUsed === true) {

				const event = new ShowToastEvent({
					"title": "Plan Name in use",
					"message": "Plan {0} is already in use",
					"variant": "error",
					"messageData": [
						this._newPlanName
					]
				});
				this.dispatchEvent(event);

				this._isSaving = false;
			} else {
				SaveAsNewPoolPlan({
					targetIds:          this._selectedAssignedTargetIds,
					poolPlanVersionId:	this._selectedVersionId,
					newPlanName:		this._newPlanName
				}).then(result => {
					this._displaySaveAsNewPoolPlanModal		= false;
					this._isSaving = false;
					this.Refresh();
				}).catch(error => {
					console.error("Error: ", JSON.parse(JSON.stringify(error)));
				});
			}

		}).catch(error => {

		});


	}

	onOpenDeletePoolPlanModal(event) {
		let poolPlanId	= event.target.dataset.poolPlanId;
		this._selectedPoolPlanId = poolPlanId;
		this._displayDeletePoolPlanModal = true;
	}

	onCloseDeletePoolPlanModal(event) {
		this._displayDeletePoolPlanModal = false;
	}

	@track _selectedVersionClientComments = "";
	
	@track _selectedPoolPlanVersionId;

	@track _selectedReferencePlanName = "";

	get editPlanModalHeader() {
		return "Edit Plan - " + this._selectedReferencePlanName;
	}

	onDeletePoolPlan(event) {
		this._isSaving = true;
		DeletePoolPlan({
            poolPlanId: this._selectedPoolPlanId
		}).then(result => {
            console.log("Deleted Plan");
			this._displayDeletePoolPlanModal = false;
			this._isSaving = false;
            this.Refresh();
		}).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	onOpenEditPlanModal(event) {
		this._displayEditPlanModal		= true;
		this._selectedReferencePlanId	= event.target.dataset.referencePlanId;
		this._selectedPoolPlanId		= event.target.dataset.poolPlanId;

		for(let i = 0; i < this._clientPoolPlans.length; i++) {
			if(this._clientPoolPlans[i].id === this._selectedPoolPlanId) {
				this._selectedPoolPlanVersionId		= this._clientPoolPlans[i].activePoolPlanVersion.id;
				this._selectedVersionClientComments	= this._clientPoolPlans[i].activePoolPlanVersion.clientComments;
				this._selectedTargetIds				= this._clientPoolPlans[i].parentTargetIds;

				this._selectedReferencePlanName		= this._clientPoolPlans[i].activePoolPlanVersion.referencePlan.name;

			}
		}

		console.log("Client Pool Plans: ", JSON.parse(JSON.stringify(this._clientPoolPlans)));

	}
	
	onCloseEditPlanModal(event) {
		this._displayEditPlanModal = false;
	}

    onSavePlan(event) {
		this._isSaving = true;
        let nutCarbCyclePlan = this.template.querySelector(".nutrition-carb-cycle-plan");

        nutCarbCyclePlan.Save(() => {
			SaveClientComments({
				poolPlanVersionId:	this._selectedPoolPlanVersionId,
				clientComments:		nutCarbCyclePlan.clientComments
			}).then(result => {
				this.Refresh();
				this._isSaving = false;
				this._displayEditPlanModal = false;
			}).catch(error => {
				console.error("Error: ", JSON.parse(JSON.stringify(error)));
			});
        }, (error) => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });
    }

    onActivateNewPlan(event) {
		this._isSaving = true;
        let nutCarbCyclePlan = this.template.querySelector(".nutrition-carb-cycle-plan");
        nutCarbCyclePlan.SaveAs((result) => {



            SaveNewVersion({
                poolPlanId: this._selectedPoolPlanId,
                newPlanId:  result.newPlanId
            }).then(newPoolPlanVersionId => {

				SaveClientComments({
					poolPlanVersionId:	newPoolPlanVersionId,
					clientComments:		nutCarbCyclePlan.clientComments
				}).then(result => {
					this.Refresh();
					this._isSaving = false;
					this._displayEditPlanModal = false;
				}).catch(error => {
					console.error("Error: ", JSON.parse(JSON.stringify(error)));
				});

            }).catch(error2 => {
                console.error("Error: ", JSON.parse(JSON.stringify(error2)));
            });
        }, (error) => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });

    }
	
	onOpenManageVersionsModal(event) {
	
		let poolPlanId					= event.target.dataset.poolPlanId;
		this._selectedPoolPlanId		= poolPlanId;

		this._displayManageVersionsModal = true;
	}

	onCloseManageVersionsModal(event) {
		this._displayManageVersionsModal = false;
	}

	onRefreshPoolPlanVersions(event) {
		this.Refresh();
	}

    onOpenUnassignPoolPlanFromTargetModal(event) {
        this._displayUnassignPoolPlanFromTargetModal    = true;
        this._selectedPoolPlanId                        = event.target.dataset.poolPlanId;
        this._selectedTargetId                          = event.target.dataset.targetId;
    }

    onCloseUnassignPoolPlanFromTargetModal(event) {
        this._displayUnassignPoolPlanFromTargetModal = false;
    }

    onUnnassignPoolPlanFromTarget(event) {
		this._isSaving = true;
        UnassignPoolPlanFromTarget({
            poolPlanId: this._selectedPoolPlanId,
            targetId: this._selectedTargetId
        }).then(result => {
            this.Refresh();
			this._isSaving = false;
            this._displayUnassignPoolPlanFromTargetModal = false;
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)))
        });
    }
    
    /* ---------- LWC EVENTS ---------- */

}