import { LightningElement, api, track } from 'lwc';
import GetTemplatePoolInformation		from '@salesforce/apex/AT_LWCC_PlannerTemplatePool.GetTemplatePoolInformation';
import UpdateCaloriePercentage			from '@salesforce/apex/AT_LWCC_PlannerTemplatePool.UpdateCaloriePercentage';
import UpdateSelectedTemplates			from '@salesforce/apex/AT_LWCC_PlannerTemplatePool.UpdateSelectedTemplates';
import SetAsDefaultPoolPlanId			from '@salesforce/apex/AT_LWCC_PlannerTemplatePool.SetAsDefaultPoolPlanId';
import SetPoolType						from '@salesforce/apex/AT_LWCC_PlannerTemplatePool.SetPoolType'

export default class PlannerXTemplatePool extends LightningElement {

	/* ---------- VARIABLES ---------- */

    /**
     * @description Is data being loaded
     */
	@track _isLoading = false;

    /** 
     * @description Is data being saved
     */
	@track _isSaving = false;

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;
	
	/**
	 * @description A list of nutrition target assigned to the Check-In
	 */
	@track _nutritionTargets = [];

    @track _displaySelectedTemplatesModal = false;

	/**
	 * @description If doing any actions on the target (Adding selected templates etc.)
	 */
	@track _selectedTargetId;

	/**
	 * @description The Nutrition Planning Mode of the Check-In
	 */
	@track _nutritionPlanningMode;

	
	/* ---------- GETTERS ---------- */

    /**
     * @description Is data being saved or loaded
     */
	get isSavingOrLoading() {
		return this._isLoading === true || this._isSaving === true;
	}

	@api get checkInId() {
		return this._checkInId;
	}

	@api get nutritionTargets() {
		return this._nutritionTargets;
	}

	get isGuidedPlans() {
		return this._nutritionPlanningMode === "Guided Plans";
	}

	get isFlexiPlans() {
		return this._nutritionPlanningMode === "Flexi Plans";
	}

	/* ---------- SETTERS ---------- */

	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		this.Refresh();
	}

	set nutritionTargets(targetValues) {
		this._nutritionTargets = targetValues;
	}

	/* ---------- CUSTOM METHODS ---------- */

	@api Refresh(onSuccess, onError) {
		this._isLoading = true;
		GetTemplatePoolInformation({
			checkInId: this._checkInId
		}).then(result => {
			this._nutritionTargets			= this.processNutritionTargets(result.nutritionTargets);
			this._nutritionPlanningMode		= result.checkInNutritionPlanningMode;
			console.log("Nutrition Targets: ", JSON.parse(JSON.stringify(this._nutritionTargets)));
			this._isLoading = false;
			if(onSuccess) {
				onSuccess();
			}
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError();
			}
		});
	}

    processNutritionTargets(nutritionTargets) {

        let processedTargets = [];

        for(let i = 0; i < nutritionTargets.length; i++) {
            let pTarget = JSON.parse(JSON.stringify(nutritionTargets[i]));

            pTarget.states = {
                hasPoolPlans:               pTarget.poolPlans.length > 0,
                isCalorieRangeTarget:       pTarget.targetPoolType === "Calorie Range",
                isSelectedTemplateTarget:   pTarget.targetPoolType === "Selected Templates"
            };

            processedTargets.push(pTarget);
        }

        return processedTargets;

    }

	/**
	 * @description Save the Template Pools
	 *
	 * @param {Function} onSuccess Executes when the Template Pool is successfully saved
	 * @param {Function} onError Executes when saving has errored
	 */
	@api Save(onSuccess, onError) {

	}

	/**
	 * @description Fire an On Change Event
	 */
	fireOnChangeEvent() {
        this.dispatchEvent(new CustomEvent('change'));
	}

	/* ---------- CUSTOM EVENTS ---------- */

	/**
	 * @description Executes when the Calorie Percentage for a target has changed
	 *
	 * @param {Event} event The event that executes this
	 */
	onCaloriePercentageChange(event) {
		let targetId	= event.target.dataset.targetId;
		let percentage	= event.target.value;
		this._isSaving	= true;
		UpdateCaloriePercentage({
			targetId:	targetId,
			percentage: percentage
		}).then(result => {
			this._isSaving = false;
			this.fireOnChangeEvent();
			this.Refresh();
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Executes when the Pool plan is selected as the default
	 *
	 * @param {Event} event The event that executes this
	 */
	onSelectPoolPlanAsDefault(event) {
		console.log("Set Pool Plan");
		let poolPlanId	= event.target.dataset.poolPlanId;
        let targetId    = event.target.dataset.targetId;
        console.log("Target Id: ", targetId);
		this._isSaving = true;
		SetAsDefaultPoolPlanId({
            targetId: targetId,
			poolPlanId: poolPlanId
		}).then(result => {
			this._isSaving = false;
			this.fireOnChangeEvent();
			this.Refresh();
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	onSelectPoolTypeMode(event) {
	
		let targetId		= event.target.dataset.targetId;
		let newPoolPlanType	= event.target.dataset.poolPlanType;
		this._isSaving = true;
		SetPoolType({
			targetId:		targetId,
			newPoolType:	newPoolPlanType
		}).then(result => {
			this._isSaving = false;
			this.fireOnChangeEvent();
			this.Refresh();
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});

	}

    onOpenSelectTemplatesModal(event) {
        this._displaySelectedTemplatesModal = true;
		this._selectedTargetId = event.target.dataset.targetId;
    }

    onCloseSelectTemplatesModal(event) {
        this._displaySelectedTemplatesModal = false;
    }

	/**
	 * @description Executes the template plans need to be saved
	 */
	onSaveTemplatePlansToTarget(event) {
		console.log("Saving template to target ", this._selectedTargetId);

		let addTemplatePlan			= this.template.querySelector(".add-template-plan");
		let targetId				= this._selectedTargetId;
		let selectedTemplatePlans	= addTemplatePlan.selectedTemplates;
		
		this._isSaving = true;
		UpdateSelectedTemplates({
			targetId:		targetId,
			templateIds:	selectedTemplatePlans
		}).then(result => {
			this._isSaving = false;
			this.fireOnChangeEvent();
			this.Refresh();
			this._displaySelectedTemplatesModal = false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});

	}
	
}