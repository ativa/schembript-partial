import { LightningElement, track, api } from 'lwc';
import FORM_FACTOR						from '@salesforce/client/formFactor';

import GetAllVersions       from '@salesforce/apex/AT_LWCC_TrackerManagePoolPlanVersions.GetAllVersions';
import MakeActiveVersion    from '@salesforce/apex/AT_LWCC_TrackerManagePoolPlanVersions.MakeActiveVersion';
import DeleteVersion		from '@salesforce/apex/AT_LWCC_TrackerManagePoolPlanVersions.DeleteVersion';
import SaveAsNewVersion		from '@salesforce/apex/AT_LWCC_TrackerManagePoolPlanVersions.SaveAsNewVersion';
import UpdateClientComments	from '@salesforce/apex/AT_LWCC_TrackerManagePoolPlanVersions.UpdateClientComments';

export default class TrackerXManagePoolPlanVersions extends LightningElement {

    /* ---------- VARIABLES ---------- */

    /**
     * @description Is the data being loaded
     */
    @track _isLoading = false;

    /**
     * @description Is the data being saved
     */
    @track _isSaving = false;

    /**
     * @description The Id of the Pool Plan
     */
    @track _poolPlanId;

    /**
     * @description All available plan versions
     */
    @track _allPlanVersions = [];

	/**
	 * @description Display the View Plan Modal
	 */
	@track _displayViewPlanModal = false;

	/**
	 * @description Display the Save as new version modal
	 */
	@track _displaySaveAsNewVersion = false;

	/**
	 * @description Display deleting the Plan Version Modal
	 */
	@track _displayDeletingPlanModal = false;

	/**
	 * @description The selected version
	 */
	@track _selectedVersion;

    @track _selectedVersionId;

	@track _newPlanName;

    /* ---------- GETTERS ---------- */
	
	get isMobile() {
		return FORM_FACTOR !== "Large";
	}

    /**
     * @description Returns the Id of the Pool Plan
     */
    @api get poolPlanId() {
        return this._poolPlanId;
    }

    /**
     * @description Is the data being saved or loaded
     */
    get isLoadingOrSaving() {
        return this._isLoading === true || this._isSaving === true;
    }

    /* ---------- SETTERS ---------- */

    /**
     * @description Sets the Id of the Pool Plan
     *
     * @param {String} newPoolPlanId The new Pool Plan Id
     */ 
    set poolPlanId(newPoolPlanId) {
        this._poolPlanId = newPoolPlanId;
        this.Refresh();
    }

    /* ---------- CUSTOM METHODS ---------- */

    /**
     * @description Refresh The version table
     */
    @api Refresh() {
        this._isLoading = true;
        GetAllVersions({
            poolPlanId: this._poolPlanId
        }).then(result => {
            console.log("Results: ", JSON.parse(JSON.stringify(result)));
            this._allPlanVersions	= JSON.parse(JSON.stringify(result));
            this._isLoading			= false;

			//Fire a refresh event
			this.dispatchEvent(new CustomEvent('refresh'));
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
            this._isLoading = false;
        });
    }

	_findVersion(versionId) {
		for(let i = 0; i < this._allPlanVersions.length; i++) {
			if(this._allPlanVersions[i].id === versionId) {
				return this._allPlanVersions[i];
			}
		}
	}

    /* ---------- CUSTOM EVENTS ---------- */

	onClientCommentsUpdate(event) {
		
	}

    onDisplayDeletePlanModal(event) {
        this._displayDeletingPlanModal = true;
        this._selectedVersionId = event.target.dataset.versionId;
    }

    onCloseDeletePlanModal(event) {
        this._displayDeletingPlanModal = false;
    }
        
    onActivateVersion(event) {
        let versionId = event.target.dataset.versionId;
        console.log("Version Id: ", versionId);
        MakeActiveVersion({
            poolPlanVersionId: versionId
        }).then(result => {
            this.Refresh();
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });
    }

	onDeleteVersion(event) {
        let versionId = event.target.dataset.versionId;
        console.log("Version Id: ", versionId);
        DeleteVersion({
            poolPlanVersionId: this._selectedVersionId
        }).then(result => {
            this.Refresh();
            this._displayDeletingPlanModal = false;
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)));
            this._displayDeletingPlanModal = false;
        });
	}

	get editPlanModalHeader() {
		return "Edit Plan - " + this._selectedVersion.planName;
	}

	onViewPlan(event) {
        let versionId				= event.target.dataset.versionId;
		this._selectedVersion		= this._findVersion(versionId);
		this._displayViewPlanModal	= true;
		console.log("Selected Version: ", JSON.parse(JSON.stringify(this._selectedVersion)))
	}

	onCloseViewPlanModal(event) {
		event.stopPropagation();
		this._displayViewPlanModal = false;
	}

	onDisplaySaveAsNewVersionModal(event) {
        let versionId				= event.target.dataset.versionId;
		this._selectedVersion		= this._findVersion(versionId);
		this._newPlanName			= this._selectedVersion.planName;
		this._displaySaveAsNewVersion = true;
	}

	onCloseSaveAsNewVersionModal(event) {
		event.stopPropagation();
		this._displaySaveAsNewVersion = false;
	}

	onSaveNewVersion(event) {
		SaveAsNewVersion({
			poolPlanVersionId: this._selectedVersion.id,
			newPlanName: this._newPlanName
		}).then(result => {
			console.log("Saved new Version");	
			this._displaySaveAsNewVersion = false;
            this.Refresh();
		}).catch(error => {
			console.error("Error: ",JSON.parse(JSON.stringify(error)));
		});
	}

	onUpdateNewPlanName(event) {
		this._newPlanName			= event.target.value;
	}

	onSavePlanData(event) {
		let carbCyclePlan = this.template.querySelector(".carb-cycle-plan");

		let clientComments = carbCyclePlan.clientComments;

		UpdateClientComments({
			poolPlanVersionId:	this._selectedVersion.id,
			clientComments:		clientComments
		}).then(result => {
			carbCyclePlan.Save(() => {
				this._displayViewPlanModal = false;
				this.Refresh();
			});
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});

	}

    /* ---------- LWC EVENTS ---------- */

}