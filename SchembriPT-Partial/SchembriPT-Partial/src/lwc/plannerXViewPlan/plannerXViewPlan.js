import { LightningElement, api, track } from 'lwc';

import GetPlanData  from '@salesforce/apex/AT_LWCC_PlannerViewPlan.GetPlanData';

/**
 * @description View a Plan
 */
export default class PlannerXViewPlan extends LightningElement {

    /* --------------- VARIABLES --------------- */

    /**
     * @description Holds the Reference Plan Id
     */
    @track _planId;

    /**
     * @description Holds the Original Reference Plan Data
     */
    @track _originalPlanData;

    /**
     * @description Holds the Updated Reference Plan Data
     */
    @track _updatedPlanData;

	/**
	 * @description Tracks is the Plan is being loaded
	 */ 
	@track _isLoading = false;
    
    /* --------------- GETTERS --------------- */
    
    /**
     * @description Returns the Reference Plan Id
     * @returns {String} The Reference Plan Id
     */
    @api get planId() {
        return this._planId;
    }

    @api get tableClass() {

        if(this._updatedPlanData.states.clonedFromPreviousPlan === true) {
            return "with-previous-values slds-table slds-table_cell-buffer slds-table_bordered slds-table_col-bordered";
        } else {
            return "without-previous-values slds-table slds-table_cell-buffer slds-table_bordered slds-table_col-bordered";
        }

    }

    /**
     * @description Tracks if the Reference Plan Data has been loaded
     */
    get referencePlanDataLoaded() {
        return typeof(this._updatedPlanData) !== 'undefined'
    }

    /* --------------- SETTERS --------------- */

    /**
     * @description Set the Plan Id
     * @param {String} newPlanId The Id of the Plan
     */
    set planId(newPlanId) {
        this._planId = newPlanId;
        this.RefreshPlan();
    }

    /* --------------- CUSTOM METHODS --------------- */

    /**
     * 
     * @param {Function} onSuccess A function that executes when the data has been successfully refreshed
     * @param {Function} onFail A function that executes when the data failed to load
     */
    @api RefreshPlan(onSuccess, onFail) {

		this._isLoading = true;

        GetPlanData({
            planId: this._planId
        }).then(result => {
            console.log("Plan: ", JSON.parse(JSON.stringify(result)));
            this._originalPlanData = JSON.parse(JSON.stringify(result));
            this._updatedPlanData = this.processPlan(this._originalPlanData);
			this._isLoading = false;
        }).catch(error => {
            console.error("ERROR: ", JSON.parse(JSON.stringify(error)))
			this._isLoading = false;
        });
    }


    /**
     * @description Update the Reference Plan Data
     * @param {Object} refPlan The data for the Reference Plan 
     * @returns {Object} Returns an update Reference Plan Data
     */
    processPlan(refPlan) {

        let newRefPlan = JSON.parse(JSON.stringify(refPlan));

        newRefPlan.states = {};

        if(newRefPlan.previousPlanId) {
            newRefPlan.states.clonedFromPreviousPlan = true;
        } else {
            newRefPlan.states.clonedFromPreviousPlan = false;
        }

        return newRefPlan;

    }

    /* --------------- CUSTOM EVENTS --------------- */

    /* --------------- LWC EVENTS --------------- */

}