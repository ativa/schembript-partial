import { LightningElement, api, track } from 'lwc';
import CheckIfValid                     from '@salesforce/apex/AT_LC_NutritionPlannerValidation.CheckIfValid';

export default class AtNutritionXPlannerValidation extends LightningElement {

    /* ---------- VARIABLES ---------- */

    @track _checkInId;

    @track _validationCheck = {
        IsValid:            false,
        validationRules:    []
    };

    @track _displayDropdown = false;

    /* ---------- GETTERS ---------- */

    @api get checkInId() {
        return this._checkInId;
    }

    get validationRules() {

        let rules = [];

        for(let i = 0; i < this._validationCheck.validationRules.length; i++) {
            let processedRule = JSON.parse(JSON.stringify(this._validationCheck.validationRules[i]));
            processedRule.order = i;
            rules.push(processedRule);
        }

        return rules;


    }

    /* ---------- SETTERS ---------- */

    set checkInId(newCheckInId) {
        this._checkInId = newCheckInId;
        this.Refresh();
    }

    /* ---------- CUSTOM METHODS ---------- */

    @api Refresh() {
        CheckIfValid({
            checkInId: this._checkInId
        }).then(result => {
            console.log("Success: ", JSON.parse(JSON.stringify(result)))
            this._validationCheck = JSON.parse(JSON.stringify(result));
        }).catch(error => {
            console.error("Error: ", JSON.parse(JSON.stringify(error)))
        });
    }

    /* ---------- CUSTOM EVENTS ---------- */

    onInputFocus(event) {
        this._displayDropdown = true;
    }

    onInputBlur(event) {
        this._displayDropdown = false;
    }

    /* ---------- LWC EVENTS ---------- */

}