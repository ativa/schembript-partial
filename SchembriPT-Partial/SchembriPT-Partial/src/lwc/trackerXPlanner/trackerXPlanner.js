import { LightningElement, api, track } from 'lwc';

import GetClientPlannerInformation		from '@salesforce/apex/AT_LWCC_TrackerPlanner.GetClientPlannerInformation';
import DeletePlan						from '@salesforce/apex/AT_LWCC_TrackerPlanner.DeletePlan';
import AddClientPlanToPool				from '@salesforce/apex/AT_LWCC_TrackerPlanner.AddClientPlanToPool';

export default class TrackerXPlanner extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The user information
	 */
	@track _userInfo;

	/**
	 * @description The latest Check-In info
	 */
	@track _latestCheckInInfo;
    
	/**
	 * @description
	 */
	@track _selectedClientPlanId;

	/**
	 * @description The client plans
	 */
	@track _clientPlans = [];

	/**
	 * @description All template pool plans
	 */
	@track _allTemplatePoolPlans = [];

	/**
	 * @description Tracks if new data is being loaded
	 */
	@track _isLoading = false;

	/**
	 * @description Tracks if data is being saved
	 */
	@track _isSaving = false;
	
	/**
	 * @description Tracks if data is being deleted
	 */
	@track _isDeleting = false;

	/**
	 * @description Tracks if the Add Template Plan Modal is displayed 
	 */
	@track _displayAddTemplatePlanModal = false;

	/**
	 * @description Tracks if the deleting plan modal is displayed
	 */
	@track _displayDeletingPlanModal = false;

	/**
	 * @description Tracks if the clone plan modal is displayed
	 */
	@track _displayClonePlanModal = false;

	/**
	 * @description Tracks if the edit plan modal is displayed
	 */
	@track _displayEditPlanModal = false;

	/**
	 * @description Tracks if the saving plan as template modal is displayed
	 */
	@track _displaySavingPlanAsTemplate = false;

	/**
	 * @description Tracks if the modal for assigning to days is displayed
	 */
	@track _displayAssignToDaysWeeksModal = false;

	/**
	 * @description a list of assign to days selected
	 */
	@track assignToDayWeeksSelected = {};

	@track _fromConvertId;
	@track _toConvertId;

	/* ---------- GETTERS ---------- */

	/**
	 * @description Returns the Id of the Account
	 */
	get clientId() {
		return this._clientId;
	}

	/**
	 * @description Is data either being saved or loaded?
	 */
	get isLoadingOrSavingOrDeleting() {
		return this._isLoading === true && this._isSaving === true && this._isDeleting === true;
	}

	get displayedPlans() {
		let p = [];

		for(let i = 0; i < this._clientPlans.length; i++) {
			p.push({
				id:			this._clientPlans[i].id,
				name:		this._clientPlans[i].name,
				isSelected: this._clientPlans[i].id === this._selectedClientPlanId
			});
		}

		return p;
	}

	get fromConvertPlanOptions() {

        let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
		let selectedTemplatePools = assignPlansToTargets.GetSelectedTemplatePoolIds();

		let options = [];
		for(let i = 0; i < this._allTemplatePoolPlans.length; i++) {

			if(selectedTemplatePools.includes(this._allTemplatePoolPlans[i].templatePoolPlanId) === true) {
				options.push({
					label: this._allTemplatePoolPlans[i].name,
					value: this._allTemplatePoolPlans[i].templatePoolPlanId,
				});
			}

		}
		return options;
	}

	get toConvertPlanOptions() {
		let options = [];
		for(let i = 0; i < this._allTemplatePoolPlans.length; i++) {
			options.push({
				label: this._allTemplatePoolPlans[i].name,
				value: this._allTemplatePoolPlans[i].templatePoolPlanId,
			});
		}
		return options;
	}


	get hasSelectedPlan() {
		return typeof(this._selectedClientPlanId) !== "undefined";
	}

	get hasCheckInInformation() {
		return typeof(this._latestCheckInInfo) !== "undefined";
	}

    get assignToDayPlans() {
        let assignPlans = [];

		console.log("Assign to Days: ", JSON.parse(JSON.stringify(this.assignToDayWeeksSelected)));

        for(let i = 0; i < this._allTemplatePoolPlans.length; i++) {
            let plan = {
                id:					this._allTemplatePoolPlans[i].id,
                name:				this._allTemplatePoolPlans[i].name,
				templatePoolPlanId: this._allTemplatePoolPlans[i].templatePoolPlanId,
                options:			[]
            };

            //If Flexi Plans
            if(this._latestCheckInInfo.nutritionPlanningMode === "Flexi Plans") {
                plan.options.push({ label: "Monday", value: "Monday", variant: (this.assignToDayWeeksSelected.Monday === plan.templatePoolPlanId ? "brand" : "neutral")});
                plan.options.push({ label: "Tuesday", value: "Tuesday", variant: (this.assignToDayWeeksSelected.Tuesday === plan.templatePoolPlanId ? "brand" : "neutral")});
                plan.options.push({ label: "Wednesday", value: "Wednesday", variant: (this.assignToDayWeeksSelected.Wednesday === plan.templatePoolPlanId ? "brand" : "neutral")});
                plan.options.push({ label: "Thursday", value: "Thursday", variant: (this.assignToDayWeeksSelected.Thursday === plan.templatePoolPlanId ? "brand" : "neutral")});
                plan.options.push({ label: "Friday", value: "Friday", variant: (this.assignToDayWeeksSelected.Friday === plan.templatePoolPlanId ? "brand" : "neutral")});
                plan.options.push({ label: "Saturday", value: "Saturday", variant: (this.assignToDayWeeksSelected.Saturday === plan.templatePoolPlanId ? "brand" : "neutral")});
                plan.options.push({ label: "Sunday", value: "Sunday", variant: (this.assignToDayWeeksSelected.Sunday === plan.templatePoolPlanId ? "brand" : "neutral")});
            } else if(this._latestCheckInInfo.nutritionPlanningMode === "Guided Plans") {

                let totalWeekCount = parseInt(this._latestCheckInInfo.guidedPlanNumberWeeks.replace("Week ",""));

                for(let i = 1; i <= totalWeekCount; i++) {
                    let weekStr = "Week " + i;
                    plan.options.push({
                        label: weekStr,
                        value: weekStr,
                        variant: (this.assignToDayWeeksSelected[weekStr] === plan.templatePoolPlanId ? "brand" : "neutral")
                    });
                }

            }
            assignPlans.push(plan);
        }

		console.log("Assign to Days: ", JSON.parse(JSON.stringify(assignPlans)));

        return assignPlans;
    }

	/* ---------- SETTERS ---------- */
	
	/* ---------- CUSTOM METHODS ---------- */

	@api Refresh() {
		this._isLoading = true;
		GetClientPlannerInformation().then(result => {
			console.log("Result: ", JSON.parse(JSON.stringify(result)));
			this._userInfo				= JSON.parse(JSON.stringify(result.userInfo));
			this._clientPlans			= JSON.parse(JSON.stringify(result.clientPlans));
			this._latestCheckInInfo		= JSON.parse(JSON.stringify(result.latestCheckInInfo))

			//If no client plan has been selected then default to the first plan
			if(typeof(this._selectedClientPlanId) === "undefined" && this._clientPlans.length > 0) {
				this._selectedClientPlanId = this._clientPlans[0].id;
			}

			this._isLoading		= false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}


	/* ---------- CUSTOM EVENTS ---------- */

    onSaveAssignPlansToTargets(event) {

		console.log("Saving Assign Plans to Targets");

        let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
        assignPlansToTargets.Save(
            () => {
                console.log("Saved Plans to Targets");
                this.dispatchEvent(new CustomEvent('update'));
            }, (error) => {
                console.error("Error: ", JSON.parse(JSON.stringify(error)));
        });


    }
    
	onSelectPlan(event) {
		let selectedPlanId = event.target.dataset.planId;
		this._selectedClientPlanId = selectedPlanId;
	}

	/**
	 * @description Open the client plan modal
	 */
	onOpenNewClientPlanModal(event) {
		this._displayAddTemplatePlanModal = true;
	}
	
	/**
	 * @description Close the client plan modal
	 */
	onCloseNewClientPlanModal(event) {
		this._displayAddTemplatePlanModal = false;
	}

	onOpenDeletePlanModal(event) {
		this._displayDeletingPlanModal = true;
	}

	onCloseDeletePlanModal(event) {
		this._displayDeletingPlanModal = false;
	}

	onOpenClonePlanModal(event) {
		this._displayClonePlanModal = true;
	}

	onCloseClonePlanModal(event) {
		this._displayClonePlanModal = false;
	}

	onOpenEditPlanModal(event) {
		this._displayEditPlanModal = true;
	}
	
	onCloseEditPlanModal(event) {
		this._displayEditPlanModal = false;
	}

	onOpenSavePlanAsTemplate(event) {
		this._displaySavingPlanAsTemplate = true;
	}

	onCloseSavePlanAsTemplate(event) {
		this._displaySavingPlanAsTemplate = false;
	}

	onSaveTemplateData(event) {
		let savePlanAsTemplate = this.template.querySelector(".save-plan-as-template");
		savePlanAsTemplate.Save(
			() => {
				this._displaySavingPlanAsTemplate = false;
			},
			(error) => {
				this._displaySavingPlanAsTemplate = false;
			}
		);
	}

	onSaveEditedPlan(event) {
		console.log("Saving Edited Plan");
		this._isSaving = true;
		let carbCyclePlan = this.template.querySelector(".nutrition-carb-cycle-plan");
		carbCyclePlan.Save(() => {
			this._isSaving = false;
			this._displayEditPlanModal = false;
			this.Refresh();
		},(error) => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)))
			this._isSaving = false;
		});
	}

	/**
	 * @description Executes when the Plan is being deleted
	 */
	onDeletePlan(event) {
		this._isDeleting = true;
		DeletePlan({
			planId: this._selectedClientPlanId
		}).then(result => {
			this._isDeleting = false;
			console.log("Successfully deleted plan");
			this._selectedClientPlanId = undefined;
			this._displayDeletingPlanModal = false;
			this.Refresh();
		}).catch(error => {
			this._isDeleting = false;
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Executes when to add a new template plan
	 */
	onAddTemplatePlan(event) {
		let addTmpPlan = this.template.querySelector(".addTemplatePlan");
		this._isSaving = true;
		addTmpPlan.SaveAsClientPlan(this._userInfo.accountId, (planData) => {
			console.log("Saved New Plan: ", JSON.parse(JSON.stringify(planData)));
			AddClientPlanToPool({
				checkInId: this._latestCheckInInfo.id,
				newPlanId: planData.newPlanId
			}).then(result => {
				console.log("Successfully saved to Template Pool");
				this._displayAddTemplatePlanModal	= false;
				this._isSaving						= false;
				this.Refresh();

				let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
				assignPlansToTargets.RefreshPlanOptions();

			}).catch(error => {
				console.error("Error: ", error);
			});


		})
	}

	/**
	 * @description Executes when the plan is being clonned
	 */
	onClonePlan(event) {
		this._isSaving = true;
		let clonePlan = this.template.querySelector(".clonePlan");
		clonePlan.Clone(() => {
			this.Refresh();
			this._displayClonePlanModal = false;
			this._isSaving = false;
		})
	}

    /**
     * @description Executes when we want to display the assign to days/weeks
     */
    onAssignToDaysDisplay(event) {
        this._displayAssignToDaysWeeksModal = true;
    }

    onCloseAssignToDaysWeeksModal(event) {
        this._displayAssignToDaysWeeksModal = false;
    }

	onSelectDayWeek(event) {
		let valueSelected = event.target.value;
		let planId = event.target.dataset.templatePoolPlanId;
		this.assignToDayWeeksSelected[valueSelected] = planId;
	}

	onAssignPlans(event) {
        this._displayAssignToDaysWeeksModal = false;
		let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
        if(this._latestCheckInInfo.nutritionPlanningMode === "Flexi Plans") {
            console.log("Assigning Days");
            assignPlansToTargets.AssignDays(this.assignToDayWeeksSelected);
        } else if(this._latestCheckInInfo.nutritionPlanningMode === "Guided Plans") {
            console.log("Assigning Weeks");
            assignPlansToTargets.AssignWeeks(this.assignToDayWeeksSelected);
        }
	}

	onConvertPlans(event) {
		let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
		assignPlansToTargets.ConvertPlans(this._fromConvertId, this._toConvertId);
	}

	onSelectFromConvertId(event) {
		this._fromConvertId = event.target.value;
	}

	onSelectToConvertId(event) {
		this._toConvertId = event.target.value;
	}

	onAssignDaysWeeks(event) {
		console.log("Assign Days/Weeks");
		console.log(JSON.parse(JSON.stringify(event.detail)));
		let detail = event.detail;
		let assignValues = {};
		for(let i = 0; i < event.detail.assign.length; i++) {
			assignValues[event.detail.assign[i]] = event.detail.poolPlanId;
		}
		let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
        if(this._latestCheckInInfo.nutritionPlanningMode === "Flexi Plans") {
            console.log("Assigning Days");
            assignPlansToTargets.AssignDays(assignValues);
        } else if(this._latestCheckInInfo.nutritionPlanningMode === "Guided Plans") {
            console.log("Assigning Weeks");
            assignPlansToTargets.AssignWeeks(assignValues);
        }
	}

	onRefreshPoolPlans(event) {
		this.Refresh();
		let assignPlansToTargets = this.template.querySelector(".assign-plans-to-targets");
		assignPlansToTargets.RefreshPlanOptions();
	}

	/* ---------- LWC EVENTS ---------- */

	connectedCallback() {
		this.Refresh();
	}


}