import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent }               from 'lightning/platformShowToastEvent'


import SendUpdateRequest        from '@salesforce/apex/UpdateClientCheckInController.SendUpdateRequest';
import UpdateCheckInStatuses    from '@salesforce/apex/UpdateClientCheckInController.UpdateCheckInStatuses';

/**
 * @description Allows the Trainer to Update Client Details, or Allow then to send an email to the user requesting them change it
 */
export default class UpdateClientCheckIn extends LightningElement {

    /* --------------- VARIABLES --------------- */

    /**
     * @description The Check-In Id
     */
    @api recordId;

    /**
     * @description Display the Add Items Panel
     */
    @track _displayAddItemsPanel = false;

    /**
     * @description The Update Options
     */
    _updateModeOptions = [
        { label: 'Manual Update', value: 'Manual Update' }, 
        { label: 'Client Update', value: 'Client Update' }
    ];

    /**
     * @description The Current Update Mode
     */
    @track _currentUpdateMode = 'Manual Update';

    /* --------------- GETTERS --------------- */

    get isManualUpdate() {
        return this._currentUpdateMode === 'Manual Update';
    }
    
    get isClientUpdate() {
        return this._currentUpdateMode === 'Client Update';
    }

    /* --------------- SETTERS --------------- */

    /* --------------- CUSTOM MEHODS --------------- */

    fireSuccessEvent(title, message) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: 'success'
        });
        this.dispatchEvent(event);
    }

    fireErrorEvent(title, message) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: 'error'
        });
        this.dispatchEvent(event);
    }

    /**
     * @description Submit a Status Change
     * @param {String} type (Photos, Measurements, Measurements & Photos, Files)
     */
    submitStatusChange(type) {
        UpdateCheckInStatuses({
            checkInId:  this.recordId,
            updateType: type
        }).then(() => {
            if(type === 'Photos') {
                this.fireSuccessEvent('Updated Photos', 'Photos have been successfully updated');
            } else if(type === 'Measurements') {
                this.fireSuccessEvent('Updated Measurements', 'Measurements have been successfully updated');
            } else if(type === 'Measurements & Photos') {
                this.fireSuccessEvent('Updated Measurements & Photos', 'Measurements & Photos have been successfully updated');
            }
        }).catch((error) => {
            console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
            this.fireErrorEvent('Error', 'Unsuccessful while saving');
        });
    }

    /**
     * @description Send an Update Request
     * @param {String} type (Photos, Measurements, Measurements & Photos, Files)
     */
    requestUpdate(type) {
        SendUpdateRequest({
            checkInId:  this.recordId,
            updateType: type
        }).then(() => {
            if(type === 'Photos') {
                this.fireSuccessEvent('Requested Photos', 'Photos have been requested');
            } else if(type === 'Measurements') {
                this.fireSuccessEvent('Requested Measurements', 'Measurements have been requested');
            } else if(type === 'Measurements & Photos') {
                this.fireSuccessEvent('Requested Measurements & Photos', 'Measurements & Photos have been requested');
            } else if(type === 'Files') {
                this.fireSuccessEvent('Requested Files', 'Measurements & Photos have been requested');

            }
        }).catch((error) => {
            console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
            this.fireErrorEvent('Error', 'Unsuccessful while requesting')
        });
    }

    /* --------------- CUSTOM EVENTS --------------- */

    /**
     * @description Executes whenever the update mode is changed
     * @param {Event} event The Event that triggered this
     */
    onUpdateModeChange(event) {
        this._currentUpdateMode = event.target.value;
    }

    /**
     * @description Executes when the Photos Update has been submitted
     * @param {Event} event 
     */
    onSubmitPhotos(event) {
        this.submitStatusChange('Photos');
    }

    onCloseAddItems(event) {
        this._displayAddItemsPanel = false;
    }

    onShowAddItems(event) {
        this._displayAddItemsPanel = true;
    }

    /**
     * @description Executes when the Measurements Update has been submitted
     * @param {Event} event 
     */
    onSubmitMeasurements(event) {
        let measurementsSection = this.template.querySelector('.measurements__measurements-section');
        measurementsSection.Submit(() => {
            this.submitStatusChange('Measurements');
        });
    }

    /**
     * @description Executes when the Photos and Measurements Update has been submitted
     * @param {Event} event 
     */
    onSubmitPhotosAndMeasurements(event) {
        let measurementsSection = this.template.querySelector('.photos-measurements__measurements-section');
        measurementsSection.Submit(() => {
            this.submitStatusChange('Measurements & Photos');
        });
    }

    /**
     * 
     * @param {Event} event 
     */
    onRequestPhotos(event) {
        this.requestUpdate('Photos');
    }

    /**
     * 
     * @param {Event} event 
     */
    onRequestMesurements(event) {
        this.requestUpdate('Measurements');
    }
    
    /**
     * 
     * @param {Event} event 
     */
    onRequestMesurementsAndPhotos(event) {
        this.requestUpdate('Measurements & Photos');
    }
    
    /**
     * 
     * @param {Event} event 
     */
    onRequestFiles(event) {
        this.requestUpdate('Files');
    }

}