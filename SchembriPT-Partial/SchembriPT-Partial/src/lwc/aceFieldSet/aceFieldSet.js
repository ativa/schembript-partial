import { LightningElement, api, track } from 'lwc';

import getAllObjectTypes	from '@salesforce/apex/FieldSetView.getAllObjectTypes';
import getFieldSets			from '@salesforce/apex/FieldSetView.getFieldSets';

export default class AceFieldSet extends LightningElement {
	    
	/**
	 * Variables related to the Lightning Web Component
	 */
	_inputVariables = [];
	
	/**
	 * @description Context variables realted to the flow
	 */
    _builderContext = {};

	/**
	 * @description Object Options available for the fieldset
	 */
	_objectOptions = [];
	
	/**
	 * @description Fieldset Options available for the fieldset (relative to the object selected)
	 */
	_fieldsetOptions = [];

	@track _initLoading = false;

	/**
	 * @description All available Label Options
	 */
	labelOptions = [
		{ label: 'Label Inline',	value: 'label-inline' },
		{ label: 'Label Stacked',	value: 'label-stacked' }
	]


    @api get inputVariables() {
        return this._inputVariables;
    }

    // Set a field with the data that was stored from the flow.
    // This data includes the public volume property of the custom volume      
    // component.
    set inputVariables(variables) {
		console.log("Input Variables:", JSON.parse(JSON.stringify(variables)));
        this._inputVariables = variables || [];
		
		//If we have both the input variabeles and the builder context set, then init the component
		if(this._inputVariables && this._builderContext) {
			this.initComponent();
		}

    }

	@api get builderContext() {
		return this._builderContext;
	}

	set builderContext(context) {
		console.log("Builder Context:", JSON.parse(JSON.stringify(context)));
        this._builderContext = context || {};
		
		//If we have both the input variabeles and the builder context set, then init the component
		if(this._inputVariables && this._builderContext) {
			this.initComponent();
		}
	}

	initComponent() {

		//Load the object types
		this.loadObjectTypes(
			(success) => {
		
				//After successfully loading the object types, if the object type is already set, then load the field sets
				let objectName = this.getInputValue('objectApiName');
				if(objectName) {
					this.loadFieldsets(
						objectName, 
						(success) => {
							this._initLoading = false;
						},
						(error) => {
							this._initLoading = false;
						}
					);
				} else {
					//Else we are done loading
					this._initLoading = false;
				}

			},
			(error) => {
		
			}
		);

	}



	loadObjectTypes(onSuccess, onError) {
		getAllObjectTypes().then(result => {
			this._objectOptions = JSON.parse(JSON.stringify(result));
			onSuccess(this._objectOptions);
		}).catch(error => {
			onError(error);
		});
	}

	loadFieldsets(objectApiName, onSuccess, onError) {
		getFieldSets({
			objectName: objectApiName
		}).then(result => {
			this._fieldsetOptions = JSON.parse(JSON.stringify(result));

			if(onSuccess) {
				onSuccess(result);
			}

		}).catch(error => {

			if(onError) {
				onError(error);
			}

		});
	}
		
	/**
	 * @description Get the value 
	 */
	getInputValue(fieldName) {
        const param = this.inputVariables.find(({name}) => name === fieldName);
        return param && param.value;
	}

	get getRecordIdOptions() {

		let options = [];

		for(let scrVariable of this._builderContext.variables) {
			if(scrVariable.dataType === "String" && scrVariable.isCollection == false) {
				options.push({
					label: scrVariable.name,
					value: scrVariable.name
				});
			}
		}
		return options;
	}

	get recordId() {
        return this.getInputValue('recordId');
	}

	get title() {
        return this.getInputValue('title');
	}
	
	get showTitle() {
        return this.getInputValue('showTitle');
	}

	get columns() {
        return this.getInputValue('columns');
	}

	get editable() {
        return this.getInputValue('editable');
	}

	get objectApiName() {
		return this.getInputValue('objectApiName');
	}
	
	get fieldSetName() {
		return this.getInputValue('fieldSetName');
	}

	get labelPosition() {
		return this.getInputValue('labelPosition');
	}
	
	get displaySaveButton() {
		return this.getInputValue('displaySaveButton');
	}

	get channelKey(){ 
		return this.getInputValue('channelKey');
	}

	fireInputValueChangedEvent(name, newValue, newValueDataType) {
		const valueChangedEvent = new CustomEvent(
			'configuration_editor_input_value_changed', {
					bubbles: true,
					cancelable: false,
					composed: true,
					detail: {
						name,
						newValue,
						newValueDataType
					}
			}
		);
		this.dispatchEvent(valueChangedEvent);
	}

	handleRecordIdChange(event) {
		if(event && event.detail) {
			//Make a reference
			this.fireInputValueChangedEvent('recordId', '{!' + event.detail.value + '}', 'reference');
		}
	}

	handleObjectSelection(event) {
		//After selecting the object, query the fieldsets for it
		this.fireInputValueChangedEvent('objectApiName', event.target.value, 'String');

		//Need to clear the Fieldset if updating the Object API name
		this.fireInputValueChangedEvent('fieldSetName', null, 'String');

		this.loadFieldsets(event.target.value);
	}

	handleFieldsetSelection(event) {
		//Update the fieldset
		this.fireInputValueChangedEvent('fieldSetName', event.target.value, 'String');
	} 

	handleTitleChange(event) {
		this.fireInputValueChangedEvent('title', event.target.value, 'String');
	}

	handleShowTitleChange(event) {
		this.fireInputValueChangedEvent('showTitle', event.target.checked, 'Boolean');
	}

	handleColumnChange(event) {
		this.fireInputValueChangedEvent('columns',  parseInt(event.target.value), 'Number');
	}
	
	handleEditableChange(event) {
		this.fireInputValueChangedEvent('editable',  event.target.checked, 'Boolean');
	}

	handleChannelKeyChange(event) {
		this.fireInputValueChangedEvent('channelKey', event.target.value, 'String');
	}

	handleLabelOptionChange(event) {
		if(event && event.detail) {
			this.fireInputValueChangedEvent('labelPosition', event.detail.value, 'String');
		}
	}

	handleDisplaySaveButtonChange(event) {
		this.fireInputValueChangedEvent('displaySaveButton', event.target.checked, 'Boolean');
	}

	connectedCallback() {
		this._initLoading = true;
	}

}