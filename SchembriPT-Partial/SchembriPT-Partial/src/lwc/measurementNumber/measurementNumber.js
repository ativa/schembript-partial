import { LightningElement, api } from 'lwc';

export default class MeasurementNumber extends LightningElement {

	@api value;

	@api type;

	@api get isGrams() {
		return this.type === 'Grams';
	}



}