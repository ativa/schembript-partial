import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent }				from 'lightning/platformShowToastEvent';

import GetNutritionalPlan					from '@salesforce/apex/AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan';
import UpdatePlan							from '@salesforce/apex/AT_LWCC_PlannerNutritionCarbCyclePlan.UpdatePlan';
import SaveNutritionBlockAsTemplateBlock	from '@salesforce/apex/AT_LWCC_PlannerNutritionCarbCyclePlan.SaveNutritionBlockAsTemplateBlock';
import SaveNewPlan                          from '@salesforce/apex/AT_LWCC_PlannerNutritionCarbCyclePlan.SaveNewPlan';
import GetTargetInformation					from '@salesforce/apex/AT_LWCC_PlannerNutritionCarbCyclePlan.GetTargetInformation'

export default class PlannerXNutritionCarbCyclePlan extends LightningElement {

	/* ---------- VARIABLES ---------- */
	
	@api canUpdatePlanName = false;

	@api canUpdateClientComments = false;

	@api clientComments = "";

	/**
	 * @description The order of the block in the list that will be saved as a template
	 */
	@track _selectedBlockToSaveAsTemplateIndex;

	/** 
	 * @description Tracks if the modal to save a block as a template block is displayed
	 */
	@track _displaySaveBlockAsTemplateBlockModal = false;
   
	/**
	 * @description Tracks if a block is being saved as a template block
	 */
	@track _isSavingBlockAsTemplateBlock = false;

	_adjustmentOptions = [
		{ label: 'Protein',		value: 'Protein' },
		{ label: 'Carbs',		value: 'Carbs' },
		{ label: 'Fat',			value: 'Fat' },
		{ label: 'Calories',	value: 'Calories' }
	];
	
	@track _adjustmentValue = 'Protein';

	/**
	 * @description The Id of the Plan we are updating
	 */
	@track _planId;

	/**
	 * @description The Original Plan Data
	 */
	@track _originalPlanData;

	/**
	 * @description The Updated Plan Data
	 */
	@track _updatedPlanData;

	/**
	 * @description Is the planner being loaded in
	 */
	@track _isLoading = false;

	/**
	 * @description Is the plan being saved
	 */
	@track _isSaving = false;

	/**
	 * @description Has the Plan data been loaded in?
	 */
	@track _planDataAvailable = false;

	/** 
	 * @description Has the Plan data been updated (updated values, added/removed items/blocks, etc.)
	 */
	@track _hasPlanBeenUpdated = false;

	/**
	 * @description The Active tab for adding a custom item
	 */
	@track _itemActiveTab = 'standard-item';
	
	/**
	 * @description The Order of the block that we are updating (New items, rename, etc.)
	 */
	@track _selectedBlockOrder;

	/**
	 * @description Tracks if the a new item is being added to the plan
	 */
	@track _isAddingNewItem = false;

	/**
	 * @description Are we updating a block name
	 */
	@track _isUpdatingBlockName = false;
	
	/**
	 * @description Are we adding a new block to the plan
	 */
	@track _isAddingNewBlock = false;

	/**
	 * @description Are we dialing up/down the plan
	 */
	@track _isDialingUpAndDown = false;

	/**
	 * @description The New block name
	 */
	@track _newBlockName;
	
	_typeOptions = [
		{ label: 'Amount',		value: 'Amount' },
		{ label: 'Percent',		value: 'Percent' }
	];

	@track _typeValue = 'Amount';

	@track _dialUpDialDownPlanData;
	
	@track _dialUpTarget = 0;
	
	/**
	 * @description Tracks the Ids of the blocks we want to delete
	 */
	@track _deletedBlocks = [];

	/**
	 * @description Tracks the Ids of the Items we want to delete
	 */
	@track _deletedItems = [];

	@track _targetIds = [];

	@track _targets;

    @api isForTracker = false;

	/* ---------- GETTERS ---------- */

	@api get isSavingOrLoading() {
		return this._isSaving === true || this._isLoading === true;
	}

	@api get targetIds() {
		return this._targetIds;
	}

    get isReferencePlan() {
        return this._originalPlanData.recordTypeDeveloperName === "Nutrition_Reference_Plan";
    }

    get isClientPlan() {
        return this._originalPlanData.recordTypeDeveloperName === "Nutrition_Client_Plan";
    }
    
	/**
	 * @description Is the target based on an amount
	 */
	get isAmountTarget() {
		return this._typeValue === 'Amount';
	}
	
	/**
	 * @description Is the target based on an percent
	 */
	get isPercentTarget() {
		return this._typeValue === 'Percent';
	}

	/**
	 * @description The Id of the Plan
	 */
	@api get planId() {
		return this._planId
	}

	/**
	 * @description Tracks the plan changes that have occurred
	 */
	get updatedPlanChanges() {
		return this.calculateDifference(this._originalPlanData, this._updatedPlanData);
	}

	/**
	 * @description Tracks the plan changes that have occurred
	 */
	get dialupPlanChanges() {
		return this.calculateDifference(this._originalPlanData, this._dialUpDialDownPlanData);
	}

	get tableClass() {

		let tableClass = "slds-table slds-table_cell-buffer slds-table_bordered slds-table_col-bordered";

        if(this.isForTracker === true) {
            tableClass += " plan-table__tracker";
        }
        
		if(this._hasPlanBeenUpdated === false) {
			tableClass += " plan-table__no-changes";
		} else {
			tableClass += " plan-table__changes";
		}

		if(typeof(this._targets) === "undefined") {
			tableClass += " plan-table__no-target";
		} else {
			tableClass += " plan-table__attached-target";

			if(this._targets.length == 1) {
				tableClass += " target-count__one";
			} else if(this._targets.length == 2) {
				tableClass += " target-count__two";
			} else if(this._targets.length == 3) {
				tableClass += " target-count__three";
			} else if(this._targets.length == 4) {
				tableClass += " target-count__four";
			} else if(this._targets.length == 5) {
				tableClass += " target-count__five";
			} else if(this._targets.length == 6) {
				tableClass += " target-count__six";
			} else if(this._targets.length == 7) {
				tableClass += " target-count__seven";
			} else if(this._targets.length == 8) {
				tableClass += " target-count__eight";
			} else if(this._targets.length == 9) {
				tableClass += " target-count__nine";
			} else if(this._targets.length == 10) {
				tableClass += " target-count__ten";
			} else if(this._targets.length == 11) {
				tableClass += " target-count__eleven";
			} else if(this._targets.length == 12) {
				tableClass += " target-count__twelve";
			}

		}

		return tableClass;
	}

	/**
	 * @description Returns the Record Type used for new custom template items
	 */
	get newCustomTemplateItemRecordType() {
		if(this._originalPlanData.recordTypeDeveloperName === "Nutrition_Reference_Plan") {
			return "Nutrition";
		} else if(this._originalPlanData.recordTypeDeveloperName === "Nutrition_Client_Plan"){
			return "Custom Nutrition";
		}	
	}

	get hasTargetInformation() {
		return typeof(this._targets) !== "undefined";
	}

	
	/* ---------- SETTERS ---------- */
	
	set targetIds(newTargetIds) {
		this._targetIds = newTargetIds;
		console.log("Target Ids: ", JSON.parse(JSON.stringify(this._targetIds)));
		this.RefreshTarget();
	}

	/** 
	 * @description Set the Plan Id
	 */
	set planId(newPlanId) {
		this._planId = newPlanId;
		this.refresh();
	}
    	
	/* ---------- CUSTOM METHODS ---------- */

	@api RefreshTarget() {
		GetTargetInformation({
			targetIds: this._targetIds
		}).then(result => {
			console.log("Target Information: ", JSON.parse(JSON.stringify(result)));
			this._targets = JSON.parse(JSON.stringify(result));
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Calculate the difference between 2 plan datas
	 */
	calculateDifference(originalPlanData, updatedPlanData) {
	
		let changes = {};

		let calorieDifference	= parseInt(updatedPlanData.calories.toFixed(0)) - parseInt(originalPlanData.calories.toFixed(0));
		let caloriePercentage	= parseInt(updatedPlanData.calories.toFixed(0)) / parseInt(originalPlanData.calories.toFixed(0));

		if(calorieDifference < 0) {
			changes.calorieDifference	= calorieDifference.toFixed(0);
			changes.caloriePercentage	= (caloriePercentage * 100).toFixed(0);
			changes.calorieClass		= 'calories__column changes_decrease';
		} else if(calorieDifference == 0) {
			changes.calorieDifference	= calorieDifference.toFixed(0);
			changes.caloriePercentage	= (caloriePercentage * 100).toFixed(0);
			changes.calorieClass		= 'calories__column changes_same';
		} else {
			changes.calorieDifference	= '+' + calorieDifference.toFixed(0);
			changes.caloriePercentage	= (caloriePercentage * 100).toFixed(0);
			changes.calorieClass		= 'calories__column changes_increase';
		}

		let carbsDifference		= parseInt(updatedPlanData.carbs.toFixed(0)) - parseInt(originalPlanData.carbs.toFixed(0));
		let carbsPercentage		= parseInt(updatedPlanData.carbs.toFixed(0)) / parseInt(originalPlanData.carbs.toFixed(0));
		
		if(carbsDifference < 0) {
			changes.carbsDifference	= carbsDifference.toFixed(0);
			changes.carbsPercentage	= (carbsPercentage * 100).toFixed(0);
			changes.carbsClass		= 'carbs__column changes_decrease';
		} else if(carbsDifference == 0) {
			changes.carbsDifference	= carbsDifference.toFixed(0);
			changes.carbsPercentage	= (carbsPercentage * 100).toFixed(0);
			changes.carbsClass		= 'carbs__column changes_same';
		} else {
			changes.carbsDifference	= '+' + carbsDifference.toFixed(0);
			changes.carbsPercentage	= (carbsPercentage * 100).toFixed(0);
			changes.carbsClass		= 'carbs__column changes_increase';
		}

		let fatDifference		= parseInt(updatedPlanData.fat.toFixed(0)) - parseInt(originalPlanData.fat.toFixed(0));
		let fatPercentage		= parseInt(updatedPlanData.fat.toFixed(0)) / parseInt(originalPlanData.fat.toFixed(0));
		
		if(fatDifference < 0) {
			changes.fatDifference	= fatDifference.toFixed(0);
			changes.fatPercentage	= (fatPercentage * 100).toFixed(0);
			changes.fatClass		= 'fat__column changes_decrease';
		} else if(fatDifference == 0) {
			changes.fatDifference	= fatDifference.toFixed(0);
			changes.fatPercentage	= (fatPercentage * 100).toFixed(0);
			changes.fatClass		= 'fat__column changes_same';
		} else {
			changes.fatDifference	= '+' + fatDifference.toFixed(0);
			changes.fatPercentage	= (fatPercentage * 100).toFixed(0);
			changes.fatClass		= 'fat__column changes_increase';
		}

		let proteinDifference	= parseInt(updatedPlanData.protein.toFixed(0)) - parseInt(originalPlanData.protein.toFixed(0));
		let proteinPercentage	= parseInt(updatedPlanData.protein.toFixed(0)) / parseInt(originalPlanData.protein.toFixed(0));
		
		if(proteinDifference < 0) {
			changes.proteinDifference	= proteinDifference.toFixed(0);
			changes.proteinPercentage	= (proteinPercentage * 100).toFixed(0);
			changes.proteinClass		= 'protein__column changes_decrease';
		} else if(proteinDifference == 0) {
			changes.proteinDifference	= proteinDifference.toFixed(0);
			changes.proteinPercentage	= (proteinPercentage * 100).toFixed(0);
			changes.proteinClass		= 'protein__column changes_same';
		} else {
			changes.proteinDifference	= '+' + proteinDifference.toFixed(0);
			changes.proteinPercentage	= (proteinPercentage * 100).toFixed(0);
			changes.proteinClass		= 'protein__column changes_increase';
		}


		return changes;

	}

	/**
	 * @description Do a deep clone for the object
	 */
	deepClone(obj) {
		return JSON.parse(JSON.stringify(obj));
	}

	/**
	 * @description HACK fix some bad data
	 */
	HACK_fixData(refPlanData) {
		for(let i = 0; i < refPlanData.blocks.length; i++) {
			refPlanData.blocks[i].order = i;
			for(let j = 0; j < refPlanData.blocks[i].items.length; j++) {
				refPlanData.blocks[i].items[j].order = j;
			}
		}
	}


	/**
	 * @description Refresh the Plan Data
	 */
	@api refresh() {
		this._isLoading = true;
		GetNutritionalPlan({
			refPlanId: this._planId
		}).then(result => {
			this._originalPlanData		= this.deepClone(result);
			this.HACK_fixData(this._originalPlanData);
			this._updatedPlanData		= this.processData(this.deepClone(this._originalPlanData));
			this._isLoading				= false;
			this._planDataAvailable		= true;
			this._hasPlanBeenUpdated	= false;
			console.log("SUCCESS: ", this.deepClone(this._updatedPlanData));
		}).catch(error => {
			this._isLoading = false;
			console.error("ERROR: ", this.deepClone(error));
		});
	}

	/**
	 * @description Process the plan data, by adding extra states, and values
	 */
	processData(refPlanData) {
		refPlanData.states = {};
		for(let i = 0; i < refPlanData.blocks.length; i++) {
			refPlanData.blocks[i].states = {
				isNewBlock: false,
			};
			refPlanData.blocks[i].values = {
				key: 'B-' + refPlanData.blocks[i].order
			};
			for(let j = 0; j < refPlanData.blocks[i].items.length; j++) {
				refPlanData.blocks[i].items[j].states = {
					isNewItem:		false,
					hasBeenUpdated: false
				};
				refPlanData.blocks[i].items[j].values = {
					key:				refPlanData.blocks[i].values.key + '-I-' + refPlanData.blocks[i].items[j].order,
					proteinPerUnit:		refPlanData.blocks[i].items[j].referenceProtein     / refPlanData.blocks[i].items[j].referenceQuantity,
					fatPerUnit:			refPlanData.blocks[i].items[j].referenceFat         / refPlanData.blocks[i].items[j].referenceQuantity,
					carbsPerUnit:		refPlanData.blocks[i].items[j].referenceCarbs       / refPlanData.blocks[i].items[j].referenceQuantity,
					caloriesPerUnit:	refPlanData.blocks[i].items[j].referenceCalories    / refPlanData.blocks[i].items[j].referenceQuantity
				};
			}
		}
		return refPlanData;
	}

	/**
	 * @description Update the Item value with a new value
	 */
	updateItemValue(refPlanData, blockOrder, itemOrder, newValue) {
		let itemQuantity = (newValue != null && isNaN(newValue) === false) ? newValue : 0;
		refPlanData.blocks[blockOrder].items[itemOrder].quantity	= itemQuantity;
		refPlanData.blocks[blockOrder].items[itemOrder].calories	= parseFloat(itemQuantity * refPlanData.blocks[blockOrder].items[itemOrder].values.caloriesPerUnit);
		refPlanData.blocks[blockOrder].items[itemOrder].carbs		= parseFloat(itemQuantity * refPlanData.blocks[blockOrder].items[itemOrder].values.carbsPerUnit);
		refPlanData.blocks[blockOrder].items[itemOrder].fat			= parseFloat(itemQuantity * refPlanData.blocks[blockOrder].items[itemOrder].values.fatPerUnit);
		refPlanData.blocks[blockOrder].items[itemOrder].protein		= parseFloat(itemQuantity * refPlanData.blocks[blockOrder].items[itemOrder].values.proteinPerUnit);

		refPlanData.blocks[blockOrder].items[itemOrder].states.hasBeenUpdated = true;
	}

	/**
	 * @description Update the Block value
	 */
	updateBlockValue(refPlanData, blockOrder) {
	
		let blockCalories	= 0;
		let blockCarbs		= 0;
		let blockFat		= 0;
		let blockProtein	= 0;

		for(let i = 0; i < refPlanData.blocks[blockOrder].items.length; i++) {
			blockCalories	+= parseFloat(refPlanData.blocks[blockOrder].items[i].calories);
			blockCarbs		+= parseFloat(refPlanData.blocks[blockOrder].items[i].carbs);
			blockFat		+= parseFloat(refPlanData.blocks[blockOrder].items[i].fat);
			blockProtein	+= parseFloat(refPlanData.blocks[blockOrder].items[i].protein);
		}

		refPlanData.blocks[blockOrder].calories	= blockCalories;
		refPlanData.blocks[blockOrder].carbs	= blockCarbs;
		refPlanData.blocks[blockOrder].fat		= blockFat;
		refPlanData.blocks[blockOrder].protein	= blockProtein;

	}

	/**
	 * @description Updated the Plan values
	 */
	 updatePlanValue(refPlanData) {
	 
		let planCalories	= 0;
		let planCarbs		= 0;
		let planFat			= 0;
		let planProtein		= 0;

		for(let i = 0; i < refPlanData.blocks.length; i++) {
			planCalories	+= parseFloat(refPlanData.blocks[i].calories);
			planCarbs		+= parseFloat(refPlanData.blocks[i].carbs);
			planFat			+= parseFloat(refPlanData.blocks[i].fat);
			planProtein		+= parseFloat(refPlanData.blocks[i].protein);
		}

		refPlanData.calories = planCalories;
		refPlanData.carbs	= planCarbs;
		refPlanData.fat		= planFat;
		refPlanData.protein	= planProtein;

	 }

	 /**
	  * @description Add Template Item to block
	  */
	 addTemplateItemToBlock(templateItem, blockOrder) {

		let newItem = {};
		newItem.name					    = templateItem.itemName;
		newItem.quantity					= templateItem.itemQuantity;
		newItem.calories					= templateItem.itemCalories;
		newItem.carbs						= templateItem.itemCarbs;
		newItem.fat							= templateItem.itemFat;
		newItem.protein						= templateItem.itemProtein;
        newItem.measurement                 = templateItem.itemMeasurement;

		
		newItem.referenceId					= templateItem.itemId;
		newItem.referenceItemName			= templateItem.itemName;
		newItem.referenceQuantity			= templateItem.itemQuantity;
		newItem.referenceCalories			= templateItem.itemCalories;
		newItem.referenceCarbs				= templateItem.itemCarbs;
		newItem.referenceFat				= templateItem.itemFat;
		newItem.referenceProtein			= templateItem.itemProtein;
		newItem.referenceMeasurement		= templateItem.itemMeasurement;
		newItem.referenceQuantityIncrement	= templateItem.quantityIncrement;
		newItem.referenceMinimumQuantity	= templateItem.minimumQuantity;
		
		newItem.states = {
			isNewItem:		true,
			hasBeenUpdated: false,
		}

		if(this._updatedPlanData.blocks[blockOrder].items.length === 0) {
			newItem.order		= 0;
		} else {
			newItem.order		= this._updatedPlanData.blocks[blockOrder].items[this._updatedPlanData.blocks[blockOrder].items.length - 1].order + 1;
		}

		newItem.values = {
			key:				this._updatedPlanData.blocks[blockOrder].values.key + '-I-' + newItem.order,
			proteinPerUnit:		newItem.referenceProtein / newItem.referenceQuantity,
			fatPerUnit:			newItem.referenceFat / newItem.referenceQuantity,
			carbsPerUnit:		newItem.referenceCarbs / newItem.referenceQuantity,
			caloriesPerUnit:	newItem.referenceCalories / newItem.referenceQuantity
		};

		this._updatedPlanData.blocks[blockOrder].items.push(newItem);

	 }

	 /**
	  * @description Add custom Item to Block
	  */
	 addCustomItemToBlock(customItem, blockOrder) {
	 
		let newItem = {};
		newItem.itemName					= customItem.fields.Name.value;
		newItem.quantity					= customItem.fields.Item_Quantity__c.value;
		newItem.calories					= customItem.fields.Calories__c.value;
		newItem.carbs						= customItem.fields.Carbs__c.value;
		newItem.fat							= customItem.fields.Fat__c.value;
		newItem.protein						= customItem.fields.Protein__c.value;
		
		newItem.templateId					= customItem.id;
		newItem.templateItemName			= customItem.fields.Name.value;
		newItem.templateQuantity			= customItem.fields.Item_Quantity__c.value;
		newItem.templateCalories			= customItem.fields.Calories__c.value;
		newItem.templateCarbs				= customItem.fields.Carbs__c.value;
		newItem.templateFat					= customItem.fields.Fat__c.value;
		newItem.templateProtein				= customItem.fields.Protein__c.value;
		newItem.templateMeasurement			= customItem.fields.Measurement__c.value;
		newItem.templateQuantityIncrement	= customItem.fields.Quantity_Increment__c.value;
		newItem.templateMinimumQuantity		= customItem.fields.Minimum_Quantity__c.value;
		
		newItem.states = {
			isNewItem: true,
			hasBeenUpdated: false
		}
		
		if(this._updatedPlanData.blocks[blockOrder].items.length === 0) {
			newItem.order		= 0;
		} else {
			newItem.order		= this._updatedPlanData.blocks[blockOrder].items[this._updatedPlanData.blocks[blockOrder].items.length - 1].order + 1;
		}

		newItem.values = {
			key:				this._updatedPlanData.blocks[blockOrder].values.key + '-I-' + newItem.order,
			proteinPerUnit:		newItem.referenceProtein / newItem.referenceQuantity,
			fatPerUnit:			newItem.referenceFat / newItem.referenceQuantity,
			carbsPerUnit:		newItem.referenceCarbs / newItem.referenceQuantity,
            caloriesPerUnit:    newItem.referenceCalories / newItem.referenceQuantity
		};

		this._updatedPlanData.blocks[blockOrder].items.push(newItem);


	 }

	/**
	 * @description Swap the order of two blocks
	 * 
	 * @param {Integer} blockOrderOne The Order of the first Block
	 * @param {Integer} blockOrderTwo The Order of the second Block
	 */
	swapBlocks(blockOneOrder, blockTwoOrder) {
		let blockOne	= JSON.parse(JSON.stringify(this._updatedPlanData.blocks[blockOneOrder]));
		let blockTwo	= JSON.parse(JSON.stringify(this._updatedPlanData.blocks[blockTwoOrder]));
		this._updatedPlanData.blocks[blockOneOrder] = blockTwo;
		this._updatedPlanData.blocks[blockTwoOrder] = blockOne;
	}
	
	/**
	 * @description Swap the order of two items
	 * 
	 * @param {Integer} blockOrderOne The Order of the first Block
	 * @param {Integer} blockOrderTwo The Order of the second Block
	 */
	swapItems(blockOrder, itemOneOrder, itemTwoOrder) {
		let itemOne		= JSON.parse(JSON.stringify(this._updatedPlanData.blocks[blockOrder].items[itemOneOrder]));
		let itemTwo		= JSON.parse(JSON.stringify(this._updatedPlanData.blocks[blockOrder].items[itemTwoOrder]));
		let blockKey	= this._updatedPlanData.blocks[blockOrder].values.key;
		this._updatedPlanData.blocks[blockOrder].items[itemOneOrder] = itemTwo;
		this._updatedPlanData.blocks[blockOrder].items[itemTwoOrder] = itemOne;
	}

	/**
	 * @description Update the orders of the blocks and the items
	 *
	 * @param {Object} refPlanData The Plan data we are updating
	 */
	updateBlockAndItemOrders(refPlanData) {
		
		for(let i = 0; i < refPlanData.blocks.length; i++) {

			refPlanData.blocks[i].order			= i;
			refPlanData.blocks[i].values.key	= 'B-' + i;

			for(let j = 0; j < refPlanData.blocks[i].items.length; j++) {
				refPlanData.blocks[i].items[j].order		= j;
				refPlanData.blocks[i].items[j].values.key	= refPlanData.blocks[i].values.key + '-I-' + j;
			}


		}

	}

	/**
	 * @description Remove a block from the plan
	 *
	 * @param {Integer} blockOrder The order of the block within the plan
	 */
	removeBlock(blockOrder) {
		//If this is a newly added block don't worry about updating it in the system
		if(this._updatedPlanData.blocks[blockOrder].states.isNewBlock === false) {
			this._deletedBlocks.push(this._updatedPlanData.blocks[blockOrder].id);
		}
		this._updatedPlanData.blocks.splice(blockOrder, 1);
	}
	
	/**
	 * @description Remove an item from a block in the plan
	 *
	 * @param {Integer} blockOrder The order of the block within the plan
	 */
	removeItem(blockOrder, itemOrder) {
		//If this is a newly added item don't worry about updating it in the system
		if(this._updatedPlanData.blocks[blockOrder].items[itemOrder].states.isNewItem == false) {
			this._deletedItems.push(this._updatedPlanData.blocks[blockOrder].items[itemOrder].id);
		}
		this._updatedPlanData.blocks[blockOrder].items.splice(itemOrder, 1);
	}

	/**
	 * @description Add template Block to Plan
	 *
	 * @param {Object} templateBlock The block we want to add
	 */
	addTemplateBlockToPlan(templateBlock) {

		let newBlock				= {};
		//blockId
		newBlock.name			= templateBlock.templateName;
		newBlock.items				= [];

		if(this._updatedPlanData.blocks.length === 0) {
			newBlock.order = 0;
		} else {
			newBlock.order = this._updatedPlanData.blocks[this._updatedPlanData.blocks.length - 1].order + 1;
		}

		newBlock.planId				= this._updatedPlanData.planId;
		newBlock.states				= {
			isNewBlock: true
		};
		newBlock.referenceBlockId		= templateBlock.templateId;
		newBlock.referenceCalories	    = templateBlock.templateCalories;
		newBlock.referenceCarbs		    = templateBlock.templateCarbs;
		newBlock.referenceFat		    = templateBlock.templateFat;
		newBlock.referenceProtein	    = templateBlock.templateProtein;
		newBlock.calories				= templateBlock.templateCalories;
		newBlock.carbs					= templateBlock.templateCarbs;
		newBlock.fat					= templateBlock.templateFat;
		newBlock.protein				= templateBlock.templateProtein;
		newBlock.values = {
			key: 'B-' + newBlock.order
		};

		for(let i = 0; i < templateBlock.templateItems.length; i++) {
			newBlock.items.push({
				name:				templateBlock.templateItems[i].templateName,
				order:					i,
				states: {
					isNewItem: true,
					hasBeenUpdated: false
				},

                measurement:            templateBlock.templateItems[i].templateMeasurement,
                referenceCalories:      templateBlock.templateItems[i].templateCalories,
                referenceCarbs:         templateBlock.templateItems[i].templateCarbs,
                referenceFat:           templateBlock.templateItems[i].templateFat,
                referenceProtein:       templateBlock.templateItems[i].templateProtein,
                referenceQuantity:      templateBlock.templateItems[i].templateQuantity,

				referenceId:			templateBlock.templateItems[i].templateId,
				referenceItemName:		templateBlock.templateItems[i].templateName,
				referenceItemBlockId:	templateBlock.templateItems[i].templateBlockId,
				referenceMeasurement:	templateBlock.templateItems[i].templateMeasurement,
				calories:				templateBlock.templateItems[i].templateBlockItemCalories,
				carbs:					templateBlock.templateItems[i].templateBlockItemCarbs,
				fat:					templateBlock.templateItems[i].templateBlockItemFat,
				protein:				templateBlock.templateItems[i].templateBlockItemProtein,
				quantity:				templateBlock.templateItems[i].templateBlockItemQuantity,
				values: {
					caloriesPerUnit:	templateBlock.templateItems[i].templateCalories / templateBlock.templateItems[i].templateQuantity,
					carbsPerUnit:		templateBlock.templateItems[i].templateCarbs / templateBlock.templateItems[i].templateQuantity,
					fatPerUnit:			templateBlock.templateItems[i].templateFat / templateBlock.templateItems[i].templateQuantity,
					proteinPerUnit:		templateBlock.templateItems[i].templateProtein / templateBlock.templateItems[i].templateQuantity,
					key:				newBlock.values.key + '-I-' + i
				}
			});
		}

		this._updatedPlanData.blocks.push(newBlock);

	}
	
	updateStartingValue(adjustmentValue, typeValue) {
		
		if(this._typeValue === 'Percent') {
			return 0;
		} else {
			if(this._adjustmentValue === 'Protein') {
				return parseFloat(this._dialUpDialDownPlanData.protein.toFixed(0));
			} else if(this._adjustmentValue === 'Carbs') {
				return parseFloat(this._dialUpDialDownPlanData.carbs.toFixed(0));
			} else if(this._adjustmentValue === 'Fat') {
				return parseFloat(this._dialUpDialDownPlanData.fat.toFixed(0));
			} else if(this._adjustmentValue === 'Calories') {
				return parseFloat(this._dialUpDialDownPlanData.calories.toFixed(0));
			}
		}
	}

    /**
     * @description Save a new Plan
     *
     * @param {Function} onSuccess Executes when the save was successful
     * @param {Function} onError Executes when the save has failed
     */
    @api SaveAs(onSuccess, onError) {

		this._isSaving = true;

        SaveNewPlan({
            nPlan: this._updatedPlanData
        }).then(result => {

			this._isSaving = false;

            if(onSuccess) {
                onSuccess({
                    newPlanId: result
                });
            }
        }).catch(error => {
            if(onError) {
                onError(error);
            }
        }); 

    }

	/**
	 * @description Save the changes to the plan to the database
	 */
	@api Save(onSuccess, onError) {
	
		this._isSaving = true;

        let hasItemsWith0Quantities = false;

        for(let i = 0; i < this._updatedPlanData.blocks.length; i++) {
            for(let j = 0; j < this._updatedPlanData.blocks[i].items.length; j++) {
                if(this._updatedPlanData.blocks[i].items[j].quantity === 0) {
                    hasItemsWith0Quantities = true;
                }
            }
        }

        if(hasItemsWith0Quantities === true) {
            this.fireEvent("0 Quantity found", "All Items require the quantity to be greater the 0 to save", "error");
        } else {
            UpdatePlan({
                refPlan:            this._updatedPlanData,
                deletedBlockIds:    this._deletedBlocks,
                deletedItemIds:     this._deletedItems
            }).then(result => {
			
				this._isSaving = false;

                if(onSuccess) {
                    onSuccess();
                }
            }).catch(error => {
                console.error("ERROR: ", JSON.parse(JSON.stringify(error)))
                if(onError) {
                    onError(error);
                }
            }); 
        }
    
	}

	fireEvent(title, message, variant, mode) {

		let toastEventData = {
			title,
			message
		};
		if(variant) {
			toastEventData.variant = variant;
		}      
		if(mode) {
			toastEventData.mode = mode;
		}
		const event = new ShowToastEvent(toastEventData);
        this.dispatchEvent(event);
	}

	/* ---------- CUSTOM EVENTS ---------- */

	onChangeTrainerPlanComments(event) {
		this._updatedPlanData.trainerComments = event.target.value;
	}

	/**
	 * @description Executes when the plan name gets updated
	 */
	onPlanNameUpdate(event) {
		this._updatedPlanData.name = event.target.value;

		console.log("Updated Name: ", this._updatedPlanData.name);

	}

	/**
	* @description Executes when the item quantity is updated
	*/
	onItemQuantityUpdate(event) {
		//Update the Item, its block, and the parent plan

        console.log("On Update Item")

		let blockOrder	= event.target.dataset.blockOrder;
		let itemOrder	= event.target.dataset.itemOrder;
		let newQuantity = event.detail.value;

        console.log("Block Order:  ", blockOrder);
        console.log("Item Order:   ", itemOrder);
        console.log("New Quantity: ", newQuantity);

        
		this._hasPlanBeenUpdated = true;
		this.updateItemValue(this._updatedPlanData, blockOrder, itemOrder, newQuantity);
		this.updateBlockValue(this._updatedPlanData, blockOrder);
		this.updatePlanValue(this._updatedPlanData);
	}

	/**
	 * @description Executes when the a New Item is going to be added
	 */
	onAddNewItem(event) {
		this._isAddingNewItem = true;
		this._selectedBlockOrder = event.target.dataset.blockOrder;
	}

	/**
	 * @description Executes when we want to close the new item modal
	 */
	onCloseNewItemModal(event) {
		this._isAddingNewItem		= false;
		this._selectedBlockOrder	= null;
	}

	/**
	 * @description Executes when changing new items tab
	 */
	onNewItemModeSelect(event) {
		this._itemActiveTab = event.target.value;
	}

	/**
	 * @description Executes when we want to save the new item
	 */
	onSaveNewItem(event) {
		
		//Add Standard Template Items onto the block
		if(this._itemActiveTab === 'standard-item') {
			let tmpItmModal			= this.template.querySelector('.template-item__modal');
			let selectedItems		= tmpItmModal.saveItems();
			this._isAddingNewItem	= false;
			for(let i = 0; i < selectedItems.length; i++) {
				 this.addTemplateItemToBlock(selectedItems[i], this._selectedBlockOrder);
			}
			this.updateBlockValue(this._updatedPlanData, this._selectedBlockOrder);
			this.updatePlanValue(this._updatedPlanData);
			this._hasPlanBeenUpdated = true;
			this._selectedBlockOrder	= null;

		//Add the new Custom Item onto the block
		} else if(this._itemActiveTab === 'custom-item') {
			let customItmModal			= this.template.querySelector('.custom-item__modal');
			customItmModal.Submit((result) => {
				this.addCustomItemToBlock(JSON.parse(JSON.stringify(result)), this._selectedBlockOrder);
				this.updateBlockValue(this._updatedPlanData, this._selectedBlockOrder);
				this.updatePlanValue(this._updatedPlanData);
				this._isAddingNewItem = false;
			this._hasPlanBeenUpdated = true;
				this._selectedBlockOrder	= null;
			},(error) => {
				this._isAddingNewItem = false;
				this._selectedBlockOrder	= null;
			});
		}

	}

	/**
	 * @description Update the Client Comments
	 */
	onUpdateClientComments(event) {
		this.clientComments = event.target.value;
	}

	/**
	 * @description Executes when the block gets renamed
	 */
	onRenameBlock(event) {
		this._isUpdatingBlockName	= true;
		this._selectedBlockOrder	= event.target.dataset.blockOrder;
		this._newBlockName			= this._updatedPlanData.blocks[this._selectedBlockOrder].name;
	}
	
	/**
	 * @description Executes when closing the rename block modal
	 */
	onCloseRenameModal(event) {
		this._isUpdatingBlockName = false;
		this._newBlockName = null;
	}
	
	/**
	 * @description Executes when the block name gets updated
	 */
	onBlockNameUpdate(event) {
		this._newBlockName = event.target.value;
	}

	/**
	 * @description Executes when the block name is saved
	 */
	onBlockNameSave(event) {
		this._updatedPlanData.blocks[this._selectedBlockOrder].name = this._newBlockName;
		this._newBlockName = null;
		this._isUpdatingBlockName = false;
	}

	/**
	 * @description Executes when we want to move the block up
	 */
	onMoveBlockUp(event) {
		let blockOrder = parseInt(event.target.dataset.blockOrder);
		if(blockOrder === 0) {
			return;
		}
		this.swapBlocks(blockOrder, blockOrder - 1);
		this.updateBlockAndItemOrders(this._updatedPlanData);
	}
	
	/**
	 * @description Executes when we want to move the block down
	 */
	onMoveBlockDown(event) {
		let blockOrder = parseInt(event.target.dataset.blockOrder);
		if(blockOrder === this._updatedPlanData.blocks.length - 1) {
			return;
		}
		this.swapBlocks(blockOrder, blockOrder + 1);
		this.updateBlockAndItemOrders(this._updatedPlanData);
	}
	
	/**
	 * @description Executes when we want to delete the block
	 */
	onBlockDelete(event) {
		let blockOrder = parseInt(event.target.dataset.blockOrder);
		this.removeBlock(blockOrder);
		this.updateBlockAndItemOrders(this._updatedPlanData);
		this.updatePlanValue(this._updatedPlanData);
		this._hasPlanBeenUpdated = true;
	}

	/**
	 * @description Executes when we want to move the item up
	 */
	onMoveItemUp(event) {
		let blockOrder	= parseInt(event.target.dataset.blockOrder);
		let itemOrder	= parseInt(event.target.dataset.itemOrder);
		if(itemOrder == 0 && blockOrder == 0) {
			return;
		}

		if(itemOrder == 0) {
			this.addItemToTopOfBlock(blockOrder, itemOrder, blockOrder - 1);
		} else {
			this.swapItems(blockOrder, itemOrder, itemOrder - 1);
		}
		this.updateBlockAndItemOrders(this._updatedPlanData);
	}

	addItemToTopOfBlock(cBlkIndex, cItmIndex, nBlkIndex) {
		let itemToMove = JSON.parse(JSON.stringify(this._updatedPlanData.blocks[cBlkIndex].items[cItmIndex]));
		this._updatedPlanData.blocks[cBlkIndex].items.splice(cItmIndex, 1);
		this._updatedPlanData.blocks[nBlkIndex].items.push(itemToMove);
	}

	addItemToBottomOfBlock(cBlkIndex, cItmIndex, nBlkIndex) {
		let itemToMove = JSON.parse(JSON.stringify(this._updatedPlanData.blocks[cBlkIndex].items[cItmIndex]));
		this._updatedPlanData.blocks[cBlkIndex].items.splice(cItmIndex, 1);
		this._updatedPlanData.blocks[nBlkIndex].items.unshift(itemToMove);
	}

	/**
	 * @description Executes when we want to move the item down
	 */
	onMoveItemDown(event) {
		let blockOrder	= parseInt(event.target.dataset.blockOrder);
		let itemOrder	= parseInt(event.target.dataset.itemOrder);
		if(itemOrder === this._updatedPlanData.blocks[blockOrder].items.length - 1 && blockOrder === this._updatedPlanData.blocks.length - 1) {
			return;
		}

		if(itemOrder == this._updatedPlanData.blocks[blockOrder].items.length - 1) {
			this.addItemToBottomOfBlock(blockOrder, itemOrder, blockOrder + 1);
		} else {
			this.swapItems(blockOrder, itemOrder, itemOrder + 1);
		}

		this.updateBlockAndItemOrders(this._updatedPlanData);
	}
	
	/**
	 * @description Executes when we want to delete the item
	 */
	onDeleteItem(event) {
		let blockOrder	= parseInt(event.target.dataset.blockOrder);
		let itemOrder	= parseInt(event.target.dataset.itemOrder);
		this.removeItem(blockOrder, itemOrder);
		this.updateBlockAndItemOrders(this._updatedPlanData);
		this.updateBlockValue(this._updatedPlanData, blockOrder);
		this.updatePlanValue(this._updatedPlanData);
		this._hasPlanBeenUpdated = true;
	}
	
	/**
	 * @description Executes when we want to open the modal that lets us add a block
	 *
	 * @param {Event} event The Event that executes this action
	 */
	onOpenAddBlockModal(event) {
		this._isAddingNewBlock = true;
	}
	
	/**
	 * @description Executes when we want to close the modal that lets us add a block
	 *
	 * @param {Event} event The Event that executes this action
	 */
	onCloseAddBlockModal(event) {
		this._isAddingNewBlock = false;
	}
	
	/**
	 * @description Executes when we want to add a new Template Block from the template block modal
	 *
	 * @param {Event} event The Event that executes this action
	 */
	onAddTemplateBlock(event) {
	
		let addTmpBlockToPlanModal = this.template.querySelector('.add-template-block-to-plan__modal');
		let selBlockPacket = addTmpBlockToPlanModal.getSelectedBlock();

		if(selBlockPacket.hasValidName === true && selBlockPacket.hasSelectedBlock === true) {
			this.addTemplateBlockToPlan(JSON.parse(JSON.stringify(selBlockPacket.selectedTemplateBlock)));
			this.updatePlanValue(this._updatedPlanData);
			this._hasPlanBeenUpdated = true;
			this._isAddingNewBlock = false;
		} else {
			if(selBlock.hasValidName === false) {
				this.fireEvent("Name not valid", "Need to insert a block name before continuing", "error");
			} else if(selBlock.hasSelectedBlock === false) {
				this.fireEvent("Block not selected", "Need to select a block before continuing", "error");
			}
		}

	}
	
	/**
	 * @description Executes when we want to close the dial Up/Down modal
	 *
	 * @param {Event} event The Event that executes this action
	 */
	onCloseDialUpAndDownModal(event) {
		this._isDialingUpAndDown = false;
	}
	
	/**
	 * @description Executes when we want to open the dial Up/Down modal
	 *
	 * @param {Event} event The Event that executes this action
	 */
	onOpenDialUpAndDownModal(event) {
		this._isDialingUpAndDown = true;
		this._dialUpDialDownPlanData = JSON.parse(JSON.stringify(this._updatedPlanData));
		this._dialUpTarget		= this.updateStartingValue();
	}

	onAdjustmentValueUpdate(event) {
		this._adjustmentValue	= event.target.value;
		this._dialUpTarget		= this.updateStartingValue();
	}

	onTypeValueUpdate(event) {
		this._typeValue			= event.target.value;
		this._dialUpTarget		= this.updateStartingValue();
	}

	onUpdateDialUpDownTarget(event) {

		console.log("New Target Value: ", event.target.value);

		if(event.target.value) {
			this._dialUpTarget = parseFloat(event.target.value);
		}

	}

	onCalculateDialUpDown(event) {

        console.log("Dial Up/Down");

		let targetAdjustment	= this._adjustmentValue;
		let upOrDown;
		//Set up the target value
		let targetValue = 0;
		if(this._typeValue === 'Amount') {
			targetValue = this._dialUpTarget;
		} else if(this._typeValue === 'Percent') {
			if(this._adjustmentValue === 'Protein') {
				targetValue = this._dialUpDialDownPlanData.protein * ((100 + this._dialUpTarget) / 100);
			} else if(this._adjustmentValue === 'Carbs') {
				targetValue = this._dialUpDialDownPlanData.carbs * ((100 + this._dialUpTarget) / 100);
			} else if(this._adjustmentValue === 'Fat') {
				targetValue = this._dialUpDialDownPlanData.fat * ((100 + this._dialUpTarget) / 100);
			} else if(this._adjustmentValue === 'Calories') {
				targetValue = this._dialUpDialDownPlanData.calories * ((100 + this._dialUpTarget) / 100);
			}
		}

        console.log("Target Adjustment: ", this._adjustmentValue);
        console.log("Target Value: ", targetValue);

		//If the target value already matches, then don't do anything.
		if(this._adjustmentValue === 'Protein' && targetValue === this._dialUpDialDownPlanData.protein) {
			return;
		} else if(this._adjustmentValue === 'Carbs' && targetValue === this._dialUpDialDownPlanData.varbs) {
			return;
		} else if(this._adjustmentValue === 'Fat' && targetValue === this._dialUpDialDownPlanData.fat) {
			return;
		} else if(this._adjustmentValue === 'Calories' && targetValue === this._dialUpDialDownPlanData.calories) {
			return;
		}

		
		//Check if we need to increase or decrease (If true it's increasing, otherwise it's decreasing)
		if(this._adjustmentValue === 'Protein') {
			upOrDown = targetValue > this._dialUpDialDownPlanData.protein;
		} else if(this._adjustmentValue === 'Carbs') {
			upOrDown = targetValue > this._dialUpDialDownPlanData.carbs;
		} else if(this._adjustmentValue === 'Fat') {
			upOrDown = targetValue > this._dialUpDialDownPlanData.fat;
		} else if(this._adjustmentValue === 'Calories') {
			upOrDown = targetValue > this._dialUpDialDownPlanData.calories;
		}

        console.log("Up or Down: ", upOrDown);

		dialUpDownLoop:while(true) {
			
			let updatedItem = false;

			for(let i = 0; i < this._dialUpDialDownPlanData.blocks.length; i++) {
				for(let j = 0; j < this._dialUpDialDownPlanData.blocks[i].items.length; j++) {

					const item = this._dialUpDialDownPlanData.blocks[i].items[j];

					//We only want to use the item if the value is above 0

					let useItem;

					//If increasing check that the item value can be adjusted
					if(upOrDown === true) {
						useItem =	(this._adjustmentValue === 'Protein' && item.protein > 0) ||
									(this._adjustmentValue === 'Carbs' && item.varbs > 0) ||
									(this._adjustmentValue === 'Fat' && item.fat > 0) ||
									(this._adjustmentValue === 'Calories' && item.calories > 0);
					} else {
						//If decreasing
						useItem =	(this._adjustmentValue === 'Protein' && item.protein > 0) ||
									(this._adjustmentValue === 'Carbs' && item.carbs > 0) ||
									(this._adjustmentValue === 'Fat' && item.fat > 0) ||
									(this._adjustmentValue === 'Calories' && item.calories > 0);
					}


					if(useItem === true) {

						let newQuantity = item.quantity + (item.referenceQuantityIncrement * (upOrDown ? 1 : -1));
						if(newQuantity < item.referenceMinimumQuantity) {
							newQuantity = item.referenceMinimumQuantity;
						}

						//If the new quantity is the same as the old quantity then move to the next item   
						if(item.referenceMinimumQuantity <= newQuantity && item.quantity !== newQuantity) {
							updatedItem = true;
							this.updateItemValue(this._dialUpDialDownPlanData, i, j, newQuantity);
							this.updateBlockValue(this._dialUpDialDownPlanData, i);
							this.updatePlanValue(this._dialUpDialDownPlanData, i);

							if(upOrDown === true) {
								if((this._adjustmentValue === 'Protein' && this._dialUpDialDownPlanData.protein >= targetValue) ||
									(this._adjustmentValue === 'Carbs' && this._dialUpDialDownPlanData.carbs >= targetValue) ||
									(this._adjustmentValue === 'Fat' && this._dialUpDialDownPlanData.fat >= targetValue) ||
									(this._adjustmentValue === 'Calories' && this._dialUpDialDownPlanData.calories >= targetValue)) {
									break dialUpDownLoop;
								}
							} else {
								if((this._adjustmentValue === 'Protein' && this._dialUpDialDownPlanData.protein <= targetValue) ||
									(this._adjustmentValue === 'Carbs' && this._dialUpDialDownPlanData.carbs <= targetValue) ||
									(this._adjustmentValue === 'Fat' && this._dialUpDialDownPlanData.fat <= targetValue) ||
									(this._adjustmentValue === 'Calories' && this._dialUpDialDownPlanData.calories <= targetValue)) {
									break dialUpDownLoop;
								}
							}
						}
					}
				}
			}

			//If for any reason we haven't updated any items then stop the loop
			if(updatedItem === false) {
				break;
			}

		}
		
	}

	/**
	 * @description Reset the Dial Up/Down values
	 *
	 * @param {Event} event The event that executes this
	 */
	onResetDialUpDownValues(event) {
		this._dialUpDialDownPlanData = JSON.parse(JSON.stringify(this._updatedPlanData));
	}

	onSaveDialUpDown(event) {
		this._updatedPlanData		= JSON.parse(JSON.stringify(this._dialUpDialDownPlanData));
		this._isDialingUpAndDown	= false;
		this._hasPlanBeenUpdated	= true;
	}

	/**
	 * @description Executes when opening the modal that allows for the SaveBlockAsTemplateBlock functionality
	 *
	 * @param {Event} event The event that executes this
	 */
	onOpenSaveBlockAsTemplateBlockModal(event) {
		let blockOrder								= parseInt(event.target.dataset.blockOrder);
		this._selectedBlockToSaveAsTemplateIndex	= blockOrder;
		this._displaySaveBlockAsTemplateBlockModal	= true;
	}

	/**
	 * @description Executes when closing the modal that allows for the SaveBlockAsTemplateBlock functionality
	 *
	 * @param {Event} event The event that executes this
	 */
	onCloseSaveBlockAsTemplateBlockModal(event) {
		event.stopPropagation();
		this._displaySaveBlockAsTemplateBlockModal = false;
	}
	
	/**
	 *	@description Executes when the Template Block is saved
	 */
	onSaveBlockAsTemplate(event) {

		this._isSavingBlockAsTemplateBlock = true;

		SaveNutritionBlockAsTemplateBlock({	
			nutRefBlock: JSON.parse(JSON.stringify(this._updatedPlanData.blocks[this._selectedBlockToSaveAsTemplateIndex]))
		}).then(result => {
			console.log("Successfully Saved Template Block");
			this._displaySaveBlockAsTemplateBlockModal	= false;
			this._isSavingBlockAsTemplateBlock			= false;

		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			this._displaySaveBlockAsTemplateBlockModal	= false;
			this._isSavingBlockAsTemplateBlock			= false;
		});
	}



}