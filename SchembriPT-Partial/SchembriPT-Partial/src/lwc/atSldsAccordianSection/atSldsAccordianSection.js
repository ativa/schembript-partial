import { LightningElement, api, track } from 'lwc';

export default class AtSldsAccordianSection extends LightningElement {

    /**
     * @description Is the accordian section open?
     */
    @api isOpen = false;

	get sectionClass() {
		if(this.isOpen === true) {
			return "slds-accordion__section slds-is-open";
		} else {
			return "slds-accordion__section";
		}
	}

	onAccordionSectionToggle() {
		this.isOpen = !this.isOpen;
	}

}