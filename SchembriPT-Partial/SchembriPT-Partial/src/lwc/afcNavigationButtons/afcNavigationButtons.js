import { LightningElement, api, track, wire } from 'lwc';
import FlowNavRequestChannel from '@salesforce/messageChannel/FlowNavRequestChannel__c';
import FlowNavResponseChannel from '@salesforce/messageChannel/FlowNavResponseChannel__c';
import { subscribe, publish, MessageContext } from 'lightning/messageService';
import { 
	FlowAttributeChangeEvent, 
	FlowNavigationBackEvent,
	FlowNavigationNextEvent,
	FlowNavigationPauseEvent,
	FlowNavigationFinishEvent
} from 'lightning/flowSupport';

export default class AfcNavigationButtons extends LightningElement {

	subscription = null;

	/**
	 * @description Available Flow Actions
	 */
    @api availableActions = [];
    
    @wire(MessageContext)
    messageContext;

    /**
     * The Labels of the buttons
     */
    @api buttonLabels;

    /**
     * The Navigation Actions of the Buttons
     */
    @api buttonActions;

    /**
     * @description The Values of the buttons
     */
    @api buttonValues;

    /**
     * @description The Variants of the buttons
     */
    @api buttonVariants;

    /**
     * @description The Action of the button that was selected
     */
    @api selectedAction;

    /**
     * @description The Value of the button that was selected
     */
    @api selectedValue;

    /**
     * @description 
     */
    @api activeFieldsets = 0;
	
	/**
	 * @description A Unique key that identitifes the the channel to use for this field set
	 */
	@api channelKey = '';

    @track _submissionsReceived = 0;

    get flowButtons() {
        
        let buttonLabels    = this.buttonLabels.split(";");
        let buttonActions   = this.buttonActions.split(";");
        let buttonValues    = this.buttonValues.split(";");
        let buttonVariants  = this.buttonVariants.split(';');

        let buttons = [];

        let index = 0;
        while(index < buttonLabels.length) {

            buttons.push({
                label:      buttonLabels[index],
                action:     buttonActions[index],
                value:      buttonValues[index],
                variant:    buttonVariants[index],
                order:      index
            });

            index += 1;

        }

        console.log("Flow Buttons: ", JSON.parse(JSON.stringify(buttons)));

        return buttons;

    }

    onButtonSelect(event) {

        let action  = event.target.dataset.action;
        let value   = event.target.dataset.value;

        this.selectedAction = action;
        this.selectedValue    = value;

        this._submissionsReceived = 0;

		let messagePacket = {
			channelKey: this.channelKey
		};

        //Publish Changes
        publish(this.messageContext, FlowNavRequestChannel, messagePacket);

    }

	connectedCallback() {

		if (this.subscription) {
            return;
		}
		
        this.subscription = subscribe(this.messageContext, FlowNavResponseChannel, (message) => {
            console.log("Submission Received");
			
			//If the channel key is the same, receive the submission
			if(message.channelKey === this.channelKey) {
			
				this._submissionsReceived += 1;

				console.log(this._submissionsReceived + " : " + this.activeFieldsets);

				if(this._submissionsReceived === this.activeFieldsets) {
                

					let navigateEvent = null;
		
					if(this.selectedAction === 'NEXT' && this.availableActions.find(act => act === 'NEXT')) {
						navigateEvent = new FlowNavigationNextEvent(); // Navigate to the next screen
					} else if(this.selectedAction === 'NEXT' && this.availableActions.find(act => act === 'FINISH')) {
						navigateEvent = new FlowNavigationFinishEvent(); // Navigate to the finish screen
					} else if(this.selectedAction === 'BACK' && this.availableActions.find(act => act === 'BACK')) {
						navigateEvent = new FlowNavigationBackEvent(); // Navigate to the previous screen
					}
                
					this.dispatchEvent(navigateEvent);
                
				}

			}

        });
	}


}