import { LightningElement, api, track, wire }	from 'lwc';
import { getObjectInfo }						from 'lightning/uiObjectInfoApi';
import Photo__c									from '@salesforce/schema/Photo__c';
import UploadFileToDropbox						from '@salesforce/apex/CommunityCheckInController.UploadFileToDropbox';
import GetAllMultiMediaFiles					from '@salesforce/apex/CommunityCheckInController.GetAllMultiMediaFiles';
import DeleteMultiMediaFile						from '@salesforce/apex/CommunityCheckInController.DeleteMultiMediaFile';

/**
 * @description Add MultiMedia files for the Client
 */
export default class Cci_AddMultiMediaFiles extends LightningElement {

	/* --------------- VARIABLES --------------- */

	@track _isLoading = false;
	
	/**
	 * @description Tracks if a new file is being saved
	 */
	@track _isSavingFile = false;

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;

	/**
	 * @description Holds the Record Type for the Other Photos
	 */ 
	@track _otherPhotoRecType;

	/**
	 * @description The Id of the New file
	 */ 
	@track _newFileId;

	/**
	 * @description The Id of the file that we want to delete
	 */
	@track _fileToDelete;

	/**
	 * @description Are we displaying the Add Item Panel 
	 */
	@track _displayAddItemPanel = false;

	/**
	 * @description All MultiMedia Files that relate to the Check-In
	 */
	@track _allMultiMediaFiles = [];

	/* --------------- GETTERS --------------- */
	
	/**
	 * @description The Id of the Check-In
	 */
	@api get checkInId() {
		return this._checkInId;
	}
	
	/**
	 * @description Tracks if the Delete Item Panel is open
	 */
	get _displayDeleteItemPanel() {
		return typeof this._fileToDelete !== 'undefined'
	}

	/**
	 * @description Is all the data loaded
	 */
	get allDataIsLoaded() {
		return typeof this._otherPhotoRecType !== 'undefined' && typeof this._checkInId !== 'undefined';
	}

	/**
	 * @description Tracks if a new file an be submitted or not
	 */
	get cantSubmitNewFile() {
		return typeof this._newFileId === 'undefined';
	}

	get isFileUploaded() {
		return typeof this._newFileId !== 'undefined';
	}

	/* --------------- SETTERS --------------- */

	/**
	 * @description The Id of the Check-In
	 * @param {String} newCheckInId The new Id of the Check-In
	 */
	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		//Refresh Data
		this.refreshData(); 
	}

	/* --------------- CUSTOM METHODS --------------- */

	/**
	 * @description Refresh the Multimedia Files
	 */
	@api refreshData(onSuccess, onError) {
	
		GetAllMultiMediaFiles({
			checkInId: this._checkInId
		}).then(result => {
			this._allMultiMediaFiles = JSON.parse(JSON.stringify(result));

			if(onSuccess) {
				onSuccess();
			}

		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)) );
			
			if(onError) {
				onError();
			}
		});

	}

	/**
	 * @description Delete File
	 */
	deleteFile(fileId) {
	
		this._isLoading = true;

		DeleteMultiMediaFile({
			fileId: fileId
		}).then(result => {
			this._fileToDelete = undefined;
			this.refreshData(() => {
				this._isLoading = false;
			});
			
		}).catch(error => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
		});
	}
	
	/* --------------- CUSTOM EVENTS --------------- */

	onMultiMediaPanelClose() {
        this.dispatchEvent(new CustomEvent('close'));
	}

	onCancelDeleteFile() {
		this._fileToDelete = undefined;
	}

	onConfirmDeleteFile() {
		this.deleteFile(this._fileToDelete);
	}	

	/**
	 * @description Remove File
	 */
	onRemoveFile(event) {
		let fileId			= event.target.dataset.itemId;
		this._fileToDelete	= fileId;
	}

	onDisplayAddMultiMediaItemPanel(event) {
		this._displayAddItemPanel	= true;
		this._newFileId				= undefined;
	} 

	onCloseAddMultiMediaItemPanel(event) {
		this._displayAddItemPanel	= false;
		this._newFileId				= undefined;
	}

	onFileUploadCompleted(event) {
		console.log("File Successfully Uploaded: ", JSON.parse(JSON.stringify(event.detail)));
		this._newFileId = event.detail.files[0].documentId;
	}

	onPhotoSubmissionSuccess(event) {

		this._isSavingFile = true;

		console.log("Successfully Created Photo: ", JSON.parse(JSON.stringify(event.detail)));

		let newPhoto = JSON.parse(JSON.stringify(event.detail));

		UploadFileToDropbox({
			documentId: this._newFileId,
			photoId:	newPhoto.id,
			checkInId:	this._checkInId
		}).then(result => {
			console.log("[cci_AddMultiMediaFiles] Successfully Logged to Dropbox");
			this._isSavingFile			= false;
			this._displayAddItemPanel	= false;
			this._newFileId				= undefined;
			this.refreshData();
		}).catch(error => {
			console.error("[cci_AddMultiMediaFiles] ERROR: ", JSON.parse(JSON.stringify(error)) );
			this._isSavingFile = false;
		}); 

	}

	@wire(getObjectInfo, { objectApiName: Photo__c })
	getRecordTypeData({ data, error }) {

		if(data) {
			
			for(let recId in data.recordTypeInfos) {
				let recType = data.recordTypeInfos[recId];
				if(recType.name === "Client Other Photos") {
					console.log("Client Other Photos Record Type: ", JSON.parse(JSON.stringify(recType)));
					this._otherPhotoRecType = recType;
				}
			}

		} else if(error) {
			console.error("[cci_AddMultiMediaFiles] ERROR: ", JSON.parse(JSON.stringify(error)) );
		}

	}



}