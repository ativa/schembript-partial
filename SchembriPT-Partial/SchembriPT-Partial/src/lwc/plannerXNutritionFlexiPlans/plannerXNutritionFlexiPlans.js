import { LightningElement, api, track } from 'lwc';
import GetFlexiPlanData					from '@salesforce/apex/AT_LWCC_PlannerNutritionFlexiPlans.GetFlexiPlanData';
import GetNutritionalTargets			from '@salesforce/apex/AT_LWCC_PlannerNutritionFlexiPlans.GetNutritionalTargets';
import SaveNutritionalTargets			from '@salesforce/apex/AT_LWCC_PlannerNutritionFlexiPlans.SaveNutritionalTargets';
import UpdateFlexiPlansLocked			from '@salesforce/apex/AT_LWCC_PlannerNutritionFlexiPlans.UpdateFlexiPlansLocked';

export default class PlannerXNutritionFlexiPlans extends LightningElement {

	/* ---------- VARIABLES ---------- */

	_assignToDaysOptions = [
		{
			label: "Monday",
			value: "Monday"
		},
		{
			label: "Tuesday",
			value: "Tuesday"
		},
		{
			label: "Wednesday",
			value: "Wednesday"
		},
		{
			label: "Thursday",
			value: "Thursday"
		},
		{
			label: "Friday",
			value: "Friday"
		},
		{
			label: "Saturday",
			value: "Saturday"
		},
		{
			label: "Sunday",
			value: "Sunday"
		}
	];

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;

	/**
	 * @description Holds checkInInformation
	 */
	@track _checkInInformation = {};

	/**
	 * @description Can the Plans have days assigned to them
	 */
	@track _canAssignDaysToPlan = false;

	/**
	 * @description The original target data
	 */
	@track _originalTargetsData;

	/**
	 * @description The updated target data
	 */
	@track _updatedTargetsData = [];

	/**
	 * @description A list of targets to delete
	 */
	@track _targetsToDelete = [];

	/**
	 * @description Is the Plan data being loaded
	 */
	@track _isLoading = false;

	/**
	 * @description Is the Plan data being saved
	 */
	@track _isSaving = false;

	/**
	 * @description All allergy options for the selected template
	 */
	@track _selectedTemplatesAllergyOptions = [];

	/**
	 * @description All restriction options for the selected template
	 */
	@track _selectedTemplatesRestrictionOptions = [];

	
	/* ---------- GETTERS ---------- */

	/**
	 * @description Tracks if we are loading/saving any information
	 */
	get _isSavingOrLoading() {
		return this._isLoading === true || this._isSaving === true;
	}

	/**
	 * @description Returns the Id of the Check-In
	 */
	@api get checkInId() {
		return this._checkInId;
	}

	get _calorieRangeTargets() {
		let calorieRangeTargets = [];
		if(this._updatedTargetsData) {
			for(let i = 0; i < this._updatedTargetsData.length; i++) {
				calorieRangeTargets.push({
					order: i,
					label:		this._updatedTargetsData[i].name,
					protein:	this._updatedTargetsData[i].protein,
					carbs:		this._updatedTargetsData[i].carbs,
					fat:		this._updatedTargetsData[i].fat,
					calories:	this._updatedTargetsData[i].calories
				});
			}
		}
		return calorieRangeTargets;
	}

	get _activeAllergies() {
		let actAllergies = [];
		for(let i = 0; i < this._selectedTemplatesAllergyOptions.length; i++) {
			if(this._selectedTemplatesAllergyOptions[i].checked === true) {
				actAllergies.push(this._selectedTemplatesAllergyOptions[i].value);
			}
		}
		return actAllergies;
	}

	get _activeRestrictions() {
		let actRestrictions = [];
		for(let i = 0; i < this._selectedTemplatesRestrictionOptions.length; i++) {
			if(this._selectedTemplatesRestrictionOptions[i].checked === true) {
				actRestrictions.push(this._selectedTemplatesRestrictionOptions[i].value);
			}
		}
		return actRestrictions;
	}
	
	/* ---------- SETTERS ---------- */

	/**
	 * @description Set the Id of the Check-In
	 * @param {String} newCheckInId The new Check-In Id
	 */
	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		this.getFlexiPlanData();
	}

	/* ---------- CUSTOM METHODS ---------- */

	@api getFlexiPlanData() {
		this._isLoading = true;
		GetFlexiPlanData({
			checkInId: this._checkInId
		}).then(result => {
			console.log("Flexi Plan Data: ", JSON.parse(JSON.stringify(result)));
			this._checkInInformation					= JSON.parse(JSON.stringify(result.checkInInfo));
			this._canAssignDaysToPlan					= result.canAssignToDays;
			this._originalTargetsData					= JSON.parse(JSON.stringify(result.nutritionTargets));
			this._updatedTargetsData					= this.processTargets(this._originalTargetsData);
			this._selectedTemplatesAllergyOptions		= this.processPicklistOptions(result.allergies);
			this._selectedTemplatesRestrictionOptions	= this.processPicklistOptions(result.dietaryRestrictions);

			console.log("Template Allergies: ", JSON.parse(JSON.stringify(this._selectedTemplatesAllergyOptions)));
			console.log("Template Restrictions: ", JSON.parse(JSON.stringify(this._selectedTemplatesRestrictionOptions)));

			this._isLoading								= false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Process the picklist options
	 */
	processPicklistOptions(options) {
		let newOp = [];
		for(let i = 0; i < options.length; i++) {
			newOp.push({
				label:		options[i].label,
				value:		options[i].value,
				checked:	false
			});
		}
		return newOp;
	}

	/**
	 * @description Refresh the targets being used
	 */
	@api refreshTargets() {

		this._isLoading = true;
		GetNutritionalTargets({
			checkInId: this._checkInId
		}).then(result => {
			this._originalTargetsData	= JSON.parse(JSON.stringify(result));
			this._updatedTargetsData	= this.processTargets(this._originalTargetsData);
			console.log("Nutritional Targets: ", JSON.parse(JSON.stringify(this._updatedTargetsData)));
			this._isLoading = false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/**
	 * @description Reorder the targets within the list
	 */
	reorderTargets() {
		for(let i = 0; i < this._updatedTargetsData.length; i++) {
			this._updatedTargetsData[i].values.order = i;
		}
	}

	/**
	 * @description Add additional states and values
	 */
	processTargets(originalTargets) {
		let oTargets = JSON.parse(JSON.stringify(originalTargets));
		for(let i = 0; i < oTargets.length; i++) {
			oTargets[i].states = {
				isNewTarget: false
			};
			oTargets[i].values = {
				order:					i,
				hasMondaySelected:		typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Monday'),
				hasTuesdaySelected:		typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Tuesday'),
				hasWednesdaySelected:	typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Wednesday'),
				hasThursdaySelected:	typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Thursday'),
				hasFridaySelected:		typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Friday'),
				hasSaturdaySelected:	typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Saturday'),
				hasSundaySelected:		typeof(oTargets[i].assignedTo) !== 'undefined' && oTargets[i].assignedTo.includes('Sunday'),
			};
		}
		return oTargets;
	}

	/**
	 * @description Add a new target
	 */
	addNewTarget() {
		let newTarget = {
			name:		    "Target " + (this._updatedTargetsData.length + 1),
            targetPoolType: "Calorie Range",
			checkInId:	    this._checkInId,
			calories:	    0,
			carbs:		    0,
			fat:		    0,
			protein:	    0,
			values: {
				order:					this._updatedTargetsData.length,
				hasMondaySelected:		false,
				hasTuesdaySelected:		false,
				hasWednesdaySelected:	false,
				hasThursdaySelected:	false,
				hasFridaySelected:		false,
				hasSaturdaySelected:	false,
				hasSundaySelected:		false
			},
			states: {
				isNewTarget: true
			}
		};
		this._updatedTargetsData.push(newTarget);
	}

	/**
	 * @description Update the calorie value for the target
	 */
	updateCalorieTargetValue(targetOrder) {
		let protein = this._updatedTargetsData[targetOrder].protein;
		let carbs	= this._updatedTargetsData[targetOrder].carbs;
		let fat		= this._updatedTargetsData[targetOrder].fat;
		this._updatedTargetsData[targetOrder].calories = (4 * protein) + (4 * carbs) + (9 * fat);
	}
	
	removeValue(list, value) {
		for(let i = 0; i < list.length; i++) {
			if(list[i] === value) {
				list.splice(i, 1);
			}
		}
	}

	toggleDay(targetOrder, day) {

		console.log("Target Order: ", targetOrder);
		console.log("Day: ", day);

		for(let i = 0; i < this._updatedTargetsData.length; i++) {
			if(i === targetOrder) {

				//Update the State to true
				if(day === "Monday") {		this._updatedTargetsData[i].values.hasMondaySelected = true; } 
				if(day === "Tuesday") {		this._updatedTargetsData[i].values.hasTuesdaySelected = true; }
				if(day === "Wednesday") {	this._updatedTargetsData[i].values.hasWednesdaySelected = true; } 
				if(day === "Thursday") {	this._updatedTargetsData[i].values.hasThursdaySelected = true; } 
				if(day === "Friday") {		this._updatedTargetsData[i].values.hasFridaySelected = true; } 
				if(day === "Saturday") {	this._updatedTargetsData[i].values.hasSaturdaySelected = true; } 
				if(day === "Sunday") {		this._updatedTargetsData[i].values.hasSundaySelected = true; }

				if(this._updatedTargetsData[i].assignedTo && this._updatedTargetsData[i].assignedTo.includes(day) === false) {
					this._updatedTargetsData[i].assignedTo.push(day);
				} else {
					this._updatedTargetsData[i].assignedTo = [day]
				}

			} else {
				if(day === "Monday") {		this._updatedTargetsData[i].values.hasMondaySelected = false; } 
				if(day === "Tuesday") {		this._updatedTargetsData[i].values.hasTuesdaySelected = false; }
				if(day === "Wednesday") {	this._updatedTargetsData[i].values.hasWednesdaySelected = false; } 
				if(day === "Thursday") {	this._updatedTargetsData[i].values.hasThursdaySelected = false; } 
				if(day === "Friday") {		this._updatedTargetsData[i].values.hasFridaySelected = false; } 
				if(day === "Saturday") {	this._updatedTargetsData[i].values.hasSaturdaySelected = false; } 
				if(day === "Sunday") {		this._updatedTargetsData[i].values.hasSundaySelected = false; }

				if(this._updatedTargetsData[i].assignedTo && this._updatedTargetsData[i].assignedTo.includes(day)) {
					this.removeValue(this._updatedTargetsData[i].assignedTo, day);
				}

			}
		}

		console.log("Updated Targets Data: ", JSON.parse(JSON.stringify(this._updatedTargetsData)));

	}

	saveTargets(onSuccess, onError) {
		this._isSaving = true;
		let tData = JSON.parse(JSON.stringify(this._updatedTargetsData));

		SaveNutritionalTargets({
			nutritionalTargets: tData,
			targetsToDelete:	this._targetsToDelete
		}).then(result => {
			if(onSuccess) {
				onSuccess();
			}
			this._isSaving = false;
			this.refreshTargets();
		}).catch(error => {
			if(onError) {
				onError(error);
			}
		});
	}

	_refreshPlannerValidation() {
		let plannerValidaton = this.template.querySelector('.planner-validation');
		plannerValidaton.Refresh();
	}

	_saveToDatabase() {
		//Save the targets, and refresh the template pool
		this.saveTargets(() => {
			let templatePool = this.template.querySelector('.templatePool');
			templatePool.Refresh(() => {
				this.refreshTargets();
				this._refreshPlannerValidation();
			}, (error) => {
				console.error("Error: ", JSON.parse(JSON.stringify(error)));
			});
		}, (error) => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	/* ---------- CUSTOM EVENTS ---------- */

	_flexiPlanLockedUpdating = false;

	onUpdateFlexiPlanLocked(event) {
		this._checkInInformation.flexiPlansLocked = event.target.checked;
		this._flexiPlanLockedUpdating = true;
		UpdateFlexiPlansLocked({
			checkInId:	this._checkInInformation.id,
			isLocked:	this._checkInInformation.flexiPlansLocked
		}).then(result => {
			console.log("Update Flexi Plans Locked");
			this._flexiPlanLockedUpdating = false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});


	}

	onTemplatePoolChange(event) {
		this.refreshTargets();
		this._refreshPlannerValidation();
	}

	/**
	 * @description Executed when an allergy is selected
	 */
	onAllergySelect(event) {
        const allergy	= event.target.name;
        const selected	= event.target.checked;
		for(let i = 0; i < this._selectedTemplatesAllergyOptions.length; i++) {
			if(this._selectedTemplatesAllergyOptions[i].value === allergy) {
				this._selectedTemplatesAllergyOptions[i].checked = selected;
			}
		}
	}

	/**
	 * @description Executed when a restiction is selected
	 */
	onRestrictionSelect(event) {
        const restriction	= event.target.name;
        const selected		= event.target.checked;
		for(let i = 0; i < this._selectedTemplatesRestrictionOptions.length; i++) {
			if(this._selectedTemplatesRestrictionOptions[i].value === restriction) {
				this._selectedTemplatesRestrictionOptions[i].checked = selected;
			}
		}
	}

	/**
	 * @description Executes when a new target is added
	 */
	onAddNewTarget(event) {
		this.addNewTarget();
		//Save the targets, and refresh the template pool
		this._saveToDatabase();
	}

	/**
	 * @description Executes when the user wants to delete a target
	 */
	onDeleteTarget(event) {
		let order = parseInt(event.target.dataset.order);
		if(this._updatedTargetsData[order].id) {
			this._targetsToDelete.push(this._updatedTargetsData[order].id);
		}
		this._updatedTargetsData.splice(order,1);
		this.reorderTargets();
		//Save the targets, and refresh the template pool
		this._saveToDatabase();
	}

	/**
	 * @description Executes when a target name gets updated
	 */
	onTargetNameUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		this._updatedTargetsData[order].name	= event.target.value;
		this._saveToDatabase();
	}

	/**
	 * @description Executes when a target protein gets updated
	 */
	onTargetProteinUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		this._updatedTargetsData[order].protein = parseFloat(event.target.value);
		this.updateCalorieTargetValue(order);
		this._saveToDatabase();
	}
	
	/**
	 * @description Executes when a target carbs gets updated
	 */
	onTargetCarbsUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		this._updatedTargetsData[order].carbs	= parseFloat(event.target.value);
		this.updateCalorieTargetValue(order);
		this._saveToDatabase();
	}
	
	/**
	 * @description Executes when a target fat gets updated
	 */
	onTargetFatUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		this._updatedTargetsData[order].fat		= parseFloat(event.target.value);
		this.updateCalorieTargetValue(order);
		this._saveToDatabase();
	}
	
	/**
	 * @description Executes  when a day gets selected
	 */
	onSelectDay(event) {
		let order	= parseInt(event.target.dataset.order);
		let day		= event.target.dataset.day;
		this.toggleDay(order, day);
		this._saveToDatabase();
	}


}