import { LightningElement, api, track } from 'lwc';

import getClientSummary				from '@salesforce/apex/Planner.getClientSummary';
import getPlans						from '@salesforce/apex/Planner.getPlans';
import UpdateNutritionPlanningMode	from '@salesforce/apex/Planner.UpdateNutritionPlanningMode';

export default class ClientInfo extends LightningElement {
    
	/* ---------- VARIABLES ---------- */

	/**
	 * @description The nutrition planning mode for the current Check-In
	 */
	@track _nutritionPlanningMode;

	/**
	 * @description Is the planning mode field on the checkIn readonly
	 */
	@api readonlyPlanningMode = false;

	/**
	 * @description The Nutrition Planning mode options for the Check-In
	 */
	_nutritionPlanningModeOptions = [
		{ label: 'Carb-Cycle', value: 'Carb-Cycle' },
		{ label: 'Flexi Plans', value: 'Flexi Plans' },
		{ label: 'Guided Plans', value: 'Guided Plans' }
	];

	/* ---------- GETTERS ---------- */
	/* ---------- SETTERS ---------- */
	/* ---------- CUSTOM METHODS ---------- */

	/**
	 * @description Fire a mode change event
	 */
	fireModeChange(previousMode, newMode) {
		const modeChangeEvent = new CustomEvent('modechange', { 
			detail: { previousMode, newMode },
			bubbles: true,
			composed: true
		});
		this.dispatchEvent(modeChangeEvent);
	}

	/** 
	 * @description Executes when the planning mode is updated
	 *
	 * @param {Event} event The event that executes this
	 */
	onUpdatePlanningMode(event) {
		let previousMode	= this._nutritionPlanningMode;
		let newMode			= event.target.value;
		this._nutritionPlanningMode = newMode;
		console.log("New Planning Mode: ", newMode);
		UpdateNutritionPlanningMode({
			checkInId:			this._checkInID,
			newPlanningMode:	this._nutritionPlanningMode
		}).then(result => {
			console.log("Updated Nutrition Planning Mode");
			this.fireModeChange(previousMode, newMode);
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});

	}

	/* ---------- CUSTOM EVENTS ---------- */
	/* ---------- LWC EVENTS ---------- */

	@track _checkInID;


	/**
	 * @description If true then hide most of the information
	 */
    @api collapsed = false;

    @api embedded = false;

	@api allergiesDisabled = false;

    @track mode = 'full';

	/**
	 * @description Has the client information been loaded
	 */
    @track loaded = false;

    @track info;
    @track plans;
    @track allergies;
    @track restrictions;
    @track likes;
    @track dislikes;

	@track nutritionPlanStartDate;

	@track nutritionPlanStartDay;

	/**
	 * @description If true, use the slot field for the allergies row
	 * HACK 
	 */
	@api useAllergiesSlot = false;

	/**
	 * @description If true, use the slot field for the dietary row
	 * HACK
	 */
	@api useDietarySlot = false;

    @api get checkInId() {
        return this._checkInID;
    }

    set checkInId(newID) {
        if (this.embedded) this.mode = 'embedded';

        this._checkInID = newID;

		//When the Check-In Id is updated, then update the Check-In data
		this.refreshData();

    }

	@api refreshData() {

		this.loaded = false;
	
        getClientSummary({
			checkInID: this._checkInID
		}).then(info => {

			console.log("[clientInfo] Refresh Data: ", JSON.parse(JSON.stringify(info)));

            this.info  = info;

			this._nutritionPlanningMode = info.nutritionPlanningMode;

			this.nutritionPlanStartDate = info.nutritionPlanStartDate;
			this.nutritionPlanStartDay	= info.nutritionPlanStartDay;

            this.likes					= info.likes.length ? info.likes.join(', ') : '';
            this.dislikes				= info.dislikes.length ? info.dislikes.join(', ') : '';
            this.allergies				= info.allergies.length ? info.allergies.join(', ') : '';
            this.restrictions			= info.restrictions.length ? info.restrictions.join(', ') : '';

            getPlans({
                checkInID: this._checkInID, 
                type: 'Nutrition_Reference_Plan'
            }).then(plans => {

                let pList   = JSON.parse(plans);
                let retList = [];
                for(let i = 0; i < pList.length; i++) {
                    if(pList[i].weekdays) {
                        retList.push(pList[i]);
                    }
                }
                this.plans	= retList;
                this.loaded = true;

            }).catch(error => {
                console.error("ERROR: ", JSON.parse(JSON.stringify(error)));
            });

            console.log('done');
        }).catch(error => {
            console.error("ERROR: ", JSON.stringify(error));
        });
	}

	getAdvisoryString(advisoryList) {

		if(advisoryList.length == 0) {
			return 'N/A';
		}
		
		let allAllergies = '';

		let index = 0;

		for(let allergy of advisoryList) {

			if(allergy.selected === false) {
				continue;
			}

			if(index > 0) {
				allAllergies += ', ';
			}
			allAllergies += allergy.name;
			index += 1;
		}


		return allAllergies;
	}

	/**
	 * @description Are the nutrition fields being worked on?
	 */
	get isNutritionWIP() {
		return this.info.nutritionWIP;
	}

	/**
	 * @description Returns true, if there are plans attached to this Check-In, false otherwise
	 * @return True: Plan count greater than 0
	 */
	get hasPlans() {
		return (this.plans && this.plans.length > 0);
	}

	get hasOtherAllergies() {
		return this.info.otherAllergies.length > 0;
	}


	get otherAllergyString() {
		return this.getAdvisoryString(this.info.otherAllergies);
	}

	get standardLikesString() {
		return this.getAdvisoryString(this.info.standardLikes);
	}
	
	get otherLikesString() {
		return this.getAdvisoryString(this.info.otherLikes);
	}
	
	get standardDislikesString() {
		return this.getAdvisoryString(this.info.standardDislikes);
	}
	
	get otherDislikesString() {
		return this.getAdvisoryString(this.info.otherDislikes);
	}

    show(event) {
        this.collapsed = false;
    }

    hide(event) {
        this.collapsed = true;
    }
}