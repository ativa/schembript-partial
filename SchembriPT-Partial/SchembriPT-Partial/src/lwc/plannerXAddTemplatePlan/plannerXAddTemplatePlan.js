import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent }				from 'lightning/platformShowToastEvent';

import getFilters						from '@salesforce/apex/Planner.getFilters';
import planNameExists					from '@salesforce/apex/Planner.planNameExists';

import GetTemplatePlans					from '@salesforce/apex/AT_LWCC_PlannerAddTemplatePlan.GetTemplatePlans';
import SaveAsReferencePlan				from '@salesforce/apex/AT_LWCC_PlannerAddTemplatePlan.SaveAsReferencePlan';
import SaveAsClientPlan					from '@salesforce/apex/AT_LWCC_PlannerAddTemplatePlan.SaveAsClientPlan';

export default class PlannerXAddTemplatePlan extends LightningElement {

	/* -------------------- VARIABLES -------------------- */
	
	/**
	 * @description The Check-In Id that will be linked to the new Reference Plan
	 */
	@track _checkInId;

	/**
	 * @description If true, allowed to change the plan name
	 */
	@api canChangePlanName = false;

	/** 
	 * @description Can Multiple Template Plans be selected
	 */
	@api multiple = false;

	/**
	 * @description The template ids to ignore
	 */
	@api ignoreTemplateIds = [];

	/**
	 * @description Tracks if the Client Info Pannel displayed is collapsed or not
	 */
	@track collapsedClientInfoPanel = false;

	/**
	 * @description All of the Template Plans in the Organisation
	 */
	@track allTemplatePlans = [];

	/**
	 * @description All Filters for the Plan
	 */
	@track filters;

	/**
	 * @description The name of the reference plan
	 */
	@track newPlanName = "";

	/**
	 * @description Tracks to see if this name already exists
	 */
	@track nameExists = false;

	/**
	 * @description Tracks the search parameter for the name
	 */
	@track nameSearchParameter;

	/**
	 * @description Tracks the search parameter for the calories
	 */
	@track calorieSearchParameter;

	/**
	 * @description Tracks the selected Template
	 * DEPRECATED
	 */
	@api selectedTemplate;

	/**
	 * @description Tracks a list of Selected templates (If 'multiple' is false, will only ever have one)
	 */
	@api selectedTemplates = [];

	/**
	 * @description The Id of the displayed Template Plan
	 */
	@track _displayedTemplateId;

	@track _displayedTemplate;

	/**
	 * @description Are we querying morning, night or all plans
	 */
	@track _trainingValue = 'all';
	
	/**
	 * @description All querying options for training
	 */
	@track _trainingOptions = [
		{ label: 'All', value: 'all' },
		{ label: 'AM',	value: 'am' },
		{ label: 'PM',	value: 'pm' }
	];

	/**
	 * @description The allergy filters to use (Lactose, Gluten etc.) if empty include everything
	 */
	@api filterAllergies = [];

	/**
	 * @description The dietary restriction filters to use (Vegan, Vegetarian etc.) if empty include everything
	 */
	@api filterDietaryRestrictions = [];


	/* -------------------- GETTERS -------------------- */
	
	@api get checkInId() {
		return this._checkInId;
	}

	
	/**
	 * @description Using the filters, only return the filters that satisfy those conditions
	 */
	get filteredTemplates() {
	
		const calories = (this.calorieSearchParameter && this.calorieSearchParameter != '') ? Number(this.calorieSearchParameter) : 0;

		let filteredTemplatePlans = [];

        this.allTemplatePlans.forEach(template => {

			if(this.ignoreTemplateIds.includes(template.id)) {
				return;
			}

            const caloriePercent	= ((template.calories ? template.calories : 0) / calories);
            const nameMatches		= (this.nameSearchParameter == null) || (this.nameSearchParameter == '') || (template.name.toLowerCase().indexOf(this.nameSearchParameter.toLowerCase()) >= 0);
            const allergiesMatch    = this.filterAllergies.length == 0 || (template.allergies.filter(e => this.filterAllergies.includes(e)).length == 0);
            const restrictionsMatch = this.filterDietaryRestrictions.length == 0 || (template.restrictions.filter(e => this.filterDietaryRestrictions.includes(e)).length > 0);
			
			template.states = {
				isSelected: this.selectedTemplates.includes(template.id)
			}

            if (nameMatches && allergiesMatch && restrictionsMatch && ((calories == 0) || ((caloriePercent <= 1.05) && (caloriePercent >= 0.95)))) {
				if((this._trainingValue === 'all') || (this._trainingValue === template.trainingTime)) {
					filteredTemplatePlans.push(template);
				}
            }

		});

		return filteredTemplatePlans;
	} 

	/**
	 * @description Tracks if we need to display the Required Name Error
	 */
	get displayNameRequiredError() {
		return typeof(newPlanName) != "undefined" || this.newPlanName === "";
	}

	/**
	 * @description Tracks if we should show the selected template
	 */
	get showSelectedTemplate() {
		return typeof(this._displayedTemplateId) !== "undefined";
	}

	/**
	 * @description Get the Allergies Filters
	 */
	get allergiesFilter() {
		if(this.filters) {
			return this.filters.allergies;
		} else {
			return [];
		}
	}

	get restrictionsFilter() {
		if(this.filters) {
			return this.filters.restrictions;
		} else {
			return [];
		}
	}

	/* -------------------- SETTERS -------------------- */
	
	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		this.Refresh();
	}
	
	/* -------------------- CUSTOM METHODS -------------------- */

	@api Refresh() {
	
		//Get all of the Template Plans
		GetTemplatePlans({
			checkInId: this._checkInId
		}).then(templates => {
            this.allTemplatePlans = JSON.parse(JSON.stringify(templates));
			
			//Then get all of the filters
			getFilters().then(filters => {
				
				let newFilters = {};
				if (filters.allergies) {
					newFilters.allergies	= filters.allergies.map(a => {return {name: a, checked: false}});
				}
				if (filters.restrictions) {
					newFilters.restrictions = filters.restrictions.map(r => {return {name: r, checked: false};});
				}
				this.filters = newFilters;


				console.log("[plannerXAddTemplatePlan] Filters:", newFilters);
			}).catch(error => {
				console.error(JSON.stringify(error));
			});

        }).catch(error => {
            console.error(JSON.stringify(error));
        });

	}

	@api SaveAsReferencePlan(checkInId, onSuccess, onError) {
		SaveAsReferencePlan({
			checkInId:		checkInId,
			newName:		this.newPlanName,
			templatePlanId: this.selectedTemplates[0]
		}).then(result => { 
			console.log("Saved New Plan!!!: ", JSON.parse(JSON.stringify(result)));
			
			if(onSuccess) {
				onSuccess({
					newPlanId:		result,
					newPlanName:	this.newPlanName
				});
			}
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
		});
	}

	/**
	 * @description Save the Template Plan as a Client Plan
	 *
	 * @param {String} accountId The Id of the Account to save the plan too
	 * @param {Function} onSuccess Executes after the client plan has successfully been saved
	 * @param {Function} onerror Executes after the client plan save has errored
	 *
	 * @return Returns if all the conditions to save the plan were successful
	 */
	@api SaveAsClientPlan(accountId, onSuccess, onError) {

		SaveAsClientPlan({
			accountId:		accountId,
			newName:		this.newPlanName,
			templatePlanId: this.selectedTemplates[0].id
		}).then(result => {
			console.log("Saved New Plan!!!: ", JSON.parse(JSON.stringify(result)));
			if(onSuccess) {
				onSuccess({
					newPlanId: result
				});
			}
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError();
			}
		});

	}

	processTemplate(template) {

		let updatedTemplate = JSON.parse(JSON.stringify(template));

		updatedTemplate.states = {
			caloriesOver10000 : false
		}

		if(updatedTemplate.calories > 10000) {
			updatedTemplate.states.caloriesOver10000 = true;
		}


		return updatedTemplate;

	}


	/* -------------------- CUSTOM EVENTS -------------------- */

	/**
	 * @description Triggers if the Plan name has been changed
	 */
	onChangePlanName(event) {
		//Update the new plan name
	    this.newPlanName = event.target.value;
		this.fireChangeEvent();
	} 

	onTrainingValueChange(event) {
		this._trainingValue = event.target.value;
	}

	/**
	 * @description Triggers if the Name Search Input field is changed
	 */
	onSearchByNameChangeInput(event) {
        this.nameSearchParameter = event.target.value;
	}

	onSearchByCaloriesChangeInput(event) {
		this.calorieSearchParameter = event.target.value;
	}

	/**
	 * @description Executes when a template was selected
	 * 
	 * @param event {Event} The event that executes this
	 */
	onSelectTemplateV2(event) {

		let templateId = event.target.dataset.templateId;

		if(this.multiple === true) {
			
			this._displayedTemplateId	= templateId;
			this._displayedTemplate		= this.processTemplate(this.filteredTemplates.filter(t => (t.id === templateId))[0]);

			if(this.selectedTemplates.includes(templateId) === true) {			
				for(var i = 0; i < this.selectedTemplates.length; i++){ 
					if (this.selectedTemplates[i] === templateId) {     
						this.selectedTemplates.splice(i, 1); 
					}
				}
			} else {
				this.selectedTemplates.push(templateId);
			}
		} else {
			if(this.selectedTemplates.includes(templateId) === true) {	
				this.selectedTemplates	= [];
				this._displayedTemplateId	= undefined;
				this._displayedTemplate		= undefined;
			} else {
				this.selectedTemplates	= [templateId];
				this._displayedTemplateId	= templateId;
				this._displayedTemplate		= this.processTemplate(this.filteredTemplates.filter(t => (t.id === templateId))[0]);
			}
		}

		console.log("Selected Templates: ",JSON.parse(JSON.stringify(this.selectedTemplates)));
		

	}

	onClickCancelButton(event) {
		let cancelEvent = new CustomEvent('cancel');
		this.dispatchEvent(cancelEvent);
	}

	onAllergiesSelect(event) {
        const allergy	= event.target.name;
        const selected	= event.target.checked;
		for(let i = 0; i < this.filters.allergies.length; i++) {
			if(this.filters.allergies[i].name === allergy) {
				this.filters.allergies[i].checked = selected;
			}
		}
	}
	
	onRestrictionsSelect(event) {
        const restriction	= event.target.name;
        const selected		= event.target.checked;
		for(let i = 0; i < this.filters.restrictions.length; i++) {
			if(this.filters.restrictions[i].name === restriction) {
				this.filters.restrictions[i].checked = selected;
			}
		}
	}

	/* -------------------- LWC EVENTS -------------------- */

}