import { LightningElement, api, track } from 'lwc';
import GetGuidedPlanData				from '@salesforce/apex/AT_LWCC_PlannerNutritionGuidedPlans.GetGuidedPlanData';
import SaveNutritionalTargets			from '@salesforce/apex/AT_LWCC_PlannerNutritionGuidedPlans.SaveNutritionalTargets';

export default class PlannerXNutritionGuidedPlans extends LightningElement {

	/* ---------- VARIABLES ---------- */

	/**
	 * @description The Id of the Check-In
	 */
	@track _checkInId;

	/**
	 * @description Allergy filters for the Template Pool
	 */
	@track _allergyFilters = [];
	
	/**
	 * @description Dietary filters for the Template Pool
	 */
	@track _dietaryFilters = [];
	
	/**
	 * @description The original target data
	 */
	@track _originalTargetsData = [];

	/**
	 * @description The updated target data
	 */
	@track _updatedTargetsData = [];
	
	/**
	 * @description Is the Plan data being loaded
	 */
	@track _isLoading = false;

	/**
	 * @description Is the Plan data being saved
	 */
	@track _isSaving = false;
	
	/**
	 * @description Assign to week options
	 */
	@track _assignToWeekOptions = [];

	/**
	 * @description A list of targets that will be deleted from the database
	 */
	@track _targetsToDelete = [];


	/* ---------- GETTERS ---------- */
	
	/**
	 * @description Tracks if we are loading/saving any information
	 */
	get _isSavingOrLoading() {
		return this._isLoading === true || this._isSaving === true;
	}

	/**
	 * @description Returns the Check-In Id
	 */
	@api get checkInId() {
		return this._checkInId;
	}
	
	/**
	 * @description A list of active allergy filters
	 */
	get _activeAllergies() {
		let actAllergies = [];
		for(let i = 0; i < this._allergyFilters.length; i++) {
			if(this._allergyFilters[i].checked === true) {
				actAllergies.push(this._allergyFilters[i].value);
			}
		}
		return actAllergies;
	}
	
	/**
	 * @description A list of active dietary restriction filters
	 */
	get _activeRestrictions() {
		let actRestrictions = [];
		for(let i = 0; i < this._dietaryFilters.length; i++) {
			if(this._dietaryFilters[i].checked === true) {
				actRestrictions.push(this._dietaryFilters[i].value);
			}
		}
		return actRestrictions;
	}
	
	
	/* ---------- SETTERS ---------- */

	/**
	 * @description Set the Check-In Id
	 *
	 * @param {String} newCheckInId The new Check-In Id
	 */
	set checkInId(newCheckInId) {
		this._checkInId = newCheckInId;
		this.refresh();
	}
	
	/* ---------- CUSTOM METHODS ---------- */

	@api refresh(onSuccess, onError) {
		this._isLoading = true;
		GetGuidedPlanData({
			checkInId: this._checkInId
		}).then(result => {
			console.log("Guided Plan Data: ", JSON.parse(JSON.stringify(result)));
			this._originalTargetsData	= JSON.parse(JSON.stringify(result.nutritionTargets));
			this._assignToWeekOptions	= result.assignToWeekOptions;
			this._updatedTargetsData	= this.processTargets(this._originalTargetsData);
			this._allergyFilters		= this.processPicklistOptions(result.allergies);
			this._dietaryFilters		= this.processPicklistOptions(result.dietaryRestrictions);
			console.log("Guided Targets Data: ", JSON.parse(JSON.stringify(this._updatedTargetsData)));
			if(onSuccess) {
				onSuccess(result);
			}

			let plannerValidation = this.template.querySelector('.planner-validation');
			plannerValidation.Refresh();

			this._isLoading = false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
			this._isLoading = false;
		});
	}

	
	_saveToDatabase() {
		//Save the targets, and refresh the template pool
		this.save(() => {
			console.log("Successfully saved Targets");
			let templatePool = this.template.querySelector('.templatePool');
			templatePool.Refresh(() => {
				console.log("Successfully saved everything");
				this.refresh();
			}, (error) => {
				console.error("Error: ", JSON.parse(JSON.stringify(error)));
			});
		}, (error) => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
		});
	}

	@api save(onSuccess, onError) {
		this._isSaving = true;
		SaveNutritionalTargets({
			nutritionalTargets: JSON.parse(JSON.stringify(this._updatedTargetsData)),
			targetsToDelete:	this._targetsToDelete
		}).then(result => {
			this._targetsToDelete = [];
			console.log("Successfully Saved Targets");
			if(onSuccess) {
				onSuccess();
			}
			this._isSaving = false;
		}).catch(error => {
			console.error("Error: ", JSON.parse(JSON.stringify(error)));
			if(onError) {
				onError(error);
			}
			this._isSaving = false;
		});

	}

	/**
	 * @description Add additional states and values
	 */
	processTargets(originalTargets) {
		let oTargets = JSON.parse(JSON.stringify(originalTargets));
		for(let i = 0; i < oTargets.length; i++) {
			oTargets[i].states = {
				isNewTarget: false
			};
			oTargets[i].values = {
				order:					i,
				assignToWeeks:			[]
			};
			//Loop through the assignToWeekOptions
			for(let j = 0; j < this._assignToWeekOptions.length; j++) {
				oTargets[i].values.assignToWeeks.push({
					key:		i + '-' + this._assignToWeekOptions[j],
					label:		this._assignToWeekOptions[j],
					value:		this._assignToWeekOptions[j],
					checked:	(oTargets[i].assignedWeeks && oTargets[i].assignedWeeks.includes(this._assignToWeekOptions[j])) ? true : false,
				});
			}

		}
		return oTargets;
	}

	processPicklistOptions(picklistOptions) {
		let processedOptions = [];
		for(let i = 0; i < picklistOptions.length; i++) {
			processedOptions.push({
				label:		picklistOptions[i].label,
				value:		picklistOptions[i].value,
				checked:	false
			});
		}
		return processedOptions;
	}
	
	/**
	 * @description Update the calorie value for the target
	 *
	 * @param {int} targetOrder The order of the target in the list
	 */
	updateCalorieTargetValue(targetOrder) {
		let protein = this._updatedTargetsData[targetOrder].protein;
		let carbs	= this._updatedTargetsData[targetOrder].carbs;
		let fat		= this._updatedTargetsData[targetOrder].fat;
		this._updatedTargetsData[targetOrder].calories = (4 * protein) + (4 * carbs) + (9 * fat);
	}
	
	/**
	 * @description
	 *
	 * @param {int} order The order of the target in the list
	 * @param {String} week The week selected
	 */
	updateWeekSelection(order, week) {

		//Find the week index
		let weekIndex = -1;
		for(let i = 0; i < this._updatedTargetsData[0].values.assignToWeeks.length; i++) {
			if(this._updatedTargetsData[0].values.assignToWeeks[i].value === week) {
				weekIndex = i;
				break;
			}
		}

		//Update the week checked
		for(let i = 0; i < this._updatedTargetsData.length; i++) {
			if(i === order) {
				this._updatedTargetsData[i].values.assignToWeeks[weekIndex].checked = true;
			} else {
				this._updatedTargetsData[i].values.assignToWeeks[weekIndex].checked = false;
			}
		}

		//Update the assigned weeks for each target
		for(let i = 0; i < this._updatedTargetsData.length; i++) {
			let assignedWeeks = [];
			for(let j = 0; j < this._updatedTargetsData[i].values.assignToWeeks.length; j++) {
				if(this._updatedTargetsData[i].values.assignToWeeks[j].checked === true) {
					assignedWeeks.push(this._updatedTargetsData[i].values.assignToWeeks[j].value);
				}
			}
			this._updatedTargetsData[i].assignedWeeks = assignedWeeks;
		}


	}
	
	/**
	 * @description Add a new target
	 */
	addNewTarget() {
		let newTarget = {
			name:		"Target " + (this._updatedTargetsData.length + 1),
			checkInId:	this._checkInId,
			calories:	0,
			carbs:		0,
			fat:		0,
			protein:	0,
			values: {
				order:					this._updatedTargetsData.length,
				assignToWeeks:			[]
			},
			states: {
				isNewTarget: true
			}
		};

		//Loop through the assignToWeekOptions
		for(let j = 0; j < this._assignToWeekOptions.length; j++) {
			newTarget.values.assignToWeeks.push({
				key:		this._updatedTargetsData.length + '-' + this._assignToWeekOptions[j],
				label:		this._assignToWeekOptions[j],
				value:		this._assignToWeekOptions[j],
				checked:	false,
			});
		}


		this._updatedTargetsData.push(newTarget);
	}
	
	/**
	 * @description Reorder the targets within the list
	 */
	reorderTargets() {
		for(let i = 0; i < this._updatedTargetsData.length; i++) {
			this._updatedTargetsData[i].values.order = i;
		}
	}


	/* ---------- CUSTOM EVENTS ---------- */

	onTemplatePoolChange(event) {
		this.refresh();
	}

	/**
	 * @description Executes when an allergy filter is selected
	 */
	onAllergySelect(event) {
        const allergy	= event.target.name;
        const selected	= event.target.checked;
		for(let i = 0; i < this._allergyFilters.length; i++) {
			if(this._allergyFilters[i].value === allergy) {
				this._allergyFilters[i].checked = selected;
			}
		}
	}
	
	/**
	 * @description Executes when an dietary filter is selected
	 */
	onRestrictionSelect(event) {
        const restriction	= event.target.name;
        const selected		= event.target.checked;
		for(let i = 0; i < this._dietaryFilters.length; i++) {
			if(this._dietaryFilters[i].value === restriction) {
				this._dietaryFilters[i].checked = selected;
			}
		}
	}

	/**
	 * @description Executes when the target name has been updated
	 *
	 * @param {Event} event The event that triggers this method
	 */
	onTargetNameUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		let newName								= event.target.value;
		this._updatedTargetsData[order].name	= newName;
		this._saveToDatabase();
	}
	
	/**
	 * @description Executes when the target protein has been updated
	 *
	 * @param {Event} event The event that triggers this method
	 */
	onTargetProteinUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		let newProtein							= parseFloat(event.target.value);
		this._updatedTargetsData[order].protein = newProtein;		
		this.updateCalorieTargetValue(order);
		this._saveToDatabase();
	}
	
	/**
	 * @description Executes when the target carbs has been updated
	 *
	 * @param {Event} event The event that triggers this method
	 */
	onTargetCarbsUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		let newCarbs							= parseFloat(event.target.value);
		this._updatedTargetsData[order].carbs	= newCarbs;
		this.updateCalorieTargetValue(order);
		this._saveToDatabase();
	}
	
	/**
	 * @description Executes when the target fat has been updated
	 *
	 * @param {Event} event The event that triggers this method
	 */
	onTargetFatUpdate(event) {
		let order								= parseInt(event.target.dataset.order);
		let newFat								= parseFloat(event.target.value);
		this._updatedTargetsData[order].fat		= newFat;
		this.updateCalorieTargetValue(order);
		this._saveToDatabase();
	}

	/**
	 * @description Executes when a week is selected for a target
	 *
	 * @param {Event} event The event that triggers this method
	 */
	onWeekSelection(event) {
		let order	= parseInt(event.target.dataset.order);
		let week	= event.target.dataset.week;
		this.updateWeekSelection(order, week);
		this._saveToDatabase();
	}

	/**
	 * @description Executes when the targets are saved
	 */
	onSaveTargets(event) {
		this.save(() => {
			console.log("Successfully saved Targets");
			let templatePool = this.template.querySelector('.templatePool');
			templatePool.save(() => {
				console.log("Successfully saved everything");
				this.refresh();
			}, (error) => {
				console.error("Error: ", JSON.parse(JSON.stringify(error)));
			});

		});
	}

	/**
	 * @description Executes when a new target is added
	 * 
	 * @param {Event} event The event that triggers this method
	 */
	onAddNewTarget(event) {
		this.addNewTarget();
		this._saveToDatabase();
	}

	/**
	 * @description Executes when a target is deleted
	 *
	 * @param {Event} event The event that triggers this method
	 */
	onDeleteTarget(event) {
		let order = parseInt(event.target.dataset.order);
		if(this._updatedTargetsData[order].id) {
			this._targetsToDelete.push(this._updatedTargetsData[order].id);
		}
		this._updatedTargetsData.splice(order,1);
		this.reorderTargets();

	}

	/* ---------- LWC EVENTS ---------- */

}