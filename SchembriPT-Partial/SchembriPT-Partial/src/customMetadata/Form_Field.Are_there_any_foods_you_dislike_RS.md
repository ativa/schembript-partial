<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Are there any foods you dislike?</label>
    <protected>false</protected>
    <values>
        <field>ApiName__c</field>
        <value xsi:type="xsd:string">Are_there_any_foods_you_dislike__pc</value>
    </values>
    <values>
        <field>Column_Width__c</field>
        <value xsi:type="xsd:double">12.0</value>
    </values>
    <values>
        <field>Controlling_Comparison__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Controlling_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Controlling_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Controlling_Value__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Display_Label__c</field>
        <value xsi:type="xsd:string">Are there any foods you dislike?</value>
    </values>
    <values>
        <field>Editable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Form_Section__c</field>
        <value xsi:type="xsd:string">Nutrition_RS</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">17.0</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
