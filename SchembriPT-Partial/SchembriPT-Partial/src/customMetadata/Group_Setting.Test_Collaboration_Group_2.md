<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test Collaboration Group 2</label>
    <protected>false</protected>
    <values>
        <field>Community_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Notification_Frequency__c</field>
        <value xsi:type="xsd:string">D</value>
    </values>
    <values>
        <field>User_IDs__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
