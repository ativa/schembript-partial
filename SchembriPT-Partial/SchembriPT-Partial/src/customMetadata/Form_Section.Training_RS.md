<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Training RS</label>
    <protected>false</protected>
    <values>
        <field>Display_Label__c</field>
        <value xsi:type="xsd:string">Training</value>
    </values>
    <values>
        <field>Form__c</field>
        <value xsi:type="xsd:string">Restart_Form</value>
    </values>
    <values>
        <field>Object_Api_Name__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
</CustomMetadata>
