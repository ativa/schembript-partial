({
	init: function(component, event, helper) {
        helper.init(component, helper);
	},
	onDisplayMultiMediaFiles: function(component, event, helper) {
		component.set("v.displayMultiMediaItems", true);
	},
	onHideMultiMediaFiles: function(component, event, helper) {
		component.set("v.displayMultiMediaItems", false);
	},
    pageReferenceChanged: function(component, event, helper) {
        helper.init(component, helper);
    },
    previous: function(component, event, helper) {
        const stage = component.find("accordion").get('v.activeSectionName');
        
        let newStage;
        //Measurements,Photos,Advisories,Survey,Feedback
        if (stage == 'Photos') newStage = 'Measurements';
        if (stage == 'Advisories') newStage = 'Photos';
        if (stage == 'Survey') newStage = 'Advisories';
        if (stage == 'Feedback') newStage = 'Survey';
        
        component.find("accordion").set('v.activeSectionName', newStage);
    },
 	next: function(component, event, helper) {
        const stage = component.find("accordion").get('v.activeSectionName');
        
		//Based on the current stage, set the next stage
        let newStage;
        if (stage === "Measurements")	{ newStage = "Photos"; } 
		if (stage === "Photos")			{ newStage = "Advisories"; } 
		if (stage === "Advisories")		{ newStage = "Survey"; } 
		if (stage === "Survey")			{ newStage = "Feedback"; }
        
        component.find("accordion").set('v.activeSectionName', newStage);
    },
	onCheckInSectionChange: function(component, event, helper) {

		let previousSection = component.get("v.currentSection");
		let currentSection	= component.find("accordion").get('v.activeSectionName');
		
		//If the sections have been changed
		if(previousSection !== currentSection) {
			//Based on the previous section, preform some actions
			if(previousSection === "Measurements") {

				console.log("Measurements will be Submitted");

				component.find('measurements').Submit((returnValue) => {
					component.set("v.measurementsSubmitted",	returnValue.valuesSet);
				});
			} else if(previousSection === "Photos") {
				//Don't do anything
			} else if(previousSection === "Advisories") {
				//Don't do anything
			} else if(previousSection === "Survey") {
				component.find('survey').Submit(() => {
					let surveySet = component.find('survey').allValuesSet();
					component.set("v.surveySubmitted", surveySet.valuesSet);
				});
			} else if(previousSection === "Feedback") {
				component.find('feedback').Submit(() => {
					let	feedbackGeneralValue	= component.find('feedback').allValuesSet();
					component.set("v.feedbackSubmitted", feedbackGeneralValue.valuesSet);
				});
			}
			component.set("v.currentSection", currentSection);
		}


	},
	onAcknowledgeError: function(component, event, helper) {
		component.set("v.submissionErrorDisplayed", false);
	},
    measurementsSuccess: function(component, event, helper) {
        if (component.get('v.returnHome')) {
            helper.returnHome(component);
        } else {
            const result = event.getParams().response;
        }
    },
    measurementsError: function(component, event, helper) {
        const result = event.getParams().error;
        console.log('Error: ', result.error, result.message, result.detail);
    },
    surveySuccess: function(component, event, helper) {

		console.log("Survey Successfully Saved");

        if (component.get('v.returnHome')) {
            helper.returnHome(component);
        } else {
            //const result = event.getParams().response;
        }
    },
    surveyError: function(component, event, helper) {
		var error = event.getParams();
		//console.error("Survey Error: ", JSON.parse(JSON.stringify(error)));
    },
    feedbackSuccess: function(component, event, helper) {
		if(component.set("v.returnHome") === true) {
			helper.returnHome(component);
		}
    },
    feedbackError: function(component, event, helper) {
		var error = event.getParams();
		console.error("Feedback Error: ", JSON.parse(JSON.stringify(error)));
    },
    submit: function(component, event, helper) {
        
	},
	onPhotoUpload: function(component, event, helper) {
		console.log("Community Check-In photo uploaded");
		component.set("v.photosSubmitted", event.getParam("allPhotosUploaded"));
	},
    returnHome: function(component, event, helper) {
        const stage = component.find("accordion").get('v.activeSectionName');
        
        component.set('v.returnHome', true); //Set to return home
        
		let homeUrl = component.get('v.startURL') + "/s/";

        if (stage == 'Measurements') {
            component.find('measurements').Submit(() => {
				var urlEvent = $A.get("e.force:navigateToURL");
				urlEvent.setParams({
				  "url": "/"
				});
				urlEvent.fire();
			});
        } if (stage == 'Photos') {
            helper.returnHome(component);
        } if (stage == 'Advisories') {
            helper.returnHome(component);
        } if (stage == 'Survey') {
            component.find('survey').Submit(() => {
				var urlEvent = $A.get("e.force:navigateToURL");
				urlEvent.setParams({
				  "url": "/"
				});
				urlEvent.fire();
			});
        } if (stage == 'Feedback') {
			component.find('feedback').Submit(() => {
				var urlEvent = $A.get("e.force:navigateToURL");
				urlEvent.setParams({
				  "url": "/"
				});
				urlEvent.fire();
			});
        }
    },
    newAdvisory: function(component, event, helper) {
        component.set('v.editAdvisory', true);
        component.set('v.advisoryID', null);
    },
    editAdvisory: function(component, event, helper) {
        component.set('v.editAdvisory', true);
        
		let advisoryId = null;
        if (event.target.name) {
            advisoryId = event.target.name;
        } else {
			advisoryId = event.getSource().get('v.name');
        }

		//Set the Id and the Record Type of the Advisory
		component.set('v.advisoryID', advisoryId);
		let advisories = component.get("v.advisories");
		for(let i = 0; i < advisories.length; i++) {
			let ad = advisories[i];
			if(ad.id === advisoryId) {
				component.set("v.newAdvisoryType", ad.recTypeId);
			}
		}

    },
    cancelAdvisory: function(component, event, helper) {
        component.set('v.editAdvisory', false);
    },
    saveAdvisory: function(component, event, helper) {

		console.log("Saving Advisory");

		let advisoryId = component.get('v.advisoryID');

		if(advisoryId) {
			console.log("Updating Advisory");
			component.find('updateAdvisory').submit();

		} else {
			console.log("Creating New Advisory");
			component.find('newAdvisory').submit();
			console.log('Here');
		}

    },
    advisorySuccess: function(component, event, helper) {
        const result = event.getParams().response;
        helper.getAdvisories(component, helper);
    },
    advisoryError: function(component, event, helper) {
        const result = event.getParams().error;
		console.error(JSON.parse(JSON.stringify(result)));
        console.log('Error: ', result.error, result.message, result.detail);
    },
    sliderChanged: function(component, event, helper) {
        //console.log('Name: ', JSON.stringify(event.getSource().getElement()));
        console.log('Value: ', JSON.stringify(event.getSource().get('v.value')));
    },
	onSubmitCheckIn: function(component, event, helper) {
	
		let mode = component.get("v.mode");

		component.set("v.submittingCheckIn", true);

		//Before submitting make sure that the data is saved
		let currentSection	= component.find("accordion").get('v.activeSectionName');
		if(currentSection === "Measurements") {
			if(mode === 'checkIn') {
				component.find('measurements').Submit(() => {
					helper.submitCheckIn(component, event, helper);
				});
			} else if(mode === 'measurements') {
				component.find('measurements').Submit(() => {
					helper.submitMeasurements(component);
				});
			} else if(mode === 'photosAndMeasurements') {
				component.find('measurements').Submit(() => {
					helper.submitPhotosAndMeasurements(component);
				});
			}
		} else if(currentSection === "Photos") {
			if(mode === 'checkIn') {
				helper.submitCheckIn(component, event, helper);
			} else if(mode === "photos") {
				helper.submitPhotos(component);
			} else if(mode === 'photosAndMeasurements') {
				helper.submitPhotosAndMeasurements(component);
			} else if(mode === 'filesUpload') {
				//Return home
				component.set("v.returnHome", true);
				let homeURL = component.get('v.startURL') + "/s/";
				var urlEvent = $A.get("e.force:navigateToURL");
				urlEvent.setParams({
					"url": "/"
				});
				urlEvent.fire();
			}

			//Don't do anything
		} else if(currentSection === "Advisories") {
			//Don't do anything
		} else if(currentSection === "Survey") {
			component.find('survey').Submit(() => {
				helper.submitCheckIn(component, event, helper);
			});
		} else if(currentSection === "Feedback") {
			component.find('feedback').Submit(() => {
				console.log("Submitting Feedback");
				helper.submitCheckIn(component, event, helper);
			});
		}

	},
	onCheckboxSelect: function(component, event, helper) {
		let isChecked = event.getSource().get('v.checked');
		let fieldName = event.getSource().get('v.name');
		
		//Get the Survey fields
		let surveyFields = component.get("v.surveyFields");

		for(let i = 0; i < surveyFields.length; i++) {
			if(surveyFields[i].name === fieldName) {
				if(isChecked === true) {
					surveyFields[i].value = 'n/a';
				} else {
					surveyFields[i].value = '1';
				}
			}
		}

		component.set("v.surveyFields", surveyFields);

	},
	onEditFormFieldChange: function(component, event, helper) {

		//Get the form field
        let fieldName = event.getSource().get("v.fieldName"); 
        let newValue =  event.getSource().get("v.value");
		let formFields	= JSON.parse(JSON.stringify(component.get("v.advisoryFormValues")));
		formFields = (formFields) ? formFields : {};
		formFields[fieldName] = newValue;
		component.set("v.advisoryFormValues", formFields);

		console.log("Form Values: ", JSON.parse(JSON.stringify(component.get("v.advisoryFormValues"))));

		//Do a validation check on the Description field, we need to display an error message if it's not set and the sub-type is 'Other'
		component.set("v.displayDescriptionError", helper.displayDescriptionError(component));

	}
})