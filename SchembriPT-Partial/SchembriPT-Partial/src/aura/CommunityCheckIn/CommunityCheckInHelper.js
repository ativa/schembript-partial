({
	init: function(component, helper) {

        const currentDate	= new Date()
		const formattedDate = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate();
        
        component.set('v.today', formattedDate);
        
        helper.getInfo(component, helper);
	},
	displayDescriptionError: function(component) {
	
		let formFields	= JSON.parse(JSON.stringify(component.get("v.advisoryFormValues")));

		if(formFields.Sub_Type__c && formFields.Sub_Type__c === "Other") {
			if(formFields.Description__c && formFields.Description__c.length > 0) {
				return false;
			} else { 
				//If the subtype is set to other, and the description is not set
				return true;
			}
		} else {
			return false;
		}


	},
    getInfo: function(component, helper) {

		component.set("v.isLoading", true);

        const getCheckInInfo = component.get('c.getCheckInInfo');
        
        getCheckInInfo.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                const info = response.getReturnValue();

				console.log('Check-In Info: ', JSON.parse(JSON.stringify(info)));

				component.set("v.isSelfServiceCheckIn", info.isSelfServiceCheckIn);
                
				component.set("v.clientID",				info.clientId);
				component.set("v.clientGender",			info.clientGender);
                component.set('v.checkInID',			info.checkInID);
				component.set("v.programId",			info.programID);
                component.set('v.checkInName',			info.checkInName);
				
				let checkInDate = '';
				if(info.checkInDate) {
					checkInDate = helper.formatDate(info.checkInDate);
					component.set('v.checkInDate',			checkInDate);
				}
				
                component.set('v.checkInNumber',		info.checkInNumber);
                component.set('v.checkInOnboarding',	info.checkInOnboarding);
                component.set('v.surveyID',				info.surveyID);
                component.set('v.surveyFields',			info.surveyFields);
				component.set('v.startURL',				info.startURL);

				helper.getAdvisories(component, helper);
				
                helper.getAdvisoryTypes(component, helper);

				let currentMode = component.get("v.mode");
				if(currentMode === 'checkIn') {
					component.set("v.checkInTitle", 'Check-In #' + info.checkInNumber + ' (' + checkInDate + ')');
				} else if(currentMode === 'photos') {
					component.set("v.checkInTitle", 'Upload New Photos');
				} else if(currentMode === 'measurements') {
					component.set("v.checkInTitle", 'Update Measurements');
				} else if(currentMode === 'photosAndMeasurements') {
					component.set("v.checkInTitle", 'Update Photos & Measurements');
				} else if(currentMode === 'filesUpload') {
					component.set("v.checkInTitle", 'Files Upload');
				}
				
				//Before we submit to the backend, we need to check to see if the minimal requirements are met
				setTimeout(function(){ 

					let mode = component.get("v.mode");

					if(mode === 'checkIn') {

						let measurementsSet			= component.find('measurements').allValuesSet();
						let	photosSet				= component.find('photos').allValuesSet();
						let surveySet				= component.find('survey').allValuesSet();
						let	feedbackGeneralValue	= component.find('feedback').allValuesSet();

						component.set("v.measurementsSubmitted",	measurementsSet.valuesSet);
						component.set("v.photosSubmitted",			photosSet.valuesSet);
						component.set("v.surveySubmitted",			surveySet.valuesSet);
						component.set("v.feedbackSubmitted",		feedbackGeneralValue.valuesSet);

					}

					component.set("v.isLoading", false);


				}, 4000);

            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getCheckInInfo);
    },
    getAdvisories: function(component, helper) {
        const getAdvisories = component.get('c.getAdvisories');
        
        getAdvisories.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                let advisories = response.getReturnValue();
                
				console.log('Advisories Returned: ', JSON.parse(JSON.stringify(advisories)));
                

                advisories.forEach(ad => {

                    if (ad.startDate) ad.startDate = helper.formatDate(ad.startDate);
					if (ad.eventDate) ad.eventDate = helper.formatDate(ad.eventDate);
                    if (ad.endDate) ad.endDate = helper.formatDate(ad.endDate);
                    if (ad.content) ad.content = ad.content.replace(';', ', ');
                });
                
                component.set('v.advisories', advisories);
                console.log('Advisories: ', JSON.parse(JSON.stringify(advisories)));


                component.set('v.editAdvisory', false);
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getAdvisories);
    },
    getAdvisoryTypes: function(component, helper) {
        const getAdvisoryTypes = component.get('c.getAdvisoryTypes');
        
        getAdvisoryTypes.setCallback(this, function(response) {
            const state = response.getState();

			console.log("Response Returned for Advisories");

            if (state === 'SUCCESS') {
				
                let types = response.getReturnValue();
                let advisoryTypes = types.map(t => {
                    return {
                    	value:		t.Id,
                    	label:		t.Name
                	}
                });
                
                component.set('v.advisoryTypes',	advisoryTypes);
				component.set('v.newAdvisoryType',	advisoryTypes[0].value);
				
				console.log('Advisory Types: ', JSON.parse(JSON.stringify(component.get("v.advisoryTypes"))));
				
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getAdvisoryTypes);
    },
	displaySuccessfulFileUpload: function() {
		
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title":	"Upload Successful",
			"message":	"The Image has successfully been uploaded",
			"type":		"success"
		});
		toastEvent.fire();

	},
    recordPhoto: function(name, documentID, component, helper) {
        const clientID = component.get('v.clientID');
        const checkInID = component.get('v.checkInID');
        
        const recordPhoto = component.get('c.recordPhoto');

        recordPhoto.setParams({
            clientID:	clientID,
            checkInID:	checkInID,
            name:		name,
            documentID: documentID
        });
        recordPhoto.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
			
                const photoID = response.getReturnValue();
				
				console.log("Successfully Uploaded: ", photoID);
        
                component.set('v.' + name + 'PhotoURL', photoID);

				this.displaySuccessfulFileUpload();
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(recordPhoto);
    },
    deletePhoto: function(photoName, fileID, component, helper) {
        const deletePhoto = component.get('c.getCheckInInfo');
      
        deletePhoto.setParams({fileID: fileID});
        deletePhoto.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                component.set('v.' + photoName + 'PhotoID', null);
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(deletePhoto);
    },
    formatDate: function(date) {

		const segs = date.split("-");

		const year		= parseInt(segs[0]);
		const month		= parseInt(segs[1]);
		const day		= parseInt(segs[2]);

		let strDay		= (day < 10) ? "0" + day : "" + day;
		let strMonth	= (month < 10) ? "0" + month : "" + month;
		let strYear		= "" + year;

        return strDay + '/' + strMonth + '/' + strYear;
    },
    returnHome: function(component) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/"
        });
        urlEvent.fire();
    },
	submitPhotosAndMeasurements: function(component) {
	
		const submitPhotoAction = component.get('c.SubmitPhotosAndMeasurements');
		submitPhotoAction.setParams({
			checkInId:		component.get("v.checkInID")
		});
		submitPhotoAction.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
			
				//Fire Success Event
				var successEvent = $A.get("e.force:showToast");
				successEvent.setParams({
					"title":	"Successful",
					"message":	"Successfully Updated Photos and Measurements",
					"type":		"success"
				});
				successEvent.fire();
			
				let homeURL = component.get('v.startURL') + "/s/";
				setTimeout(() => {
					//Return home
					component.set("v.returnHome", true);
					let homeUrl = homeURL;
					var urlEvent = $A.get("e.force:navigateToURL");
					urlEvent.setParams({
						"url": "/"
					});
					urlEvent.fire();
				}, 1000);
            } else if (state === "ERROR") {
				console.error("ERROR: ", JSON.parse(JSON.stringify(response.getError())));
            }


		});
        $A.enqueueAction(submitPhotoAction);

	},
	submitMeasurements: function(component) {
	
		const submitPhotoAction = component.get('c.SubmitMeasurements');
		submitPhotoAction.setParams({
			checkInId:		component.get("v.checkInID")
		});
		submitPhotoAction.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
			
				//Fire Success Event
				var successEvent = $A.get("e.force:showToast");
				successEvent.setParams({
					"title":	"Successful",
					"message":	"Successfully Updated Measurements",
					"type":		"success"
				});
				successEvent.fire();
			
				let homeURL = component.get('v.startURL') + "/s/";
				setTimeout(() => {
					//Return home
					component.set("v.returnHome", true);
					let homeUrl = homeURL;
					var urlEvent = $A.get("e.force:navigateToURL");
					urlEvent.setParams({
						"url": "/"
					});
					urlEvent.fire();
				}, 1000);
            } else if (state === "ERROR") {
				console.error("ERROR: ", JSON.parse(JSON.stringify(response.getError())));
            }


		});
        $A.enqueueAction(submitPhotoAction);

	},
	submitPhotos: function(component) {
	
		const submitPhotoAction = component.get('c.SubmitPhotos');
		submitPhotoAction.setParams({
			checkInId:		component.get("v.checkInID")
		});
		submitPhotoAction.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
			
				//Fire Success Event
				var successEvent = $A.get("e.force:showToast");
				successEvent.setParams({
					"title":	"Successful",
					"message":	"Successfully Updated Photos",
					"type":		"success"
				});
				successEvent.fire();
			
				let homeURL = component.get('v.startURL') + "/s/";
				setTimeout(() => {
					//Return home
					component.set("v.returnHome", true);
					let homeUrl = homeURL;
					var urlEvent = $A.get("e.force:navigateToURL");
					urlEvent.setParams({
						"url": "/"
					});
					urlEvent.fire();
				}, 1000);
            } else if (state === "ERROR") {
				console.error("ERROR: ", JSON.parse(JSON.stringify(response.getError())));
            }


		});
        $A.enqueueAction(submitPhotoAction);

	},
	submitCheckIn: function(component, event, helper) {

		//Submit the Check-In
		component.set("v.submissionErrorDisplayed", false);

		//Before we submit to the backend, we need to check to see if the minimal requirements are met
		let measurementsSet			= component.find('measurements').allValuesSet();
		let surveySet				= component.find('survey').allValuesSet();
		let	feedbackGeneralValue	= component.find('feedback').allValuesSet();
		let photosSet				= component.find('photos').allValuesSet();

		let isSelfServiceCheckIn = component.get("v.isSelfServiceCheckIn");

		component.set("v.measurementsSet",			measurementsSet.valuesSet);
		component.set("v.measurementsFailedFields", measurementsSet.notSetFields);

		component.set("v.feedbackGeneralSet",		feedbackGeneralValue.valuesSet);
		component.set("v.feedbackFailedFields",		feedbackGeneralValue.notSetFields);

		component.set("v.surveySet",				surveySet.valuesSet);
		component.set("v.surveyFailedFields",		surveySet.notSetFields);

		//Check to see if the minimum set
		if(
			(isSelfServiceCheckIn == false && measurementsSet.valuesSet === true && surveySet.valuesSet === true && feedbackGeneralValue.valuesSet === true)  ||
			(isSelfServiceCheckIn == true && measurementsSet.valuesSet === true && photosSet.valuesSet === true && surveySet.valuesSet === true && feedbackGeneralValue.valuesSet === true)
		) {

			let subType = '';

			//If the photos are set, then do a full submission
			if(photosSet.valuesSet === true) {
				subType = 'Full';
			//Else do a partial submission
			} else {
				subType = 'Partial';
			}

			const submitCheckInAction = component.get('c.SubmitCheckIn');

			submitCheckInAction.setParams({
				checkInId:		component.get("v.checkInID"),
				submissionType: subType
			});
			submitCheckInAction.setCallback(this, function(response) {

				const state = response.getState();

				if(state === 'SUCCESS') {
					component.set("v.submittingCheckIn", false);
					console.log("Successfully Submitted Check-In");
					const stage = component.find("accordion").get('v.activeSectionName');

					//Fire Success Event
					var successEvent = $A.get("e.force:showToast");
					successEvent.setParams({
						"title":	"Successful",
						"message":	"Thank you for your submission",
						"type":		"success"
					});
					successEvent.fire();

					let homeURL = component.get('v.startURL') + "/s/";

					setTimeout(() => {
						//Return home
						component.set("v.returnHome", true);
						let homeUrl = homeURL;
						var urlEvent = $A.get("e.force:navigateToURL");
						urlEvent.setParams({
							"url": "/"
						});
						urlEvent.fire();
					}, 1000);


				} else if(state === 'ERROR') {
					console.error("ERROR: ",JSON.parse(JSON.stringify(response.getError())));
					component.set("v.submittingCheckIn", false);
				}

			});

        $A.enqueueAction(submitCheckInAction);



		} else {
			component.set("v.submissionErrorDisplayed", true);
			component.set("v.submittingCheckIn", false);
		}
		
	}
})