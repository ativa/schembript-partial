({
	onInit: function (component, event, helper) {

		var initAction = component.get("c.GetProgramDetails");
		initAction.setParams({
			checkInId: component.get("v.recordId")
		});
		initAction.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				console.log('SUCCESS',JSON.parse(JSON.stringify(response.getReturnValue())));
				component.set("v.programId", response.getReturnValue().programId);
				component.set("v.isLoading", false);
			} else {
				console.error("ERROR", JSON.parse(JSON.stringify(response.getError())));
				component.set("v.isLoading", false);
			}
		});
		$A.enqueueAction(initAction);
	}
})