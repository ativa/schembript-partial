({
	init: function(component, event, helper) {
        helper.init(component, helper);
	},
    pageReferenceChanged: function(component, event, helper) {
        helper.init(component, helper);
    },
    scriptsLoaded: function(component, event, helper) {
        console.log('Scripts loaded');
        helper.init(component, helper);
	},
    togglePhotos: function(component, event, helper) {
        component.set('v.showPhotos', !component.get('v.showPhotos'));
	},
	onCheckInIdChange: function(component, event, helper) {
		console.log('CheckIn Id is updated too ' + component.get('v.checkInID'));
		helper.loadPhotos(component, helper);
	},
	onPhotoRangeChange: function(component, event, helper) {
		console.log('Photo Range is... ' + component.get('v.photoRange'));
		helper.loadPhotos(component, helper);
	},
	handlePhotoDisplayerClose: function(component, event, helper) {
		component.set("v.displayPopupPhoto", false);
	},
	onSelectPhoto: function(component, event, helper) {
		console.log("Select Photo");

		console.log(event.target.src);

		component.set("v.displayPopupUrl", event.target.src);
		component.set("v.displayPopupPhoto", true);

	}
})