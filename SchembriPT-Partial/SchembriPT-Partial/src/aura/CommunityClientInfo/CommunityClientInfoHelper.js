({
	init: function(component, helper) {
		const pageReference = component.get('v.pageReference');

        if (pageReference && pageReference.state) {
            const clientID = pageReference.state.c__clientID;

			console.log("Client Id: ", clientID);

            // clientID holds the record ID for the client's Account
            component.set('v.clientID', clientID);
        }

        helper.loadInfo(component, helper);
        //helper.loadPhotos(component, helper);
	},
    loadInfo: function(component, helper) {
        const clientID = component.get('v.clientID');

        const getClientInfo = component.get('c.getClientInfo');

        getClientInfo.setParams({clientID: clientID});
        getClientInfo.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                const info = response.getReturnValue();

				component.set('v.clientID',				info.clientId);
                component.set('v.clientName',			info.clientName);
				component.set("v.clientGender",			info.clientGender);

                component.set('v.checkInID',			info.checkInID);
                component.set('v.checkInName',			info.checkInName);
                component.set('v.checkInNumber',		info.checkInNumber);
				component.set('v.isAdminProfile',		info.isAdminProfile);
                component.set('v.checkInGoals',			info.checkInGoals);
                component.set('v.programName',			info.programName);
				if(info.programStart) {
					component.set('v.programStart',		helper.formatDate(info.programStart));
				}
				if(info.programEnd) {
					component.set('v.programEnd',		helper.formatDate(info.programEnd));
				}
                component.set('v.programFrequency',		info.programFrequency);
                component.set('v.hasProgram',			(info.programID != undefined && info.programID != null));
                component.set('v.hasCheckIn',			(info.checkInID != undefined && info.checkInID != null));
                component.set('v.hasGoals',				(info.checkInGoals != undefined && info.checkInGoals != null));
				if(info.checkInDate) {
					component.set('v.checkInDate',		helper.formatDate(info.checkInDate));
				}

				var urlString = window.location.href;
				var baseURL = urlString.substring("https://".length);
				baseURL = baseURL.substring(baseURL.indexOf('/'));
				baseURL = baseURL.substring(0, baseURL.indexOf('/s'));
				component.set('v.startURL', baseURL);
				
				component.set("v.checkInAvailable",		info.checkInAvailable);
				component.set("v.checkInSubmitted",		info.checkInSubmitted);
				component.set("v.checkInBeforeDate",	info.checkInBeforeDate);
				component.set("v.checkInAfterDate",		info.checkInBeforeDate);
				component.set("v.checkInStage",			info.checkInStage);
				
				this.loadPhotoOptions(
					component,
					(success) => {
						console.log("[Load Photo Options] SUCCESS: ", JSON.parse(JSON.stringify(success)));
						
						let photoOptions = [];
						
						if(success.hasPreviousCheckIn === true) {
							photoOptions.push({
								label: 'Previous Check-In', 
								value: 'PreviousCheck-In'
							});
						}
						if(success.has3Months === true) {
							photoOptions.push({
								label: '3 Months', 
								value: '3Months'
							});
						}
						if(success.has6Months === true) {
							photoOptions.push({
								label: '6 Months', 
								value: '6Months'
							});
						}
						if(success.has1Year === true) {
							photoOptions.push({
								label: '1 Year', 
								value: '1Year'
							});
						}
						if(success.has2Years === true) {
							photoOptions.push({
								label: '2 Year', 
								value: '2Year'
							});
						}
						if(success.hasAllTime === true) {
							photoOptions.push({
								label: 'All Time', 
								value: 'AllTime'
							});
						}
						if(success.hasProgramStart === true) {
							photoOptions.push({
								label: 'Program Start', 
								value: 'CurrentProgram'
							});	
						}

						component.set("v.photoSelections", photoOptions);
						this.loadPhotos(component, helper);

					}, (error) => {
						console.error("[Load Photo Options] ERROR: ", JSON.parse(JSON.stringify(error)));
					}
				);


            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getClientInfo);
    },
    formatDate: function(date) {
        console.log('Formatting date: ' + date);
        
        const d = new Date(date);
        
        console.log('Got date: ', d);
        
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

        return da + ' ' + mo + ' ' + ye;
    },
	loadPhotoOptions: function(component, onSuccess, onError) {
		
		let getPhotoOptionsAction = component.get("c.GetPhotoOptions");
		getPhotoOptionsAction.setParams({
			accountId: component.get("v.clientID")
		});
		getPhotoOptionsAction.setCallback(this, function(response) {
			const state = response.getState();

			if(state === 'SUCCESS') {
				if(onSuccess) {
					onSuccess(response.getReturnValue());
				}
			} else if (state == 'ERROR') {
				if(onError) {
					onError(response.getError());
				}
            }

		});
        $A.enqueueAction(getPhotoOptionsAction);

	},
    loadPhotos: function(component, helper) {

        const clientID	= component.get('v.clientID');
		const checkInID = component.get('v.checkInID');
		const range		= component.get('v.photoRange');
        const getPhotos = component.get('c.getPhotos');

		const photoInfoSent = {
			clientID:	clientID,
			//checkInID:	checkInID,
			range:		range
		};

		console.log('Info', photoInfoSent);

        getPhotos.setParams(photoInfoSent);
        getPhotos.setCallback(this, function(response) {
            const state = response.getState();

			console.log('Loading Returned');

            if (state == 'SUCCESS') {

                component.set('v.latestPhotos',			JSON.parse(JSON.stringify(response.getReturnValue().latest)));
				component.set('v.previousPhotos',		JSON.parse(JSON.stringify(response.getReturnValue().previous)));

				component.set('v.hasLatestPhotos',		response.getReturnValue().latest.length > 0);
				component.set('v.hasPreviousPhotos',	response.getReturnValue().previous.length > 0);

				console.log('Latest Photos',	JSON.parse(JSON.stringify(component.get('v.latestPhotos'))));
				console.log('Previous Photos',	JSON.parse(JSON.stringify(component.get('v.previousPhotos'))));

				//Clear the Photos at the start
				component.set('v.frontPreviousPhoto', null);
				component.set('v.sidePreviousPhoto', null);
				component.set('v.backPreviousPhoto', null);

				component.set('v.frontLatestPhoto', null);
				component.set('v.sideLatestPhoto', null);
				component.set('v.backLatestPhoto', null);


				for(let i = 0; i < response.getReturnValue().previous.length; i++) {
					if(response.getReturnValue().previous[i].name === 'Front') {
						component.set('v.frontPreviousPhoto', response.getReturnValue().previous[i]);
					}
					if(response.getReturnValue().previous[i].name === 'Side') {
						component.set('v.sidePreviousPhoto', response.getReturnValue().previous[i]);
					}
					if(response.getReturnValue().previous[i].name === 'Back') {
						component.set('v.backPreviousPhoto', response.getReturnValue().previous[i]);
					}
				}
				
				for(let i = 0; i < response.getReturnValue().latest.length; i++) {
					if(response.getReturnValue().latest[i].name === 'Front') {
						component.set('v.frontLatestPhoto', response.getReturnValue().latest[i]);
					}
					if(response.getReturnValue().latest[i].name === 'Side') {
						component.set('v.sideLatestPhoto', response.getReturnValue().latest[i]);
					}
					if(response.getReturnValue().latest[i].name === 'Back') {
						component.set('v.backLatestPhoto', response.getReturnValue().latest[i]);
					}
				}








            } else if (state == 'ERROR') {
                console.error(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getPhotos);
    }
})