({
    loadInfo: function(component, helper) {
       	const getClientInfo = component.get('c.getClientInfo');
        
        console.log('Loading client info');
        
        getClientInfo.setParams({clientID: null});
        getClientInfo.setCallback(this, function(response) {
            console.log('callback');
            const state = response.getState();
            console.log('Got client info: ' + state);
            if (state == 'SUCCESS') {
                const info = response.getReturnValue();
                console.log('Info', info);

                component.set('v.clientName', info.clientName);
                component.set('v.trainerID', info.trainerID);
                component.set('v.trainerName', info.trainerName);
                
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        console.log('queuing');
        $A.enqueueAction(getClientInfo);
    }
})