({
	init: function(component, event, helper) {
		const getProfilePhoto = component.get('c.getProfilePhoto');
        
        getProfilePhoto.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
            	component.find('profile').getElement().src = response.getReturnValue();
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getProfilePhoto);
        
        helper.loadInfo(component, helper);
	},
    menuSelect: function(component, event, helper) {
        var item = event.getParam("value");
        
        if (item == 'account') {
            var urlEvent = $A.get('e.force:navigateToURL');
            urlEvent.setParams({
                'url': '/schembriPTv2/s/profile-photo'
            });
            urlEvent.fire();
        }
    },
    messageToTrainer: function(component, event, helper) {
        component.set('v.messageToID', component.get('v.trainerID'));
        component.set('v.messageToName', component.get('v.trainerName'));
        component.set('v.sendMessage', true);
    },
    messageToGroup: function(component, event, helper) {
        component.set('v.messageTo', 'Group');
        component.set('v.sendMessage', true);
    },
    messageSend: function(component, event, helper) {
        console.log('Sending');
        component.set('v.sendMessage', false);
    },
    messageCancel: function(component, event, helper) {
        console.log('Cancelling');
        component.set('v.sendMessage', false);
    }
})