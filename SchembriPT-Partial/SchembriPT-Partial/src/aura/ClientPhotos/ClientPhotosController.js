({
	onInit: function (component, event, helper) {

		
		const checkInId = component.get("v.recordId");

		const getClientId = component.get("c.GetClientIdFromCheckIn");
		getClientId.setParams({checkInId: checkInId});
		getClientId.setCallback(this, function(response) {

			const state = response.getState();

			if(state === 'SUCCESS') {
                const clientId = response.getReturnValue();
				component.set('v.clientId', clientId);

				helper.initialisePhotoOptions(component, () => {
					helper.loadPhotos(component);
				});

			} else { 
                console.error("Error", JSON.stringify(response.getError()));
			}

		});
        $A.enqueueAction(getClientId);
	},
	onPhotoSelect: function (component, event, helper) {
		let photoType = event.target.name;
		helper.displayModal(component, photoType);
	},
	onCloseModal: function(component, event, helper) {
		component.set("v.displayModal", false);
	},
	onPeriodValueChange: function(component, event, helper) {
		helper.loadPhotos(component);
	},
	onPreviousImageMouseDown: function (component, event, helper) {
		
		let pView		= component.get("v.photoView");

		console.log('On Mouse Down');
		let scrollElement	= component.find(pView + "-Previous-Scroll").getElement();

		let pos = { 
			left: scrollElement.scrollLeft,
			top: scrollElement.scrollTop,
			x: event.clientX,
			y: event.clientY 
		};
		
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", true);

	},
	onPreviousImageMouseMove: function (component, event, helper) {
	
		let pView		= component.get("v.photoView");

		console.log('On Mouse Move');

		if(component.get("v.canScroll") === true) {
			let scrollElement	= component.find(pView + "-Previous-Scroll").getElement();
			let pos				= component.get("v.scrollPos");
			const dx = event.clientX - pos.x;
			const dy = event.clientY - pos.y;
			scrollElement.scrollTop		= pos.top - dy;
			scrollElement.scrollLeft	= pos.left - dx;
		}
		
	},
	onPreviousImageMouseUp: function (component, event, helper) {
		let pos = { 
			left:	0,
			top:	0,
			x:		0,
			y:		0
		};
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", false);
	},
	onCurrentImageMouseDown: function (component, event, helper) {
		
		let pView		= component.get("v.photoView");

		console.log('On Mouse Down');
		let scrollElement	= component.find(pView + "-Current-Scroll").getElement();

		let pos = { 
			left: scrollElement.scrollLeft,
			top: scrollElement.scrollTop,
			x: event.clientX,
			y: event.clientY 
		};
		
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", true);

	},
	onCurrentImageMouseMove: function (component, event, helper) {
	
		let pView		= component.get("v.photoView");

		console.log('On Mouse Move');

		if(component.get("v.canScroll") === true) {
			let scrollElement	= component.find(pView + "-Current-Scroll").getElement();
			let pos				= component.get("v.scrollPos");
			const dx = event.clientX - pos.x;
			const dy = event.clientY - pos.y;
			scrollElement.scrollTop		= pos.top - dy;
			scrollElement.scrollLeft	= pos.left - dx;
		}
		
	},
	onCurrentImageMouseUp: function (component, event, helper) {
		let pos = { 
			left:	0,
			top:	0,
			x:		0,
			y:		0
		};
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", false);
	},
	onModalImageMouseDown: function (component, event, helper) {

		let pView		= component.get("v.photoView");
		let pDisplay	= component.get("v.prevCurrDisplay");

		console.log('On Mouse Down');

		let scrollElement	= component.find(pView + "-" + pDisplay + "-Scroll").getElement();

		let pos = { 
			left: scrollElement.scrollLeft,
			top: scrollElement.scrollTop,
			x: event.clientX,
			y: event.clientY 
		};
		
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", true);
	},
	onModalImageMouseMove: function (component, event, helper) {
	
		let pView		= component.get("v.photoView");
		let pDisplay	= component.get("v.prevCurrDisplay");

		console.log('On Mouse Move');

		if(component.get("v.canScroll") === true) {
			let scrollElement	= component.find(pView + "-" + pDisplay + "-Scroll").getElement();
			let pos				= component.get("v.scrollPos");
			const dx = event.clientX - pos.x;
			const dy = event.clientY - pos.y;
			scrollElement.scrollTop		= pos.top - dy;
			scrollElement.scrollLeft	= pos.left - dx;
		}
		
	},
	onModalImageMouseUp: function (component, event, helper) {
		console.log('On Mouse Up');
		let pos = { 
			left:	0,
			top:	0,
			x:		0,
			y:		0
		};
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", false);
	},
	onModalImageMouseLeave: function(component, event, helper) {
	
		let pos = { 
			left:	0,
			top:	0,
			x:		0,
			y:		0
		};
		component.set("v.scrollPos", pos);
		component.set("v.canScroll", false);
	}
})