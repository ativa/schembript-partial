({
	initialisePhotoOptions: function(component, onSuccess) {

		const getPhotoOptionsAction = component.get("c.GetPhotoOptions");
		getPhotoOptionsAction.setParams({accountId: component.get('v.clientId')});
		getPhotoOptionsAction.setCallback(this, function(response) {

			const state = response.getState();
			if(state === 'SUCCESS') {
                const photoOptions = response.getReturnValue();
				console.log("Photo Options", JSON.parse(JSON.stringify(photoOptions)));

				let options = [];
				
				if(photoOptions.hasPreviousCheckIn) {
					options.push({
						label: 'Prev. Check-In',
						value: 'PreviousCheck-In'
					});
				}
				if(photoOptions.has3Months) {
					options.push({
						label: '3 Months',
						value: '3Months'
					});
				}
				if(photoOptions.has6Months) {
					options.push({
						label: '6 Months',
						value: '6Months'
					});
				}
				if(photoOptions.has1Year) {
					options.push({
						label: '1 Year',
						value: '1Year'
					});

				}
				if(photoOptions.has2Years) {
					options.push({
						label: '2 Year',
						value: '2Year'
					});
				}
				if(photoOptions.hasProgramStart) {
					options.push({
						label: 'Program Start',
						value: 'CurrentProgram'
					});
				}
				if(photoOptions.hasAllTime) {
					options.push({
						label: 'All Time',
						value: 'AllTime'
					});
				}

				component.set("v.periodOptions", options);

				onSuccess();

			} else { 
                console.error("Error", JSON.stringify(response.getError()));
			}
			
		});
        $A.enqueueAction(getPhotoOptionsAction);


	},
	loadImage: function (imgURL, onSuccess) {
		var newImage = new Image();
		newImage.src = imgURL;
		
		newImage.onload = function() {
			onSuccess(newImage);
		};
	},
	displayModal: function(component, photoType) {
	
		if(photoType === "current-front") {
			component.set("v.photoView",		"Front");
			component.set("v.prevCurrDisplay",	"Current");
			component.set("v.displayModal",		true);
		} else if(photoType === "current-side") {
			component.set("v.photoView",		"Side");
			component.set("v.prevCurrDisplay",	"Current");
			component.set("v.displayModal",		true);
		} else if(photoType === "current-back") {
			component.set("v.photoView",		"Back");
			component.set("v.prevCurrDisplay",	"Current");
			component.set("v.displayModal",		true);
		} else if(photoType === "previous-front") {
			component.set("v.photoView",		"Front");
			component.set("v.prevCurrDisplay",	"Previous");
			component.set("v.displayModal",		true);
		} else if(photoType === "previous-side") {
			component.set("v.photoView",		"Side");
			component.set("v.prevCurrDisplay",	"Previous");
			component.set("v.displayModal",		true);
		} else if(photoType === "previous-back") {
			component.set("v.photoView",		"Back");
			component.set("v.prevCurrDisplay",	"Previous");
			component.set("v.displayModal",		true);
		}

	},
    loadPhotos: function(component) {

		component.set("v.isLoading", true);

        const clientID	= component.get('v.clientId');
		const checkInID = component.get('v.recordId');
		const range		= component.get('v.periodValue');
       
        const getPhotos = component.get('c.getPhotosForCheckIn');

        getPhotos.setParams({ clientID, checkInID, range });
        getPhotos.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                const photos = response.getReturnValue();
                
                console.log('Photos', response.getReturnValue());

				component.set("v.clientGender", photos.clientGender);

                if (photos) {

					component.set("v.prevCheckInId",	photos.previousCheckInId);
					component.set("v.prevCheckInName",	photos.previousCheckInName);
					component.set("v.currCheckInId",	photos.latestCheckInId);
					component.set("v.currCheckInName",	photos.latestCheckInName);

					//Set the latest, and previous photosets
                    let latestPhotoset		= this.groupPhotoSet(photos.latest);
                    let previousPhotoset	= this.groupPhotoSet(photos.previous);

					if(previousPhotoset.front) {
						component.set("v.frontPrevImage", previousPhotoset.front.DropboxPath);	
					} else {
						component.set("v.frontPrevImage", null);	
					}

					if(previousPhotoset.side) {
						component.set("v.sidePrevImage", previousPhotoset.side.DropboxPath);	
					} else {
						component.set("v.sidePrevImage", null);	
					}

					if(previousPhotoset.back) {
						component.set("v.backPrevImage", previousPhotoset.back.DropboxPath);	
					} else {
						component.set("v.backPrevImage", null);	
					}

					if(latestPhotoset.front) {
						component.set("v.frontCurrImage", latestPhotoset.front.DropboxPath);	
					} else {
						component.set("v.frontCurrImage", null);	
					}

					if(latestPhotoset.side) {
						component.set("v.sideCurrImage", latestPhotoset.side.DropboxPath);	
					} else {
						component.set("v.sideCurrImage", null);	
					}

					if(latestPhotoset.back) {
						component.set("v.backCurrImage", latestPhotoset.back.DropboxPath);	
					} else {
						component.set("v.backCurrImage", null);	
					}

					console.log('Latest photos', JSON.parse(JSON.stringify(latestPhotoset)));
					console.log('Previous photos', JSON.parse(JSON.stringify(previousPhotoset)));
					
					this.loadAspectRatios(component);

                }


            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));

				component.set("v.isLoading", false);

            }
        });

        $A.enqueueAction(getPhotos);
    },
	loadAspectRatios: function(component) {
	
		this.loadImage(component.get("v.frontPrevImage"), (x) => {
			let aspectRatio = x.width / x.height;
			component.set("v.frontPrevRatio", aspectRatio);
		});
		this.loadImage(component.get("v.sidePrevImage"), (x) => {
			let aspectRatio = x.width / x.height;
			component.set("v.sidePrevRatio", aspectRatio);
		});
		this.loadImage(component.get("v.backPrevImage"), (x) => {
			let aspectRatio = x.width / x.height;
			component.set("v.backPrevRatio", aspectRatio);
		});
		this.loadImage(component.get("v.frontCurrImage"), (x) => {
			let aspectRatio = x.width / x.height;
			component.set("v.frontCurrRatio", aspectRatio);
		});
		this.loadImage(component.get("v.sideCurrImage"), (x) => {
			let aspectRatio = x.width / x.height;
			component.set("v.sideCurrRatio", aspectRatio);
		});
		this.loadImage(component.get("v.backCurrImage"), (x) => {
			let aspectRatio = x.width / x.height;
			component.set("v.backCurrRatio", aspectRatio);
		});
		component.set("v.isLoading", false);

	},
	groupPhotoSet: function(photos) {
	
		let photoSet = {};
		for(let i = 0; i < photos.length; i++) {

			let p = photos[i];
			if(p.name === "Front") {
				photoSet.front = p;
			}
			if(p.name === "Side") {
				photoSet.side = p;
			}
			if(p.name === "Back") {
				photoSet.back = p;
			}
		}

		return photoSet;
	}
})