({
	initialiseCharts: function (component) {

		//Initialise the charts
		let allCharts		= component.get("v.charts");

		var weightCtx		= component.find("weight-chart").getElement().getContext("2d");
		var weightChart		= new Chart(weightCtx.canvas, JSON.parse(allCharts.weight));
		
		var chestCtx		= component.find("chest-chart").getElement().getContext("2d");
		var chestChart		= new Chart(chestCtx.canvas, JSON.parse(allCharts.chest));
		
		var waistCtx		= component.find("waist-chart").getElement().getContext("2d");
		var waistChart		= new Chart(waistCtx.canvas, JSON.parse(allCharts.waist));
		
		var hipsCtx			= component.find("hips-chart").getElement().getContext("2d");
		var hipsChart		= new Chart(hipsCtx.canvas, JSON.parse(allCharts.hips));
		
		var thighCtx		= component.find("thigh-chart").getElement().getContext("2d");
		var thighChart		= new Chart(thighCtx.canvas, JSON.parse(allCharts.thigh));
		
		var calorieCtx		= component.find("calorie-chart").getElement().getContext("2d");
		var calorieChart	= new Chart(calorieCtx.canvas, JSON.parse(allCharts.calorie));
		
		//Save the Charts for later use
		component.set("v.weightChart",	weightChart);
		component.set("v.chestChart",	chestChart);
		component.set("v.waistChart",	waistChart);
		component.set("v.hipsChart",	hipsChart);
		component.set("v.thighChart",	thighChart);
		component.set("v.calorieChart", calorieChart);

	},
	updateData: function(component) {

		
		var getDataAction = component.get("c.GetData");

		getDataAction.setParams({ 
			checkInId:	component.get("v.recordId"),
			range:		component.get("v.queryType")
		});
		getDataAction.setCallback(this, (response) => {
			var state = response.getState();
			if(state === "SUCCESS") {
				console.log("Loaded Data succesfully", JSON.parse(JSON.stringify(response.getReturnValue())));

				let data = response.getReturnValue();
				
				//Update the chart data
				let weightChart									= component.get("v.weightChart");
				weightChart.options.scales.yAxes[0].ticks.min	= data.minWeightValue;
				weightChart.options.scales.yAxes[0].ticks.max	= data.maxWeightValue;
				weightChart.data.datasets[0].data				= data.weightData;
				weightChart.data.labels							= data.dateLabels;
				weightChart.update();
				let chestChart									= component.get("v.chestChart");
				chestChart.options.scales.yAxes[0].ticks.min	= data.minChestValue;
				chestChart.options.scales.yAxes[0].ticks.max	= data.maxChestValue;
				chestChart.data.datasets[0].data				= data.chestData;
				chestChart.data.labels							= data.dateLabels;
				chestChart.update();
				let waistChart									= component.get("v.waistChart");
				waistChart.options.scales.yAxes[0].ticks.min	= data.minWaistValue;
				waistChart.options.scales.yAxes[0].ticks.max	= data.maxWaistValue;
				waistChart.data.datasets[0].data				= data.waistData;
				waistChart.data.labels							= data.dateLabels;
				waistChart.update();
				let hipsChart									= component.get("v.hipsChart");
				hipsChart.options.scales.yAxes[0].ticks.min		= data.minHipsValue;
				hipsChart.options.scales.yAxes[0].ticks.max		= data.maxHipsValue;
				hipsChart.data.datasets[0].data					= data.hipsData;
				hipsChart.data.labels							= data.dateLabels;
				hipsChart.update();
				let thighChart									= component.get("v.thighChart");
				thighChart.options.scales.yAxes[0].ticks.min	= data.minThighValue;
				thighChart.options.scales.yAxes[0].ticks.max	= data.maxThighValue;
				thighChart.data.datasets[0].data				= data.thighData;
				thighChart.data.labels							= data.dateLabels;
				thighChart.update();
				let calorieChart								= component.get("v.calorieChart");
				calorieChart.options.scales.yAxes[0].ticks.max	= data.maxCalorieValue;
				calorieChart.data.datasets[0].data				= data.calorieData;
				calorieChart.data.labels						= data.dateLabels;
				calorieChart.update();

				component.set("v.periodValue",	data.periodValue);
				component.set("v.periodType",	data.periodType);

			} else if (state === "ERROR") {
				console.error("ERROR", JSON.parse(JSON.stringify(response.getError())));
			}
		});
		$A.enqueueAction(getDataAction);

	}
})