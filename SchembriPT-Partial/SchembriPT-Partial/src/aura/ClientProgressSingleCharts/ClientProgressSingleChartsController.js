({
	onInit: function (component, event, helper) {
		var getDataAction = component.get("c.InitProgressCharts");

		getDataAction.setParams({ 
			checkInId:	component.get("v.recordId"),
			range:		component.get("v.queryType")
		});
		getDataAction.setCallback(this, (response) => {
			var state = response.getState();
			if(state === "SUCCESS") {
				console.log("Loaded Data succesfully", JSON.parse(JSON.stringify(response.getReturnValue())));

				component.set("v.metrics",		response.getReturnValue().metrics);
				component.set("v.charts",		response.getReturnValue().charts);
				
				//Set the period values
				component.set("v.periodValue",	response.getReturnValue().periodValue);
				component.set("v.periodType",	response.getReturnValue().periodType);

				//Track that the data has been initialised
				component.set("v.dataInit", true);

				if(component.get("v.chartJsInit") === true && component.get("v.dataInit") === true) {
					helper.initialiseCharts(component);
				}

			} else if (state === "ERROR") {
				console.error("ERROR", JSON.parse(JSON.stringify(response.getError())));
			}
		});
		$A.enqueueAction(getDataAction);

	},
	onChartJSLoad: function(component, event, helper) {
	
		console.log("ChartJS Scripts loaded");

		//Track that the data has been initialised
		component.set("v.chartJsInit", true);

		if(component.get("v.chartJsInit") === true && component.get("v.dataInit") === true) {
			helper.initialiseCharts(component);
		}

	},
	onDatasetChange: function(component, event, helper) {
		helper.updateData(component);
	}
})