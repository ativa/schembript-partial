({
	onInit: function (component, event, helper) {

		//Get the account and user IDs
		let userAction = component.get("c.getUserInfo");
		userAction.setCallback(this, function(response) {
			var state = response.getState();
			if(state === 'SUCCESS') {
                const info = response.getReturnValue();

				console.log("Info: ", JSON.parse(JSON.stringify(info)));
				
				if(info.communityPortalPage != "Restart (>4 wks)") {
					var navService = component.find("navService");
					navService.navigate({
						type: "standard__webPage", 
						attributes: { 
							url: '/' 
						} 
					});
				}


				component.set("v.userId",		info.userID);
                component.set("v.recordId",		info.accountID);
				component.set("v.checkInId",	info.latestCheckInId);
                component.set("v.portalURL",	info.portalURL);

			} else if(state === "ERROR") {
				console.error(response.getError());
			}
		});
		
		$A.enqueueAction(userAction);

	},
	onStepSuccess: function (component, event, helper) {

		let stepIndex = component.get("v.stepIndex");
		
		component.set("v.submittingForm", false);

		//If we are at the end, then move to the main page
		if(stepIndex === 7) {
			let flagSetAction = component.get("c.FlagFormAsCompleted");
			flagSetAction.setParams({
				accountId: component.get("v.recordId")
			});
			flagSetAction.setCallback(this, function(response) {
				var state = response.getState();
				if(state === 'SUCCESS') {
					document.location.href = component.get('v.portalURL');
				} else if(state === "ERROR") {
					console.error(response.getError());
					helper.displayErrorMessage("Error Occured", "Form data received.  Completion error - no further action needed (data has been stored). Schembri PT has been alerted to your submission.");
				}
			});
			$A.enqueueAction(flagSetAction);
		} else {
			//Else continue to the next step
			component.set("v.stepIndex", stepIndex + 1);
		}

	},
	onStepSubmit: function(component, event, helper) {
	
		let stepIndex = component.get("v.stepIndex");

		if(stepIndex == 3) {
			component.set("v.stepIndex", stepIndex + 1);
		} else {
			component.set("v.submittingForm", true);
			let stepForm				= component.find('Step' + stepIndex + 'Form');
			let isSubmissionSuccessful = stepForm.Submit();
			if(isSubmissionSuccessful === false) {
				component.set("v.submittingForm", false);
			}
		}


		
	},
	onNavigateToSchembriPTPage: function(component, event, helper) {
		window.location.href = "https://schembript.com.au/";
	}
})