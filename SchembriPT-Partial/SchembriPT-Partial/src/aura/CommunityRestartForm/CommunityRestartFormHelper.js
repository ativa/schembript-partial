({
	getInitialValues: function (component, onSuccess, onError) {

	
		//console.log("User Id: " + component.get("v.userId"));

		var getParentRecordAction = component.get("c.getInitialValues");
		//getParentRecordAction.setParams({ userId: component.get("v.userId") });

		getParentRecordAction.setCallback(this, function(response) {
		
			var state = response.getState();
            
            console.log('Initial values: ' + state);

			if(state === 'SUCCESS') {
				onSuccess(response.getReturnValue());
			} else if(state === 'ERROR') {
				onError(response.getError());
			}
		});
		$A.enqueueAction(getParentRecordAction);
	},
	updateFileName: function(component, documentId, fileName) {
		
		const clientId	= component.get('v.accountId');
        const checkInId = component.get('v.checkInId');
        
        const recordPhotoAction = component.get('c.recordPhoto');
        
        recordPhotoAction.setParams({
            clientID:	clientId,
            checkInID:	checkInId,
            name:		fileName,
            documentID: documentId
        });

		recordPhotoAction.setCallback(this, function(response) {
			const state = response.getState();
			if (state == 'SUCCESS') {
				const photoId = response.getReturnValue();
				component.set('v.' + fileName + 'PhotoId', photoId);
			} else if (state == 'ERROR') {
				console.error('ERROR');
				console.error(JSON.stringify(response.getError()));
			}
		});

		$A.enqueueAction(recordPhotoAction);

	},
	deleteFile: function(component, fileName) {
		
		const versionId = component.get('v.' + fileName + 'PhotoId');
        const checkInId = component.get('v.checkInId');

		const deletePhotoAction = component.get('c.deletePhoto');
		deletePhotoAction.setParams({
            checkInId:	checkInId,
            versionId:	versionId,
        });
		deletePhotoAction.setCallback(this, function(response) {

			const state = response.getState();
			if (state == 'SUCCESS') {
				const photoId = response.getReturnValue();
				component.set('v.' + fileName + 'PhotoId', null);
			} else if (state == 'ERROR') {
				console.error('ERROR');
				console.error(JSON.stringify(response.getError()));
			}

		});
		$A.enqueueAction(deletePhotoAction);

	},
	allFormsSaved: function(component) {

		let checkInSaved	= component.get('v.checkInSaved');
		let caseSaved		= component.get('v.caseSaved');
		let accountSaved	= component.get('v.accountSaved');
		return checkInSaved === true && caseSaved === true && accountSaved === true;

	},
	saveForm: function(component) {

		

	},
	displayErrorMessage: function(title, msg) {
		
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title":	title,
			"message":	msg,
			"type":		"error"
		});
		toastEvent.fire();

	}
})