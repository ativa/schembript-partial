({
	init: function(component, helper) {
		/*const pageReference = component.get('v.pageReference');
        
        if (pageReference && pageReference.state) {
            const feedID = pageReference.state.c__feedID;
            
            component.set('v.feedID', feedID);
        }*/
        
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        console.log('URL:', sPageURL);
        console.log('Variables:', sURLVariables);
        
        let feedID;
        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            if (sParameterName[0] === 'c__feedID') { //lets say you are looking for param name - firstName
                //sParameterName[1] === undefined ? 'Not found' : sParameterName[1];
                feedID = sParameterName[1];
            }
        }
        
        console.log('FeedID:', feedID);
        
        if (feedID) component.set('v.feedID', feedID);
	}
})