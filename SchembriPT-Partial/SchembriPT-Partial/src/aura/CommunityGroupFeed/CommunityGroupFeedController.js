({
	init: function(component, event, helper) {
        helper.init(component, helper);
	},
    pageReferenceChanged: function(component, event, helper) {
        helper.init(component, helper);
    }
})