({
	displayErrorMessage: function(title, msg) {
		
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title":	title,
			"message":	msg,
			"type":		"error"
		});
		toastEvent.fire();

	}
})