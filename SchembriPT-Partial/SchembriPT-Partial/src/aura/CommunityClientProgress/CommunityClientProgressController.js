({
	onComponentInit: function (component, event, helper) {
        // Use compact mode if the user is viewing this one a phone, or if it's on a record page
		component.set('v.compactMode', (component.get('v.isPhone') || (component.get('v.displayMode') === "Compact")));

		component.set("v.periodOptions",	helper.setupPeriodOptions());
		//component.set("v.visibleLines",		helper.setupDatasetVisibilityOptions());
		
		//Get the current Account Id
		let accountId = component.get("v.accountId");
		console.log(accountId);
		
		if(accountId) {
			var a = component.get("c.onAccountIdUpdate");
			$A.enqueueAction(a);
		} else {

			var getAccountID = component.get("c.getAccountID");
            
            getAccountID.setParams({recordID: component.get('v.recordId')});
			getAccountID.setCallback(this, function(response) {
				component.set('v.accountId', response.getReturnValue());

				console.log('Account Id', response.getReturnValue());

			});
			$A.enqueueAction(getAccountID);
		} 

		console.log("Init Component");

		//Add resize listener
		window.addEventListener('resize', $A.getCallback(function(){

			//After the scripts have loaded, reload the chart
			if(component.get("v.chartJSScriptLoaded") === true) {
				let chart = component.get("v.progressChart");
				chart.update();
			}

        }));       
		
	},
	onLineVisibilityUpdate : function (component, event, helper) {
		let vis = component.get("v.visibleLines");		
		console.log(JSON.parse(JSON.stringify(vis)));
		//Update the chart
		helper.updateChart(component);

	},
	onAccountIdUpdate: function (component, event, helper) {
		var accountId = component.get("v.accountId");

		let dateRange = helper.getDateRange(component);

		console.log("[CommunityClientProgress] Date Range", dateRange);

		helper.getClientProgress(component, accountId, dateRange.startDate, dateRange.endDate, (success) => {
			console.log("SUCCESS");
			console.log(success);

			//Group the Progress data together
			let progressData = helper.groupProgressData(success.checkIns);

			console.log("+------------Progress Data-------------------------------------------------------+");
			console.log(progressData);
			console.log("+--------------------------------------------------------------------------------+");

			component.set("v.progressData", progressData);
			
			//Setup the dates
			helper.setupDates(component, progressData.dates);
			
			//Update the data that is displayed to the client
			helper.updateVisualData(component);
			
			//Update the chart
			helper.updateChart(component);
			
		}, (error) => {
			console.log("ERROR");
			console.log(error);
		});

	},
	onFirstDateUpdate: function (component, event, helper) {
		
		//Update the second date options
		helper.updateSecondOptions(component);
		
		//Update the data that is displayed to the client
		helper.updateVisualData(component);
		
		//Update the chart
		helper.updateChart(component);
			
	},
	onSecondDateUpdate: function (component, event, helper) {
		//Update the first date options
		helper.updateFirstOptions(component);
		
		//Update the data that is displayed to the client
		helper.updateVisualData(component);
		
		//Update the chart
		helper.updateChart(component);
			
	},
	onPeriodValueUpdate: function (component, event, helper) {
		var accountId = component.get("v.accountId");

		let dateRange = helper.getDateRange(component);

		console.log(dateRange);

		helper.getClientProgress(component, accountId, dateRange.startDate, dateRange.endDate, (success) => {
			console.log("SUCCESS");
			console.log(success);

			//Group the Progress data together
			let progressData = helper.groupProgressData(success.checkIns);

			console.log("+------------Progress Data------------+");
			console.log(progressData);
			console.log("+-------------------------------------+");

			component.set("v.progressData", progressData);

			//Setup the dates
			helper.setupDates(component, progressData.dates);

			//Update the data that is displayed to the client
			helper.updateVisualData(component);

			//Update the chart
			helper.updateChart(component);

			
			component.set("v.visProgressData", JSON.parse(JSON.stringify(component.get("v.visProgressData"))));
			
		}, (error) => {
			console.log("ERROR");
			console.log(error);
		});

	},
	onChartJSLoad: function(component, event, helper) {
		
		console.log("Chart JS: 1");
		helper.initChartJS(component);
		console.log("Chart JS: 2");
		component.set("v.chartJSScriptLoaded", true);
		console.log("Chart JS: 3");

	},
	onWeightDatasetToggle: function(component, event, helper) {
		let visibleLines = component.get('v.visibleLines');
		if(visibleLines[0].selected === true) {
			visibleLines[0].selected = false;
		} else {
			visibleLines[0].selected = true;
		}
		component.set('v.visibleLines', visibleLines);
	},
	onChestDatasetToggle: function(component, event, helper) {
		let visibleLines = component.get('v.visibleLines');
		if(visibleLines[1].selected === true) {
			visibleLines[1].selected = false;
		} else {
			visibleLines[1].selected = true;
		}
		component.set('v.visibleLines', visibleLines);
	},
	onWaistDatasetToggle: function(component, event, helper) {
		let visibleLines = component.get('v.visibleLines');
		if(visibleLines[2].selected === true) {
			visibleLines[2].selected = false;
		} else {
			visibleLines[2].selected = true;
		}
		component.set('v.visibleLines', visibleLines);
	},
	onHipsDatasetToggle: function(component, event, helper) {
		let visibleLines = component.get('v.visibleLines');
		if(visibleLines[3].selected === true) {
			visibleLines[3].selected = false;
		} else {
			visibleLines[3].selected = true;
		}
		component.set('v.visibleLines', visibleLines);
	},
	onThighDatasetToggle: function(component, event, helper) {
		let visibleLines = component.get('v.visibleLines');
		if(visibleLines[4].selected === true) {
			visibleLines[4].selected = false;
		} else {
			visibleLines[4].selected = true;
		}
		component.set('v.visibleLines', visibleLines);
	},
	onCaloriesDatasetToggle: function(component, event, helper) {
		let visibleLines = component.get('v.visibleLines');
		if(visibleLines[5].selected === true) {
			visibleLines[5].selected = false;
		} else {
			visibleLines[5].selected = true;
		}
		component.set('v.visibleLines', visibleLines);
	}
})