({
	initChartJS: function(component) {
	
		//Smaller Size
		let lineDataLabelOptions = {
			anchor: 'start',
			align: 'end',
			display: true,
			font: {
				size: 14,
			},
			offset: 0,
			textStrokeWidth: 3,
			textStrokeColor: 'rgba(33,33,33,1)',
			textShadowBlur: 3,
			textShadowColor: 'rgba(100,100,100,1)',
			color: 'rgba(255,255,255,1)'
		};
		
		let barDataLabelOptions = {
			anchor: 'start',
			align: 'end',
			display: false,
			font: {
				size: 16,
			},
			clamp: true,
			textStrokeWidth: 3,
			textStrokeColor: 'rgba(33,33,33,1)',
			textShadowBlur: 3,
			textShadowColor: 'rgba(100,100,100,1)',
			color: 'rgba(255,155,155,1)'
		}
		
		var ctx = component.find('progress-chart-auraId').getElement().getContext('2d');
		
		var chart = new Chart(ctx.canvas, {
			// The type of chart we want to create
			type: 'bar',
			// The data for our dataset
			data: {
				labels: [],
				datasets: [
				{
					label: 'Weight (kg)',
					type: 'line',
					borderColor: '#382119',
					backgroundColor: 'rgba(0,0,0,0)',
					data: [],
					datalabels: lineDataLabelOptions,
					pointStyle: 'circle',
					radius: 5,
					hoverRadius: 7
				},
				{
					label: 'Chest (cm)',
					type: 'line',
					borderColor: '#85c0f9',
					backgroundColor: 'rgba(0,0,0,0)',
					data: [],
					datalabels: lineDataLabelOptions,
					pointStyle: 'crossRot',
					radius: 5,
					hoverRadius: 7
				},
				{
					label: 'Waist (cm)',
					type: 'line',
					borderColor: '#f5793a',
					backgroundColor: 'rgba(0,0,0,0)',
					data: [],
					datalabels: lineDataLabelOptions,
					pointStyle: 'rectRot',
					radius: 5,
					hoverRadius: 7
				},
				{
					label: 'Hips (cm)',
					type: 'line',
					borderColor: '#a95aa1',
					backgroundColor: 'rgba(0,0,0,0)',
					data: [],
					datalabels: lineDataLabelOptions,
					pointStyle: 'star',
					radius: 5,
					hoverRadius: 7
				},
				{
					label: 'Thigh (cm)',
					type: 'line',
					borderColor: '#5cca44',
					backgroundColor: 'rgba(0,0,0,0)',
					data: [],
					datalabels: lineDataLabelOptions,
					pointStyle: 'triangle',
					radius: 5,
					hoverRadius: 7
				},
				{
					label: 'Calories (cals)',
					type: 'bar',
					borderColor:'rgba(130, 127, 120, 1)',
					borderWidth: 2,
					backgroundColor: 'rgba(130, 127, 120, 0.2)',
					datalabels: barDataLabelOptions,
					labels: [],
					data: [],
					scale: {
						offset: false
					}
				}
				]
			},
			
			// Configuration options go here
			options: {
				layout: {
					padding: {
						left: 0,
						right: 0,
						top: 30,
						bottom: 0
					}
				},
				maintainAspectRatio: false,
				responsive: true,
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							stepSize: 10
							//min: 0
						}
					}],
					xAxes: [
						{
							ticks: {
								autoSkip: false,
								fontSize: 14,
								callback: function(value, index, values) {
                                    if (value) {
										let comps = value.split("-");
										if(comps.length == 3) {

											let day		= comps[2];
											let month	= '<N/A>';
											let year	= comps[0].substring(2);

											if(parseInt(comps[1]) === 1) {
												month = 'Jan';
											} else if(parseInt(comps[1]) === 2) {
												month = 'Feb';
											} else if(parseInt(comps[1]) === 3) {
												month = 'Mar';
											} else if(parseInt(comps[1]) === 4) {
												month = 'Apr';
											} else if(parseInt(comps[1]) === 5) {
												month = 'May';
											} else if(parseInt(comps[1]) === 6) {
												month = 'Jun';
											} else if(parseInt(comps[1]) === 7) {
												month = 'Jul';
											} else if(parseInt(comps[1]) === 8) {
												month = 'Aug';
											} else if(parseInt(comps[1]) === 9) {
												month = 'Sep';
											} else if(parseInt(comps[1]) === 10) {
												month = 'Oct';
											} else if(parseInt(comps[1]) === 11) {
												month = 'Nov';
											} else if(parseInt(comps[1]) === 12) {
												month = 'Dec';
											}

											return month + day + '-' + year;

										} else {
											return '';
										}
                                    } else {
									    return '';
                                    }
								}
							}
						}
					]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							
							let dataset = data.datasets[tooltipItem.datasetIndex];

							if(dataset.label == 'Calories (cals)') {
								let datasetLabel = dataset.label;
								return datasetLabel + ': ' + dataset.labels[tooltipItem.index];
							} else {
								let datasetLabel = dataset.label;
								let dataItem = dataset.data[tooltipItem.index];
								return datasetLabel + ': ' + dataItem;
							}
						}
					}
				},
				plugins: {
					datalabels: {
						formatter: function(value, context) {
							if(context.dataset.label == 'Calories (cals)') {
								return context.dataset.labels[context.dataIndex];
							} else {
								return value;
							}

						}
					}
				}
			}
		});
		
		component.set("v.progressChart", chart);
		
	},
	updateMaxYAxis: function (component) {
		
		let chart		= component.get("v.progressChart");
		let visData		= component.get("v.visProgressData");
		
		//Get the max value
		let maxValue = 0;
		let minValue = 900000000000000;

		for(let i = 0; i < visData.visWeight.length; i++) {

			//Get the max value
			maxValue = Math.max(
				maxValue,
				chart.data.datasets[0].data[i],
				chart.data.datasets[1].data[i],
				chart.data.datasets[2].data[i],
				chart.data.datasets[3].data[i],
				chart.data.datasets[4].data[i]
			);

			minValue = Math.min(
				minValue,
				chart.data.datasets[0].data[i],
				chart.data.datasets[1].data[i],
				chart.data.datasets[2].data[i],
				chart.data.datasets[3].data[i],
				chart.data.datasets[4].data[i]
			);

		}


		
		let maxNearestCieling = (Math.ceil((maxValue) / 10) * 10) + 10;
		chart.options.scales.yAxes[0].ticks.max = maxNearestCieling;
		let minNearestCieling = (Math.floor((minValue) / 10) * 10) - 10;
		chart.options.scales.yAxes[0].ticks.min = minNearestCieling;

		chart.update();

	},
	setupDatasetVisibilityOptions: function () {
		return [
			{ label: "Weight (kg)",		selected: true },
			{ label: "Chest (cm)",		selected: true },
			{ label: "Waist (cm)",		selected: true },
			{ label: "Hips (cm)",		selected: true },
			{ label: "Thigh (cm)",		selected: true },
			{ label: "Calories (cals)", selected: true }
		];
	}, 
	setupPeriodOptions: function () {
		return [
			{ label: "Prev. Check-In",	value: "PreviousCheck-In" },
			{ label: "3 Months",		value: "3Months" },
			{ label: "6 Months",		value: "6Months" },
			{ label: "1 Year",			value: "1Year" },
			{ label: "2 Year",			value: "2Year" },
			{ label: "All Time",		value: "AllTime" },
			{ label: "Program Start",	value: "CurrentProgram" }
		];
	},
	convertDateToISO: function(dd) {

		let year		= dd.getUTCFullYear();
		let month		= dd.getUTCMonth() + 1;
		let date		= dd.getUTCDate();

		let strYear		= year.toString();
		let strMonth	= (month < 10) ? '0' + month : month.toString();
		let strDate		= (date < 10) ? '0' + date : date.toString();

		return strYear + '-' + strMonth + '-' + strDate;

	},
	getDateRange: function (component) {
	
		let perOps = component.get("v.periodOptions");
		let perValue = component.get("v.periodValue");

		let startDate	= new Date(new Date().getFullYear(),	new Date().getMonth(),	new Date().getDate());
		let endDate		= new Date(new Date().getFullYear(),	new Date().getMonth(),	new Date().getDate());

		if(perValue === '3Months') {
			startDate.setMonth(startDate.getMonth() - 3);
		} else if(perValue === '6Months') {
			startDate.setMonth(startDate.getMonth() - 6);
		} else if(perValue === '1Year') {
			startDate.setFullYear(startDate.getFullYear() - 1);
		} else if(perValue === '2Year') {
			startDate.setFullYear(startDate.getFullYear() - 2);
		} else if(perValue === 'AllTime') {
			startDate = new Date(1970,1,1);
		} 
		//If the value is CurrentProgram then don't do anything, we want to use only one date
		
		return { startDate, endDate };

	},
	updateChart: function (component) {

		let visData		= component.get("v.visProgressData");
		let dates		= component.get("v.allDates");

		let startDate	= component.get("v.startDate");
		let endDate		= component.get("v.endDate");
		let firstDate	= component.get("v.firstDate");
		let secondDate	= component.get("v.secondDate");

		//Get the visible lines
		let visLines = component.get("v.visibleLines");
		
		let chart = component.get("v.progressChart");

		//Set the data and visibility in the datasets
		let chartCols = [];

		console.log("StartDate: ",startDate);
		console.log("EndDate: ",endDate);

		chartCols.push(startDate);
		if(firstDate) {
			chartCols.push(firstDate);
		}
		if(secondDate) {
			chartCols.push(secondDate);
		}
		if(endDate) {
			chartCols.push(endDate);
		}

		//Get the max value
		let maxNonCalorieValue = 0;
		let maxCalorieValue = 0;

		for(let i = 0; i < visData.visWeight.length; i++) {

			chart.data.datasets[0].data[i]		= visData.visWeight[i].value;
			chart.data.datasets[0].hidden		= !visLines[0].selected;

			chart.data.datasets[1].data[i]		= visData.visChest[i].value;
			chart.data.datasets[1].hidden		= !visLines[1].selected;

			chart.data.datasets[2].data[i]		= visData.visWaist[i].value;
			chart.data.datasets[2].hidden		= !visLines[2].selected;

			chart.data.datasets[3].data[i]		= visData.visHips[i].value;
			chart.data.datasets[3].hidden		= !visLines[3].selected;

			chart.data.datasets[4].data[i]		= visData.visThigh[i].value;
			chart.data.datasets[4].hidden		= !visLines[4].selected;
			
			//Set the datalabel and visibility of the dataset, we will do the data later
			chart.data.datasets[5].labels[i]	= visData.visCalories[i].value;
			if(chart.data.datasets[5].labels[i]) {
				chart.data.datasets[5].labels[i] = chart.data.datasets[5].labels[i].toFixed(0);
				chart.data.datasets[5].labels[i] = chart.data.datasets[5].labels[i].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}

			chart.data.datasets[5].hidden		= !visLines[5].selected;

			//Get the max value
			maxNonCalorieValue = Math.max(
				maxNonCalorieValue,
				chart.data.datasets[0].data[i],
				chart.data.datasets[1].data[i],
				chart.data.datasets[2].data[i],
				chart.data.datasets[3].data[i],
				chart.data.datasets[4].data[i]
			);

			if(visData.visCalories[i].value) {
				maxCalorieValue = Math.max(
					maxCalorieValue, 
					visData.visCalories[i].value
				);
			}

		}

		//If the max calorie value is 0, then disable the calorie chart
		if(maxCalorieValue === 0) {
			chart.data.datasets[5].hidden = true;
		}

		let nearestCieling = Math.ceil((maxNonCalorieValue + 1) / 10) * 10;
		for(let i = 0; i < visData.visWeight.length; i++) {
			//Get percentage 
			let percent = visData.visCalories[i].value / maxCalorieValue;
			chart.data.datasets[5].data[i]		= nearestCieling * percent;
		}

		//Removed the extra data points
		while(visData.visWeight.length < chart.data.datasets[0].data.length) {
			this.chartRemoveLastElement(chart);
		}


		//Update the chart
		chart.data.labels = chartCols;
		chart.update();

		//Update the max/min value for the Y-Axis
		this.updateMaxYAxis(component);

	},
	chartRemoveLastElement: function(chart) {
		chart.data.labels.pop();
		chart.data.datasets.forEach((dataset) => {
			dataset.data.pop();
		});
	},
	getClientProgress: function(component, accountId, startDate, endDate, onSuccess, onError) {

		//Get the period value, if we are 
		let perValue = component.get("v.periodValue");
		
		if(perValue === 'PreviousCheck-In') {
			var queryPlans = component.get("c.queryLatestCheckIns");

			queryPlans.setParams({
				personAccId:		accountId,
				checkInLimit:		2,
			});

			queryPlans.setCallback(this, function(response) {
			
				var state = response.getState();
				console.log('Reponse found');

				if(state === 'SUCCESS') {
					component.set("v.noDataAvailable", response.getReturnValue().checkIns.length === 0);
					onSuccess(response.getReturnValue());
				} else if(state === "ERROR") {
					onError(response.getError());
				}
			});

			$A.enqueueAction(queryPlans);
		} else if(perValue === 'CurrentProgram') {
			var queryPlans = component.get("c.queryProgram");

			queryPlans.setParams({
				personAccId:	accountId,
				queryDate:		startDate,
			});

			queryPlans.setCallback(this, function(response) {
			
				var state = response.getState();

				if(state === 'SUCCESS') {
					component.set("v.noDataAvailable", response.getReturnValue().checkIns.length === 0);
					onSuccess(response.getReturnValue());
				} else if(state === "ERROR") {
					onError(response.getError());
				}
			});

			$A.enqueueAction(queryPlans);
		} else {
		
			var queryPlans = component.get("c.queryProgress");

			queryPlans.setParams({
				personAccId:	accountId,
				startDate:		startDate,
				endDate:		endDate
			});

			queryPlans.setCallback(this, function(response) {
			
				var state = response.getState();
				
				if(state === 'SUCCESS') {
					component.set("v.noDataAvailable", response.getReturnValue().checkIns.length === 0);
					onSuccess(response.getReturnValue());
				} else if(state === "ERROR") {
					onError(response.getError());
				}
			});

			$A.enqueueAction(queryPlans);
		}
		

		
	},
	groupProgressData: function(progressData) {

		//Get a list of dates
		let dates = [];

		//Get the data for each row
		let weightRow	= [];
		let chestRow	= [];
		let waistRow	= [];
		let hipsRow		= [];
		let thighRow	= [];
		let caloriesRow = [];

		for(let i = 0; i < progressData.length; i++) {
			
			let dataSegment = progressData[i];

			dates.push(dataSegment.Scheduled_Check_In_Date__c);

			weightRow.push({
				date:	dataSegment.Scheduled_Check_In_Date__c,
				value:	dataSegment.Weight__c
			});
			chestRow.push({
				date:	dataSegment.Scheduled_Check_In_Date__c,
				value:	dataSegment.Chest_Measurement__c
			});
			waistRow.push({
				date:	dataSegment.Scheduled_Check_In_Date__c,
				value:	dataSegment.Waist_Measurement__c
			});
			hipsRow.push({
				date:	dataSegment.Scheduled_Check_In_Date__c,
				value:	dataSegment.Hip_Measurement__c
			});
			thighRow.push({
				date:	dataSegment.Scheduled_Check_In_Date__c,
				value:	dataSegment.Thigh_Measurement__c
			});
			caloriesRow.push({
				date:	dataSegment.Scheduled_Check_In_Date__c,
				value:	dataSegment.Calories_Consumed_average__c
			});
		}

		return {
			dates,
			weightRow,
			chestRow,
			waistRow,
			hipsRow,
			thighRow,
			caloriesRow
		};


	},
	setupDates: function(component, allDates) {
	
		//Set all the dates, and set the start and end dates
		component.set("v.allDates",		allDates);
		component.set("v.startDate",	allDates[0]);
		if(allDates.length > 1) {
			component.set("v.endDate",		allDates[allDates.length - 1]);
		} else {
			component.set("v.endDate",		null);
		}

		if(allDates.length > 2) {
			component.set("v.firstDate",	allDates[1]);
		} else {
			component.set("v.firstDate",	null);
		}
		if(allDates.length > 3) {
			component.set("v.secondDate",	allDates[allDates.length - 2]);
			this.updateFirstOptions(component);
			this.updateSecondOptions(component);
		} else {
			component.set("v.secondDate",	null);
		}

	},
	updateFirstOptions: function(component) {

		let allDates = component.get("v.allDates");
		let firstDateOptions	= this.getAvailableDates(allDates, component.get("v.startDate"), component.get("v.secondDate"));
		let fDOp = [];
		for(let i = 0; i < firstDateOptions.length; i++) {
			fDOp.push({ label: firstDateOptions[i], value: firstDateOptions[i] });
		}
		component.set("v.firstDateOps", fDOp);

	},
	updateSecondOptions: function(component) {

		let allDates = component.get("v.allDates");
		let secondDateOptions	= this.getAvailableDates(allDates, component.get("v.firstDate"), component.get("v.endDate"));
		let sDOp = [];
		for(let i = 0; i < secondDateOptions.length; i++) {
			sDOp.push({ label: secondDateOptions[i], value: secondDateOptions[i] });
		}
		component.set("v.secondDateOps", sDOp);

	},
	getAvailableDates: function(allDates, startDate, endDate) {

		//Find the index of the start date
		let sIndex = -1;
		for(let i = 0; i < allDates.length; i++) {
			if(allDates[i] === startDate) {
				sIndex = i;
				break;
			}
		}

		//Find the index of the end date
		let eIndex = -1;
		for(let i = 0; i < allDates.length; i++) {
			if(allDates[i] === endDate) {
				eIndex = i;
				break;
			}
		}

		let availDates = [];
		for(let i = sIndex+1; i <= eIndex-1; i++) {
			availDates.push(allDates[i]);
		}
		return availDates;

	},
	updateVisualData: function(component) {

		let progressData = component.get("v.progressData");
		
		//Get the important dates
		let startDate	= component.get("v.startDate");
		let endDate		= component.get("v.endDate");
		let firstDate	= component.get("v.firstDate");
		let secondDate	= component.get("v.secondDate");

		let validDates = [];
		validDates.push(startDate);
		if(firstDate) {
			validDates.push(firstDate);
		}
		if(secondDate) {
			validDates.push(secondDate);
		}
		validDates.push(endDate);

		//Setup the data that will be visible to the user
		let visWeight		= [];
		let visChest		= [];
		let visWaist		= [];
		let visHips			= [];
		let visThigh		= [];
		let visCalories		= [];

		for(let i = 0; i < progressData.dates.length; i++) {
			if(validDates.includes(progressData.dates[i])) {
				visWeight.push(progressData.weightRow[i]);
				visChest.push(progressData.chestRow[i]);
				visWaist.push(progressData.waistRow[i]);
				visHips.push(progressData.hipsRow[i]);
				visThigh.push(progressData.thighRow[i]);
				visCalories.push(progressData.caloriesRow[i]);
			}
		}

		let visibleData = {
			visWeight, visChest, visWaist, visHips, visThigh, visCalories
		}

		console.log("+----visProgressData-------------------------------------------------------+");
		console.log(visibleData);
		console.log("+----visProgressData-------------------------------------------------------+");

		component.set("v.visProgressData", visibleData);

	}
})