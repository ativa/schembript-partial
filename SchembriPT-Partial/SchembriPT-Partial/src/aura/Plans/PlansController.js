({
	init: function(component, event, helper) {
        console.log('init');
        helper.init(component, helper);
	},
    pageReferenceChanged: function(component, event, helper) {
        console.log('page ref changed');
        helper.init(component, helper);
    }
})