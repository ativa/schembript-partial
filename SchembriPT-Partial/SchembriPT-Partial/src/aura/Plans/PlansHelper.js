({
	init: function(component, helper) {
        console.log('Init');
        
        const pageReference = component.get('v.pageReference');
        
        const checkInID = pageReference.state.c__checkInID;
        
        console.log('Opening plans for ' + checkInID);
        
        component.set('v.checkInID', checkInID);
        
        console.log('ID', checkInID);
    }
})