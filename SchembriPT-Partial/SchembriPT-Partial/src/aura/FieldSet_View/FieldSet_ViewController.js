({
	init: function(component, event, helper) {
        const columnCount = component.get('v.columnCount');
        const title       = component.get('v.title');
        const columns = Number(columnCount);
        component.set('v.columns', columns);

		let overrideRecId = component.get('v.oRecordId');
		let standardRecId = component.get('v.recordId');

		let recordId = (overrideRecId) ? overrideRecId : standardRecId;
		component.set("v.activeRecordId", recordId);

		console.log("Record Id", recordId);

        const getData = component.get('c.getData');
        getData.setParams({
            recordID:     recordId,
            fieldSetName: component.get('v.fieldSetName')
        });
        getData.setCallback(this, function(response) {
            const state = response.getState();
            if (state === 'SUCCESS') {
                const data = response.getReturnValue();
                
                if (!title) component.set('v.title', data.title);
                
                let fields = data.fields;

				console.log("Fields", JSON.parse(JSON.stringify(fields)));
               
                component.set('v.fields', fields);
                component.set('v.objectName', data.objectName);
            } else {
                console.log(response.getError());
            }
        });
        
        $A.enqueueAction(getData);
	},
	onFormSubmit: function(component, event, helper) {
		component.set("v.isSaving", true);
	},
	onFormSuccess: function(component, event, helper) {
		component.set("v.isSaving", false);
	},
	onFormError: function(component, event, helper) {
		component.set("v.isSaving", false);
	}
})