({
	onInit: function (component, event, helper) {
		console.log("Initialised Form");

		helper.getInitialValues(component, (success) => {
			console.log(success);

			component.set("v.checkInId",	success.parentRecords.checkInId);
			component.set("v.caseId",		success.parentRecords.caseId);
			component.set("v.accountId",	success.parentRecords.accountId);
			component.set("v.onSuccessURL", success.portalSetting.portalURL);

			if(success.photoSet.front) {
				component.set("v.frontPhotoId", success.photoSet.front.id);
			}
			if(success.photoSet.side) {
				component.set("v.sidePhotoId",	success.photoSet.side.id);
			}
			if(success.photoSet.back) {
				component.set("v.backPhotoId",	success.photoSet.back.id);
			}

		}, (error) => {
			console.error(error);
		})


	},
	onFrontPictureUpload: function (component, event, helper) {
		var uploadedFiles = event.getParam("files");
		component.set("v.frontPhotoId", uploadedFiles[0].documentId);
		console.log(uploadedFiles[0].documentId);
		helper.updateFileName(component, uploadedFiles[0].documentId, 'front');
	},
	onFrontPictureDelete: function (component, event, helper) {
		helper.deleteFile(component, 'front');
	},
	onSidePictureUpload: function (component, event, helper) {
		var uploadedFiles = event.getParam("files");
		component.set("v.sidePhotoId", uploadedFiles[0].documentId);
		console.log(uploadedFiles[0].documentId);
		helper.updateFileName(component, uploadedFiles[0].documentId, 'side');
	},
	onSidePictureDelete: function (component, event, helper) {
		helper.deleteFile(component, 'side');
	},
	onBackPictureUpload: function (component, event, helper) {
		var uploadedFiles = event.getParam("files");
		component.set("v.backPhotoId", uploadedFiles[0].documentId);
		console.log(uploadedFiles[0].documentId);
		helper.updateFileName(component, uploadedFiles[0].documentId, 'back');
	},
	onBackPictureDelete: function (component, event, helper) {
		helper.deleteFile(component, 'back');
	},
	onPhotoRefreshDone: function (component, event, helper) {
		document.location.href = component.get('v.onSuccessURL');
	}
})