({
    init: function(component, event, helper) {
        const clientID = component.get('v.clientID');

        const getClientInfo = component.get('c.getClientInfo');

        getClientInfo.setParams({clientID: clientID});
        getClientInfo.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                const info=response.getReturnValue();


                console.log('Info:', info);

                component.set('v.checkInID', info.checkInID);
                component.set('v.checkInName', info.checkInName);
                component.set('v.checkInNumber', info.checkInNumber);
                component.set('v.clientName', info.clientName);

            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getClientInfo);
    }
})