({
	onInit: function (component, event, helper) {
		
		let recordId = component.get("v.recordId");

		/*
		*/

		var checkValidityAction = component.get("c.CheckValidity");
        checkValidityAction.setParams({ 
			checkInId : recordId 
		});
		checkValidityAction.setCallback(this, (response) => {
			var state = response.getState();
			if (state === "SUCCESS") {
				
				let validation = response.getReturnValue();

				console.log("SUCCESS: ", validation);


				component.set("v.nutritionPlanStartDateSet", validation.nutritionPlanStartDateSet);
				
				if(component.get("v.nutritionPlanStartDateSet") === true) {
					window.open('/lightning/cmp/c__Plans?c__checkInID=' + recordId,'_blank');
				}

			} else if (state === "INCOMPLETE") {
				// do something
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.error("Error message: " + errors[0].message);
					}
				} else {
					console.error("Unknown error");
				}
			}
        });
        $A.enqueueAction(checkValidityAction);


	}
})