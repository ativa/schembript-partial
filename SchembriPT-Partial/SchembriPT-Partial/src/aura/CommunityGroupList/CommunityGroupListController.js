({
	onInit: function (component, event, helper) {
		console.log("Init Chatter Group List");

		helper.queryChatterPage(component, (success) => {
			console.log(success);
			component.set("v.groupData", helper.formatGroupData(success.collabGroups));
		}, (error) => {
			console.error(error);
		});

	}
})