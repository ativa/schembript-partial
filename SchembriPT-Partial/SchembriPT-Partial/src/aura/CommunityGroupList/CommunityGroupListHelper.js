({
	queryChatterPage: function (component, onSuccess, onFail) {
		
		var groupPage = component.get("c.getGroupPage");
		groupPage.setParams({
			pageIndex:		component.get("v.pageIndex"),
			pageSize:		component.get("v.pageSize")
		});
		groupPage.setCallback(this, function(response) {
			var state = response.getState();
			if(state === 'SUCCESS') {
				onSuccess(response.getReturnValue());
			} else if(state === "ERROR") {
				onFail(response.getError());
			}
		});
		$A.enqueueAction(groupPage);
	},
	formatGroupData: function(groupData) {

		let fmtData = [];

		for(let i = 0; i < groupData.length; i++) {
			let newRow = {
				Id:						groupData[i].Id,
				Name:					groupData[i].Name,
				SmallPhotoUrl:			groupData[i].SmallPhotoUrl,
				LastFeedModifiedDate:	groupData[i].LastFeedModifiedDate
			};
			fmtData.push(newRow);
		}

		return fmtData;

	}
})