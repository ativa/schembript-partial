({
	onInit: function (component, event, helper) {
		
		console.log("Record Id: ",  component.get("v.recordId"));

		//Get the account and user IDs
		let userAction = component.get("c.getUserInfo");
		userAction.setCallback(this, function(response) {
		
			console.log('Response returned');

			var state = response.getState();
			if(state === 'SUCCESS') {
                const info = response.getReturnValue();

				console.log('Succesful', JSON.parse(JSON.stringify(info)));
				
				if(info.communityPortalPage != "Onboarding (new)") {
					var navService = component.find("navService");
					navService.navigate({
						type: "standard__webPage", 
						attributes: { 
							url: '/' 
						} 
					});
				}

				component.set("v.userId",		info.userID);
                component.set("v.recordId",		info.accountID);
				component.set("v.checkInId",	info.latestCheckInId);
                component.set("v.portalURL",	info.portalURL);

			} else if(state === "ERROR") {
				console.error(response.getError());
			}
		});
		
		$A.enqueueAction(userAction);

	},
	onFormDataChange: function (component, event, helper) {
		console.log(JSON.parse(JSON.stringify(component.get("v.formData"))));
	},
	onStepSubmit: function(component, event, helper) {
	
		let stepIndex = component.get("v.stepIndex");

		component.set("v.submittingForm", true);

		//If this is the first step, submit the nickname form first
		if(stepIndex === 1) {
			let nicknameForm = component.find('CommunityNickNameForm');
			nicknameForm.submit();
		//If on the Photo Section, just continue to the next step
		} else if(stepIndex === 3) {
			component.set("v.stepIndex", stepIndex + 1);
			component.set("v.submittingForm", false);
		} else {
			//Else submit the standard form
			let stepForm = component.find('Step' + stepIndex + 'Form');
			let isSubmissionSuccessful = stepForm.Submit();

			if(isSubmissionSuccessful === false) {
				component.set("v.submittingForm", false);
			}

		}
		
	},
	onStepSuccess: function(component, event, helper) {
	
		let stepIndex = component.get("v.stepIndex");
		
		if(stepIndex === 7) {
			//If we are at the end, then we have finished the form
			//Add Advisories
			let addAdvisoriesAction = component.get("c.addAdvisories");
			addAdvisoriesAction.setParams({
				accountId: component.get("v.recordId")
			})
			addAdvisoriesAction.setCallback(this, function(response) {
				console.log('Response returned');
				var state = response.getState();
				if(state === 'SUCCESS') {

					let flagSetAction = component.get("c.FlagFormAsCompleted");
					flagSetAction.setParams({
						accountId: component.get("v.recordId")
					});
					flagSetAction.setCallback(this, function(response) {
						var state = response.getState();
						if(state === 'SUCCESS') {

							//UpdateSignupValues
							let updateSignupValuesAction = component.get("c.UpdateSignupValues");

							updateSignupValuesAction.setParams({
								accountId: component.get("v.recordId")
							})
							updateSignupValuesAction.setCallback(this, function(response) {
							
								var state = response.getState();
								if(state === 'SUCCESS') {
									document.location.href = component.get('v.portalURL');
									component.set("v.submittingForm", false);
								} else if(state === "ERROR") {
									console.error(response.getError());
								}

							});
							$A.enqueueAction(updateSignupValuesAction);



						} else if(state === "ERROR") {
							console.error(response.getError());
						}
					});
					$A.enqueueAction(flagSetAction);

				} else if(state === "ERROR") {
					console.error(response.getError());
					helper.displayErrorMessage("Error Occured", "Form data received.  Completion error - no further action needed (data has been stored). Schembri PT has been alerted to your submission.");
				}
			});

			$A.enqueueAction(addAdvisoriesAction);

		} else {
			//Else continue to the next step
			component.set("v.stepIndex", stepIndex + 1);
			component.set("v.submittingForm", false);

		}

	},
	onCommunityNickNameFormError: function (component, event, helper) {
		//Focus on the input that will move to the Community NickName
		let nickNameInput = component.find("CommunityNickNameInputFocus");
		nickNameInput.focus();
		component.set("v.submittingForm", false);
	},
	onCommunityNickNameFormSuccess: function (component, event, handler) {
		console.log("Nickname successfully entered");
		let step1Form = component.find('Step1Form');
		let isSubmissionSuccessful = step1Form.Submit();
		
		if(isSubmissionSuccessful === false) {
			component.set("v.submittingForm", false);
		}


	},
	onNavigateToSchembriPTPage: function (component, event, handler) {
		window.location.href = "https://schembript.com.au/";
	}
})