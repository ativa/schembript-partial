({
	getDayLabels: function () {

		return [
			{ label: 'Mon', title: 'Monday' },
			{ label: 'Tue', title: 'Tuesday' },
			{ label: 'Wed', title: 'Wednesday' },
			{ label: 'Thu', title: 'Thursday' },
			{ label: 'Fri', title: 'Friday' },
			{ label: 'Sat', title: 'Saturday' },
			{ label: 'Sun', title: 'Sunday' },
		];

	},
	getMobileDataValueOptions: function(component) {

		let nutritionOptions = [
			{ label: 'Calories (cals)', altLabel: 'Cals',	value: 'Calories (cals)' },
			{ label: 'Protein (g)',		altLabel: 'P',		value: 'Protein (g)' },
			{ label: 'Carbs (g)',		altLabel: 'C',		value: 'Carbs (g)' },
			{ label: 'Fat (g)',			altLabel: 'F',		value: 'Fat (g)' }
		];
		return nutritionOptions;

	},
	groupDesktopPlanData: function(component) {
	
		//Get the planData
		let planData = component.get("v.planData");

		let desktopPlanData = [];
		
		let dayOptions = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
		
		for(let i = 0; i < planData.datesHeading.length; i++) {

			let dateRow = {
				dayLabel:	dayOptions[i],
				dateLabel:	planData.datesHeading[i].label,
				dateTitle:	planData.datesHeading[i].title,
				calorie:	planData.calorieRow[i],
				protein:	planData.proteinRow[i],
				carb:		planData.carbRow[i],
				fat:		planData.fatRow[i]
			};

			desktopPlanData.push(dateRow);

		}

		return desktopPlanData;


	},
	groupMobilePlanData: function(component) {

		let mobilePlanData = [];

		//Get the planData
		let planData = component.get("v.planData");

		let dayOptions = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];

		for(let i = 0; i < planData.datesHeading.length; i++) {

			let newRow = {
				dayLabel:	dayOptions[i],
				dateLabel:	planData.datesHeading[i].label,
				dateTitle:	planData.datesHeading[i].title,
				values: [
					{ target: planData.calorieRow[i].target,	actual:	planData.calorieRow[i].actual },
					{ target: planData.proteinRow[i].target,	actual:	planData.proteinRow[i].actual },
					{ target: planData.carbRow[i].target,		actual:	planData.carbRow[i].actual },
					{ target: planData.fatRow[i].target,		actual:	planData.fatRow[i].actual }
				]
			};

			mobilePlanData.push(newRow);
		}

		return mobilePlanData;

	},
	queryPlans: function(component, currentDate, accountId, onSuccess, onError) {

		let startOfWeek = null;
		let endOfWeek	= null;

		if(currentDate.getDay() === 0) { //Sunday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 5));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 1));
		} else if(currentDate.getDay() === 1) { //Monday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - (-1)));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 7));
		} else if(currentDate.getDay() === 2) { //Tuesday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 0));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 6));
		} else if(currentDate.getDay() === 3) { //Wednesday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 1));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 5));
		} else if(currentDate.getDay() === 4) { //Thursday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 2));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 4));
		} else if(currentDate.getDay() === 5) { //Friday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 3));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 3));
		} else if(currentDate.getDay() === 6) { //Saturday
			startOfWeek = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 4));
			endOfWeek	= new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() + 2));
		}

		console.log("[CommunityWeeklyPlans] Start of Week: ", this.convertDateToISO(startOfWeek));
		console.log("[CommunityWeeklyPlans] End of Week: ", this.convertDateToISO(endOfWeek));

		console.log('1');

		let queryPlans = component.get("c.queryWeeklyData");
		
		console.log('2');

		queryPlans.setParams({
			personAccId: accountId,
			startDate: this.convertDateToISO(startOfWeek),
			endDate: this.convertDateToISO(endOfWeek)
		});
		
		console.log('3');

		queryPlans.setCallback(this, function(response) {
		
			console.log("[CommunityWeeklyPlans] Plans Returned");
			console.log("[CommunityWeeklyPlans] State: " + JSON.parse(JSON.stringify(response.getState())));
			console.log("[CommunityWeeklyPlans] Plans: " , JSON.parse(JSON.stringify(response.getReturnValue())));


			const state = response.getState();
			
			if(state === 'SUCCESS') {
				onSuccess(response.getReturnValue());
			} else if(state === "ERROR") {
				onError(response.getError());
			}
			
		});
		
		console.log('4');

		$A.enqueueAction(queryPlans);
		
		console.log('5');

	},
	convertDateToISO: function(dd) {

		let year		= dd.getUTCFullYear();
		let month		= dd.getUTCMonth() + 1;
		let date		= dd.getUTCDate();

		let strYear		= year.toString();
		let strMonth	= (month < 10) ? '0' + month : month.toString();
		let strDate		= (date < 10) ? '0' + date : date.toString();

		return strYear + '-' + strMonth + '-' + strDate;

	},
	groupPlanData: function(currentDate, plans) {

		let months = { 0: 'Jan', 1: 'Feb', 2: 'Mar', 3: 'Apr', 4: 'May', 5: 'Jun', 6: 'Jul', 7: 'Aug', 8: 'Sep', 9: 'Oct', 10: 'Nov', 11: 'Dec' };

		let datesHeading	= [];
		let nutritionRow	= [];
		let calorieRow		= [];
		let proteinRow		= [];
		let carbRow			= [];
		let fatRow			= [];

		//Get the start date of the week
		let currDate = null;

		console.log("Current Day: ", currentDate);

		if(currentDate.getDay() === 0) { //Sunday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 6));
		} else if(currentDate.getDay() === 1) { //Monday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - (-0)));
		} else if(currentDate.getDay() === 2) { //Tuesday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 1));
		} else if(currentDate.getDay() === 3) { //Wednesday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 2));
		} else if(currentDate.getDay() === 4) { //Thursday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 3));
		} else if(currentDate.getDay() === 5) { //Friday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 4));
		} else if(currentDate.getDay() === 6) { //Saturday
			currDate = new Date((new Date(currentDate)).setDate((new Date(currentDate)).getDate() - 5));
		}

		//Setup the Dates, and the values
		for(let i = 0; i < 7; i++) {
			
			nutritionRow.push({
				label: 'Target (Actual)'
			});
			
			datesHeading.push({
				label: currDate.getDate() + '-' + months[currDate.getMonth()], title: months[currDate.getMonth()]
			})
			
			calorieRow.push({ target: '-', actual: '-' });
			proteinRow.push({ target: '-', actual: '-' });
			carbRow.push({ target: '-', actual: '-' });
			fatRow.push({ target: '-', actual: '-' });

			
			//Move the Date up by 1
			currDate.setDate(currDate.getDate() + 1);
		}
		
		let planIndex = 0;

		for(let i = 0; i < plans.length; i++) {
		//for(let i = 7 - plans.length; i < 7; i++) {


			//Set the Calories
			let totalCalories;
			let actualCalories;

			if(plans[planIndex].Ref_Trainer_Total_Calories__c) {
				totalCalories = plans[planIndex].Ref_Trainer_Total_Calories__c;
			}
			if(plans[planIndex].Actual_Plan_Calories__c) {
				actualCalories = plans[planIndex].Actual_Plan_Calories__c;
			}

			//Set the Protein
			let totalProtein;
			let actualProtein;
				
			if(plans[planIndex].Ref_Trainer_Total_Protein__c) {
				totalProtein = plans[planIndex].Ref_Trainer_Total_Protein__c;
			}
			if(plans[planIndex].Actual_Plan_Protein__c) {
				actualProtein = plans[planIndex].Actual_Plan_Protein__c;
			}

			//Set the Carbs
			let totalCarbs;
			let actualCarbs;
				
			if(plans[planIndex].Ref_Trainer_Total_Carbs__c) {
				totalCarbs = plans[planIndex].Ref_Trainer_Total_Carbs__c;
			}
			if(plans[planIndex].Actual_Plan_Carbs__c) {
				actualCarbs = plans[planIndex].Actual_Plan_Carbs__c;
			}

			//Set the Fats
			let totalFat;
			let actualFat;
				
			if(plans[planIndex].Ref_Trainer_Total_Fat__c) {
				totalFat = plans[planIndex].Ref_Trainer_Total_Fat__c;
			}
			if(plans[planIndex].Actual_Plan_Fat__c) {
				actualFat = plans[planIndex].Actual_Plan_Fat__c;
			}

			//Get the difference between the total and actuals for each value type, and if it's less than 5 percent
			//Then it's on target, else it's off
			let calorieThreshold = totalCalories * 0.05;
			if(Math.abs(totalCalories - actualCalories) < calorieThreshold) {
				calorieRow[i] = { 
					target:			totalCalories, 
					actual:			actualCalories,
					classValue:		"on-value"
				};
			} else {
				calorieRow[i] = { 
					target:			totalCalories, 
					actual:			actualCalories,
					classValue:		"off-value"
				};
			}

			let proteinThreshold = totalProtein * 0.05;
			if(Math.abs(totalProtein - actualProtein) < proteinThreshold) {
				proteinRow[i] = { 
					target:			totalProtein, 
					actual:			actualProtein,
					classValue:		"on-value"
				};
			} else {
				proteinRow[i] = { 
					target:			totalProtein, 
					actual:			actualProtein,
					classValue:		"off-value"
				};
			}

			let carbThreshold = totalCarbs * 0.05;
			if(Math.abs(totalCarbs - actualCarbs) < carbThreshold) {
				carbRow[i] = { 
					target:			totalCarbs, 
					actual:			actualCarbs,
					classValue:		"on-value"
				};
			} else {
				carbRow[i] = { 
					target:			totalCarbs, 
					actual:			actualCarbs,
					classValue:		"off-value"
				};
			}
				
			let fatThreshold = totalFat * 0.05;
			if(Math.abs(totalFat - actualFat) < fatThreshold) {
				fatRow[i] = { 
					target:			totalFat, 
					actual:			actualFat,
					classValue:		"on-value"
				};
			} else {
				fatRow[i] = { 
					target:			totalFat, 
					actual:			actualFat,
					classValue:		"off-value"
				};
			}

			planIndex += 1;

		}

		/*
		//Loop through each day of the week
		let planIndex = 0;
		for(let i = 0; i < 7; i++) {

			nutritionRow.push({
				label: 'Target (Actual)'
			});

			datesHeading.push({
				label: currDate.getDate() + '-' + months[currDate.getMonth()], title: months[currDate.getMonth()]
			})

			//Get the ISO Date for the string
			let isoDate = this.convertDateToISO(currDate);

			if(planIndex < plans.length) {
				console.log(plans[planIndex].Session_Date__c + '-' + isoDate);
			}

			//If we have the plan for this date, then update the row, otherwise add empty data
			if(planIndex < plans.length && plans[planIndex].Session_Date__c === isoDate) {

				//Set the Calories
				let totalCalories;
				let actualCalories;

				if(plans[planIndex].Ref_Trainer_Total_Calories__c) {
					totalCalories = parseFloat(parseFloat(plans[planIndex].Ref_Trainer_Total_Calories__c).toFixed(0));
				}
				if(plans[planIndex].Actual_Plan_Calories__c) {
					actualCalories = parseFloat(parseFloat(plans[planIndex].Actual_Plan_Calories__c).toFixed(0));
				}

				//Set the Protein
				let totalProtein;
				let actualProtein;
				
				if(plans[planIndex].Ref_Trainer_Total_Protein__c) {
					totalProtein = parseFloat(parseFloat(plans[planIndex].Ref_Trainer_Total_Protein__c).toFixed(0));
				}
				if(plans[planIndex].Actual_Plan_Protein__c) {
					actualProtein = parseFloat(parseFloat(plans[planIndex].Actual_Plan_Protein__c).toFixed(0));
				}

				//Set the Carbs
				let totalCarbs;
				let actualCarbs;
				
				if(plans[planIndex].Ref_Trainer_Total_Carbs__c) {
					totalCarbs = parseFloat(parseFloat(plans[planIndex].Ref_Trainer_Total_Carbs__c).toFixed(0));
				}
				if(plans[planIndex].Actual_Plan_Carbs__c) {
					actualCarbs = parseFloat(parseFloat(plans[planIndex].Actual_Plan_Carbs__c).toFixed(0));
				}

				//Set the Fats
				let totalFat;
				let actualFat;
				
				if(plans[planIndex].Ref_Trainer_Total_Fat__c) {
					totalFat = parseFloat(parseFloat(plans[planIndex].Ref_Trainer_Total_Fat__c).toFixed(0));
				}
				if(plans[planIndex].Actual_Plan_Fat__c) {
					actualFat = parseFloat(parseFloat(plans[planIndex].Actual_Plan_Fat__c).toFixed(0));
				}

				//Get the difference between the total and actuals for each value type, and if it's less than 5 percent
				//Then it's on target, else it's off
				let calorieThreshold = totalCalories * 0.05;
				if(Math.abs(totalCalories - actualCalories) < calorieThreshold) {
					calorieRow.push({ 
						target:			totalCalories, 
						actual:			actualCalories,
						classValue:		"on-value"
					});
				} else {
					calorieRow.push({ 
						target:			totalCalories, 
						actual:			actualCalories,
						classValue:		"off-value"
					});
				}

				let proteinThreshold = totalProtein * 0.05;
				if(Math.abs(totalProtein - actualProtein) < proteinThreshold) {
					proteinRow.push({ 
						target:			totalProtein, 
						actual:			actualProtein,
						classValue:		"on-value"
					});
				} else {
					proteinRow.push({ 
						target:			totalProtein, 
						actual:			actualProtein,
						classValue:		"off-value"
					});
				}

				let carbThreshold = totalCarbs * 0.05;
				if(Math.abs(totalCarbs - actualCarbs) < carbThreshold) {
					carbRow.push({ 
						target:			totalCarbs, 
						actual:			actualCarbs,
						classValue:		"on-value"
					});
				} else {
					carbRow.push({ 
						target:			totalCarbs, 
						actual:			actualCarbs,
						classValue:		"off-value"
					});
				}
				
				let fatThreshold = totalFat * 0.05;
				if(Math.abs(totalFat - actualFat) < fatThreshold) {
					fatRow.push({ 
						target:			totalFat, 
						actual:			actualFat,
						classValue:		"on-value"
					});
				} else {
					fatRow.push({ 
						target:			totalFat, 
						actual:			actualFat,
						classValue:		"off-value"
					});
				}

				planIndex += 1;
				
			} else {

				calorieRow.push({
					target: '-',
					actual: '-'
				});
				proteinRow.push({
					target: '-',
					actual: '-'
				});
				carbRow.push({
					target: '-',
					actual: '-'
				});
				fatRow.push({
					target: '-',
					actual: '-'
				});

			}

			currDate.setDate(currDate.getDate() + 1);
		}
		*/
		let weeklyPlanData = {
			datesHeading,
			nutritionRow,
			calorieRow,
			proteinRow,
			carbRow,
			fatRow
		};

		console.log("[CommunityWeeklyPlans] Weekly Plan Data: ", JSON.parse(JSON.stringify(weeklyPlanData)));

		return weeklyPlanData;

	}
})