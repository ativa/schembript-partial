({
	onInit: function (component, event, helper) {

		//Setup the days in the week header
		component.set('v.daysInWeek', helper.getDayLabels());
		component.set('v.mobile_valueOptions',	helper.getMobileDataValueOptions(component));

		let accountId = component.get("v.accountId");

		//If there is no account Id already loaded in, then get the account Id associated with the user
		//Otherwise force a refresh of onAccountIdUpdate
		if(accountId === null) {

			//If there is no accout Id then try to load the account Id from the user
			component.set("v.isLoading", true);
			const getAccountID = component.get("c.getAccountID");
			getAccountID.setCallback(this, function(response) {
				const state = response.getState();
				if(state === 'SUCCESS') {
					component.set('v.accountId', response.getReturnValue());
				} else if(state === "ERROR") {
					console.error(JSON.stringify(response.getError()));
				}
				component.set("v.isLoading", false);

			});
			$A.enqueueAction(getAccountID);
		} else {
			const a = component.get("c.onAccountIdUpdate");
			$A.enqueueAction(a);
		}

	},
	onAccountIdUpdate: function (component, event, helper) {

		component.set("v.isLoading", true);

		const accountId = component.get("v.accountId");
		//Get todays date
		let dte = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

		//Query the plan data for the week
		helper.queryPlans(component, dte, accountId, (success) => {
			console.log('SUCCESS');
			console.log("[CommunityWeeklyPlans] Successfully queried plans: ", success);

			//Group the Plan data for the table
			let planData = helper.groupPlanData(dte, success.plans);
			component.set("v.planData", planData);
			console.log("[CommunityWeeklyPlans] Plan Data: ", planData);
			component.set("v.desktop_planData", helper.groupDesktopPlanData(component));

			console.log("[CommunityWeeklyPlans] Desktop Plan Data: ", JSON.parse(JSON.stringify(component.get("v.desktop_planData"))));

			component.set('v.mobile_valueSelected', 'Calories (cals)');

			component.set("v.isLoading", false);

		}, (error) => {
			console.error("ERROR: ", JSON.parse(JSON.stringify(error)));

			component.set("v.isLoading", false);
		});

	},
	onMobileValueSelectedUpdate: function (component, event, helper) {
		let mobilePlanData = helper.groupMobilePlanData(component);
		component.set("v.mobile_planData", mobilePlanData);
	},
	onDataTypeUpdate: function (component, event, helper) {
		console.log("Updated Data Type");
	},
	clickNutritionButton: function (component, event, helper) {
		let dataType = component.get("v.dataType");
		console.log("Current Data Type: " + dataType);

		if(dataType === 'Nutrition') {
			component.set("v.dataType","Training");
		} else if(dataType === 'Training') {
			component.set("v.dataType","Nutrition");
		}

	}
})