({
	init: function(component, event, helper) {
        const menu = document.getElementsByClassName('selfServiceUserProfileMenu');
        
        console.log('Menus: ' + menu.length);
        
		const getClientInfo = component.get('c.getClientInfo');
        
        getClientInfo.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {

                const info=response.getReturnValue();
                console.log('Info:', info);
				component.set('v.clientID',				info.clientId);
                component.set('v.clientName',			info.clientName);
                component.set('v.clientGender',         info.clientGender);
				component.set('v.checkInID',			info.checkInID);
				component.set('v.checkInName',			info.checkInName);
				component.set('v.checkInNumber',		info.checkInNumber);
				component.set('v.isAdminProfile',		info.isAdminProfile);
				component.set('v.checkInGoals',			info.checkInGoals);
				component.set('v.programName',			info.programName);
				if(info.programStart) {
					component.set('v.programStart',		helper.formatDate(info.programStart));
				}
				if(info.programEnd) {
					component.set('v.programEnd',		helper.formatDate(info.programEnd));
				}
				component.set('v.programFrequency',		info.programFrequency);
				component.set('v.hasProgram',			(info.programID != undefined && info.programID != null));
				component.set('v.hasCheckIn',			(info.checkInID != undefined && info.checkInID != null));
				component.set('v.hasGoals',				(info.checkInGoals != undefined && info.checkInGoals != null));
				if(info.checkInDate) {
					component.set('v.checkInDate',		helper.formatDate(info.checkInDate));
                }
                
                
				var urlString = window.location.href;
				var baseURL = urlString.substring("https://".length);
				baseURL = baseURL.substring(baseURL.indexOf('/'));
				baseURL = baseURL.substring(0, baseURL.indexOf('/s'));

				component.set('v.startURL', baseURL);
				component.set("v.checkInAvailable",		info.checkInAvailable);
				component.set("v.checkInSubmitted",		info.checkInSubmitted);
				component.set("v.checkInBeforeDate",	info.checkInBeforeDate);
				component.set("v.checkInAfterDate",		info.checkInAfterDate);
				component.set("v.checkInStage",			info.checkInStage);

                helper.loadPhotos(component, helper);

            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getClientInfo);
	},
    returnHome: function(component, event, helper) {
        component.set('v.mode', 'home');
    }, 
	viewCheckIn: function(component, event, helper) {
        component.set('v.mode', 'check-in');
	},
    viewGoals: function(component, event, helper) {
        component.set('v.mode', 'goals');
    },
    viewProgress: function(component, event, helper) {
        component.set('v.mode', 'progress');
    },
    viewFeedback: function(component, event, helper) {
        component.set('v.mode', 'feedback');
    },
    viewWeek: function(component, event, helper) {
        component.set('v.mode', 'week');
    },
    messageTrainer: function(component, event, helper) {
        component.set('v.mode', 'messageTrainer');
    },
    messageGroup: function(component, event, helper) {
        component.set('v.mode', 'messageGroup');
    },
    changePhotoView: function(component, event, helper) {
        const newView = event.getParam("value");
        component.set('v.photoView', newView);
    },
	onPhotoRangeChange: function(component, event, helper) {
		helper.loadPhotos(component, helper);
	},
	postGroupMessage: function(component, event, helper) {
		var messageMyGroup = component.get("c.messageMyGroup");
        
		messageMyGroup.setParams({
            message: component.get('v.msgGroup')
        });

        messageMyGroup.setCallback(this, function(response) {
            var state = response.getState();

            if(state === 'SUCCESS') {

				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					"title":	"Message Sent",
					"message":	"Message has been posted to your group",
					"type":		"success"
				});
				toastEvent.fire();

            } else if(state === "ERROR") {
                const toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    'type': 'error',
                    "title": "Error",
                    message: JSON.stringify(response.getError())
                });
                toastEvent.fire();
                
                console.log('ERROR:');
                console.log(JSON.stringify(response.getError()));
            }
        });
        
    	$A.enqueueAction(messageMyGroup);
	},
	postTrainerMessage: function(component, event, helper) {
		var messageTrainer = component.get("c.messageTrainer");
        
		messageTrainer.setParams({
            message: component.get('v.msgTrainer')
        });

        messageTrainer.setCallback(this, function(response) {
            var state = response.getState();

            if(state === 'SUCCESS') {
                const trainerName = response.getReturnValue();
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					"title":	"Message Sent to " + trainerName,
					"message":	trainerName + " has received your message",
					"type":		"success"
				});
				toastEvent.fire();
            } else if(state === "ERROR") {
                const trainerName = response.getReturnValue();
                
                const toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    'type': 'error',
                    "title": "Error",
                    message: JSON.stringify(response.getError())
                });
                toastEvent.fire();
                
                console.log('ERROR:');
                console.log(JSON.stringify(response.getError()));
            }
        });
        
    	$A.enqueueAction(messageTrainer);
	},
	onSelectPhoto: function(component, event, helper) {

		console.log("Selected Photo");

		component.set("v.displayPopupUrl",		event.target.src);
		component.set("v.displayPopupPhoto",	true);
	},
	handlePhotoDisplayerClose: function(component, event, helper) {
		component.set("v.displayPopupPhoto", false);
	}
})