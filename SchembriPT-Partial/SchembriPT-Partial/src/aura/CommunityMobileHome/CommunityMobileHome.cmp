<!--
  @description       : 
  @author            : ChangeMeIn@UserSettingsUnder.SFDoc
  @group             : 
  @last modified on  : 12-29-2020
  @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
  Modifications Log 
  Ver   Date         Author                               Modification
  1.0   12-29-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
-->
<aura:component implements="forceCommunity:availableForAllPageTypes" access="global" controller="CommunityHome">
    
	<aura:handler name="init" value="{!this}" action="{!c.init}"/>
    <aura:handler name="change" value="{!v.photoRange}" action="{!c.onPhotoRangeChange}"/>

	<aura:attribute type="String" name="startURL"/>

	<!--Account Information-->
	<aura:attribute type="String"	name="clientID" description="The Id of the client"/>
    <aura:attribute type="String"	name="clientName" description="The Name of the client"/>
    <aura:attribute type="String"   name="clientGender" description="The Gender of the client"/>
	<aura:attribute type="Boolean"	name="isAdminProfile"/>

	<!--Check-In(Check_In__c) Information-->
	<aura:attribute type="String"	name="checkInID" description="The Id of the current Check-In"/>
    <aura:attribute type="String"	name="checkInName"/>
    <aura:attribute type="String"	name="checkInNumber"/>
    <aura:attribute type="String"	name="checkInDate"/>
    <aura:attribute type="String"	name="checkInGoals"/>
    <aura:attribute type="Boolean"	name="hasGoals"/>
    <aura:attribute type="String"	name="checkInDay"/>
    <aura:attribute type="String"	name="checkInAvailableDate"/>
	<aura:attribute type="Boolean"	name="hasCheckIn"/>
    <aura:attribute type="String"	name="checkInAvailable"	default="Too Soon" description="Tracks if the user can do a checkin now, It's either ('Yes', 'Too Soon', 'Too Late')"/>
	<aura:attribute type="Boolean"	name="checkInSubmitted"	default="false" description="Tracks if the User has submitted the latest Check-In"/>
	<aura:attribute type="String"	name="checkInBeforeDate" />
	<aura:attribute type="String"	name="checkInAfterDate" />
	<aura:attribute type="String"	name="checkInStage"/>

	<!--Program(Case) Information-->
	<aura:attribute type="String"	name="programName"/>
	<aura:attribute type="String"	name="programStart"/>
	<aura:attribute type="String"	name="programEnd"/>
	<aura:attribute type="String"	name="programFrequency"/>
	<aura:attribute type="Boolean"	name="hasProgram"/>

    
	<aura:attribute type="String" name="msgGroup"	description="The Message we will send to the Group"/>
	<aura:attribute type="String" name="msgTrainer" description="The Message we will send to the Trainer"/>
    
    <aura:attribute type="Object" name="photos"/>

    <aura:attribute type="Object" name="previousPhotos"/>
    <aura:attribute type="Object" name="latestPhotos"/>

    <aura:attribute type="Integer" name="photoCount" default="0"/>
    
    <aura:attribute type="String" name="photoView" default="Front"/>
    <aura:attribute name="photoViews" type="List" default="[
                                                          {'label': 'Front', 'value': 'Front'},
                                                          {'label': 'Side', 'value': 'Side'},
                                                          {'label': 'Back', 'value': 'Back'}
                                                          ]"/>
    
    <aura:attribute type="Boolean" name="showGoals" default="true"/>
    
    <aura:attribute type="Boolean" name="showPhotos" default="true"/>
    <aura:attribute type="List" name="photoSelections" default="[
                                                                {'label': 'Previous Check-In',	'value': 'PreviousCheck-In'},
                                                                {'label': '3 Months',			'value': '3Months'},
                                                                {'label': '6 Months',			'value': '6Months'},
                                                                {'label': '1 Year',				'value': '1Year'},
                                                                {'label': '2 Year',				'value': '2Year'},
                                                                {'label': 'All Time',			'value': 'AllTime'},
                                                                {'label': 'Program Start',		'value': 'CurrentProgram'}
                                                        ]"/>
	<aura:attribute type="String" name="photoRange" default="AllTime"/>
    
    <aura:attribute type="String" name="mode" default="home"/>

	<aura:attribute type="String" name="displayPopupUrl" description="The photo to display in the popup"/>
	<aura:attribute type="Boolean" name="displayPopupPhoto" default="false" description="Tracks if the Popup photo is displayed"/>

    
    <aura:if isTrue="{!$Browser.isPhone}">
        <div>
        <aura:if isTrue="{!v.mode == 'home'}">
        <div style="text-align: center;">
            <table>
                <tbody>
                    <tr>
                    	<td colspan = "2">
							<!-- Stage__c == 'Pending' -->
							<aura:if isTrue="{!v.checkInStage == 'Pending'}">
								<div class="checkin-box">
									<div class="primary-text">Check-In Opens</div>
									<div class="secondary-text">{!v.checkInDate}</div>
								</div>
							</aura:if>
							<!-- Stage__c == 'Client Information' -->
							<aura:if isTrue="{!v.checkInStage == 'Client Information'}">
								<div class="checkin-box">
									<div class="primary-text">Check-In Due</div>
									<div class="secondary-text">{!v.checkInDate}</div>
								</div>
							</aura:if>
							<!-- Stage__c == 'VA Review' OR  Stage__c == 'Trainer Review' OR  Stage__c == 'Super-Coach Review' OR Stage__c == 'Completed' -->
							<aura:if isTrue="{!v.checkInStage == 'VA Review' || v.checkInStage == 'Trainer Review' || v.checkInStage == 'Super-Coach Review' || v.checkInStage == 'Completed'}">
								<div class="checkin-box">
									<div class="primary-text">Check-In Closed</div>
								</div>
							</aura:if>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="{!if(v.checkInStage == 'Client Information', 'tile', 'tile noClick')}" onclick="{!c.viewCheckIn}" disabled="{!v.checkInStage != 'Client Information'}">
                                <lightning:icon iconName="utility:record_update" variant="inverse" size="medium"/>
                                <br/>
                                Check-In
                            </button>
                        </td>
                        
                        <td>
                            <button class="tile" onclick="{!c.viewGoals}">
                                <lightning:icon iconName="utility:campaign" variant="inverse" size="medium"/>
                                <br/>
                                Goals
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="tile" onclick="{!c.viewProgress}">
                                <lightning:icon iconName="utility:graph" variant="inverse" size="medium"/>
                                <br/>
                                Progress
                            </button>
                        </td>
                        
                        <td>
                            <button class="tile" onclick="{!c.viewFeedback}">
                                <lightning:icon iconName="utility:answer" variant="inverse" size="medium"/>
                                <br/>
                                Feedback
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="tile" onclick="{!c.viewWeek}">
                                <lightning:icon iconName="utility:event" variant="inverse" size="medium"/>
                                <br/>
                                My Week
                            </button>
                        </td>
                        <td>
                            <aura:if isTrue="{!v.startURL == '/SelfService'}">
                                <a href="{!v.startURL + '/s/self-serve-tracker'}">
                                    <lightning:icon iconName="utility:note" variant="inverse" size="medium"/>
                                    <br/>
                                    Tracker
                                </a>
                            </aura:if>
                            <aura:if isTrue="{!v.startURL == '/Members'}">
                                <a href="{!v.startURL + '/s/member-tracker'}">
                                    <lightning:icon iconName="utility:note" variant="inverse" size="medium"/>
                                    <br/>
                                    Tracker
                                </a>
                            </aura:if>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="community">
                <thead>
                	<tr>
                    	<th>
                            <hr/>
                        	Schembri PT Community
                        </th>
                    </tr>
                </thead>
            	<tbody>
                	<tr>
                        <td>                            
                            <div class="slds-grid">
                                <div class="slds-col slds-size_1-of-3">
                                    
                                    <a class="topics" href="{!v.startURL + '/s/topic/0TO2w000000XjabGAC/training'}">Training</a>
                                </div>
                                <div class="slds-col slds-size_1-of-3">
                                    <a class="topics" href="{!v.startURL + '/s/topic/0TO2w000000Xj2IGAS/nutrition'}">Nutrition</a>
                                </div>
                                <div class="slds-col slds-size_1-of-3">
                                    <a class="topics" href="{!v.startURL + '/s/topic/0TO2w000000XjagGAC/lifestyle'}">Lifestyle</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="slds-grid">
                                <div class="slds-col slds-size_1-of-2">
                                    <a class="messaging" href="{!v.startURL + '/s/message-trainer'}">
                                        <lightning:icon iconName="utility:chat" size="small"/>
                                        Message Trainer
                                    </a>
                                </div>
                                <div class="slds-col slds-size_1-of-2">
                                    <a class="messaging" href="{!v.startURL + '/s/message-my-group'}">
                                        <lightning:icon iconName="utility:comments" size="small"/>
                                        Message My Group
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
					<tr>
						<td>
							<a class="topics" href="{!v.startURL + '/s/topiccatalog'}">FAQs</a>
						</td>
					</tr>
                </tbody>
            </table>
        </div>
        </aura:if>
		<aura:if isTrue="{!v.mode == 'check-in'}">
			<c:CommunityCheckIn/>
		</aura:if>
        <aura:if isTrue="{!v.mode == 'goals'}">
            <lightning:button label="Return to Home" variant="neutral" iconName="utility:home" onclick="{!c.returnHome}"/>
            <br/><br/>
            <h2>Goals</h2>
            <br/>
            <p>
                {!v.checkInGoals}
            </p>
        </aura:if>
        <aura:if isTrue="{!v.mode == 'progress'}">
            <lightning:button label="Return to Home" variant="neutral" iconName="utility:home" onclick="{!c.returnHome}"/>
            <br/><br/>
            <h2>Progress</h2>
            <br/>
            <table class="photos">	
				<c:communityPhotoDisplayer photoUrl="{!v.displayPopupUrl}" showImg="{!v.displayPopupPhoto}" onclose="{!c.handlePhotoDisplayerClose}"></c:communityPhotoDisplayer>

                <tbody>
                    <tr>
                        <td style="width: 50%">
                            <lightning:combobox label="" variant="label-hidden" value="{!v.photoRange}" options="{!v.photoSelections}"/>
                        </td>
                        <td style="width: 50%; text-align: center;">
                            <b>Latest Check-In</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; text-align: center;">
                            <aura:if isTrue="{!v.photoView == 'Front'}">
                                <aura:if isTrue="{!v.previousPhotos.front != null}">
									<img src="{!v.previousPhotos.front.DropboxPath}" ondblclick="{!c.onSelectPhoto}" style="max-height: 350px;"/>
									<aura:set attribute="else">
                                        <aura:if isTrue="{!v.clientGender == 'Male'}">
                                            <td><img src="https://www.dropbox.com/s/2vo9m3wvrpxnxm1/Male_Front.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
                                        <aura:if isTrue="{!v.clientGender == 'Female'}">
                                            <td><img src="https://www.dropbox.com/s/icgbz6xiyedec8d/Female_Front.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
									</aura:set>
								</aura:if>
                            </aura:if>
                            <aura:if isTrue="{!v.photoView == 'Side'}">
                                <aura:if isTrue="{!v.previousPhotos.side != null}">
									<img src="{!v.previousPhotos.side.DropboxPath}" ondblclick="{!c.onSelectPhoto}" style="max-height: 350px;"/>
									<aura:set attribute="else">
                                        <aura:if isTrue="{!v.clientGender == 'Male'}">
                                            <td><img src="https://www.dropbox.com/s/axbx3my9avml1d4/Male_Side.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
                                        <aura:if isTrue="{!v.clientGender == 'Female'}">
                                            <td><img src="https://www.dropbox.com/s/os1sir4u7kiiote/Female_Side.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
									</aura:set>
								</aura:if>
                            </aura:if>
                            <aura:if isTrue="{!v.photoView == 'Back'}">
                                <aura:if isTrue="{!v.previousPhotos.back != null}">
									<img src="{!v.previousPhotos.back.DropboxPath}" ondblclick="{!c.onSelectPhoto}" style="max-height: 350px;"/>
									<aura:set attribute="else">
                                        <aura:if isTrue="{!v.clientGender == 'Male'}">
                                            <td><img src="https://www.dropbox.com/s/zclxmcmnzfqmgfi/Male_Back.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
                                        <aura:if isTrue="{!v.clientGender == 'Female'}">
                                            <td><img src="https://www.dropbox.com/s/akfakp0dbtwwfyv/Female_Back.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
									</aura:set>
								</aura:if>
                            </aura:if>
                        </td>          
                        <td style="border-left: 1px solid #3a3a3a; width: 50%; text-align: center;">
                            <aura:if isTrue="{!v.photoView == 'Front'}">
								<aura:if isTrue="{!v.latestPhotos.front != null}">
									<img src="{!v.latestPhotos.front.DropboxPath}" ondblclick="{!c.onSelectPhoto}" style="max-height: 350px;"/>
									<aura:set attribute="else">
										<div class="slds-text-align_center slds-text-color_weak">
                                            <aura:if isTrue="{!v.clientGender == 'Male'}">
                                                <td><img src="https://www.dropbox.com/s/2vo9m3wvrpxnxm1/Male_Front.jpg?raw=1" style="max-height: 350px;"/></td>
                                            </aura:if>
                                            <aura:if isTrue="{!v.clientGender == 'Female'}">
                                                <td><img src="https://www.dropbox.com/s/icgbz6xiyedec8d/Female_Front.jpg?raw=1" style="max-height: 350px;"/></td>
                                            </aura:if>
                                        </div>
									</aura:set>
								</aura:if>
                            </aura:if>
                            <aura:if isTrue="{!v.photoView == 'Side'}">
								<aura:if isTrue="{!v.latestPhotos.side != null}">
									<img src="{!v.latestPhotos.side.DropboxPath}" ondblclick="{!c.onSelectPhoto}" style="max-height: 350px;"/>
									<aura:set attribute="else">
										<div class="slds-text-align_center slds-text-color_weak">
                                            <aura:if isTrue="{!v.clientGender == 'Male'}">
                                                <td><img src="https://www.dropbox.com/s/axbx3my9avml1d4/Male_Side.jpg?raw=1" style="max-height: 350px;"/></td>
                                            </aura:if>
                                            <aura:if isTrue="{!v.clientGender == 'Female'}">
                                                <td><img src="https://www.dropbox.com/s/os1sir4u7kiiote/Female_Side.jpg?raw=1" style="max-height: 350px;"/></td>
                                            </aura:if>
                                        </div>
									</aura:set>
								</aura:if>
                            </aura:if>
                            <aura:if isTrue="{!v.photoView == 'Back'}">

								<aura:if isTrue="{!v.latestPhotos.back != null}">
									<img src="{!v.latestPhotos.back.DropboxPath}" ondblclick="{!c.onSelectPhoto}" style="max-height: 350px;"/>
									<aura:set attribute="else">
                                        <aura:if isTrue="{!v.clientGender == 'Male'}">
                                            <td><img src="https://www.dropbox.com/s/zclxmcmnzfqmgfi/Male_Back.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
                                        <aura:if isTrue="{!v.clientGender == 'Female'}">
                                            <td><img src="https://www.dropbox.com/s/akfakp0dbtwwfyv/Female_Back.jpg?raw=1" style="max-height: 350px;"/></td>
                                        </aura:if>
									</aura:set>
								</aura:if>

                            </aura:if>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <lightning:radioGroup aura:id="photoView"
                                                  options="{! v.photoViews }"
                                                  value="{! v.photoView }"
                                                  type="button"
                                                  onclick="{!c.changePhotoView}"/>
                        </td>
                    </tr>
                </tbody>
            </table>
			<!--The Client Progress Displays (e.g. Weight, Calories, Chest etc.)-->
            <c:CommunityClientProgress periodValue="{!v.photoRange}" />
        </aura:if>
        <aura:if isTrue="{!v.mode == 'feedback'}">
            <lightning:button label="Return to Home" variant="neutral" iconName="utility:home" onclick="{!c.returnHome}"/>
            <br/><br/>
            <h2>
                Feedback
            	&nbsp;
                &nbsp;
                <forceChatter:publisher context="GLOBAL" />
            </h2>
            <forceChatter:feed type="UserProfile"/>
        </aura:if>
        <aura:if isTrue="{!v.mode == 'week'}">
            <lightning:button label="Return to Home" variant="neutral" iconName="utility:home" onclick="{!c.returnHome}"/>
            <br/><br/>
            <c:CommunityWeeklyPlans/>
        </aura:if>
        <aura:if isTrue="{!v.mode == 'messageTrainer'}">
            <lightning:button label="Return to Home" variant="neutral" iconName="utility:home" onclick="{!c.returnHome}"/>
            <br/><br/>
            <h2>
                Message Trainer
            </h2>
            <br/><br/>
            <div>
				<lightning:textarea placeholder="Enter message to trainer..." value="{!v.msgTrainer}"/>
				<lightning:button label="Post Message" onclick="{!c.postTrainerMessage}"/>
				<lightning:button label="Return Home" onclick="{!c.returnHome}"/>
			</div>
        </aura:if>
        <aura:if isTrue="{!v.mode == 'messageGroup'}">
            <lightning:button label="Return to Home" variant="neutral" iconName="utility:home" onclick="{!c.returnHome}"/>
            <br/><br/>
			<h2>
                Message Group
            </h2>
			<div>
				<lightning:textarea placeholder="Enter message to group..." value="{!v.msgGroup}"/>
				<lightning:button label="Post Message"	onclick="{!c.postGroupMessage}"/>
				<lightning:button label="Return Home"	onclick="{!c.returnHome}"/>
			</div>
        </aura:if>
            </div>
    </aura:if>
</aura:component>