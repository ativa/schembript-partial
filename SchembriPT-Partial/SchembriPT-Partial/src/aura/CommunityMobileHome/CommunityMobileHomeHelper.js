({
	formatDate: function(date) {
        const d = new Date(date);
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        
        return da + ' ' + mo + ' ' + ye;
    },
    formatShortDate: function(date) {
        const d = new Date(date);
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        
        return da + ' ' + mo;
    },
    loadPhotos: function(component, helper) {
        //const clientID = component.get('v.clientID');

        const clientID	= component.get('v.clientID');
		const checkInID = component.get('v.checkInId');
		const range		= component.get('v.photoRange');

		const photoInfoSent = {
			clientID:	clientID,
			checkInID:	checkInID,
			range:		range
		};

		console.log('[CommunityMobileHome] Photo Info: ', JSON.parse(JSON.stringify(photoInfoSent)));
		
        const getPhotos = component.get('c.getPhotos');
        getPhotos.setParams(photoInfoSent);
        getPhotos.setCallback(this, function(response) {
            const state = response.getState();

			console.log('Response Returned');

            if (state == 'SUCCESS') {
                const photos = response.getReturnValue();
                
                console.log('Photos', response.getReturnValue());
                //component.set('v.photos', photos);
                
                if (photos) {

					//Set the latest photoset
                    let latestPhotoset = {};
					for(let i = 0; i < photos.latest.length; i++) {
						let p = photos.latest[i];
						if(p.name === "Front") {
							latestPhotoset.front = p;
						}
						if(p.name === "Side") {
							latestPhotoset.side = p;
						}
						if(p.name === "Back") {
							latestPhotoset.back = p;
						}
					}
                    component.set('v.latestPhotos', latestPhotoset);
					
					//Set the previous photoset
                    let previousPhotoset = {};
					for(let i = 0; i < photos.previous.length; i++) {
						let p = photos.previous[i];
						if(p.name === "Front") {
							previousPhotoset.front = p;
						}
						if(p.name === "Side") {
							previousPhotoset.side = p;
						}
						if(p.name === "Back") {
							previousPhotoset.back = p;
						}
					}
                    component.set('v.previousPhotos', previousPhotoset);
                }

				console.log('LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL', JSON.parse(JSON.stringify(component.get('v.latestPhotos'))));
				console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP', JSON.parse(JSON.stringify(component.get('v.previousPhotos'))));
                
                //component.set('v.photoCount', (photos && photos.length > 0) ? photos.length : 0);
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getPhotos);
    }
})