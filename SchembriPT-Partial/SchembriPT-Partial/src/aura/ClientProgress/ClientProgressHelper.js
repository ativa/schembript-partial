({
	init: function(component, helper) {

		

		const checkInId = component.get("v.recordId");

		const getClientId = component.get("c.GetClientIdFromCheckIn");
		getClientId.setParams({checkInId: checkInId});
		getClientId.setCallback(this, function(response) {

			const state = response.getState();

			if(state === 'SUCCESS') {
                const clientId = response.getReturnValue();
				component.set('v.clientID', clientId);

				helper.loadInfo(component, helper);
				helper.loadPhotos(component, helper);

			} else { 
                console.error("Error", JSON.stringify(response.getError()));
			}

		});
        $A.enqueueAction(getClientId);

	},
    loadInfo: function(component, helper) {
        const clientID	= component.get('v.clientID');
        const getClientInfo = component.get('c.getClientInfo');
        
        getClientInfo.setParams({clientID: clientID});
        getClientInfo.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                const info=response.getReturnValue();
                console.log('Info:', info);
                
                console.log('Checkin', info.checkInID);
                
                component.set('v.imageID', info.profilePhotoID);
                //component.set('v.goals', info.goals.join(', '));
                component.set('v.checkInName', info.checkInName);
                component.set('v.checkInNumber', info.checkInNumber);
                component.set('v.clientName', info.clientName);
                component.set('v.checkInGoals', info.checkInGoals);
                component.set('v.programName', info.programName);
                component.set('v.programStart', helper.formatDate(info.programStart));
                component.set('v.programEnd', helper.formatDate(info.programEnd));
                component.set('v.programFrequency', info.programFrequency);
                
                component.set('v.checkInDate', helper.formatDate(info.checkInDate));
                component.set('v.checkInAvailableDate', helper.formatShortDate(info.checkInAvailableDate));
				component.set('v.checkInAvailable', info.checkInAvailable);
                
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getClientInfo);
    },
    formatDate: function(date) {
        const d = new Date(date);
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        
        return da + ' ' + mo + ' ' + ye;
    },
    formatShortDate: function(date) {
        const d = new Date(date);
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        
        return da + ' ' + mo;
    },
    loadPhotos: function(component, helper) {

        const clientID	= component.get('v.clientID');
		const checkInID = component.get('v.recordId');
		const range		= component.get('v.periodValue');
       
        const getPhotos = component.get('c.getPhotos');

		console.log('Client Id', clientID)
		console.log('Checkin Id', checkInID)
		console.log('Range Id', range)

        getPhotos.setParams({
			clientID:	clientID,
			checkInID:	checkInID,
			range:		range
		});
        getPhotos.setCallback(this, function(response) {
            const state = response.getState();

            if (state == 'SUCCESS') {
                const photos = response.getReturnValue();
                
                console.log('Photos', response.getReturnValue());
				
				let photoCount = 0;

				
                if (photos) {

					//Set the latest photoset
                    let latestPhotoset = {};
					for(let i = 0; i < photos.latest.length; i++) {
						let p = photos.latest[i];
						if(p.name === "Front") {
							latestPhotoset.front = p;
							photoCount += 1;
						}
						if(p.name === "Side") {
							latestPhotoset.side = p;
							photoCount += 1;
						}
						if(p.name === "Back") {
							latestPhotoset.back = p;
							photoCount += 1;
						}
					}
                    component.set('v.latestPhotos', latestPhotoset);
					
					//Set the previous photoset
                    let previousPhotoset = {};
					for(let i = 0; i < photos.previous.length; i++) {
						let p = photos.previous[i];
						if(p.name === "Front") {
							previousPhotoset.front = p;
							photoCount += 1;
						}
						if(p.name === "Side") {
							previousPhotoset.side = p;
							photoCount += 1;
						}
						if(p.name === "Back") {
							previousPhotoset.back = p;
							photoCount += 1;
						}
					}
                    component.set('v.previousPhotos', previousPhotoset);
                    component.set('v.photoCount', photoCount);
                }

                console.log('Latest photos', JSON.parse(JSON.stringify(component.get('v.latestPhotos'))));
                console.log('Previous photos', JSON.parse(JSON.stringify(component.get('v.previousPhotos'))));
                
                component.set('v.photoCount', (photos && photos.length > 0) ? photos.length : 0);
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getPhotos);
    }
})