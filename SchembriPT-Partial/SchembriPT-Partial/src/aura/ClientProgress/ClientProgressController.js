({
	init: function(component, event, helper) {
        helper.init(component, helper);
	},
    pageReferenceChanged: function(component, event, helper) {
        helper.init(component, helper);
    },
    scriptsLoaded: function(component, event, helper) {
        console.log('Scripts loaded');
        helper.init(component, helper);
	},
    togglePhotos: function(component, event, helper) {
        component.set('v.showPhotos', !component.get('v.showPhotos'));
	},
    toggleGoals: function(component, event, helper) {
        component.set('v.showGoals', !component.get('v.showGoals'));
	},
	onPeriodValueChange: function(component, event, helper) { 
	
		helper.loadPhotos(component, helper);
	},
    changePhotoView: function(component, event, helper) {
        //const newView = event.getParam("value");
        
        //component.set('v.photoView', newView);

    }
})