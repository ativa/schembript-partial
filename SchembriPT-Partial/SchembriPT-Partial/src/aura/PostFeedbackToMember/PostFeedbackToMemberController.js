({
	init: function(component, event, helper) {
		const getCheckInMemberName = component.get('c.getCheckInMemberName');
        
        getCheckInMemberName.setParams({checkInID: component.get('v.recordId')});
        
        getCheckInMemberName.setCallback(this, function(response) {
            const state = response.getState();
            
            if (state == 'SUCCESS') {
                const checkIn = response.getReturnValue();
                
                component.set('v.memberName', checkIn.Account__r.Name);
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(getCheckInMemberName);
	},
    feedbackChanged: function(component, event, helper) {
        console.log('changed');
        
        const text = event.getSource().get('v.value');
        
        console.log('text: ', text.trim());
        console.log('text: ', (text && (text.trim() != '')));
        
        component.set('v.enableSubmit', (text && (text.trim() != '')));
    },
    submit: function(component, event, helper) {
		const postFeedback = component.get('c.postFeedback');
        
        postFeedback.setParams({
            checkInID: component.get('v.recordId'),
            text: component.get('v.feedback')
        });
        
        postFeedback.setCallback(this, function(response) {
            const state = response.getState();
            
            if (state == 'SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Feedback Posted",
                    "type": "success",
                    "mode": "pester"
                });
                toastEvent.fire();
                
                $A.get("e.force:closeQuickAction").fire();
            } else if (state == 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });

        $A.enqueueAction(postFeedback);
	},
    cancel: function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
})