({
	getCaseId: function (component, onSuccess, onError) {
	
		
        var recordId		= component.get("v.recordId");

		var getCaseAction	= component.get("c.getCaseId");
		getCaseAction.setParams({
            "checkInId" : recordId
		});
		getCaseAction.setCallback(this, function(response){
			let state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.caseId",	response.getReturnValue());
				onSuccess(response.getReturnValue());
			} else if(state === "ERROR") {
				onError(response.getReturnValue());
			}
        });
        $A.enqueueAction(getCaseAction);



	},
	getActivityTimeline: function(component) {

		var recordId = component.get("v.caseId");
        
       	// retrieve server method
        var action = component.get("c.getActivityTimeline");
        
        // set method paramaters
        action.setParams({
            "recordId" : recordId
        })

        // set call back instructions
        action.setCallback(this, function(a){

			console.log(1);
			let state = a.getState();
			console.log(2);
			if(state === "SUCCESS") {
				console.log("SUCCESS", JSON.parse(JSON.stringify(a.getReturnValue())));
				// assign server retrieved items to component variable
				component.set("v.timeLineItems", a.getReturnValue());
				console.log(3);
			} else if(state === "ERROR") {
				console.error("ERROR", JSON.parse(JSON.stringify(a.getError())));
				console.log(4);
			} else {

			}
			console.log(5);
        });
        
        // queue action on the server
        $A.enqueueAction(action);
	}
})