({
	onInit: function(component, event, helper) {
		
		var getPageDetailsAction = component.get("c.GetPageDetails");

		getPageDetailsAction.setCallback(this, function(response) {
			
			var state = response.getState();

			if(state === "SUCCESS") {

                const pageDetails = response.getReturnValue();
				console.log("Page Detais: ", pageDetails);
				component.set("v.pageDetails", pageDetails);

            } else if(state === "ERROR") {

                const toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    'type': 'error',
                    "title": "Error",
                    message: JSON.stringify(response.getError())
                });
                toastEvent.fire();
                
                console.log('ERROR:');
                console.log(JSON.stringify(response.getError()));

            }

		});
    	$A.enqueueAction(getPageDetailsAction);


	},
	postMessage: function(component, event, helper) {
		var messageTrainer = component.get("c.messageTrainer");
        
		messageTrainer.setParams({
            message: component.get('v.message')
        });

        messageTrainer.setCallback(this, function(response) {
            var state = response.getState();

            if(state === 'SUCCESS') {
                const trainerName = response.getReturnValue();
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					"title":	"Message Sent to " + trainerName,
					"message":	trainerName + " has received your message",
					"type":		"success"
				});
				toastEvent.fire();
            } else if(state === "ERROR") {
                const toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    'type': 'error',
                    "title": "Error",
                    message: JSON.stringify(response.getError())
                });
                toastEvent.fire();
                
                console.log('ERROR:');
                console.log(JSON.stringify(response.getError()));
            }
        });
        
    	$A.enqueueAction(messageTrainer);
	},
	returnHome: function(component, event, helper) {

		let pageDetails = component.get("v.pageDetails");

		var navService = component.find("navService");
		var webpageReference = {  
			"type": "standard__webPage",
			"attributes": {
				"url": pageDetails.HomePageUrl + '/s/'
			}
		};
        event.preventDefault();
        navService.navigate(webpageReference);

	}
})