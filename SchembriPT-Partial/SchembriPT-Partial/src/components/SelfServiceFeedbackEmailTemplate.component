<apex:component access="global" controller="ClientFeedbackEmailTemplateController">
    
    <apex:attribute access="global" type="String" name="queryType"  assignTo="{!checkInQuery}" description="How the Check-Ins will be queried"/>
    <apex:attribute access="global" type="String" name="chartType"  assignto="{!feedbackChartType}" description="The type of feedback chart, 'Single'  or 'All'." />
    <apex:attribute access="global" type="String" name="cId"        assignto="{!contactId}" description="The Contact Id" />
    <apex:attribute access="global" type="String" name="checkId"    assignTo="{!checkInId}" description="The Id of the Check-In object" />
    

    <div style="font-size: 16px;" class="feedback-text">
        <p>Hi {!cont.FirstName },</p>
        <p>Thanks for submitting your Self-Service Check-In.  Please see below your progress over time in the Schembri PT Programs and comparison photos for This Check-In vs Previous Check-In.</p>
        <p>Your next Check-In is due on {!currentCheckIn.Next_Check_In_Date__c}.  Please visit the Self-Service Portal in the meantime to track your progress at https://schembript.force.com/SelfService/s/.</p>
    </div>
    <div style="display:flex;">
        <div style="padding:0 1rem;">
            <table style="border-spacing: 0; width: 150px;" class="metric-table">
                <thead>
                    <tr class="table-key-row">
                        <td style="width: 50px; background-color: #000; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 600;">
                            Latest
                        </td>
                        <td style="width: 50px; background-color: #000; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 600;">
                            Total
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr class="metric-label">
                        <td colspan="2" style="width: 100px; background-color: #ec0e0e; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 18px; font-weight: 700;">
                            Weight (kg)
                        </td>
                    </tr>
                    <tr class="metric-data">
                        <td style="{!metrics.weight.latest.valueStyle}">
                            {!metrics.weight.latest.value}
                        </td>
                        <td style="{!metrics.weight.total.valueStyle}">
                            {!metrics.weight.total.value}
                        </td>
                    </tr>
                    <tr class="metric-space" style="line-height: 2rem;">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr class="metric-label">
                        <td colspan="2" style="width: 100px; background-color: #ec0e0e; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 18px; font-weight: 700;">
                            Chest (cm)
                        </td>
                    </tr>
                    <tr class="metric-data">
                        <td style="{!metrics.chest.latest.valueStyle}">
                            {!metrics.chest.latest.value}
                        </td>
                        <td style="{!metrics.chest.total.valueStyle}">
                            {!metrics.chest.total.value}
                        </td>
                    </tr>
                    <tr class="metric-space" style="line-height: 2rem;">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr class="metric-label">
                        <td colspan="2" style="width: 100px; background-color: #ec0e0e; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 18px; font-weight: 700;">
                            Waist (cm)
                        </td>
                    </tr>
                    <tr class="metric-data">
                        <td style="{!metrics.waist.latest.valueStyle}" class="{!metrics.waist.latest.valueClass}">
                            {!metrics.waist.latest.value}
                        </td>
                        <td style="{!metrics.waist.total.valueStyle}" class="{!metrics.waist.total.valueClass}">
                            {!metrics.waist.total.value}
                        </td>
                    </tr>
                    <tr class="metric-space" style="line-height: 2rem;">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr class="metric-label">
                        <td colspan="2" style="width: 100px; background-color: #ec0e0e; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 18px; font-weight: 700;">
                            Hip (cm)
                        </td>
                    </tr>
                    <tr class="metric-data">
                        <td style="{!metrics.hips.latest.valueStyle}" class="{!metrics.hips.latest.valueClass}">
                            {!metrics.hips.latest.value}
                        </td>
                        <td style="{!metrics.hips.total.valueStyle}" class="{!metrics.hips.total.valueClass}">
                            {!metrics.hips.total.value}
                        </td>
                    </tr>
                    <tr class="metric-space" style="line-height: 2rem;">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr class="metric-label">
                        <td colspan="2" style="width: 100px; background-color: #ec0e0e; color: white; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 18px; font-weight: 700;">
                            Thigh (cm)
                        </td>
                    </tr>
                    <tr class="metric-data">
                        <td style="{!metrics.thigh.latest.valueStyle}" class="{!metrics.thigh.latest.valueClass}">
                            {!metrics.thigh.latest.value}
                        </td>
                        <td style="{!metrics.thigh.total.valueStyle}" class="{!metrics.thigh.total.valueClass}">
                            {!metrics.thigh.total.value}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <table style="width: 910px;">
            <thead>
                <tr>
                    <td style="width: 490px; text-align: center;  font-size: 24px; font-weight: 600;">
                        Measurements
                    </td>
                    <td style="width: 420px; text-align: center; font-size: 24px;  font-weight: 600;">
                        Calories
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div style="width: 490px;">
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <img src="{!charts.weight}" style="width: 490px; height: 175px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{!charts.chest}" style="width: 245px; height: 175px;" />
                                        </td>
                                        <td>
                                            <img src="{!charts.waist}" style="width: 245px; height: 175px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{!charts.hips}" style="width: 245px; height: 175px;" />
                                        </td>
                                        <td>
                                            <img src="{!charts.thigh}" style="width: 245px; height: 175px;" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div style="position: relative; padding-left: 1rem; width: 420px; height: 525px;">
                            <img src="{!charts.calorie}" style="width: 420px; height: 525px; " />
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <div style="display: flex;">
        <div style="display: flex; margin: 0.5rem;">
            <div style="background-color: #000000c9; padding: 0.5rem;">
                <img style="width: 150px;" src="{!photos.previousFront}" />
            </div>
            <div style="background-color: #000000c9; padding: 0.5rem;">
                <img style="width: 150px;" src="{!photos.latestFront}" />
            </div>
        </div>
        <div style="display: flex; margin: 0.5rem;">
            <div style="background-color: #000000c9; padding: 0.5rem;">
                <img style="width: 150px;" src="{!photos.previousSide}" />
            </div>
            <div style="background-color: #000000c9; padding: 0.5rem;">
                <img style="width: 150px;" src="{!photos.latestSide}" />
            </div>
        </div>
        <div style="display: flex; margin: 0.5rem;">
            <div style="background-color: #000000c9; padding: 0.5rem;">
                <img style="width: 150px;" src="{!photos.previousBack}" />
            </div>
            <div style="background-color: #000000c9; padding: 0.5rem;">
                <img style="width: 150px;" src="{!photos.latestBack}" />
            </div>
        </div>
    </div>
</apex:component>