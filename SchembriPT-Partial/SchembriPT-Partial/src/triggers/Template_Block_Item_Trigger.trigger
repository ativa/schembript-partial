trigger Template_Block_Item_Trigger on Template_Block_Item__c (after insert, after update, after delete)  { 

	//Query the Template Blocks
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			Template_Block_Item_TriggerClass.afterInsert(Trigger.new);
		}
		if(Trigger.isUpdate) {
			Template_Block_Item_TriggerClass.afterUpdate(Trigger.new);
		}
		if(Trigger.isDelete) {
			Template_Block_Item_TriggerClass.afterDelete(Trigger.old);
		}
	}

}