/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-28-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-28-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
trigger CheckIn_Trigger on Check_In__c (before insert) {

}