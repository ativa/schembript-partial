trigger FeedCommentTrigger on FeedComment (after insert) {
    if (ChatterUtil.disableTrigger) return;
    
    Set<Id> itemIDs = new Set<Id>();
    
    Map<Id, FeedItem> itemMap = new Map<Id, FeedItem>();
    
    for (FeedComment comment: Trigger.NEW) itemIDs.add(comment.FeedItemId);

    Map<Id, Id> links = new Map<Id, Id>();
    
    for (Feed_Link__c link: [SELECT Internal_ID__c, Community_ID__c FROM Feed_Link__c WHERE (Internal_ID__c IN :itemIDs) OR (Community_ID__c IN :itemIDs)]) {
        links.put(link.Internal_ID__c, link.Community_ID__c);
        links.put(link.Community_ID__c, link.Internal_ID__c);
    }
    
    System.debug('links: ');
    System.debug(links);
    
    if (links.keySet().isEmpty()) return;
    
    ChatterUtil.disableTrigger = true;
    
    List<FeedComment> copiedComments = new List<FeedComment>();
    
    for (FeedComment comment: Trigger.NEW) {
        Id linkedID = links.get(comment.FeedItemId);
        
        System.debug(comment.FeedItemId + ' -> ' + linkedID);
        
        if (linkedID != null) {
            //ConnectApi.Comment copy = ConnectApi.ChatterFeeds.postCommentToFeedElement(null, linkedID, comment.CommentBody);
            
            //System.debug(copy);
            
            FeedComment copied = new FeedComment(FeedItemId = linkedID, CommentBody = comment.CommentBody.unescapeHtml4(), IsRichText = true);
            copiedComments.add(copied);
        }
    }
    
    insert copiedComments;
    
    ChatterUtil.disableTrigger = false;
}