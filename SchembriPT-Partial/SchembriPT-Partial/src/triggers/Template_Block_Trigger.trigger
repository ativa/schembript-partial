trigger Template_Block_Trigger on Template_Block__c (after insert, after update, after delete)  { 


	//Query the Template Blocks
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			Template_Block_TriggerClass.afterInsert(Trigger.new);
		}
		if(Trigger.isUpdate) {
			Template_Block_TriggerClass.afterUpdate(Trigger.new);
		}
		if(Trigger.isDelete) {
			Template_Block_TriggerClass.afterDelete(Trigger.old);
		}
	}



}