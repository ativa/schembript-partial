trigger Account_Trigger on Account (after insert, after update)  { 

    //If Automation settings are disabled then don't run the trigger
    Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
    if(autoSettings.Enable_Automation__c == false) {
        return;
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            Integer startQuery = Limits.getQueries();
            Account_Trigger_Tools.afterInsert(Trigger.new);
            Integer endQuery = Limits.getQueries();
        }
    }
   
}