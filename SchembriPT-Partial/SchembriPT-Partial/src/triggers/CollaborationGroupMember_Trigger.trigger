trigger CollaborationGroupMember_Trigger on CollaborationGroupMember (before insert)  { 

    //If Automation settings are disabled then don't run the trigger
    Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
    if(autoSettings.Enable_Automation__c == false) {
        return;
    }

    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            CollaborationGroupMember_Trigger_Tools.beforeInsert(Trigger.new);
        }
    }

}