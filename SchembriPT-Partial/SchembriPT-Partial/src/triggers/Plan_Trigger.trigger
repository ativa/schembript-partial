/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-15-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-15-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
trigger Plan_Trigger on Plan__c (before insert, before update, before delete, after insert, after update, after delete)  {

    /*
    //If Automation settings are disabled then don't run the trigger
    Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
    if(autoSettings.Enable_Automation__c == false) {
        return;
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            Plan_Trigger_Tools.insertPlans(Trigger.new);
        } else if(Trigger.isUpdate) {
            Plan_Trigger_Tools.updatePlans(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isDelete) {
            Plan_Trigger_Tools.deletePlans(Trigger.old);
        }
    }
    */

}