trigger Case_Trigger on Case (before insert, before update) {
  //If Automation settings are disabled then don't run the trigger
    Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
    if(autoSettings.Enable_Automation__c == false) {
        return;
    }

    if(Trigger.isBefore) {
        if(Trigger.isInsert || Trigger.isUpdate) {
            Integer startQuery = Limits.getQueries();
            Case_Trigger_Tools.beforeInsertOrUpdate(Trigger.new, Trigger.newMap);
            Integer endQuery = Limits.getQueries();
        }
    }
}