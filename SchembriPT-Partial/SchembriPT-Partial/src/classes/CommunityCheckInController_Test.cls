@isTest 
private class CommunityCheckInController_Test {

	@isTest
	private static void GetAllMultiMediaFiles_FunctionalTest() {
		Test.startTest();
		CommunityCheckInController.GetAllMultiMediaFiles(null);
		Test.stopTest();
	}
	
	@isTest
	private static void DeleteMultiMediaFile_FunctionalTest() {

		Photo__c newPhoto = new Photo__c();
		newPhoto.Type__c = 'Front';
		newPhoto.Source__c = 'Dropbox';
		insert newPhoto;

		Test.startTest();
		CommunityCheckInController.DeleteMultiMediaFile(newPhoto.Id);
		Test.stopTest();
	}
}