public without sharing class ClientProgressLatestCheckInController {

	 public string recordId { get; set; } 

	 public Id contactId { get; set; }

	 Public ClientProgressLatestCheckInController(ApexPages.StandardController controller) { 
		recordId = ApexPages.CurrentPage().getparameters().get('id'); 

		Check_In__c cCheckIn = [
			SELECT
				Id,
				Contact__c
			FROM Check_In__c
			WHERE Id = :recordId
		];

		contactId = cCheckIn.Contact__c;

	 } 

}