/**
* @author Harrison Campbell
* @description Lightning Controller for the 'plannerXNutritionFlexiPlans'
*/ 
public without sharing class AT_LWCC_PlannerNutritionFlexiPlans {

	/**
	* @author Harrison Campbell
	* @description An exception that is thrown when field data is missing
	*/ 
	public class MissingFieldException extends Exception {}

	/**
	* @author Harrison Campbell
	* @description Data for the flexi plan
	*/ 
	public class FlexiPlanData { 
		
		@AuraEnabled
		public CheckInInformation checkInInfo;

		@AuraEnabled
		public List<AT_M_Nutrition.NutritionTarget> nutritionTargets;

		@AuraEnabled
		public List<PicklistValue> allergies;

		@AuraEnabled
		public List<PicklistValue> dietaryRestrictions;

		public FlexiPlanData() {
			this.nutritionTargets		= new List<AT_M_Nutrition.NutritionTarget>();
			this.allergies				= new List<PicklistValue>();
			this.dietaryRestrictions	= new List<PicklistValue>();
		}

	}

	public class CheckInInformation {

		@AuraEnabled
		public Id id { get; set; }
		
		@AuraEnabled
		public Boolean flexiPlansLocked { get; set; }

		public CheckInInformation(Check_In__c checkIn) {
			this.id					= checkIn.Id;
			this.flexiPlansLocked	= checkIn.Flexi_Plans_Locked__c;
		}

	}

	/**
	* @author Harrison Campbell
	* @description Stores information on a picklist field
	*/ 
	public class PicklistValue {
		
		@AuraEnabled
		public String label;
		
		@AuraEnabled
		public String value;

		public PicklistValue(Schema.PicklistEntry pv) {
			this.label = pv.getLabel();
			this.value = pv.getValue();
		}

	}

	/**
	* @description Get the picklist values for a given field
	* @param objectApiName The API name of the object
	* @param fieldApiName The API name of the field
	* @return Returns picklist values
	*/ 
	private static List<PicklistValue> GetPicklistValues(String objectApiName, String fieldApiName) {
        Map<String, Schema.SObjectType> gd			= Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map	= gd.get(objectApiName).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues	= field_map.get(fieldApiName).getDescribe().getPickListValues();       
		List<PicklistValue> optionlist = new List<PicklistValue>();
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(new PicklistValue(pv));
        }
        return optionlist;
	}

	/**
	* @description Query Nutritional Targets
	* @param checkInId The Check-In that the targets relate to
	* @return A list of Nutritional Targets
	*/ 
	private static List<Targets__c> QueryNutritionalTargets(String checkInId) {
		return [
			SELECT
				Id,
				Check_In__c,
				Calories__c,
				Carbs__c,
				Fat__c,
				Protein__c,
				Weekdays__c,
				Display_Name__c
			FROM Targets__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Nutrition'
		];
	}

	/**
	* @description Query the Nutrition Target Record Type
	* @return The RecordType object
	*/ 
	private static RecordType QueryNutritionTargetRecordType() {
		return [SELECT Id FROM RecordType WHERE SobjectType = 'Targets__c' AND DeveloperName = 'Nutrition'];
	}
	
	private static void UpdateTarget(AT_M_Nutrition.NutritionTarget nTarget, Targets__c newTarget) {
		newTarget.Display_Name__c	= nTarget.name;

		if(nTarget.checkInId == null) {
			throw new MissingFieldException('The Check-In Id needs to be set');
		}

        newTarget.Target_Pool_Type__c   = nTarget.targetPoolType;
		newTarget.Check_In__c		    = nTarget.checkInId;
		newTarget.Carbs__c			    = nTarget.carbs;
		newTarget.Fat__c			    = nTarget.fat;
		newTarget.Protein__c		    = nTarget.protein;

		if(nTarget.assignedTo != null && nTarget.assignedTo.size() > 0) {
			newTarget.Weekdays__c = '';
			for(Integer i = 0; i < nTarget.assignedTo.size(); i++) {
				if(i > 0) {
					newTarget.Weekdays__c += ';';
				}
				newTarget.Weekdays__c += nTarget.assignedTo.get(i);
			}
		}
	}

	private static void AddNewNutritionalTargets(List<AT_M_Nutrition.NutritionTarget> nutritionalTargets) {
		RecordType nutTargetRecType = QueryNutritionTargetRecordType();
		List<Targets__c> newTargets = new List<Targets__c>();

		for(AT_M_Nutrition.NutritionTarget nTarget : nutritionalTargets) {
			Targets__c newTarget		= new Targets__c();
			newTarget.RecordTypeId	= nutTargetRecType.Id;
			UpdateTarget(nTarget, newTarget);
			newTargets.add(newTarget);
		}

		insert newTargets;
	}

	private static void UpdateOldNutritionalTargets(List<AT_M_Nutrition.NutritionTarget> nutritionalTargets) {

		List<Id> tIds = new List<Id>();
		for(AT_M_Nutrition.NutritionTarget nTarget : nutritionalTargets) {
			tIds.add(nTarget.id);
		}
		
		Map<Id, Targets__c> oldTargets = new Map<Id, Targets__c>([
			SELECT
				Id,
				Target_Pool_Type__c,
				Check_In__c,
				Carbs__c,
				Fat__c,
				Protein__c,
				Weekdays__c
			FROM Targets__c
			WHERE Id IN :tIds
		]);

		List<Id> calorieTargetIds = new List<Id>();
		
		for(AT_M_Nutrition.NutritionTarget oTarget : nutritionalTargets) {
			Targets__c oldTarget = oldTargets.get(oTarget.id);			
			if(oTarget.targetPoolType == 'Calorie Range') {
				calorieTargetIds.add(oTarget.id);
			}
			UpdateTarget(oTarget, oldTarget);
		}
		update oldTargets.values();

		AT_M_Nutrition.RefreshCalorieRangeTargets(calorieTargetIds);
	}

	@AuraEnabled
	public static FlexiPlanData GetFlexiPlanData(Id checkInId) {
		FlexiPlanData fPlanData			= new FlexiPlanData();
		fPlanData.checkInInfo			= new CheckInInformation([SELECT Id, Flexi_Plans_Locked__c FROM Check_In__c WHERE Id = :checkInId]);
		fPlanData.nutritionTargets		= GetNutritionalTargets(checkInId);
		fPlanData.allergies				= GetPicklistValues('Advisory__c','Allergies_and_Intolerances__c');
		fPlanData.dietaryRestrictions	= GetPicklistValues('Advisory__c','Dietary_Restrictions__c');
		return fPlanData;
	}

	@AuraEnabled
	public static void UpdateFlexiPlansLocked(Id checkInId, Boolean isLocked) {
		Check_In__c checkIn = [
			SELECT
				Id,
				Flexi_Plans_Locked__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];
		checkIn.Flexi_Plans_Locked__c = isLocked;
		update checkIn;
	}

	/**
	* @description Get the Nutritional Targets
	* @param checkInId The Id of the Check-In
	* @return Returns the Nutritional Targets
	*/ 
	@AuraEnabled
	public static List<AT_M_Nutrition.NutritionTarget> GetNutritionalTargets(String checkInId) {
		return AT_M_Nutrition.GetCheckInTargets(checkInId);
	}
	
	/**
	* @description Save Nutritional Targets
	* @param nutritionalTargets The new/old Targets to insert/update
	* @param targetsToDelete A set of target Ids marked to be deleted
	*/ 
	@AuraEnabled
	public static void SaveNutritionalTargets(List<AT_M_Nutrition.NutritionTarget> nutritionalTargets, String[] targetsToDelete) {
		

		System.debug(JSON.serializePretty(nutritionalTargets));

		//Separate the targets into new targets and old targets
		List<AT_M_Nutrition.NutritionTarget> newTargets = new List<AT_M_Nutrition.NutritionTarget>();
		List<AT_M_Nutrition.NutritionTarget> oldTargets = new List<AT_M_Nutrition.NutritionTarget>();
		for(AT_M_Nutrition.NutritionTarget nTarget : nutritionalTargets) {
			if(nTarget.id == null) {
				newTargets.add(nTarget);
			} else {
				oldTargets.add(nTarget);
			}
		}
		if(targetsToDelete != null && targetsToDelete.size() > 0) {
			List<Targets__c> delTargets = [SELECT Id FROM Targets__c WHERE Id = :targetsToDelete];
			delete delTargets;
		}

		//Insert/Update Nutritional Targets
		AddNewNutritionalTargets(newTargets);
		UpdateOldNutritionalTargets(oldTargets);

	}

}