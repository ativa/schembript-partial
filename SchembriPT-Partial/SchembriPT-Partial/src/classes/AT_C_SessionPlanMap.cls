/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 12-24-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   12-21-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class AT_C_SessionPlanMap {

    @AuraEnabled
    public String sessionPlanId { get; set; }

    @AuraEnabled
    public Date targetDate { get; set; }
    
    @AuraEnabled
    public String referencePlanId { get; set; }
    
    public AT_C_SessionPlanMap(){}
    
}