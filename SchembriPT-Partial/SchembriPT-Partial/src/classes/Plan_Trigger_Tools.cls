/**
* @author Harrison Campbell
* @description All Plans that 
*/ 
public without sharing class Plan_Trigger_Tools {
	
	/**
	* @description Get all of the plans that are reference plans, and have a Check-In that is not closed
	* @param plans The Plans 
	* @return Returns Reference Plans with not closed parent checkins
	*/
	private static List<Plan__c> getRefPlansWithNotClosedCheckIn(List<Plan__c> plans) {

		//Get the Ids for the For the CheckIns
		Id planReferenceRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Reference_Plan'].Id;

		Set<Id> checkInIds = new Set<Id>();

		for(Plan__c plan : plans) {
			if(plan.Check_In__c != null) {
				checkInIds.add(plan.Check_In__c);
			}
		}	

		//Query the reference plans with the associated CheckIns

		return [
			SELECT
				Id,
				Name,
				Order__c,
				RecordTypeId,
				Check_In__c,
				Weekdays__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Carbs__c,
				Trainer_Plan_Calories__c,
				Trainer_Plan_Protein__c
			FROM Plan__c
			WHERE Check_In__c IN :checkInIds AND RecordTypeId = :planReferenceRecId
		];

	}

	/**
	* @description Get The Check-Ins that are parent to the Plans
	* @param plans A list of Plans
	* @return Returns the Check-Ins Mapped by their Id
	*/
	private static Map<Id, Check_In__c> getParentCheckins(List<Plan__c> plans) {
	
		//Get the Ids for the Plans
		Set<Id> planCheckInIds = new Set<Id>();
		for(Plan__c plan : plans) {
			planCheckInIds.add(plan.Check_In__c);
		}

		//Get the Check-Ins from the plans
		Map<Id, Check_In__c> checkIns = new Map<Id, Check_In__c>([
			SELECT
				Id,
				Name,
				Nutrition_Plan_WIP__c,
				Check_In_Number_v2__c,
				Client_Program__c
			FROM Check_In__c
			WHERE Id IN :planCheckInIds
			FOR UPDATE
		]);

		return checkIns;

	}

	/**
	* @description Get a List of session plans That are grouped by their Check-Ins
	* @param checkIns The Check-ins
	* @return Returns A list of session plans grouped by the Check-In Ids
	*/
	private static Map<Id, List<Plan__c>> getSessionPlans(List<Check_In__c> checkIns) {
	
		
		Map<Id, List<Plan__c>> groupedPlans = new Map<Id, List<Plan__c>>();

		for(Check_In__c chIn : checkIns) {
			groupedPlans.put(chIn.Id, new List<Plan__c>());
		}
		
		//Query the Session Plans, order then by their order field
		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;
		List<Plan__c> checkPlans = [
			SELECT
				Id,
				Name,
				Order__c,
				Check_In__c,
				Weekdays__c
			FROM Plan__c
			WHERE Check_In__c IN :checkIns AND RecordTypeId = :planSessionRecId
			ORDER BY Check_In__c ASC, Order__c ASC
		];

		for(Plan__c p : checkPlans) {
			
			groupedPlans.get(p.Check_In__c).add(p);
		}

		return groupedPlans;

	}

	/**
	* @description Update the Plans
	* @param refPlans The reference plans related to the Check-Ins
	*/
	public static void updatePlansOnSameCheckIn(List<Plan__c> refPlans) {

		//Get the Check-Ins from the reference plans
		Map<Id, Check_In__c> checkIns = getParentCheckins(refPlans);

		//Group the reference Plans by CheckIns
		Map<Id, List<Plan__c>> refPlanGoup = new Map<Id, List<Plan__c>>();
		for(Plan__c p : refPlans) {
			if (!refPlanGoup.containsKey(p.Check_In__c)){
				refPlanGoup.put(p.Check_In__c, new List<Plan__c>());
			}
			refPlanGoup.get(p.Check_In__c).add(p);
		}


		//Query all of the session plans and group them by Check-Ins
		Map<Id, List<Plan__c>> sessionPlans = getSessionPlans(checkIns.values());
		
		Map<Id, Check_In__c> programIdsToUpdate = new Map<Id, Check_In__c>();

		//For each check-in concat the child plan totals
		for(Check_In__c checkIn : checkIns.values()) {

			updatePlanTargetsAndAverages(checkIn, refPlanGoup.get(checkIn.Id), sessionPlans.get(checkIn.Id));
			
			//If this is the first check in grab it as we will use it to update the program
			if(checkIn.Check_In_Number_v2__c == 0) {
				programIdsToUpdate.put(checkIn.Client_Program__c, checkIn);
			}

		}
		
		update checkIns.values();
		
		//After we have update the checkins update all of the programs that have updated the 0 checkin
		Map<Id, Case> programs = new Map<Id, Case>([
			SELECT
				Id,
				Check_In_0_Carb__c,
				Check_In_0_Fat__c,
				Check_In_0_Protein__c,
				Check_In_0_Calories__c
			FROM Case
			WHERE Id IN :programIdsToUpdate.keySet()
			FOR UPDATE
		]);
		for(Case program : programs.values()) {
			Check_In__c cIn					= programIdsToUpdate.get(program.Id);
			program.Check_In_0_Fat__c		= cIn.Plan_Summary_Fat_New__c;
			program.Check_In_0_Protein__c	= cIn.Plan_Summary_Protein_New__c;
			program.Check_In_0_Carb__c		= cIn.Plan_Summary_Carbs_New__c;
			program.Check_In_0_Calories__c	= cIn.Plan_Summary_Calories_New__c;
		}
		
		update programs.values();
		
	}

	/**
	* @description Get a shortened list of the Weekdays
	* @param plan The Plan that contains the weekdays
	* @return Returns a list of weekdays with their names shortened (eg. Monday -> Mon)
	*/ 
	private static String getShortWeekdaysList(Plan__c plan) {

		String value = '';

		//If the Weekdays have not been set, then return an empty string
		if(plan.Weekdays__c == null) {
			return value;
		}

		String[] days = plan.Weekdays__c.split(';');

		Integer index = 0;
		for(String day : days) {
			
			if(index > 0) {
				value += ',';
			}

			if(day == 'Monday') {
				value += 'Mon';
			}
			
			if(day == 'Tuesday') {
				value += 'Tue';
			}
			
			if(day == 'Wednesday') {
				value += 'Wed';
			}
			
			if(day == 'Thursday') {
				value += 'Thu';
			}
			
			if(day == 'Friday') {
				value += 'Fri';
			}
			
			if(day == 'Saturday') {
				value += 'Sat';
			}
			
			if(day == 'Sunday') {
				value += 'Sun';
			}

			index += 1;

		}

		return value;

	}

	/**
	* @description Update the Nutrition Targets and Actuals for the Check-In
	* @param checkIn The Check-In we are updating
	* @param refPlans The Reference Plans related to the Check-In
	* @param sessPlans The Session Plans related to the Check-In
	*/ 
	private static void updatePlanTargetsAndAverages(Check_In__c checkIn, List<Plan__c> refPlans, List<Plan__c> sessPlans) {
		
		//Before continuing Check that all days are assigned
		Boolean mondayIsAssigned	= false;
		Boolean tuesdayIsAssigned	= false;
		Boolean wednesdayIsAssigned = false;
		Boolean thursdayIsAssigned	= false;
		Boolean fridayIsAssigned	= false;
		Boolean saturdayIsAssigned	= false;
		Boolean sundayIsAssigned	= false;
		
		for(Plan__c plan : refPlans) {

			if(plan.Weekdays__c == null) {
				continue;
			}

			List<String> weekdays = plan.Weekdays__c.split(';');
			if(weekdays.contains('Monday'))		{ mondayIsAssigned = true; }
			if(weekdays.contains('Tuesday'))	{ tuesdayIsAssigned = true; }
			if(weekdays.contains('Wednesday'))	{ wednesdayIsAssigned = true; }
			if(weekdays.contains('Thursday'))	{ thursdayIsAssigned = true; }
			if(weekdays.contains('Friday'))		{ fridayIsAssigned = true; }
			if(weekdays.contains('Saturday'))	{ saturdayIsAssigned = true; }
			if(weekdays.contains('Sunday'))		{ sundayIsAssigned = true; }

		}

		if(mondayIsAssigned == true && tuesdayIsAssigned == true && wednesdayIsAssigned == true && thursdayIsAssigned == true && fridayIsAssigned == true && saturdayIsAssigned == true && sundayIsAssigned == true) {
			
			String fatString		= '';
			String proteinString	= '';
			String carbString		= '';
			String calorieString	= '';
			
			Decimal fatTotal		= 0;
			Decimal proteinTotal	= 0;
			Decimal carbTotal		= 0;
			Decimal calorieTotal	= 0;
		
			Decimal fatAverage		= 0;
			Decimal proteinAverage	= 0;
			Decimal carbAverage		= 0;
			Decimal calorieAverage	= 0;

			//Loop through the Reference Plans, and concatenate the nutrition values
			for(Plan__c plan : refPlans) {

				String weekdayList = getShortWeekdaysList(plan);

				Integer dayCount = (plan.Weekdays__c != null) ? plan.Weekdays__c.split(';').size() : 0;

				Decimal fat		= plan.Trainer_Plan_Fat__c == null ? 0 : Math.round(plan.Trainer_Plan_Fat__c);
				Decimal protein = plan.Trainer_Plan_Protein__c == null ? 0 : Math.round(plan.Trainer_Plan_Protein__c);
				Decimal carb	= plan.Trainer_Plan_Carbs__c == null ? 0 : Math.round(plan.Trainer_Plan_Carbs__c);
				Decimal calorie = plan.Trainer_Plan_Calories__c == null ? 0 : Math.round(plan.Trainer_Plan_Calories__c);

				//If there are weekdays assigned to this plan, then add it before the value
				if(String.isBlank(weekdayList)) {
					//fatString		+= plan.Name + ': ' + fat.format() + 'g\n';
					//proteinString	+= plan.Name + ': ' + protein.format() + 'g\n';
					//carbString	+= plan.Name + ': ' + carb.format() + 'g\n';
					//calorieString	+= plan.Name + ': ' + calorie.format() + '\n';
				} else {

					//Plan A: 25g (Mon,Thu,Sat)

					fatString		+= plan.Name + ': ' + fat.format() + 'g ' + '(' + weekdayList + ')\n';
					proteinString	+= plan.Name + ': ' + protein.format() + 'g ' + '(' + weekdayList + ')\n';
					carbString		+= plan.Name + ': ' + carb.format() + 'g ' + '(' + weekdayList + ')\n';
					calorieString	+= plan.Name + ': ' + calorie.format() + ' ' + '(' + weekdayList + ')\n';

				}

				fatTotal		+= fat;
				proteinTotal	+= protein;
				carbTotal		+= carb;
				calorieTotal	+= calorie;

				fatAverage		+= fat		* dayCount;
				proteinAverage	+= protein	* dayCount;
				carbAverage		+= carb		* dayCount;
				calorieAverage	+= calorie	* dayCount;

			}

			checkIn.Plan_Summary_Fat_New__c				= fatString;
			checkIn.Plan_Summary_Protein_New__c			= proteinString;
			checkIn.Plan_Summary_Carbs_New__c			= carbString;
			checkIn.Plan_Summary_Calories_New__c		= calorieString;
			
			checkIn.Plan_Average_Fat_New__c				= fatAverage		/ 7;
			checkIn.Plan_Average_Protein_New__c			= proteinAverage	/ 7;
			checkIn.Plan_Average_Carbs_New__c			= carbAverage		/ 7;
			checkIn.Plan_Average_Calories_New__c		= calorieAverage	/ 7;

		} else {
		
			checkIn.Plan_Summary_Fat_New__c				= 'WIP';
			checkIn.Plan_Summary_Protein_New__c			= 'WIP';
			checkIn.Plan_Summary_Carbs_New__c			= 'WIP';
			checkIn.Plan_Summary_Calories_New__c		= 'WIP';
			
			checkIn.Plan_Average_Fat_New__c				= 0;
			checkIn.Plan_Average_Protein_New__c			= 0;
			checkIn.Plan_Average_Carbs_New__c			= 0;
			checkIn.Plan_Average_Calories_New__c		= 0;

		}


	}
	
	/**
	* @description Update Plans in the database
	* @param oldPlans The Plans before they were updated
	* @param newPlans The Plans after they were updated
	*/
	public static void updatePlans(Map<Id,Plan__c> oldPlans, Map<Id,Plan__c> newPlans) {

		//Get two groups, Plans on the same CheckIn Plans on Different CheckIn
		List<Plan__c> sameCheckinPlans		= new List<Plan__c>();
		List<Plan__c> differentCheckinPlans	= new List<Plan__c>();
		Map<Id, Plan__c> relatedDiffences	= new Map<Id, Plan__c>();
		for(Id key : oldPlans.keySet()) {
			if(oldPlans.get(key).Check_In__c == newPlans.get(key).Check_In__c) {
				sameCheckinPlans.add(newPlans.get(key));
			} else {
				differentCheckinPlans.add(newPlans.get(key));
				relatedDiffences.put(key, oldPlans.get(key));
			}
		}

		sameCheckinPlans		= getRefPlansWithNotClosedCheckIn(sameCheckinPlans);
		differentCheckinPlans	= getRefPlansWithNotClosedCheckIn(differentCheckinPlans);

		//Update the ones on the same checkin
		updatePlansOnSameCheckIn(sameCheckinPlans);
		//updatePlansOnDifferentCheckIn(differentCheckinPlans, relatedDiffences);

	}

	/**
	* @description Update the Plan Summary fields on a Check-In
	* @author ChangeMeIn@UserSettingsUnder.SFDoc | 01-16-2021 
	* @param refPlanId 
	**/
	@AuraEnabled
	public static void UpdateCheckInSummary(String refPlanId){
		
		List<Plan__c> refPlan = [
			SELECT 
				Id,
				Name,
				Check_In__c,
				Weekdays__c,
				Trainer_Plan_Carbs__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Protein__c,
				Trainer_Plan_Calories__c 
			FROM Plan__c
			WHERE Id = :refPlanId
		];
		String checkInId = refPlan.get(0).Check_In__c;
		UpdatePlanSummaryForCheckIn(checkInId);
	}

	/**
	* @description Update the Plan Summary for the Check-In
	* @param checkInId The Id of the Check-In
	*/ 
    @AuraEnabled
	public static void UpdatePlanSummaryForCheckIn(String checkInId) {
		
		//Query all of the Reference Plans related to the CheckIn
		List<Plan__c> refPlans = [
			SELECT
				Id,
				Name,
				Check_In__c,
				Weekdays__c,
				Trainer_Plan_Carbs__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Protein__c,
				Trainer_Plan_Calories__c 
			FROM Plan__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Nutrition_Reference_Plan'
		];


		updatePlansOnSameCheckIn(refPlans);
	}

}