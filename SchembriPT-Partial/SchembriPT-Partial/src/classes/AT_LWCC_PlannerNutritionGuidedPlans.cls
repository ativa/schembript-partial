/**
* @author Harrison Campbell
* @description Lightning Controller for the 'plannerXNutritionGuidedPlans'
*/ 
public class AT_LWCC_PlannerNutritionGuidedPlans {

	/**
	* @author Harrison Campbell
	* @description An exception that is thrown when field data is missing
	*/ 
	public class MissingFieldException extends Exception {}

	/**
	* @author Harrison Campbell
	* @description Stores information on a nutritional target
	*/ 
	public class NutritionalTarget {

		/**
		* @description The Id of the target
		*/ 
		@AuraEnabled
		public String id { get; set; }
		
		/**
		* @description The display name of the target
		*/ 
		@AuraEnabled
		public String name { get; set; }
		
		/**
		* @description The Check-In Id of the target
		*/ 
		@AuraEnabled
		public String checkInId { get; set; }
		
		/**
		* @description The target calories
		*/ 
		@AuraEnabled
		public Decimal calories { get; set; }
		
		/**
		* @description The target carbs
		*/ 
		@AuraEnabled
		public Decimal carbs { get; set; }
		
		/**
		* @description The target fat
		*/ 
		@AuraEnabled
		public Decimal fat { get; set; }
		
		/**
		* @description The target protein
		*/ 
		@AuraEnabled
		public Decimal protein { get; set; }
		
		/**
		* @description The assigned Weeks
		*/ 
		@AuraEnabled
		public String[] assignedWeeks { get; set; }

		public NutritionalTarget() { }

		/**
		* @description Create a nutritional target
		* @param target The Target Salesforce Object
		*/ 
		public NutritionalTarget(Targets__c target) {
			this.id				= target.Id;
			this.name			= target.Display_Name__c;
			this.checkInId		= target.Check_In__c;
			this.calories		= target.Calories__c;
			this.carbs			= target.Carbs__c;
			this.fat			= target.Fat__c;
			this.protein		= target.Protein__c;
			this.assignedWeeks	= (target.Assign_to_Weeks__c != null) ? target.Assign_to_Weeks__c.split(';') : null;
		}


	}
	
	/**
	* @author Harrison Campbell
	* @description Data for the Guided Plan 
	*/ 
	public class GuidedPlanData { 

		@AuraEnabled
		public String[] assignToWeekOptions;
		
		@AuraEnabled
		public List<NutritionalTarget> nutritionTargets;

		@AuraEnabled
		public List<PicklistValue> allergies;

		@AuraEnabled
		public List<PicklistValue> dietaryRestrictions;

		public GuidedPlanData() {
			this.nutritionTargets		= new List<NutritionalTarget>();
			this.allergies				= new List<PicklistValue>();
			this.dietaryRestrictions	= new List<PicklistValue>();
		}

	}
	/**
	* @author Harrison Campbell
	* @description Stores information on a picklist field
	*/ 
	public class PicklistValue {
		
		@AuraEnabled
		public String label;
		
		@AuraEnabled
		public String value;

		public PicklistValue(Schema.PicklistEntry pv) {
			this.label = pv.getLabel();
			this.value = pv.getValue();
		}

	}
	
	/**
	* @description Query Nutritional Targets
	* @param checkInId The Check-In that the targets relate to
	* @return A list of Nutritional Targets
	*/ 
	private static List<Targets__c> QueryNutritionalTargets(String checkInId) {
		return [
			SELECT
				Id,
				Check_In__c,
				Calories__c,
				Carbs__c,
				Fat__c,
				Protein__c,
				Weekdays__c,
				Assign_to_Weeks__c,
				Display_Name__c
			FROM Targets__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Nutrition'
		];
	}

	/**
	* @description Get the picklist values for a given field
	* @param objectApiName The API name of the object
	* @param fieldApiName The API name of the field
	* @return Returns picklist values
	*/ 
	private static List<PicklistValue> GetPicklistValues(String objectApiName, String fieldApiName) {
        Map<String, Schema.SObjectType> gd			= Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map	= gd.get(objectApiName).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues	= field_map.get(fieldApiName).getDescribe().getPickListValues();       
		List<PicklistValue> optionlist = new List<PicklistValue>();
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(new PicklistValue(pv));
        }
        return optionlist;
	}
	
	/**
	* @description Query the Nutrition Target Record Type
	* @return The RecordType object
	*/ 
	private static RecordType QueryNutritionTargetRecordType() {
		return [SELECT Id FROM RecordType WHERE SobjectType = 'Targets__c' AND DeveloperName = 'Nutrition'];
	}

	private static void AddNewNutritionalTargets(List<NutritionalTarget> nutritionalTargets) {

		RecordType nutTargetRecType = QueryNutritionTargetRecordType();
		List<Targets__c> newTargets = new List<Targets__c>();

		for(NutritionalTarget nTarget : nutritionalTargets) {
			Targets__c newTarget		= new Targets__c();
			newTarget.RecordTypeId	= nutTargetRecType.Id;
			UpdateTarget(nTarget, newTarget);
			newTargets.add(newTarget);
		}

		insert newTargets;

	}
	
	private static void UpdateTarget(NutritionalTarget nTarget, Targets__c newTarget) {
	
		if(nTarget.checkInId == null) {
			throw new MissingFieldException('The Check-In Id needs to be set');
		}

		newTarget.Display_Name__c		= nTarget.name;
		newTarget.Target_Pool_Type__c	= 'Selected Templates';
		newTarget.Check_In__c			= nTarget.checkInId;
		newTarget.Carbs__c				= nTarget.carbs;
		newTarget.Fat__c				= nTarget.fat;
		newTarget.Protein__c			= nTarget.protein;

		if(nTarget.assignedWeeks != null && nTarget.assignedWeeks.size() > 0) {
			newTarget.Assign_to_Weeks__c = '';
			for(Integer i = 0; i < nTarget.assignedWeeks.size(); i++) {
				if(i > 0) {
					newTarget.Assign_to_Weeks__c += ';';
				}
				newTarget.Assign_to_Weeks__c += nTarget.assignedWeeks.get(i);
			}
		}

	}

	private static void UpdateOldNutritionalTargets(List<NutritionalTarget> nutritionalTargets) {
	
		List<Targets__c> oldTargets = new List<Targets__c>();
		
		for(NutritionalTarget oTarget : nutritionalTargets) {
			Targets__c oldTarget = new Targets__c();
			oldTarget.Id = oTarget.Id;
			UpdateTarget(oTarget, oldTarget);
			oldTargets.add(oldTarget);
		}
		update oldTargets;

	}
	
	/**
	* @description Get the Nutritional Targets
	* @param checkInId The Id of the Check-In
	* @return Returns the Nutritional Targets
	*/ 
	@AuraEnabled
	public static List<NutritionalTarget> GetNutritionalTargets(String checkInId) {
		List<Targets__c> nutritionalTargets = QueryNutritionalTargets(checkInId);
		List<NutritionalTarget> nutTargets = new List<NutritionalTarget>();
		for(Targets__c nTarget : nutritionalTargets) {
			nutTargets.add(new NutritionalTarget(nTarget));
		}
		return nutTargets;
	}
	
	/**
	* @description Get the data used for the guided plan
	* @return Returns the guided plan data
	*/ 
	@AuraEnabled
	public static GuidedPlanData GetGuidedPlanData(String checkInId) {

		GuidedPlanData gPlanData		= new GuidedPlanData();
		
		Check_In__c checkIn = [
			SELECT
				Id,
				Guided_Plan_Number_Weeks__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		if(checkIn.Guided_Plan_Number_Weeks__c != null) {
			String selectedWeek = checkIn.Guided_Plan_Number_Weeks__c;
			List<PicklistValue> assignToWeeks = GetPicklistValues('Check_In__c','Guided_Plan_Number_Weeks__c');
			List<String> pValues = new List<String>();
			for(Integer i = 0; i < assignToWeeks.size(); i++) {
				if(assignToWeeks.get(i).value == selectedWeek) {
					for(Integer j = 0; j <= i; j++) {
						pValues.add(assignToWeeks.get(j).value);
					}
					break;
				}
			}
			gPlanData.assignToWeekOptions = pValues;
		}

		gPlanData.nutritionTargets		= GetNutritionalTargets(checkInId);
		gPlanData.allergies				= GetPicklistValues('Advisory__c','Allergies_and_Intolerances__c');
		gPlanData.dietaryRestrictions	= GetPicklistValues('Advisory__c','Dietary_Restrictions__c');

		return gPlanData;

	}

	/**
	* @description Save Nutritional Targets
	* @param nutritionalTargets The new/old Targets to insert/update
	* @param targetsToDelete A set of target Ids marked to be deleted
	*/ 
	@AuraEnabled
	public static void SaveNutritionalTargets(List<NutritionalTarget> nutritionalTargets, String[] targetsToDelete) {
		//Separate the targets into new targets and old targets
		List<NutritionalTarget> newTargets = new List<NutritionalTarget>();
		List<NutritionalTarget> oldTargets = new List<NutritionalTarget>();

		for(NutritionalTarget nTarget : nutritionalTargets) {
			if(nTarget.id == null) {
				newTargets.add(nTarget);
			} else {
				oldTargets.add(nTarget);
			}
		}

		if(targetsToDelete != null && targetsToDelete.size() > 0) {
			List<Targets__c> delTargets = [SELECT Id FROM Targets__c WHERE Id = :targetsToDelete];
			delete delTargets;

		}

		//Insert/Update Nutritional Targets
		AddNewNutritionalTargets(newTargets);
		UpdateOldNutritionalTargets(oldTargets);
	}

}