public without sharing class AT_F_SetupUser  {

	public class Request {
	
		@InvocableVariable(label='User Action' description='Does this create or update the User' required=false)
		public String userAction;

		@InvocableVariable(label='User Id' description='If updating the Id of the user' required=false)
		public String userId;

		@InvocableVariable(label='First Name' description='' required=false)
		public String firstName;
		
		@InvocableVariable(label='Last Name' description='' required=false)
		public String lastName;
		
		@InvocableVariable(label='Alias' description='' required=false)
		public String alias;
		
		@InvocableVariable(label='Email' description='' required=false)
		public String email;
		
		@InvocableVariable(label='Username' description='' required=false)
		public String username;
		
		@InvocableVariable(label='Nickname' description='' required=false)
		public String nickname;

		@InvocableVariable(label='Role Id' description='' required=false)
		public String roleId;
		
		@InvocableVariable(label='Profile Id' description='' required=false)
		public String profileId;
		
		@InvocableVariable(label='Is Active' description='' required=false)
		public Boolean active;
		
		@InvocableVariable(label='Mobile' description='' required=false)
		public String mobile;
		
		@InvocableVariable(label='Salesforce Content User' description='' required=false)
		public Boolean salesforceContentUser;
		
		@InvocableVariable(label='Time Zone' description='' required=false)
		public String timeZone;
		
		@InvocableVariable(label='Locale' description='' required=false)
		public String locale;
		
		@InvocableVariable(label='Email Encoding' description='' required=false)
		public String emailEncoding;
		
		@InvocableVariable(label='Language' description='' required=false)
		public String langauge;
		
		@InvocableVariable(label='Contact Id' description='' required=false)
		public String contactId;
		
		@InvocableVariable(label='Password' description='' required=false)
		public String password;

		@InvocableVariable(label='License Name' description='' required=false)
		public String licenseName;

	}


	@InvocableMethod(label = 'Setup User' description='' category='Ativa - Apex Flow Action')
	public static void SetupUser(List<Request> requests) {
		for(Request req : requests) { 
			System.enqueueJob(new AT_Q_UpdateUser(req.userAction, req.userId, req.firstName, req.lastName, req.alias, req.email, req.username, req.nickname, req.roleId, req.profileId, req.active, req.mobile, req.salesforceContentUser, req.timeZone, req.locale, req.emailEncoding, req.langauge, req.contactId, req.password, req.licenseName)); 
		}
	}

}