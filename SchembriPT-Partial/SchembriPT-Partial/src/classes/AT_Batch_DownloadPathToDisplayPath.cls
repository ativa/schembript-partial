global class AT_Batch_DownloadPathToDisplayPath implements Database.Batchable<SObject> {

	global AT_Batch_DownloadPathToDisplayPath() {
	}

	global Database.QueryLocator start(Database.BatchableContext cntxt) {
		return Database.getQueryLocator([
			SELECT
				Id,
				Reference__c,
				Dropbox_Path__c
			FROM Photo__c
		]);
	}

	global void execute(Database.BatchableContext cntxt, List<Photo__c> scope) {
		
		for(Photo__c p : scope) {

			if(p.Dropbox_Path__c != null) {

				//If we have changes the 'dl=0' at the end of the string, then update the dropbox path
				if(p.Dropbox_Path__c.endsWith('dl=0')) {
					
					//check if the end is not already 'raw=1'
					
					String beforeLast	= p.Dropbox_Path__c.substringBeforeLast('dl=0');
					String afterLast	= p.Dropbox_Path__c.substringAfterLast('dl=0');


					String newDropboxPath	= beforeLast + 'raw=1';
					p.Dropbox_Path__c		= newDropboxPath;
				}


			}

		}
		update scope;

	}

	global void finish(Database.BatchableContext cntxt) {

	}

}