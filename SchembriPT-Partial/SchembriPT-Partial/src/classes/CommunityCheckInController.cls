/**
* @author Harrison Campbell
* @description Class used for lwc components in 'CommunityCheckIn'
*/ 
public without sharing class CommunityCheckInController  {

	/**
	* @description Return All MultiMedia Files
	* @param checkInId The Id of the Check-In that all the files relate too
	* @return Returns the Returned files
	*/ 
	@AuraEnabled
	public static List<Photo__c> GetAllMultiMediaFiles(String checkInId) {

		List<Photo__c> allMultiMediaFiles = [
			SELECT
				Id,
				Name,
				Type__c,
				Reference__c,
				Dropbox_Path__c,
				Description__c,
				Timestamp__c
			FROM Photo__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Client_Other_Photos'
			ORDER BY Timestamp__c DESC
		];

		return allMultiMediaFiles;

	}

	@AuraEnabled
	public static void UploadFileToDropbox(String documentId, String photoId, String checkInId) {

		//Get the CheckIn, and the content version
		Check_In__c currentCheckIn = [
			SELECT
				Id,
				Account__r.Name,
				Account__r.Dropbox_Reference_Path__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];
        ContentVersion contVer = [
			SELECT 
				Id, 
				Title,
				FileExtension,
				FileType,
				Image_Type__c, 
				VersionData,
				ContentDocumentId
			FROM ContentVersion 
			WHERE (ContentDocumentId = :documentId) AND (isLatest = true)
		];

		Photo__c multiMediaFile = [
			SELECT
				Id,
				Name,
				Reference__c,
				Dropbox_Path__c,
				Timestamp__c
			FROM Photo__c
			WHERE Id = :photoId
		];
		
		//Construct the path to the folder
		String fullPath = currentCheckIn.Account__r.Dropbox_Reference_Path__c + contVer.Title + '.' + contVer.FileExtension;

		//Upload the file top dropbox and share it
		Dropbox_FilesAPI.UploadResult upRes								= Dropbox_FilesAPI.Upload(fullPath, contVer.VersionData);
		Dropbox_SharingAPI.CreateSharedLinkWithSettingsPacket sPacket	= Dropbox_SharingAPI.CreateSharedLinkWithSettings(upRes.path_display);

		String sharingURL = null;
		if(sPacket.statusCode == 200) { //Success
			sharingURL = sPacket.result.url.removeEnd('dl=0') + 'raw=1';
		} else if(sPacket.statusCode == 409) { //Conflict
			if(sPacket.error.error.shared_link_already_exists != null) {
				sharingURL = sPacket.error.error.shared_link_already_exists.metadata.url.removeEnd('dl=0') + 'raw=1';
			}
		}
		
		//Update the Reference Path, and the Dropbox Path
		multiMediaFile.Reference__c		= fullPath;
		multiMediaFile.Dropbox_Path__c	= sharingURL;

		update multiMediaFile;


	}

	@AuraEnabled
	public static void DeleteMultiMediaFile(String fileId) {

		Photo__c p = [SELECT Id FROM Photo__c WHERE Id = :fileId];
		delete p;
	}

	@AuraEnabled
	public static Map<String, String> GetDropboxDetails(String checkInId) {
		
		Dropbox_Settings__c dSettings = Dropbox_Settings__c.getInstance();

		Check_In__c currCheckIn = [
			SELECT
				Id,
				Name,
				Account__r.Dropbox_Reference_Path__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		Map<String, String> results = new Map<String, String>();
		results.put('CheckInName',				currCheckIn.Name);
		results.put('DropboxReferencePath',		currCheckIn.Account__r.Dropbox_Reference_Path__c);
		results.put('DropboxAuthorizationKey',	dSettings.Authorization_Code__c);
		return results;

	}

}