public class ChartJS_ScaleLabel  {

	@AuraEnabled
	public String fontColor { get; set; }

	@AuraEnabled
	public Integer fontSize { get; set; }

	@AuraEnabled
	public String labelString { get; set; }

}