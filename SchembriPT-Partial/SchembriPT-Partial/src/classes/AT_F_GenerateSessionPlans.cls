/**
* @author Harrison Campbell
* @description Generate Session Plans
*/ 
public without sharing class AT_F_GenerateSessionPlans {

	private class NewSessionPlanData {

		public Id newSessionId;

		public Id relatedReferenceId;

		public Date sessionDate;

	}

	private static Map<Id, Id> MapReferenceIdToCheckInId(List<AT_C_SessionPlanMap> sessionPlanMaps) {
		
		List<Id> referenceIds = new List<Id>();

		for(AT_C_SessionPlanMap sPlanMap : sessionPlanMaps) {
			referenceIds.add(sPlanMap.referencePlanId);
		}

		List<Plan__c> refPlans = [
			SELECT
				Id,
				Check_In__c
			FROM Plan__c
			WHERE Id IN :referenceIds
		];

		Map<Id, Id> refToCheckIn = new Map<Id, Id>();
		for(Plan__c rPlan : refPlans) {
			refToCheckIn.put(rPlan.Id, rPlan.Check_In__c);
		}
		return refToCheckIn;
	}

	private static Map<Id, AT_C_SessionPlanMap> MapToSessionId(List<AT_C_SessionPlanMap> sessionPlanMaps) {
		
		Map<Id, AT_C_SessionPlanMap> sessIdToRefId = new Map<Id, AT_C_SessionPlanMap>();
		for(AT_C_SessionPlanMap sessPlanMap : sessionPlanMaps) {
			sessIdToRefId.put(sessPlanMap.sessionPlanId, sessPlanMap);
		}
		return sessIdToRefId;

	}

	@InvocableMethod(label='Generate Session Plans' description='Generate new session plans' category='Ativa - Apex Flow Action')
	public static void GenerateSessionPlans(List<Request> requests) {

		for(Request r : requests) {

			//Map the session plans by the Session Id
			Map<Id, AT_C_SessionPlanMap> sessionPlanMapBasedOnOldSessionId	= MapToSessionId(r.sessionPlanMaps);
			
			//Get all of the session Ids
			List<String> sessionPlanIds = new List<String>();
			for(AT_C_SessionPlanMap sessPlanMap : r.sessionPlanMaps) {
				sessionPlanIds.add(sessPlanMap.sessionPlanId);
			}

			AT_F_ClonePlans.Request cPlanReq			= new AT_F_ClonePlans.Request();
			cPlanReq.targetCheckInId					= r.targetCheckInId;
			cPlanReq.planIds							= sessionPlanIds;
			List<AT_F_ClonePlans.Result> cPlanRes		= AT_F_ClonePlans.ClonePlans(new AT_F_ClonePlans.Request[] { cPlanReq });

			List<NewSessionPlanData> nSessionPlanData = new List<NewSessionPlanData>();

			for(AT_C_ClonedPlan cPlan : cPlanRes.get(0).clonedPlans) {
				AT_C_SessionPlanMap oldSessPlan = sessionPlanMapBasedOnOldSessionId.get(cPlan.planId);
				NewSessionPlanData nSessPData	= new NewSessionPlanData();
				nSessPData.newSessionId			= cPlan.clonedPlanId;
				nSessPData.relatedReferenceId	= oldSessPlan.referencePlanId;
				nSessPData.sessionDate			= oldSessPlan.targetDate;
				nSessionPlanData.add(nSessPData);
			}

			System.debug(nSessionPlanData);

			//Link the session plans to the correct reference plans, and assign to the correct session dates
			Set<Id> newSessIds = new Set<Id>();
            Set<Id> newRefIds = new Set<Id>();
			for(NewSessionPlanData nPlanData : nSessionPlanData) {
				newSessIds.add(nPlanData.newSessionId);
                newRefIds.add(nPlanData.relatedReferenceId);
			}
			Map<Id, Plan__c> newSessionPlans = new Map<Id, Plan__c>([
				SELECT
					Id,
					Name,
					Reference_Plan__c,
					Session_Date__c
				FROM Plan__c
				WHERE Id IN :newSessIds
			]);
            
            Map<Id, Plan__c> newRefPlans = new Map<Id, Plan__c>([
               SELECT
               	Id,
					Name,
                Trainer_Plan_Protein__c,
                Trainer_Plan_Carbs__c,
                Trainer_Plan_Fat__c,
                Trainer_Plan_Calories__c
               FROM Plan__c
                WHERE Id IN :newRefIds
            ]);
            
            
			for(NewSessionPlanData nPlanData : nSessionPlanData) {
				Plan__c sessPlan			= newSessionPlans.get(nPlanData.newSessionId);
                Plan__c relPlan				= newRefPlans.get(nPlanData.relatedReferenceId);

				Datetime nowDateTime = Datetime.newInstance(nPlanData.sessionDate, Time.newInstance(0,0,0,0));

				sessPlan.Session_Date__c	= nPlanData.sessionDate;
				sessPlan.Weekdays__c		= nowDateTime.format('EEEE');
                sessPlan.Name				= relPlan.Name + '-' + nowDateTime.format('EEEE') + '-' + nowDateTime.format('MMM') + nowDateTime.format('dd') + '-' + nowDateTime.format('yy');

                sessPlan.Reference_Plan__c 			= relPlan.Id;
                sessPlan.Trainer_Plan_Protein__c 	= relPlan.Trainer_Plan_Protein__c;
                sessPlan.Trainer_Plan_Carbs__c 		= relPlan.Trainer_Plan_Carbs__c;
                sessPlan.Trainer_Plan_Fat__c 		= relPlan.Trainer_Plan_Fat__c;
                sessPlan.Trainer_Plan_Calories__c 	= relPlan.Trainer_Plan_Calories__c;
                
			}
			update newSessionPlans.values();

			System.Debug('New Session Plans');
			System.Debug(newSessionPlans.values());


		}

	}
	
	public class Request {

		@InvocableVariable(label='Target Check-In Id')
		public String targetCheckInId;
		
		@InvocableVariable(label='Session Plan Maps')
		public List<AT_C_SessionPlanMap> sessionPlanMaps;

	}

}