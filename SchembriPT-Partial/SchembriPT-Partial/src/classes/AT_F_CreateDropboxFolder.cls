public class AT_F_CreateDropboxFolder {

	public class Request {
	
        @InvocableVariable(label='Folder Path' required=true)
		public String folderPath;

	}

	public class Result {
	
        @InvocableVariable(label='Path Display' required=true)
		public String pathDisplay;

		public Result(String pathDisplay) {
			this.pathDisplay = pathDisplay;
		}

	}
	
    @InvocableMethod(label='Create Dropbox Folder' description='Create a new folder for the dropbox' category='Ativa - Apex Flow Action')
	public static List<Result> CreateDropboxFolder(List<Request> reqs) {

		List<Result> results = new List<Result>();

		for(Request req : reqs) {
			Map<String, Object> folderResult	= (Map<String, Object>)Dropbox_FilesAPI.CreateFolder(req.folderPath.removeEnd('/'), true);
			Map<String, Object> mData			= (Map<String, Object>)folderResult.get('metadata');
			String pathDisplay			= (String)mData.get('path_display');
			results.add(new Result(pathDisplay));
		}

		return results;

	}


}