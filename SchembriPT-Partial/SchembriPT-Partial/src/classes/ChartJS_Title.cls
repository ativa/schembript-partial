/**
* @author Harrison Campbell
* @description Style for the Title of the chart
*/ 
public class ChartJS_Title  {

	/**
	* @description Is the title shown?
	*/ 
	public Boolean display { get; set; }

	/**
	* @description The size of the title
	*/ 
	public Integer fontSize { get; set; } 

	/**
	* @description Title text to display. If specified as an array, text is rendered on multiple lines.
	*/ 
	public String text { get; set; }

	/**
	* @description Number of pixels to add above and below the title text.
	*/ 
	public Integer padding { get; set; }

}