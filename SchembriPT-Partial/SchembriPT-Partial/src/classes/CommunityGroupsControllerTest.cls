/**
* @author Harrison Campbell
* @description 
*/ 
@isTest 
private class CommunityGroupsControllerTest {

	/**
	* @description Create Collaboration Groups
	* @param numGroups The number of groups to create
	* @return Returns the Collaboration groups 
	*/ 
	private static List<CollaborationGroup> createTestGroups(Integer numGroups) {
	
		List<CollaborationGroup> collGroups = new List<CollaborationGroup>();
		for(Integer i = 0; i < numGroups; i++) {
			CollaborationGroup newGroup = new CollaborationGroup();

			String num = '';
			if(i < 10) {
				num = '0' + i;
			} else {
				num = '' + i;
			}

			newGroup.Name						= 'Test Collab Group ' + num;
			newGroup.CollaborationType			= 'Public';
			collGroups.add(newGroup);
		}
		return collGroups;
	}

	@isTest
	private static void getGroupPage_GetPagePageSizeLessThanTotalRecords() {

		//Create several groups
		Integer totalGroups = 20;
		Integer pageSize	= 10;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Test.startTest();
		CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(0, pageSize);
		Test.stopTest();
		
		System.assertEquals(totalGroups,	cGroup.totalGroupCount);
		System.assertEquals(pageSize,		cGroup.collabGroups.size());
		for(Integer i = 0; i < cGroup.collabGroups.size(); i++) {
			System.assertEquals(collGroups[i].Id, cGroup.collabGroups[i].Id, collGroups[i].Name + '-' + cGroup.collabGroups[i].Name);
		}

	}

	@IsTest
	private static void getGroupPage_getSecondPage() {
	
		//Create several groups
		Integer totalGroups = 20;
		Integer pageSize	= 10;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Test.startTest();
		CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(1, pageSize);
		Test.stopTest();
		
		System.assertEquals(totalGroups,	cGroup.totalGroupCount);
		System.assertEquals(pageSize,		cGroup.collabGroups.size());
		for(Integer i = 0; i < cGroup.collabGroups.size(); i++) {
			System.assertEquals(collGroups[i + pageSize].Id, cGroup.collabGroups[i].Id, collGroups[i].Name + '-' + cGroup.collabGroups[i].Name);
		}

	}
	
	@isTest
	private static void getGroupPage_GetPagePageSizeEqualsTotalRecords() {

		//Create several groups
		Integer totalGroups = 20;
		Integer pageSize	= 20;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Test.startTest();
		CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(0, pageSize);
		Test.stopTest();
		
		System.assertEquals(totalGroups,	cGroup.totalGroupCount);
		System.assertEquals(pageSize,		cGroup.collabGroups.size());
		for(Integer i = 0; i < cGroup.collabGroups.size(); i++) {
			System.assertEquals(collGroups[i].Id, cGroup.collabGroups[i].Id, collGroups[i].Name + '-' + cGroup.collabGroups[i].Name);
		}

	}
	
	@isTest
	private static void getGroupPage_GetPagePageSizeGreaterThanTotalRecords() {

		//Create several groups
		Integer totalGroups = 20;
		Integer pageSize	= 30;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Test.startTest();
		CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(0, pageSize);
		Test.stopTest();
		
		System.assertEquals(totalGroups,	cGroup.totalGroupCount);
		System.assertEquals(totalGroups,		cGroup.collabGroups.size());
		for(Integer i = 0; i < cGroup.collabGroups.size(); i++) {
			System.assertEquals(collGroups[i].Id, cGroup.collabGroups[i].Id, collGroups[i].Name + '-' + cGroup.collabGroups[i].Name);
		}

	}
	
	@isTest
	private static void getGroupPage_GetInvalidPageIndex() {
	
		//Create several groups
		Integer pageIndex	= -1;
		Integer totalGroups = 20;
		Integer pageSize	= 20;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Boolean exceptionCaught = false;

		try {
			Test.startTest();
			CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(pageIndex, pageSize);
			Test.stopTest();
		} catch(CommunityGroupsController.InvalidPageParameterException e) {
			exceptionCaught = true;
		}
		System.assert(exceptionCaught);
	}
	
	@isTest
	private static void getGroupPage_GetPageSize0() {
		
		//Create several groups
		Integer totalGroups = 20;
		Integer pageSize	= 0;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Test.startTest();
		CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(0, pageSize);
		Test.stopTest();
		
		System.assertEquals(totalGroups,	cGroup.totalGroupCount);
		System.assertEquals(pageSize,		cGroup.collabGroups.size());

	}
	
	@isTest
	private static void getGroupPage_GetInvalidPageSize() {
	
		//Create several groups
		Integer pageIndex	= 0;
		Integer totalGroups = 20;
		Integer pageSize	= -1;
		List<CollaborationGroup> collGroups = createTestGroups(totalGroups);
		insert collGroups;
	
		Boolean exceptionCaught = false;

		try {
			Test.startTest();
			CommunityGroupsController.ChatterGroupPage cGroup = CommunityGroupsController.getGroupPage(pageIndex, pageSize);
			Test.stopTest();
		} catch(CommunityGroupsController.InvalidPageParameterException e) {
			exceptionCaught = true;
		}
		System.assert(exceptionCaught);
	}

    @isTest
    private static void getGroupDetails() {
		List<CollaborationGroup> collGroups = createTestGroups(1);

		
    }
}