/**
* @author Harrison Campbell
* @description Test the 'AT_LWCC_PlannerViewPlan'
*/ 
@IsTest
public without sharing class AT_LWCC_PlannerViewPlan_T  {

	@IsTest
	public static void GetPlanData_GetData() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1',1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1',2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2',3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c newItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newTmpItem1_1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);
        Item__c newItem2_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newTmpItem2_1.Id);
        Item__c newItem2_2              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newTmpItem2_2.Id);

		Test.startTest();
        AT_M_Nutrition.NutritionPlan nPlan = AT_LWCC_PlannerViewPlan.GetPlanData(newPlan1.Id);
		Test.stopTest();

        //Assert that the Plans are correct
        AT_TestingTools.Assert_NutritionReferencePlan(newPlan1, nPlan);
        System.assertEquals(2, nPlan.blocks.size());
        AT_TestingTools.Assert_NutritionReferenceBlock(newBlock1, nPlan.blocks.get(0));
        AT_TestingTools.Assert_NutritionReferenceBlock(newBlock2, nPlan.blocks.get(1));
		System.assertEquals(1, nPlan.blocks.get(0).items.size());
		AT_TestingTools.Assert_NutritionReferenceItem(newItem1_1, nPlan.blocks.get(0).items.get(0));
		System.assertEquals(2, nPlan.blocks.get(1).items.size());
		AT_TestingTools.Assert_NutritionReferenceItem(newItem2_1, nPlan.blocks.get(1).items.get(0));
		AT_TestingTools.Assert_NutritionReferenceItem(newItem2_2, nPlan.blocks.get(1).items.get(1));


	}
	

}