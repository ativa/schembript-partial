/**
* @author Harrison Campbell
* @description Controller for the 'plannerXNutritionCarbCyclePlan'
*/ 
public without sharing class AT_LWCC_PlannerNutritionCarbCyclePlan {

	/**
	* @author Harrison
	* @description Throw an exception if we were unable to find a plan
	*/ 
	public class NoPlanFoundException extends Exception {}
 
	private static Plan__c GetNutritionReferencePlan(String refPlanId) {
	
		if(refPlanId == null) {
			throw new NoPlanFoundException('No Plan Id was provided');
		}

		List<Plan__c> nutRefPlan = [
			SELECT
				Id,
				Name,
				Trainer_Plan_Calories__c,
				Trainer_Plan_Carbs__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Protein__c,
				Client_Plan_Calories__c,
				Client_Plan_Carbs__c,
				Client_Plan_Fat__c,
				Client_Plan_Protein__c,
				RecordType.DeveloperName
			FROM Plan__c
			WHERE Id = :refPlanId 
		];

		if(nutRefPlan.size() == 0) {
			throw new NoPlanFoundException('Either the Plan Id is invalid, or it does not link to a Nutrition Reference Plan');
		}

		return nutRefPlan.get(0);
	}

	private static List<Block__c> GetReferenceBlocks(String refPlanId) {
		return [
			SELECT
				Id,
				Name,
				Plan__c,
				Trainer_Block_Calories__c,
				Trainer_Block_Carbs__c,
				Trainer_Block_Fat__c,
				Trainer_Block_Protein__c,
				Client_Block_Calories__c,
				Client_Block_Carbs__c,
				Client_Block_Fat__c,
				Client_Block_Protein__c,
				Template_Block_Calories__c,
				Template_Block_Carbs__c,
				Template_Block_Fat__c,
				Template_Block_Protein__c,
				Template_Block__c,
				Display_Order__c,
				RecordType.DeveloperName
			FROM Block__c
			WHERE Plan__c = :refPlanId
			ORDER BY Display_Order__c ASC
		];
	}

	private static Map<Id, List<Item__c>> GetReferenceItems(List<Block__c> refBlocks) {

		Set<Id> blockIds = (new Map<Id, Block__c>(refBlocks)).keyset();
		
		List<Item__c> nutRefItems = [
			SELECT
				Id,
				Name,
				Block__c,
				Trainer_Calories__c,
				Trainer_Carbs__c,
				Trainer_Fat__c,
				Trainer_Protein__c,
				Trainer_Quantity__c,
				Client_Calories__c,
				Client_Carbs__c,
				Client_Fat__c,
				Client_Protein__c,
				Client_Quantity__c,
				Display_Order__c,
				Template_Item_Id__c,
				Template_Item_Name__c,
				Template_Block_Item__c,
				Template_Quantity__c,
				Template_Calories__c,
				Template_Carbs__c,
				Template_Fat__c,
				Template_Protein__c,
				Template_Measurement__c,
				Template_Quantity_Increment__c,
				Template_Minimum_Quantity__c,
				RecordType.DeveloperName
			FROM Item__c
			WHERE Block__c IN :blockIds
			ORDER BY Display_Order__c ASC
		];

		Map<Id, List<Item__c>> blocksToItems = new Map<Id, List<Item__c>>();
		for(Item__c itm : nutRefItems) {
			if(!blocksToItems.containsKey(itm.Block__c)) {
				blocksToItems.put(itm.Block__c, new List<Item__c>());
			}
			blocksToItems.get(itm.Block__c).add(itm);
		}
		return blocksToItems;



	}

	private static RecordType GetRecordType(String sObjectName, String developerName) {
		return [SELECT Id FROM RecordType WHERE SobjectType = :sObjectName AND DeveloperName = :developerName];
	}

	private static Id GetNutritionReferenceBlockRecordTypeId() {
		return [SELECT Id FROM RecordType WHERE DeveloperName = 'Nutrition_Reference_Block'].Id;
	}

	private static Id GetNutritionReferenceItemRecordTypeId() {
		return [SELECT Id FROM RecordType WHERE DeveloperName = 'Nutrition_Reference_Item'].Id;
	}
	
	private static Id GetNutritionTemplateBlockRecordTypeId() {
		return [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Block__c' AND DeveloperName = 'Template_Nutrition_Block'].Id;
	}

	private static Id GetNutritionTemplateBlockItemRecordTypeId() {
		return [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Block_Item__c' AND DeveloperName = 'Template_Nutrition_Block_Item'].Id;
	}


	private static void DeleteBlocks(List<String> blockIds) {
		List<Block__c> delBlocks = [SELECT Id FROM Block__c WHERE Id IN :blockIds];
		delete delBlocks;
	}

	private static void DeleteItems(List<String> itemIds) {
		List<Item__c> delItems = [SELECT Id FROM Item__c WHERE Id IN :itemIds];
		delete delItems;
	}

	private static void UpdatePlanData(AT_M_Nutrition.NutritionPlan refPlan) {

		Plan__c plan = GetNutritionReferencePlan(refPlan.id);
		plan.Name						= refPlan.name;
		
		if(plan.RecordType.DeveloperName == 'Nutrition_Reference_Plan') {
			plan.Trainer_Comments__c		= refPlan.trainerComments;
			plan.Trainer_Plan_Calories__c	= refPlan.calories;
			plan.Trainer_Plan_Carbs__c		= refPlan.carbs;
			plan.Trainer_Plan_Fat__c		= refPlan.fat;
			plan.Trainer_Plan_Protein__c	= refPlan.protein;
		} else if(plan.RecordType.DeveloperName == 'Nutrition_Client_Plan') {
			plan.Client_Plan_Calories__c	= refPlan.calories;
			plan.Client_Plan_Carbs__c		= refPlan.carbs;
			plan.Client_Plan_Fat__c		= refPlan.fat;
			plan.Client_Plan_Protein__c	= refPlan.protein;
		}

		update plan;

	}

	private static void UpdateBlocks(AT_M_Nutrition.NutritionPlan refPlan, List<AT_M_Nutrition.NutritionBlock> oldBlocks) {

		Id itemRecordTypeId = null;

		if(refPlan.recordTypeDeveloperName == 'Nutrition_Reference_Plan') {
			itemRecordTypeId = GetRecordType('Item__c', 'Nutrition_Reference_Item').Id;
		} else if(refPlan.recordTypeDeveloperName == 'Nutrition_Client_Plan') {
			itemRecordTypeId = GetRecordType('Item__c', 'Nutrition_Client_Item').Id;
		}

		Set<Id> oBlockIds = new Set<Id>();
		for(AT_M_Nutrition.NutritionBlock nutRefBlk : oldBlocks) {
			oBlockIds.add(nutRefBlk.id);
		} 
		

		Map<Id, Block__c> oRefBlocks = new Map<Id, Block__c>([
			SELECT
				Id,
				Name,
				Plan__c,
				Trainer_Block_Calories__c,
				Trainer_Block_Carbs__c,
				Trainer_Block_Fat__c,
				Trainer_Block_Protein__c,
				Display_Order__c,
				RecordType.DeveloperName
			FROM Block__c
			WHERE Id IN :oBlockIds
		]);

		Map<Id, List<Item__c>> oRefItems		= GetReferenceItems(oRefBlocks.values());
		List<Item__c> itemsToInsertOrUpdate = new List<Item__c>();

		for(AT_M_Nutrition.NutritionBlock nutBlock : oldBlocks) {

			//Update the Block
			Block__c linkedBlock = oRefBlocks.get(nutBlock.id);
			linkedBlock.Name						= nutBlock.name;

			
			if(linkedBlock.RecordType.DeveloperName == 'Nutrition_Reference_Block') {
				linkedBlock.Trainer_Block_Calories__c	= nutBlock.calories;
				linkedBlock.Trainer_Block_Carbs__c		= nutBlock.carbs;
				linkedBlock.Trainer_Block_Fat__c		= nutBlock.fat;
				linkedBlock.Trainer_Block_Protein__c	= nutBlock.protein;
			} else if(linkedBlock.RecordType.DeveloperName == 'Nutrition_Client_Block') {
				linkedBlock.Client_Block_Calories__c	= nutBlock.calories;
				linkedBlock.Client_Block_Carbs__c		= nutBlock.carbs;
				linkedBlock.Client_Block_Fat__c			= nutBlock.fat;
				linkedBlock.Client_Block_Protein__c		= nutBlock.protein;
			}

			linkedBlock.Display_Order__c			= nutBlock.order;
			
			Map<Id, Item__c> relatedItems = new Map<Id, Item__c>(oRefItems.containsKey(linkedBlock.Id) ? oRefItems.get(linkedBlock.Id) : new List<Item__c>());

			//Update the Block Items
			for(AT_M_Nutrition.NutritionItem nutItem : nutBlock.items) {
				
				//If the item is attached to the block update it
				if(relatedItems.containsKey(nutItem.id)) {
					
					Item__c linkedItem = relatedItems.get(nutItem.id);
					
					if(refPlan.recordTypeDeveloperName == 'Nutrition_Reference_Plan') {
						linkedItem.Trainer_Calories__c		= nutItem.calories;
						linkedItem.Trainer_Carbs__c			= nutItem.carbs;
						linkedItem.Trainer_Fat__c			= nutItem.fat;
						linkedItem.Trainer_Protein__c		= nutItem.protein;
						linkedItem.Trainer_Quantity__c		= nutItem.quantity;
					} else if(refPlan.recordTypeDeveloperName == 'Nutrition_Client_Plan') {
						linkedItem.Client_Calories__c		= nutItem.calories;
						linkedItem.Client_Carbs__c			= nutItem.carbs;
						linkedItem.Client_Fat__c			= nutItem.fat;
						linkedItem.Client_Protein__c		= nutItem.protein;
						linkedItem.Client_Quantity__c		= nutItem.quantity;
					}

					linkedItem.Display_Order__c		= nutItem.order;
					itemsToInsertOrUpdate.add(linkedItem);
				//Otherwise create a new item	
				} else {

					Item__c newItem = new Item__c();
					newItem.RecordTypeId					= itemRecordTypeId;
					newItem.Name							= nutItem.name;
					newItem.Block__c						= linkedBlock.Id;
					
					if(refPlan.recordTypeDeveloperName == 'Nutrition_Reference_Plan') {
						newItem.Trainer_Calories__c		= nutItem.calories;
						newItem.Trainer_Carbs__c		= nutItem.carbs;
						newItem.Trainer_Fat__c			= nutItem.fat;
						newItem.Trainer_Protein__c		= nutItem.protein;
						newItem.Trainer_Quantity__c		= nutItem.quantity;
					} else if(refPlan.recordTypeDeveloperName == 'Nutrition_Client_Plan') {
						newItem.Client_Calories__c		= nutItem.calories;
						newItem.Client_Carbs__c			= nutItem.carbs;
						newItem.Client_Fat__c			= nutItem.fat;
						newItem.Client_Protein__c		= nutItem.protein;
						newItem.Client_Quantity__c		= nutItem.quantity;
					}

					newItem.Display_Order__c				= nutItem.order;
					newItem.Template_Item_Id__c				= nutItem.referenceId;
					newItem.Template_Block_Item__c			= nutItem.referenceItemBlockId;
					newItem.Template_Item_Name__c			= nutItem.referenceItemName;
					newItem.Template_Quantity__c			= nutItem.referenceQuantity;
					newItem.Template_Calories__c			= nutItem.referenceCalories;
					newItem.Template_Carbs__c				= nutItem.referenceCarbs;
					newItem.Template_Fat__c					= nutItem.referenceFat;
					newItem.Template_Protein__c				= nutItem.referenceProtein;
					newItem.Template_Measurement__c			= nutItem.referenceMeasurement;
					newItem.Template_Quantity_Increment__c	= nutItem.referenceQuantityIncrement;
					newItem.Template_Minimum_Quantity__c	= nutItem.referenceMinimumQuantity;
					itemsToInsertOrUpdate.add(newItem);

				}

			}

		}
		
		//Update the blocks
		update oRefBlocks.values();
		upsert itemsToInsertOrUpdate;


	}
	
	private static void AddBlocks(AT_M_Nutrition.NutritionPlan refPlan, List<AT_M_Nutrition.NutritionBlock> newBlocks) {
	
		Id blockRecordTypeId	= null;
		Id itemRecordTypeId		= null;

		if(refPlan.recordTypeDeveloperName == 'Nutrition_Reference_Plan') {
			blockRecordTypeId	= GetRecordType('Block__c',	'Nutrition_Reference_Block').Id;
			itemRecordTypeId	= GetRecordType('Item__c',	'Nutrition_Reference_Item').Id;
		} else if(refPlan.recordTypeDeveloperName == 'Nutrition_Client_Plan') {
			blockRecordTypeId	= GetRecordType('Block__c',	'Nutrition_Client_Block').Id;
			itemRecordTypeId	= GetRecordType('Item__c',	'Nutrition_Client_Item').Id;
		}

		List<Block__c> nBlocks = new List<Block__c>();
		
		//Create the new blocks
		for(AT_M_Nutrition.NutritionBlock nutBlock : newBlocks) {

			Block__c newBlock					= new Block__c();
			newBlock.RecordTypeId				= blockRecordTypeId;
			newBlock.Plan__c					= refPlan.id;
			newBlock.Name						= nutBlock.name;

			
			if(refPlan.recordTypeDeveloperName == 'Nutrition_Reference_Plan') {
				newBlock.Trainer_Block_Calories__c	= nutBlock.calories;
				newBlock.Trainer_Block_Carbs__c		= nutBlock.carbs;
				newBlock.Trainer_Block_Fat__c		= nutBlock.fat;
				newBlock.Trainer_Block_Protein__c	= nutBlock.protein;
			} else if(refPlan.recordTypeDeveloperName == 'Nutrition_Client_Plan') {
				newBlock.Client_Block_Calories__c	= nutBlock.calories;
				newBlock.Client_Block_Carbs__c		= nutBlock.carbs;
				newBlock.Client_Block_Fat__c		= nutBlock.fat;
				newBlock.Client_Block_Protein__c	= nutBlock.protein;
			}

			newBlock.Template_Block_v2__c		= nutBlock.referenceId;
			newBlock.Template_Block_Calories__c	= nutBlock.referenceCalories;
			newBlock.Template_Block_Carbs__c	= nutBlock.referenceCarbs;
			newBlock.Template_Block_Fat__c		= nutBlock.referenceFat;
			newBlock.Template_Block_Protein__c	= nutBlock.referenceProtein;
			newBlock.Display_Order__c			= nutBlock.order;

			nBlocks.add(newBlock);

		}
		insert nBlocks;

		//Create the related items for the blocks
		List<Item__c> newItems = new List<Item__c>();
		Integer blockIndex = 0;
		for(AT_M_Nutrition.NutritionBlock nutBlock : newBlocks) {
			for(AT_M_Nutrition.NutritionItem nutItem : nutBlock.items) {
				
				Item__c newItem = new Item__c();
				newItem.RecordTypeId					= itemRecordTypeId;
				newItem.Name							= nutItem.name;
				newItem.Block__c						= nBlocks[blockIndex].Id;
				
				if(refPlan.recordTypeDeveloperName == 'Nutrition_Reference_Plan') {
					newItem.Trainer_Calories__c			= nutItem.calories;
					newItem.Trainer_Carbs__c			= nutItem.carbs;
					newItem.Trainer_Fat__c				= nutItem.fat;
					newItem.Trainer_Protein__c			= nutItem.protein;
					newItem.Trainer_Quantity__c			= nutItem.quantity;
				} else if(refPlan.recordTypeDeveloperName == 'Nutrition_Client_Plan') {
					newItem.Client_Calories__c			= nutItem.calories;
					newItem.Client_Carbs__c				= nutItem.carbs;
					newItem.Client_Fat__c				= nutItem.fat;
					newItem.Client_Protein__c			= nutItem.protein;
					newItem.Client_Quantity__c			= nutItem.quantity;
				}

				newItem.Display_Order__c				= nutItem.order;
				newItem.Template_Item_Id__c				= nutItem.referenceId;
				newItem.Template_Block_Item__c			= nutItem.referenceItemBlockId;
				newItem.Template_Item_Name__c			= nutItem.referenceItemName;
				newItem.Template_Quantity__c			= nutItem.referenceQuantity;
				newItem.Template_Calories__c			= nutItem.referenceCalories;
				newItem.Template_Carbs__c				= nutItem.referenceCarbs;
				newItem.Template_Fat__c					= nutItem.referenceFat;
				newItem.Template_Protein__c				= nutItem.referenceProtein;
				newItem.Template_Measurement__c			= nutItem.referenceMeasurement;
				newItem.Template_Quantity_Increment__c	= nutItem.referenceQuantityIncrement;
				newItem.Template_Minimum_Quantity__c	= nutItem.referenceMinimumQuantity;
				newItems.add(newItem);

			}
			blockIndex += 1;
		}

		insert newItems;

			

	}
	
	/**
	* @description Get the Nutritional Reference Plan information
	* @param refPlanId The Id of the Nutritional Reference Plan
	* @return Returns the Nutritional Reference Plan related to the Id
	*/ 
	@AuraEnabled
	public static AT_M_Nutrition.NutritionPlan GetNutritionalPlan(String refPlanId) {
        AT_M_Nutrition.NutritionPlan nPlan = AT_M_Nutrition.QueryNutritionPlans(new Id[] { refPlanId })[0];
        return nPlan;
	}

    @AuraEnabled
    public static Id SaveNewPlan(AT_M_Nutrition.NutritionPlan nPlan) {
		return AT_M_Nutrition.SaveAsNewPlan(nPlan, nPlan.recordTypeId).Id;
    }

	@AuraEnabled 
	public static void UpdatePlan(AT_M_Nutrition.NutritionPlan refPlan, List<String> deletedBlockIds, List<String> deletedItemIds) {

		//Delete the blocks and items
		DeleteBlocks(deletedBlockIds);
		DeleteItems(deletedItemIds);

		//Update the name, and totals of the plan
		UpdatePlanData(refPlan);

		//Separate the old and new 
		List<AT_M_Nutrition.NutritionBlock> oldBlocks = new List<AT_M_Nutrition.NutritionBlock>();
		List<AT_M_Nutrition.NutritionBlock> newBlocks = new List<AT_M_Nutrition.NutritionBlock>();
		
		for(AT_M_Nutrition.NutritionBlock rBlock : refPlan.blocks) {
			if(rBlock.id == null) {
				newBlocks.add(rBlock);
			} else {
				oldBlocks.add(rBlock);
			}
		}

		//Update the old blocks, and add the new ones
		UpdateBlocks(refPlan, oldBlocks);
		AddBlocks(refPlan, newBlocks);

	}

	/**
	* @description Save the nutrition block as a template block
	* @param nutRefBlock The Nutrition reference block data
	*/ 
	@AuraEnabled
	public static void SaveNutritionBlockAsTemplateBlock(AT_M_Nutrition.NutritionBlock nutRefBlock) {

		Id templateBlockRecordId		= GetNutritionTemplateBlockRecordTypeId();
		Id templateBlockItemRecordId	= GetNutritionTemplateBlockItemRecordTypeId();

		Template_Block__c newTemplateBlock			= new Template_Block__c();
		newTemplateBlock.RecordTypeId				= templateBlockRecordId;
		newTemplateBlock.Name						= nutRefBlock.name;
		newTemplateBlock.Template_Block_Protein__c	= nutRefBlock.protein;
		newTemplateBlock.Template_Block_Carbs__c	= nutRefBlock.carbs;
		newTemplateBlock.Template_Block_Fat__c		= nutRefBlock.fat;
		newTemplateBlock.Template_Block_Calories__c = nutRefBlock.calories;

		insert newTemplateBlock;
		
		List<Template_Block_Item__c> newTemplateBlockItems = new List<Template_Block_Item__c>();

		for(AT_M_Nutrition.NutritionItem nutRefItem : nutRefBlock.items) {
			Template_Block_Item__c newTemplateBlockItem = new Template_Block_Item__c();
			newTemplateBlockItem.RecordTypeId			= templateBlockItemRecordId;
			newTemplateBlockItem.Quantity__c			= nutRefItem.quantity;
			newTemplateBlockItem.Display_Order__c		= nutRefItem.order;
			newTemplateBlockItem.Template_Block__c		= newTemplateBlock.Id;
			newTemplateBlockItem.Template_Item__c		= nutRefItem.referenceId;
			newTemplateBlockItems.add(newTemplateBlockItem);
		}

		if(newTemplateBlockItems.size() > 0) {
			insert newTemplateBlockItems;
		}

	}

	/**
	* @description Get Target Information
	* @param targetId The Id of the target
	*/ 
	@AuraEnabled
	public static List<AT_M_Nutrition.NutritionTarget> GetTargetInformation(String[] targetIds) {
		return AT_M_Nutrition.GetTargets(targetIds);
	}


}