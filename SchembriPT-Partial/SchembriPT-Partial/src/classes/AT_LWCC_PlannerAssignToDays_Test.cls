@IsTest
public without sharing class AT_LWCC_PlannerAssignToDays_Test  {

	private static Account testTool_createPersonAccount() {
	
		Id pAccRecType = [SELECT Id FROM RecordType WHERE IsPersonType = true AND SobjectType = 'Account'].Id;

        Account acc	= new Account(
			RecordTypeID				= pAccRecType,
			FirstName					= 'Test',
			LastName					= 'Account',
			PersonMailingStreet			= 'test@yahoo.com',
			PersonMailingPostalCode		= '12345',
			PersonMailingCity			= 'SFO',
			PersonEmail					= 'test@yahoo.com',
			PersonHomePhone				= '1234567',
			PersonMobilePhone			= '12345678',
			SF_Community_User__pc		= UserInfo.getUserId()
        );

        insert acc;
		
		return acc;
	}

	private static User testTool_createPortalUser(Account personAccount) {
	
		Account acc = [
			SELECT
				Id,
				PersonContactId
			FROM Account
			WHERE Id = :personAccount.Id
		];

		Profile p = [SELECT Id FROM Profile WHERE Name='SPT Full Member']; 
        User u = new User(
			Alias = 'standt', 
			Email='aaaaaaa@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='aaaaaaaa@testorg.com'
		);
		u.ContactId = acc.PersonContactId;
		insert u;

		return u;
	}

	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact testTool_updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		//testContact.Email				= 'test@tst.com';
		//testContact.Phone				= '0489123345';
		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	private static Case testTool_createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		
		//Create a Product for the case
		Programs_Products__c prod			= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 12;
		prod.Check_In_Frequency__c	= '1 Week';
		prod.Name					= 'Body Revolt'; //I need to do this for the process builder
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;


		return testProgram;
	}

	private static RecordType testTool_getNutritionalReferencePlanRecordType() {
		return [ SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Reference_Plan'];
	}

	private static RecordType testTool_getNutritionalSessionPlanRecordType() {
		return [ SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'];
	}

	@IsTest
	private static void GetNutritionReferencePlans_GetReferencePlans() {
	
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);

		Check_In__c checkIn					= new Check_In__c();
		checkIn.h_Test_Record__c			= true;
		checkIn.Check_In_Number_v2__c		= 0;
		checkIn.Stage__c					= 'Pending';
		checkIn.Scheduled_Check_In_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c			= personCase.Id;
		checkIn.Contact__c					= personContact.Id;
		insert checkIn;

		Plan__c refPlan			= new Plan__c();
		refPlan.Check_In__c		= checkIn.Id;
		refPlan.RecordTypeId	= testTool_getNutritionalReferencePlanRecordType().Id;
		insert refPlan;
		
		Plan__c sessPlan		= new Plan__c();
		sessPlan.Check_In__c	= checkIn.Id;
		sessPlan.RecordTypeId	= testTool_getNutritionalSessionPlanRecordType().Id;
		insert sessPlan;


		Test.startTest();
		List<Plan__c> refPlans = AT_LWCC_PlannerAssignToDays.GetNutritionReferencePlans(checkIn.Id);
		Test.stopTest();

		System.assertEquals(1, refPlans.size());
		System.assertEquals(refPlan.Id, refPlans.get(0).Id);

	}
	
	@IsTest
	private static void AssignDaysToReferencePlans_AssignAllDaysDifferentReferencePlans() {
	
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);

		Check_In__c checkIn					= new Check_In__c();
		checkIn.h_Test_Record__c			= true;
		checkIn.Check_In_Number_v2__c		= 0;
		checkIn.Stage__c					= 'Pending';
		checkIn.Scheduled_Check_In_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c			= personCase.Id;
		checkIn.Contact__c					= personContact.Id;
		insert checkIn;

		RecordType refPlanRecType = testTool_getNutritionalReferencePlanRecordType();

		Plan__c refPlanMonday			= new Plan__c();
		refPlanMonday.Check_In__c		= checkIn.Id;
		refPlanMonday.RecordTypeId		= refPlanRecType.Id;
		
		Plan__c refPlanTuesday			= new Plan__c();
		refPlanTuesday.Check_In__c		= checkIn.Id;
		refPlanTuesday.RecordTypeId		= refPlanRecType.Id;
		
		Plan__c refPlanWednesday		= new Plan__c();
		refPlanWednesday.Check_In__c	= checkIn.Id;
		refPlanWednesday.RecordTypeId	= refPlanRecType.Id;
		
		Plan__c refPlanThursday			= new Plan__c();
		refPlanThursday.Check_In__c		= checkIn.Id;
		refPlanThursday.RecordTypeId	= refPlanRecType.Id;
		
		Plan__c refPlanFriday			= new Plan__c();
		refPlanFriday.Check_In__c		= checkIn.Id;
		refPlanFriday.RecordTypeId		= refPlanRecType.Id;
		
		Plan__c refPlanSaturday			= new Plan__c();
		refPlanSaturday.Check_In__c		= checkIn.Id;
		refPlanSaturday.RecordTypeId	= refPlanRecType.Id;
		
		Plan__c refPlanSunday			= new Plan__c();
		refPlanSunday.Check_In__c		= checkIn.Id;
		refPlanSunday.RecordTypeId		= refPlanRecType.Id;

		Plan__c[] refPlans = new Plan__c[] {
			refPlanMonday,
			refPlanTuesday,
			refPlanWednesday,
			refPlanThursday,
			refPlanFriday,
			refPlanSaturday,
			refPlanSunday
		};
		insert refPlans;

		Map<String, String> selDays = new Map<String, String>{
			'Monday'	=> refPlanMonday.Id,
			'Tuesday'	=> refPlanTuesday.Id,
			'Wednesday' => refPlanWednesday.Id,
			'Thursday'	=> refPlanThursday.Id,
			'Friday'	=> refPlanFriday.Id,
			'Saturday'	=> refPlanSaturday.Id,
			'Sunday'	=> refPlanSunday.Id
		};

		Test.startTest();
		AT_LWCC_PlannerAssignToDays.AssignDaysToReferencePlans(checkIn.Id, selDays);
		Test.stopTest();

		refPlanMonday		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanMonday.Id];
		refPlanTuesday		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanTuesday.Id];
		refPlanWednesday	= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanWednesday.Id];
		refPlanThursday		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanThursday.Id];
		refPlanFriday		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanFriday.Id];
		refPlanSaturday		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanSaturday.Id];
		refPlanSunday		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlanSunday.Id];

		System.assertEquals('Monday',		refPlanMonday.Weekdays__c);
		System.assertEquals('Tuesday',		refPlanTuesday.Weekdays__c);
		System.assertEquals('Wednesday',	refPlanWednesday.Weekdays__c);
		System.assertEquals('Thursday',		refPlanThursday.Weekdays__c);
		System.assertEquals('Friday',		refPlanFriday.Weekdays__c);
		System.assertEquals('Saturday',		refPlanSaturday.Weekdays__c);
		System.assertEquals('Sunday',		refPlanSunday.Weekdays__c);

	}
	
	@IsTest
	private static void AssignDaysToReferencePlans_AssignAllDaysSameReferencePlan() {
	
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);

		Check_In__c checkIn					= new Check_In__c();
		checkIn.h_Test_Record__c			= true;
		checkIn.Check_In_Number_v2__c		= 0;
		checkIn.Stage__c					= 'Pending';
		checkIn.Scheduled_Check_In_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c			= personCase.Id;
		checkIn.Contact__c					= personContact.Id;
		insert checkIn;

		RecordType refPlanRecType = testTool_getNutritionalReferencePlanRecordType();

		Plan__c refPlan			= new Plan__c();
		refPlan.Check_In__c		= checkIn.Id;
		refPlan.RecordTypeId	= refPlanRecType.Id;
		
		insert refPlan;

		Map<String, String> selDays = new Map<String, String>{
			'Monday'	=> refPlan.Id,
			'Tuesday'	=> refPlan.Id,
			'Wednesday' => refPlan.Id,
			'Thursday'	=> refPlan.Id,
			'Friday'	=> refPlan.Id,
			'Saturday'	=> refPlan.Id,
			'Sunday'	=> refPlan.Id
		};

		Test.startTest();
		AT_LWCC_PlannerAssignToDays.AssignDaysToReferencePlans(checkIn.Id, selDays);
		Test.stopTest();

		refPlan		= [SELECT Id, Weekdays__c FROM Plan__c WHERE Id = :refPlan.Id];

		System.assertEquals('Sunday;Monday;Tuesday;Wednesday;Thursday;Friday;Saturday', refPlan.Weekdays__c);

	}
}