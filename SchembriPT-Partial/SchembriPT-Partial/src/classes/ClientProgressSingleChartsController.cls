/**
* @author Harrison Campbell
* @description Controller for the Client Progress Component
*/ 
global without sharing class ClientProgressSingleChartsController  {

	global class ChartUrls {

		@AuraEnabled
		global String weight { get; set; }

		@AuraEnabled
		global String chest { get; set; }

		@AuraEnabled
		global String waist { get; set; }

		@AuraEnabled
		global String hips { get; set; }

		@AuraEnabled
		global String thigh { get; set; }

		@AuraEnabled
		global String calorie { get; set; }

	}

	global class AllMetrics {

		@AuraEnabled
		global MetricType weight { get; set; }

		@AuraEnabled
		global MetricType chest { get; set; }

		@AuraEnabled
		global MetricType waist { get; set; }

		@AuraEnabled
		global MetricType hips { get; set; }

		@AuraEnabled
		global MetricType thigh { get; set; }

	}
	
	global class MetricType { 
	
		@AuraEnabled
		global MetricValue latest { get; set; }

		@AuraEnabled
		global MetricValue total { get; set; }


	}

	global class MetricValue {

		@AuraEnabled
		global String value { get; set; }

		@AuraEnabled
		global String valueClass { get; set; }

		@AuraEnabled
		global String valueStyle { get; set; }

		global MetricValue(Decimal newValue) {

			value = newValue.toPlainString();

			if(newValue > 0) {
				valueClass = 'value-increase';
				valueStyle = 'width: 50px; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 700; color: #45c445;';
			} else if(newValue < 0) {
				valueClass = 'value-decrease';
				valueStyle = 'width: 50px; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 700; color: #c42323;';
			} else {
				valueClass = 'value-same';
				valueStyle = 'width: 50px; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 700; color: black;';
			}

		}


	}

	global class ProgressInit {
	
		@AuraEnabled
		global Integer periodValue { get; set; }

		@AuraEnabled
		global String periodType { get; set; }
		
		@AuraEnabled
		global ChartUrls charts { get; set; }

		@AuraEnabled
		global AllMetrics metrics { get; set; }

	}

	global class ProgressData { 
	
		@AuraEnabled
		global Integer periodValue { get; set; }

		@AuraEnabled
		global String periodType { get; set; }

		@AuraEnabled
		global String[] dateLabels { get; set; }

		@AuraEnabled
		global Decimal[] weightData { get; set; }
		
		@AuraEnabled
		global Decimal[] chestData { get; set; }
		
		@AuraEnabled
		global Decimal[] waistData { get; set; }
		
		@AuraEnabled
		global Decimal[] hipsData { get; set; }
		
		@AuraEnabled
		global Decimal[] thighData { get; set; }
		
		@AuraEnabled
		global Decimal[] calorieData { get; set; }
		
		@AuraEnabled
		global Decimal minWeightValue { get; set; }
		
		@AuraEnabled
		global Decimal maxWeightValue { get; set; }
		
		@AuraEnabled
		global Decimal minChestValue { get; set; }
		
		@AuraEnabled
		global Decimal maxChestValue { get; set; }
		
		@AuraEnabled
		global Decimal minWaistValue { get; set; }
		
		@AuraEnabled
		global Decimal maxWaistValue { get; set; }
		
		@AuraEnabled
		global Decimal minHipsValue { get; set; }
		
		@AuraEnabled
		global Decimal maxHipsValue { get; set; }
		
		@AuraEnabled
		global Decimal minThighValue { get; set; }
		
		@AuraEnabled
		global Decimal maxThighValue { get; set; }

		@AuraEnabled
		global Decimal maxCalorieValue { get; set; }

	}


	private static void addMinMaxValues(ProgressData pData) {
	
		pData.minWeightValue	= 2147483647.0;
		pData.maxWeightValue	= -2147483647.0;
		
		pData.minChestValue		= 2147483647.0;
		pData.maxChestValue		= -2147483647.0;
		
		pData.minWaistValue		= 2147483647.0;
		pData.maxWaistValue		= -2147483647.0;
		
		pData.minHipsValue		= 2147483647.0;
		pData.maxHipsValue		= -2147483647.0;
		
		pData.minThighValue		= 2147483647.0;
		pData.maxThighValue		= -2147483647.0;
		
		pData.maxCalorieValue	= -2147483647.0;


		for(Integer i = 0; i < pData.weightData.size(); i++) {

			pData.minWeightValue	= Math.min(pData.minWeightValue, pData.weightData[i]);
			pData.maxWeightValue	= Math.max(pData.maxWeightValue, pData.weightData[i]);
			
			pData.minChestValue		= Math.min(pData.minChestValue, pData.chestData[i]);
			pData.maxChestValue		= Math.max(pData.maxChestValue, pData.chestData[i]);

			pData.minWaistValue		= Math.min(pData.minWaistValue, pData.waistData[i]);
			pData.maxWaistValue		= Math.max(pData.maxWaistValue, pData.waistData[i]);

			pData.minHipsValue		= Math.min(pData.minHipsValue, pData.hipsData[i]);
			pData.maxHipsValue		= Math.max(pData.maxHipsValue, pData.hipsData[i]);

			pData.minThighValue		= Math.min(pData.minThighValue, pData.thighData[i]);
			pData.maxThighValue		= Math.max(pData.maxThighValue, pData.thighData[i]);

			pData.maxCalorieValue	= Math.max(pData.maxCalorieValue, (pData.calorieData[i] != null) ? pData.calorieData[i] : 0);


		}
		
		pData.minWeightValue	-= 10;
		pData.minChestValue		-= 10;
		pData.minWaistValue		-= 10;
		pData.minHipsValue		-= 10;
		pData.minThighValue		-= 10;
	
		
		pData.maxWeightValue	+= 10;
		pData.maxChestValue		+= 10;
		pData.maxWaistValue		+= 10;
		pData.maxHipsValue		+= 10;
		pData.maxThighValue		+= 10;
		
		pData.minWeightValue	/= 10;
		pData.minChestValue		/= 10;
		pData.minWaistValue		/= 10;
		pData.minHipsValue		/= 10;
		pData.minThighValue		/= 10;
		pData.maxWeightValue	/= 10;
		pData.maxChestValue		/= 10;
		pData.maxWaistValue		/= 10;
		pData.maxHipsValue		/= 10;
		pData.maxThighValue		/= 10;
		
		pData.minWeightValue	= pData.minWeightValue.setScale(0,	RoundingMode.FLOOR);
		pData.minChestValue		= pData.minChestValue.setScale(0,	RoundingMode.FLOOR);
		pData.minWaistValue		= pData.minWaistValue.setScale(0,	RoundingMode.FLOOR);
		pData.minHipsValue		= pData.minHipsValue.setScale(0,	RoundingMode.FLOOR);
		pData.minThighValue		= pData.minThighValue.setScale(0,	RoundingMode.FLOOR);
		pData.maxWeightValue	= pData.maxWeightValue.setScale(0,	RoundingMode.CEILING);
		pData.maxChestValue		= pData.maxChestValue.setScale(0,	RoundingMode.CEILING);
		pData.maxWaistValue		= pData.maxWaistValue.setScale(0,	RoundingMode.CEILING);
		pData.maxHipsValue		= pData.maxHipsValue.setScale(0,	RoundingMode.CEILING);
		pData.maxThighValue		= pData.maxThighValue.setScale(0,	RoundingMode.CEILING);
		
		pData.minWeightValue	*= 10;
		pData.minChestValue		*= 10;
		pData.minWaistValue		*= 10;
		pData.minHipsValue		*= 10;
		pData.minThighValue		*= 10;
		pData.maxWeightValue	*= 10;
		pData.maxChestValue		*= 10;
		pData.maxWaistValue		*= 10;
		pData.maxHipsValue		*= 10;
		pData.maxThighValue		*= 10;

		//Calorie
		pData.maxCalorieValue	+= 1000;
		pData.maxCalorieValue	/= 1000;
		pData.maxCalorieValue	= pData.maxCalorieValue.setScale(0, RoundingMode.CEILING);
		pData.maxCalorieValue	*= 1000;

	}

	@AuraEnabled
	global static ProgressInit InitProgressCharts(Id checkInId, String range) {

		Check_In__c cCheckIn = [
			SELECT
				Id,
				Stage__c,
				Scheduled_Check_In_Date__c,
				Contact__c,
				Super_Coach__c,
				Weight__c,
				Chest_Measurement__c,
				Waist_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,
				Starting_Weight_Difference__c,
				Starting_Chest_Difference__c,
				Starting_Waist_Difference__c,
				Starting_Hips_Difference__c,
				Starting_Thigh_Difference__c,
				Weight_Difference__c,
				Chest_Difference__c,
				Waist_Difference__c,
				Hip_Difference__c,
				Thigh_Difference__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];
		
		Contact cont = [
			SELECT
				Id,
				FirstName,
				AccountId
			FROM Contact
			WHERE Id = :cCheckIn.Contact__c
		];

		//Get the Previous Check-In, if this Check-In is still in the 'Pending' or 'Client Information' stages
		if(cCheckIn.Stage__c == 'Pending' || cCheckIn.Stage__c == 'Client Information') {
			cCheckIn = [
				SELECT
					Id,
					Stage__c,
					Contact__c,
					Scheduled_Check_In_Date__c,
					Super_Coach__c,
					Weight__c,
					Chest_Measurement__c,
					Waist_Measurement__c,
					Hip_Measurement__c,
					Thigh_Measurement__c,
					Starting_Weight_Difference__c,
					Starting_Chest_Difference__c,
					Starting_Waist_Difference__c,
					Starting_Hips_Difference__c,
					Starting_Thigh_Difference__c,
					Weight_Difference__c,
					Chest_Difference__c,
					Waist_Difference__c,
					Hip_Difference__c,
					Thigh_Difference__c
				FROM Check_In__c
				WHERE Scheduled_Check_In_Date__c != null AND Scheduled_Check_In_Date__c < :cCheckIn.Scheduled_Check_In_Date__c AND Account__c = :cont.AccountId
				ORDER BY Scheduled_Check_In_Date__c DESC 
				LIMIT 1
			];
		}

		ChartUrls charts = loadCharts(cont, range, cCheckIn.Scheduled_Check_In_Date__c);

		AllMetrics metrics = loadMetrics(cCheckIn);
		
		ProgressInit progData	= new ProgressInit();
		progData.charts			= charts;
		progData.metrics		= metrics;
		
		
		List<Check_In__c> compChecks = ClientFeedbackCharts.GetCompletedCheckIns(cont.AccountId, range, cCheckIn.Scheduled_Check_In_Date__c);
		
		
		if(compChecks.size() > 1) {
		
			Integer periodValue = compChecks.get(0).Scheduled_Check_In_Date__c.daysBetween(compChecks.get(1).Scheduled_Check_In_Date__c);
		
			if(periodValue >= 30) {
		
				Integer months			= periodValue / 30;
				progData.periodType		= 'Mths';
				progData.periodValue	= months;
			} else {
		
				Integer weeks			= periodValue / 7;
				progData.periodType		= 'Weeks';
				progData.periodValue	= weeks;

			}
		
		}
		
		return progData;


	}

	@AuraEnabled
	global static ProgressData GetData(Id checkInId, String range) {
	
		Check_In__c cCheckIn = [
			SELECT
				Id,
				Stage__c,
				Contact__c,
				Scheduled_Check_In_Date__c,
				Super_Coach__c,
				Weight__c,
				Chest_Measurement__c,
				Waist_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,
				Starting_Weight_Difference__c,
				Starting_Chest_Difference__c,
				Starting_Waist_Difference__c,
				Starting_Hips_Difference__c,
				Starting_Thigh_Difference__c,
				Weight_Difference__c,
				Chest_Difference__c,
				Waist_Difference__c,
				Hip_Difference__c,
				Thigh_Difference__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		Contact cont = [
			SELECT
				Id,
				FirstName,
				AccountId
			FROM Contact
			WHERE Id = :cCheckIn.Contact__c
		];

		//If the CheckIn stage is set to 'Pending' or 'Client Information', then use the previous check In instead
		if(cCheckIn.Stage__c == 'Pending' || cCheckIn.Stage__c == 'Client Information') {
			cCheckIn = [
				SELECT
					Id,
					Stage__c,
					Contact__c,
					Scheduled_Check_In_Date__c,
					Super_Coach__c,
					Weight__c,
					Chest_Measurement__c,
					Waist_Measurement__c,
					Hip_Measurement__c,
					Thigh_Measurement__c,
					Starting_Weight_Difference__c,
					Starting_Chest_Difference__c,
					Starting_Waist_Difference__c,
					Starting_Hips_Difference__c,
					Starting_Thigh_Difference__c,
					Weight_Difference__c,
					Chest_Difference__c,
					Waist_Difference__c,
					Hip_Difference__c,
					Thigh_Difference__c
				FROM Check_In__c
				WHERE Scheduled_Check_In_Date__c != null AND Scheduled_Check_In_Date__c < :cCheckIn.Scheduled_Check_In_Date__c AND Account__c = :cont.AccountId
				ORDER BY Scheduled_Check_In_Date__c DESC 
				LIMIT 1
			];
		} 


		System.debug('Account: ' + cont.AccountId);
		System.debug('Latest Check-In: ' + cCheckIn.Id);

		ClientFeedback_Chart weightChart	= ClientFeedbackCharts.GetWeightChart(cont.AccountId,	range, cCheckIn.Scheduled_Check_In_Date__c);
		ClientFeedback_Chart chestChart		= ClientFeedbackCharts.GetChestChart(cont.AccountId,	range, cCheckIn.Scheduled_Check_In_Date__c);
		ClientFeedback_Chart waistChart		= ClientFeedbackCharts.GetWaistChart(cont.AccountId,	range, cCheckIn.Scheduled_Check_In_Date__c);
		ClientFeedback_Chart hipsChart		= ClientFeedbackCharts.GetHipChart(cont.AccountId,		range, cCheckIn.Scheduled_Check_In_Date__c);
		ClientFeedback_Chart thighChart		= ClientFeedbackCharts.GetThighChart(cont.AccountId,	range, cCheckIn.Scheduled_Check_In_Date__c);
		ClientFeedback_Chart calorieChart	= ClientFeedbackCharts.GetCalorieChart(cont.AccountId,	range, cCheckIn.Scheduled_Check_In_Date__c);
		
		ProgressData pData	= new ProgressData();
		pData.dateLabels	= weightChart.chart.data.labels;
		pData.weightData	= weightChart.chart.data.datasets.get(0).data;
		pData.chestData		= chestChart.chart.data.datasets.get(0).data;
		pData.waistData		= waistChart.chart.data.datasets.get(0).data;
		pData.hipsData		= hipsChart.chart.data.datasets.get(0).data;
		pData.thighData		= thighChart.chart.data.datasets.get(0).data;
		pData.calorieData	= calorieChart.chart.data.datasets.get(0).data;

		addMinMaxValues(pData);
		

		List<Check_In__c> compChecks	= ClientFeedbackCharts.GetCompletedCheckIns(cont.AccountId, range, cCheckIn.Scheduled_Check_In_Date__c);

		if(compChecks.size() > 1) {
			Integer periodValue				= compChecks.get(0).Scheduled_Check_In_Date__c.daysBetween(compChecks.get(1).Scheduled_Check_In_Date__c);
			if(periodValue >= 30) {
				Integer months			= periodValue / 30;
				pData.periodType		= 'Mths';
				pData.periodValue	= months;
			} else {
				Integer weeks			= periodValue / 7;
				pData.periodType		= 'Weeks';
				pData.periodValue	= weeks;
			}
		}

		return pData;


	}
	
	private static Check_In__c GetLatestCheckIn(Contact cont) {

		String[] photoTypes = new String[] {
			'Front',
			'Side',
			'Back'
		};

		return [
			SELECT
				Id,
				Super_Coach__c,

				Weight__c,
				Chest_Measurement__c,
				Waist_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,

				Starting_Weight_Difference__c,
				Starting_Chest_Difference__c,
				Starting_Waist_Difference__c,
				Starting_Hips_Difference__c,
				Starting_Thigh_Difference__c,

				Weight_Difference__c,
				Chest_Difference__c,
				Waist_Difference__c,
				Hip_Difference__c,
				Thigh_Difference__c,

				(
					SELECT
						Id,
						Type__c,
						Dropbox_Path__c
					FROM Photos__r
					WHERE Type__c IN :photoTypes
					Order BY Timestamp__c DESC
				)

			FROM Check_In__c
			WHERE Account__c = :cont.AccountId
			ORDER BY Scheduled_Check_In_Date__c DESC NULLS LAST
			LIMIT 1
		];

	}
	
	private Check_In__c GetPreviousCheckIn(Contact cont) {
	
		String[] photoTypes = new String[] {
			'Front',
			'Side',
			'Back'
		};

		return [
			SELECT
				Id,
				Super_Coach__c,

				Weight__c,
				Chest_Measurement__c,
				Waist_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,

				Starting_Weight_Difference__c,
				Starting_Chest_Difference__c,
				Starting_Waist_Difference__c,
				Starting_Hips_Difference__c,
				Starting_Thigh_Difference__c,

				Weight_Difference__c,
				Chest_Difference__c,
				Waist_Difference__c,
				Hip_Difference__c,
				Thigh_Difference__c,

				(
					SELECT
						Id,
						Type__c,
						Dropbox_Path__c
					FROM Photos__r
					WHERE Type__c IN :photoTypes
					Order BY Timestamp__c DESC
				)

			FROM Check_In__c
			WHERE Account__c = :cont.AccountId AND Stage__c = 'Completed'
			ORDER BY Scheduled_Check_In_Date__c DESC NULLS LAST
			LIMIT 1 OFFSET 1
		];

	}
	
	/**
	* @description Load Chart URLs
	*/ 
	private static ChartUrls loadCharts(Contact cont, String range, Date latestDate) {

		ChartUrls charts = new ChartUrls();

		Integer chartWidth	= 175;
		Integer chartHeight = 100;
	
		ClientFeedback_Chart weightChart	= ClientFeedbackCharts.GetWeightChart(cont.AccountId,	range, latestDate);
		ClientFeedback_Chart chestChart		= ClientFeedbackCharts.GetChestChart(cont.AccountId,	range, latestDate);
		ClientFeedback_Chart waistChart		= ClientFeedbackCharts.GetWaistChart(cont.AccountId,	range, latestDate);
		ClientFeedback_Chart hipsChart		= ClientFeedbackCharts.GetHipChart(cont.AccountId,		range, latestDate);
		ClientFeedback_Chart thighChart		= ClientFeedbackCharts.GetThighChart(cont.AccountId,	range, latestDate);
		ClientFeedback_Chart calorieChart	= ClientFeedbackCharts.GetCalorieChart(cont.AccountId,	range, latestDate);
		
		Integer xTickSize = 14;
		Integer yTickSize = 16;

		//Adjust some of the ticks
		weightChart.chart.options.scales.yAxes.get(0).ticks.fontSize	= yTickSize;
		chestChart.chart.options.scales.yAxes.get(0).ticks.fontSize		= yTickSize;
		waistChart.chart.options.scales.yAxes.get(0).ticks.fontSize		= yTickSize;
		hipsChart.chart.options.scales.yAxes.get(0).ticks.fontSize		= yTickSize;
		thighChart.chart.options.scales.yAxes.get(0).ticks.fontSize		= yTickSize;
		calorieChart.chart.options.scales.yAxes.get(0).ticks.fontSize	= yTickSize;


		weightChart.chart.options.scales.xAxes.get(0).ticks.fontSize	= xTickSize;
		chestChart.chart.options.scales.xAxes.get(0).ticks.fontSize		= xTickSize;
		waistChart.chart.options.scales.xAxes.get(0).ticks.fontSize		= xTickSize;
		hipsChart.chart.options.scales.xAxes.get(0).ticks.fontSize		= xTickSize;
		thighChart.chart.options.scales.xAxes.get(0).ticks.fontSize		= xTickSize;
		calorieChart.chart.options.scales.xAxes.get(0).ticks.fontSize	= xTickSize;

		//Adjust the title size
		Integer titleFontSize		= 18;
		Integer titleFontPadding	= 12;

		weightChart.chart.options.title.fontSize						= titleFontSize;
		weightChart.chart.options.title.padding							= titleFontPadding;

		chestChart.chart.options.title.fontSize							= titleFontSize;
		chestChart.chart.options.title.padding							= titleFontPadding;

		waistChart.chart.options.title.fontSize							= titleFontSize;
		waistChart.chart.options.title.padding							= titleFontPadding;

		hipsChart.chart.options.title.fontSize							= titleFontSize;
		hipsChart.chart.options.title.padding							= titleFontPadding;

		thighChart.chart.options.title.fontSize							= titleFontSize;
		thighChart.chart.options.title.padding							= titleFontPadding;

		//Adjust the datalabel

		String datalabelFontSize		= '14';
		String datalabelSmallerFontSize = '13';
		Integer dataLabelTextStroke = 3;


		weightChart.chart.data.datasets.get(0).datalabels.font.size			= datalabelFontSize;
		weightChart.chart.data.datasets.get(0).datalabels.textStrokeWidth	= dataLabelTextStroke;
		chestChart.chart.data.datasets.get(0).datalabels.font.size			= datalabelSmallerFontSize;
		chestChart.chart.data.datasets.get(0).datalabels.textStrokeWidth	= dataLabelTextStroke;
		waistChart.chart.data.datasets.get(0).datalabels.font.size			= datalabelSmallerFontSize;
		waistChart.chart.data.datasets.get(0).datalabels.textStrokeWidth	= dataLabelTextStroke;
		hipsChart.chart.data.datasets.get(0).datalabels.font.size			= datalabelSmallerFontSize;
		hipsChart.chart.data.datasets.get(0).datalabels.textStrokeWidth		= dataLabelTextStroke;
		thighChart.chart.data.datasets.get(0).datalabels.font.size			= datalabelSmallerFontSize;
		thighChart.chart.data.datasets.get(0).datalabels.textStrokeWidth	= dataLabelTextStroke;



		charts.weight						= JSON.serialize(weightChart.chart,		true);
		charts.chest						= JSON.serialize(chestChart.chart,		true);
		charts.waist						= JSON.serialize(waistChart.chart,		true);
		charts.hips							= JSON.serialize(hipsChart.chart,		true);
		charts.thigh						= JSON.serialize(thighChart.chart,		true);
		charts.calorie						= JSON.serialize(calorieChart.chart,	true);
		
		weightChart.chart.options.scales.yAxes.get(0).ticks.fontSize = 00;


		return charts;

	}

	/**
	* @description Load the key data for each category(weight, chest, etc.)
	* @param latest The Latest CheckIn
	*/ 
	private static AllMetrics loadMetrics(Check_In__c latestCheckIn) {

		AllMetrics metrics = new AllMetrics();

		metrics.weight			= new MetricType();
		metrics.weight.latest	= new MetricValue(latestCheckIn.Weight_Difference__c);
		metrics.weight.total	= new MetricValue(latestCheckIn.Starting_Weight_Difference__c);
		
		metrics.chest			= new MetricType();
		metrics.chest.latest	= new MetricValue(latestCheckIn.Chest_Difference__c);
		metrics.chest.total		= new MetricValue(latestCheckIn.Starting_Chest_Difference__c);

		metrics.waist			= new MetricType();
		metrics.waist.latest	= new MetricValue(latestCheckIn.Waist_Difference__c);
		metrics.waist.total		= new MetricValue(latestCheckIn.Starting_Waist_Difference__c);

		metrics.hips			= new MetricType();
		metrics.hips.latest		= new MetricValue(latestCheckIn.Hip_Difference__c);
		metrics.hips.total		= new MetricValue(latestCheckIn.Starting_Hips_Difference__c);

		metrics.thigh			= new MetricType();
		metrics.thigh.latest	= new MetricValue(latestCheckIn.Thigh_Difference__c);
		metrics.thigh.total		= new MetricValue(latestCheckIn.Starting_Thigh_Difference__c);
	
		return metrics;

	}

}