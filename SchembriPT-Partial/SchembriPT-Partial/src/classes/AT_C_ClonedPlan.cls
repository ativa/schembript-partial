/**
* @author Harrison Campbell
* @description Holds the Plan data that was cloned
*/ 
public class AT_C_ClonedPlan {

	/**
	* @description The Id that is being cloned
	*/ 
	public Id planId { get; set; }

	/**
	* @description The resulting cloned plan Id
	*/ 
	public Id clonedPlanId { get; set; }

}