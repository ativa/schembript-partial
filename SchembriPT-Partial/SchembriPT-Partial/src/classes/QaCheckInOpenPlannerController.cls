/**
* @author Harrison Campbell
* @description Controls the QaCheckInOpenPlanner component
*/ 
public class QaCheckInOpenPlannerController {

	/**
	* @author Harrison Campbell
	* @description Get's any information needed for the Check-In Open Planner Validation
	*/ 
	public class CheckInOpenPlannerValidation {
		
		/**
		* @description Tracks if the Nutrition Plan Start Date was set
		*/ 
		@AuraEnabled
		public Boolean nutritionPlanStartDateSet { get; set; }

	}

	
	/**
	* @description Check's if we can open the Planner
	* @param checkInId The Id of the Check-In
	* @return The areas that are invalid/valid
	*/ 
	@AuraEnabled
	public static CheckInOpenPlannerValidation CheckValidity(String checkInId) {

		Check_In__c currentCheckIn = [
			SELECT
				Id,
				Nutrition_Plan_Start_Date__c
			FROM Check_In__c
			WHERE Id = :checkInId	
		];

		CheckInOpenPlannerValidation checkInValidation	= new CheckInOpenPlannerValidation();
		checkInValidation.nutritionPlanStartDateSet		= currentCheckIn.Nutrition_Plan_Start_Date__c != null;
		return checkInValidation;

	}


}