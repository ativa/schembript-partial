/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    private static final Portal_Settings__c settings = Portal_Settings__c.getInstance();


    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        User memberUser = [
			SELECT
				ContactId,
				AccountId
			FROM User 
			WHERE Id = :UserInfo.getUserId()
		];


        Contact memberAccount = [
			SELECT 
				Onboarding_Complete__c 
			FROM Contact 
			WHERE Id = :memberUser.ContactId
		];

		Account mAccount = [
			SELECT
				Id,
				Community_Portal_Page__c
			FROM Account
			WHERE Id = :memberUser.AccountId
		];

		if(mAccount.Community_Portal_Page__c == 'Main Page') {
			return new PageReference('/s/');
		} else if(mAccount.Community_Portal_Page__c == 'Onboarding (new)') {
			return new PageReference('/s/onboarding');
		} else if(mAccount.Community_Portal_Page__c == 'Restart (>4 wks)') {
			return new PageReference('/s/restart');
		} else if(mAccount.Community_Portal_Page__c == 'Rejoin Recent (<12 mths)') {
			return new PageReference('/s/rejoin');
		} else if(mAccount.Community_Portal_Page__c == 'Rejoin Old (12+ mths)') {
			return new PageReference('/s/rejoin');
		}

		
		return new PageReference('/');

    }

    public CommunitiesLandingController() {}
}