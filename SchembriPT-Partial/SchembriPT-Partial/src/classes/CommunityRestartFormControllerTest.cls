@isTest 
private class CommunityRestartFormControllerTest {

	@TestSetup
	private static void setup() {
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

	}

	/**
	* @description Create a Test Account
	* @return Returns a Test Account
	*/ 
	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		Id clientRecordId			= [SELECT Id FROM RecordType WHERE DeveloperName = 'Client' AND SobjectType = 'Account'].Id;
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		testAccount.PersonEmail		= 'test@tst.com';
		testAccount.Phone			= '0489123345';
		insert testAccount;
		
		return testAccount;

	}

	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact updateTestContact(Account testAccount) {
		//
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		//testContact.Email				= 'test@tst.com';
		//testContact.Phone				= '0489123345';
		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		
		//Create a Product for the case

		Programs_Products__c prod	= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 12;
		prod.Name					= 'Body Revolt'; //I need to do this for the process builder
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;


		return testProgram;
	}

	private static void createTestFormFields() {

		String[] sectionTypes = new String[] {
			'Measurements',
			'Lifestyle',
			'Training',
			'Medical',
			'Other'
		};

		List<Survey_Question__c> survQuestions = new List<Survey_Question__c>();

		Integer index = 0;
		for(String t : sectionTypes) {
			Survey_Question__c newQuestion = new Survey_Question__c(
				Name				= 'Test ' + index,
				Field_Name__c		= 'Field Name ' + index,
				Form_Name__c		= 'Restart',
				Object_Name__c		= 'Case',
				Order__c			= index,
				Section_Name__c		= sectionTypes.get(index)
			);
			survQuestions.add(newQuestion);
			index += 1;
		}
		insert survQuestions;
	}

	private static void createTestPhotoSet() {

		/*CommunityRestartFormController.Photo frontPhoto = new CommunityRestartFormController.Photo();
		frontPhoto.id	= '1';
		frontPhoto.name = 'front';
		
		CommunityRestartFormController.Photo sidePhoto = new CommunityRestartFormController.Photo();
		sidePhoto.id	= '2';
		sidePhoto.name	= 'side';
		
		CommunityRestartFormController.Photo backPhoto = new CommunityRestartFormController.Photo();
		backPhoto.id	= '3';
		backPhoto.name	= 'back';

		List<CommunityRestartFormController.Photo> photos = new List<CommunityRestartFormController.Photo>();
		photos.add(frontPhoto);
		photos.add(sidePhoto);
		photos.add(backPhoto);*/

		Check_In__c check = [
			SELECT 
				Id,
				Photos__c
			FROM Check_In__c
		];

		//check.Photos__c = JSON.serializePretty(photos, true);
		//update check;

		ContentVersion file = new ContentVersion(Title='test', PathOnClient=('test.jpg'),Image_Type__c = 'Photo Front', Check_In__c = check.Id, IsMajorVersion=false, VersionData=Blob.valueOf(' '));
        
        insert file;
            
        file = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :file.Id LIMIT 1];
            
        ContentDocumentLink newLink = new ContentDocumentLink(LinkedEntityId = check.Id, ContentDocumentId = file.ContentDocumentId, ShareType = 'I');
            
        insert newLink;
        
        file = new ContentVersion(Title='test', PathOnClient=('test.jpg'),Image_Type__c = 'Photo Side', Check_In__c = check.Id, IsMajorVersion=false, VersionData=Blob.valueOf(' '));
        
        insert file;
            
        file = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :file.Id LIMIT 1];
            
        newLink = new ContentDocumentLink(LinkedEntityId = check.Id, ContentDocumentId = file.ContentDocumentId, ShareType = 'I');
            
        insert newLink;

        file = new ContentVersion(Title='test', PathOnClient=('test.jpg'),Image_Type__c = 'Photo Back', Check_In__c = check.Id, IsMajorVersion=false, VersionData=Blob.valueOf(' '));
        
        insert file;
            
        file = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :file.Id LIMIT 1];
            
        newLink = new ContentDocumentLink(LinkedEntityId = check.Id, ContentDocumentId = file.ContentDocumentId, ShareType = 'I');
            
        insert newLink;
        
        CommunityRestartFormController.getPhotos(check);
	}

	private static void testTool_assertFormFieldsAreCorrect(CommunityRestartFormController.RestartFormFields formFields) {
	
		System.assertEquals(1,				formFields.measurementFields.size());
		System.assertEquals('Case',			formFields.measurementFields.get(0).Object_Name__c);
		System.assertEquals('Measurements', formFields.measurementFields.get(0).Section_Name__c);
		System.assertEquals('Field Name 0',	formFields.measurementFields.get(0).Field_Name__c);
		
		System.assertEquals(1,				formFields.lifestyleFields.size());
		System.assertEquals('Case',			formFields.lifestyleFields.get(0).Object_Name__c);
		System.assertEquals('Lifestyle',	formFields.lifestyleFields.get(0).Section_Name__c);
		System.assertEquals('Field Name 1',	formFields.lifestyleFields.get(0).Field_Name__c);
		
		System.assertEquals(2,				formFields.trainingAndMedicalFields.size());
		System.assertEquals('Case',			formFields.trainingAndMedicalFields.get(0).Object_Name__c);
		System.assertEquals('Training',		formFields.trainingAndMedicalFields.get(0).Section_Name__c);
		System.assertEquals('Field Name 2',	formFields.trainingAndMedicalFields.get(0).Field_Name__c);
		System.assertEquals('Case',			formFields.trainingAndMedicalFields.get(1).Object_Name__c);
		System.assertEquals('Medical',		formFields.trainingAndMedicalFields.get(1).Section_Name__c);
		System.assertEquals('Field Name 3',	formFields.trainingAndMedicalFields.get(1).Field_Name__c);
		
		System.assertEquals(1,				formFields.otherFields.size());
		System.assertEquals('Case',			formFields.otherFields.get(0).Object_Name__c);
		System.assertEquals('Other',		formFields.otherFields.get(0).Section_Name__c);
		System.assertEquals('Field Name 4',	formFields.otherFields.get(0).Field_Name__c);
		
	}

	private static void testTool_assertPhotosAreCorrect(CommunityRestartFormController.PhotoSet photoS) {
	
		/* Need to update these later
		
		System.assertEquals('1',		photoS.front.id);
		System.assertEquals('front',	photoS.front.name);
		System.assertEquals('2',		photoS.side.id);
		System.assertEquals('side',		photoS.side.name);
		System.assertEquals('3',		photoS.back.id);
		System.assertEquals('back',		photoS.back.name);*/

	}


	@isTest
	private static void getInitialValues_getValues() {
		
		//Create Account, Contact, and Case
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		Check_In__c check		= [SELECT Id FROM Check_In__c];


		//Create the Formfields and the Photoset
		createTestFormFields();
		createTestPhotoSet();

		CommunityRestartFormController.InitialValues initValues = CommunityRestartFormController.getInitialValues(UserInfo.getUserId());

		//Assert the form fields are correct
		testTool_assertFormFieldsAreCorrect(initValues.formFields);

		//Assert the photos are correct
		testTool_assertPhotosAreCorrect(initValues.photoSet);

		//Assert the parent records are correct
		//System.assertEquals(testAccount.Id, initValues.parentRecords.accountId);
		//System.assertEquals(testProgram.Id, initValues.parentRecords.caseId);

	}

	@IsTest
	private static void getInitialValues_NoFormFields() {
	
		//Create Account, Contact, and Case
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		Check_In__c check		= [SELECT Id FROM Check_In__c];


		//Create a Photoset
		createTestPhotoSet();

		CommunityRestartFormController.InitialValues initValues = CommunityRestartFormController.getInitialValues(UserInfo.getUserId());

		//Assert that there are no form fields
		System.assertEquals(0, initValues.formFields.measurementFields.size());
		System.assertEquals(0, initValues.formFields.lifestyleFields.size());
		System.assertEquals(0, initValues.formFields.trainingAndMedicalFields.size());
		System.assertEquals(0, initValues.formFields.otherFields.size());

		//Assert the photos are correct
		testTool_assertPhotosAreCorrect(initValues.photoSet);

		//Assert the parent records are correct
		//System.assertEquals(testAccount.Id, initValues.parentRecords.accountId);
		//System.assertEquals(testProgram.Id, initValues.parentRecords.caseId);

	}
	
	@IsTest
	private static void getInitialValues_NoPhotos() {

		//Create Account, Contact, and Case
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		Check_In__c check		= [SELECT Id FROM Check_In__c];

		//Create the Formfields
		createTestFormFields();

		CommunityRestartFormController.InitialValues initValues = CommunityRestartFormController.getInitialValues(UserInfo.getUserId());

		//Assert the form fields are correct
		testTool_assertFormFieldsAreCorrect(initValues.formFields);

		//Assert there are no photos returned
		System.assertEquals(null, initValues.photoSet);

		//Assert the parent records are correct
		//System.assertEquals(testAccount.Id, initValues.parentRecords.accountId);
		//System.assertEquals(testProgram.Id, initValues.parentRecords.caseId);

	}

	@IsTest
	private static void deletePhoto_deleteFile() {
	
		//Create Account, Contact, and Case
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		Check_In__c check		= [SELECT Id,Photos__c FROM Check_In__c];

		ContentVersion cv	= new Contentversion();
		cv.title			= 'ABC';
		cv.PathOnClient		= 'test';
		cv.versiondata		= EncodingUtil.base64Decode('Unit Test Attachment Body');
		insert cv;
		
		CommunityRestartFormController.Photo frontPhoto = new CommunityRestartFormController.Photo();
		frontPhoto.id	= cv.Id;
		frontPhoto.name = 'front';
		List<CommunityRestartFormController.Photo> photos = new List<CommunityRestartFormController.Photo>();
		//photos.add(frontPhoto);
		//check.Photos__c = JSON.serializePretty(photos, true);
		//update check;

		//Assert that the photo was deleted
		CommunityRestartFormController.deletePhoto(check.Id, cv.Id);
		//check		= [SELECT Id,Photos__c FROM Check_In__c];
		//System.assertEquals('[ ]', check.Photos__c);

		//List<ContentVersion> allVersions = [SELECT Id FROM ContentVersion];
		//System.assertEquals(0, allVersions.size());

	}
	
	@IsTest
	private static void deletePhoto_nullCheckInId() {
		
		//Create Account, Contact, and Case
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		Check_In__c check		= [SELECT Id,Photos__c FROM Check_In__c];

		ContentVersion cv	= new Contentversion();
		cv.title			= 'ABC';
		cv.PathOnClient		= 'test';
		cv.versiondata		= EncodingUtil.base64Decode('Unit Test Attachment Body');
		insert cv;
		
		CommunityRestartFormController.Photo frontPhoto = new CommunityRestartFormController.Photo();
		frontPhoto.id	= cv.Id;
		frontPhoto.name = 'front';
		List<CommunityRestartFormController.Photo> photos = new List<CommunityRestartFormController.Photo>();
		photos.add(frontPhoto);
		check.Photos__c = JSON.serializePretty(photos, true);
		update check;


		Boolean exceptionCaught = false;

		Test.startTest();
		//try {
			CommunityRestartFormController.deletePhoto(null, cv.Id);
		//} catch(System.QueryException e) {
		//	exceptionCaught = true;
		//}
		Test.stopTest();

		//System.assert(exceptionCaught);

	}
	
	@IsTest
	private static void deletePhoto_nullVersionId() {

		//Create Account, Contact, and Case
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		Check_In__c check		= [SELECT Id,Photos__c FROM Check_In__c];

		ContentVersion cv	= new Contentversion();
		cv.title			= 'ABC';
		cv.PathOnClient		= 'test';
		cv.versiondata		= EncodingUtil.base64Decode('Unit Test Attachment Body');
		insert cv;
		
		CommunityRestartFormController.Photo frontPhoto = new CommunityRestartFormController.Photo();
		frontPhoto.id	= cv.Id;
		frontPhoto.name = 'front';
		List<CommunityRestartFormController.Photo> photos = new List<CommunityRestartFormController.Photo>();
		photos.add(frontPhoto);
		check.Photos__c = JSON.serializePretty(photos, true);
		update check;

		Boolean exceptionCaught = false;

		Test.startTest();
		try {
			CommunityRestartFormController.deletePhoto(check.Id, null);
		} catch(System.QueryException e) {
			exceptionCaught = true;
		}
		Test.stopTest();

		System.assert(exceptionCaught);

	}

}