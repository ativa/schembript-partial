@IsTest
public without sharing class AT_LWCC_PlannerAddTemplateBlockToPlan_T  {

	private static RecordType testTool_getTemplateNutritionBlockRecordType(String developerName) {
		return [SELECT Id FROM RecordType WHERE DeveloperName = :developerName];
	}

	private static Template_Block__c testTool_createTemplateBlock(String blockName, Id recordTypeId) {
		Template_Block__c newTmpBlock = new Template_Block__c();
		newTmpBlock.Status__c = 'Public';
		if(recordTypeId != null) {
			newTmpBlock.RecordTypeId = recordTypeId;
		}
		newTmpBlock.Name = blockName;
		insert newTmpBlock;
		return newTmpBlock;
	}

	private static Template_Block_Item__c testTool_createTemplateItem(String itemName, Integer order, Integer quantity, Template_Block__c tmpBlock) {
		Template_Item__c newTmpItem = new Template_Item__c();
		newTmpItem.Name = itemName;
		newTmpItem.Protein__c = 2;
		newTmpItem.Carbs__c = 1;
		newTmpItem.Fat__c = 3;
		insert newTmpItem;

		Template_Block_Item__c newTmpBlkItem	= new Template_Block_Item__c();
		newTmpBlkItem.Display_Order__c			= order;
		newTmpBlkItem.Quantity__c				= quantity;
		newTmpBlkItem.Template_Item__c			= newTmpItem.Id;
		newTmpBlkItem.Template_Block__c			= tmpBlock.Id;
		insert newTmpBlkItem;
		return newTmpBlkItem;
	}

	@IsTest
	public static void GetNutrionalTemplateBlocks_GetBlockWithItems() {
		
		RecordType recType			= testTool_getTemplateNutritionBlockRecordType('Template_Nutrition_Block');
		Template_Block__c tstBlk	= testTool_createTemplateBlock('Test Block', recType.Id);

		Template_Block_Item__c item1 = testTool_createTemplateItem('Test Item 1', 1, 1, tstBlk);
		Template_Block_Item__c item0 = testTool_createTemplateItem('Test Item 0', 0, 1, tstBlk);
		Template_Block_Item__c item2 = testTool_createTemplateItem('Test Item 2', 2, 1, tstBlk);

		Test.startTest();
		List<AT_LWCC_PlannerAddTemplateBlockToPlan.TemplateBlock> tmpBlocks = AT_LWCC_PlannerAddTemplateBlockToPlan.GetNutrionalTemplateBlocks();
		Test.stopTest();

		System.assertEquals(1, tmpBlocks.size());
		System.assertEquals(tstBlk.Id, tmpBlocks.get(0).templateId);
		System.assertEquals(3, tmpBlocks.get(0).templateItems.size());
		System.assertEquals(item0.Template_Item__c, tmpBlocks.get(0).templateItems.get(0).templateId);
		System.assertEquals(item1.Template_Item__c, tmpBlocks.get(0).templateItems.get(1).templateId);
		System.assertEquals(item2.Template_Item__c, tmpBlocks.get(0).templateItems.get(2).templateId);

	}
	
	@IsTest
	public static void GetNutrionalTemplateBlocks_GetBlockWithoutItems() {
		RecordType recType			= testTool_getTemplateNutritionBlockRecordType('Template_Nutrition_Block');
		Template_Block__c tstBlk	= testTool_createTemplateBlock('Test Block', recType.Id);
		
		Test.startTest();
		List<AT_LWCC_PlannerAddTemplateBlockToPlan.TemplateBlock> tmpBlocks = AT_LWCC_PlannerAddTemplateBlockToPlan.GetNutrionalTemplateBlocks();
		Test.stopTest();

		System.assertEquals(1, tmpBlocks.size());
		System.assertEquals(tstBlk.Id, tmpBlocks.get(0).templateId);
		System.assertEquals(0, tmpBlocks.get(0).templateItems.size());

	}
	
	@IsTest
	public static void GetNutrionalTemplateBlocks_GetNoBlocks() {
	
		Test.startTest();
		List<AT_LWCC_PlannerAddTemplateBlockToPlan.TemplateBlock> tmpBlocks = AT_LWCC_PlannerAddTemplateBlockToPlan.GetNutrionalTemplateBlocks();
		Test.stopTest();

		System.assertEquals(0, tmpBlocks.size());
	}

	@IsTest
	public static void GetNutrionalTemplateBlocks_QueryNonNutritionalBlock() {
	
		RecordType recType			= testTool_getTemplateNutritionBlockRecordType('Template_Training_Block');
		Template_Block__c tstBlk	= testTool_createTemplateBlock('Test Block', recType.Id);
		
		Test.startTest();
		List<AT_LWCC_PlannerAddTemplateBlockToPlan.TemplateBlock> tmpBlocks = AT_LWCC_PlannerAddTemplateBlockToPlan.GetNutrionalTemplateBlocks();
		Test.stopTest();

		System.assertEquals(0,			tmpBlocks.size());
	}

}