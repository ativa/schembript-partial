/**
* @author Pratyush Chalasani
* @description Test the Case_Trigger_Tools class
*/ 
@isTest
public class Case_Trigger_Tools_Test {
	@testSetup
    static void init() {
        Organization org = [SELECT IsSandbox FROM Organization LIMIT 1];
        
        String communityName = org.IsSandbox ? 'SchembriPTv2' : 'Schembri PT Client Community';
        
        Portal_Settings__c settings = new Portal_Settings__c(User_Role__c = 'ativCustomerPersonAccount', User_Profile__c = 'Customer Community Plus User - CUSTOM', Portal_URL__c = '/', Community_Name__c = communityName, Subscription_Active_Status__c = 'active');
        
        insert settings;

		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;
    }
    
    /**
	* @description Create a Test Account
	* @return Returns a Test Account
	*/ 
	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		insert testAccount;
		
		return testAccount;
	}
	
	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact createTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		testContact.Email				= 'test@tst.com';
		testContact.Phone				= '0489123345';

		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	@isTest
	static void testTrigger() {
	
		Account testAccount = createTestAccount();
        Contact testContact = createTestContact(testAccount);

        Test.startTest();
        
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		//Create a Product for the case
		Programs_Products__c prod	= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 3;
		prod.Name					= 'Body Revolt';
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
        testProgram.Original_Program_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
		//testProgram.Program_End_Date__c		= Date.newInstance(Date.today().year(), Date.today().month() + 3, 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		insert testProgram;	
				
        List<Check_In__c> checkIns = [SELECT Id FROM Check_In__c WHERE Client_Program__c = :testProgram.Id];
        
        System.assertEquals(0, checkIns.size(), 'Should be no check-ins ot start with');
        
		testProgram.Status = 'Active';
		update testProgram;
        
        checkIns = [SELECT Id FROM Check_In__c WHERE Client_Program__c = :testProgram.Id AND Check_In_Number_v2__c = 0];
        
        System.assertEquals(1, checkIns.size(), 'Should be 1 check-in 0');
        
        testProgram = [SELECT Check_in_0_created__c, Current_Check_In__c, Current_Check_In_Number__c FROM Case WHERE Id = :testProgram.Id];
        
        System.assertEquals(true, testProgram.Check_in_0_created__c, 'Check-In 0 Created flag should be set');
        System.assertEquals(checkIns[0].Id, testProgram.Current_Check_In__c, 'Program should be linked to Check-In 0');
        System.assertEquals(0, testProgram.Current_Check_In_Number__c, 'Current Check-In number should be 0');
        
        Case testProgram2					= new Case();
		testProgram2.AccountId				= testAccount.Id;
		testProgram2.ContactId				= testContact.Id;
		testProgram2.Status					= 'Active';
        testProgram2.Last_Program__c        = testProgram.Id;
		testProgram2.Programs_Products__c = prod.Id;

		testProgram2.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
        testProgram2.Original_Program_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram2.Check_In_Frequency__c	= '1 Week';
		insert testProgram2;	

        Test.stopTest();
	}
}