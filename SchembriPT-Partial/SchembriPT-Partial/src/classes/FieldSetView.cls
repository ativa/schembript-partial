public without sharing class FieldSetView {

    public class FieldSetData {
        @AuraEnabled
        public String objectName;
        @AuraEnabled
        public String title;
        @AuraEnabled
        public List<String> fields;

		@AuraEnabled
		public List<FieldData> fieldData { get; set; }
    }

	public class ObjectMetaData {

		/**
		* @description The Object Label
		*/ 
		@AuraEnabled
		public String label { get; set; }

		/**
		* @description The Object Name
		*/ 
		@AuraEnabled
		public String value { get; set; }

		public ObjectMetaData(Schema.DescribeSObjectResult sObjResult) {
			this.label = sObjResult.getLabel();
			this.value = sObjResult.getName();
		}
		

	}

	/**
	* @author Harrison Campbell
	* @description Holds the Field Set Metadata
	*/ 
	public class FieldSetMetaData {

		/**
		* @description The Field Set Label
		*/ 
		@AuraEnabled
		public String label { get; set; }

		/**
		* @description The Field Set Name
		*/ 
		@AuraEnabled
		public String value { get; set; }
		
		/**
		* @description Create a Field Set Metadata
		* @param fSet 
		*/ 
		public FieldSetMetaData(Schema.FieldSet fSet) {
			this.label	= fSet.getLabel();
			this.value	= fSet.getName();
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds data for a field
	*/ 
	public class FieldData {

		/**
		* @description The Label of the Field
		*/ 
		@AuraEnabled
		public String label { get; set; }

		/**
		* @description The Developer Name of the Field
		*/ 
		@AuraEnabled
		public String name { get; set; }

		/**
		* @description Can the User update this field
		*/ 
		@AuraEnabled
		public Boolean updateable { get; set; }

		public FieldData(Schema.DescribeFieldResult fResult) {
			label		= fResult.getLabel();
			name		= fResult.getName();
			updateable	= fResult.isUpdateable();
		}

	}

	@AuraEnabled
	public static List<ObjectMetaData> getAllObjectTypes() {
		List<Schema.SObjectType> allObjects = Schema.getGlobalDescribe().Values();
		List<ObjectMetaData> objMDList		= new List<ObjectMetaData>();
		for(Schema.SObjectType sObjType : allObjects) {

			Schema.DescribeSObjectResult sObjRes = sObjType.getDescribe();

			//Ignore custom setings, and if they have no Field Sets
			if(sObjRes.isCustomSetting() || sObjRes.FieldSets.getMap().size() == 0) {
				continue;
			}

			objMDList.add(new ObjectMetaData(sObjType.getDescribe()));
		}
		return objMDList;

	}

	@AuraEnabled
	public static Map<String, List<FieldSetMetaData>> getAllObjectFieldSets() {

		Map<String, List<FieldSetMetaData>> allObjFieldSets = new Map<String, List<FieldSetMetaData>>();

		for(ObjectMetaData objMData : getAllObjectTypes()) {
			allObjFieldSets.put(objMData.value, getFieldSets(objMData.value));
		}

		return allObjFieldSets;

	}

	
	private static List<Schema.FieldSet> getAllFieldSets(String objectName) {
		Map<String, Schema.SObjectType> GlobalDescribeMap		= Schema.getGlobalDescribe(); 
		Schema.SObjectType SObjectTypeObj						= GlobalDescribeMap.get(objectName);
		Schema.DescribeSObjectResult DescribeSObjectResultObj	= SObjectTypeObj.getDescribe();

		List<Schema.FieldSet> fieldSets = DescribeSObjectResultObj.FieldSets.getMap().values();
		return fieldSets;

	}

	@AuraEnabled 
	public static List<FieldSetMetaData> getFieldSets(String objectName) {
		List<Schema.FieldSet> fSets = getAllFieldSets(objectName);
		List<FieldSetMetaData> fSetMetadata = new List<FieldSetMetaData>();
		for(Schema.FieldSet fSet : fSets) {
			fSetMetadata.add(new FieldSetMetaData(fSet));
		}
		return fSetMetadata;
	}

    
    @AuraEnabled
    public static FieldSetData getData(String recordID, String fieldSetName) {
        FieldSetData data	= new FieldSetData();
        data.fields			= new List<String>();
		data.fieldData		= new List<FieldData>();
        
        if (String.isNotBlank(recordID) && String.isNotBlank(fieldSetName)) {
            Id idObj = Id.valueOf(recordID);
            
            Schema.DescribeSObjectResult obj = idObj.getSobjectType().getDescribe();
            
            data.objectName = obj.getName();

            if (!Test.isRunningTest()) {

				//Get a Map of all the fields
				Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(obj.getName().toLowerCase()).getDescribe().Fields.getMap();

                data.title = idObj.getSobjectType().getDescribe().FieldSets.getMap().get(fieldSetName).getLabel();
                for (Schema.FieldSetMember field: idObj.getSobjectType().getDescribe().FieldSets.getMap().get(fieldSetName).getFields()) {
		
                    data.fields.add(field.getFieldPath());

					//Get the Field Describe
					Schema.SObjectField fieldSObject = fieldMap.get(field.getFieldPath());
					FieldData fData = new FieldData(fieldSObject.getDescribe());
					data.fieldData.add(fData);

                }
            }
        }
        
        return data;
    }

}