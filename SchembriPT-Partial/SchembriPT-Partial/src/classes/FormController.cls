/**
* @author Harrison Campbell
* @description Controller for the Form Section
*/
public without sharing class FormController  {

	/**
	* @author Harrison Campbell
	* @description Holds data for a form section
	*/ 
	public class FormSection {

		/**
		* @description The label to display for the section
		*/ 
		@AuraEnabled
		public String label { get; set; }

		/**
		* @description The Api of the Salesforce Object that is being used in the form
		*/ 
		@AuraEnabled
		public String objectApiName { get; set; }

		/**
		* @description All the fields that relate to this section
		*/ 
		@AuraEnabled
		public List<FormField> fields { get; set; }

		public FormSection(Form_Section__mdt fSectionMetadata) {
			this.label			= fSectionMetadata.Display_Label__c;
			this.objectApiName	= fSectionMetadata.Object_Api_Name__c;
			this.fields			= new List<FormField>();
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds data for a form field
	*/ 
	public class FormField {

		/**
		* @description The label to display for the field
		*/ 
		@AuraEnabled
		public String label { get; set; }

		
		/**
		* @description The Api Name of the field
		*/ 
		@AuraEnabled
		public String apiName { get; set; }

		/**
		* @description Is this field required?
		*/ 
		@AuraEnabled
		public Boolean required { get; set; }

		/**
		* @description Can the field be updated?
		*/ 
		@AuraEnabled
		public Boolean editable { get; set; }

		/**
		* @description The Width of the field in the form
		*/ 
		@AuraEnabled
		public Integer columnWidth { get; set; }

		@AuraEnabled
		public String controllingValue { get; set; }
		
		@AuraEnabled
		public String controllingField { get; set; }
		
		@AuraEnabled
		public String controllingType { get; set; }
		
		@AuraEnabled
		public String controllingComparison { get; set; }

		public FormField(Form_Field__mdt fFieldMetadata) {
			this.label					= fFieldMetadata.Display_Label__c;
			this.apiName				= fFieldMetadata.ApiName__c;
			this.required				= fFieldMetadata.Required__c;
			this.editable				= fFieldMetadata.Editable__c;
			this.columnWidth			= Integer.valueOf(fFieldMetadata.Column_Width__c);

			this.controllingValue		= fFieldMetadata.Controlling_Value__c;
			this.controllingField		= fFieldMetadata.Controlling_Field__c;
			this.controllingType		= fFieldMetadata.Controlling_Type__c;
			this.controllingComparison	= fFieldMetadata.Controlling_Comparison__c;

		}

	}

	/**
	* @description Get Form Section Data
	* @param formSectionName The name of the section
	* @return Returns the Form Section Data
	*/ 
	@AuraEnabled
	public static FormSection GetFormSectionData(String formSectionName) {

		Form_Section__mdt fSection = [
			SELECT
				Id,
				Display_Label__c,
				Object_Api_Name__c
			FROM Form_Section__mdt
			WHERE DeveloperName = :formSectionName
		];

		List<Form_Field__mdt> fFields = [
			SELECT
				Id,
				Display_Label__c,
				ApiName__c,
				Required__c,
				Editable__c,
				Column_Width__c,
				Controlling_Value__c,
				Controlling_Field__c,
				Controlling_Type__c,
				Controlling_Comparison__c,
				Order__c
			FROM Form_Field__mdt
			WHERE Form_Section__c = :fSection.Id
			ORDER By Order__c ASC
		];

		FormSection fSectionData = new FormSection(fSection);
		for(Form_Field__mdt ffMetadata : fFields) {
			fSectionData.fields.add(new FormField(ffMetadata));
		}

		System.debug(fSectionData.fields);

		return fSectionData;

	}

}