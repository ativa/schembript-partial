/**
* @author Harrison Campbell
* @description Send an Email Template
*/ 
public without sharing class AT_F_SendEmailTemplate {

	public class FailedEmailTemplateException extends Exception {}

	@InvocableMethod(label='Send Email Template' description='Send an Email Template based on the name' category='Ativa - Apex Flow Action')
	public static void SendEmailTemplate(List<Request> requests) {

		List<Messaging.Email> emailsToSend = new List<Messaging.Email>();

		for(Request req : requests) {
			
			if(req.asynchronous == false) {
				EmailTemplate template				= [SELECT Id FROM EmailTemplate WHERE DeveloperName = :req.templateName];
				OrgWideEmailAddress orgEmailAddress = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :req.sentFromEmail];
				Messaging.SingleEmailMessage newMessage = Messaging.renderStoredEmailTemplate(template.Id, req.whoId, req.whatId);
				newMessage.setOrgWideEmailAddressId(orgEmailAddress.Id);
				emailsToSend.add(newMessage);
			} else {
				SendEmailTemplateAsync(req.templateName, req.sentFromEmail, req.whoId, req.whatId);
			}

		}
		
		Messaging.sendEmail(emailsToSend);

	} 

	@Future
	private static void SendEmailTemplateAsync(String templateName, String sentFromEmail, String whoId, String whatId) {

		System.Debug(templateName);
		System.Debug(sentFromEmail);
		System.Debug(whoId);
		System.Debug(whatId);

		EmailTemplate template					= [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName];
		OrgWideEmailAddress orgEmailAddress		= [SELECT Id FROM OrgWideEmailAddress WHERE Address = :sentFromEmail];

		Messaging.SingleEmailMessage newMessage;

		try {
			newMessage = Messaging.renderStoredEmailTemplate(template.Id, whoId, whatId);
		} catch(System.Exception e) {

			String msg	= 'Failed to create new message: \n';
			msg			+= 'Template Name: ' + templateName + '\n';
			msg			+= 'Sent From Email: ' + sentFromEmail + '\n';
			msg			+= 'Who Id: ' + whoId + '\n';
			msg			+= 'What Id: ' + whatId;

			throw new FailedEmailTemplateException(msg);
		}
		newMessage.setOrgWideEmailAddressId(orgEmailAddress.Id);
		Messaging.sendEmail(new Messaging.Email[] { newMessage });
	}

	/**
	* @author Harrison Campbell
	* @description Send Email Request
	*/ 
	public class Request {

		@InvocableVariable(label='Asynchronous' description='Is the email template synchronous or asynchronous' required=true)
		public Boolean asynchronous;

		@InvocableVariable(label='The Email that the it\'s being send from' description='Use the email set in the Organization-Wide Email Addresses Setup' required=true)
		public String sentFromEmail;

		@InvocableVariable(label='Email Template Name' description='The developer name of the email template' required=true)
		public String templateName;
		
		@InvocableVariable(label='Who Id' description='Who the person is being send too' required=true)
		public String whoId;
		
		@InvocableVariable(label='What Id' description='What the email is in relation too' required=false)
		public String whatId;

	}


}