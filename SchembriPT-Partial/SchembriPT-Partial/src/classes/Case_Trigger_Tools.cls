/**
* @author Pratyush Chalasani
* @description All tools used for the Case Trigger
*/
public without sharing class Case_Trigger_Tools {


  public static void beforeInsertOrUpdate(List<Case> newCases, Map<Id, Case> caseMap) {
        Set<Id> accountIDs = new Set<Id>();
        Set<Id> productIDs = new Set<Id>();
        Set<Id> lastIDs = new Set<Id>();

        for (Case c: newCases) {
            accountIDs.add(c.AccountId);
            productIDs.add(c.Programs_Products__c);
            lastIDs.add(c.Last_Program__c);
        }

        Map<Id, Account> accountMap = new Map<Id, Account>(queryAccounts(accountIDs));
		//NEED TO ADD BACK IN
        //Map<Id, Products__c> productMap = new Map<Id, Products__c>([SELECT Name, Duration_days__c, Check_In_Frequency__c FROM Products__c WHERE Id IN :productIDs]);
        Map<Id, Case> lastMap = new Map<Id, Case>(queryLastPrograms(lastIDs));

        List<Check_In__c> newCheckIns = new List<Check_In__c>();

        for (Case c: newCases) {
            System.debug('Processing Case: ' + c.Id);
            System.debug(c.Status + ' (' + c.Check_in_0_created__c + ')');

            // Client (PersonAccount) linked to the Program (Case)
            Account acc = accountMap.get(c.AccountId);

            // Product defined for the Program
			// Products__c prod = productMap.get(c.Programs_Products__c);

            // The previous Program for the Client
            Case last = lastMap.get(c.Last_Program__c);

            // Copy across values from client profile
            if (c.Status == 'New' && c.Last_Program__c == null) {
                copyFromAccountToCase(c, acc);
            }

            // Create check-in 0
            if (c.Id != null && c.Status == 'Active' && c.Check_in_0_created__c == false) {
				System.debug('Creating Check-In 0');
                newCheckIns.add(createCheckIn0(c, acc));
                c.Check_in_0_created__c = true;
            }

            // Check program not-null and set program end-date
			/*
            if (prod != null && prod.Duration_days__c != null && c.Program_Start_Date__c != null && c.Program_End_Date__c == null) {
                c.Program_End_Date__c = c.Program_Start_Date__c.addDays(prod.Duration_days__c.intValue());
            }
			*/

            // Update Most Current Program on Account
            if ((c.Status == 'Active' || c.Status == 'Completed' || c.status == 'On Hold') && (c.Original_Program_Start_Date__c >= acc.Most_Current_Program_Start_Date__pc)) {
                acc.Most_Current_Program_Start_Date__pc = c.Original_Program_Start_Date__c;
                acc.Most_Current_Program_Status__pc = c.Status;
                acc.Most_Current_Program__pc = c.Id;
            }

            // Update New Program from Child Program
            if (last != null && c.Details_Copied_from_Child_Program__c == false) {
				copyFromLastProgram(c, last);
            }
        }

        if (!newCheckIns.isEmpty()) {
            insert newCheckIns;

            for (Check_In__c checkIn: newCheckIns) {
                Case c = caseMap.get(checkIn.Client_Program__c);

                c.Current_Check_In__c = checkIn.Id;
                c.Current_Check_In_Number__c = 0;
                c.Current_Check_in_Last_Update_Date__c = System.Today();
            }
        }
  }

    private static List<Account> queryAccounts(Set<Id> accountIDs) {
        return [
			SELECT 
				Id,
				Name, 
				FirstName, 
				LastName,
				What_made_you_restart_this_program__c,
				Most_Current_Program_Start_Date__pc,
				Most_Current_Program_Status__pc,
				Most_Current_Program__pc,
				Any_Exercises_You_Can_t_Perform__pc,
				Any_Injuries__pc,
				Any_additional_info_you_deem_relevant__pc,
				Are_there_any_foods_you_dislike__pc,
				Are_you_Vegetarian_or_Vegan__pc,
				Are_you_currently_breastfeeding__pc,
				Are_you_taking_any_medication__pc,
				Body_Fat_Reduction__pc,
				Body_Recomposition__pc,
				Body_Transformation__pc,
				Bodybuilding_Competition__pc,
				Do_you_drink_alcohol__pc,
				Do_you_smoke__pc,
				Do_you_take_any_recreational_drugs__pc,
				What_Exercises_are_you_Comforatble_Perfo__pc,
				For_other_foods_you_enjoy_please_specify__pc,
				Have_you_been_ill_recently__pc,
				Hospitalised_recently__pc,
				How_active_are_you_most_days__pc,
				How_long_have_you_been_going_to_the_gym__pc,
				How_many_drinks_per_week__pc,
				How_many_times_do_you_do_drugs_per_week__pc,
				If_other_selected_please_specify__pc,
				If_selected_other_injury_please_specify__pc,
				Implement_Regular_Physical_Training__pc,
				Improve_Cardiovascular_Fitness__pc,
				Improve_Overall_Lifestyle_and_Fitness__pc,
				Increase_Lean_Muscle_Mass__pc,
				Increase_Strength__pc,
				Learn_to_Manage_Training_Hours__pc,
				Names_of_Family_Friends_in_our_Program__pc,
				Other_activities_you_enjoy_doing__pc,
				Other_exercises_you_re_comfortable_doing__pc,
				Other_fitness_goals__pc,
				For_other_foods_you_dislike_specify__pc,
				Post_Injury_Rehab_Recovery__pc,
				Potential_Heart_Concerns__pc,
				Pregnant_or_given_birth_in_last_12_month__pc,
				Specific_detail_of_any_injury_or_illness__pc,
				Surgery_recently__pc,
				What_Foods_Do_You_Enjoy_Eating__pc,
				What_time_of_day_do_you_train__pc,
				What_is_your_Relationship_to_them__pc,
				Your_ideal_training_week_Cardio__pc,
				Your_ideal_training_week_HIIT__pc,
				Your_ideal_training_week_Resistance__pc,
				Dairy_Allergy__pc,
				Gluten_Free__pc,
				Lactose_intolerant__pc,
				Fish_Allergy__pc,
				Egg_Allergy__pc,
				Nut_Allergy__pc,
				Soy_Allergy__pc,
				Corn_Allergy__pc,
				Meat_Allergy__pc,
				Peanut_Allergy__pc,
				Shellfish_Allergy__pc,
				Other_food_allergies_intollerances_V2__pc,
				Specific_detail_of_any_injury_or_illn_V2__pc,
				Where_did_you_hear_about_Schembri_PT__pc,
				What_made_you_start_this_program__pc,
				Any_friends_or_family_with_Schembri__pc,
				Overall_Goals_v2__pc,
				Injuries_or_illnesses__pc,
				Details_Regarding_Allergies__pc,
				For_other_foods_you_enjoy_please_spec_V2__pc,
				For_other_foods_you_dislike_specify_V2__pc,
				Meal_Plan_Followed_before_SPT__pc,
				Explain_your_current_training_week__pc,
				Agree_to_Schembri_PT_Terms_Conditions__pc,
				Permission_to_use_results_for_marketing__pc,
				Current_Resistance_p_wk__pc,
				Current_HIIT_p_wk__pc,
				Current_Cardio_p_wk__pc,
				Ideal_Resistance_p_wk__pc,
				Ideal_HIIT_p_wk__pc,
				Ideal_Cardio_p_wk__pc,
				Starting_Chest__pc,
				Starting_Hips__pc,
				Starting_Thigh__pc,
				Starting_Waist__pc,
				Starting_Weight__pc,
				Do_you_have_food_allergies__pc
			FROM Account 
			WHERE Id IN :accountIDs
		];

    }

    private static List<Case> queryLastPrograms(Set<Id> caseIDs) {
	
        return [SELECT Id,
                Any_Exercises_Can_t_Perform__c,
				Specific_detail_of_any_injury_or_illn_V2__c,
                Any_Injuries__c,
                Any_additional_info_you_deem_relevant__c,
                Are_there_any_foods_you_dislike__c,
                Are_you_Vegetarian_or_Vegan__c,
                Are_you_currently_breastfeeding__c,
                Are_you_taking_any_medication__c,
                Body_Fat_Reduction__c,
                Body_Recomposition__c,
                Body_Transformation__c,
                Bodybuilding_Competition__c,
                Check_In_0_Calories__c,
                Check_In_0_Carb__c,
                Check_In_0_Cardio_Session__c,
                Check_In_0_Circuit_Session__c,
                Check_In_0_Fat__c,
                Check_In_0_HIIT_Session__c,
                Check_In_0_Protein__c,
                Check_In_0_Resistance_Session__c,
                Check_in_0_Metabolic_Condition_Session__c,
                Check_in_0_created__c,
                Contact_Values_Copied__c,
                Current_Calorie_Targets__c,
                Current_Carb_Targets__c,
                Current_Cardio_Session_Targets__c,
                Current_Chest_Measurement__c,
                Current_Circuit_Session_Targets__c,
                Current_Fat_Targets__c,
                Current_HIIT_Session_Targets__c,
                Current_Hip_Measurement__c,
                Current_Protein_Targets__c,
                Current_Resistance_Session_Targets__c,
                Current_Thigh_Measurement__c,
                Current_Waist_Measurement__c,
                Current_Weight__c,
                Current_metabolic_conditioning_targets__c,
                Current_specific_changes_to_nutrition__c,
                Date_Last_Program_finished__c,
                Do_you_drink_alcohol__c,
                Do_you_smoke__c,
                Do_you_take_any_recreational_drugs__c,
                Due_date_of_the_following_nutrition_plan__c,
                Due_date_of_the_following_training_plan__c,
                Due_date_of_the_next_nutrition_plan__c,
                Due_date_of_the_next_training_plan__c,
                Exercises_you_re_comfortable_performing__c,
                For_other_foods_you_enjoy_please_specify__c,
                Have_photos_been_provided__c,
                Have_you_been_ill_recently__c,
                Hospitalised_recently__c,
                How_active_are_you_most_days__c,
                How_long_have_you_been_going_to_the_gym__c,
                How_many_drinks_per_week__c,
                How_many_times_do_you_do_drugs_per_week__c,
                If_other_selected_please_specify__c,
                If_selected_other_injury_please_specify__c,
                Implement_Regular_Physical_Training__c,
                Improve_Cardiovascular_Fitness__c,
                Improve_Overall_Lifestyle_and_Fitness__c,
                Increase_Lean_Muscle_Mass__c,
                Increase_Strength__c,
                Last_1_week_Scheduled_Break__c,
                Last_time_original_photos_were_checked__c,
                Learn_to_Manage_Training_Hours__c,
                Measurements__c,
                Medical_Required__c,
                Names_of_Family_Friends_in_our_Program__c,
                Original_Program_Start_Date__c,
                Other_activities_you_enjoy_doing__c,
                Other_exercises_you_re_comfortable_doing__c,
                Other_fitness_goals__c,
                Photos__c,
                Please_give_details__c,
                Post_Injury_Rehab_Recovery__c,
                Potential_Heart_Concerns__c,
                Pregnant_or_given_birth_in_last_12_month__c,
                Program_Start_Date__c,
                Questionaire__c,
                Supercoach__c,
                Surgery_recently__c,
                Trainer__c,
                What_Foods_Do_You_Enjoy_Eating__c,
                What_Time_of_Day_You_Train__c,
                What_is_your_Relationship_to_them__c,
                Your_ideal_training_week_Cardio__c,
                Your_ideal_training_week_HIIT__c,
                Your_ideal_training_week_Resistance__c,
                current_reason_for_changes_to_training__c,
                current_reasons_for_changes_to_nutrition__c,
                current_specific_changes_to_training__c,
				Current_Resistance_p_wk__c,
				Current_HIIT_p_wk__c,
				Current_Cardio_p_wk__c,
				Ideal_Resistance_p_wk__c,
				Ideal_HIIT_p_wk__c,
				Ideal_Cardio_p_wk__c,
				What_made_you_restart_this_program__c,
				Do_you_have_food_allergies__c
               FROM Case WHERE Id IN :caseIDs];
    }
	
    private static Check_In__c createCheckIn0(Case c, Account acc) {
        Check_In__c ci = new Check_In__c(
			Name = 'Check-In 0 - ' + acc.FirstName + ' ' + acc.LastName,
			Check_In_Number_V2__c = 0,
			Check_In_Number__c = '0',
			Stage__c = 'Pending');

        ci.Account__c = acc.Id;

		copyFromProgramToCheckIn(ci, c);

        return ci;
    }

	public static void copyFromProgramToCheckIn(Check_In__c ci, Case c) {
	
		ci.Do_you_have_food_allergies__c				= c.Do_you_have_food_allergies__c;
		ci.What_made_you_restart_this_program__c		= c.What_made_you_restart_this_program__c;
        ci.Client_Program__c							= c.Id;
        ci.Contact__c									= c.ContactId;
        ci.Check_In_Day__c								= c.Check_In_Day__c;
        ci.Check_In_Frequency__c						= c.Check_In_Frequency_V2__c;
        ci.Trainer__c									= c.Trainer__c;
        ci.Body_Fat_Reduction__c						= c.Body_Fat_Reduction__c;
        ci.Body_Transformation__c						= c.Body_Transformation__c;
        ci.Bodybuilding_Competition__c					= c.Bodybuilding_Competition__c;
        ci.Chest_Measurement__c							= c.Sign_Up_Chest__c;
        ci.Hip_Measurement__c							= c.Sign_Up_Hips__c;
        ci.How_long_have_you_been_going_to_the_gym__c	= c.How_long_have_you_been_going_to_the_gym__c;
        ci.Implement_Regular_Physical_Training__c		= c.Implement_Regular_Physical_Training__c;
        ci.Improve_Cardiovascular_Fitness__c			= c.Improve_Cardiovascular_Fitness__c;
        ci.Improve_Overall_Lifestyle_and_Fitness__c		= c.Improve_Overall_Lifestyle_and_Fitness__c;
        ci.Increase_Lean_Muscle_Mass__c					= c.Increase_Lean_Muscle_Mass__c;
        ci.Increase_Strength__c							= c.Increase_Strength__c;
        ci.Last_1_week_Scheduled_Break__c				= c.Last_1_week_Scheduled_Break__c;
        ci.Learn_to_Manage_Training_Hours__c			= c.Learn_to_Manage_Training_Hours__c;
        ci.Names_of_Family_Friends_in_our_Program__c	= c.Names_of_Family_Friends_in_our_Program__c;
        ci.New_Cardio_Session_Targets__c				= c.Check_In_0_Cardio_Session__c;
        ci.New_Circuit_Session_Targets__c				= c.Check_In_0_Circuit_Session__c;
        ci.New_HIIT_Session_Targets__c					= c.Check_In_0_HIIT_Session__c;
        ci.New_Metabolic_Conditioning_Targets__c		= c.Current_metabolic_conditioning_targets__c;
        ci.New_Resistance_Session_Targets__c			= c.Check_In_0_Resistance_Session__c;
        ci.New_reason_for_changes_to_training__c		= c.current_reason_for_changes_to_training__c;
        ci.New_reasons_for_changes_to_nutrition__c		= c.current_reasons_for_changes_to_nutrition__c;
        ci.New_specific_changes_to_nutrition__c			= c.Current_specific_changes_to_nutrition__c;
        ci.New_specific_changes_to_training__c			= c.current_specific_changes_to_training__c;
        ci.Other_activities_you_enjoy_doing__c			= c.Other_activities_you_enjoy_doing__c;
        ci.Overall_goals__c								= c.Other_fitness_goals__c;
        ci.Post_Injury_Rehab_Recovery__c				= c.Post_Injury_Rehab_Recovery__c;
        ci.Thigh_Measurement__c							= c.Sign_Up_Thigh__c;
        ci.Waist_Measurement__c							= c.Sign_Up_Waist__c;
        ci.Weight__c									= c.SIgn_Up_Weight__c;
        ci.What_is_your_Relationship_to_them__c			= c.What_is_your_Relationship_to_them__c;
        ci.What_time_of_day_do_you_train__c				= c.What_Time_of_Day_You_Train__c;
        ci.Your_ideal_training_week_Cardio__c			= c.Your_ideal_training_week_Cardio__c;
        ci.Your_ideal_training_week_HIIT__c				= c.Your_ideal_training_week_HIIT__c;
        ci.Your_ideal_training_week_Resistance__c		= c.Your_ideal_training_week_Resistance__c;
        ci.bod__c										= c.Body_Recomposition__c;
		ci.Where_did_you_hear_about_SchembriPT__c		= c.Where_did_you_hear_about_SchembriPT__c;
		ci.What_made_you_start_this_program_V2__c		= c.What_made_you_start_this_program_V2__c;
		ci.Overall_Goals_v2__c							= c.Overall_Goals_v2__c;
		ci.Any_injuries_and_or_illnesses__c				= c.Any_injuries_and_or_illnesses__c;
		ci.Details_regarding_Allergies__c				= c.Details_regarding_Allergies__c;
		ci.What_Foods_Do_You_Enjoy_Eating__c			= c.What_Foods_Do_You_Enjoy_Eating__c;
		ci.For_other_foods_you_enjoy_please_specify__c	= c.For_other_foods_you_enjoy_please_specify__c;
		ci.Are_there_any_foods_you_dislike__c			= c.Are_there_any_foods_you_dislike__c;
		ci.For_other_foods_you_dislike_specify__c		= c.For_other_foods_you_dislike_specify__c;
		ci.Meal_Plan_Followed_before_SPT__c				= c.Meal_Plan_Followed_before_SPT__c;
		ci.Exercises_you_are_comfortable_performing__c	= c.Exercises_you_are_comfortable_performing__c;
		ci.Any_Exercises_you_can_t_perform__c			= c.Any_Exercises_you_can_t_perform__c;
		ci.If_other_selected_please_specify__c			= c.If_other_selected_please_specify__c;
		ci.Explain_your_current_training_week__c		= c.Explain_your_current_training_week__c;
		ci.Explain_your_ideal_training_week__c			= c.Explain_your_ideal_training_week__c;
		ci.Any_additional_info_you_deem_relevant__c		= c.Any_additional_info_you_deem_relevant__c;
		ci.Agree_to_Schembri_PT_Terms_Conditions__c		= c.Agree_to_Schembri_PT_Terms_Conditions__c;
		ci.Permission_to_use_results_for_marketing__c	= c.Permission_to_use_results_for_marketing__c;

		ci.Current_Resistance_p_wk__c					= c.Current_Resistance_p_wk__c;
		ci.Current_HIIT_p_wk__c							= c.Current_HIIT_p_wk__c;
		ci.Current_Cardio_p_wk__c						= c.Current_Cardio_p_wk__c;
		ci.Ideal_Resistance_p_wk__c						= c.Ideal_Resistance_p_wk__c;
		ci.Ideal_HIIT_p_wk__c							= c.Ideal_HIIT_p_wk__c;
		ci.Ideal_Cardio_p_wk__c							= c.Ideal_Cardio_p_wk__c;
		ci.Specific_detail_of_any_injury_or_illn_V2__c	= c.Specific_detail_of_any_injury_or_illn_V2__c;

	}

    public static void copyFromAccountToCase(Case c, Account acc) {
	
		c.Do_you_have_food_allergies__c					= acc.Do_you_have_food_allergies__pc;
		c.What_made_you_restart_this_program__c			= acc.What_made_you_restart_this_program__c;
        c.Any_Exercises_Can_t_Perform__c				= acc.Any_Exercises_You_Can_t_Perform__pc;
        c.Any_Injuries__c								= acc.Any_Injuries__pc;
        c.Any_additional_info_you_deem_relevant__c		= acc.Any_additional_info_you_deem_relevant__pc;
        c.Are_there_any_foods_you_dislike__c			= acc.Are_there_any_foods_you_dislike__pc;
        c.Are_you_Vegetarian_or_Vegan__c				= acc.Are_you_Vegetarian_or_Vegan__pc;
        c.Are_you_currently_breastfeeding__c			= acc.Are_you_currently_breastfeeding__pc;
        c.Are_you_taking_any_medication__c				= acc.Are_you_taking_any_medication__pc;
        c.Body_Fat_Reduction__c							= acc.Body_Fat_Reduction__pc;
        c.Body_Recomposition__c							= acc.Body_Recomposition__pc;
        c.Body_Transformation__c						= acc.Body_Transformation__pc;
        c.Bodybuilding_Competition__c					= acc.Bodybuilding_Competition__pc;
        c.Do_you_drink_alcohol__c						= acc.Do_you_drink_alcohol__pc;
        c.Do_you_smoke__c								= acc.Do_you_smoke__pc;
        c.Do_you_take_any_recreational_drugs__c			= acc.Do_you_take_any_recreational_drugs__pc;
        c.Exercises_you_re_comfortable_performing__c	= acc.What_Exercises_are_you_Comforatble_Perfo__pc;
        c.For_other_foods_you_enjoy_please_specify__c	= acc.For_other_foods_you_enjoy_please_specify__pc;
        c.Have_you_been_ill_recently__c					= acc.Have_you_been_ill_recently__pc;
        c.Hospitalised_recently__c						= acc.Hospitalised_recently__pc;
        c.How_active_are_you_most_days__c				= acc.How_active_are_you_most_days__pc;
        c.How_long_have_you_been_going_to_the_gym__c	= acc.How_long_have_you_been_going_to_the_gym__pc;
        c.How_many_drinks_per_week__c					= acc.How_many_drinks_per_week__pc;
        c.How_many_times_do_you_do_drugs_per_week__c	= acc.How_many_times_do_you_do_drugs_per_week__pc;
        c.If_other_selected_please_specify__c			= acc.If_other_selected_please_specify__pc;
        c.If_selected_other_injury_please_specify__c	= acc.If_selected_other_injury_please_specify__pc;
        c.Implement_Regular_Physical_Training__c		= acc.Implement_Regular_Physical_Training__pc;
        c.Improve_Cardiovascular_Fitness__c				= acc.Improve_Cardiovascular_Fitness__pc;
        c.Improve_Overall_Lifestyle_and_Fitness__c		= acc.Improve_Overall_Lifestyle_and_Fitness__pc;
        c.Increase_Lean_Muscle_Mass__c					= acc.Increase_Lean_Muscle_Mass__pc;
        c.Increase_Strength__c							= acc.Increase_Strength__pc;
        c.Learn_to_Manage_Training_Hours__c				= acc.Learn_to_Manage_Training_Hours__pc;
        c.Names_of_Family_Friends_in_our_Program__c		= acc.Names_of_Family_Friends_in_our_Program__pc;
        c.Other_activities_you_enjoy_doing__c			= acc.Other_activities_you_enjoy_doing__pc;
        c.Other_exercises_you_re_comfortable_doing__c	= acc.Other_exercises_you_re_comfortable_doing__pc;
        c.Other_fitness_goals__c						= acc.Other_fitness_goals__pc;
        c.Please_give_details__c						= acc.For_other_foods_you_dislike_specify__pc;
        c.Post_Injury_Rehab_Recovery__c					= acc.Post_Injury_Rehab_Recovery__pc;
        c.Potential_Heart_Concerns__c					= acc.Potential_Heart_Concerns__pc;
        c.Pregnant_or_given_birth_in_last_12_month__c	= acc.Pregnant_or_given_birth_in_last_12_month__pc;
        c.Surgery_recently__c							= acc.Surgery_recently__pc;
        c.What_Foods_Do_You_Enjoy_Eating__c				= acc.What_Foods_Do_You_Enjoy_Eating__pc;
        c.What_Time_of_Day_You_Train__c					= acc.What_time_of_day_do_you_train__pc;
        c.What_is_your_Relationship_to_them__c			= acc.What_is_your_Relationship_to_them__pc;
        c.Your_ideal_training_week_Cardio__c			= acc.Your_ideal_training_week_Cardio__pc;
        c.Your_ideal_training_week_HIIT__c				= acc.Your_ideal_training_week_HIIT__pc;
        c.Your_ideal_training_week_Resistance__c		= acc.Your_ideal_training_week_Resistance__pc;

        c.Dairy_Allergy__c								= acc.Dairy_Allergy__pc;
        c.Gluten_Free__c								= acc.Gluten_Free__pc;
        c.Lactose_intolerant__c							= acc.Lactose_intolerant__pc;
        c.Egg_Allergy__c								= acc.Egg_Allergy__pc;
        c.Nut_Allergy__c								= acc.Nut_Allergy__pc;
        c.Soy_Allergy__c								= acc.Soy_Allergy__pc;
        c.Corn_Allergy__c								= acc.Corn_Allergy__pc;
        c.Meat_Allergy__c								= acc.Meat_Allergy__pc;
        c.Peanut_Allergy__c								= acc.Peanut_Allergy__pc;
		c.Fish_Allergy__c								= acc.Fish_Allergy__pc;
        c.Shellfish_Allergy__c							= acc.Shellfish_Allergy__pc;
        c.Other_food_allergies_intolerances__c			= acc.Other_food_allergies_intollerances_V2__pc;

        c.Please_give_details__c						= acc.For_other_foods_you_dislike_specify_V2__pc;
		c.For_other_foods_you_enjoy_please_specify__c   = acc.For_other_foods_you_enjoy_please_spec_V2__pc;

		c.Where_did_you_hear_about_SchembriPT__c		= acc.Where_did_you_hear_about_Schembri_PT__pc;
		c.What_Foods_Do_You_Enjoy_Eating__c				= acc.What_Foods_Do_You_Enjoy_Eating__pc;
		c.What_made_you_start_this_program_V2__c		= acc.What_made_you_start_this_program__pc;
		c.Any_family_or_friends_you_could_refer__c		= acc.Any_friends_or_family_with_Schembri__pc;
		c.Names_of_Family_Friends_in_our_Program__c		= acc.Names_of_Family_Friends_in_our_Program__pc;

		c.Overall_Goals_v2__c							= acc.Overall_Goals_v2__pc;

		c.Any_injuries_and_or_illnesses__c				= acc.Injuries_or_illnesses__pc;

		c.Details_regarding_Allergies__c				= acc.Details_Regarding_Allergies__pc;

		c.For_other_foods_you_enjoy_please_specify__c	= acc.For_other_foods_you_enjoy_please_spec_V2__pc;
		c.For_other_foods_you_dislike_specify__c		= acc.For_other_foods_you_dislike_specify_V2__pc;
		c.Meal_Plan_Followed_before_SPT__c				= acc.Meal_Plan_Followed_before_SPT__pc;

		c.Exercises_you_are_comfortable_performing__c	= acc.What_Exercises_are_you_Comforatble_Perfo__pc;
		c.Any_Exercises_you_can_t_perform__c			= acc.Any_Exercises_You_Can_t_Perform__pc;
		c.If_other_selected_please_specify_V2__c		= acc.If_other_selected_please_specify__pc;
		c.Explain_your_current_training_week__c			= acc.Explain_your_current_training_week__pc;

		c.Agree_to_Schembri_PT_Terms_Conditions__c		= acc.Agree_to_Schembri_PT_Terms_Conditions__pc;
		c.Permission_to_use_results_for_marketing__c	= acc.Permission_to_use_results_for_marketing__pc;
		
		c.Current_Resistance_p_wk__c					= acc.Current_Resistance_p_wk__pc;
		c.Current_HIIT_p_wk__c							= acc.Current_HIIT_p_wk__pc;
		c.Current_Cardio_p_wk__c						= acc.Current_Cardio_p_wk__pc;
		c.Ideal_Resistance_p_wk__c						= acc.Ideal_Resistance_p_wk__pc;
		c.Ideal_HIIT_p_wk__c							= acc.Ideal_HIIT_p_wk__pc;
		c.Ideal_Cardio_p_wk__c							= acc.Ideal_Cardio_p_wk__pc;

		c.Program_Start_Chest__c						= acc.Starting_Chest__pc;
		c.Program_Start_Hips__c							= acc.Starting_Hips__pc;
		c.Program_Start_Thighs__c						= acc.Starting_Thigh__pc;
		c.Program_Start_Waist__c						= acc.Starting_Waist__pc;
		c.Program_Start_Weight__c						= acc.Starting_Weight__pc;
		c.Specific_detail_of_any_injury_or_illn_V2__c	= acc.Specific_detail_of_any_injury_or_illn_V2__pc;

    }

    private static void copyFromLastProgram(Case c, Case last) {

		c.Do_you_have_food_allergies__c					= last.Do_you_have_food_allergies__c;
        c.Details_Copied_from_Child_Program__c			= true;
        c.Any_Exercises_Can_t_Perform__c				= last.Any_Exercises_Can_t_Perform__c;
        c.Any_Injuries__c								= last.Any_Injuries__c;
        c.Are_there_any_foods_you_dislike__c			= last.Are_there_any_foods_you_dislike__c;
        c.Are_you_Vegetarian_or_Vegan__c				= last.Are_you_Vegetarian_or_Vegan__c;
        c.Are_you_currently_breastfeeding__c			= last.Are_you_currently_breastfeeding__c;
        c.Are_you_taking_any_medication__c				= last.Are_you_taking_any_medication__c;
        c.Body_Fat_Reduction__c							= last.Body_Fat_Reduction__c;
        c.Body_Recomposition__c							= last.Body_Recomposition__c;
        c.Body_Transformation__c						= last.Body_Transformation__c;
        c.Bodybuilding_Competition__c					= last.Bodybuilding_Competition__c;
        c.Check_In_0_Calories__c						= last.Check_In_0_Calories__c;
        c.Check_In_0_Carb__c							= last.Check_In_0_Carb__c;
        c.Check_In_0_Cardio_Session__c					= last.Check_In_0_Cardio_Session__c;
        c.Check_In_0_Circuit_Session__c					= last.Check_In_0_Circuit_Session__c;
        c.Check_In_0_Fat__c								= last.Check_In_0_Fat__c;
        c.Check_In_0_HIIT_Session__c					= last.Check_In_0_HIIT_Session__c;
        c.Check_In_0_Protein__c							= last.Check_In_0_Protein__c;
        c.Check_In_0_Resistance_Session__c				= last.Check_In_0_Resistance_Session__c;
        c.Check_in_0_Metabolic_Condition_Session__c		= last.Check_in_0_Metabolic_Condition_Session__c;
        c.Check_in_0_created__c							= last.Check_in_0_created__c;
        c.Contact_Values_Copied__c						= last.Contact_Values_Copied__c;
        c.Current_Calorie_Targets__c					= last.Current_Calorie_Targets__c;
        c.Current_Carb_Targets__c						= last.Current_Carb_Targets__c;
        c.Current_Cardio_Session_Targets__c				= last.Current_Cardio_Session_Targets__c;
        c.Current_Chest_Measurement__c					= last.Current_Chest_Measurement__c;
        c.Current_Circuit_Session_Targets__c			= last.Current_Circuit_Session_Targets__c;
        c.Current_Fat_Targets__c						= last.Current_Fat_Targets__c;
        c.Current_HIIT_Session_Targets__c				= last.Current_HIIT_Session_Targets__c;
        c.Current_Hip_Measurement__c					= last.Current_Hip_Measurement__c;
        c.Current_Protein_Targets__c					= last.Current_Protein_Targets__c;
        c.Current_Resistance_Session_Targets__c			= last.Current_Resistance_Session_Targets__c;
        c.Current_Thigh_Measurement__c					= last.Current_Thigh_Measurement__c;
        c.Current_Waist_Measurement__c					= last.Current_Waist_Measurement__c;
        c.Current_Weight__c								= last.Current_Weight__c;
        c.Current_metabolic_conditioning_targets__c		= last.Current_metabolic_conditioning_targets__c;
        c.Current_specific_changes_to_nutrition__c		= last.Current_specific_changes_to_nutrition__c;
        c.Date_Last_Program_finished__c					= last.Date_Last_Program_finished__c;
        c.Do_you_smoke__c								= last.Do_you_smoke__c;
        c.Do_you_take_any_recreational_drugs__c			= last.Do_you_take_any_recreational_drugs__c;
        c.Due_date_of_the_following_nutrition_plan__c	= last.Due_date_of_the_following_nutrition_plan__c;
        c.Due_date_of_the_following_training_plan__c	= last.Due_date_of_the_following_training_plan__c;
        c.Due_date_of_the_next_nutrition_plan__c		= last.Due_date_of_the_next_nutrition_plan__c;
        c.Due_date_of_the_next_training_plan__c			= last.Due_date_of_the_next_training_plan__c;
        c.Exercises_you_re_comfortable_performing__c	= last.Exercises_you_re_comfortable_performing__c;
        c.For_other_foods_you_enjoy_please_specify__c	= last.For_other_foods_you_enjoy_please_specify__c;
        c.Have_photos_been_provided__c					= last.Have_photos_been_provided__c;
        c.Have_you_been_ill_recently__c					= last.Have_you_been_ill_recently__c;
        c.Hospitalised_recently__c						= last.Hospitalised_recently__c;
        c.How_active_are_you_most_days__c				= last.How_active_are_you_most_days__c;
        c.How_long_have_you_been_going_to_the_gym__c	= last.How_long_have_you_been_going_to_the_gym__c;
        c.How_many_drinks_per_week__c					= last.How_many_drinks_per_week__c;
        c.How_many_times_do_you_do_drugs_per_week__c	= last.How_many_times_do_you_do_drugs_per_week__c;
        c.If_other_selected_please_specify__c			= last.If_other_selected_please_specify__c;
        c.If_selected_other_injury_please_specify__c	= last.If_selected_other_injury_please_specify__c;
        c.Implement_Regular_Physical_Training__c		= last.Implement_Regular_Physical_Training__c;
        c.Improve_Cardiovascular_Fitness__c				= last.Improve_Cardiovascular_Fitness__c;
        c.Improve_Overall_Lifestyle_and_Fitness__c		= last.Improve_Overall_Lifestyle_and_Fitness__c;
        c.Increase_Lean_Muscle_Mass__c					= last.Increase_Lean_Muscle_Mass__c;
        c.Increase_Strength__c							= last.Increase_Strength__c;
        c.Last_1_week_Scheduled_Break__c				= last.Last_1_week_Scheduled_Break__c;
        c.Last_time_original_photos_were_checked__c		= last.Last_time_original_photos_were_checked__c;
        c.Learn_to_Manage_Training_Hours__c				= last.Learn_to_Manage_Training_Hours__c;
        c.Measurements__c								= last.Measurements__c;
        c.Medical_Required__c							= last.Medical_Required__c;
        c.Names_of_Family_Friends_in_our_Program__c		= last.Names_of_Family_Friends_in_our_Program__c;
        c.Original_Program_Start_Date__c				= last.Original_Program_Start_Date__c;
        c.Other_activities_you_enjoy_doing__c			= last.Other_activities_you_enjoy_doing__c;
        c.Other_exercises_you_re_comfortable_doing__c	= last.Other_exercises_you_re_comfortable_doing__c;
        c.Other_fitness_goals__c						= last.Other_fitness_goals__c;
        c.Photos__c										= last.Photos__c;
        c.Please_give_details__c						= last.Please_give_details__c;
        c.Post_Injury_Rehab_Recovery__c					= last.Post_Injury_Rehab_Recovery__c;
        c.Potential_Heart_Concerns__c					= last.Potential_Heart_Concerns__c;
        c.Pregnant_or_given_birth_in_last_12_month__c	= last.Pregnant_or_given_birth_in_last_12_month__c;
        c.Program_Start_Date__c							= last.Program_Start_Date__c;
        c.Questionaire__c								= last.Questionaire__c;
        c.Supercoach__c									= last.Supercoach__c;
        c.Surgery_recently__c							= last.Surgery_recently__c;
        c.Trainer__c									= last.Trainer__c;
        c.What_Foods_Do_You_Enjoy_Eating__c				= last.What_Foods_Do_You_Enjoy_Eating__c;
        c.What_Time_of_Day_You_Train__c					= last.What_Time_of_Day_You_Train__c;
        c.What_is_your_Relationship_to_them__c			= last.What_is_your_Relationship_to_them__c;
        c.Your_ideal_training_week_Cardio__c			= last.Your_ideal_training_week_Cardio__c;
        c.Your_ideal_training_week_HIIT__c				= last.Your_ideal_training_week_HIIT__c;
        c.Your_ideal_training_week_Resistance__c		= last.Your_ideal_training_week_Resistance__c;
        c.current_reason_for_changes_to_training__c		= last.current_reason_for_changes_to_training__c;
        c.current_reasons_for_changes_to_nutrition__c	= last.current_reasons_for_changes_to_nutrition__c;
        c.current_specific_changes_to_training__c		= last.current_specific_changes_to_training__c;
		c.What_made_you_restart_this_program__c			= last.What_made_you_restart_this_program__c;
		c.Specific_detail_of_any_injury_or_illn_V2__c	= last.Specific_detail_of_any_injury_or_illn_V2__c;
    }
}