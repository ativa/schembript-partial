/**
* @author Harrison Campbell
* @description Lightning Controller for the 'plannerXViewPlan' LWC
*/ 
public without sharing class AT_LWCC_PlannerViewPlan {

	/**
	* @description Get Plan data
	* @param planId The Id of the Plan
	* @return Returns the plan data
	*/ 
	@AuraEnabled
	public static AT_M_Nutrition.NutritionPlan GetPlanData(String planId) { 
		AT_M_Nutrition.NutritionPlan nPlan = AT_M_Nutrition.QueryNutritionPlans(new Id[]{ planId })[0];
		return nPlan;
	}

}