/**
* @author Harrison Campbell
* @description Controller for the New Client Page when the first login to the portal
*/ 
public without sharing class CommunityOnboardingController {

	
    /**
    * @author Harrison Campbell
    * @description Information on the user
    */ 
    public class Info {

        @AuraEnabled 
		public String userID;

        @AuraEnabled 
		public String accountID;

        @AuraEnabled 
		public String portalURL;

		@AuraEnabled
		public Boolean skipOnboarding;

		@AuraEnabled
		public String latestCheckInId;

		@AuraEnabled
		public String communityPortalPage;

    }
    
	/**
	* @description Harrison Campbell
	* @return Get information on the user
	*/ 
	@AuraEnabled
	public static Info getUserInfo() {
        Info info = new Info();
		
        info.userID		= UserInfo.getUserId();
		User usr		= [
			SELECT 
				Contact.AccountId,
				Profile.Name 
			FROM User 
			WHERE Id = :info.userID
		];

		Account userAccount = [
			SELECT
				Id,
				Skip_Onboarding_Process__c,
				Community_Portal_Page__c
			FROM Account
			WHERE Id = :usr.Contact.AccountId
		];

        info.accountID				= userAccount.Id;
		info.skipOnboarding			= userAccount.Skip_Onboarding_Process__c;
		info.communityPortalPage	= userAccount.Community_Portal_Page__c;


		if(usr.Profile.Name == 'SPT Full Member') {
			info.portalURL = '/Members';
		} else if(usr.Profile.Name == 'SPT Self-Serve') {
			info.portalURL = '/SelfService';
		}

		List<Check_In__c> latestCheckIn = [
			SELECT
				Id,
				Scheduled_Check_In_Date__c
			FROM Check_In__c
			WHERE Account__c = :userAccount.Id
			ORDER BY Scheduled_Check_In_Date__c DESC NULLS LAST
			LIMIT 1
		];

		if(latestCheckIn.size() > 0) {
			info.latestCheckInId = latestCheckIn.get(0).Id;
		}

        createPrivateGroup(info.userID);
        
		return info;
	}

	@AuraEnabled
	public static void UpdateSignupValues(Id accountId) {

		Account acc = [
			SELECT 
				Id,

				Sign_Up_Chest__c,
				Sign_Up_Hips__c,
				Sign_Up_Thighs__c,
				Sign_Up_Waist__c,
				Sign_Up_Weight__c,

				Starting_Chest__pc,
				Starting_Hips__pc,
				Starting_Thigh__pc,
				Starting_Waist__pc,
				Starting_Weight__pc

			FROM Account
			WHERE Id = :accountId
		];

		acc.Sign_Up_Chest__c	= acc.Starting_Chest__pc;
		acc.Sign_Up_Hips__c		= acc.Starting_Hips__pc;
		acc.Sign_Up_Thighs__c	= acc.Starting_Thigh__pc;
		acc.Sign_Up_Waist__c	= acc.Starting_Waist__pc;
		acc.Sign_Up_Weight__c	= acc.Starting_Weight__pc;

		update acc;
	}

	/**
	* @description Create Advisories and attach them to the account
	* @param accountId The Id of the account
	*/ 
	@AuraEnabled
	public static void addAdvisories(Id accountId) {
		
		List<Account> clientAccount = [
			SELECT
				Id,
				Any_Injuries__pc,
				Any_Exercises_You_Can_t_Perform__pc,
				Have_you_been_ill_recently__pc,
				Specific_detail_of_any_injury_or_illness__pc,
				Are_you_Vegetarian_or_Vegan__pc,
				What_Foods_Do_You_Enjoy_Eating__pc,
				For_other_foods_you_enjoy_please_specify__pc,
				Are_there_any_foods_you_dislike__pc,
				For_other_foods_you_dislike_specify__pc,
				What_Exercises_are_you_Comforatble_Perfo__pc,
				Other_exercises_you_re_comfortable_doing__pc,
				Lactose_intolerant__pc,
				Gluten_Free__pc,
				Fish_Allergy__pc,
				Shellfish_Allergy__pc,
				Nut_Allergy__pc,
				Corn_Allergy__pc,
				Dairy_Allergy__pc,
				Egg_Allergy__pc,
				Peanut_Allergy__pc,
				Meat_Allergy__pc,
				Soy_Allergy__pc,
				Other_food_allergies_intollerances__pc,
				If_selected_other_injury_please_specify__pc,
				If_other_selected_please_specify__pc,
				CreatedDate
			FROM Account
			WHERE Id = :accountId
		];

		Account_Trigger_Tools.createAdvisories(clientAccount);


	}

    @future
    private static void createPrivateGroup(Id userID) {
        Portal_Settings__c settings = Portal_Settings__c.getInstance();
        
        User member = [SELECT Id, AccountId FROM User WHERE Id = :userID];
        
        Account acc = [SELECT Name, FirstName, LastName, PersonMobilePhone, PersonEmail, PersonContactId, Private_Group_ID__c FROM Account WHERE Id = :member.AccountId];

		if (acc.Private_Group_ID__c != null) return;
        
        Network community = [SELECT Id FROM Network WHERE Name = :settings.Community_Name__c]; 
        
        CollaborationGroup privateGroup = new CollaborationGroup(
            CollaborationType = 'Private',
            Name = acc.Name,
            NetworkId = community.Id,
            Description = 'Private group for ' + acc.Name);
        
        insert privateGroup;
        
        CollaborationGroupMember groupMember = new CollaborationGroupMember(MemberId = member.Id, CollaborationGroupId = privateGroup.Id, CollaborationRole = 'Admin');
        
        List<CollaborationGroupMember> existingMember = [SELECT Id FROM CollaborationGroupMember WHERE (MemberId = :member.Id) AND (CollaborationGroupId = :privateGroup.Id) LIMIT 1];
        
        if (existingMember.isEmpty()) insert groupMember;
        
        update new Account(Id = acc.Id, Private_Group_ID__c = privateGroup.Id);
    }

	@AuraEnabled
	public static void FlagFormAsCompleted(Id accountId) {
		
		Account acc = [
			SELECT
				Id,
				Community_Portal_Page__c,
				Form_Submitted_Datetime__c
			FROM Account
			WHERE Id = :accountId
		];

		acc.Community_Portal_Page__c	= 'Main Page';
		acc.Form_Submitted_Datetime__c	= Datetime.now();

		update acc;
		

	}

}