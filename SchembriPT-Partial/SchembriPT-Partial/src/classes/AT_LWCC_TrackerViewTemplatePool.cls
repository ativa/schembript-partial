public without sharing class AT_LWCC_TrackerViewTemplatePool {

	public class CheckInPoolPlanInformation {

		@AuraEnabled
		public CheckInInformation checkInInformation { get; set; }

		@AuraEnabled
		public List<PoolPlanVersion> latestPoolPlanVersions { get; set; }

		public CheckInPoolPlanInformation() {
			this.latestPoolPlanVersions = new List<PoolPlanVersion>();
		}
  
	}

	public virtual class TemplateValues {
	
		@AuraEnabled
		public String id { get; set; }

		@AuraEnabled
		public String name { get; set; }

		@AuraEnabled
		public Decimal protein { get; set; }

		@AuraEnabled
		public Decimal carbs { get; set; }

		@AuraEnabled
		public Decimal fat { get; set; }

		@AuraEnabled
		public Decimal calories { get; set; }

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a Template Pool Plan
	*/ 
	public class PoolPlanVersion extends TemplateValues {
    
        @AuraEnabled
        public String planId { get; set; }

        @AuraEnabled
        public String planRecordTypeName { get; set; }
    
        @AuraEnabled
        public String poolPlanId { get; set; }
    
        @AuraEnabled
        public Integer versionNumber { get; set; }

		@AuraEnabled
		public Datetime lastUpdatedDateTime { get; set; }
	
		@AuraEnabled
		public List<TemplatePoolBlock> blocks { get; set; }
      
		public PoolPlanVersion(Pool_Plan_Version__c poolVersion) {
            this.id						= poolVersion.Id;
            this.poolPlanId				= poolVersion.Pool_Plan__c;
            this.planId					= poolVersion.Reference_Plan__r.Id;
            this.planRecordTypeName		= poolVersion.Reference_Plan__r.RecordType.DeveloperName;
			this.name					= poolVersion.Reference_Plan__r.Name;
            this.versionNumber			= poolVersion.Version__c.intValue();
			this.lastUpdatedDateTime	= poolVersion.LastModifiedDate;

			this.protein				= poolVersion.Reference_Plan__r.Trainer_Plan_Protein__c;
			this.carbs					= poolVersion.Reference_Plan__r.Trainer_Plan_Carbs__c;
			this.fat					= poolVersion.Reference_Plan__r.Trainer_Plan_Fat__c;
			this.calories				= poolVersion.Reference_Plan__r.Trainer_Plan_Calories__c;

			this.blocks					= new List<TemplatePoolBlock>();
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a Template Pool Block
	*/ 
	public class TemplatePoolBlock extends TemplateValues {
		
		@AuraEnabled
		public List<TemplatePoolItem> items { get; set; }
  
		public TemplatePoolBlock(Block__c refBlock) {
			this.id			= refBlock.Id;
			this.name		= refBlock.Name;
			this.protein	= refBlock.Trainer_Block_Protein__c;
			this.carbs		= refBlock.Trainer_Block_Carbs__c;
			this.fat		= refBlock.Trainer_Block_Fat__c;
			this.calories	= refBlock.Trainer_Block_Calories__c;
			this.items		= new List<TemplatePoolItem>();
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a Template Pool Item
	*/ 
	public class TemplatePoolItem extends TemplateValues {

        @AuraEnabled
        public string measurement { get; set; }

        @AuraEnabled
        public Decimal quantity { get; set; }
          
		public TemplatePoolItem(Item__c refItem) {
			this.id			    = refItem.Id;
			this.name		    = refItem.Name;
			this.protein	    = refItem.Trainer_Protein__c;
			this.carbs		    = refItem.Trainer_Carbs__c;
			this.fat		    = refItem.Trainer_Fat__c;
			this.calories	    = refItem.Trainer_Calories__c;
            this.measurement    = refItem.Template_Measurement__c;
            this.quantity       = refItem.Trainer_Quantity__c;
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds Check-In information
	*/ 
	public class CheckInInformation {
		
		/**
		* @description The Id of the Check-In
		*/ 
		@AuraEnabled
		public String checkInId { get; set; }

		/**
		* @description The planning mode of the Check-In
		*/ 
		@AuraEnabled
		public String nutritionPlanningMode { get; set; }

		@AuraEnabled
		public String guidedPlanWeek { get; set; }

		public CheckInInformation(Check_In__c checkIn) {
			this.checkInId				= checkIn.Id;
			this.nutritionPlanningMode	= checkIn.Nutrition_Planning_Mode__c;
			this.guidedPlanWeek			= checkIn.Guided_Plan_Number_Weeks__c;
		}

	}

	private static List<Pool_Plan_Version__c> query_LatestClientPlans(String checkInId) {
	
        List<Pool_Plan__c> poolPlans = [
            SELECT
                Id,
                (
                    SELECT
                        Id,
                        RecordType.DeveloperName,
						LastModifiedDate,
                        Pool_Plan__c,
                        Version__c,
                        Reference_Plan__c,
                        Reference_Plan__r.RecordType.DeveloperName,
                        Reference_Plan__r.Name,
                        Reference_Plan__r.Trainer_Plan_Protein__c,
                        Reference_Plan__r.Trainer_Plan_Carbs__c,
                        Reference_Plan__r.Trainer_Plan_Fat__c,
                        Reference_Plan__r.Trainer_Plan_Calories__c
                    FROM Pool_Plan_Versions__r
                    WHERE Active_Version__c = true AND Reference_Plan__r.RecordType.DeveloperName = 'Nutrition_Client_Plan'
                )
            FROM Pool_Plan__c
            WHERE Check_In__c = :checkInId
        ];

        List<Pool_Plan_Version__c> poolPlanVersions = new List<Pool_Plan_Version__c>();
        for(Pool_Plan__c poolPlan : poolPlans) {
            if(poolPlan.Pool_Plan_Versions__r.size() > 0) {
                poolPlanVersions.add(poolPlan.Pool_Plan_Versions__r.get(0));
            }
        }
		return poolPlanVersions;
	}

	private static List<Pool_Plan_Version__c> query_LatestReferencePlans(String checkInId) {
	
        List<Pool_Plan__c> poolPlans = [
            SELECT
                Id,
                (
                    SELECT
                        Id,
                        RecordType.DeveloperName,
						LastModifiedDate,
                        Pool_Plan__c,
                        Version__c,
                        Reference_Plan__c,
                        Reference_Plan__r.RecordType.DeveloperName,
                        Reference_Plan__r.Name,
                        Reference_Plan__r.Trainer_Plan_Protein__c,
                        Reference_Plan__r.Trainer_Plan_Carbs__c,
                        Reference_Plan__r.Trainer_Plan_Fat__c,
                        Reference_Plan__r.Trainer_Plan_Calories__c
                    FROM Pool_Plan_Versions__r
                    WHERE Active_Version__c = true AND Reference_Plan__r.RecordType.DeveloperName = 'Nutrition_Reference_Plan'
                )
            FROM Pool_Plan__c
            WHERE Check_In__c = :checkInId
        ];

        List<Pool_Plan_Version__c> poolPlanVersions = new List<Pool_Plan_Version__c>();
        for(Pool_Plan__c poolPlan : poolPlans) {
            if(poolPlan.Pool_Plan_Versions__r.size() > 0) {
                poolPlanVersions.add(poolPlan.Pool_Plan_Versions__r.get(0));
            }
        }
		return poolPlanVersions;
	}

	/**
	* @description Query the active pool plan versions for the Check-In
	* @param checkInId The id of the Check-In
	* @return Returns the active pool plan versions
	*/ 
	private static List<Pool_Plan_Version__c> QueryLatestPoolPlanVersions(String checkInId) {

        List<Pool_Plan__c> poolPlans = [
            SELECT
                Id,
                (
                    SELECT
                        Id,
                        RecordType.DeveloperName,
						LastModifiedDate,
                        Pool_Plan__c,
                        Version__c,
                        Reference_Plan__c,
                        Reference_Plan__r.RecordType.DeveloperName,
                        Reference_Plan__r.Name,
                        Reference_Plan__r.Trainer_Plan_Protein__c,
                        Reference_Plan__r.Trainer_Plan_Carbs__c,
                        Reference_Plan__r.Trainer_Plan_Fat__c,
                        Reference_Plan__r.Trainer_Plan_Calories__c
                    FROM Pool_Plan_Versions__r
                    WHERE Active_Version__c = true
                )
            FROM Pool_Plan__c
            WHERE Check_In__c = :checkInId
        ];

        List<Pool_Plan_Version__c> poolPlanVersions = new List<Pool_Plan_Version__c>();
        for(Pool_Plan__c poolPlan : poolPlans) {
            if(poolPlan.Pool_Plan_Versions__r.size() > 0) {
                poolPlanVersions.add(poolPlan.Pool_Plan_Versions__r.get(0));
            }
        }
		return poolPlanVersions;
	}
    
    private static Map<Id, List<Block__c>> QueryBlocksByPlans(List<Pool_Plan_Version__c> tmpPoolPlans) {

        List<String> refPlanIds = new List<String>();

        for(Pool_Plan_Version__c tPP : tmpPoolPlans) {
            refPlanIds.add(tPP.Reference_Plan__c);
        }

        List<Block__c> blks = [
            SELECT
                Id,
                Name,
                Plan__c,
                Trainer_Block_Protein__c,
                Trainer_Block_Carbs__c,
                Trainer_Block_Fat__c,
                Trainer_Block_Calories__c,
                (
                    SELECT 
                        Id,
                        Name,
                        Template_Measurement__c,
                        Trainer_Quantity__c,
                        Trainer_Protein__c,
                        Trainer_Carbs__c,
                        Trainer_Fat__c,
                        Trainer_Calories__c
                    FROM Plan_Items__r
                )
            FROM Block__c
            WHERE Plan__c IN :refPlanIds
        ];

        Map<Id, List<Block__c>> retBlockPlans = new Map<Id, List<Block__c>>();

        for(Block__c blk : blks) {
            if(!retBlockPlans.containsKey(blk.Plan__c)) {
                retBlockPlans.put(blk.Plan__c, new List<Block__c>());
            }
            retBlockPlans.get(blk.Plan__c).add(blk);
        }

        return retBlockPlans;
    }
   
	private static List<PoolPlanVersion> GetPoolPlanVersions(String checkInId) {

		//Get the Template Values
		List<Pool_Plan_Version__c> latestPoolPlanVersions		= QueryLatestPoolPlanVersions(checkInId);

        Map<Id, List<Block__c>> aBlocks = QueryBlocksByPlans(latestPoolPlanVersions);

        List<PoolPlanVersion> tPPlans = new List<PoolPlanVersion>();

        for(Pool_Plan_Version__c tpp : latestPoolPlanVersions) {

            PoolPlanVersion newTPP = new PoolPlanVersion(tpp);
			if(aBlocks.containsKey(tpp.Reference_Plan__c)) {
				List<Block__c> relBlocks = aBlocks.get(tpp.Reference_Plan__c);
				for(Block__c blk : relBlocks) {
					TemplatePoolBlock newTPB = new TemplatePoolBlock(blk);
					for(Item__c itm : blk.Plan_Items__r) {
						TemplatePoolItem newTPI = new TemplatePoolItem(itm);
						newTPB.items.add(newTPI);
					}
					newTPP.blocks.add(newTPB);
				}
			}
            tPPlans.add(newTPP);
        }
		return tPPlans;
	}

    @AuraEnabled
    public static PoolPlanVersion RefreshLatestPoolPlanVersion(String poolPlanId) {

        Pool_Plan__c poolPlan = [
            SELECT
                Id,
                (
                    SELECT
                        Id,
                        Pool_Plan__c,
                        Version__c,
                        Reference_Plan__c,
                        Reference_Plan__r.Name,
                        Reference_Plan__r.Trainer_Plan_Protein__c,
                        Reference_Plan__r.Trainer_Plan_Carbs__c,
                        Reference_Plan__r.Trainer_Plan_Fat__c,
                        Reference_Plan__r.Trainer_Plan_Calories__c
                    FROM Pool_Plan_Versions__r
                    WHERE Active_Version__c = true
                )
            FROM Pool_Plan__c
            WHERE Id = :poolPlanId
        ];

        Map<Id, List<Block__c>> aBlocks = QueryBlocksByPlans(new Pool_Plan_Version__c[] { poolPlan.Pool_Plan_Versions__r[0] });


        PoolPlanVersion tpp = new PoolPlanVersion(poolPlan.Pool_Plan_Versions__r[0]);
        List<Block__c> relBlocks = aBlocks.get(poolPlan.Pool_Plan_Versions__r[0].Reference_Plan__c);
        for(Block__c blk : relBlocks) {
            TemplatePoolBlock newTPB = new TemplatePoolBlock(blk);
            for(Item__c itm : blk.Plan_Items__r) {
                TemplatePoolItem newTPI = new TemplatePoolItem(itm);
                newTPB.items.add(newTPI);
            }
            tpp.blocks.add(newTPB);
        }

        return tpp;
        

    }

    @AuraEnabled
    public static void SaveNewVersion(Id poolPlanId, Id newPlanId) {

        Pool_Plan__c pPlan = [
            SELECT
                Id,
                Account__c,
                Check_In__c,
                (
                    SELECT
                        Id,
                        Version__c
                    FROM Pool_Plan_Versions__r
                    ORDER BY Version__c DESC NULLS LAST
                )
            FROM Pool_Plan__c
            WHERE Id = :poolPlanId
        ];

        Plan__c newPlan = [
            SELECT
                Id
            FROM Plan__c
            WHERE Id = :newPlanId
        ];
        newPlan.Account__c  = pPlan.Account__c;
        newPlan.Check_In__c = pPlan.Check_In__c;
        update newPlan;

        //Add the new version
        Pool_Plan_Version__c newVersion = new Pool_Plan_Version__c();
        newVersion.Pool_Plan__c         = pPlan.Id;
        newVersion.Version__c           = pPlan.Pool_Plan_Versions__r.get(0).Version__c + 1;
        newVersion.Reference_Plan__c    = newPlan.Id;
        newVersion.Active_Version__c    = true;
        insert newVersion;

        //Make sure that all of the previous versions aren't active
        for(Pool_Plan_Version__c v : pPlan.Pool_Plan_Versions__r) {
            v.Active_Version__c = false;
        }

        update pPlan.Pool_Plan_Versions__r;


    }

	@AuraEnabled
	public static void SaveAsNewPoolPlan(String poolPlanVersionId, String newPlanName) {

		Pool_Plan_Version__c currVersion = [
			SELECT
				Id,
				Reference_Plan__c,
				Reference_Plan__r.Check_In__c,
				Reference_Plan__r.Account__c
			FROM Pool_Plan_Version__c
			WHERE Id = :poolPlanVersionId
		];

		RecordType rPlanType = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Client_Plan'];

		//Create a new Pool Plan
		Pool_Plan__c newPoolPlan	= new Pool_Plan__c();
		newPoolPlan.Check_In__c		= currVersion.Reference_Plan__r.Check_In__c;
		newPoolPlan.Account__c		= currVersion.Reference_Plan__r.Account__c;
		insert newPoolPlan;

		//Create the new Plan
		Id newPlanId = AT_M_Nutrition.SaveAsNewPlan(currVersion.Reference_Plan__c, rPlanType.Id).Id;


		Plan__c newPlan = [
			SELECT
				Id,
				Name,
				Account__c,
				Check_In__c
			FROM Plan__c
			WHERE Id = :newPlanId
		];
		newPlan.Name			= newPlanName;
		newPlan.Account__c		= currVersion.Reference_Plan__r.Account__c;
		newPlan.Check_In__c		= currVersion.Reference_Plan__r.Check_In__c;
		update newPlan;

		//Create a new Pool Plan Version
		Pool_Plan_Version__c newPoolPlanVersion = new Pool_Plan_Version__c();
		newPoolPlanVersion.Active_Version__c	= true;
		newPoolPlanVersion.Version__c			= 1;
		newPoolPlanVersion.Pool_Plan__c			= newPoolPlan.Id;
		newPoolPlanVersion.Reference_Plan__c	= newPlan.Id;
		insert newPoolPlanVersion;

	}

    /**
    * @description Delete a Pool Plan
    * @param poolPlanId The Id of the Pool Plan
    */ 
    @AuraEnabled
    public static void DeletePoolPlan(Id poolPlanId) {

        Pool_Plan__c pPlan = [
            SELECT 
                Id
            FROM Pool_Plan__c
            WHERE Id = :poolPlanId
        ];

        delete pPlan;

    }

	@AuraEnabled
	public static CheckInPoolPlanInformation GetData(String checkInId) {
		CheckInPoolPlanInformation checkInInfo = new CheckInPoolPlanInformation();
		checkInInfo.checkInInformation = new CheckInInformation([
			SELECT
				Id,
				Nutrition_Planning_Mode__c,
				Guided_Plan_Number_Weeks__c
			FROM Check_In__c
			WHERE Id = :checkInId
		]);
		checkInInfo.latestPoolPlanVersions = GetPoolPlanVersions(checkInId);
		return checkInInfo;
	}

}