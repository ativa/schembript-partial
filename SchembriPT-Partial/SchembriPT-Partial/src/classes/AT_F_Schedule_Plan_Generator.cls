/**
 * @description       : Generate Plan 
 * @author            : Harrison Campbell
 * @group             : Ativa
 * @last modified on  : 01-27-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   12-21-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class AT_F_Schedule_Plan_Generator {
    
    /**
    * @description Generate Plans
    * @author Harrison Campbell
    * @param reqs Requests to Generate Plans
    **/
    @InvocableMethod(label='Generate Plans' description='Generate New Plans' category='Ativa - Apex Flow Action')
    public static void GeneratePlans(List<Request> reqs) {
        for(Request req : reqs) {

			AT_B_SchedulePlanGenerator newBatch = new AT_B_SchedulePlanGenerator(
				req.category, 
				req.generationScenario, 
				req.sourceCheckInId, 
				req.targetCheckInId, 
				req.referencePlanIds, 
				req.sessionPlanMap
			);
			Database.executeBatch(newBatch);
        }
    }

    public class Request {
        
        /* Drives Logic */

        @InvocableVariable(label='Category' required=false)
        public String category;

        @InvocableVariable(label='Generation Scenario' required=false)
        public String generationScenario;

        /* Both */

        @InvocableVariable(label='Source Check-In Id' required=false)
        public String sourceCheckInId;

        @InvocableVariable(label='Target Check-In Id' required=false)
        public String targetCheckInId;

        /* Reference */

        @InvocableVariable(label='Reference Plan Ids' required=false)
        public List<String> referencePlanIds;

        /* Session */

        @InvocableVariable(label='Session Plan Map' required=false)
        public List<AT_C_SessionPlanMap> sessionPlanMap;

    }
    
}