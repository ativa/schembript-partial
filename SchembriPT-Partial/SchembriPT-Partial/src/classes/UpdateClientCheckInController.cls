public without sharing class UpdateClientCheckInController {

    /**
     * @description Update Check-In Statuses
     */
    @AuraEnabled
    public static void UpdateCheckInStatuses(string checkInId, String updateType) {
        
        Check_In__c currCheckIn = [
            SELECT 
                Id, 
                Contact__c,
                Photo_Status__c,
                Measurements_Status__c
            FROM Check_In__c
            WHERE Id = :checkInId
        ];

        if(updateType == 'Photos') {
            currCheckIn.Photo_Status__c         = 'New Photos Requested';
        } else if(updateType == 'Measurements') {
            currCheckIn.Measurements_Status__c  = 'Updated Measurements Requested';
        } else if(updateType == 'Measurements & Photos') {
            currCheckIn.Photo_Status__c         = 'New Photos Requested';
            currCheckIn.Measurements_Status__c  = 'Updated Measurements Requested';
        }

        update currCheckIn;
        
    }

    /**
     * @description Send an update Request
     */
    @AuraEnabled
    public static void SendUpdateRequest(String checkInId, String updateType){
        
        Check_In__c currCheckIn = [
            SELECT 
                Id, 
                Contact__c,
                Photo_Status__c,
                Measurements_Status__c
            FROM Check_In__c
            WHERE Id = :checkInId
        ];

        //Send an email out requesting updates
        Email_Settings__c emailSettings = Email_Settings__c.getInstance();

        String templateDevName  = '';
        String fromEmail        = emailSettings.Send_From_Email__c;
        if(updateType == 'Photos') {
            templateDevName                     = emailSettings.Photos_Template_Dev_Name__c;
        } else if(updateType == 'Measurements') {
            templateDevName                     = emailSettings.Measurements_Template_Dev_Name__c;
        } else if(updateType == 'Measurements & Photos') {
            templateDevName                     = emailSettings.Measurements_Photos_Template_Dev_Name__c;
        } else if(updateType == 'Files') {
            templateDevName                     = emailSettings.Files_Template_Dev_Name__c;
        }
        
		EmailTemplate template					= [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateDevName];
        OrgWideEmailAddress orgEmailAddress		= [SELECT Id FROM OrgWideEmailAddress WHERE Address = :fromEmail];
		Messaging.SingleEmailMessage newMessage = Messaging.renderStoredEmailTemplate(template.Id, currCheckIn.Contact__c, currCheckIn.Id);
		newMessage.setOrgWideEmailAddressId(orgEmailAddress.Id);
		Messaging.sendEmail(new Messaging.Email[] { newMessage });

        //Update the Statuses of the Photos, and Measurements after the email has been sent
        //Before will cause errors
        UpdateCheckInStatuses(checkInId, updateType);

    }



}