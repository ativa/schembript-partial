public with sharing class Template_Block_TriggerClass {

	private static List<Template_Plan__c> GetTemplatePlans(List<Template_Block__c> tmpBlocks) {
	
		RecordType nutritionRecType = [
			SELECT 
				Id 
			FROM RecordType 
			WHERE SobjectType = 'Template_Block__c' AND DeveloperName = 'Template_Nutrition_Block'
		];

		Set<Id> tmpBlockIds = new Set<Id>();

		for(Template_Block__c tmpBlock : tmpBlocks) {
			if(tmpBlock.RecordTypeId == nutritionRecType.Id) {
				tmpBlockIds.add(tmpBlock.Id);
			}
		}

		Set<Id> planIds	= new Set<Id>();

		for(Template_Plan_Block__c tmpPlanBlk : [
			SELECT
				Id,
				Template_Plan__c
			FROM Template_Plan_Block__c
			WHERE Template_Block__c IN :tmpBlockIds
		]) {
			planIds.add(tmpPlanBlk.Template_Plan__c);
		}

		return [
			SELECT
				Id,
				Template_Plan_Calories__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Protein__c,
				(
					SELECT
						Id,
						Template_Block_Calories__c,
						Template_Block_Carbs__c,
						Template_Block_Fat__c,
						Template_Block_Protein__c
					FROM Template_Blocks__r
				)
			FROM Template_Plan__c
			WHERE Id IN: planIds
		];



	}

	private static void UpdateTemplatePlans(List<Template_Plan__c> tmpPlans) {

		for(Template_Plan__c tmpPlan : tmpPlans) {

			tmpPlan.Template_Plan_Calories__c	= 0;
			tmpPlan.Template_Plan_Carbs__c		= 0;
			tmpPlan.Template_Plan_Fat__c		= 0;
			tmpPlan.Template_Plan_Protein__c	= 0;

			for(Template_Plan_Block__c tmpPlanBlocks : tmpPlan.Template_Blocks__r) {
				tmpPlan.Template_Plan_Calories__c	+= tmpPlanBlocks.Template_Block_Calories__c;
				tmpPlan.Template_Plan_Carbs__c		+= tmpPlanBlocks.Template_Block_Carbs__c;
				tmpPlan.Template_Plan_Fat__c		+= tmpPlanBlocks.Template_Block_Fat__c;
				tmpPlan.Template_Plan_Protein__c	+= tmpPlanBlocks.Template_Block_Protein__c;
			}

		}

		update tmpPlans;

	}

	public static void afterInsert(List<Template_Block__c> tmpBlocks) {
		UpdateTemplatePlans(GetTemplatePlans(tmpBlocks));
	}

	public static void afterUpdate(List<Template_Block__c> tmpBlocks) {
		UpdateTemplatePlans(GetTemplatePlans(tmpBlocks));
	}

	public static void afterDelete(List<Template_Block__c> tmpBlocks) {
		UpdateTemplatePlans(GetTemplatePlans(tmpBlocks));
	}

}