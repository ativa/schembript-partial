/**
* @author Harrison Campbell
* @description Quickchart Adaptor that allows us to connect to https://quickchart.io/
*/ 
public class QuickChart_Adaptor {

	private static QuickChart_Settings__c settings = QuickChart_Settings__c.getInstance();

	public class ShortURLRequestPacket {

		public String key { get; set; }

		public ChartJS_Chart chart { get; set; }

		public Integer width { get; set; }

		public Integer height { get; set; }

	}
	
	public class ShortURLResponsePacket {

		public Boolean success { get; set; }

		public String url { get; set; }

	}

	/**
	* @description Constructs the url for the image
	* @param chartJs The ChartJS object
	* @return Returns a url that loads the Chart
	*/ 
	public static String getImageURL(ChartJS_Chart chartJs) {
		String url = 'https://quickchart.io/chart?c=' + JSON.serialize(chartJs);
		return url;
	}
	
	/**
	* @description Constructs the url for the image
	* @param chartJs The ChartJS object
	* @param width The width in pixels of the chart
	* @param height the height in pixesl of the chart
	* @return Returns a url that loads the Chart
	*/ 
	public static String getImageURL(ChartJS_Chart chartJs, Integer width, Integer height) {
		String url = 'https://quickchart.io/chart?c=' + JSON.serialize(chartJs) + '&width=' + width + '&height=' + height;
		return url;
	}
	
	public static String createShortURL(ChartJS_Chart chartJs, Integer width, Integer height) {
	
		string shortURLEndpoint = 'https://quickchart.io/chart/create';

		ShortURLRequestPacket urlPacket	= new ShortURLRequestPacket();
		urlPacket.key					= settings.API_Key__c;
		urlPacket.chart					= chartJs;
		urlPacket.width					= width;
		urlPacket.height				= height;

		Http hp = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(shortURLEndpoint);
		req.setMethod('POST');
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(60000);
		req.setBody(JSON.serialize(urlPacket, true));

		HttpResponse res = hp.send(req);
		ShortURLResponsePacket urlResponse = (ShortURLResponsePacket)JSON.deserialize(res.getBody(), ShortURLResponsePacket.class);
		return urlResponse.url;

	}


}