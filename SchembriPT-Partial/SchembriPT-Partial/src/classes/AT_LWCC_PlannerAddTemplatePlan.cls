/**
* @author Harrison Campbell
* @description Controller for the 'plannerXAddTemplatePlan' Lighning Web Component
*/ 
public without sharing class AT_LWCC_PlannerAddTemplatePlan {

    /**
    * @author Harrison Campbell
    * @description Holds Information on a TemplatePlan
    */ 
    public class TemplatePlan {

        /**
        * @description The Id of the Plan
        */ 
        @AuraEnabled 
		public String id { get; set; }

        /**
        * @description The Template Plan Id
        */ 
        @AuraEnabled 
		public String templateID { get; set; }

        /**
        * @description The name of the Plan
        */ 
        @AuraEnabled 
		public String name { get; set; }

        /**
        * @description The Template Blocks related to the Plan
        */ 
        @AuraEnabled 
		public List<TemplateBlock> blocks { get; set; }

		/**
		* @description Is this AM or PM
		*/ 
		@AuraEnabled
		public String trainingTime { get; set; }

        @AuraEnabled 
		public List<String> allergies { get; set; }

        @AuraEnabled 
		public List<String> restrictions { get; set; }

		/**
		* @description The Total Protein of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal protein { get; set; }
		
		/**
		* @description The Total Fat of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal fat { get; set; }
		
		/**
		* @description The Total Carbs of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal carbs { get; set; }

        /**
        * @description The Total Calories of the Template Plan
        */ 
        @AuraEnabled 
		public Decimal calories { get; set; }

		public TemplatePlan(Template_Plan__c tmpPlan) {
			this.id				= tmpPlan.Id;
			this.templateID		= tmpPlan.Id;
			this.name			= tmpPlan.Name;
			this.blocks			= new List<TemplateBlock>();
			this.protein		= tmpPlan.Template_Plan_Protein__c;
			this.fat			= tmpPlan.Template_Plan_Fat__c;
			this.carbs			= tmpPlan.Template_Plan_Carbs__c;
			this.calories		= tmpPlan.Template_Plan_Calories__c;
			this.allergies		= new List<String>();
			this.restrictions	= new List<String> { 'Vegetarian', 'Vegan' };
			this.trainingTime	= tmpPlan.Training_Time__c;
		}


    }
	
    /**
    * @author Harrison Campbell
    * @description Holds Information on a Template Block
    */ 
    public class TemplateBlock {

        /**
        * @description The Id of the Block
        */ 
        @AuraEnabled 
		public String id { get; set; }

		/**
        * @description The Id of the Block
        */
        @AuraEnabled 
		public String templateID { get; set; }

        /**
        * @description The name of the Block
        */ 
        @AuraEnabled 
		public String name { get; set; }

        /**
        * @description The Order of the block within the plan
        */ 
        @AuraEnabled 
		public Integer index { get; set; }

		@AuraEnabled
		public Decimal calories { get; set; }

		@AuraEnabled
		public Decimal fat { get; set; }

		@AuraEnabled
		public Decimal protein { get; set; }

		@AuraEnabled
		public Decimal carbs { get; set; }

        /**
        * @description The Items Related to the Block
        */ 
        @AuraEnabled 
		public List<TemplateItem> items { get; set; }

		public TemplateBlock(Template_Plan_Block__c blk) {
            this.id				= blk.Id;
            this.templateID		= blk.Id;
            this.index			= blk.Order__c == null ? 0 : blk.Order__c.intValue();
            this.name			= blk.Template_Block__r.Name;
			this.items			= new List<TemplateItem>();
			this.calories		= blk.Template_Block_Calories__c;
			this.fat			= blk.Template_Block_Fat__c;
			this.protein		= blk.Template_Block_Protein__c;
			this.carbs			= blk.Template_Block_Carbs__c;
		}

    }
	
    /**
    * @author Harrison Campbell
    * @description Holds Information on an Template Item
    */ 
    public class TemplateItem {

        /**
        * @description A Unique Key to identify the Item 
        */ 
        @AuraEnabled 
		public String key { get; set; } 

        /**
        * @description The Order of the Item
        */ 
        @AuraEnabled 
		public Integer index { get; set; }

        /**
        * @description The Id of the Item
        */ 
        @AuraEnabled 
		public String id { get; set; }

        @AuraEnabled 
		public String blockItemID { get; set; }

        @AuraEnabled public String templateID;
        @AuraEnabled public String name;
        @AuraEnabled public String measure;
        @AuraEnabled public Boolean divisible;
        @AuraEnabled public Decimal increment;

        @AuraEnabled public Decimal quantity = 0;

        @AuraEnabled public Decimal protein = 0;

        @AuraEnabled public Decimal fat = 0;

        @AuraEnabled public Decimal carbs = 0;

        @AuraEnabled public Decimal calories = 0;

		/**
		* @description The minimum quantity for the item
		*/ 
		@AuraEnabled
		public Decimal minimumQuantity { get; set; }

		
		/**
		* @description If the value us 'null' then set it to 0
		* @param value The value we are checking if null
		* @return Returns the Value if set, otherwise returns 0
		*/ 
		private Decimal Zero(Decimal value) {
			return (value == null) ? 0 : value;
		}

		public TemplateItem(Template_Block_Item__c itm) {

			Decimal itmQuantity		= (itm.Quantity__c != null) ? itm.Quantity__c : 0;
			Decimal itmTmpQuantity	= (itm.Template_Item__r.Item_Quantity__c != null && itm.Template_Item__r.Item_Quantity__c != 0) ? itm.Template_Item__r.Item_Quantity__c : 1;

            this.id					= itm.Id;
            this.blockItemID		= itm.Id;
            this.templateID			= itm.Template_Item__c;
            this.name				= itm.Template_Item__r.Name;
            this.measure			= itm.Template_Item__r.Measurement__c;
            this.divisible			= (itm.Template_Item__r.Measurement__c == 'Grams'); 
            this.increment			= itm.Template_Item__r.Increment__c;
            this.quantity			= Zero(itm.Quantity__c);
            this.carbs				= Zero(itm.Template_Item__r.Carbs__c) * itmQuantity / itmTmpQuantity;
            this.protein			= Zero(itm.Template_Item__r.Protein__c) * itmQuantity / itmTmpQuantity;
            this.fat				= Zero(itm.Template_Item__r.Fat__c) * itmQuantity / itmTmpQuantity;
            this.calories			= Zero(itm.Template_Item__r.Calories__c)  * itmQuantity / itmTmpQuantity;
			this.minimumQuantity	= Zero(itm.Template_Item__r.Minimum_Quantity__c) * itmQuantity / itmTmpQuantity;
		}

    }
	
	/**
	* @description Get All Groups of Template Block Items, Grouped by their corresponding Template Blocks
	* @return 
	*/ 
	private static Map<Id, List<Template_Block_Item__c>> GetAllTemplateBlockItemGroups() {

		Map<Id, List<Template_Block_Item__c>> tmpBlkItemMap = new Map<Id, List<Template_Block_Item__c>>();

        for (Template_Block_Item__c itm: [
			SELECT 
				Template_Block__c, 
				Template_Item__c, 
				Template_Item__r.Name, 
				Template_Item__r.Measurement__c, 
				Template_Item__r.Divisible__c, 
				Template_Item__r.Increment__c, 
				Template_Item__r.Item_Quantity__c,
				Quantity__c, 
				Template_Item__r.Carbs__c, 
				Template_Item__r.Protein__c, 
				Template_Item__r.Fat__c, 
				Template_Item__r.Calories__c, 
				Template_Item__r.Allergy__c, 
				Template_Item__r.Restriction__c,
				Template_Item__r.Minimum_Quantity__c 
			FROM Template_Block_Item__c
			ORDER BY Display_Order__c ASC
		]) {
            List<Template_Block_Item__c> items = tmpBlkItemMap.get(itm.Template_Block__c);
            if (items == null) {
                items = new List<Template_Block_Item__c>();
                tmpBlkItemMap.put(itm.Template_Block__c, items);
            }

            items.add(itm);
        }

		return tmpBlkItemMap;

	}

	/**
	* @description Get All Groups of Template Blocks, Grouped by their corresponding Template Plans
	* @return 
	*/ 
	private static Map<Id, List<Template_Plan_Block__c>> GetAllTemplateBlockGroups() {
	
        Map<Id, List<Template_Plan_Block__c>> planBlocks = new Map<Id, List<Template_Plan_Block__c>>();

        for (Template_Plan_Block__c blk: [
			SELECT 
				Id, 
				Template_Plan__c, 
				Template_Block__c, 
				Order__c, 
				Template_Block__r.Name,
				Template_Block_Calories__c,
				Template_Block_Fat__c,
				Template_Block_Protein__c,
				Template_Block_Carbs__c
			FROM Template_Plan_Block__c 
			ORDER BY Order__c ASC
		]) {
            List<Template_Plan_Block__c> blocks = planBlocks.get(blk.Template_Plan__c);

            if (blocks == null) {
                blocks = new List<Template_Plan_Block__c>();

                planBlocks.put(blk.Template_Plan__c, blocks);
            }

            blocks.add(blk);
        }

		return planBlocks;

	}

	/**
	* @description Get All of the Template Plans in the Org
	* @return Returns all the Template Plans
	*/ 
	@AuraEnabled
	public static List<TemplatePlan> GetTemplatePlans(String checkInId, String accountId) {

		Id clientId = null;

		if(checkInId != null) {
			Check_In__c checkIn = [
				SELECT
					Id,
					Account__r.SF_Community_User__pc
				FROM Check_In__c
				WHERE Id = :checkInId
			];
			clientId = checkIn.Account__r.SF_Community_User__pc;
		} else if(accountId != null) {
			Account acc = [
				SELECT
					Id,
					SF_Community_User__pc
				FROM Account
				WHERE Id = :accountId
			];
			clientId = acc.SF_Community_User__pc;
		}

		RecordType tmpNutPlanRecType = [
			SELECT
				Id
			FROM RecordType
			WHERE DeveloperName = 'Template_Nutrition_Plan' AND SobjectType = 'Template_Plan__c'
		];

		//Get A Map of the Template Block Items, grouped by their Template Block
        Map<Id, List<Template_Block_Item__c>> blockItems = GetAllTemplateBlockItemGroups();
        Map<Id, List<Template_Plan_Block__c>> planBlocks = GetAllTemplateBlockGroups();

		List<TemplatePlan> allPlans = new List<TemplatePlan>();

		System.debug('Searching Template Plans');

		for(Template_Plan__c tmpl : [
			SELECT 
				Id, 
				Name,
				Template_Plan_Calories__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Protein__c,
				Training_Time__c
			FROM Template_Plan__c 
			WHERE RecordTypeId = :tmpNutPlanRecType.Id AND (Status__c = 'Public' OR (Status__c = 'Client Specific' AND Client__c = :clientId))
			ORDER BY Name ASC
		]) {
		
			System.debug('Template Plan: ' + tmpl.Id);

			//Add the Template Plan Information
			TemplatePlan template = new TemplatePlan(tmpl);

            Boolean hasItems = false; //Track if the Template has template Items associated with it, if it doesn't then ignore it


			if(planBlocks.containsKey(tmpl.Id)) {

				for (Template_Plan_Block__c blk: planBlocks.get(tmpl.Id)) {

					//Add the Block Information
					TemplateBlock block = new TemplateBlock(blk);
	
					Integer index = 0;
	
					if(blockItems.containsKey(blk.Template_Block__c)) {

						for (Template_Block_Item__c itm: blockItems.get(blk.Template_Block__c)) {
	
							//Add the item information, and set the order of the items
							TemplateItem item	= new TemplateItem(itm);
							item.key			= (block.index + '-' + index);
							item.index			= index;
		
							//Add the Allergy Information, and Restrictions
							if (String.isNotBlank(itm.Template_Item__r.Allergy__c)) {
								String[] values = itm.Template_Item__r.Allergy__c.split(';');
								template.allergies = values;
							}
		
							Set<String> check = new Set<String>();
							check.addAll(template.restrictions);
							check.retainAll(String.isBlank(itm.Template_Item__r.Restriction__c) ? new List<String>() : itm.Template_Item__r.Restriction__c.split(';'));
							template.restrictions = new List<String>(check);
		
							block.items.add(item);
		
							hasItems = true;
		
							index++;
						}
					}
	
					template.blocks.add(block);
				}
			}

			allPlans.add(template);

		}

		return allPlans;

	}
	
	private static Map<Id, Template_Plan__c> QueryTemplatePlans(List<String> templatePlanIds) {
		return new Map<Id, Template_Plan__c>([
			SELECT 
				Id,
				Template_Plan_Protein__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Calories__c 
			FROM Template_Plan__c 
			WHERE Id IN :templatePlanIds
		]);
	}

	private static Map<Id, List<Template_Plan_Block__c>> QueryTemplatePlanBlocks(List<String> templatePlanIds) {
		List<Template_Plan_Block__c> allTemplatePlanBlocks = [
			SELECT 
				Id, 
				Order__c,
				Template_Plan__c,
				Template_Block__c,
				Template_Block_Protein__c, 
				Template_Block_Carbs__c, 
				Template_Block_Fat__c, 
				Template_Block_Calories__c
			FROM Template_Plan_Block__c 
			WHERE Template_Plan__c IN :templatePlanIds
			ORDER BY Order__c ASC
		];
		Map<Id, List<Template_Plan_Block__c>> tmpPlanBlockMap = new Map<Id, List<Template_Plan_Block__c>>();
		for(String tpId : templatePlanIds) {
			tmpPlanBlockMap.put(tpId, new List<Template_Plan_Block__c>());
		}
		for(Template_Plan_Block__c tPlanBlock : allTemplatePlanBlocks) {
			tmpPlanBlockMap.get(tPlanBlock.Template_Plan__c).add(tPlanBlock);
		}
		return tmpPlanBlockMap;
	}

	private static Map<Id, Template_Block__c> QueryTemplateBlocks(List<String> templateBlockIds) {
		return new Map<Id, Template_Block__c>([
			SELECT 
				Id, 
				Name 
			FROM Template_Block__c 
			WHERE Id IN :templateBlockIds
		]);
	}

	private static Map<Id, List<Template_Block_Item__c>> QueryTemplateBlockItems(List<String> templateBlockIds) {

		List<Template_Block_Item__c> tmpBlockItems = [
			SELECT 
				Id,
				Name,
				Template_Item__c,
				Template_Block__c,
				Quantity__c,
				Template_Item__r.Item_Quantity__c,
				Template_Item__r.Protein__c,
				Template_Item__r.Carbs__c,
				Template_Item__r.Fat__c,
				Template_Item__r.Calories__c,
				Template_Item__r.Measurement__c,
				Template_Item__r.Name,
				Template_Item__r.Minimum_Quantity__c,
				Template_Item__r.Divisible__c,
				Display_Order__c,
				Template_Item_Quantity_Increments__c
			FROM Template_Block_Item__c 
			WHERE Template_Block__c IN : templateBlockIds
			ORDER BY Display_Order__c ASC
		];

		Map<Id, List<Template_Block_Item__c>> tmpBlockItemMap = new Map<Id, List<Template_Block_Item__c>>();
		for(String tpId : templateBlockIds) {
			tmpBlockItemMap.put(tpId, new List<Template_Block_Item__c>());
		}
		for(Template_Block_Item__c tBlockItem : tmpBlockItems) {
			tmpBlockItemMap.get(tBlockItem.Template_Block__c).add(tBlockItem);
		}

		return tmpBlockItemMap;

	}

	/**
	* @description Save a New Reference Plan From a Template
	* @param checkInId The Id of the related Check-In
	* @param newName The New Name of the Plan
	* @param templatePlanId The Id of the Template Plan we want to copy
	* @returns Returns the Id newly created Reference Plan
	*/ 
	@AuraEnabled
	public static Id SaveAsReferencePlan(String checkInId, String newName, String templatePlanId) {
	
		//Query the Check-In
        Check_In__c checkIn = [
			SELECT
				Nutrition_Plan_Start_Date__c,
				Nutrition_Plan_End_Date__c,
				Account__c 
			FROM Check_In__c 
			WHERE Id = :checkInId
		];
		
        Id refPlanRecTypeId		= Schema.SObjectType.Plan__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Reference_Plan').getRecordTypeId();
		Id refBlockRecTypeId	= Schema.SObjectType.Block__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Reference_Block').getRecordTypeId();
		Id refItemRecTypeId		= Schema.SObjectType.Item__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Reference_Item').getRecordTypeId();

		//Query all of the template information
		Template_Plan__c tmpPlan					= QueryTemplatePlans(new String[]{ templatePlanId }).get(templatePlanId);
		List<Template_Plan_Block__c> tmpPlanBlkLst	= QueryTemplatePlanBlocks(new String[]{ templatePlanId }).get(templatePlanId);
		List<Id> tmpBlockIds = new List<Id>();
		for(Template_Plan_Block__c tmpPlanBlk : tmpPlanBlkLst) { tmpBlockIds.add(tmpPlanBlk.Template_Block__c); }
		Map<Id, Template_Block__c> tmpBlockMap = QueryTemplateBlocks(tmpBlockIds);
		Map<Id, List<Template_Block_Item__c>> tmpBlockItemMap = QueryTemplateBlockItems(tmpBlockIds);

		//Create a Reference Plan
		Plan__c newRefPlan = new Plan__c(
			Name							= newName,
			Check_In__c						= checkIn.Id,
			Account__c						= checkIn.Account__c,
			Template_Plan__c				= tmpPlan.Id,
			RecordTypeId					= refPlanRecTypeId,
			Template_Plan_Carbs__c			= tmpPlan.Template_Plan_Carbs__c,
			Template_Plan_Protein__c		= tmpPlan.Template_Plan_Protein__c,
			Template_Plan_Fat__c			= tmpPlan.Template_Plan_Fat__c,
			Template_Plan_Calories__c		= tmpPlan.Template_Plan_Calories__c,
			Trainer_Plan_Carbs__c			= tmpPlan.Template_Plan_Carbs__c,
			Trainer_Plan_Protein__c			= tmpPlan.Template_Plan_Protein__c,
			Trainer_Plan_Fat__c				= tmpPlan.Template_Plan_Fat__c,
			Trainer_Plan_Calories__c		= tmpPlan.Template_Plan_Calories__c,
			Reference_Start_Date__c			= checkIn.Nutrition_Plan_Start_Date__c,
			Reference_End_Date__c			= checkIn.Nutrition_Plan_End_Date__c,
			Check_In_nr__c					= checkIn.Id,
			Account_nr__c					= checkIn.Account__c
		);

		insert newRefPlan;

		//Create new Blocks based on the Template Blocks
		Map<Id, Block__c> newBlkMap = new Map<Id, Block__c>();
		for(Template_Plan_Block__c tpb : tmpPlanBlkLst) {
			Template_Block__c tBlock = tmpBlockMap.get(tpb.Template_Block__c);
			Block__c newBlk = new Block__c(
				Name						= tBlock.Name, 
				Block_Short_Name__c			= tBlock.Name,
				RecordTypeId				= refBlockRecTypeId,
				Plan__c						= newRefPlan.Id, 
				Display_Order__c			= tpb.Order__c, 
				Template_Block_Protein__c	= tpb.Template_Block_Protein__c,
				Template_Block_Carbs__c		= tpb.Template_Block_Carbs__c,
				Template_Block_Fat__c		= tpb.Template_Block_Fat__c,
				Template_Block_Calories__c	= tpb.Template_Block_Calories__c,
				Trainer_Block_Protein__c	= tpb.Template_Block_Protein__c,
				Trainer_Block_Carbs__c		= tpb.Template_Block_Carbs__c,
				Trainer_Block_Fat__c		= tpb.Template_Block_Fat__c,
				Trainer_Block_Calories__c	= tpb.Template_Block_Calories__c,
				Template_Plan_Block__c		= tpb.Id,
				Template_Block_v2__c		= tpb.Template_Block__c
			);

			newBlkMap.put(tBlock.Id, newBlk);
		}

		if(newBlkMap.size() > 0) {
			insert newBlkMap.values();
		}

		//Create new Items based on the Template Items

		List<Item__c> newItems = new List<Item__c>();
		for(Template_Plan_Block__c tpb : tmpPlanBlkLst) {

			Template_Block__c tBlock	= tmpBlockMap.get(tpb.Template_Block__c);
			Block__c nBlk				= newBlkMap.get(tpb.Template_Block__c);
			
			for(Template_Block_Item__c tmpBlockItem : tmpBlockItemMap.get(tBlock.Id)) {
				
				Decimal tmpItemQuantity = (tmpBlockItem.Template_Item__r.Item_Quantity__c != null && tmpBlockItem.Template_Item__r.Item_Quantity__c != 0) ? tmpBlockItem.Template_Item__r.Item_Quantity__c : 1; //HACK

				Decimal itmQuantity = (tmpBlockItem.Quantity__c != null) ? tmpBlockItem.Quantity__c : 1; //HACK

				Decimal proteinPerUnit	= tmpBlockItem.Template_Item__r.Protein__c / tmpItemQuantity;
				Decimal carbsPerUnit	= tmpBlockItem.Template_Item__r.Carbs__c / tmpItemQuantity;
				Decimal fatPerUnit		= tmpBlockItem.Template_Item__r.Fat__c / tmpItemQuantity;
				Decimal caloriesPerUnit = tmpBlockItem.Template_Item__r.Calories__c / tmpItemQuantity;

				Item__c newItem = new Item__c(
					Name							= tmpBlockItem.Template_Item__r.Name, 
					RecordTypeId					= refItemRecTypeId,
					Display_Order__c				= tmpBlockItem.Display_Order__c, 
					Block__c						= nBlk.Id,
					Template_Item_Id__c				= tmpBlockItem.Template_Item__c, 
					Template_Block_Item__c			= tmpBlockItem.Id, 
					Template_Protein__c				= proteinPerUnit	* itmQuantity,
					Template_Carbs__c				= carbsPerUnit		* itmQuantity,
					Template_Fat__c					= fatPerUnit		* itmQuantity,
					Template_Calories__c			= caloriesPerUnit	* itmQuantity,
					Template_Quantity__c			= itmQuantity,
					Template_Measurement__c			= tmpBlockItem.Template_Item__r.Measurement__c,
					Template_Minimum_Quantity__c	= tmpBlockItem.Template_Item__r.Minimum_Quantity__c,
					Template_Item_Name__c			= tmpBlockItem.Template_Item__r.Name,
					Trainer_Protein__c				= proteinPerUnit	* itmQuantity,
					Trainer_Carbs__c				= carbsPerUnit		* itmQuantity,
					Trainer_Fat__c					= fatPerUnit		* itmQuantity,
					Trainer_Calories__c				= caloriesPerUnit	* itmQuantity,
					Trainer_Quantity__c				= itmQuantity,
					Template_Quantity_Increment__c	= tmpBlockItem.Template_Item_Quantity_Increments__c
				);

				newItems.add(newItem);

			}
		}

		if(newItems.size() > 0) {
			insert newItems;
		}
		
		if(newBlkMap.values().size() > 0) {
			update newBlkMap.values();
		}

		return newRefPlan.Id;

	}

	/**
	* @description Save the Template Plan as a Nutrition Client Plan
	* @param accountId The Id of related account
	* @param newName The name of the Plan
	* @param templatePlanId The Template Plan Id we are copying
	* @return Returns the Id of the Client Plan
	*/ 
	@AuraEnabled
	public static Id SaveAsClientPlan(string accountId, String newName, String templatePlanId) {
	
        Id clientPlanRecTypeId		= Schema.SObjectType.Plan__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Client_Plan').getRecordTypeId();
		Id clientBlockRecTypeId		= Schema.SObjectType.Block__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Client_Block').getRecordTypeId();
		Id clientItemRecTypeId		= Schema.SObjectType.Item__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Client_Item').getRecordTypeId();
		
		//Query all of the template information
		Template_Plan__c tmpPlan					= QueryTemplatePlans(new String[]{ templatePlanId }).get(templatePlanId);
		List<Template_Plan_Block__c> tmpPlanBlkLst	= QueryTemplatePlanBlocks(new String[]{ templatePlanId }).get(templatePlanId);
		List<Id> tmpBlockIds = new List<Id>();
		for(Template_Plan_Block__c tmpPlanBlk : tmpPlanBlkLst) { tmpBlockIds.add(tmpPlanBlk.Template_Block__c); }
		Map<Id, Template_Block__c> tmpBlockMap = QueryTemplateBlocks(tmpBlockIds);
		Map<Id, List<Template_Block_Item__c>> tmpBlockItemMap = QueryTemplateBlockItems(tmpBlockIds);

		//Create a Reference Plan
		Plan__c newRefPlan = new Plan__c(
			Name							= newName,
			Account__c						= accountId,
			Template_Plan__c				= tmpPlan.Id,
			RecordTypeId					= clientPlanRecTypeId,
			Template_Plan_Carbs__c			= tmpPlan.Template_Plan_Carbs__c,
			Template_Plan_Protein__c		= tmpPlan.Template_Plan_Protein__c,
			Template_Plan_Fat__c			= tmpPlan.Template_Plan_Fat__c,
			Template_Plan_Calories__c		= tmpPlan.Template_Plan_Calories__c,
			Client_Plan_Carbs__c			= tmpPlan.Template_Plan_Carbs__c,
			Client_Plan_Protein__c			= tmpPlan.Template_Plan_Protein__c,
			Client_Plan_Fat__c				= tmpPlan.Template_Plan_Fat__c,
			Client_Plan_Calories__c			= tmpPlan.Template_Plan_Calories__c,
			Account_ns__c					= accountId
		);

		insert newRefPlan;

		//Create new Blocks based on the Template Blocks
		Map<Id, Block__c> newBlkMap = new Map<Id, Block__c>();
		for(Template_Plan_Block__c tpb : tmpPlanBlkLst) {
			Template_Block__c tBlock = tmpBlockMap.get(tpb.Template_Block__c);
			Block__c newBlk = new Block__c(
				Name						= tBlock.Name, 
				Block_Short_Name__c			= tBlock.Name,
				RecordTypeId				= clientBlockRecTypeId,
				Plan__c						= newRefPlan.Id, 
				Display_Order__c			= tpb.Order__c, 
				Template_Block_Protein__c	= tpb.Template_Block_Protein__c,
				Template_Block_Carbs__c		= tpb.Template_Block_Carbs__c,
				Template_Block_Fat__c		= tpb.Template_Block_Fat__c,
				Template_Block_Calories__c	= tpb.Template_Block_Calories__c,
				Client_Block_Protein__c		= tpb.Template_Block_Protein__c,
				Client_Block_Carbs__c		= tpb.Template_Block_Carbs__c,
				Client_Block_Fat__c			= tpb.Template_Block_Fat__c,
				Client_Block_Calories__c	= tpb.Template_Block_Calories__c,
				Template_Plan_Block__c		= tpb.Id,
				Template_Block_v2__c		= tpb.Template_Block__c
			);

			newBlkMap.put(tBlock.Id, newBlk);
		}

		if(newBlkMap.size() > 0) {
			insert newBlkMap.values();
		}

		//Create new Items based on the Template Items

		List<Item__c> newItems = new List<Item__c>();
		for(Template_Plan_Block__c tpb : tmpPlanBlkLst) {

			Template_Block__c tBlock	= tmpBlockMap.get(tpb.Template_Block__c);
			Block__c nBlk				= newBlkMap.get(tpb.Template_Block__c);
			
			for(Template_Block_Item__c tmpBlockItem : tmpBlockItemMap.get(tBlock.Id)) {
				
				Decimal tmpItemQuantity = (tmpBlockItem.Template_Item__r.Item_Quantity__c != null && tmpBlockItem.Template_Item__r.Item_Quantity__c != 0) ? tmpBlockItem.Template_Item__r.Item_Quantity__c : 1; //HACK

				Decimal itmQuantity = (tmpBlockItem.Quantity__c != null) ? tmpBlockItem.Quantity__c : 1; //HACK

				Decimal proteinPerUnit	= tmpBlockItem.Template_Item__r.Protein__c / tmpItemQuantity;
				Decimal carbsPerUnit	= tmpBlockItem.Template_Item__r.Carbs__c / tmpItemQuantity;
				Decimal fatPerUnit		= tmpBlockItem.Template_Item__r.Fat__c / tmpItemQuantity;
				Decimal caloriesPerUnit = tmpBlockItem.Template_Item__r.Calories__c / tmpItemQuantity;

				Item__c newItem = new Item__c(
					Name							= tmpBlockItem.Template_Item__r.Name, 
					RecordTypeId					= clientItemRecTypeId,
					Display_Order__c				= tmpBlockItem.Display_Order__c, 
					Block__c						= nBlk.Id,
					Template_Item_Id__c				= tmpBlockItem.Template_Item__c, 
					Template_Block_Item__c			= tmpBlockItem.Id, 
					Template_Protein__c				= proteinPerUnit	* itmQuantity,
					Template_Carbs__c				= carbsPerUnit		* itmQuantity,
					Template_Fat__c					= fatPerUnit		* itmQuantity,
					Template_Calories__c			= caloriesPerUnit	* itmQuantity,
					Template_Quantity__c			= itmQuantity,
					Template_Measurement__c			= tmpBlockItem.Template_Item__r.Measurement__c,
					Template_Minimum_Quantity__c	= tmpBlockItem.Template_Item__r.Minimum_Quantity__c,
					Template_Item_Name__c			= tmpBlockItem.Template_Item__r.Name,
					Client_Protein__c				= proteinPerUnit	* itmQuantity,
					Client_Carbs__c					= carbsPerUnit		* itmQuantity,
					Client_Fat__c					= fatPerUnit		* itmQuantity,
					Client_Calories__c				= caloriesPerUnit	* itmQuantity,
					Client_Quantity__c				= itmQuantity,
					Template_Quantity_Increment__c	= tmpBlockItem.Template_Item_Quantity_Increments__c
				);

				newItems.add(newItem);

			}
		}

		if(newItems.size() > 0) {
			insert newItems;
		}
		
		if(newBlkMap.values().size() > 0) {
			update newBlkMap.values();
		}

		return newRefPlan.Id;



	}


}