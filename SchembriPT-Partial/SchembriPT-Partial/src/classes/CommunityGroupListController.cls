/**
* @author Harrison Campbell
* @description Controller for the CommunityGroupList controller
*/ 
public with sharing class CommunityGroupListController {

	/**
	* @author Harrison Campbell
	* @description Throws an exception if the Page Index, or Page Size is less than 0
	*/ 
	public class InvalidPageParameterException extends Exception {}

	/**
	* @author Harrison Campbell
	* @description Page details for a Chatter Group
	*/ 
	public class ChatterGroupPage {

		/**
		* @description The total number of groups available to query
		*/ 
		@AuraEnabled
		public Integer totalGroupCount { get; set; }
		
		/**
		* @description Collaboration Groups
		*/ 
		@AuraEnabled
		public List<CollaborationGroup> collabGroups { get; set; }

		public ChatterGroupPage(List<CollaborationGroup> collabGroups, Integer totalGroupCount) {
			this.collabGroups		= collabGroups;
			this.totalGroupCount	= totalGroupCount;
		}

	}

	
	/**
	* @description Get all of the chatter groups for a page
	* @param pageIndex The index of the page
	* @param pageSize The Size of the page
	* @return Returns A Chatter Group Page
	*/ 
	@AuraEnabled
	public static ChatterGroupPage getGroupPage(Integer pageIndex, Integer pageSize) {

		if(pageIndex < 0 || pageSize < 0) {
			throw new InvalidPageParameterException();
		} 

		Integer recordOffset = pageIndex * pageSize;
		List<CollaborationGroup> collGroups = [
			SELECT
				Id,
				Name,
				SmallPhotoUrl,
				LastFeedModifiedDate
			FROM CollaborationGroup
			WHERE CollaborationType = 'Public' AND IsArchived = false 
			ORDER BY Name ASC
			LIMIT :pageSize OFFSET :recordOffset
		];
		List<CollaborationGroup> allGroups = [
			SELECT
				Id
			FROM CollaborationGroup
			WHERE CollaborationType = 'Public' AND IsArchived = false
		];

		return new ChatterGroupPage(collGroups, allGroups.size());

	}


}