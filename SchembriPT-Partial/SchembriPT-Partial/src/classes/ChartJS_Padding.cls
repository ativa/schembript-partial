public class ChartJS_Padding  {

	@AuraEnabled
	public Integer left { get; set; }

	@AuraEnabled
	public Integer right { get; set; }

	@AuraEnabled
	public Integer top { get; set; }

	@AuraEnabled
	public Integer bottom { get; set; }

}