/**
* @author Harrison Campbell
* @description A Flow Action that copies the value from one field to another
*/ 
public class AT_F_CopyFieldValue  {

	@InvocableMethod(label='Copy Field Value' description='Copy the details of the Field to another field' category='Ativa - Apex Flow Action')
	public static void CopyFieldValue(List<Request> reqs) {

		for(Request req : reqs) {
			SObject srcObject	= req.srcObject;
			SObject destObject	= req.destObject;
			destObject.put(req.destField, srcObject.get(req.srcField));
		}

	}
	
	/**
	* @author Harrison Campbell
	* @description The Copy Field Request
	*/ 
	public class Request {

		@InvocableVariable(label='Source Sobject' description='The Object that we are copying the field value from' required=true)
		public SObject srcObject;
		
		@InvocableVariable(label='Destination Sobject' description='The Object that we are saving the field value too' required=true)
		public SObject destObject;
		
		@InvocableVariable(label='Source Field' description='The Field API Name that we are copying (Attached to the Source SObject)' required=true)
		public String srcField;
				
		@InvocableVariable(label='Destination Field' description='The Field API Name that we are saving (Attached to the Destination SObject)' required=true)
		public String destField;

	}


}