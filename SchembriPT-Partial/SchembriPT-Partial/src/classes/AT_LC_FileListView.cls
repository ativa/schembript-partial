public without sharing class AT_LC_FileListView  {

	public class TrainingVideo {
	
		@AuraEnabled
		public string id { get; set; }
		
		@AuraEnabled
		public Datetime timeStamp { get; set; }
		
		@AuraEnabled
		public String displayLabel { get; set; }

		@AuraEnabled
		public String dropboxPath { get; set; }

		public TrainingVideo(Photo__c photoVideo) {
			this.id				= photoVideo.Id;
			this.timeStamp		= photoVideo.Timestamp__c;
			this.displayLabel	= photoVideo.Display_Label__c;
			this.dropboxPath	= photoVideo.Dropbox_Path__c;
		}

	}

	public class VideoCollection {

		@AuraEnabled
		public List<TrainingVideo> trainingVideos = new List<TrainingVideo>();

	}
	
	private static List<Photo__c> p_queryTrainerVideos(String nameSearch, String primaryCategory, String secondaryCategory, String tertiaryCategory) {

		String selectSOQL	= 'SELECT Id, Timestamp__c, Display_Label__c, Dropbox_Path__c, Primary_Category__c, Secondary_Category__c, Tertiary_Category__c FROM Photo__c';
		String whereSOQL	= 'WHERE RecordType.DeveloperName = \'Trainer_Photos_Videos\' AND Type__c = \'Trainer Video\'';

		if(primaryCategory != null) { whereSOQL += ' AND Primary_Category__c = \'' + primaryCategory + '\''; }
		if(secondaryCategory != null) { whereSOQL += ' AND Secondary_Category__c = \'' + secondaryCategory + '\''; }
		if(tertiaryCategory != null) { whereSOQL += ' AND Tertiary_Category__c = \'' + tertiaryCategory + '\''; }

		if(nameSearch != null) { whereSOQL += ' AND Display_Label__c LIKE ' + '\'%' + nameSearch  + '%\''; }

		List<SObject> sobj = Database.query(selectSOQL + ' ' + whereSOQL + ' ORDER BY Display_Label__c ASC');
		return sobj;

	}

	@AuraEnabled 
	public static VideoCollection GetVideoCollection(String nameSearch, String primaryCategory, String secondaryCategory, String tertiaryCategory) {

		VideoCollection vCollection = new VideoCollection();

		List<TrainingVideo> trainingVideos = new List<TrainingVideo>();
		for(Photo__c p : p_queryTrainerVideos(nameSearch, primaryCategory, secondaryCategory, tertiaryCategory)) {
			trainingVideos.add(new TrainingVideo(p));
		}

		vCollection.trainingVideos	= trainingVideos;
		return vCollection;

	}

}