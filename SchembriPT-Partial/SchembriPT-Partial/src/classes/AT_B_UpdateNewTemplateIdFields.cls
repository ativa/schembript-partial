global class AT_B_UpdateNewTemplateIdFields implements Database.Batchable<Item__c> {
	
	private String whereClause;

	global AT_B_UpdateNewTemplateIdFields(String whereClause) {
		this.whereClause = whereClause;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global List<Item__c> start(Database.BatchableContext context) {
		String query = 'SELECT Id, Template_Item__c, Template_Item_Id__c FROM Item__c';
		if(whereClause != null) { query += ' WHERE ' + whereClause; }
		return Database.query(query);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Item__c> scope) {
		for(Item__c itm : scope) {
			itm.Template_Item_Id__c = itm.Template_Item__c;
		}
		update scope;
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}
}