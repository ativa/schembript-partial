/**
* @author Harrison Campbell
* @description Contains the Data for a chart
*/ 
public class ChartJS_Data {

	@AuraEnabled
	public String[] labels { get; set; }

	@AuraEnabled
	public ChartJS_Dataset[] datasets { get; set; }

}