/**
* @author Harrison Campbell
* @description
*/ 
public without sharing class AT_LWCC_PlannerAddTemplateBlockToPlan  {

	public class TemplateBlock {
		
		@AuraEnabled
		public String templateId				{ get; set; }
		
		@AuraEnabled
		public String templateName				{ get; set; }
		
		@AuraEnabled
		public Decimal templateProtein			{ get; set; }
		
		@AuraEnabled
		public Decimal templateCarbs			{ get; set; }
			
		@AuraEnabled
		public Decimal templateFat				{ get; set; }
		
		@AuraEnabled
		public Decimal templateCalories			{ get; set; }
		
		@AuraEnabled
		public List<TemplateItem> templateItems { get; set; }

		public TemplateBlock(Template_Block__c tmpBlock) {
			this.templateId			= tmpBlock.Id;
			this.templateName		= tmpBlock.Name;
			this.templateProtein	= tmpBlock.Template_Block_Protein__c;
			this.templateCarbs		= tmpBlock.Template_Block_Carbs__c;
			this.templateFat		= tmpBlock.Template_Block_Fat__c;
			this.templateCalories	= tmpBlock.Template_Block_Calories__c;
			this.templateItems		= new List<TemplateItem>();
		}

	}

	public class TemplateItem {
	
		@AuraEnabled
		public String templateId					{ get; set; }

		@AuraEnabled
		public String templateName					{ get; set; }
		
		@AuraEnabled
		public Decimal templateQuantity				{ get; set; }
		
		@AuraEnabled
		public String templateMeasurement			{ get; set; }
		
		@AuraEnabled
		public Decimal templateProtein				{ get; set; }
		
		@AuraEnabled
		public Decimal templateCarbs				{ get; set; }
		
		@AuraEnabled
		public Decimal templateFat					{ get; set; }
		
		@AuraEnabled
		public Decimal templateCalories				{ get; set; }
		
		@AuraEnabled
		public String templateBlockId				{ get; set; }

		@AuraEnabled
		public Decimal templateBlockItemQuantity	{ get; set; }
		
		@AuraEnabled
		public Decimal templateBlockItemProtein		{ get; set; }

		@AuraEnabled
		public Decimal templateBlockItemCarbs		{ get; set; }
		
		@AuraEnabled
		public Decimal templateBlockItemFat			{ get; set; }

		@AuraEnabled
		public Decimal templateBlockItemCalories	{ get; set; }

		public TemplateItem(Template_Block_Item__c tmpblkItem) {

			Template_Item__c tmpItem = tmpblkItem.Template_Item__r;

			Decimal tmpQuantity				= tmpItem.Item_Quantity__c != null && tmpItem.Item_Quantity__c != 0 ? tmpItem.Item_Quantity__c : 1;
			Decimal tmpBlockQuantity		= tmpblkItem.Quantity__c != null ? tmpblkItem.Quantity__c : 0;
		
			this.templateBlockId			= tmpblkItem.Id;
			this.templateId					= tmpblkItem.Template_Item__c;
			this.templateName				= tmpItem.Name;
			this.templateQuantity			= tmpQuantity;
			this.templateMeasurement		= tmpItem.Measurement__c;
			this.templateProtein			= tmpItem.Protein__c;
			this.templateCarbs				= tmpItem.Carbs__c;
			this.templateFat				= tmpItem.Fat__c;
			this.templateCalories			= tmpItem.Calories__c;

			Decimal proteinPerUnit			= tmpItem.Protein__c / tmpQuantity;
			Decimal carbsPerUnit			= tmpItem.Carbs__c / tmpQuantity;
			Decimal fatPerUnit				= tmpItem.Fat__c / tmpQuantity;
			Decimal caloriesPerUnit			= tmpItem.Calories__c / tmpQuantity;

			this.templateBlockItemQuantity	= tmpBlockQuantity;
			this.templateBlockItemProtein	= (proteinPerUnit	* tmpBlockQuantity).setScale(5);
			this.templateBlockItemCarbs		= (carbsPerUnit		* tmpBlockQuantity).setScale(5);
			this.templateBlockItemFat		= (fatPerUnit		* tmpBlockQuantity).setScale(5);
			this.templateBlockItemCalories	= (caloriesPerUnit	* tmpBlockQuantity).setScale(5);

		}


	}

	/**
	* @description Get the Nutritional Template Blocks
	* @return 
	*/ 
	@AuraEnabled
	public static List<TemplateBlock> GetNutrionalTemplateBlocks(String planId) {

		Plan__c plan = [
			SELECT
				Id,
				Account__r.SF_Community_User__pc
			FROM Plan__c
			WHERE Id = :planId
		];

		List<Template_Block__c> allTemplateBlocks = [
			SELECT 
				Id,
				Name,
				Template_Block_Protein__c,
				Template_Block_Carbs__c,
				Template_Block_Fat__c,
				Template_Block_Calories__c,
				(
					SELECT 
						Id,
						Quantity__c,
						Template_Item__r.Name,
						Template_Item__r.Item_Quantity__c,
						Template_Item__r.Measurement__c,
						Template_Item__r.Protein__c,
						Template_Item__r.Carbs__c,
						Template_Item__r.Fat__c,
						Template_Item__r.Calories__c,
						Display_Order__c
					FROM Block_Items__r
					ORDER BY Display_Order__c ASC
				)
			FROM Template_Block__c
			WHERE RecordType.DeveloperName = 'Template_Nutrition_Block' AND (Status__c = 'Public' OR (Status__c = 'Client Specific' AND Client__c = :plan.Account__r.SF_Community_User__pc))
		];

		List<TemplateBlock> tmpBlocks = new List<TemplateBlock>();
		for(Template_Block__c tmpBlock : allTemplateBlocks) {
			TemplateBlock newTmpBlock = new TemplateBlock(tmpBlock);
			for(Template_Block_Item__c tmpBlkItem : tmpBlock.Block_Items__r) {
				newTmpBlock.templateItems.add(new TemplateItem(tmpBlkItem));
			}
			tmpBlocks.add(newTmpBlock);
		}
		return tmpBlocks;
	}

}