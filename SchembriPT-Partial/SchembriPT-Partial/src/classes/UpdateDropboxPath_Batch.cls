global class UpdateDropboxPath_Batch implements Database.Batchable<SObject>, Database.AllowsCallouts {
	
	private String whereFilter { get; set; }
	
	global UpdateDropboxPath_Batch() {
	}

	global UpdateDropboxPath_Batch(String whereFilter) {
		this.whereFilter = whereFilter;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {

		if(this.whereFilter != null) {
			return Database.getQueryLocator('SELECT Id, Name, h_Last_Sharing_Batch_Failed__c, Reference__c, Dropbox_Path__c FROM Photo__c ' + this.whereFilter);
		} else {
			return Database.getQueryLocator('SELECT Id, Name, h_Last_Sharing_Batch_Failed__c, Reference__c, Dropbox_Path__c FROM Photo__c');
		}

	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Photo__c> scope) {
		for(Photo__c p : scope) {
			if(p.Reference__c != null) {
				Dropbox_SharingAPI.CreateSharedLinkWithSettingsPacket sharingPacket = Dropbox_SharingAPI.CreateSharedLinkWithSettings(p.Reference__c);
				String sharingURL = null;
				if(sharingPacket.statusCode == 200) { //Success
					sharingURL = sharingPacket.result.url.removeEnd('dl=0') + 'raw=1';
					p.h_Last_Sharing_Batch_Failed__c = false;
				} else if(sharingPacket.statusCode == 409) { //Conflict
					if(sharingPacket.error.error.shared_link_already_exists != null) {
						sharingURL = sharingPacket.error.error.shared_link_already_exists.metadata.url.removeEnd('dl=0') + 'raw=1';
						p.h_Last_Sharing_Batch_Failed__c = false;
					} else {
						p.h_Last_Sharing_Batch_Failed__c = true;
					}
				} else {
					p.h_Last_Sharing_Batch_Failed__c = true;
				}

				//Don't update the Dropbox path, unless we have a url for it.
				if(sharingURL != null) {
					p.Dropbox_Path__c = sharingURL;
				}
			} else {
				p.h_Last_Sharing_Batch_Failed__c = true;
			}
		}
		update scope;
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
	}
}