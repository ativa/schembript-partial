public class ChartJS_DataLabel  {

	public String anchor { get; set; }
	
	public String align { get; set; }

	public Boolean display { get; set; }

	public ChartJS_Font font { get; set; }

	public Boolean clamp { get; set; }

	public Integer offset { get; set; }

	public Integer textStrokeWidth { get; set; }

	public String textStrokeColor { get; set; }

	public Integer textShadowBlur { get; set; }

	public String textShadowColor { get; set; }

	public String color { get; set; }

}