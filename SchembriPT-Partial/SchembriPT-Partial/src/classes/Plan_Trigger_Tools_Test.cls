/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-28-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-15-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest 
private class Plan_Trigger_Tools_Test {
	
	@TestSetup
	private static void setup() {
		
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		autoSettings.Enable_Onboarding_Automation__c = true;
		insert autoSettings;

	}

	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
			testAccount.SF_Community_User__pc		= UserInfo.getUserId();
		insert testAccount;
		
		return testAccount;

	}

	private static Contact updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		testContact.Email				= 'test@tst.com';
		testContact.Phone				= '0489123345';

		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Lower Back';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';
		testProgram.Program_Start_Date__c	= Date.newInstance(2020, 3, 2);
		testProgram.Program_End_Date__c		= Date.newInstance(2020, 5, 25);
		testProgram.Check_In_Frequency__c	= '1 Week';

		//Create a Product for the case
		Programs_Products__c prod	= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 3;
		prod.Name					= 'Body Revolt';
		insert prod;
		testProgram.Programs_Products__c = prod.Id;
		insert testProgram;
				
		testProgram.Status = 'Active';
		update testProgram;

		return testProgram;
	}

	private static Plan__c testTool_createReferencePlan(String refPlanName, Check_In__c cCheckIn, String weekdays, Boolean setValues) {
	
		Id planReferenceRecId	= [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Reference_Plan'].Id;

		Plan__c refPlan = new Plan__c();
		refPlan.RecordTypeId		= planReferenceRecId;
		refPlan.Name				= refPlanName;
		refPlan.Check_In__c			= cCheckIn.Id;
		if(setValues == true) {
			refPlan.Trainer_Plan_Fat__c	= 100;
			refPlan.Trainer_Plan_Protein__c	= 200;
			refPlan.Trainer_Plan_Carbs__c		= 300;
			refPlan.Trainer_Plan_Calories__c	= 400;
		}
		refPlan.Weekdays__c			= weekdays;
		insert refPlan;
		return refPlan;
	}

	private static List<Plan__c> testTool_createSessionPlans(String namePrefix, Check_In__c cCheckIn, Boolean setValues) {
	
		Id planSessionRecId		= [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		List<Plan__c> sessionPlans = new List<Plan__c>();
		//Create a Session Plan for each day of the plan
		for(Integer i = 0; i < 7; i++) {
			Plan__c newSessionPlan				= new Plan__c();
			newSessionPlan.RecordTypeId			= planSessionRecId;
			newSessionPlan.Name					= namePrefix + (i+1);
			newSessionPlan.Check_In__c			= cCheckIn.Id;
			if(setValues == true) {
				newSessionPlan.Client_Plan_Fat__c			= 100;
				newSessionPlan.Client_Plan_Protein__c		= 200;
				newSessionPlan.Client_Plan_Carbs__c		= 300;
				newSessionPlan.Client_Plan_Calories__c	= 400;
			}
			newSessionPlan.Order__c				= i;
			sessionPlans.add(newSessionPlan);
		}
		return sessionPlans;
	}
	
	@IsTest
	public static void insertPlans_insertNewPlansForCheckIn0() {
			
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		//Query CheckIn 0
		Check_In__c checkIn0 = [SELECT Id, Name, Check_In_Number_v2__c, Stage__c FROM Check_In__c];

		//Create a reference Plan, and it's related session plans
		Plan__c refPlan				= testTool_createReferencePlan('Reference Plan 1', checkIn0, 'Sunday;Monday;Tuesday;Wednesday;Thursday;Friday;Saturday', true);
		List<Plan__c> sessionPlans	= testTool_createSessionPlans('Plan ', checkIn0, true);

		Test.startTest();
		Plan_Trigger_Tools.UpdateCheckInSummary(refPlan.Id);
		Test.stopTest();

		checkIn0 = [
			SELECT 
				Id, 
				Name, 
				Check_In_Number_v2__c,
				Plan_Summary_Fat_New__c,
				Plan_Summary_Protein_New__c,
				Plan_Summary_Carbs_New__c,
				Plan_Summary_Calories_New__c,
				Plan_Average_Fat_New__c,
				Plan_Average_Protein_New__c,
				Plan_Average_Carbs_New__c,
				Plan_Average_Calories_New__c
			FROM Check_In__c
		];

		//Assert that the checkins were updated
		System.assertEquals('Reference Plan 1: 100g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Fat_New__c);
		System.assertEquals('Reference Plan 1: 200g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Protein_New__c);
		System.assertEquals('Reference Plan 1: 300g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Carbs_New__c);
		System.assertEquals('Reference Plan 1: 400 (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Calories_New__c);

		System.assertEquals(100,	checkIn0.Plan_Average_Fat_New__c);
		System.assertEquals(200,	checkIn0.Plan_Average_Protein_New__c);
		System.assertEquals(300,	checkIn0.Plan_Average_Carbs_New__c);
		System.assertEquals(400,	checkIn0.Plan_Average_Calories_New__c);
		
		//Assert that the program was successfully updated.
		testProgram = [
			SELECT
				Id,
				Check_In_0_Carb__c,
				Check_In_0_Fat__c,
				Check_In_0_Protein__c,
				Check_In_0_Calories__c
			FROM Case
		];
		
		System.assertEquals('Reference Plan 1: 100g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Fat__c);
		System.assertEquals('Reference Plan 1: 200g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Protein__c);
		System.assertEquals('Reference Plan 1: 300g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Carb__c);
		System.assertEquals('Reference Plan 1: 400 (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Calories__c);

	}

	
	private static void testTool_createCheckIns(Case currentProgram, Account acc, Contact cont, Integer checkInCount) {

		List<Check_In__c> allProgramCheckIns = [
			SELECT
				Id
			FROM Check_In__c
			WHERE Client_Program__c = :currentProgram.Id
		];
		delete allProgramCheckIns;

		List<Check_In__c> newCheckIns = new List<Check_In__c>();

		for(Integer i = 0; i < checkInCount; i++) {
			Check_In__c newCheckIn 					= new Check_In__c();
			newCheckIn.Name 						= 'Check-In' + i;
			newCheckIn.Client_Program__c 			= currentProgram.Id;
			newCheckIn.Scheduled_Check_In_Date__c 	= Date.today().addDays(i);
			newCheckIn.Check_In_Number_v2__c		= i;
			newCheckIn.Thigh_Measurement__c			= 33 + (i+1);
			newCheckIn.Waist_Measurement__c			= 45 + (i+1);
			newCheckIn.Chest_Measurement__c			= 56 + (i+1);
			newCheckIn.Hip_Measurement__c			= 50 + (i+1);
			newCheckIn.Weight__c					= 75 + (i+1);
			newCheckIn.Stage__c 					= 'Completed';
			newCheckIn.Contact__c					= cont.Id;
			newCheckIn.Account__c					= acc.Id;
			newCheckIn.h_Test_Record__c				= true;
			newCheckIns.add(newCheckIn);
		}

		insert newCheckIns;

	}
	

	@IsTest
	public static void insertPlans_insertNewPlansForCheckIn1() {
		
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testTool_createCheckIns(testProgram, testAccount, testContact, 2);

		Check_In__c checkIn1 = [SELECT Id, Name, Check_In_Number_v2__c, Stage__c FROM Check_In__c WHERE Check_In_Number_v2__c = 1];
		
		//Create a reference Plan, and it's related session plans
		Plan__c refPlan				= testTool_createReferencePlan('Reference Plan 1', checkIn1, 'Sunday;Monday;Tuesday;Wednesday;Thursday;Friday;Saturday', true);
		List<Plan__c> sessionPlans	= testTool_createSessionPlans('Plan ', checkIn1, true);

		Test.startTest();
		Plan_Trigger_Tools.UpdateCheckInSummary(refPlan.Id);
		Test.stopTest();
		
		checkIn1 = [
			SELECT 
				Id, 
				Name, 
				Check_In_Number_v2__c,
				Plan_Summary_Fat_New__c,
				Plan_Summary_Protein_New__c,
				Plan_Summary_Carbs_New__c,
				Plan_Summary_Calories_New__c,
				Plan_Average_Fat_New__c,
				Plan_Average_Protein_New__c,
				Plan_Average_Carbs_New__c,
				Plan_Average_Calories_New__c
			FROM Check_In__c
			WHERE Check_In_Number_v2__c = 1
		];
		
		//Assert that the checkins were updated
		System.assertEquals('Reference Plan 1: 100g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn1.Plan_Summary_Fat_New__c);
		System.assertEquals('Reference Plan 1: 200g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn1.Plan_Summary_Protein_New__c);
		System.assertEquals('Reference Plan 1: 300g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn1.Plan_Summary_Carbs_New__c);
		System.assertEquals('Reference Plan 1: 400 (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn1.Plan_Summary_Calories_New__c);
		
		System.assertEquals(100,	checkIn1.Plan_Average_Fat_New__c);
		System.assertEquals(200,	checkIn1.Plan_Average_Protein_New__c);
		System.assertEquals(300,	checkIn1.Plan_Average_Carbs_New__c);
		System.assertEquals(400,	checkIn1.Plan_Average_Calories_New__c);
		
	}

	@IsTest
	public static void updatePlans_updatePlansForCheckIn0() {
		
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		//Query CheckIn 0
		Check_In__c checkIn0 = [SELECT Id, Name, Check_In_Number_v2__c, Stage__c FROM Check_In__c];
		
		Plan__c refPlan				= testTool_createReferencePlan('Reference Plan 1', checkIn0, 'Sunday;Monday;Tuesday;Wednesday;Thursday;Friday;Saturday', true);
		List<Plan__c> sessionPlans	= testTool_createSessionPlans('Plan ', checkIn0, true);
		insert sessionPlans;

		//Update both the ref plans, and the session plans to reflect the new changes
		refPlan.Trainer_Plan_Fat__c		= 10;
		refPlan.Trainer_Plan_Protein__c	= 20;
		refPlan.Trainer_Plan_Carbs__c		= 30;
		refPlan.Trainer_Plan_Calories__c	= 40;
		update refPlan;

		for(Plan__c pSess : sessionPlans) {
			pSess.Client_Plan_Fat__c		= 10;
			pSess.Client_Plan_Protein__c	= 20;
			pSess.Client_Plan_Carbs__c	= 30;
			pSess.Client_Plan_Calories__c	= 40;
		}

		Test.startTest();
		Plan_Trigger_Tools.UpdateCheckInSummary(refPlan.Id);
		Test.stopTest();

		checkIn0 = [
			SELECT 
				Id, 
				Name, 
				Check_In_Number_v2__c,
				Plan_Summary_Fat_New__c,
				Plan_Summary_Protein_New__c,
				Plan_Summary_Carbs_New__c,
				Plan_Summary_Calories_New__c,
				Plan_Average_Fat_New__c,
				Plan_Average_Protein_New__c,
				Plan_Average_Carbs_New__c,
				Plan_Average_Calories_New__c
			FROM Check_In__c
		];

		//Assert that the checkins were updated
		System.assertEquals('Reference Plan 1: 10g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Fat_New__c);
		System.assertEquals('Reference Plan 1: 20g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Protein_New__c);
		System.assertEquals('Reference Plan 1: 30g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Carbs_New__c);
		System.assertEquals('Reference Plan 1: 40 (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	checkIn0.Plan_Summary_Calories_New__c);
		
		System.assertEquals(10,		checkIn0.Plan_Average_Fat_New__c);
		System.assertEquals(20,		checkIn0.Plan_Average_Protein_New__c);
		System.assertEquals(30,		checkIn0.Plan_Average_Carbs_New__c);
		System.assertEquals(40,		checkIn0.Plan_Average_Calories_New__c);

		testProgram = [
			SELECT
				Id,
				Check_In_0_Carb__c,
				Check_In_0_Fat__c,
				Check_In_0_Protein__c,
				Check_In_0_Calories__c
			FROM Case
		];
		
		System.assertEquals('Reference Plan 1: 10g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Fat__c);
		System.assertEquals('Reference Plan 1: 20g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Protein__c);
		System.assertEquals('Reference Plan 1: 30g (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Carb__c);
		System.assertEquals('Reference Plan 1: 40 (Sun,Mon,Tue,Wed,Thu,Fri,Sat)',	testProgram.Check_In_0_Calories__c);

	}

	
}