/**
* @author Harrison Campbell
* @description Holds data to be used with ChartJS
*/ 
public class ChartJS_Chart {

	/**
	* @description The Type of Chart (eg. bar, line, radar, etc.)
	*/ 
	@AuraEnabled
	public string type { get; set; }

	/**
	* @description The data that will display on the chart
	*/ 
	@AuraEnabled
	public ChartJS_Data data { get; set; }

	/**
	* @description The Options of the chart
	*/ 
	@AuraEnabled
	public ChartJS_Options options { get; set; }

}