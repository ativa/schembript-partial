/**
* @author Harrison Campbell
* @description The Lightning Component for the 'trackerXAssignPlansToTargets'
*/ 
public without sharing class AT_LWCC_TrackerAssignPlansToTargets {

	/**
	* @author Harrison Campbell
	* @description Holds information about the Target
	*/ 
	public virtual class Target {
	
		/**
		* @description The Id of the target
		*/ 
		@AuraEnabled
		public String targetId { get; set; }

		/**
		* @description The Name of the target
		*/ 
		@AuraEnabled
		public String targetName { get; set; }
		
		/**
		* @description The protein of the target
		*/ 
		@AuraEnabled
		public Decimal targetProtein { get; set; }

		/**
		* @description The carbs of the target
		*/ 
		@AuraEnabled
		public Decimal targetCarbs { get; set; }
		
		/**
		* @description The fat of the target
		*/ 
		@AuraEnabled
		public Decimal targetFat { get; set; }

		/**
		* @description The calories of the target
		*/ 
		@AuraEnabled
		public Decimal targetCalories { get; set; }
		
		/**
		 * @description The Id of the selected Plan relating to this target
		 */
		@AuraEnabled
		public String selectedPlanId { get; set; }
		
	}

	/**
	* @author Harrison Campbell
	* @description Holds information about the Flexi Plan Target
	*/ 
	public class FlexiPlanTarget extends Target {
	
		/**
		* @description The date that the target is assigned too
		*/ 
		@AuraEnabled
		public Date targetDate { get; set; }

	}

	public class CarbCyclePlanTarget extends Target {
	
		/**
		* @description The date that the target is assigned too
		*/ 
		@AuraEnabled
		public Date targetDate { get; set; }

	}

    public class GuidedPlanTarget extends Target {

        /**
        * @description The date that the target is assigned too
        */ 
        @AuraEnabled
        public Date targetStartDate { get; set; }

        /**
        * @description The date that the target is assigned too
        */ 
        @AuraEnabled
        public Date targetEndDate { get; set; }

        /**
         * @description The assigned Week
         */
        @AuraEnabled
        public String assignedWeek { get; set; }

        /**
         * @description The label for the week
         */
        @AuraEnabled
        public String weekLabel { get; set; }

    }

	/**
	* @author Harrison Campbell
	* @description An option for a client plan
	*/ 
	public class ClientPlanOption {

		@AuraEnabled
		public String label { get; set; }
        
		@AuraEnabled
		public String value { get; set; }

		@AuraEnabled
		public String poolPlanId { get; set; }

        @AuraEnabled
        public String relatedPlanId { get; set; }

        @AuraEnabled
        public String relatedPlanRecordType { get; set; }
		
		@AuraEnabled
		public String type { get; set; }

		@AuraEnabled
		public Decimal protein { get; set; }

		@AuraEnabled
		public Decimal carbs { get; set; }

		@AuraEnabled
		public Decimal fat { get; set; }

		@AuraEnabled
		public Decimal calories { get; set; }

		public ClientPlanOption(Pool_Plan__c poolPlan) {
			this.label			= poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Name;
			this.value			= poolPlan.Id;

			this.poolPlanId = poolPlan.Id;

			this.relatedPlanId	        = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__c;
            this.relatedPlanRecordType  = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.RecordType.DeveloperName;
			
			if(poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.RecordType.DeveloperName == 'Nutrition_Reference_Plan') {
				this.protein	    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Trainer_Plan_Protein__c;
				this.carbs		    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Trainer_Plan_Carbs__c;
				this.fat		    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Trainer_Plan_Fat__c;
				this.calories	    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Trainer_Plan_Calories__c;
			} else if(poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.RecordType.DeveloperName == 'Nutrition_Client_Plan') {
				this.protein	    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Client_Plan_Protein__c;
				this.carbs		    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Client_Plan_Carbs__c;
				this.fat		    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Client_Plan_Fat__c;
				this.calories	    = poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Client_Plan_Calories__c;
			}

		}

	}

	public class AssignPlanInformation {

		@AuraEnabled
		public Map<Id, AT_M_Nutrition.NutritionTarget> nutritionTargets { get; set; }
	
		/**
		 * @description All of the date targets
		 */ 
		@AuraEnabled
		public List<FlexiPlanTarget> flexiPlanTargets { get; set; }

        @AuraEnabled
        public List<GuidedPlanTarget> guidedPlanTargets { get; set; }

		@AuraEnabled
		public List<CarbCyclePlanTarget> carbCycleTargets { get; set; }

		/**
		 * @description available Plan Options
		 */
		@AuraEnabled
		public List<ClientPlanOption> clientPlanOptions { get; set; }

		@AuraEnabled
		public Map<Id, List<ClientPlanOption>> trackerPlanOptions { get; set; }

		@AuraEnabled
		public Map<Id, List<String>> targetAssignToDays { get; set; }


		@AuraEnabled
		public CheckInInformation checkInInformation { get; set; }

	}

	public class CheckInInformation {

		/**
		* @description The Id of the Check-In
		*/ 
		@AuraEnabled
		public String id { get; set; }
		
		/**
		* @description The Name of the Check-In
		*/ 
		@AuraEnabled
		public String name { get; set; }
		
		/**
		* @description The Stage of the Check-In
		*/ 
		@AuraEnabled
		public String stage { get; set; }

        /**
         * @description The planning mode for the Check-In
         */
        @AuraEnabled
        public String nutritionPlanningMode { get; set; }

		/**
		* @description If flexi plans, are they locked to the assigned days
		*/ 
		@AuraEnabled
		public Boolean flexiPlansLocked { get; set; }
        
		/**
		* @description 
		* @param checkIn 
		*/ 
		public CheckInInformation(Check_In__c checkIn) {
			id		                = checkIn.Id;
			name	                = checkIn.Name;
			stage	                = checkIn.Stage__c;
            nutritionPlanningMode   = checkIn.Nutrition_Planning_Mode__c;
			flexiPlansLocked		= checkIn.Flexi_Plans_Locked__c;
		}

	}

	/**
	* @description Get the current User
	* @return Returns the User Object
	*/ 
	private static User GetCurrentUser() {
		return [
			SELECT
				Id,
				AccountId
			FROM User
			WHERE Id = :UserInfo.getUserId()
		];
	}

	/**
	* @description Get the latest Completed Check-In
	* @param accountId The Id of the account that holds all of the Check-Ins
	* @return Returns the latest Completed Check-In
	*/ 
	private static Check_In__c GetLatestCompletedCheckIn(String accountId) {
		return [
			SELECT
				Id,
				Name,
				Stage__c,
                Guided_Plan_Number_Weeks__c,
                Nutrition_Planning_Mode__c,
				Nutrition_Plan_Start_Date__c,
				Nutrition_Plan_End_Date__c,
				Flexi_Plans_Locked__c
			FROM Check_In__c
			WHERE Account__c = :accountId AND Stage__c = 'Completed'
			ORDER BY Scheduled_Check_In_Date__c DESC
			LIMIT 1
		];
	}

    private static List<GuidedPlanTarget> GetGuidedPlanWeekTargets(Check_In__c checkIn, Integer latestWeek) {

        List<Targets__c> targets = [
            SELECT
                Id,
                Name,
                Display_Name__c,
                Assign_to_Weeks__c,
                Protein__c,
                Carbs__c,
                Fat__c,
                Calories__c
            FROM Targets__c
            WHERE Check_In__c = :checkIn.Id
        ];
        Set<Id> targetIds = new Map<Id, Targets__c>(targets).KeySet();

        List<Plan_Day__c> planDays = [
            SELECT
                Id,
                Date__c,
                Week__c,
                Pool_Plan__c
            FROM Plan_Day__c
            WHERE Targets__c IN :targetIds
            ORDER BY Date__c
        ];

        Map<String, Id> weekToTemplatePoolPlanId = new Map<String, Id>();
        for(Plan_Day__c pDay : planDays) {
            if(!weekToTemplatePoolPlanId.containsKey(pDay.Week__c)) {
                weekToTemplatePoolPlanId.put(pDay.Week__c, pDay.Pool_Plan__c);
            }
        }

        //Sort the targets by weeks
        Map<String, Targets__c> weekToTarget = new Map<String, Targets__c>();
        for(Targets__c t : targets) {
            if(t.Assign_to_Weeks__c == null) {
                continue;
            }
            List<String> weeks = t.Assign_to_Weeks__c.split(';');
            for(String week : weeks) {
                weekToTarget.put(week, t);
            }
        }

        Date currentDate = checkIn.Nutrition_Plan_Start_Date__c;

        List<GuidedPlanTarget> guidedPlanTargets = new List<GuidedPlanTarget>();

        for(Integer i = 0; i < latestWeek; i++) {

            String week = 'Week ' + (i + 1);

            Targets__c cTarget              = weekToTarget.get(week);
            GuidedPlanTarget newDateTarget  = new GuidedPlanTarget();

            if(weekToTemplatePoolPlanId.containsKey(week)) {
                newDateTarget.selectedPlanId = weekToTemplatePoolPlanId.get(week);
            }

            newDateTarget.weekLabel         = week;
            newDateTarget.assignedWeek      = week;
            newDateTarget.targetStartDate   = currentDate;
            newDateTarget.targetEndDate     = currentDate.addDays(6);
            newDateTarget.targetId          = cTarget.Id;
            newDateTarget.targetName        = cTarget.Display_Name__c;
            newDateTarget.targetProtein     = cTarget.Protein__c;
            newDateTarget.targetCarbs       = cTarget.Carbs__c;
            newDateTarget.targetFat         = cTarget.Fat__c;
            newDateTarget.targetCalories    = cTarget.Calories__c;
            guidedPlanTargets.add(newDateTarget);

            currentDate = currentDate.addDays(7);
        }

        return guidedPlanTargets;
    }
    
	/**
	* @description Get all of the Date Targets
	* @param checkInId The Check-In Id that these targets related too
	* @param startPlanDate The start range of the targets
	* @param endPlanDate The end range of the targets
	* @return Returns the Date targets related to the Check-In
	*/ 
	private static List<FlexiPlanTarget> GetFlexiPlanDateTargets(String checkInId, Date startPlanDate, Date endPlanDate) {
    
		List<Targets__c> targets = [
			SELECT
				Id,
				Name,
                Display_Name__c,
                Assign_to_Weeks__c,
				Weekdays__c,
				Protein__c,
				Carbs__c,
				Fat__c,
				Calories__c
			FROM Targets__c
			WHERE Check_In__c = :checkInId
		];

        List<Plan_Day__c> planDays = [
            SELECT
                Id,
                Name,
                Pool_Plan__c,
                Date__c,
                Week__c
            FROM Plan_Day__c
            WHERE Targets__c IN: (new Map<String, Targets__c>(targets)).KeySet()
        ];

        Map<Date, Plan_Day__c> dateToPlanDays = new Map<Date, Plan_Day__c>();
        for(Plan_Day__c pDay : planDays) {
            dateToPlanDays.put(pDay.Date__c, pDay);
        }

		//Group the targets by the weekdays
		Map<String, Targets__c> weekdayTargets = new Map<String, Targets__c>();
		for(Targets__c t : targets) {
			if(t.Weekdays__c == null) {
				continue;
			}
			String[] targetWeekdays = t.Weekdays__c.split(';');
			for(String weekday : targetWeekdays) {
				weekdayTargets.put(weekday, t);
			}		
		}

		List<FlexiPlanTarget> allDayTargets = new List<FlexiPlanTarget>();

		Date currentDate = startPlanDate;
		while (currentDate <= endPlanDate){
			String weekday					= ((DateTime)currentDate).format('EEEE');
			System.debug(currentDate + ' | ' + weekday);
			Targets__c cTarget				= weekdayTargets.get(weekday);

			FlexiPlanTarget newDateTarget		= new FlexiPlanTarget();

			if(cTarget != null) {

				if(dateToPlanDays.containsKey(currentDate)) {
					newDateTarget.selectedPlanId    = dateToPlanDays.get(currentDate).Pool_Plan__c;
				}

				newDateTarget.targetDate		= currentDate;
				newDateTarget.targetId			= cTarget.Id;
				newDateTarget.targetName		= cTarget.Display_Name__c;
				newDateTarget.targetProtein		= cTarget.Protein__c;
				newDateTarget.targetCarbs		= cTarget.Carbs__c;
				newDateTarget.targetFat			= cTarget.Fat__c;
				newDateTarget.targetCalories	= cTarget.Calories__c;
				allDayTargets.add(newDateTarget);

			} else {
				newDateTarget.targetDate		= currentDate;
				allDayTargets.add(newDateTarget);
			}

			currentDate					= currentDate.addDays(1);
		}

		return allDayTargets;
	}
	
	private static List<CarbCyclePlanTarget> GetCarbCycleDateTargets(String checkInId, Date startPlanDate, Date endPlanDate) {
    
		List<Targets__c> targets = [
			SELECT
				Id,
				Name,
                Display_Name__c,
                Assign_to_Weeks__c,
				Weekdays__c,
				Protein__c,
				Carbs__c,
				Fat__c,
				Calories__c
			FROM Targets__c
			WHERE Check_In__c = :checkInId
		];

		System.debug('Targets');
		System.debug(targets);

        List<Plan_Day__c> planDays = [
            SELECT
                Id,
                Name,
                Pool_Plan__c,
                Date__c,
                Week__c
            FROM Plan_Day__c
            WHERE Targets__c IN: (new Map<String, Targets__c>(targets)).KeySet()
        ];

        Map<Date, Plan_Day__c> dateToPlanDays = new Map<Date, Plan_Day__c>();
        for(Plan_Day__c pDay : planDays) {
            dateToPlanDays.put(pDay.Date__c, pDay);
        }

		//Group the targets by the weekdays
		Map<String, Targets__c> weekdayTargets = new Map<String, Targets__c>();
		for(Targets__c t : targets) {
			if(t.Weekdays__c == null) {
				continue;
			}
			String[] targetWeekdays = t.Weekdays__c.split(';');
			for(String weekday : targetWeekdays) {
				weekdayTargets.put(weekday, t);
			}		
		}

		List<CarbCyclePlanTarget> allDayTargets = new List<CarbCyclePlanTarget>();

		Date currentDate = startPlanDate;
		while (currentDate <= endPlanDate){
			String weekday					= ((DateTime)currentDate).format('EEEE');
			Targets__c cTarget				= weekdayTargets.get(weekday);

			CarbCyclePlanTarget newDateTarget		= new CarbCyclePlanTarget();

			if(cTarget != null) {

				if(dateToPlanDays.containsKey(currentDate)) {
					newDateTarget.selectedPlanId    = dateToPlanDays.get(currentDate).Pool_Plan__c;
				}

				newDateTarget.targetDate		= currentDate;
				newDateTarget.targetId			= cTarget.Id;
				newDateTarget.targetName		= cTarget.Display_Name__c;
				newDateTarget.targetProtein		= cTarget.Protein__c;
				newDateTarget.targetCarbs		= cTarget.Carbs__c;
				newDateTarget.targetFat			= cTarget.Fat__c;
				newDateTarget.targetCalories	= cTarget.Calories__c;
				allDayTargets.add(newDateTarget);

			} else {
				newDateTarget.targetDate		= currentDate;
				allDayTargets.add(newDateTarget);
			}

			currentDate					= currentDate.addDays(1);
		}

		return allDayTargets;
	}

	@AuraEnabled
	public static Map<Id, List<ClientPlanOption>> GetPlanOptions2(String checkInId) {

		Check_In__c checkIn = [
			SELECT
				Id,
				Nutrition_Planning_Mode__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		List<Targets__c> checkInTargets = [
			SELECT
				Id
			FROM Targets__c
			WHERE Check_In__c = :checkInId
		];
        Set<Id> targetIds = new Map<Id, Targets__c>(checkInTargets).KeySet();

        //Query the Target/Pool Plans
        List<Targets_Pool_Plan__c> relTargetPoolPlans = [
            SELECT
                Id,
                Targets__c,
                Pool_Plan__c
            FROM Targets_Pool_Plan__c
            WHERE Targets__c = :targetIds AND Pool_Plan__c != null
        ];
        List<Id> poolPlanIds = new List<Id>();
        for(Targets_Pool_Plan__c tpp : relTargetPoolPlans) {
            poolPlanIds.add(tpp.Pool_Plan__c);
        }

        Map<Id, Pool_Plan__c> relPoolPlans = new Map<Id, Pool_Plan__c>([
            SELECT
                Id,
                (
                    SELECT
                        Id,
                        Reference_Plan__c,
                        Reference_Plan__r.Name,
                        Reference_Plan__r.RecordType.DeveloperName,
                        Reference_Plan__r.Trainer_Plan_Protein__c,
                        Reference_Plan__r.Trainer_Plan_Carbs__c,
                        Reference_Plan__r.Trainer_Plan_Fat__c,
                        Reference_Plan__r.Trainer_Plan_Calories__c,
                        Reference_Plan__r.Client_Plan_Protein__c,
                        Reference_Plan__r.Client_Plan_Carbs__c,
                        Reference_Plan__r.Client_Plan_Fat__c,
                        Reference_Plan__r.Client_Plan_Calories__c
                    FROM Pool_Plan_Versions__r
                    WHERE Active_Version__c = true
                )
            FROM Pool_Plan__c
            WHERE Id IN :poolPlanIds
        ]);
              
		Map<Id, List<ClientPlanOption>> planOptionMap = new Map<Id, List<ClientPlanOption>>();

        for(Targets_Pool_Plan__c tpp : relTargetPoolPlans) {
            if(!planOptionMap.containsKey(tpp.Targets__c)) {
                planOptionMap.put(tpp.Targets__c, new List<ClientPlanOption>());
            }

            Pool_Plan__c pp = relPoolPlans.get(tpp.Pool_Plan__c);

            if(checkIn.Nutrition_Planning_Mode__c == 'Carb-Cycle' || checkIn.Nutrition_Planning_Mode__c == 'Flexi Plans' || checkIn.Nutrition_Planning_Mode__c == 'Guided Plans') {
                planOptionMap.get(tpp.Targets__c).add(new ClientPlanOption(pp));
            }

        }
      

		return planOptionMap;

	}

	/**
	* @description Get a list of plan options
	* @param checkInId The Id of the Check-In that these plans relate too
	* @return Returns Client Plan Options
	*/ 
	@AuraEnabled
	public static List<ClientPlanOption> GetPlanOptions(String checkInId) {

		List<Pool_Plan__c> checkInPoolPlans = [
			SELECT
				Id,
				(
					SELECT
						Id,
						Reference_Plan__c,
						Reference_Plan__r.Name,
						Reference_Plan__r.RecordType.DeveloperName,
						Reference_Plan__r.Trainer_Plan_Protein__c,
						Reference_Plan__r.Trainer_Plan_Carbs__c,
						Reference_Plan__r.Trainer_Plan_Fat__c,
						Reference_Plan__r.Trainer_Plan_Calories__c,
						Reference_Plan__r.Client_Plan_Protein__c,
						Reference_Plan__r.Client_Plan_Carbs__c,
						Reference_Plan__r.Client_Plan_Fat__c,
						Reference_Plan__r.Client_Plan_Calories__c
					FROM Pool_Plan_Versions__r
					WHERE Active_Version__c = true
				)
			FROM Pool_Plan__c
			WHERE Check_In__c = :checkInId
		];

		List<ClientPlanOption> clientPlanOptions = new List<ClientPlanOption>();
		for(Pool_Plan__c pPlan : checkInPoolPlans) { 
            clientPlanOptions.add(new ClientPlanOption(pPlan)); 
        }
		return clientPlanOptions;
	}

	@AuraEnabled 
	public static AssignPlanInformation GetAssignPlanInformation() {
		User currentUser						= GetCurrentUser();
		Check_In__c latestCheckIn				= GetLatestCompletedCheckIn(currentUser.AccountId);
		AssignPlanInformation assignPlanInfo	= new AssignPlanInformation();

        if(latestCheckIn.Nutrition_Planning_Mode__c == 'Carb-Cycle') {
			assignPlanInfo.carbCycleTargets		= GetCarbCycleDateTargets(latestCheckIn.Id, latestCheckIn.Nutrition_Plan_Start_Date__c, latestCheckIn.Nutrition_Plan_End_Date__c);
        } else if(latestCheckIn.Nutrition_Planning_Mode__c == 'Flexi Plans') {
            assignPlanInfo.flexiPlanTargets		= GetFlexiPlanDateTargets(latestCheckIn.Id, latestCheckIn.Nutrition_Plan_Start_Date__c, latestCheckIn.Nutrition_Plan_End_Date__c);
        } else if(latestCheckIn.Nutrition_Planning_Mode__c == 'Guided Plans') {
            Integer maxWeeks = Integer.valueOf(latestCheckIn.Guided_Plan_Number_Weeks__c.remove('Week '));
            assignPlanInfo.guidedPlanTargets	= GetGuidedPlanWeekTargets(latestCheckIn, maxWeeks);
        }
        
        
		assignPlanInfo.clientPlanOptions		= GetPlanOptions(latestCheckIn.Id);
		assignPlanInfo.trackerPlanOptions		= GetPlanOptions2(latestCheckIn.Id);
		assignPlanInfo.checkInInformation		= new CheckInInformation(latestCheckIn);
		assignPlanInfo.targetAssignToDays		= GetTargetAssignToDays(latestCheckIn.Id);

		assignPlanInfo.nutritionTargets = new Map<Id, AT_M_Nutrition.NutritionTarget>();
		for(AT_M_Nutrition.NutritionTarget nTarget : AT_M_Nutrition.GetCheckInTargets(latestCheckIn.Id)) {
			assignPlanInfo.nutritionTargets.put(nTarget.id, nTarget);
		}


		return assignPlanInfo;
	}

	@AuraEnabled
	public static Map<Id, List<String>> GetTargetAssignToDays(String checkInId) {

		Check_In__c checkIn = [
			SELECT
				Id,
				Nutrition_Planning_Mode__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		List<Targets__c> checkInTargets = [
			SELECT
				Id,
				Weekdays__c,
				Assign_to_Weeks__c
			FROM Targets__c
			WHERE Check_In__c = :checkInId
		];

		Map<Id, List<String>> targetAssignToDays = new Map<Id, List<String>>();

		if(checkIn.Nutrition_Planning_Mode__c == 'Carb-Cycle' || checkIn.Nutrition_Planning_Mode__c == 'Flexi Plans') {
			for(Targets__c t : checkInTargets) {
				if(t.Weekdays__c != null) {
					targetAssignToDays.put(t.Id, t.Weekdays__c.split(';'));
				}
			}
		} else if(checkIn.Nutrition_Planning_Mode__c == 'Guided Plans') {
		
			for(Targets__c t : checkInTargets) {
				if(t.Assign_to_Weeks__c != null) {
					targetAssignToDays.put(t.Id, t.Assign_to_Weeks__c.split(';'));
				}
			}
        }

		return targetAssignToDays;

	}

    /**
     * @description 
     */
    public class FlexiPlanDay {

        /**
         * @description The related pool plan
         */
        @AuraEnabled 
        public String poolPlanId { get; set; }

        /**
         * @description The Id of the Target
         */
        @AuraEnabled
        public String targetId { get; set; }

        /**
         * @description 
         */
        @AuraEnabled
        public Date planDate { get; set; }

        /**
         * @description 
         */
        public FlexiPlanDay() { }

    }

    /**
     * @description 
     */
    public class CarbCyclePlanDay {

        /**
         * @description The related pool plan
         */
        @AuraEnabled 
        public String poolPlanId { get; set; }

        /**
         * @description The Id of the Target
         */
        @AuraEnabled
        public String targetId { get; set; }

        /**
         * @description 
         */
        @AuraEnabled
        public Date planDate { get; set; }

        /**
         * @description 
         */
        public CarbCyclePlanDay() { }

    }
	
    /**
    * @description Clear the Plan Days, and the related Session Plans
    * @param flexiPlanDays 
    */ 
    private static void p_clearPlanDays(List<CarbCyclePlanDay> carbCycleDays) {

        //Query the related Plan Days
        Set<Id> targetIds       = new Set<Id>();
        List<Date> flexiDates   = new List<Date>();
        for(CarbCyclePlanDay fPlanDay : carbCycleDays) {
            targetIds.add(fPlanDay.targetId);
            flexiDates.add(fPlanDay.planDate);
        }
        List<Plan_Day__c> planDays = [
            SELECT 
                Id, 
                Date__c, 
                Pool_Plan__c 
            FROM Plan_Day__c 
            WHERE Targets__c IN :targetIds
        ];

        //Query the active Pool Plan Version
        Set<Id> poolPlanIds = new Set<Id>();
        for(Plan_Day__c pDay : planDays) {
            poolPlanIds.add(pDay.Pool_Plan__c);
        }

        List<Pool_Plan_Version__c> activeVersions = [
            SELECT
                Id,
                Reference_Plan__c
            FROM Pool_Plan_Version__c
            WHERE Pool_Plan__c IN :poolPlanIds AND Active_Version__c = true
        ];
        Set<Id> refPlanIds = new Set<Id>();
        for(Pool_Plan_Version__c ppV : activeVersions) {
            refPlanIds.add(ppV.Reference_Plan__c);
        }

        //Query the related Reference Plan, with it's generated session plans
        List<Plan__c> sessionPlans = [
            SELECT
                Id,
                Session_Date__c
            FROM Plan__c
            WHERE Reference_Plan__c IN :refPlanIds AND RecordType.DeveloperName = 'Nutrition_Session_Plan' AND Session_Date__c IN :flexiDates
        ];

        if(sessionPlans.size() > 0) {
            delete sessionPlans;
        }

    }

    /**
    * @description Clear the Plan Days, and the related Session Plans
    * @param flexiPlanDays 
    */ 
    private static void p_clearPlanDays(List<FlexiPlanDay> flexiPlanDays) {

        //Query the related Plan Days
        Set<Id> targetIds       = new Set<Id>();
        List<Date> flexiDates   = new List<Date>();
        for(FlexiPlanDay fPlanDay : flexiPlanDays) {
            targetIds.add(fPlanDay.targetId);
            flexiDates.add(fPlanDay.planDate);
        }
        List<Plan_Day__c> planDays = [
            SELECT 
                Id, 
                Date__c, 
                Pool_Plan__c 
            FROM Plan_Day__c 
            WHERE Targets__c IN :targetIds
        ];

        //Query the active Pool Plan Version
        Set<Id> poolPlanIds = new Set<Id>();
        for(Plan_Day__c pDay : planDays) {
            poolPlanIds.add(pDay.Pool_Plan__c);
        }

        List<Pool_Plan_Version__c> activeVersions = [
            SELECT
                Id,
                Reference_Plan__c
            FROM Pool_Plan_Version__c
            WHERE Pool_Plan__c IN :poolPlanIds AND Active_Version__c = true
        ];
        Set<Id> refPlanIds = new Set<Id>();
        for(Pool_Plan_Version__c ppV : activeVersions) {
            refPlanIds.add(ppV.Reference_Plan__c);
        }

        //Query the related Reference Plan, with it's generated session plans
        List<Plan__c> sessionPlans = [
            SELECT
                Id,
                Session_Date__c
            FROM Plan__c
            WHERE Reference_Plan__c IN :refPlanIds AND RecordType.DeveloperName = 'Nutrition_Session_Plan' AND Session_Date__c IN :flexiDates
        ];

        if(sessionPlans.size() > 0) {
            delete sessionPlans;
        }

    }

	private static void p_createFlexiPlanDays(List<FlexiPlanDay> flexiPlanDays, Check_In__c checkIn) {
	
        //Create the new plan days
        List<Plan_Day__c> newPlanDays = new List<Plan_Day__c>();

		Map<Id, List<Date>> poolPlanToDates = new Map<Id, List<Date>>();
        for(Integer i = 0; i < flexiPlanDays.size(); i++) {
            Plan_Day__c newPlanDay              = new Plan_Day__c();
            newPlanDay.Targets__c               = flexiPlanDays.get(i).targetId;
            newPlanDay.Date__c                  = flexiPlanDays.get(i).planDate;
            newPlanDay.Pool_Plan__c				= flexiPlanDays.get(i).poolPlanId;
            newPlanDays.add(newPlanDay);

			//Get the Pool Plans, and the dates that it's being used for
			if(!poolPlanToDates.containsKey(newPlanDay.Pool_Plan__c)) {
				poolPlanToDates.put(newPlanDay.Pool_Plan__c, new List<Date>());
			}
			poolPlanToDates.get(newPlanDay.Pool_Plan__c).add(newPlanDay.Date__c);

        }

        if(newPlanDays.size() > 0) {
            insert newPlanDays;
        }

		//Get the Active Version for each pool plan
		List<Pool_Plan_Version__c> activePoolPlanVersions = [
			SELECT
				Id,
				Pool_Plan__c,
				Reference_Plan__c
			FROM Pool_Plan_Version__c
            WHERE Pool_Plan__c IN :poolPlanToDates.keySet() AND Active_Version__c = true
		];

		List<Id> refPlanIds = new List<Id>();
		Map<Id, Pool_Plan_Version__c> poolPlanToPoolPlanVersion = new Map<Id, Pool_Plan_Version__c>();
		for(Pool_Plan_Version__c ppV : activePoolPlanVersions) {
			poolPlanToPoolPlanVersion.put(ppV.Pool_Plan__c, ppv);
			for(Date sessDate : poolPlanToDates.get(ppV.Pool_Plan__c)) {
				refPlanIds.add(ppV.Reference_Plan__c);
			}
		}

		RecordType nutritionSessionPlan = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'];

		Map<Id, List<Plan__c>> newPlans = AT_M_Nutrition.SaveAsNewPlans(refPlanIds, nutritionSessionPlan.Id);

		List<Plan__c> updatedPlans = new List<Plan__c>();
		for(Pool_Plan_Version__c ppv : activePoolPlanVersions) {
			List<Plan__c> plansCloned	= newPlans.get(ppv.Reference_Plan__c);
			List<Date> sessionDates		= poolPlanToDates.get(ppv.Pool_Plan__c);

			for(Integer i = 0; i < sessionDates.size(); i++) {
				Plan__c newSessPlan				= plansCloned.get(i);
				Date sessDate					= sessionDates.get(i);
				newSessPlan.Reference_Plan__c	= ppv.Reference_Plan__c;
				newSessPlan.Session_Date__c		= sessDate;
				newSessPlan.Check_In__c			= checkIn.Id;
				newSessPlan.Check_In_ns__c		= checkIn.Id;
				newSessPlan.Account__c			= checkIn.Account__c;
				newSessPlan.Account_ns__c		= checkIn.Account__c;
				updatedPlans.add(newSessPlan);
			}
		}

		update updatedPlans;

	}
	
	private static void p_createCarbCyclePlanDays(List<CarbCyclePlanDay> carbCyclePlanDays, Check_In__c checkIn) {
	
        //Create the new plan days
        List<Plan_Day__c> newPlanDays = new List<Plan_Day__c>();

		Map<Id, List<Date>> poolPlanToDates = new Map<Id, List<Date>>();
        for(Integer i = 0; i < carbCyclePlanDays.size(); i++) {
            Plan_Day__c newPlanDay              = new Plan_Day__c();
            newPlanDay.Targets__c               = carbCyclePlanDays.get(i).targetId;
            newPlanDay.Date__c                  = carbCyclePlanDays.get(i).planDate;
            newPlanDay.Pool_Plan__c				= carbCyclePlanDays.get(i).poolPlanId;
            newPlanDays.add(newPlanDay);

			//Get the Pool Plans, and the dates that it's being used for
			if(!poolPlanToDates.containsKey(newPlanDay.Pool_Plan__c)) {
				poolPlanToDates.put(newPlanDay.Pool_Plan__c, new List<Date>());
			}
			poolPlanToDates.get(newPlanDay.Pool_Plan__c).add(newPlanDay.Date__c);

        }

        if(newPlanDays.size() > 0) {
            insert newPlanDays;
        }

		//Get the Active Version for each pool plan
		List<Pool_Plan_Version__c> activePoolPlanVersions = [
			SELECT
				Id,
				Pool_Plan__c,
				Reference_Plan__c
			FROM Pool_Plan_Version__c
            WHERE Pool_Plan__c IN :poolPlanToDates.keySet() AND Active_Version__c = true
		];

		List<Id> refPlanIds = new List<Id>();
		Map<Id, Pool_Plan_Version__c> poolPlanToPoolPlanVersion = new Map<Id, Pool_Plan_Version__c>();
		for(Pool_Plan_Version__c ppV : activePoolPlanVersions) {
			poolPlanToPoolPlanVersion.put(ppV.Pool_Plan__c, ppv);
			for(Date sessDate : poolPlanToDates.get(ppV.Pool_Plan__c)) {
				refPlanIds.add(ppV.Reference_Plan__c);
			}
		}

		RecordType nutritionSessionPlan = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'];

		Map<Id, List<Plan__c>> newPlans = AT_M_Nutrition.SaveAsNewPlans(refPlanIds, nutritionSessionPlan.Id);

		List<Plan__c> updatedPlans = new List<Plan__c>();
		for(Pool_Plan_Version__c ppv : activePoolPlanVersions) {
			List<Plan__c> plansCloned	= newPlans.get(ppv.Reference_Plan__c);
			List<Date> sessionDates		= poolPlanToDates.get(ppv.Pool_Plan__c);

			for(Integer i = 0; i < sessionDates.size(); i++) {
				Plan__c newSessPlan				= plansCloned.get(i);
				Date sessDate					= sessionDates.get(i);
				newSessPlan.Reference_Plan__c	= ppv.Reference_Plan__c;
				newSessPlan.Session_Date__c		= sessDate;
				newSessPlan.Check_In__c			= checkIn.Id;
				newSessPlan.Check_In_ns__c		= checkIn.Id;
				newSessPlan.Account__c			= checkIn.Account__c;
				newSessPlan.Account_ns__c		= checkIn.Account__c;
				updatedPlans.add(newSessPlan);
			}
		}

		update updatedPlans;

	}


    private static void ClearPlanDays(String checkInId) {

        //Query the Target Ids related to the Check-In
        Set<String> targetIds = new Map<String, Targets__c>([SELECT Id FROM Targets__c WHERE Check_In__c = :checkInId]).KeySet();

        //Delete the Plan Days related to the targets
        List<Plan_Day__c> checkInDays = [
            SELECT 
                Id,
                Date__c,
                Week__c,
                Pool_Plan__c
            FROM Plan_Day__c 
            WHERE Targets__c IN :targetIds
        ];
        if(checkInDays.size() > 0) {
            delete checkInDays;
        }
    }
    
    @AuraEnabled
    public static void SaveFlexiPlanTargets(String checkInId, List<FlexiPlanDay> flexiPlanDays) {
		Check_In__c checkIn = [SELECT Id, Account__c FROM Check_In__c WHERE Id = :checkInId];
        //Clear the Plan Days
        p_clearPlanDays(flexiPlanDays);
		p_createFlexiPlanDays(flexiPlanDays, checkIn);
    }
	
    @AuraEnabled
    public static void SaveCarbCycleTargets(String checkInId, List<CarbCyclePlanDay> carbCycleDays) {
		Check_In__c checkIn = [SELECT Id, Account__c FROM Check_In__c WHERE Id = :checkInId];
        //Clear the Plan Days
        p_clearPlanDays(carbCycleDays);
		p_createCarbCyclePlanDays(carbCycleDays , checkIn);
    }

    public class GuidedPlanDay {

        /**
         * @description The related pool plan
         */
        @AuraEnabled 
        public String poolPlanId { get; set; }

        /**
         * @description The Id of the Target
         */
        @AuraEnabled
        public String targetId { get; set; }

        /**
         * @description The Assigned Week
         */
        @AuraEnabled
        public String assignedWeek { get; set; }

        /**
         * @description 
         */
        @AuraEnabled
        public Date startDate { get; set; }

        /**
         * @description 
         */
        @AuraEnabled
        public Date endDate { get; set; }

    }

    @AuraEnabled
    public static void SaveGuidedPlanTargets(String checkInId, List<GuidedPlanDay> guidedPlanDays) {

        //Clear the Plan Days
        ClearPlanDays(checkInId);

        List<Plan_Day__c> newPlanDays = new List<Plan_Day__c>();

        for(Integer i = 0; i < guidedPlanDays.size(); i++) {
            GuidedPlanDay currPlanDay = guidedPlanDays.get(i);

            System.debug(currPlanDay);

            Date currDate = currPlanDay.startDate;
            while(currDate <= currPlanDay.endDate) {
                Plan_Day__c newPlanDay              = new Plan_Day__c();
                newPlanDay.Targets__c               = currPlanDay.targetId;
                newPlanDay.Date__c                  = currDate;
                newPlanDay.Week__c                  = currPlanDay.assignedWeek;
                newPlanDay.Pool_Plan__c				= currPlanDay.poolPlanId;
                newPlanDays.add(newPlanDay);
                currDate = currDate.addDays(1);
            }
        }

        if(newPlanDays.size() > 0) {

            insert newPlanDays;

            for(Plan_Day__c pDay : newPlanDays) {
                System.debug(pDay);
            }

        } 

    }

}