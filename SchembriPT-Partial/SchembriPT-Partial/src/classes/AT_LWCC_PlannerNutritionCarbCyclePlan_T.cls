/**
* @author Harrison Campbell
* @description Tests the 'AT_LWCC_PlannerNutritionCarbCyclePlan' class
*/ 
@IsTest
public without sharing class AT_LWCC_PlannerNutritionCarbCyclePlan_T {

	private static RecordType testTool_getRecordType(String developerName, String sObjectName) {
		return [SELECT Id FROM RecordType WHERE SobjectType = :sObjectName AND DeveloperName = :developerName];
	}

	private static Plan__c testTool_createPlan(String name, Decimal carbs, Decimal fat, Decimal protein, Decimal calories, Template_Plan__c tmpPlan, Id recTypeId) {
		Plan__c newPlan						= new Plan__c();
		newPlan.RecordTypeId				= recTypeId;
		newPlan.Name						= name;
		newPlan.Trainer_Plan_Calories__c	= calories;
		newPlan.Trainer_Plan_Carbs__c		= carbs;
		newPlan.Trainer_Plan_Fat__c			= fat;
		newPlan.Trainer_Plan_Protein__c		= protein;
		newPlan.Template_Plan__c			= tmpPlan.Id;
		newPlan.Template_Plan_Calories__c	= tmpPlan.Template_Plan_Calories__c;
		newPlan.Template_Plan_Carbs__c		= tmpPlan.Template_Plan_Carbs__c;
		newPlan.Template_Plan_Fat__c		= tmpPlan.Template_Plan_Fat__c;
		newPlan.Template_Plan_Protein__c	= tmpPlan.Template_Plan_Protein__c;
		insert newPlan;
		return newPlan;
	}

	private static Block__c testTool_createBlock(String name, Decimal carbs, Decimal fat, Decimal protein, Decimal calories, Integer order, Template_Block__c tmpBlock, Template_Plan_Block__c tmpBlockPlan, Plan__c plan) {
		Block__c newBlock = new Block__c();

		newBlock.Name						= name;
		newBlock.Trainer_Block_Calories__c	= calories;
		newBlock.Trainer_Block_Carbs__c		= carbs;
		newBlock.Trainer_Block_Fat__c		= fat;
		newBlock.Trainer_Block_Protein__c	= protein;
		newBlock.Plan__c					= plan.Id;


		newBlock.Template_Block_Calories__c	= tmpBlock.Template_Block_Calories__c;
		newBlock.Template_Block_Carbs__c	= tmpBlock.Template_Block_Carbs__c;
		newBlock.Template_Block_Fat__c		= tmpBlock.Template_Block_Fat__c;
		newBlock.Template_Block_Protein__c	= tmpBlock.Template_Block_Protein__c;

		newBlock.Display_Order__c			= order;

		newBlock.Template_Block_v2__c		= tmpBlock.Id;
		if(tmpBlockPlan != null) {
			newBlock.Template_Plan_Block__c		= tmpBlockPlan.Id;
		}

		insert newBlock;
		return newBlock;
	}

	private static Item__c testTool_createItem(String name, Decimal carbs, Decimal fat, Decimal protein, Decimal calories, Integer order, Template_Item__c tmpItem, Template_Block_Item__c tmpBlkItem, Block__c blk) {

		Item__c newItem							= new Item__c();
		newItem.Block__c						= blk.Id;
		newItem.Name							= name;
		newItem.Trainer_Calories__c				= calories;
		newItem.Trainer_Carbs__c				= carbs;
		newItem.Trainer_Fat__c					= fat;
		newItem.Trainer_Protein__c				= protein;
		newItem.Display_Order__c				= order;
		newItem.Template_Item_Id__c				= tmpItem.Id;
		newItem.Template_Item_Name__c			= tmpItem.Name;
		if(tmpBlkItem != null) {
			newItem.Template_Block_Item__c			= tmpBlkItem.Id;
		}
		newItem.Template_Quantity__c			= tmpItem.Quantity__c;
		newItem.Template_Calories__c			= tmpItem.Calories__c;
		newItem.Template_Carbs__c				= tmpItem.Carbs__c;
		newItem.Template_Fat__c					= tmpItem.Fat__c;
		newItem.Template_Protein__c				= tmpItem.Protein__c;
		newItem.Template_Measurement__c			= tmpItem.Measurement__c;
		newItem.Template_Quantity_Increment__c	= tmpItem.Quantity_Increment__c;
		newItem.Template_Minimum_Quantity__c	= tmpItem.Minimum_Quantity__c;
		insert newItem;
		return newItem;
	}

	private static Template_Plan__c testTool_createTemplatePlan(String name, Decimal carbs, Decimal fat, Decimal protein, Decimal calories, Id recordTypeId) {
		Template_Plan__c tmpPlan = new Template_Plan__c();
		tmpPlan.RecordTypeId				= recordTypeId;
		tmpPlan.Template_Plan_Carbs__c		= carbs;
		tmpPlan.Template_Plan_Fat__c		= fat;
		tmpPlan.Template_Plan_Protein__c	= protein;
		tmpPlan.Template_Plan_Calories__c	= calories;
		insert tmpPlan;
		return tmpPlan;
	}

	private static Template_Block__c testTool_createTemplateBlock(String name, Decimal carbs, Decimal fat, Decimal protein, Decimal calories, Id recordTypeId) {
		Template_Block__c tmpBlock			= new Template_Block__c();
		tmpBlock.RecordTypeId				= recordTypeId;
		tmpBlock.Template_Block_Carbs__c		= carbs;
		tmpBlock.Template_Block_Fat__c		= fat;
		tmpBlock.Template_Block_Protein__c	= protein;
		tmpBlock.Template_Block_Calories__c	= calories;
		insert tmpBlock;
		return tmpBlock;
	}

	private static Template_Item__c testTool_createTemplateItem(String name, Decimal carbs, Decimal fat, Decimal protein, Decimal calories, String measurement, Decimal quantityIncrement, Decimal minimumQuantity, Id recordTypeId) {
		Template_Item__c newTmpItem			= new Template_Item__c();
		newTmpItem.RecordTypeId				= recordTypeId;
		newTmpItem.Name						= name;
		newTmpItem.Carbs__c					= carbs;
		newTmpItem.Fat__c					= fat;
		newTmpItem.Protein__c				= protein;
		newTmpItem.Measurement__c			= measurement;
		newTmpItem.Quantity_Increment__c	= quantityIncrement;
		newTmpItem.Minimum_Quantity__c		= minimumQuantity;
		insert newTmpItem;
		return newTmpItem;

	}

	private static Template_Plan_Block__c testTool_createTemplatePlanBlock(Template_Plan__c tmpPlan, Template_Block__c tmpBlock, Integer order) {
		Template_Plan_Block__c tmpPlanBlock = new Template_Plan_Block__c();
		tmpPlanBlock.Template_Plan__c		= tmpPlan.Id;
		tmpPlanBlock.Template_Block__c		= tmpBlock.Id;
		tmpPlanBlock.Order__c				= order;
		insert tmpPlanBlock;
		return tmpPlanBlock;
	}

	private static Template_Block_Item__c testTool_createTemplateBlockItem(Template_Block__c tmpBlock, Template_Item__c tmpItem, Integer order) {
		Template_Block_Item__c newTmpBlockItem	= new Template_Block_Item__c();
		newTmpBlockItem.Template_Block__c		= tmpBlock.Id;
		newTmpBlockItem.Template_Item__c		= tmpItem.Id;
		newTmpBlockItem.Display_Order__c		= order;
		insert newTmpBlockItem;
		return newTmpBlockItem;
	}

	private static void assertTool_assertPlan(Plan__c planRef, AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nRefPlan) {
		System.assertEquals(planRef.Id,							nRefPlan.planId);
		System.assertEquals(planRef.Name,						nRefPlan.planName);
		System.assertEquals(planRef.Trainer_Plan_Calories__c,	nRefPlan.trainerCalories);
		System.assertEquals(planRef.Trainer_Plan_Carbs__c,		nRefPlan.trainerCarbs);
		System.assertEquals(planRef.Trainer_Plan_Fat__c,		nRefPlan.trainerFat);
		System.assertEquals(planRef.Trainer_Plan_Protein__c,	nRefPlan.trainerProtein);
	}

	private static void assertTool_assertBlock(Block__c blockRef, AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock nRefBlock) {
		System.assertEquals(blockRef.Id,							nRefBlock.blockId);
		System.assertEquals(blockRef.Name,							nRefBlock.blockName);
		System.assertEquals(blockRef.Plan__c,						nRefBlock.planId);
		System.assertEquals(blockRef.Trainer_Block_Calories__c,		nRefBlock.trainerCalories);
		System.assertEquals(blockRef.Trainer_Block_Carbs__c,		nRefBlock.trainerCarbs);
		System.assertEquals(blockRef.Trainer_Block_Fat__c,			nRefBlock.trainerFat);
		System.assertEquals(blockRef.Trainer_Block_Protein__c,		nRefBlock.trainerProtein);
		System.assertEquals(blockRef.Template_Block_v2__c,			nRefBlock.templateBlockId);
		System.assertEquals(blockRef.Template_Block_Calories__c,	nRefBlock.templateBlockCalories);
		System.assertEquals(blockRef.Template_Block_Carbs__c,		nRefBlock.templateBlockCarbs);
		System.assertEquals(blockRef.Template_Block_Fat__c,			nRefBlock.templateBlockFat);
		System.assertEquals(blockRef.Template_Block_Protein__c,		nRefBlock.templateBlockProtein);
		System.assertEquals(blockRef.Display_Order__c,				nRefBlock.order);
	}

	private static void assertTool_assertItem(Item__c itemRef, AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem nRefItem) {
		System.assertEquals(itemRef.Id,								nRefItem.itemId);
		System.assertEquals(itemRef.Name,							nRefItem.itemName);
		System.assertEquals(itemRef.Trainer_Quantity__c,			nRefItem.trainerQuantity);
		System.assertEquals(itemRef.Trainer_Calories__c,			nRefItem.trainerCalories);
		System.assertEquals(itemRef.Trainer_Carbs__c,				nRefItem.trainerCarbs);
		System.assertEquals(itemRef.Trainer_Fat__c,					nRefItem.trainerFat);
		System.assertEquals(itemRef.Trainer_Protein__c,				nRefItem.trainerProtein);
		System.assertEquals(itemRef.Display_Order__c,				nRefItem.order);
		System.assertEquals(itemRef.Template_Item_Id__c,			nRefItem.templateId);
		System.assertEquals(itemRef.Template_Item_Name__c,			nRefItem.templateItemName);
		System.assertEquals(itemRef.Template_Block_Item__c,			nRefItem.templateItemBlockId);
		System.assertEquals(itemRef.Template_Quantity__c,			nRefItem.templateQuantity);
		System.assertEquals(itemRef.Template_Calories__c,			nRefItem.templateCalories);
		System.assertEquals(itemRef.Template_Carbs__c,				nRefItem.templateCarbs);
		System.assertEquals(itemRef.Template_Fat__c,				nRefItem.templateFat);
		System.assertEquals(itemRef.Template_Protein__c,			nRefItem.templateProtein);
		System.assertEquals(itemRef.Template_Measurement__c,		nRefItem.templateMeasurement);
		System.assertEquals(itemRef.Template_Quantity_Increment__c,	nRefItem.templateQuantityIncrement);
		System.assertEquals(itemRef.Template_Minimum_Quantity__c,	nRefItem.templateMinimumQuantity);
	}

	@IsTest
	private static void GetNutritionalPlan_GetPlanAndBlocksAndItems() {

		RecordType nutTempPlanRecType			= testTool_getRecordType('Template_Nutrition_Plan', 'Template_Plan__c');
		RecordType nutTempBlockRecType			= testTool_getRecordType('Template_Nutrition_Block', 'Template_Block__c');
		RecordType nutTempItemRecType			= testTool_getRecordType('Nutrition', 'Template_Item__c');
		RecordType nutPlanRecType				= testTool_getRecordType('Nutrition_Reference_Plan', 'Plan__c');
		RecordType nutBlockRecType				= testTool_getRecordType('Nutrition_Reference_Block', 'Block__c');
		RecordType nutItemRecType				= testTool_getRecordType('Nutrition_Reference_Item', 'Item__c');

		Template_Plan__c tmpPlan				= testTool_createTemplatePlan('Test Template Plan', 100, 200, 300, 400, nutTempPlanRecType.Id);
		Template_Block__c tmpBlock				= testTool_createTemplateBlock('Test Template Block', 100, 200, 300, 400, nutTempBlockRecType.Id);
		Template_Item__c tmpItem				= testTool_createTemplateItem('Test Template Item', 100, 200, 300, 400, 'Slice', 1, 0, nutTempItemRecType.Id);

		Template_Plan_Block__c newPlanBlock		= testTool_createTemplatePlanBlock(tmpPlan, tmpBlock, 0);
		Template_Block_Item__c newBlockItem		= testTool_createTemplateBlockItem(tmpBlock, tmpItem, 0);

		Plan__c newPlan		= testTool_createPlan('Test Plan', 100, 200, 300, 400, tmpPlan, nutPlanRecType.Id);
		Block__c newBlock	= testTool_createBlock('Test Block', 100, 200, 300, 400, 0, tmpBlock, newPlanBlock, newPlan);
		Item__c newItem		= testTool_createItem('Test Item', 100, 200, 300, 400, 0, tmpItem, newBlockItem, newBlock);

		Test.startTest();
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nutRefPlan = AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan(newPlan.Id);
		Test.stopTest();

		assertTool_assertPlan(newPlan, nutRefPlan);
		System.assertEquals(1, nutRefPlan.blocks.size());
		assertTool_assertBlock(newBlock, nutRefPlan.blocks.get(0));
		System.assertEquals(1, nutRefPlan.blocks.get(0).items.size());
		assertTool_assertItem(newItem, nutRefPlan.blocks.get(0).items.get(0));
	}
	
	@IsTest
	private static void GetNutritionalPlan_GetPlanAndBlocks() {

		RecordType nutTempPlanRecType			= testTool_getRecordType('Template_Nutrition_Plan', 'Template_Plan__c');
		RecordType nutTempBlockRecType			= testTool_getRecordType('Template_Nutrition_Block', 'Template_Block__c');
		RecordType nutTempItemRecType			= testTool_getRecordType('Nutrition', 'Template_Item__c');
		RecordType nutPlanRecType				= testTool_getRecordType('Nutrition_Reference_Plan', 'Plan__c');
		RecordType nutBlockRecType				= testTool_getRecordType('Nutrition_Reference_Block', 'Block__c');

		Template_Plan__c tmpPlan				= testTool_createTemplatePlan('Test Template Plan', 100, 200, 300, 400, nutTempPlanRecType.Id);
		Template_Block__c tmpBlock				= testTool_createTemplateBlock('Test Template Block', 100, 200, 300, 400, nutTempBlockRecType.Id);
		Template_Item__c tmpItem				= testTool_createTemplateItem('Test Template Item', 100, 200, 300, 400, 'Slice', 1, 0, nutTempItemRecType.Id);

		Template_Plan_Block__c newPlanBlock		= testTool_createTemplatePlanBlock(tmpPlan, tmpBlock, 0);
		Template_Block_Item__c newBlockItem		= testTool_createTemplateBlockItem(tmpBlock, tmpItem, 0);

		Plan__c newPlan		= testTool_createPlan('Test Plan', 100, 200, 300, 400, tmpPlan, nutPlanRecType.Id);
		Block__c newBlock	= testTool_createBlock('Test Block', 100, 200, 300, 400, 0, tmpBlock, newPlanBlock, newPlan);

		Test.startTest();
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nutRefPlan = AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan(newPlan.Id);
		Test.stopTest();

		assertTool_assertPlan(newPlan, nutRefPlan);
		System.assertEquals(1, nutRefPlan.blocks.size());
		assertTool_assertBlock(newBlock, nutRefPlan.blocks.get(0));
		System.assertEquals(0, nutRefPlan.blocks.get(0).items.size());

	}
	
	@IsTest
	private static void GetNutritionalPlan_GetPlan() {

		RecordType nutTempPlanRecType			= testTool_getRecordType('Template_Nutrition_Plan', 'Template_Plan__c');
		RecordType nutTempBlockRecType			= testTool_getRecordType('Template_Nutrition_Block', 'Template_Block__c');
		RecordType nutTempItemRecType			= testTool_getRecordType('Nutrition', 'Template_Item__c');
		RecordType nutPlanRecType				= testTool_getRecordType('Nutrition_Reference_Plan', 'Plan__c');

		Template_Plan__c tmpPlan				= testTool_createTemplatePlan('Test Template Plan', 100, 200, 300, 400, nutTempPlanRecType.Id);
		Template_Block__c tmpBlock				= testTool_createTemplateBlock('Test Template Block', 100, 200, 300, 400, nutTempBlockRecType.Id);
		Template_Item__c tmpItem				= testTool_createTemplateItem('Test Template Item', 100, 200, 300, 400, 'Slice', 1, 0, nutTempItemRecType.Id);

		Template_Plan_Block__c newPlanBlock		= testTool_createTemplatePlanBlock(tmpPlan, tmpBlock, 0);
		Template_Block_Item__c newBlockItem		= testTool_createTemplateBlockItem(tmpBlock, tmpItem, 0);

		Plan__c newPlan		= testTool_createPlan('Test Plan', 100, 200, 300, 400, tmpPlan, nutPlanRecType.Id);

		Test.startTest();
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nutRefPlan = AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan(newPlan.Id);
		Test.stopTest();

		assertTool_assertPlan(newPlan, nutRefPlan);
		System.assertEquals(0, nutRefPlan.blocks.size());

	}

	@IsTest
	private static void GetNutritionalPlan_GetNonNutritionalPlan() {
		
		RecordType nutTempPlanRecType			= testTool_getRecordType('Template_Nutrition_Plan', 'Template_Plan__c');
		RecordType nutTempBlockRecType			= testTool_getRecordType('Template_Nutrition_Block', 'Template_Block__c');
		RecordType nutTempItemRecType			= testTool_getRecordType('Nutrition', 'Template_Item__c');
		RecordType nutPlanRecType				= testTool_getRecordType('Nutrition_Session_Plan', 'Plan__c');

		Template_Plan__c tmpPlan				= testTool_createTemplatePlan('Test Template Plan', 100, 200, 300, 400, nutTempPlanRecType.Id);
		Template_Block__c tmpBlock				= testTool_createTemplateBlock('Test Template Block', 100, 200, 300, 400, nutTempBlockRecType.Id);
		Template_Item__c tmpItem				= testTool_createTemplateItem('Test Template Item', 100, 200, 300, 400, 'Slice', 1, 0, nutTempItemRecType.Id);

		Template_Plan_Block__c newPlanBlock		= testTool_createTemplatePlanBlock(tmpPlan, tmpBlock, 0);
		Template_Block_Item__c newBlockItem		= testTool_createTemplateBlockItem(tmpBlock, tmpItem, 0);

		Plan__c newPlan		= testTool_createPlan('Test Plan', 100, 200, 300, 400, tmpPlan, nutPlanRecType.Id);

		Boolean exceptionCaught = false;

		Test.startTest();
		try{			
			AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nutRefPlan = AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan(newPlan.Id);
		} catch (AT_LWCC_PlannerNutritionCarbCyclePlan.NoPlanFoundException e){
			exceptionCaught = true;
		}
		Test.stopTest();

		System.assert(exceptionCaught);

	}
	
	@IsTest
	private static void GetNutritionalPlan_NoPlanId() {
		
		Boolean exceptionCaught = false;
		
		Test.startTest();
		try{			
			AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nutRefPlan = AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan(null);
		} catch (AT_LWCC_PlannerNutritionCarbCyclePlan.NoPlanFoundException e){
			exceptionCaught = true;
		}
		Test.stopTest();

		System.assert(exceptionCaught);

	}

	/**
	* @description NEED TO REPLACE WITH PROPER TESTS!!!!
	*/ 
	@IsTest
	private static void UpdatePlanData_FunctionalTest() {

		//NOTE: Should really create new NutritionReferencePlan
		RecordType nutTempPlanRecType			= testTool_getRecordType('Template_Nutrition_Plan', 'Template_Plan__c');
		RecordType nutTempBlockRecType			= testTool_getRecordType('Template_Nutrition_Block', 'Template_Block__c');
		RecordType nutTempItemRecType			= testTool_getRecordType('Nutrition', 'Template_Item__c');
		RecordType nutPlanRecType				= testTool_getRecordType('Nutrition_Reference_Plan', 'Plan__c');
		RecordType nutBlockRecType				= testTool_getRecordType('Nutrition_Reference_Block', 'Block__c');
		RecordType nutItemRecType				= testTool_getRecordType('Nutrition_Reference_Item', 'Item__c');

		Template_Plan__c tmpPlan				= testTool_createTemplatePlan('Test Template Plan', 100, 200, 300, 400, nutTempPlanRecType.Id);
		Template_Block__c tmpBlock				= testTool_createTemplateBlock('Test Template Block', 100, 200, 300, 400, nutTempBlockRecType.Id);
		Template_Item__c tmpItem				= testTool_createTemplateItem('Test Template Item', 100, 200, 300, 400, 'Slice', 1, 0, nutTempItemRecType.Id);

		Template_Plan_Block__c newPlanBlock		= testTool_createTemplatePlanBlock(tmpPlan, tmpBlock, 0);
		Template_Block_Item__c newBlockItem		= testTool_createTemplateBlockItem(tmpBlock, tmpItem, 0);

		Plan__c newPlan		= testTool_createPlan('Test Plan', 100, 200, 300, 400, tmpPlan, nutPlanRecType.Id);
		Block__c newBlock	= testTool_createBlock('Test Block', 100, 200, 300, 400, 0, tmpBlock, newPlanBlock, newPlan);
		Item__c newItem		= testTool_createItem('Test Item', 100, 200, 300, 400, 0, tmpItem, newBlockItem, newBlock);

		Block__c newBlock2	= testTool_createBlock('Test Block 2', 100, 200, 300, 400, 1, tmpBlock, null, newPlan);
		Item__c newItem2	= testTool_createItem('Test Item 2', 100, 200, 300, 400, 0, tmpItem, null, newBlock2);

		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferencePlan nutRefPlan = AT_LWCC_PlannerNutritionCarbCyclePlan.GetNutritionalPlan(newPlan.Id);
		nutRefPlan.blocks.remove(0);
		System.debug(nutRefPlan.blocks);
		nutRefPlan.blocks.get(0).items.remove(0);
		
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock newRefBlock = new AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock();
		newRefBlock.blockName		= 'Empty Block';
		newRefBlock.planId			= newPlan.Id;
		newRefBlock.trainerCalories			= 0;
		newRefBlock.trainerCarbs			= 0;
		newRefBlock.trainerFat				= 0;
		newRefBlock.trainerProtein			= 0;
		newRefBlock.templateBlockId			= tmpBlock.Id;
		newRefBlock.templateBlockCalories	= 1;
		newRefBlock.templateBlockCarbs		= 2;
		newRefBlock.templateBlockFat		= 3;
		newRefBlock.templateBlockProtein	= 4;
		newRefBlock.order					= 0;
		newRefBlock.items					= new List<AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem>();
		nutRefPlan.blocks.add(newRefBlock);

		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem newRefItem = new AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem();
		newRefItem.itemName						= '?';
		newRefItem.trainerCalories				= 0;
		newRefItem.trainerCarbs					= 0;
		newRefItem.trainerFat					= 0;
		newRefItem.trainerProtein				= 0;
		newRefItem.trainerQuantity				= 1;
		newRefItem.order						= 0;
		newRefItem.templateId					= tmpItem.Id;
		newRefItem.templateItemBlockId			= newBlockItem.Id;
		newRefItem.templateItemName				= '?';
		newRefItem.templateQuantity				= 0;
		newRefItem.templateCalories				= 0;
		newRefItem.templateCarbs				= 0;
		newRefItem.templateFat					= 0;
		newRefItem.templateProtein				= 0;
		newRefItem.templateMeasurement			= 'Grams';
		newRefItem.templateQuantityIncrement	= 1;
		newRefItem.templateMinimumQuantity		= 0;
		newRefBlock.items.add(newRefItem);

		
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem newRefItem2 = new AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem();
		newRefItem2.itemName					= '?';
		newRefItem2.trainerCalories				= 0;
		newRefItem2.trainerCarbs				= 0;
		newRefItem2.trainerFat					= 0;
		newRefItem2.trainerProtein				= 0;
		newRefItem2.trainerQuantity				= 1;
		newRefItem2.order						= 0;
		newRefItem2.templateId					= tmpItem.Id;
		newRefItem2.templateItemBlockId			= newBlockItem.Id;
		newRefItem2.templateItemName			= '?';
		newRefItem2.templateQuantity			= 0;
		newRefItem2.templateCalories			= 0;
		newRefItem2.templateCarbs				= 0;
		newRefItem2.templateFat					= 0;
		newRefItem2.templateProtein				= 0;
		newRefItem2.templateMeasurement			= 'Grams';
		newRefItem2.templateQuantityIncrement	= 1;
		newRefItem2.templateMinimumQuantity		= 0;
		nutRefPlan.blocks.get(0).items.add(newRefItem2);

		Test.startTest();
		AT_LWCC_PlannerNutritionCarbCyclePlan.UpdatePlan(nutRefPlan, new String[]{newBlock.Id}, new String[]{newItem2.Id});
		Test.stopTest();

	}
	
	private static AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock testTool_createNutritionReferenceBlock(String blockName, Decimal protein, Decimal carbs, Decimal fat, Decimal calories) {
		
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock newRefBlock = new AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock();
		newRefBlock.blockName			= blockName;
		newRefBlock.trainerProtein		= protein;
		newRefBlock.trainerCarbs		= carbs;
		newRefBlock.trainerFat			= fat;
		newRefBlock.trainerCalories		= calories;
		newRefBlock.items				= new List<AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem>();
		return newRefBlock;

	}

	private static AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem testTool_createNutritionReferenceItem(Decimal trainerQuantity, Integer order, String templateId) {
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem newRefItem = new AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem();
		newRefItem.trainerQuantity	= trainerQuantity;
		newRefItem.order			= order;
		newRefItem.templateId		= templateId;
		return newRefItem;
	}

	private static void assertTool_assertTemplateBlockWasCreatedFromReferenceBlock(AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock expected, Template_Block__c actual) {
		System.assertEquals(expected.blockName,			actual.Name);
		System.assertEquals(expected.trainerProtein,	actual.Template_Block_Protein__c);
		System.assertEquals(expected.trainerCarbs,		actual.Template_Block_Carbs__c);
		System.assertEquals(expected.trainerFat,		actual.Template_Block_Fat__c);
		System.assertEquals(expected.trainerCalories,	actual.Template_Block_Calories__c);
	}

	private static void assertTool_assertTemplateBlockItemWasCreatedFromReferenceItem(AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem expected, Template_Block_Item__c actual) {
		System.assertEquals(expected.trainerQuantity,	actual.Quantity__c);
		System.assertEquals(expected.order,				actual.Display_Order__c);
		System.assertEquals(expected.templateId,		actual.Template_Item__c);
	}

	@IsTest
	private static void SaveNutritionBlockAsTemplateBlock_SaveNutritionBlockWithOneItem() {

		Id nutTempItemRecordTypeId = testTool_getRecordType('Nutrition', 'Template_Item__c').Id;

		Template_Item__c tempItemOne = testTool_createTemplateItem('Template Item 1', 10, 20, 30, 40, 'Grams', 1, 1, nutTempItemRecordTypeId);
		
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock nutRefBlock = testTool_createNutritionReferenceBlock('Test Block', 30, 10, 20, 340);
		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceItem nutRefItemOne = testTool_createNutritionReferenceItem(1, 0, tempItemOne.Id);
		nutRefBlock.items.add(nutRefItemOne);

		Test.startTest();
		AT_LWCC_PlannerNutritionCarbCyclePlan.SaveNutritionBlockAsTemplateBlock(nutRefBlock);
		Test.stopTest();

		//Assert that the Template Block is created, and the Template Item is created
		Template_Block__c tmpBlock = [
			SELECT
				Id,
				Name,
				Template_Block_Protein__c,
				Template_Block_Carbs__c,
				Template_Block_Fat__c,
				Template_Block_Calories__c
			FROM Template_Block__c
		];
		assertTool_assertTemplateBlockWasCreatedFromReferenceBlock(nutRefBlock, tmpBlock);

		Template_Block_Item__c tmpBlkItem = [
			SELECT
				Id,
				Quantity__c,
				Display_Order__c,
				Template_Block__c,
				Template_Item__c
			FROM Template_Block_Item__c
		];

		assertTool_assertTemplateBlockItemWasCreatedFromReferenceItem(nutRefItemOne, tmpBlkItem);


	}

	@IsTest
	private static void SaveNutritionBlockAsTemplateBlock_SaveNutritionBlockWithNoItems() {
		
		Id nutTempItemRecordTypeId = testTool_getRecordType('Nutrition', 'Template_Item__c').Id;

		AT_LWCC_PlannerNutritionCarbCyclePlan.NutritionReferenceBlock nutRefBlock = testTool_createNutritionReferenceBlock('Test Block', 30, 10, 20, 340);

		Test.startTest();
		AT_LWCC_PlannerNutritionCarbCyclePlan.SaveNutritionBlockAsTemplateBlock(nutRefBlock);
		Test.stopTest();

		//Assert that the Template Block is created, and the Template Item is created
		Template_Block__c tmpBlock = [
			SELECT
				Id,
				Name,
				Template_Block_Protein__c,
				Template_Block_Carbs__c,
				Template_Block_Fat__c,
				Template_Block_Calories__c
			FROM Template_Block__c
		];
		assertTool_assertTemplateBlockWasCreatedFromReferenceBlock(nutRefBlock, tmpBlock);

	}

}