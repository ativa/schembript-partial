/**
* @author Harrison Campbell
* @description Tools for the CollaborationGroupMember Trigger
*/ 
public without sharing class CollaborationGroupMember_Trigger_Tools {

	/**
	* @description Get the Collaboration groups for group members
	* @param groupMembers A list of Group Members
	* @return Returns the Collaboration Groups mapped by the Collaboration Group Id
	*/ 
	private static Map<Id, CollaborationGroup> getCollaborationGroups(List<CollaborationGroupMember> groupMembers) {
		Set<Id> ids = new Set<Id>();
		for(CollaborationGroupMember collMember : groupMembers) {
			ids.add(collMember.CollaborationGroupId);
		}

		Map<Id, CollaborationGroup> collabGroups = new Map<Id, CollaborationGroup>([
			SELECT
				Id,
				Name
			FROM CollaborationGroup		
			WHERE Id IN :ids
		]);

		return collabGroups;
	}

	/**
	* @description Get the Group Settings for the collaboration groups
	* @param collabGroups The Collaboration groups
	* @return Returns the Group Settings mapped by the Group Setting Label
	*/
	private static Map<String, Group_Setting__mdt> getGroupSettings(List<CollaborationGroup> collabGroups) {

		Set<String> names = new Set<String>();
		for(CollaborationGroup collGroup : collabGroups) {
			names.add(collGroup.Name);
		}

		Map<Id, Group_Setting__mdt> groupSettings = new Map<Id, Group_Setting__mdt>([
			SELECT
				Id,
				Label,
				Notification_Frequency__c,
				Community_ID__c,
				User_IDs__c
			FROM Group_Setting__mdt
			WHERE Label IN :names
		]);

		Map<String, Group_Setting__mdt> groupSettingsByName = new Map<String, Group_Setting__mdt>();
		
		for(Group_Setting__mdt groupSetting : groupSettings.values()) {
			groupSettingsByName.put(groupSetting.Label, groupSetting);
		}

		return groupSettingsByName;

	}

	/**
	* @description Update the Notification frequencies for some Group members
	* @param groupMembers The List of group members
	*/
	private static void updateGroupMemberNotificationFrequencies(List<CollaborationGroupMember> groupMembers) {

		Map<Id, CollaborationGroup> collabGroups		= getCollaborationGroups(groupMembers);
		Map<String, Group_Setting__mdt> groupSettings	= getGroupSettings(collabGroups.values());

		for(CollaborationGroupMember gMember : groupMembers) {
			
			if(collabGroups.containsKey(gMember.CollaborationGroupId)) {
				
				CollaborationGroup collabGroup = collabGroups.get(gMember.CollaborationGroupId);
				
				if(groupSettings.containsKey(collabGroup.Name)) {
					Group_Setting__mdt groupSetting = groupSettings.get(collabGroup.Name);
					gMember.NotificationFrequency = groupSetting.Notification_Frequency__c;
					//If there are members to follow also make sure to follow them on chatter
					if( groupSetting.User_IDs__c != null) {

						String[] membersToFollow = groupSetting.User_IDs__c.split(';');
						for(String memberToFollow : membersToFollow) {
							ConnectApi.ChatterUsers.follow(groupSetting.Community_ID__c, gMember.MemberId, memberToFollow);
						}

					}

				}

			}

		}

	}

	/**
	* @description Handles the Before Insert Trigger
	* @param groupMembers The group members
	*/
	public static void beforeInsert(List<CollaborationGroupMember> groupMembers) {
		updateGroupMemberNotificationFrequencies(groupMembers);
	}

}