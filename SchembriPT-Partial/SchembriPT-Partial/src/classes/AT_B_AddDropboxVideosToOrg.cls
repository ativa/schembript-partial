global class AT_B_AddDropboxVideosToOrg implements Database.Batchable<SObject>, Database.AllowsCallouts {

    private string path;

    private string primaryCategory;

    private string secondaryCategory;


	global AT_B_AddDropboxVideosToOrg(String path, String primaryCategory, String secondaryCategory) {
		this.path               = path;
        this.primaryCategory    = primaryCategory;
        this.secondaryCategory  = secondaryCategory;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global List<SObject> start(Database.BatchableContext context) {
		return new List<SObject>();
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<SObject> scope) { }
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
		RecordType trainingVideoRType = [SELECT Id FROM RecordType WHERE SobjectType = 'Photo__c' AND DeveloperName = 'Trainer_Photos_Videos'];

		//Get all of the files in the path
		Dropbox_FilesAPI.ListFolderPacket getAllFiles = Dropbox_FilesAPI.ListFolder(path);

		List<Photo__c> newDropboxVideos = new List<Photo__c>();

		for(Dropbox_FilesAPI.Metadata mData : getAllFiles.entries) {

			Photo__c newVideo = new Photo__c();

			//Share the File
			Dropbox_SharingAPI.CreateSharedLinkWithSettingsPacket shareFile = Dropbox_SharingAPI.CreateSharedLinkWithSettings(mData.path_display);

			if(shareFile.result != null) {
				newVideo.Dropbox_Path__c = shareFile.result.url.removeEnd('dl=0') + 'raw=1';
			} else if(shareFile.error != null) {
				newVideo.Dropbox_Path__c = shareFile.error.error.shared_link_already_exists.metadata.url.removeEnd('dl=0') + 'raw=1';
			}
			newVideo.Reference__c			= mData.path_display;
			newVideo.RecordTypeId			= trainingVideoRType.Id;
			newVideo.Type__c				= 'Trainer Video';
			newVideo.Source__c				= 'Dropbox';

			newVideo.Display_Label__c		= mData.name.removeEnd('.mp4');
			newVideo.Primary_Category__c	= primaryCategory;
			newVideo.Secondary_Category__c	= secondaryCategory;
			newDropboxVideos.add(newVideo);

		}

		if(newDropboxVideos.size() > 0) {
			insert newDropboxVideos;
		}

		
	}
}