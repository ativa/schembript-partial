/**
* @author Harrison Campbell
* @description Tests the PlannerController Apex Class
*/ 
@isTest
public class PlannerController_Test  {

	private static Template_Plan__c testTool_createTemplatePlan(String tmpName) {
		
		Template_Plan__c newTmpPlan = new Template_Plan__c();
		newTmpPlan.Name = tmpName;
		insert newTmpPlan;

		return newTmpPlan;

	}

	private static Template_Block__c testTool_createTemplateBlock(String tmpBlkName, Template_Plan__c tmpPlan) {

		Template_Block__c newTmpBlk = new Template_Block__c();
		newTmpBlk.Name = tmpBlkName;
		insert newTmpBlk;

		Template_Plan_Block__c newTmpPlanBlock = new Template_Plan_Block__c();
		newTmpPlanBlock.Template_Plan__c	= tmpPlan.Id;
		newTmpPlanBlock.Template_Block__c	= newTmpBlk.Id;
		insert newTmpPlanBlock;

		return newTmpBlk;

	}

	private static Template_Item__c testTool_createTemplateItem(String tmpItmName, Integer quantity, Template_Block__c tmpBlock) {
		
		Template_Item__c newTmpItm	= new Template_Item__c();
		newTmpItm.Name				= tmpItmName;
		newTmpItm.Protein__c		= 0;
		newTmpItm.Carbs__c			= 0;
		newTmpItm.Fat__c			= 0;
		newTmpItm.Item_Quantity__c	= quantity;
		insert newTmpItm;

		Template_Block_Item__c newTmpBlkItm = new Template_Block_Item__c();
		newTmpBlkItm.Template_Block__c		= tmpBlock.Id;
		newTmpBlkItm.Template_Item__c		= newTmpItm.Id;
		newTmpBlkItm.Quantity__c			= quantity;
		insert newTmpBlkItm;

		return newTmpItm;

	}
	
	private static Account testTool_createPersonAccount() {
	
		Id pAccRecType = [SELECT Id FROM RecordType WHERE IsPersonType = true AND SobjectType = 'Account'].Id;

        Account acc	= new Account(
			RecordTypeID				= pAccRecType,
			FirstName					= 'Test',
			LastName					= 'Account',
			PersonMailingStreet			= 'test@yahoo.com',
			PersonMailingPostalCode		= '12345',
			PersonMailingCity			= 'SFO',
			PersonEmail					= 'test@yahoo.com',
			PersonHomePhone				= '1234567',
			PersonMobilePhone			= '12345678',
			SF_Community_User__pc		= UserInfo.getUserId()
        );

        insert acc;
		
		return acc;
	}

	private static User testTool_createPortalUser(Account personAccount) {
	
		Account acc = [
			SELECT
				Id,
				PersonContactId
			FROM Account
			WHERE Id = :personAccount.Id
		];

		Profile p = [SELECT Id FROM Profile WHERE Name='SPT Full Member']; 
        User u = new User(
			Alias = 'standt', 
			Email='aaaaaaa@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='aaaaaaaa@testorg.com'
		);
		u.ContactId = acc.PersonContactId;
		insert u;

		return u;
	}

	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact testTool_updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		//testContact.Email				= 'test@tst.com';
		//testContact.Phone				= '0489123345';
		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	private static Case testTool_createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		
		//Create a Product for the case
		Programs_Products__c prod			= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 12;
		prod.Check_In_Frequency__c	= '1 Week';
		prod.Name					= 'Body Revolt'; //I need to do this for the process builder
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;


		return testProgram;
	}

	private static void testTool_createCheckIns(Integer checkInCount) {
	
		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Session'].Id;

		List<Plan__c> newPlans = new List<Plan__c>();

		for(Integer i = 0; i < checkInCount; i++) {
		
			Check_In__c check = [
				SELECT 
					Id, 
					Name, 
					Program__c,
					Scheduled_Check_In_Date__c,
					Check_In_Number__c, 
					Status__c,
					Thigh_Measurement__c,
					Waist_Measurement__c,
					Chest_Measurement__c,
					Hip_Measurement__c,
					Weight__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :i
			];

			check.Scheduled_Check_In_Date__c = Date.today().addDays(i);

			check.Thigh_Measurement__c	= 33 + (i+1);
			check.Waist_Measurement__c	= 45 + (i+1);
			check.Chest_Measurement__c	= 56 + (i+1);
			check.Hip_Measurement__c	= 50 + (i+1);
			check.Weight__c				= 75 + (i+1);
			
			/*
			Plan__c newPlan				= new Plan__c();
			newPlan.RecordTypeId		= planSessionRecId;
			newPlan.Name				= 'Plan ' + (i+1);
			newPlan.Check_In__c			= check.Id;
			newPlan.Actual_Calories__c	= (i+1) * 13;
			newPlan.Order__c			= i;
			newPlans.add(newPlan);
			*/

			if(i < checkInCount - 1) {
				check.Stage__c = 'Completed';
			}
			update check;
		}
		/*
		insert newPlans;
		*/


	}
	
	private static Plan__c testTool_createReferencePlan(Id checkInId, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, String recType) {

		Plan__c newPlan						= new Plan__c();
		newPlan.Name						= 'Test Plan';
		newPlan.Check_In__c					= checkInId;
		newPlan.Trainer_Plan_Protein__c		= protein;
		newPlan.Trainer_Plan_Carbs__c		= carbs;
		newPlan.Trainer_Plan_Fat__c			= fat;
		newPlan.Trainer_Plan_Calories__c	= calories;
		newPlan.RecordTypeId				= Schema.SObjectType.Plan__c.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
		insert newPlan;
		return newPlan;
	}

	private static Block__c testTool_createReferenceBlock(Id planId, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, String recType, Decimal order) {
		Block__c newBlock					= new Block__c();
		newBlock.Name						= 'Test Block';
		newBlock.Plan__c					= planId;
		newBlock.Trainer_Block_Protein__c	= protein;
		newBlock.Trainer_Block_Carbs__c		= carbs;
		newBlock.Trainer_Block_Fat__c		= fat;
		newBlock.Trainer_Block_Calories__c	= calories;
		newBlock.RecordTypeId				= Schema.SObjectType.Block__c.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
		newBlock.Display_Order__c			= order;
		insert newBlock;
		return newBlock;
	}

	private static Item__c testTool_createReferenceItem(Id blockId, Decimal quantity, String measurement, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, String recType, Decimal order) {
		Item__c newItem						= new Item__c();
		newItem.Name						= 'Test Item';
		newItem.Block__c					= blockId;
		newItem.Trainer_Quantity__c			= quantity;
		newItem.Template_Measurement__c		= measurement;
		newItem.Trainer_Protein__c			= protein;
		newItem.Trainer_Carbs__c			= carbs;
		newItem.Trainer_Fat__c				= fat;
		newItem.Trainer_Calories__c			= calories;
		newItem.RecordTypeId				= Schema.SObjectType.Item__c.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
		newItem.Display_Order__c			= order;
		insert newItem;
		return newItem;
	}

	@IsTest
	private static void QueryCheckInPlans_FunctionalTest() {
		
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);

		Check_In__c checkIn					= new Check_In__c();
		checkIn.Check_In_Number_v2__c		= 0;
		checkIn.Stage__c					= 'Pending';
		checkIn.Scheduled_Check_In_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c			= personCase.Id;
		checkIn.Contact__c					= personContact.Id;
		insert checkIn;

		Template_Plan__c tmpPlan	= testTool_createTemplatePlan('Test Plan');
		Template_Block__c tmpBlock	= testTool_createTemplateBlock('Test Block', tmpPlan);
		Template_Item__c tmpItem	= testTool_createTemplateItem('Test Item', 3, tmpBlock);

		AT_LWCC_PlannerAddTemplatePlan.SaveAsReferencePlan(checkIn.Id, 'New Reference Plan', tmpPlan.Id);

		Test.startTest();
		PlannerController.QueryCheckInPlans(checkIn.Id, 'Reference');
		Test.stopTest();

	}
	
	@IsTest
	private static void AssignDaysToReferencePlans_FunctionalTest() {
		
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);
		
		Template_Plan__c tmpPlan	= testTool_createTemplatePlan('Test Plan');
		Template_Block__c tmpBlock	= testTool_createTemplateBlock('Test Block', tmpPlan);
		Template_Item__c tmpItem	= testTool_createTemplateItem('Test Item', 3, tmpBlock);

		Check_In__c checkIn						= new Check_In__c();
		checkIn.Check_In_Number_v2__c			= 0;
		checkIn.Nutrition_Plan_Start_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Stage__c						= 'Pending';
		checkIn.Scheduled_Check_In_Date__c		= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c				= personCase.Id;
		checkIn.Contact__c						= personContact.Id;
		insert checkIn;
		
		Id refPlanId = AT_LWCC_PlannerAddTemplatePlan.SaveAsReferencePlan(checkIn.Id, 'New Reference Plan', tmpPlan.Id);

		List<PlannerController.DayPlan> dPlans = new List<PlannerController.DayPlan>();
		PlannerController.DayPlan dPlan = new PlannerController.DayPlan();
		dPlan.weekday = 'Monday';
		dPlan.planID = refPlanId;
		dPlans.add(dPlan);

		Test.startTest();
		PlannerController.PlanAssignment pAssignment = PlannerController.AssignDaysToReferencePlans(checkIn.Id, JSON.serialize(dPlans));
		Test.stopTest();

	}


	@IsTest
	private static void CreateSessionPlanFromReferencePlan_FunctionalTest() {
		
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);

		Check_In__c checkIn					= new Check_In__c();
		checkIn.Check_In_Number_v2__c		= 0;
		checkIn.Stage__c					= 'Pending';
		checkIn.Scheduled_Check_In_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c			= personCase.Id;
		checkIn.Contact__c					= personContact.Id;
		insert checkIn;

		Template_Plan__c tmpPlan	= testTool_createTemplatePlan('Test Plan');
		Template_Block__c tmpBlock	= testTool_createTemplateBlock('Test Block', tmpPlan);
		Template_Item__c tmpItem	= testTool_createTemplateItem('Test Item', 3, tmpBlock);

		Id newRefPlanId = AT_LWCC_PlannerAddTemplatePlan.SaveAsReferencePlan(checkIn.Id, 'New Reference Plan', tmpPlan.Id);

		Test.startTest();
		PlannerController.CreateSessionPlanFromReferencePlan(newRefPlanId, Date.newInstance(2020, 1, 1));
		Test.stopTest();

	}

	@IsTest
	private static void AssignDayToSessionPlan_FunctionalTest() {
	
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);
		
		Template_Plan__c tmpPlan	= testTool_createTemplatePlan('Test Plan');
		Template_Block__c tmpBlock	= testTool_createTemplateBlock('Test Block', tmpPlan);
		Template_Item__c tmpItem	= testTool_createTemplateItem('Test Item', 3, tmpBlock);

		Check_In__c checkIn						= new Check_In__c();
		checkIn.Check_In_Number_v2__c			= 0;
		checkIn.Nutrition_Plan_Start_Date__c	= Date.newInstance(2020, 1, 1);
		checkIn.Stage__c						= 'Pending';
		checkIn.Scheduled_Check_In_Date__c		= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c				= personCase.Id;
		checkIn.Contact__c						= personContact.Id;
		insert checkIn;
		
		Id refPlanId = AT_LWCC_PlannerAddTemplatePlan.SaveAsReferencePlan(checkIn.Id, 'New Reference Plan', tmpPlan.Id);

		List<PlannerController.DayPlan> dPlans = new List<PlannerController.DayPlan>();
		PlannerController.DayPlan dPlan = new PlannerController.DayPlan();
		dPlan.weekday = 'Wednesday';
		dPlan.planID = refPlanId;
		dPlans.add(dPlan);

		Test.startTest();
		PlannerController.AssignDayToSessionPlan(checkIn.Id, '{"' + dPlan.weekday + '" : "' + dPlan.planID + '"}', '2020-01-01');
		Test.stopTest();

	}

	@IsTest
	private static void CreateSessionPlansFromCheckIn_FunctionalTest() {
		
		Account personAccount	= testTool_createPersonAccount();
		User testUser			= testTool_createPortalUser(personAccount);
		Contact personContact	= testTool_updateTestContact(personAccount);
		Case personCase			= testTool_createTestCase(personAccount, personContact);

		Check_In__c checkIn						= new Check_In__c();
		checkIn.Check_In_Number_v2__c			= 0;
		checkIn.Stage__c						= 'Pending';
		checkIn.Scheduled_Check_In_Date__c		= Date.newInstance(2020, 1, 1);
		checkIn.Client_Program__c				= personCase.Id;
		checkIn.Contact__c						= personContact.Id;
		checkIn.Nutrition_Plan_Start_Date__c 	= Date.newInstance(2020, 1, 1);
		checkIn.Nutrition_Plan_End_Date__c 		= Date.newInstance(2020, 1, 8);
		insert checkIn;

		Template_Plan__c tmpPlan	= testTool_createTemplatePlan('Test Plan');
		Template_Block__c tmpBlock	= testTool_createTemplateBlock('Test Block', tmpPlan);
		Template_Item__c tmpItem	= testTool_createTemplateItem('Test Item', 3, tmpBlock);

		Id newRefPlanId = AT_LWCC_PlannerAddTemplatePlan.SaveAsReferencePlan(checkIn.Id, 'New Reference Plan', tmpPlan.Id);

		Plan__c refPlan = [
			SELECT
				Id
			FROM Plan__c
			WHERE Id = :newRefPlanId
		];

		refPlan.Weekdays__c = 'Monday;Tuesday;Wednesday;Thursday;Friday;Saturday;Sunday';
		update refPlan;

		Test.startTest();
		PlannerController.CreateSessionPlansFromCheckIn(checkIn.Id);
		Test.stopTest();

	}

	private static void assertTool_assertReferencePlanDataIsCorrect(Plan__c refPlan, PlannerController.ReferencePlan refPlanData) {
		System.assertEquals(refPlan.Id,							refPlanData.planId);
		System.assertEquals(refPlan.Name,						refPlanData.planName);
		System.assertEquals(refPlan.Trainer_Plan_Protein__c,	refPlanData.planProtein);
		System.assertEquals(refPlan.Trainer_Plan_Carbs__c,		refPlanData.planCarbs);
		System.assertEquals(refPlan.Trainer_Plan_Fat__c,		refPlanData.planFat);
		System.assertEquals(refPlan.Trainer_Plan_Calories__c,	refPlanData.planCalories);
	}

	private static void assertTool_assertReferenceBlockDataIsCorrect(Block__c refBlock, PlannerController.ReferenceBlock refBlockData) {
		System.assertEquals(refBlock.Id,						refBlockData.blockId);
		System.assertEquals(refBlock.Name,						refBlockData.blockName);
		System.assertEquals(refBlock.Block_Short_Name__c,		refBlockData.blockShortName);
		System.assertEquals(refBlock.Trainer_Block_Protein__c,	refBlockData.blockProtein);
		System.assertEquals(refBlock.Trainer_Block_Carbs__c,	refBlockData.blockCarbs);
		System.assertEquals(refBlock.Trainer_Block_Fat__c,		refBlockData.blockFat);
		System.assertEquals(refBlock.Trainer_Block_Calories__c,	refBlockData.blockCalories);
	}

	private static void assertTool_assertReferenceItemDataIsCorrect(Item__c refItem, PlannerController.ReferenceItem refItemData) {
		System.assertEquals(refItem.Id,							refItemData.itemId);
		System.assertEquals(refItem.Name,						refItemData.itemName);
		System.assertEquals(refItem.Trainer_Quantity__c,		refItemData.itemQuantity);
		System.assertEquals(refItem.Template_Measurement__c,	refItemData.itemMeasurement);
		System.assertEquals(refItem.Trainer_Protein__c,			refItemData.itemProtein);
		System.assertEquals(refItem.Trainer_Carbs__c,			refItemData.itemCarbs);
		System.assertEquals(refItem.Trainer_Fat__c,				refItemData.itemFat);
		System.assertEquals(refItem.Trainer_Calories__c,		refItemData.itemCalories);
	}

}