public without sharing class AT_LWCC_PlannerCloneReferencePlan {

	@AuraEnabled
	public static Map<String, String> QueryPlanData(String planId) {

		Plan__c plan = [
			SELECT
				Id,
				Name
			FROM Plan__c
			WHERE Id = :planId
		];

		Map<String, String> planData = new Map<String, String>();
		planData.put('PlanId',		plan.Id);
		planData.put('PlanName',	plan.Name);
		return planData;

	}

	/**
	* @description Clone the Plan
	* @param planId The Id of the plan to clone
	* @param newPlanName The new plan name
	*/ 
	@AuraEnabled
	public static void ClonePlan(String planId, String newPlanName) {

		Plan__c plan = [
			SELECT
				Id,
				Check_In__c,
				RecordTypeId,
				Name,
				Template_Plan__c,
				Weekdays__c,
				Account_nr__c,
				Account_ns__c,
				

				Client_Plan_Carbs__c,
				Client_Plan_Fat__c,
				Client_Plan_Protein__c,
				Client_Plan_Calories__c,

				Trainer_Plan_Carbs__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Protein__c,
				Trainer_Plan_Calories__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Protein__c,
				Template_Plan_Calories__c,
				Account__c,
				Reference_Start_Date__c,
				Reference_End_Date__c
			FROM Plan__c
			WHERE Id = :planId
		];

		List<Plan__c> allCheckInPlans = [
			SELECT 
				Id,
				Order__c
			FROM Plan__c
			WHERE Check_In__c = :plan.Check_In__c
			ORDER By Order__c DESC NULLS LAST
		];

		Plan__c clonedPlan						= new Plan__c();
		//clonedPlan.Order__c						= allCheckInPlans.get(0).Order__c + 1;
		clonedPlan.RecordTypeId					= plan.RecordTypeId;
		clonedPlan.Check_In__c					= plan.Check_In__c;
		clonedPlan.Name							= newPlanName;
		
		clonedPlan.Client_Plan_Protein__c		= plan.Client_Plan_Protein__c;
		clonedPlan.Client_Plan_Carbs__c			= plan.Client_Plan_Carbs__c;
		clonedPlan.Client_Plan_Fat__c			= plan.Client_Plan_Fat__c;
		clonedPlan.Client_Plan_Calories__c		= plan.Client_Plan_Calories__c;

		//clonedPlan.Cloned_Plan__c				= plan.Id;
		clonedPlan.Account__c					= plan.Account__c;
		clonedPlan.Account_nr__c				= plan.Account_nr__c;
		clonedPlan.Account_ns__c				= plan.Account_ns__c;
		clonedPlan.Check_In__c					= plan.Check_In__c;
		clonedPlan.Check_In_nr__c				= plan.Check_In__c;
		clonedPlan.Reference_Start_Date__c		= plan.Reference_Start_Date__c;
		clonedPlan.Reference_End_Date__c		= plan.Reference_End_Date__c;
		clonedPlan.Template_Plan__c				= plan.Template_Plan__c;
		clonedPlan.Template_Plan_Carbs__c		= plan.Template_Plan_Carbs__c;
		clonedPlan.Template_Plan_Fat__c			= plan.Template_Plan_Fat__c;
		clonedPlan.Template_Plan_Protein__c		= plan.Template_Plan_Protein__c;
		clonedPlan.Template_Plan_Calories__c	= plan.Template_Plan_Calories__c;
		clonedPlan.Trainer_Plan_Carbs__c		= plan.Trainer_Plan_Carbs__c;
		clonedPlan.Trainer_Plan_Fat__c			= plan.Trainer_Plan_Fat__c;
		clonedPlan.Trainer_Plan_Protein__c		= plan.Trainer_Plan_Protein__c;
		clonedPlan.Trainer_Plan_Calories__c		= plan.Trainer_Plan_Calories__c;

		insert clonedPlan;

		Map<Id, Block__c> blocks = new Map<Id, Block__c>([
			SELECT
				Id,
				Name,
				RecordTypeId,
				Cloned_Block__c,
				Display_Order__c,
				Block_Short_Name__c,
				Template_Plan_Block__c,
				Template_Block_v2__c,
				Template_Block_Carbs__c,
				Template_Block_Fat__c,
				Template_Block_Protein__c,
				Template_Block_Calories__c,
								
				Client_Block_Carbs__c,
				Client_Block_Fat__c,
				Client_Block_Protein__c,
				Client_Block_Calories__c,

				Trainer_Block_Carbs__c,
				Trainer_Block_Fat__c,
				Trainer_Block_Protein__c,
				Trainer_Block_Calories__c
			FROM Block__c
			WHERE Plan__c = :plan.Id
			ORDER BY Display_Order__c ASC
		]);

		Map<Id, Block__c> clonedBlocks = new Map<Id, Block__c>();
		for(Block__c b : blocks.values()) {
			Block__c clonedBlock					= new Block__c();
			clonedBlock.RecordTypeId				= b.RecordTypeId;
			clonedBlock.Plan__c						= clonedPlan.Id;
			clonedBlock.Name						= b.Name;
			clonedBlock.Cloned_Block__c				= b.Id;
			clonedBlock.Display_Order__c			= b.Display_Order__c;
			clonedBlock.Block_Short_Name__c			= b.Block_Short_Name__c;
			clonedBlock.Template_Block_v2__c		= b.Template_Block_v2__c;
			clonedBlock.Template_Plan_Block__c		= b.Template_Plan_Block__c;
			clonedBlock.Template_Block_Carbs__c		= b.Template_Block_Carbs__c;
			clonedBlock.Template_Block_Fat__c		= b.Template_Block_Fat__c;
			clonedBlock.Template_Block_Protein__c	= b.Template_Block_Protein__c;
			clonedBlock.Template_Block_Calories__c	= b.Template_Block_Calories__c;
			clonedBlock.Trainer_Block_Carbs__c		= b.Trainer_Block_Carbs__c;
			clonedBlock.Trainer_Block_Fat__c		= b.Trainer_Block_Fat__c;
			clonedBlock.Trainer_Block_Protein__c	= b.Trainer_Block_Protein__c;
			clonedBlock.Trainer_Block_Calories__c	= b.Trainer_Block_Calories__c;
			
			clonedBlock.Client_Block_Carbs__c		= b.Client_Block_Carbs__c;
			clonedBlock.Client_Block_Fat__c			= b.Client_Block_Fat__c;
			clonedBlock.Client_Block_Protein__c		= b.Client_Block_Protein__c;
			clonedBlock.Client_Block_Calories__c	= b.Client_Block_Calories__c;

			clonedBlocks.put(b.Id, clonedBlock);
		}
		insert clonedBlocks.values();

		Map<Id, Item__c> items = new Map<Id, Item__c>([
			SELECT 
				Id,
				Name,
				Block__c,
				RecordTypeId,
				Display_Order__c,
				//Trainer Values
				Trainer_Quantity__c,
				Trainer_Protein__c,
				Trainer_Carbs__c,
				Trainer_Fat__c,
				Trainer_Calories__c,
				//Client Values
				Client_Quantity__c,
				Client_Protein__c,
				Client_Carbs__c,
				Client_Fat__c,
				Client_Calories__c,
				//Template Values
				Template_Item__c, //HACK: WILL REMOVE LATER
				Template_Item_Id__c,
				Template_Quantity__c,
				Template_Item_Name__c,
				Template_Measurement__c,
				Template_Protein__c,
				Template_Carbs__c,
				Template_Fat__c,
				Template_Calories__c,
				Template_Minimum_Quantity__c,
				Template_Quantity_Increment__c
			FROM Item__c
			WHERE Block__c IN :clonedBlocks.keySet()
			ORDER BY Display_Order__c ASC
		]);

		Map<Id, Item__c> clonedItems = new Map<Id, Item__c>();

		for(Item__c i : items.values()) {
			Item__c clonedItem							= new Item__c();
			clonedItem.Display_Order__c					= i.Display_Order__c;
			clonedItem.RecordTypeId						= i.RecordTypeId;
			clonedItem.Block__c							= clonedBlocks.get(i.Block__c).Id;
			clonedItem.Name								= i.Name;
			clonedItem.Cloned_Item__c					= i.Id;

			clonedItem.Client_Quantity__c				= i.Client_Quantity__c;
			clonedItem.Client_Protein__c				= i.Client_Protein__c;
			clonedItem.Client_Carbs__c					= i.Client_Carbs__c;
			clonedItem.Client_Fat__c					= i.Client_Fat__c;
			clonedItem.Client_Calories__c				= i.Client_Calories__c;

			clonedItem.Trainer_Quantity__c				= i.Trainer_Quantity__c;
			clonedItem.Trainer_Protein__c				= i.Trainer_Protein__c;
			clonedItem.Trainer_Carbs__c					= i.Trainer_Carbs__c;
			clonedItem.Trainer_Fat__c					= i.Trainer_Fat__c;
			clonedItem.Trainer_Calories__c				= i.Trainer_Calories__c;

			clonedItem.Template_Item_Id__c				= i.Template_Item_Id__c;
			clonedItem.Template_Quantity__c				= i.Template_Quantity__c;
			clonedItem.Template_Item_Name__c			= i.Template_Item_Name__c;
			clonedItem.Template_Measurement__c			= i.Template_Measurement__c;
			clonedItem.Template_Protein__c				= i.Template_Protein__c;
			clonedItem.Template_Carbs__c				= i.Template_Carbs__c;
			clonedItem.Template_Fat__c					= i.Template_Fat__c;
			clonedItem.Template_Calories__c				= i.Template_Calories__c;
			clonedItem.Template_Minimum_Quantity__c		= i.Template_Minimum_Quantity__c;
			clonedItem.Template_Quantity_Increment__c	= i.Template_Quantity_Increment__c;

			clonedItems.put(i.Id, clonedItem);
		}
		insert clonedItems.values();

	}

}