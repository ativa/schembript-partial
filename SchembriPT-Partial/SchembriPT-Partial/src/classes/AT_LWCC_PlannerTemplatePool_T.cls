@isTest 
private class AT_LWCC_PlannerTemplatePool_T {

    /**
     * @description Create a Test Check-In 
     */
	private static Check_In__c testTool_createCheckIn(Contact cont) {
		Check_In__c newCheckIn 		= new Check_In__c();
        newCheckIn.Contact__c 		= cont.Id;
		newCheckIn.h_Test_Record__c = true;
		insert newCheckIn;
		return newCheckIn;
	}
    
    /**
     * @description Create a Test Contact
     */
    private static Contact testTool_createContact() {
        Contact testContact = new Contact();
        testContact.LastName = 'Test Contact';
        insert testContact;
        return testContact;
    }

	private static Template_Plan__c testTool_createTemplatePlan(String name, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, Id recordTypeId) {
		Template_Plan__c newTemplatePlan			= new Template_Plan__c();
		newTemplatePlan.Status__c					= 'Public';
		newTemplatePlan.RecordTypeId				= recordTypeId;
		newTemplatePlan.Name						= name;
		newTemplatePlan.Template_Plan_Protein__c	= protein;
		newTemplatePlan.Template_Plan_Carbs__c		= carbs;
		newTemplatePlan.Template_Plan_Fat__c		= fat;
		newTemplatePlan.Template_Plan_Calories__c	= calories;
		insert newTemplatePlan;
		return newTemplatePlan;
	}
    
	private static Pool_Plan_Version__c testTool_createTemplatePoolItem(Check_In__c checkIn, Template_Plan__c templatePlan, String templatePoolType, Id recordTypeId) {
		Pool_Plan_Version__c newTemplatePoolItem	= new Pool_Plan_Version__c();
		newTemplatePoolItem.RecordTypeId			= recordTypeId;
		newTemplatePoolItem.Check_In__c				= checkIn.Id;
		newTemplatePoolItem.Template_Plan__c		= templatePlan.Id;
		newTemplatePoolItem.Template_Pool_Type__c	= templatePoolType;
		insert newTemplatePoolItem;
		return newTemplatePoolItem;
	}
    
	private static void assertTool_TemplatePlanDataIsCorrect(Template_Plan__c expected, AT_LWCC_PlannerTemplatePool.TemplatePlan actual) {
		System.assertEquals(expected.Id,						actual.id);
		System.assertEquals(expected.Name,						actual.name);
		System.assertEquals(expected.Template_Plan_Protein__c,	actual.protein);
		System.assertEquals(expected.Template_Plan_Carbs__c,	actual.carbs);
		System.assertEquals(expected.Template_Plan_Fat__c,		actual.fat);
		System.assertEquals(expected.Template_Plan_Calories__c,	actual.calories);
	}

	@isTest
	private static void GetAllNutritionTemplatePlans_GetPlans() {

		RecordType rType = [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, rType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, rType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, rType.Id);

		Test.startTest();
		List<AT_LWCC_PlannerTemplatePool.TemplatePlan> templatePlans = AT_LWCC_PlannerTemplatePool.GetAllNutritionTemplatePlans();
		Test.stopTest();

		System.assertEquals(3, templatePlans.size());
		assertTool_TemplatePlanDataIsCorrect(templatePlanOne,	templatePlans.get(0));
		assertTool_TemplatePlanDataIsCorrect(templatePlanTwo,	templatePlans.get(1));
		assertTool_TemplatePlanDataIsCorrect(templatePlanThree, templatePlans.get(2));

	}

	@isTest
	private static void GetAllNutritionTemplatePlans_GetNoPlans() {

		Test.startTest();
		List<AT_LWCC_PlannerTemplatePool.TemplatePlan> templatePlans = AT_LWCC_PlannerTemplatePool.GetAllNutritionTemplatePlans();
		Test.stopTest();

		System.assertEquals(0, templatePlans.size());

	}

	@IsTest
	private static void GetSelectedTemplates_GetTemplatePoolPlans() {
	
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];
		RecordType templatePoolItemRType	= [SELECT Id FROM RecordType WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Nutrition_Trainer_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Pool_Plan_Version__c templatePoolItemOne	= testTool_createTemplatePoolItem(testCheckIn, templatePlanOne,	'Selected Templates', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemTwo	= testTool_createTemplatePoolItem(testCheckIn, templatePlanTwo,	'Selected Templates', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemThree = testTool_createTemplatePoolItem(testCheckIn, templatePlanThree,	'Selected Templates', templatePoolItemRType.Id);

		Test.startTest();
		List<AT_LWCC_PlannerTemplatePool.TemplatePlan> selectedTemplates = AT_LWCC_PlannerTemplatePool.GetSelectedTemplates(testCheckIn.Id);
		Test.stopTest();
		
		System.assertEquals(3,									selectedTemplates.size());
		assertTool_TemplatePlanDataIsCorrect(templatePlanOne,	selectedTemplates.get(0));
		assertTool_TemplatePlanDataIsCorrect(templatePlanTwo,	selectedTemplates.get(1));
		assertTool_TemplatePlanDataIsCorrect(templatePlanThree, selectedTemplates.get(2));

	}

	@IsTest
	private static void SaveTemplatePools_SaveCalorieRangeAllNewTemplatePools() {
		
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Calorie Range', new String[] { templatePlanOne.Id, templatePlanTwo.Id, templatePlanThree.Id });
		Test.stopTest();

		//Assert That the Calorie Range Template Pool Items are created
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Calorie Range' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];

		System.assertEquals(3, calorieRangePoolItems.size());
		System.assertEquals(templatePlanOne.Id,		calorieRangePoolItems.get(0).Template_Plan__c);
		System.assertEquals(templatePlanTwo.Id,		calorieRangePoolItems.get(1).Template_Plan__c);
		System.assertEquals(templatePlanThree.Id,	calorieRangePoolItems.get(2).Template_Plan__c);

	}

	@IsTest
	private static void SaveTemplatePools_SaveCalorieRangeAllOldTemplatePools() {
		
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];
		RecordType templatePoolItemRType	= [SELECT Id FROM RecordType WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Nutrition_Trainer_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Pool_Plan_Version__c templatePoolItemOne	= testTool_createTemplatePoolItem(testCheckIn, templatePlanOne,	'Calorie Range', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemTwo	= testTool_createTemplatePoolItem(testCheckIn, templatePlanTwo,	'Calorie Range', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemThree = testTool_createTemplatePoolItem(testCheckIn, templatePlanThree,	'Calorie Range', templatePoolItemRType.Id);
		
		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Calorie Range', new String[] { templatePlanOne.Id, templatePlanTwo.Id, templatePlanThree.Id });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Calorie Range' AND RecordType.DeveloperName = 'Template_Nutrition_Pool_Item'
		];
		
		System.assertEquals(3, calorieRangePoolItems.size());
		System.assertEquals(templatePoolItemOne.Id,		calorieRangePoolItems.get(0).Id);
		System.assertEquals(templatePoolItemTwo.Id,		calorieRangePoolItems.get(1).Id);
		System.assertEquals(templatePoolItemThree.Id,	calorieRangePoolItems.get(2).Id);

	}

	@IsTest
	private static void SaveTemplatePools_SaveCalorieRangeRemoveOldTemplatePoolItems() {
	
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];
		RecordType templatePoolItemRType	= [SELECT Id FROM RecordType WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Nutrition_Trainer_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Pool_Plan_Version__c templatePoolItemOne	= testTool_createTemplatePoolItem(testCheckIn, templatePlanOne,	'Calorie Range', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemTwo	= testTool_createTemplatePoolItem(testCheckIn, templatePlanTwo,	'Calorie Range', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemThree	= testTool_createTemplatePoolItem(testCheckIn, templatePlanThree,	'Calorie Range', templatePoolItemRType.Id);
		
		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Calorie Range', new String[] { });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Calorie Range' AND RecordType.DeveloperName = 'Template_Nutrition_Pool_Item'
		];
		
		System.assertEquals(0, calorieRangePoolItems.size());

	}


	@IsTest
	private static void SaveTemplatePools_SaveCalorieRangeSomeOldSomeNewTemplatePools() {
		
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];
		RecordType templatePoolItemRType	= [SELECT Id FROM RecordType WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Nutrition_Trainer_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Pool_Plan_Version__c templatePoolItemOne	= testTool_createTemplatePoolItem(testCheckIn, templatePlanOne,	'Calorie Range', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemTwo	= testTool_createTemplatePoolItem(testCheckIn, templatePlanTwo,	'Calorie Range', templatePoolItemRType.Id);
		
		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Calorie Range', new String[] { templatePlanOne.Id, templatePlanTwo.Id, templatePlanThree.Id });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Calorie Range' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];
		
		System.assertEquals(3, calorieRangePoolItems.size());
		//Assert the first 2 are the same
		System.assertEquals(templatePoolItemOne.Id,		calorieRangePoolItems.get(0).Id);
		System.assertEquals(templatePoolItemTwo.Id,		calorieRangePoolItems.get(1).Id);
		//Asser the last 1 is new
		System.assertEquals(templatePlanThree.Id,	calorieRangePoolItems.get(2).Template_Plan__c);

	}
	
	@IsTest
	private static void SaveTemplatePools_SaveCalorieRangeZeroTemplatePools() {
		
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Calorie Range', new String[] { });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Calorie Range' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];
		
		System.assertEquals(0, calorieRangePoolItems.size());

	}
	
	@IsTest
	private static void SaveTemplatePools_SaveSelectedTemplatesTemplatePools() {
		
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Selected Templates', new String[] { templatePlanOne.Id, templatePlanTwo.Id, templatePlanThree.Id });
		Test.stopTest();

		//Assert That the Calorie Range Template Pool Items are created
		List<Pool_Plan_Version__c> selectedTemplatePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Selected Templates' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];

		System.assertEquals(3, selectedTemplatePoolItems.size());
		System.assertEquals(templatePlanOne.Id,		selectedTemplatePoolItems.get(0).Template_Plan__c);
		System.assertEquals(templatePlanTwo.Id,		selectedTemplatePoolItems.get(1).Template_Plan__c);
		System.assertEquals(templatePlanThree.Id,	selectedTemplatePoolItems.get(2).Template_Plan__c);

	}

	
	@IsTest
	private static void SaveTemplatePools_SaveSelectedTemplatesRemoveOldTemplatePoolItems() {
	
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];
		RecordType templatePoolItemRType	= [SELECT Id FROM RecordType WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Nutrition_Trainer_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Pool_Plan_Version__c templatePoolItemOne	= testTool_createTemplatePoolItem(testCheckIn, templatePlanOne,	'Selected Templates', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemTwo	= testTool_createTemplatePoolItem(testCheckIn, templatePlanTwo,	'Selected Templates', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemThree = testTool_createTemplatePoolItem(testCheckIn, templatePlanThree,	'Selected Templates', templatePoolItemRType.Id);
		
		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Selected Templates', new String[] { });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Selected Templates' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];
		
		System.assertEquals(0, calorieRangePoolItems.size());

	}

	
	@IsTest
	private static void SaveTemplatePools_SaveSelectedTemplatesSomeOldSomeNewTemplatePools() {
		
		RecordType templateItemRType		= [SELECT Id FROM RecordType WHERE SobjectType = 'Template_Plan__c' AND DeveloperName = 'Template_Nutrition_Plan'];
		RecordType templatePoolItemRType	= [SELECT Id FROM RecordType WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Nutrition_Trainer_Plan'];

        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Template_Plan__c templatePlanOne	= testTool_createTemplatePlan('Template Plan 1', 100, 200, 300, 400, templateItemRType.Id);
		Template_Plan__c templatePlanTwo	= testTool_createTemplatePlan('Template Plan 2', 200, 300, 400, 500, templateItemRType.Id);
		Template_Plan__c templatePlanThree	= testTool_createTemplatePlan('Template Plan 3', 300, 400, 500, 600, templateItemRType.Id);

		Pool_Plan_Version__c templatePoolItemOne	= testTool_createTemplatePoolItem(testCheckIn, templatePlanOne,	'Selected Templates', templatePoolItemRType.Id);
		Pool_Plan_Version__c templatePoolItemTwo	= testTool_createTemplatePoolItem(testCheckIn, templatePlanTwo,	'Selected Templates', templatePoolItemRType.Id);
		
		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Selected Templates', new String[] { templatePlanOne.Id, templatePlanTwo.Id, templatePlanThree.Id });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> selectedTemplatesPoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Selected Templates' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];
		
		System.assertEquals(3, selectedTemplatesPoolItems.size());
		//Assert the first 2 are the same
		System.assertEquals(templatePoolItemOne.Id,		selectedTemplatesPoolItems.get(0).Id);
		System.assertEquals(templatePoolItemTwo.Id,		selectedTemplatesPoolItems.get(1).Id);
		//Asser the last 1 is new
		System.assertEquals(templatePlanThree.Id,	selectedTemplatesPoolItems.get(2).Template_Plan__c);

	}
	
	@IsTest
	private static void SaveTemplatePools_SaveSelectedTemplatesZeroTemplatePools() {
		
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		Test.startTest();
		AT_LWCC_PlannerTemplatePool.SaveTemplatePools(testCheckIn.Id, 'Selected Templates', new String[] { });
		Test.stopTest();
		
		//Assert That the Calorie Range Template Pool Items are the same
		List<Pool_Plan_Version__c> calorieRangePoolItems = [
			SELECT 
				Id,
				Template_Plan__c,
				Check_In__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :testCheckIn.Id AND Template_Pool_Type__c = 'Selected Templates' AND RecordType.DeveloperName = 'Nutrition_Trainer_Plan'
		];
		
		System.assertEquals(0, calorieRangePoolItems.size());

	}
    
}