/**
* @author Harrison Campbell
* @description The Controller for the 'tracker' lightning web component
*/ 
public without sharing class TrackerController {

	@TestVisible 
	public static Date todaysDate = System.today();

	/**
	* @author Harrison Campbell
	* @description Holds Information used for the Tracker
	*/ 
	public class TrackerInformation {

		/**
		* @description Tracks if no nutrition session plans were found for the tracker
		*/ 
		@AuraEnabled
		public Boolean noPlansFound { get; set; }

		/**
		* @description The Id of the plan that is being displayed
		*/ 
		@AuraEnabled
		public String currentPlanId { get; set; }

		/**
		* @description The date of the plan that is being displayed
		*/ 
		@AuraEnabled
		public Date currentPlanDate { get; set; }

		/**
		* @description Is the current plan completed
		*/ 
		@AuraEnabled
		public Boolean currentPlanIsCompleted { get; set; }

		/**
		* @description The Id of plan 1 day before the current plan
		*/ 
		@AuraEnabled
		public String previousPlanId { get; set; }

		/**
		* @description The Id of plan 1 day after the current plan
		*/ 
		@AuraEnabled
		public String nextPlanId { get; set; }

		/**
		* @description A list of plans the user can select for the tracker. They are ordered by Plan Date in ascending order
		*/ 
		@AuraEnabled
		public List<AvailablePlan> availablePlans { get; set; }

		public TrackerInformation() {

			this.noPlansFound	= false;
			this.availablePlans = new List<AvailablePlan>();

		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a plan
	*/ 
	public class AvailablePlan {

		/**
		* @description The Date of the Plan
		*/ 
		@AuraEnabled
		public Date planDate { get; set; }

		/**
		* @description The Id of the Plan
		*/ 
		@AuraEnabled
		public String planId { get; set; }

		public AvailablePlan(Plan__c plan) {
			this.planDate	= plan.Session_Date__c;
			this.planId		= plan.Id;
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a Session Plan
	*/ 
	public class SessionPlan {

		/**
		 * @description The Id of the Session Plan
		 */
		@AuraEnabled
		public String planId { get; set; }
		
		/**
		 * @description The name of the Session Plan
		 */
		@AuraEnabled
		public String planName { get; set; }

		/**
		 * @description The Date of the Session Plan
		 */ 
		@AuraEnabled
		public Date planDate { get; set; }

		/**
		* @description The weekday of the Session Plan
		*/ 
		@AuraEnabled
		public String planWeekday { get; set; }

		/** 
		 * @description The Total Carbs for the Session Plan
		 */
		@AuraEnabled
		public Decimal totalCarbs { get; set; }
		
		/**
		 * @description The Total Fats for the Session Plan
		 */
		@AuraEnabled
		public Decimal totalFat { get; set; }

		/**
		 * @description The Total Protein for the Session Plan
		 */
		@AuraEnabled
		public Decimal totalProtein { get; set; }

		/**
		 * @description The Total Calories for the Session Plan
		 */
		@AuraEnabled
		public Decimal totalCalories { get; set; }
		
		/** 
		 * @description The Actual Carbs for the Session Plan
		 */
		@AuraEnabled
		public Decimal actualCarbs { get; set; }
		
		/**
		 * @description The Actual Fats for the Session Plan
		 */
		@AuraEnabled
		public Decimal actualFat { get; set; }

		/**
		 * @description The Actual Protein for the Session Plan
		 */
		@AuraEnabled
		public Decimal actualProtein { get; set; }

		/**
		 * @description The Actual Calories for the Session Plan
		 */
		@AuraEnabled
		public Decimal actualCalories { get; set; }
		
		/** 
		 * @description The Reference Carbs for the Session Plan
		 */
		@AuraEnabled
		public Decimal referenceCarbs { get; set; }
		
		/**
		 * @description The Reference Fats for the Session Plan
		 */
		@AuraEnabled
		public Decimal referenceFat { get; set; }

		/**
		 * @description The Reference Protein for the Session Plan
		 */
		@AuraEnabled
		public Decimal referenceProtein { get; set; }

		/**
		 * @description The Reference Calories for the Session Plan
		 */
		@AuraEnabled
		public Decimal referenceCalories { get; set; }

		/**
		 * @description The Session Blocks associated with the Plan
		 */
		@AuraEnabled
		public List<SessionBlock> sessionBlocks { get; set; }

		@AuraEnabled
		public Boolean trainerTargetsChanged { get; set; }

		@AuraEnabled
		public Boolean macroTargetsChanged { get; set; }

		public SessionPlan(Plan__c sessionPlan) {
			this.planId			= sessionPlan.Id;
			this.planName		= sessionPlan.Name;
			this.planDate		= sessionPlan.Session_Date__c;
			this.planWeekday	= sessionPlan.Weekdays__c;

			this.totalCarbs		= sessionPlan.Client_Plan_Carbs__c;
			this.totalFat		= sessionPlan.Client_Plan_Fat__c;
			this.totalProtein	= sessionPlan.Client_Plan_Protein__c;
			this.totalCalories	= sessionPlan.Client_Plan_Calories__c;

			this.actualCarbs	= (sessionPlan.Actual_Plan_Carbs__c != null) ? sessionPlan.Actual_Plan_Carbs__c : 0;
			this.actualFat		= (sessionPlan.Actual_Plan_Fat__c != null) ? sessionPlan.Actual_Plan_Fat__c : 0;
			this.actualProtein	= (sessionPlan.Actual_Plan_Protein__c != null) ? sessionPlan.Actual_Plan_Protein__c : 0;
			this.actualCalories	= (sessionPlan.Actual_Plan_Calories__c != null) ? sessionPlan.Actual_Plan_Calories__c : 0;
			
			this.referenceCarbs		= sessionPlan.Trainer_Plan_Carbs__c;
			this.referenceFat		= sessionPlan.Trainer_Plan_Fat__c;
			this.referenceProtein	= sessionPlan.Trainer_Plan_Protein__c;
			this.referenceCalories	= sessionPlan.Trainer_Plan_Calories__c;

			this.trainerTargetsChanged 	= sessionPlan.Trainer_Targets_Changed__c;
			this.macroTargetsChanged 	= sessionPlan.Macro_Targets_Changed__c;

			this.sessionBlocks	= new List<SessionBlock>();
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a Session Block
	*/ 
	public class SessionBlock {

		/**
		* @description Is the Session Block set to completed?
		*/ 
		@AuraEnabled
		public Boolean isCompleted { get; set; }

		/**
		* @description The Order of the Block in the Plan
		*/ 
		@AuraEnabled
		public Integer order { get; set; }

		/**
		* @description The Id of the block
		*/ 
		@AuraEnabled
		public String blockId { get; set; }

		/**
		 * @description The Name of the block
		 */ 
		@AuraEnabled
		public String blockName { get; set; }

		@AuraEnabled
		public Decimal blockProtein { get; set; }
		
		@AuraEnabled
		public Decimal blockCarbs { get; set; }
		
		@AuraEnabled
		public Decimal blockFat { get; set; }
		
		@AuraEnabled
		public Decimal blockCalories { get; set; }

		/**
		 * @description The Items associated with the block
		 */ 
		@AuraEnabled
		public List<SessionItem> items { get; set; }

		public SessionBlock(Block__c blk) {
			this.isCompleted	= blk.Completed__c;
			this.order			= blk.Display_Order__c.intValue();
			this.blockId		= blk.Id;
			this.blockName		= blk.Block_Short_Name__c;

			this.blockProtein	= blk.Client_Block_Protein__c;
			this.blockCarbs		= blk.Client_Block_Carbs__c;
			this.blockFat		= blk.Client_Block_Fat__c;
			this.blockCalories	= blk.Client_Block_Calories__c;

			this.items			= new List<SessionItem>();
			for(Item__c blkItm : blk.Plan_Items__r) {
				SessionItem sessItm = new SessionItem(blkItm);
				this.items.add(sessItm);
			}


		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on a Session Item
	*/ 
	public class SessionItem { 

		/**
		* @description The Id of the Item
		*/ 
		@AuraEnabled
		public String itemId { get; set; }

		/**
		* @description The Template_Item_Id_c id associated with the Session Item
		*/ 
		@AuraEnabled
		public String templateItemId { get; set; }

		/**
		 * @description The Name of the Item
		 */ 
		@AuraEnabled
		public String itemName { get; set; }

		/**
		* @description Tracks the order of the Item in the block
		*/ 
		@AuraEnabled
		public Integer order { get; set; }

		/**
		* @description Holds the measurement of the item (e.g. grams, unit, tbspn)
		*/ 
		@AuraEnabled
		public String measurement { get; set; }

		/**
		 * @description The Quantity of the Item
		 */
		@AuraEnabled
		public Decimal quantity { get; set; }

		/**
		* @description The Protein for the item
		*/ 
		@AuraEnabled
		public Decimal protein { get; set; }
		
		/**
		* @description The Fat for the item
		*/ 
		@AuraEnabled
		public Decimal fat { get; set; }
		
		/**
		* @description The Carbs for the item
		*/ 
		@AuraEnabled
		public Decimal carbs { get; set; }
		
		/**
		* @description The Calories for the item
		*/ 
		@AuraEnabled
		public Decimal calories { get; set; }

		/**
		* @description The amount of protein per '1' unit
		*/ 
		@AuraEnabled
		public Decimal proteinPerUnit { get; set; }
		
		/**
		* @description The amount of fat per '1' unit
		*/ 
		@AuraEnabled
		public Decimal fatPerUnit { get; set; }

		/**
		* @description The carbs of protein per '1' unit
		*/ 
		@AuraEnabled
		public Decimal carbsPerUnit { get; set; }

		/**
		* @description The calories of protein per '1' unit
		*/ 
		@AuraEnabled
		public Decimal caloriesPerUnit { get; set; }

		private Decimal Zero(Decimal dec) {
			return (dec == null) ? 0 : dec;
		}

		public SessionItem(Item__c itm) {
			this.itemId				= itm.Id;
			this.templateItemId		= itm.Template_Item_Id__c;
			this.itemName			= itm.Template_Item_Name__c;
			this.order				= itm.Display_Order__c.intValue();

			this.quantity			= itm.Client_Quantity__c;

			this.protein			= itm.Client_Protein__c;
			this.fat				= itm.Client_Fat__c;
			this.carbs				= itm.Client_Carbs__c;
			this.calories			= itm.Client_Calories__c;

			Decimal tmpQuantity = (itm.Reference_Plan_Item__r.Template_Quantity__c == null) ? 1 : itm.Reference_Plan_Item__r.Template_Quantity__c;

			this.proteinPerUnit		= Zero(itm.Reference_Plan_Item__r.Template_Protein__c) / tmpQuantity;
			this.fatPerUnit			= Zero(itm.Reference_Plan_Item__r.Template_Fat__c) / tmpQuantity;
			this.carbsPerUnit		= Zero(itm.Reference_Plan_Item__r.Template_Carbs__c) / tmpQuantity;
			this.caloriesPerUnit	= Zero(itm.Reference_Plan_Item__r.Template_Calories__c) / tmpQuantity;

			this.measurement		= itm.Reference_Plan_Item__r.Template_Measurement__c;
		}

	}

    /**
    * @author Harrison Campbell
    * @description Holds Information on the Nutritional Item
    */ 
    public class NutritionalItem {

        /**
        * @description The Id of a Template_Item__c
        */ 
        @AuraEnabled 
		public String id { get; set; }

        /**
        * @description The Name of the Nutritional Item
        */ 
        @AuraEnabled 
		public String name { get; set; }

        /**
        * @description Grams, tbspn, etc.
        */ 
        @AuraEnabled 
		public String measure { get; set; }

        /**
        * @description The total quantity
        */ 
        @AuraEnabled 
		public Decimal quantity { get; set; }

        /**
        * @description The Total Protein of the item
        */ 
        @AuraEnabled 
		public Decimal protein { get; set; }

        /**
        * @description The Total Fat of the item
        */ 
        @AuraEnabled 
		public Decimal fat { get; set; }

        /**
        * @description The Total Carbs of the item
        */ 
        @AuraEnabled 
		public Decimal carbs { get; set; }

        /**
        * @description The Total Calories of the item
        */ 
        @AuraEnabled 
		public Decimal calories { get; set; }
		
		/**
		* @description The increments that the quantity can increase by
		*/ 
		@AuraEnabled
		public Decimal quantityIncrement { get; set; }

		/**
		* @description The minimum quantity of the item
		*/ 
		@AuraEnabled
		public Decimal minimumQuantity { get; set; }

		/**
		* @description Create a nutritional item from a template item
		* @param tmpItem The template item
		*/ 
		public NutritionalItem(Template_Item__c tmpItem) {
			this.id					= tmpItem.Id;
			this.name				= tmpItem.Name;
			this.measure			= tmpItem.Measurement__c;
			this.quantity			= tmpItem.Item_Quantity__c;
			this.protein			= tmpItem.Protein__c;
			this.fat				= tmpItem.Fat__c;
			this.carbs				= tmpItem.Carbs__c;
			this.calories			= tmpItem.Calories__c;
			this.quantityIncrement	= tmpItem.Quantity_Increment__c;
			this.minimumQuantity	= tmpItem.Minimum_Quantity__c;
		}

    }

	/**
	* @description Query the Session Plan Data
	* @param planId The Id of the Plan
	* @return Returns the Session Plan data
	*/ 
	@AuraEnabled
	public static AT_M_Nutrition.NutritionPlan QuerySessionPlanData(String planId) {
		AT_M_Nutrition.NutritionPlan sessionPlan = AT_M_Nutrition.QueryNutritionPlans(new Id[] { planId }).get(0);
		return sessionPlan;
	}
	
	/**
	* @description Update the Block Completed status
	* @param blockId The Id of the Block__c that we want to set to completed
	* @param state The State, we want to set the block
	*/ 
	@AuraEnabled
	public static void UpdateBlockCompletedState(String blockId, Boolean state) {

		//Set the Block Status to Completed
		Block__c b = [
			SELECT
				Id,
				Plan__c,
				Completed__c
			FROM Block__c
			WHERE Id = :blockId
		];
		b.Completed__c = state;
		update b;

		//Calculate the Actuals for the block plans
		Planner.SetActualValues(b.Plan__c);

	}

	/**
	* @description Complete the Plan
	* @param planId The Id of the Plan
	*/ 
	@AuraEnabled
	public static void CompletePlan(String planId) {
		
		//Take the Plan as completed, and all of it's related blocks
		Plan__c currPlan = [
			SELECT
				Id,
				Completed__c,
				(SELECT Id, Completed__c FROM Blocks__r)
			FROM Plan__c
			WHERE Id = :planId
		];
		currPlan.Completed__c = true;
		
		for(Block__c blk : currPlan.Blocks__r) {
			blk.Completed__c = true;
		}

		update currPlan;
		update currPlan.Blocks__r;

		//Set the actual values for the plan, and blocks
		Planner.SetActualValues(currPlan.Id);

	}
	
	/**
	* @description Set the Plan Completed State
	* @param planId The Id of the Plan__c we want to update
	* @param state The state we want to set the Completed__c field (true, or false)
	*/ 
	@AuraEnabled
	public static void SetPlanCompleteState(String planId, Boolean state) {
	
		//Take the Plan as completed, and all of it's related blocks
		Plan__c currPlan = [
			SELECT
				Id,
				Completed__c,
				(SELECT Id, Completed__c FROM Blocks__r)
			FROM Plan__c
			WHERE Id = :planId
		];
		currPlan.Completed__c = state;
		update currPlan;

		//If the Plan was set to completed, then set all child blocks to completed
		if(state == true) {
			for(Block__c blk : currPlan.Blocks__r) {
				blk.Completed__c = true;
			}
			update currPlan.Blocks__r;
		}

		//Set the actual values for the plan, and blocks
		Planner.SetActualValues(currPlan.Id);

	}

	/**
	* @description Add a Templte Item to a Block
	* @param templateItemId 
	* @param blockId 
	*/ 
	@AuraEnabled
	public static void AddTemplateItemToBlock(String templateItemId, String blockId) {
	        
		Id sessItemRecTypeId = Schema.SObjectType.Item__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Session_Item').getRecordTypeId();

		Template_Item__c tmpItem = [
			SELECT
				Id,
				Name,
				RecordTypeId,
				Item_Quantity__c,
				Carbs__c,
				Protein__c,
				Fat__c,
				Calories__c,
				Allergy__c,
				Restriction__c
			FROM Template_Item__c
			WHERE Id = :templateItemId
		];

		Block__c blk = [
			SELECT
				Id,
				(
					SELECT
						Id,
						Display_Order__c
					FROM Plan_Items__r
					ORDER BY Display_Order__c DESC NULLS LAST
				)
			FROM Block__c
			WHERE Id = :blockId
		];

		Decimal tmpItemCarb			= tmpItem.Carbs__c != null ? tmpItem.Carbs__c : 0;
		Decimal tmpItemProtein		= tmpItem.Protein__c != null ? tmpItem.Protein__c : 0;
		Decimal tmpItemFat			= tmpItem.Fat__c != null ? tmpItem.Fat__c : 0;
		Decimal tmpItemCalories		= tmpItem.Calories__c != null ? tmpItem.Calories__c : 0;

		Item__c newItem = new Item__c(
			Name				= tmpItem.Name,
			RecordTypeId		= sessItemRecTypeId,
			Display_Order__c	= (blk.Plan_Items__r[0].Display_Order__c + 1),
			Block__c			= blockId,
			Template_Item_Id__c	= tmpItem.Id,
			Quantity__c			= tmpItem.Item_Quantity__c,
			Carb__c				= tmpItemCarb,
			Protein__c			= tmpItemProtein,
			Fat__c				= tmpItemFat,
			Calories__c			= tmpItemCalories
		);

		insert newItem;

	}


	/**
	* @description Get all Nutritional Items both standard and Custom made Items
	* @return Returns Nutritional Items
	*/ 
	@AuraEnabled
	public static List<NutritionalItem> GetNutritionalItems() {

		List<NutritionalItem> allNutritionalItems = new List<NutritionalItem>();

		Id accountID = [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()].Contact.AccountId;

		Account acc = [
			SELECT
				Id,
				Lactose_intolerant__pc,
				Gluten_Free__pc,
				Shellfish_Allergy__pc,
				Nut_Allergy__pc,
				Corn_Allergy__pc,
				Dairy_Allergy__pc,
				Egg_Allergy__pc,
				Peanut_Allergy__pc,
				Meat_Allergy__pc,
				Soy_Allergy__pc,
				Fish_Allergy__pc,
				Are_you_Vegetarian_or_Vegan__pc
			FROM Account
			WHERE Id = :accountID
		];

		Set<String> userAllergies = new Set<String>();
		if(acc.Lactose_intolerant__pc == true) { userAllergies.add('Lactose'); }
		if(acc.Gluten_Free__pc == true) { userAllergies.add('Gluten'); }
		if(acc.Shellfish_Allergy__pc == true) { userAllergies.add('Shellfish'); }
		if(acc.Nut_Allergy__pc == true) { userAllergies.add('Nuts'); }
		if(acc.Corn_Allergy__pc == true) { userAllergies.add('Corn'); }
		if(acc.Dairy_Allergy__pc == true) { userAllergies.add('Dairy'); }
		if(acc.Egg_Allergy__pc == true) { userAllergies.add('Egg'); }
		if(acc.Peanut_Allergy__pc == true) { userAllergies.add('Peanut'); }
		if(acc.Meat_Allergy__pc == true) { userAllergies.add('Meat'); }
		if(acc.Soy_Allergy__pc == true) { userAllergies.add('Soy'); }
		if(acc.Fish_Allergy__pc == true) { userAllergies.add('Fish'); }
		
        for (Template_Item__c itm: [
			SELECT 
				Id, 
				Name,
				Measurement__c,
				Quantity_Increment__c,
				Minimum_Quantity__c,
				Item_Quantity__c, 
				Carbs__c, 
				Protein__c, 
				Fat__c, 
				Calories__c, 
				Allergy__c, 
				Restriction__c 
			FROM Template_Item__c 
			WHERE (RecordType.Name = 'Nutrition') OR (RecordType.Name = 'Custom Nutrition' AND Client_Created_By__c = :UserInfo.getUserId()) 
			ORDER BY Name
		]) {
			Boolean cantEat = false;

			//Check that the User can use the allergies
			if(userAllergies.size() > 0 && String.isNotBlank(itm.Allergy__c)) {
				Set<String> itmAllergies = new Set<String>(itm.Allergy__c.split(';'));
				for(String allergy : itmAllergies) {
					if(userAllergies.contains(allergy)) {
						cantEat = true;
					}
				}
			}

			if(itm.Restriction__c != null && acc.Are_you_Vegetarian_or_Vegan__pc != null && (acc.Are_you_Vegetarian_or_Vegan__pc == 'Vegetarian' || acc.Are_you_Vegetarian_or_Vegan__pc == 'Vegan')) {
				if(!itm.Restriction__c.contains(acc.Are_you_Vegetarian_or_Vegan__pc)) {
					cantEat = true;
				}
			}

			if(cantEat == false) {
				NutritionalItem nutItem = new NutritionalItem(itm);
				allNutritionalItems.add(nutItem);
			}

        }
		
		return allNutritionalItems;

	}

	@AuraEnabled
	public static void UpdateTrackerData(String trackerData, String planId) {

		Map<String, Object> planData = (Map<String, Object>) JSON.deserializeUntyped(trackerData);

		//Update the Session Plan
		Plan__c sessPlan = [
			SELECT
				Id,
				Client_Plan_Calories__c,
				Client_Plan_Carbs__c,
				Client_Plan_Fat__c,
				Client_Plan_Protein__c
			FROM Plan__c
			WHERE Id = :planId
		];

		if(planData.containsKey('calories')) {
			sessPlan.Client_Plan_Calories__c	= (Decimal)planData.get('calories');
		}
		if(planData.containsKey('carbs')) {
			sessPlan.Client_Plan_Carbs__c		= (Decimal)planData.get('carbs');
		}
		if(planData.containsKey('fat')) {
			sessPlan.Client_Plan_Fat__c		= (Decimal)planData.get('fat');
		}
		if(planData.containsKey('protein')) {
			sessPlan.Client_Plan_Protein__c	= (Decimal)planData.get('protein');
		}

		//Update the Session and Items
		Map<String, Block__c> sessBlocks = new Map<String, Block__c>([
			SELECT
				Id,
				Client_Block_Calories__c,
				Client_Block_Carbs__c,
				Client_Block_Fat__c,
				Client_Block_Protein__c,
				(
					SELECT
						Id,
						Client_Calories__c,
						Client_Carbs__c,
						Client_Fat__c,
						Client_Protein__c,
						Client_Quantity__c
					FROM Plan_Items__r
				)
			FROM Block__c
			WHERE Plan__c = :planId
		]);

		List<Block__c> updatedBlocks	= new List<Block__c>();
		List<Item__c> updatedItems		= new List<Item__c>();

		Map<String, Object> allBlockData = (Map<String, Object>) planData.get('blocks');

		for(String blockId : allBlockData.keySet()) {
			
			Block__c block					= sessBlocks.get(blockId);

			Map<String, Object> blockData	= (Map<String, Object>) allBlockData.get(blockId);
			
			block.Client_Block_Calories__c	= (Decimal)blockData.get('calories');
			block.Client_Block_Carbs__c		= (Decimal)blockData.get('carbs');
			block.Client_Block_Fat__c		= (Decimal)blockData.get('fat');
			block.Client_Block_Protein__c	= (Decimal)blockData.get('protein');

			updatedBlocks.add(block);

			Map<String, Item__c> blkItems			= new Map<String, Item__c>(block.Plan_Items__r);
			Map<String, Object> allBlockItemData	= (Map<String, Object>) blockData.get('items');

			for(String itemId : allBlockItemData.keySet()) {

				Item__c itm					= blkItems.get(itemId);
				Map<String, Object> itmData	= (Map<String, Object>) allBlockItemData.get(itemId);

				itm.Client_Calories__c				= (Decimal)itmData.get('calories');
				itm.Client_Carbs__c					= (Decimal)itmData.get('carbs');
				itm.Client_Fat__c					= (Decimal)itmData.get('fat');
				itm.Client_Protein__c				= (Decimal)itmData.get('protein');
				itm.Client_Quantity__c				= (Decimal)itmData.get('quantity');

				updatedItems.add(itm);
				
			}

		}
		
		update sessPlan;
		if(updatedBlocks.size() > 0) {
			update updatedBlocks;
		}
		if(updatedItems.size() > 0) {
			update updatedItems;
		}

	}

	@AuraEnabled
	public static void UpdateBlockTrackerData(String blockId, String blockData) {
	
		//Update the Client Block Totals
		Map<String, Object> blockDataMap = (Map<String, Object>) JSON.deserializeUntyped(blockData);

		Block__c sessBlock = [
			SELECT
				Id,
				Plan__c,
				Client_Block_Calories__c,
				Client_Block_Carbs__c,
				Client_Block_Fat__c,
				Client_Block_Protein__c
			FROM Block__c
			WHERE Id = :blockId
		];
		
		sessBlock.Client_Block_Calories__c	= (Decimal)blockDataMap.get('blockCalories');
		sessBlock.Client_Block_Carbs__c		= (Decimal)blockDataMap.get('blockCarbs');
		sessBlock.Client_Block_Fat__c		= (Decimal)blockDataMap.get('blockFat');
		sessBlock.Client_Block_Protein__c	= (Decimal)blockDataMap.get('blockProtein');
		update sessBlock;

		Map<String, Object> itemDataMap	= (Map<String, Object>) blockDataMap.get('items');

		List<Item__c> blockItems = [
			SELECT
				Id,
				Client_Quantity__c,
				Client_Calories__c,
				Client_Fat__c,
				Client_Carbs__c,
				Client_Protein__c
			FROM Item__c
			WHERE Id IN :itemDataMap.keySet()
		];

		for(Item__c itm : blockItems) {

			Map<String, Object> itemData = (Map<String, Object>) itemDataMap.get(itm.Id);

			itm.Client_Quantity__c	= (Decimal)itemData.get('quantity');
			itm.Client_Calories__c	= (Decimal)itemData.get('calories');
			itm.Client_Fat__c		= (Decimal)itemData.get('fat');
			itm.Client_Carbs__c		= (Decimal)itemData.get('carbs');
			itm.Client_Protein__c	= (Decimal)itemData.get('protein');
		}

		update blockItems;
		
		//Set the Plan Totals
		Plan__c sessPlan = [
			SELECT
				Id,
				Client_Plan_Calories__c,
				Client_Plan_Carbs__c,
				Client_Plan_Fat__c,
				Client_Plan_Protein__c,
				(
					SELECT
						Id,
						Client_Block_Calories__c,
						Client_Block_Carbs__c,
						Client_Block_Fat__c,
						Client_Block_Protein__c
					FROM Blocks__r
				)
			FROM Plan__c
			WHERE Id = :sessBlock.Plan__c
		];

		Decimal planCalories	= 0;
		Decimal planCarbs		= 0;
		Decimal planFat			= 0;
		Decimal planProtein		= 0;

		for(Block__c blk : sessPlan.Blocks__r) {
			planCalories	+= (blk.Client_Block_Calories__c != null) ? blk.Client_Block_Calories__c : 0;
			planCarbs		+= (blk.Client_Block_Carbs__c != null) ? blk.Client_Block_Carbs__c : 0;
			planFat			+= (blk.Client_Block_Fat__c != null) ? blk.Client_Block_Fat__c : 0;
			planProtein		+= (blk.Client_Block_Protein__c != null) ? blk.Client_Block_Protein__c : 0;
		}

		sessPlan.Client_Plan_Calories__c	= planCalories;
		sessPlan.Client_Plan_Carbs__c		= planCarbs;
		sessPlan.Client_Plan_Fat__c		= planFat;
		sessPlan.Client_Plan_Protein__c	= planProtein;
		update sessPlan;

	}

	/**
	* @description Add New Session Items
	* @param newItems A list of new Items
	*/ 
	@AuraEnabled
	public static void AddNewSessionItems(List<AT_M_Nutrition.NutritionItem> newItems) {
		
		Id sessItemRecTypeId = Schema.SObjectType.Item__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Session_Item').getRecordTypeId();

		List<Item__c> newSessionItems = new List<Item__c>();

		for(AT_M_Nutrition.NutritionItem nItem : newItems) {
			Item__c newItem							= new Item__c();
			newItem.RecordTypeId					= sessItemRecTypeId;
			newItem.Name							= nItem.name;
			newItem.Block__c						= nItem.blockId;
			newItem.Display_Order__c				= nItem.order;

			newItem.Trainer_Quantity__c				= nItem.referenceQuantity;
			newItem.Trainer_Protein__c				= nItem.referenceProtein;
			newItem.Trainer_Carbs__c				= nItem.referenceCarbs;
			newItem.Trainer_Fat__c					= nItem.referenceFat;
			newItem.Trainer_Calories__c				= nItem.referenceCalories;

			newItem.Client_Quantity__c				= nItem.quantity;
			newItem.Client_Protein__c				= nItem.protein;
			newItem.Client_Carbs__c					= nItem.carbs;
			newItem.Client_Fat__c					= nItem.fat;
			newItem.Client_Calories__c				= nItem.calories;

			newItem.Actual_Quantity__c				= nItem.actualQuantity;
			newItem.Actual_Protein__c				= nItem.actualProtein;
			newItem.Actual_Carbs__c					= nItem.actualCarbs;
			newItem.Actual_Fat__c					= nItem.actualFat;
			newItem.Actual_Calories__c				= nItem.actualCalories;

			newItem.Template_Measurement__c			= nItem.referenceMeasurement;
			newItem.Template_Quantity_Increment__c	= nItem.referenceQuantityIncrement;
			newItem.Template_Minimum_Quantity__c	= nItem.referenceMinimumQuantity;
			/*
                this.referenceQuantityIncrement = item.Template_Quantity_Increment__c != null ? item.Template_Quantity_Increment__c : 1;
                this.referenceMinimumQuantity   = item.Template_Minimum_Quantity__c != null ? item.Template_Minimum_Quantity__c : 0;
			*/
			newSessionItems.add(newItem);

		}

		insert newSessionItems;

	}

	@AuraEnabled
	public static void AddTemplateItems(String blockTrackerData) {
	
		Id sessItemRecTypeId = Schema.SObjectType.Item__c.getRecordTypeInfosByDeveloperName().get('Nutrition_Session_Item').getRecordTypeId();

		Map<String, Object> blockData = (Map<String, Object>) JSON.deserializeUntyped(blockTrackerData);

		//Get the Template Ids, and 
		Set<String> templateItemIds = new Set<String>();
		for(String blockId : blockData.keySet()) {
			Map<String, Object> allItemData	= (Map<String, Object>) blockData.get(blockId);
			for(String itemId : allItemData.keySet()) {
				templateItemIds.add(itemId);
			}
		}

		Map<Id, Template_Item__c> templateItemMap = new Map<Id, Template_Item__c>([
			SELECT
				Id,
				Name,
				Measurement__c,
				Minimum_Quantity__c,
				Item_Quantity__c,
				Carbs__c,
				Protein__c,
				Fat__c,
				Calories__c
			FROM Template_Item__c
			WHERE Id IN :templateItemIds
		]);
		
		List<Item__c> newItems = new List<Item__c>();
	
		for(String blockId : blockData.keySet()) {
		
			Map<String, Object> allItemData	= (Map<String, Object>) blockData.get(blockId);

			for(String itemId : allItemData.keySet()) {
			
				Map<String, Object> itemData	= (Map<String, Object>) allItemData.get(itemId);

				Template_Item__c tmpItem = templateItemMap.get(itemId);
				

				Decimal tmpItemQuantity		= tmpItem.Item_Quantity__c != null ? tmpItem.Item_Quantity__c : 1; //HACK
				Decimal tmpItemCarb			= tmpItem.Carbs__c != null ? tmpItem.Carbs__c : 0;
				Decimal tmpItemProtein		= tmpItem.Protein__c != null ? tmpItem.Protein__c : 0;
				Decimal tmpItemFat			= tmpItem.Fat__c != null ? tmpItem.Fat__c : 0;
				Decimal tmpItemCalories		= tmpItem.Calories__c != null ? tmpItem.Calories__c : 0;

				Integer itemOrder	= (Integer)itemData.get('order');
				Decimal quantity	= (Decimal)itemData.get('quantity');

				Decimal tmpCarbsPerUnit		= tmpItemCarb / tmpItemQuantity;
				Decimal tmpProteinPerUnit	= tmpItemProtein / tmpItemQuantity;
				Decimal tmpFatPerUnit		= tmpItemFat / tmpItemQuantity;
				Decimal tmpCaloriesPerUnit	= tmpItemCalories / tmpItemQuantity;

				Item__c newItem = new Item__c(
					Name							= tmpItem.Name,
					RecordTypeId					= sessItemRecTypeId,
					Display_Order__c				= itemOrder,
					Block__c						= blockId,
					Template_Item_Id__c				= tmpItem.Id,
					Client_Quantity__c				= quantity,
					Client_Carbs__c					= tmpCarbsPerUnit  * quantity,
					Client_Protein__c				= tmpProteinPerUnit  * quantity,
					Client_Fat__c					= tmpFatPerUnit  * quantity,
					Client_Calories__c				= tmpCaloriesPerUnit * quantity,
					Template_Quantity__c			= tmpItemQuantity,
					Template_Item_Name__c			= tmpItem.Name,
					Template_Carbs__c				= tmpItem.Carbs__c,
					Template_Protein__c				= tmpItem.Protein__c,
					Template_Fat__c					= tmpItem.Fat__c,
					Template_Calories__c			= tmpItem.Calories__c,
					Template_Measurement__c			= tmpItem.Measurement__c,
					Template_Minimum_Quantity__c	= tmpItem.Minimum_Quantity__c
				);
				newItems.add(newItem);
			}
		}

		if(newItems.size() > 0) {
			insert newItems;
		}

	}

	/**
	* @description Delete Items
	* @param itemIds The Ids of the Items we want to delete
	*/ 
	@AuraEnabled
	public static void DeleteItems(List<String> itemIds) {

		List<Item__c> items = [
			SELECT
				Id
			FROM Item__c
			WHERE Id IN :itemIds
		];
		delete items;

	}

	@AuraEnabled
	public static TrackerInformation GetTrackerInformation(String planId) {
		
		System.debug('Getting Tracker Information');

		TrackerInformation tInformation = new TrackerInformation();
		
		//Get the Account Id for the active user
        Id accountID = [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()].Contact.AccountId;

		//If no Plan Id was provided
        if (String.isBlank(planId)) {

			//Find the most recent plan in the past
            List<Plan__c> plans = [
				SELECT 
					Id, 
					Session_Date__c,
					Completed__c
					FROM Plan__c 
				WHERE (Check_In__r.Account__c = :accountID) AND (RecordType.DeveloperName = 'Nutrition_Session_Plan') AND (Session_Date__c <= :todaysDate) 
				ORDER BY Session_Date__c DESC NULLS LAST
				LIMIT 1 
			];
			
			//If there are no plans in the past, then get one from the future
            if (plans.isEmpty()) {
                plans = [
					SELECT 
						Id, 
						Session_Date__c,
						Completed__c
					FROM Plan__c 
					WHERE (Check_In__r.Account__c = :accountID) AND (RecordType.DeveloperName = 'Nutrition_Session_Plan') AND (Session_Date__c > :todaysDate) 
					ORDER BY Session_Date__c ASC NULLS LAST 
					LIMIT 1
				];
            }

			//If the Plans are still empty, return nothing
			if(plans.isEmpty()) {
				tInformation.noPlansFound = true;
				return tInformation;
			}


			tInformation.currentPlanId			= plans[0].Id;
			tInformation.currentPlanDate		= plans[0].Session_Date__c;
			tInformation.currentPlanIsCompleted = plans[0].Completed__c;

		} else {
		
			//Else get the Plan using the Plan Id
            Plan__c current						= [SELECT Id, Session_Date__c, Completed__c, Check_In__r.Account__c FROM Plan__c WHERE Id = :planId];
            tInformation.currentPlanId			= planId;
            tInformation.currentPlanDate		= current.Session_Date__c;
			tInformation.currentPlanIsCompleted	= current.Completed__c;

		}

		
		//If the plan Id is set, then query the next and previous plans
        if (tInformation.currentPlanDate != null) {
            List<Plan__c> next = [
				SELECT 
					Id 
				FROM Plan__c 
				WHERE (Check_In__r.Account__c = :accountID) AND (RecordType.DeveloperName = 'Nutrition_Session_Plan') AND (Session_Date__c > :tInformation.currentPlanDate) 
				ORDER BY Session_Date__c ASC 
				LIMIT 1
			];
            List<Plan__c> previous = [
				SELECT 
					Id 
				FROM Plan__c 
				WHERE (Check_In__r.Account__c = :accountID) AND (RecordType.DeveloperName = 'Nutrition_Session_Plan') AND (Session_Date__c < :tInformation.currentPlanDate) 
				ORDER BY Session_Date__c DESC 
				LIMIT 1
			];

			//Get the next and previous plan Ids
            if (!next.isEmpty()) tInformation.nextPlanId = next[0].Id;
            if (!previous.isEmpty()) tInformation.previousPlanId = previous[0].Id;
        }

		
		//Get a list of plans that the user can select from to quickly navigate too.
		//Search 7 days back, and into the future
		tInformation.availablePlans = new List<AvailablePlan>();
		for(Plan__c navPlan : [
			SELECT
				Id,
				Session_Date__c
			FROM Plan__c
			WHERE (Check_In__r.Account__c = :accountID) AND (RecordType.DeveloperName = 'Nutrition_Session_Plan') AND (Session_Date__c >= :tInformation.currentPlanDate.addDays(-7)) 
			ORDER BY Session_Date__c ASC 
		]) {
			tInformation.availablePlans.add(new AvailablePlan(navPlan));
		}


		return tInformation;

	}


}