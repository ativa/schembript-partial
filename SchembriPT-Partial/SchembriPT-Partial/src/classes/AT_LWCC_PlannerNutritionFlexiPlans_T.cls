/**
* @author Harrison Campbell
* @description Test the 'AT_LWCC_PlannerNutritionFlexiPlans' class
*/ 
@isTest 
private class AT_LWCC_PlannerNutritionFlexiPlans_T {
	
	/**
	* @description Get the picklist values for a given field
	* @param objectApiName The API name of the object
	* @param fieldApiName The API name of the field
	* @return Returns picklist values
	*/ 
	private static List<AT_LWCC_PlannerNutritionFlexiPlans.PicklistValue> GetPicklistValues(String objectApiName, String fieldApiName) {
        Map<String, Schema.SObjectType> gd			= Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map	= gd.get(objectApiName).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues	= field_map.get(fieldApiName).getDescribe().getPickListValues();       
		List<AT_LWCC_PlannerNutritionFlexiPlans.PicklistValue> optionlist = new List<AT_LWCC_PlannerNutritionFlexiPlans.PicklistValue>();
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(new AT_LWCC_PlannerNutritionFlexiPlans.PicklistValue(pv));
        }
        return optionlist;
	}

	/**
	* @description Query the Nutrition Target Record Type
	* @return 
	*/ 
	private static RecordType testTool_QueryNutritionTargetRecordType() {
		return [SELECT Id FROM RecordType WHERE SobjectType = 'Targets__c' AND DeveloperName = 'Nutrition'];
	}
    
    /**
     * @description Create a Test Check-In 
     */
	private static Check_In__c testTool_createCheckIn(Contact cont) {
		Check_In__c newCheckIn 		= new Check_In__c();
        newCheckIn.Contact__c 		= cont.Id;
		newCheckIn.h_Test_Record__c = true;
		insert newCheckIn;
		return newCheckIn;
	}
    
    /**
     * @description Create a Test Contact
     */
    private static Contact testTool_createContact() {
        Contact testContact = new Contact();
        testContact.LastName = 'Test Contact';
        insert testContact;
        return testContact;
    }

    /**
     * @description Create a test Target
     * @param checkIn The Check-In linked to the target
     * @param protein The protein of the target
     * @param carbs The carbs of the target
     * @param fat The fat of the target
     * @return Returns the new target
     */
	private static Targets__c testTool_createTarget(Check_In__c checkIn, Decimal protein, Decimal carbs, Decimal fat, String weekdays, Id recordTypeId) {
		Targets__c newTarget 	= new Targets__c();
        newTarget.RecordTypeId	= recordTypeId;
		newTarget.Protein__c	= protein;
		newTarget.Carbs__c		= carbs;
		newTarget.Fat__c		= fat;
        newTarget.Weekdays__c	= weekdays;
		newTarget.Check_In__c	= checkIn.Id;
		insert newTarget;
		return newTarget;
	}
    
	private static AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget testTool_createNewTargetData(Id checkInId, String name, Decimal protein, Decimal carbs, Decimal fat, String[] weekdays) {
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget nTarget = new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget();
		nTarget.checkInId	= checkInId;
		nTarget.name		= name;
		nTarget.protein		= protein;
		nTarget.carbs		= carbs;
		nTarget.fat			= fat;
		nTarget.weekdays	= weekdays;
		return nTarget;
	}

	private static AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget testTool_createOldTargetData(Id checkInId, String name, Decimal protein, Decimal carbs, Decimal fat, String weekdays) {
		Targets__c newTarget = new Targets__c();
		newTarget.Check_In__c		= checkInId;
		newTarget.Display_Name__c	= name;
		newTarget.Protein__c		= protein;
		newTarget.Carbs__c			= carbs;
		newTarget.Fat__c			= fat;
		newTarget.Weekdays__c		= weekdays;
		insert newTarget;
		return new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget(newTarget);
	}
    private static void assertTool_assertTargetIsCorrect(Targets__c target, AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetData, Boolean newTarget) {
		if(newTarget == false) {
			System.assertEquals(target.Id, 				targetData.id);
		}
		System.assertEquals(target.Display_Name__c,		targetData.name);
        System.assertEquals(target.Protein__c, 			targetData.protein);
        System.assertEquals(target.Carbs__c, 			targetData.carbs);
        System.assertEquals(target.Fat__c, 				targetData.fat);
        //System.assertEquals(target.Calories__c, targetData.calories);
        System.assertEquals(target.Check_In__c,			targetData.checkInId);

        String[] weekdays = target.Weekdays__c.split(';');        
        System.assertEquals(weekdays.size(), targetData.weekdays.size());
        for(Integer i = 0; i < weekdays.size(); i++) {
            System.assertEquals(weekdays[i], targetData.weekdays[i]);
        }
        
    }

    private static void assertTool_assertPicklistOptions(List<AT_LWCC_PlannerNutritionFlexiPlans.PicklistValue> expectedPicklistValues, List<AT_LWCC_PlannerNutritionFlexiPlans.PicklistValue> actualPicklistValues) {
        System.assertEquals(expectedPicklistValues.size(), actualPicklistValues.size());
        for(Integer i = 0; i < expectedPicklistValues.size(); i++) {
            System.assertEquals(expectedPicklistValues.get(i).label, actualPicklistValues.get(i).label);
            System.assertEquals(expectedPicklistValues.get(i).value, actualPicklistValues.get(i).value);
        }
    }

	@isTest
	private static void GetNutritionalTargets_GetTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Monday;Tuesday', nutTargetRecType.Id);
        Targets__c targetTwo 		= testTool_createTarget(testCheckIn, 2, 4, 6, 'Wednesday;Thursday;Friday', nutTargetRecType.Id);
		Targets__c targetThree 		= testTool_createTarget(testCheckIn, 10, 31, 67, 'Saturday;Sunday', nutTargetRecType.Id);

        Test.startTest();
        List<AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget> targetData = AT_LWCC_PlannerNutritionFlexiPlans.GetNutritionalTargets(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(3, targetData.size());
        assertTool_assertTargetIsCorrect(targetOne, targetData.get(0), false);
        assertTool_assertTargetIsCorrect(targetTwo, targetData.get(1), false);
        assertTool_assertTargetIsCorrect(targetThree, targetData.get(2), false);
	}
	
	@isTest
	private static void GetNutritionalTargets_GetSingleTarget() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Monday;Tuesday', nutTargetRecType.Id);
        
        Test.startTest();
        List<AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget> targetData = AT_LWCC_PlannerNutritionFlexiPlans.GetNutritionalTargets(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(1, targetData.size());
        assertTool_assertTargetIsCorrect(targetOne, targetData.get(0), false);
	}
	
	@isTest
	private static void GetNutritionalTargets_GetNoTargets() {

        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
        
        Test.startTest();
        List<AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget> targetData = AT_LWCC_PlannerNutritionFlexiPlans.GetNutritionalTargets(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(0, targetData.size());
        
    }

    @IsTest
    private static void GetFlexiPlanData_GetTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Monday;Tuesday', nutTargetRecType.Id);
        Targets__c targetTwo 		= testTool_createTarget(testCheckIn, 2, 4, 6, 'Wednesday;Thursday;Friday', nutTargetRecType.Id);
		Targets__c targetThree 		= testTool_createTarget(testCheckIn, 10, 31, 67, 'Saturday;Sunday', nutTargetRecType.Id);

        Test.startTest();
        AT_LWCC_PlannerNutritionFlexiPlans.FlexiPlanData planData = AT_LWCC_PlannerNutritionFlexiPlans.GetFlexiPlanData(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(3, planData.nutritionTargets.size());
        assertTool_assertTargetIsCorrect(targetOne, 	planData.nutritionTargets.get(0), false);
        assertTool_assertTargetIsCorrect(targetTwo, 	planData.nutritionTargets.get(1), false);
        assertTool_assertTargetIsCorrect(targetThree, 	planData.nutritionTargets.get(2), false);
        
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Allergies_and_Intolerances__c'), 	planData.allergies);
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Dietary_Restrictions__c'), 		planData.dietaryRestrictions);
    }
    
    @IsTest
    private static void GetFlexiPlanData_GetSingleTarget() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Monday;Tuesday', nutTargetRecType.Id);

        Test.startTest();
        AT_LWCC_PlannerNutritionFlexiPlans.FlexiPlanData planData = AT_LWCC_PlannerNutritionFlexiPlans.GetFlexiPlanData(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(1, planData.nutritionTargets.size());
        assertTool_assertTargetIsCorrect(targetOne, 	planData.nutritionTargets.get(0), false);
        
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Allergies_and_Intolerances__c'), 	planData.allergies);
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Dietary_Restrictions__c'), 		planData.dietaryRestrictions);
	}
    
    @IsTest
    private static void GetFlexiPlanData_GetNoTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

        Test.startTest();
        AT_LWCC_PlannerNutritionFlexiPlans.FlexiPlanData planData = AT_LWCC_PlannerNutritionFlexiPlans.GetFlexiPlanData(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(0, planData.nutritionTargets.size());
        
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Allergies_and_Intolerances__c'), 	planData.allergies);
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Dietary_Restrictions__c'), 		planData.dietaryRestrictions);
    }

	@IsTest
	private static void SaveNutritionalTargets_CreateNewTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		//Create new targets
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetOne		= testTool_createNewTargetData(testCheckIn.Id, 'Target 1', 1, 2, 3, new String[]{ 'Monday', 'Tuesday' });
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetTwo		= testTool_createNewTargetData(testCheckIn.Id, 'Target 2', 2, 3, 4, new String[]{ 'Wednesday', 'Thursday', 'Friday' });
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetThree	= testTool_createNewTargetData(testCheckIn.Id, 'Target 3', 3, 4, 5, new String[]{ 'Saturday', 'Sunday' });

		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionFlexiPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget[] { targetOne, targetTwo, targetThree }, null);
		Test.stopTest();

		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(3, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), targetOne, true);
		assertTool_assertTargetIsCorrect(databaseTargets.get(1), targetTwo, true);
		assertTool_assertTargetIsCorrect(databaseTargets.get(2), targetThree, true);
	}
	
	@IsTest
	private static void SaveNutritionalTargets_UpdatedOldTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		//Create new targets
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetOne		= testTool_createOldTargetData(testCheckIn.Id, 'Target 1', 0, 0, 0, 'Monday;Tuesday');
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetTwo		= testTool_createOldTargetData(testCheckIn.Id, 'Target 2', 0, 0, 0, 'Wednesday;Thursday;Friday');
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetThree	= testTool_createOldTargetData(testCheckIn.Id, 'Target 3', 0, 0, 0, 'Saturday;Sunday');

		//Update the values of the nutrition targets
		targetOne.protein	= 1;	targetOne.carbs		= 2;	targetOne.fat		= 3;
		targetTwo.protein	= 2;	targetTwo.carbs		= 3;	targetTwo.fat		= 4;
		targetThree.protein	= 3;	targetThree.carbs	= 4;	targetThree.fat		= 5;


		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionFlexiPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget[] { targetOne, targetTwo, targetThree }, null);
		Test.stopTest();

		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(3, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), targetOne, false);
		assertTool_assertTargetIsCorrect(databaseTargets.get(1), targetTwo, false);
		assertTool_assertTargetIsCorrect(databaseTargets.get(2), targetThree, false);
	}

	@IsTest
	private static void SaveNutritionalTargets_CreateNewTargetsAndUpdateOldTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget newTarget		= testTool_createNewTargetData(testCheckIn.Id, 'Target 1', 1, 2, 3, new String[]{ 'Monday', 'Tuesday' });
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget oldTarget		= testTool_createOldTargetData(testCheckIn.Id, 'Target 2', 0, 0, 0, 'Wednesday;Thursday;Friday;Saturday;Sunday');
		oldTarget.protein = 2; oldTarget.carbs = 4; oldTarget.fat = 6;
		
		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionFlexiPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget[] { newTarget, oldTarget }, null);
		Test.stopTest();
		
		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(2, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), oldTarget, false);
		assertTool_assertTargetIsCorrect(databaseTargets.get(1), newTarget, true);
	}
	
	@IsTest
	private static void SaveNutritionalTargets_CreateNewTargetsWithNoCheckInId() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget newTarget		= testTool_createNewTargetData(null, 'Target 1', 1, 2, 3, new String[]{ 'Monday', 'Tuesday' });
		
		Boolean exceptionCaught = false;
		Test.startTest();
		try{	        
			AT_LWCC_PlannerNutritionFlexiPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget[] { newTarget }, null);
		} catch (AT_LWCC_PlannerNutritionFlexiPlans.MissingFieldException e){
			exceptionCaught = true;
		}
		Test.stopTest();

		System.assert(exceptionCaught);
	}

	@IsTest
	private static void SaveNutritionalTargets_UpdateOldTargetsAndDeleteTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		//Create new targets
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetOne		= testTool_createOldTargetData(testCheckIn.Id, 'Target 1', 0, 0, 0, 'Monday;Tuesday');
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetTwo		= testTool_createOldTargetData(testCheckIn.Id, 'Target 2', 0, 0, 0, 'Wednesday;Thursday;Friday');
		AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget targetThree	= testTool_createOldTargetData(testCheckIn.Id, 'Target 3', 0, 0, 0, 'Saturday;Sunday');

		//Update the values of the nutrition targets
		targetOne.protein	= 1;	targetOne.carbs		= 2;	targetOne.fat		= 3;


		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionFlexiPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionFlexiPlans.NutritionalTarget[] { targetOne }, new String[] { targetTwo.id, targetThree.id });
		Test.stopTest();

		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(1, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), targetOne, false);
	}

}