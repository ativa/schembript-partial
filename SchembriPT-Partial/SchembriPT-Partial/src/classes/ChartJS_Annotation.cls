public class ChartJS_Annotation  {

	public String id { get; set; }
	
	public String drawTime { get; set; }

	public String type { get; set; }

	public String xScaleID { get; set; }

	public String yScaleID { get; set; }

	public Integer xMin { get; set; }

	public Integer xMax { get; set; }

	public String backgroundColor { get; set; }

	public String borderColor { get; set; }
	
	/**
	* @description Data value to draw the line at
	*/ 
	public Object value { get; set; }

	/**
	* @description Optional value at which the line draw should end
	*/ 
	public Object endValue { get; set; }

	public String mode { get; set; }

	public String scaleID { get; set; }

	public Integer borderWidth { get; set; }

	public ChartJS_AnnotationLabel label { get; set; }


}