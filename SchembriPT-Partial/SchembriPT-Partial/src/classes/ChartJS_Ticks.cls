public class ChartJS_Ticks {

	/**
	* @description Does the start of the chart start at 0?
	*/ 
	public Boolean beginAtZero { get; set; }

	public Integer fontSize { get; set; }

	public Integer stepSize { get; set; }

	public Boolean autoSkip { get; set; }

	public Integer maxRotation { get; set; }

	/**
	* @description The Minimum value displayed on the axes
	*/ 
	public Decimal min { get; set; }

	/**
	* @description The Maximum value displayed on the axes
	*/ 
	public Decimal max { get; set; }
	
}