/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-22-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-20-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class AT_F_UpdatePlanAveragesAndSummary {

    
    public class Request {
        
        @InvocableVariable(label='Check-In Id' required=true)
        public String checkInId;

    }

    @InvocableMethod(label='Update Plan Averages' description='Update Plan Averages' category='Ativa - Apex Flow Action')
    public static void UpdatePlanAndAverages(List<Request> reqs) {
        
        Set<Id> checkInIds = new Set<Id>();
        for(Request req : reqs) {
            checkInIds.add(req.checkInId);
        }

        List<Plan__c> referencePlans = [
            SELECT 
                Id,
                Name,
                Check_In__c,
                Weekdays__c,
                Trainer_Plan_Carbs__c,
                Trainer_Plan_Fat__c,
                Trainer_Plan_Protein__c,
                Trainer_Plan_Calories__c 
            FROM Plan__c
            WHERE Check_In__c IN :checkInIds AND RecordType.DeveloperName = 'Nutrition_Reference_Plan'
        ];

        Plan_Trigger_Tools.updatePlansOnSameCheckIn(referencePlans);

    }

}