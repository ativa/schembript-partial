@IsTest
public without sharing class AT_LWCC_PlannerAddTemplateItem_T  {

	private static RecordType testTool_getTemplateItemRecordType(String developerName) {
		return [SELECT Id FROM RecordType WHERE DeveloperName = :developerName AND SobjectType = 'Template_Item__c'];
	}

	private static List<String> convertMultiSelectToList(String multiSelect) {
		if(String.isEmpty(multiSelect)) {
			return new List<String>();
		}
		return multiSelect.split(';');
	}

	private static Template_Item__c testTool_createTemplateItem(String name, Decimal quantity, String measurement, Decimal protein, Decimal carbs, Decimal fat, Decimal quantityIncrement, Decimal minimumQuantity, String allergies, String restrictions, String recordTypeId) {

		Template_Item__c newTmpItem			= new Template_Item__c();
		if(recordTypeId != null) {
			newTmpItem.RecordTypeId = recordTypeId;
		}
		newTmpItem.Name						= name;
		newTmpItem.Item_Quantity__c			= quantity;
		newTmpItem.Measurement__c			= measurement;
		newTmpItem.Protein__c				= protein;
		newTmpItem.Carbs__c					= carbs;
		newTmpItem.Fat__c					= fat;
		newTmpItem.Quantity_Increment__c	= quantityIncrement;
		newTmpItem.Minimum_Quantity__c		= minimumQuantity;
		newTmpItem.Allergy__c				= allergies;
		newTmpItem.Restriction__c			= restrictions;
		insert newTmpItem;
		return newTmpItem;
	}

	private static void assertTool_assertTemplateItem(Id tmpItemId, AT_LWCC_PlannerAddTemplateItem.TemplateItem tmpItem2) {

		Template_Item__c tmpItem = [
			SELECT 
				Id,
				Name,
				Item_Quantity__c,
				Measurement__c,
				Protein__c,
				Carbs__c,
				Fat__c,
				Calories__c,
				Quantity_Increment__c,
				Minimum_Quantity__c,
				Allergy__c,
				Restriction__c
			FROM Template_Item__c 
			WHERE Id = :tmpItemId
		];

		System.assertEquals(tmpItem.Id,										tmpItem2.itemId);
		System.assertEquals(tmpItem.Name,										tmpItem2.itemName);
		System.assertEquals(tmpItem.Item_Quantity__c,							tmpItem2.itemQuantity);
		System.assertEquals(tmpItem.Measurement__c,							tmpItem2.itemMeasurement);
		System.assertEquals(tmpItem.Protein__c,								tmpItem2.itemProtein);
		System.assertEquals(tmpItem.Carbs__c,									tmpItem2.itemCarbs);
		System.assertEquals(tmpItem.Fat__c,									tmpItem2.itemFat);
		System.assertEquals(tmpItem.Calories__c,								tmpItem2.itemCalories);
		System.assertEquals(tmpItem.Quantity_Increment__c,					tmpItem2.quantityIncrement);
		System.assertEquals(tmpItem.Minimum_Quantity__c,						tmpItem2.minimumQuantity);
		
		List<String> tItemAllergies		= convertMultiSelectToList(tmpItem.Allergy__c);
		System.assertEquals(tItemAllergies.size(), tmpItem2.allergies.size());
		for(Integer i = 0; i < tItemAllergies.size(); i++) {
			System.assertEquals(tItemAllergies.get(i), tmpItem2.allergies.get(i));
		}

		List<String> tItemRestrictions	= convertMultiSelectToList(tmpItem.Restriction__c);
		System.assertEquals(tItemRestrictions.size(), tmpItem2.dietRestrictions.size());
		for(Integer i = 0; i < tItemRestrictions.size(); i++) {
			System.assertEquals(tItemRestrictions.get(i), tmpItem2.dietRestrictions.get(i));
		}

	}

	private static void assertTool_assertPicklistValues(String objName, String fieldName, List<AT_LWCC_PlannerAddTemplateItem.KeyValuePair> kvPairs) {
	
		Schema.SObjectType s = Schema.getGlobalDescribe().get(objName) ;
		Schema.DescribeSObjectResult r = s.getDescribe() ;
		Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
		Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();

		System.assertEquals(fieldResult.getPicklistValues().size(), kvPairs.size());
		Integer index = 0;
		for(Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()){
			System.assertEquals(pickListVal.getLabel(), kvPairs.get(index).label);
			System.assertEquals(pickListVal.getValue(), kvPairs.get(index).value);
			index += 1;
		}
	}

	@IsTest
	public static void GetNutritionalTemplateItemInformation_GetNutritionalTemplateItems() {
		RecordType recType			= testTool_getTemplateItemRecordType('Nutrition');
		Template_Item__c tstTmp1	= testTool_createTemplateItem('Test 1', 1, 'Slice', 1, 2, 3, 4, 5, 'Lactose;Gluten;Shellfish', 'Vegan;Vegetarian', recType.Id);

		Test.startTest();
		AT_LWCC_PlannerAddTemplateItem.TemplateInformation tmpInformation = AT_LWCC_PlannerAddTemplateItem.GetNutritionalTemplateItemInformation();
		Test.stopTest();

		System.assertEquals(1, tmpInformation.allTemplateItems.size());
		assertTool_assertTemplateItem(tstTmp1.Id, tmpInformation.allTemplateItems.get(0));
		assertTool_assertPicklistValues('Template_Item__c','Allergy__c', tmpInformation.allAllergies);
		assertTool_assertPicklistValues('Template_Item__c','Restriction__c', tmpInformation.allDietRestrictions);

	}
	
	@IsTest
	public static void GetNutritionalTemplateItemInformation_GetNutritionalTemplateItemsWithNoAllergiesOrRestrictions() {
		RecordType recType			= testTool_getTemplateItemRecordType('Nutrition');
		Template_Item__c tstTmp1	= testTool_createTemplateItem('Test 1', 1, 'Slice', 1, 2, 3, 4, 5, null, null, recType.Id);

		Test.startTest();
		AT_LWCC_PlannerAddTemplateItem.TemplateInformation tmpInformation = AT_LWCC_PlannerAddTemplateItem.GetNutritionalTemplateItemInformation();
		Test.stopTest();

		System.assertEquals(1, tmpInformation.allTemplateItems.size());
		assertTool_assertTemplateItem(tstTmp1.Id, tmpInformation.allTemplateItems.get(0));
		assertTool_assertPicklistValues('Template_Item__c','Allergy__c', tmpInformation.allAllergies);
		assertTool_assertPicklistValues('Template_Item__c','Restriction__c', tmpInformation.allDietRestrictions);

	}

	@IsTest
	public static void GetNutritionalTemplateItemInformation_GetNoNutritionalTemplateItems() {

		Test.startTest();
		AT_LWCC_PlannerAddTemplateItem.TemplateInformation tmpInformation = AT_LWCC_PlannerAddTemplateItem.GetNutritionalTemplateItemInformation();
		Test.stopTest();

		System.assertEquals(0, tmpInformation.allTemplateItems.size());
		assertTool_assertPicklistValues('Template_Item__c','Allergy__c', tmpInformation.allAllergies);
		assertTool_assertPicklistValues('Template_Item__c','Restriction__c', tmpInformation.allDietRestrictions);

	}

	@IsTest
	public static void GetNutritionalTemplateItemInformation_GetTrainingTemplateItems() {
		RecordType recType			= testTool_getTemplateItemRecordType('Training');
		Template_Item__c tstTmp1	= testTool_createTemplateItem('Test 1', 1, 'Slice', 1, 2, 3, 4, 5, 'Lactose;Gluten;Shellfish', 'Vegan;Vegetarian', recType.Id);

		Test.startTest();
		AT_LWCC_PlannerAddTemplateItem.TemplateInformation tmpInformation = AT_LWCC_PlannerAddTemplateItem.GetNutritionalTemplateItemInformation();
		Test.stopTest();

		System.assertEquals(0, tmpInformation.allTemplateItems.size());
		assertTool_assertPicklistValues('Template_Item__c','Allergy__c', tmpInformation.allAllergies);
		assertTool_assertPicklistValues('Template_Item__c','Restriction__c', tmpInformation.allDietRestrictions);
	}

}