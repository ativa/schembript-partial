public class Survey_Trigger_Tools  {

	/**
	* @author Harrison Campbell
	* @description Throws an exception if more than one survey is assigned to a CheckIn
	*/ 
	/*
	public class OneSurveyToCheckInException extends Exception { }
	*/

	/**
	* @description Get all of the related checkins for the surveys
	* @param surveys The Survey Salesforce Objects
	* @return Returns the related Check-Ins
	*/
	/*
	private static Map<Id,Check_In__c> getRelatedCheckIns(List<Survey__c> surveys) {

		Set<Id> checkInIds = new Set<Id>();
		for(Survey__c surv : surveys) {
			checkInIds.add(surv.Check_In__c);
		}


		Map<Id,Check_In__c> checksMap = new Map<Id, Check_In__c>([
			SELECT 
				Id,
				Are_you_currently_taking_medication__c,
				Body_Fat_Reduction__c,
				bod__c,
				Post_Injury_Rehab_Recovery__c,
				Body_Transformation__c,
				Improve_Overall_Lifestyle_and_Fitness__c,
				Implement_Regular_Physical_Training__c,
				Increase_Lean_Muscle_Mass__c,
				Improve_Cardiovascular_Fitness__c,
				Bodybuilding_Competition__c,
				Increase_Strength__c,
				Learn_to_Manage_Training_Hours__c,
				Have_you_ever_cheated_on_your_diet__c,
				Future_goals__c,
				Have_you_been_hitting_the_set_macros_cal__c,
				Would_you_like_to_do_the_next_phase__c,
				How_do_you_relax__c,
				What_affected_your_meal_plan_consistency__c,
				How_are_you_feeling__c,
				What_stopped_you_from_performing_cardio__c,
				What_can_you_improve_on__c,
				How_challenging_was_the_PT_program_1_10__c,
				Do_you_get_hungry_after_your_last_meal__c,
				What_affected_your_completion_of_HIIT__c,
				What_were_you_most_fearful_of__c,
				What_made_you_start_this_program__c,
				What_interrupted_your_plans__c,
				Your_biggest_motivator__c,
				Who_was_your_biggest_supporter__c,
				How_many_sessions_did_you_do__c,
				How_challenging_was_the_weight_plan__c,
				What_was_challenging__c,
				What_was_easy__c,
				How_would_you_rate_the_program__c,
				How_could_we_improve__c,
				Do_we_have_good_value_for_money__c,
				X3_words_to_describe_Schembri_PT__c,
				Interested_in_a_referral_program__c,
				Any_family_or_friends_you_could_refer__c,
				(SELECT Id FROM Surveys__r)
			FROM Check_In__c 
			WHERE Id IN :checkInIds
		]);

		return checksMap;

	}
	*/
	/**
	* @description Map the Check-In object to the Survey Object
	* @param check The Check-In object
	* @param surv The survey object
	*/ 
	/*
	private static void mapCheckInFieldsToSurvey(Check_In__c check, Survey__c surv) {
		check.Body_Fat_Reduction__c							= surv.Body_Fat_Reduction__c;
		check.bod__c										= surv.Body_Recomposition__c;
		check.Post_Injury_Rehab_Recovery__c					= surv.Post_Injury_Rehab_Recovery__c;
		check.Body_Transformation__c						= surv.Body_Transformation__c;
		check.Improve_Overall_Lifestyle_and_Fitness__c		= surv.Improve_Overall_Lifestyle_and_Fitness__c;
		check.Implement_Regular_Physical_Training__c		= surv.Implement_Regular_Physical_Training__c;
		check.Increase_Lean_Muscle_Mass__c					= surv.Increase_Lean_Muscle_Mass__c;
		check.Improve_Cardiovascular_Fitness__c				= surv.Improve_Cardiovascular_Fitness__c;
		check.Bodybuilding_Competition__c					= surv.Bodybuilding_Competition__c;
		check.Increase_Strength__c							= surv.Increase_Strength__c;
		check.Learn_to_Manage_Training_Hours__c				= surv.Learn_to_Manage_Training_Hours__c;
		check.Have_you_ever_cheated_on_your_diet__c			= surv.Have_you_ever_cheated_on_your_diet__c;

		check.Future_goals__c								= surv.Future_Goals__c;
		check.Have_you_been_hitting_the_set_macros_cal__c	= surv.Have_you_been_hitting_the_set_macros_cal__c;
		check.Would_you_like_to_do_the_next_phase__c		= surv.Would_you_like_to_do_the_next_phase__c;
		check.How_do_you_relax__c							= surv.How_do_you_relax__c;
		check.What_affected_your_meal_plan_consistency__c	= surv.What_affected_your_meal_plan_consistency__c;
		check.How_are_you_feeling__c						= surv.How_are_you_feeling__c;
		check.What_stopped_you_from_performing_cardio__c	= surv.What_stopped_you_from_performing_cardio__c;
		check.What_can_you_improve_on__c					= surv.What_can_you_improve_on__c;
		check.How_challenging_was_the_PT_program_1_10__c	= surv.How_challenging_was_the_PT_program_1_10__c;
		check.Do_you_get_hungry_after_your_last_meal__c		= surv.Do_you_get_hungry_after_your_last_meal__c;
		check.What_interrupted_your_plans__c				= surv.What_interrupted_your_plans__c;
		check.What_affected_your_completion_of_HIIT__c		= surv.What_affected_your_completion_of_HIIT__c;
		check.What_were_you_most_fearful_of__c				= surv.What_were_you_most_fearful_of__c;
		check.What_made_you_start_this_program__c			= surv.What_made_you_start_this_program__c;
		check.Your_biggest_motivator__c						= surv.Your_biggest_motivator__c;
		check.Who_was_your_biggest_supporter__c				= surv.Who_was_your_biggest_supporter__c;
		check.How_many_sessions_did_you_do__c				= surv.How_many_sessions_did_you_do__c;
		check.How_challenging_was_the_weight_plan__c		= surv.How_challenging_was_the_weight_plan__c;
		check.What_was_challenging__c						= surv.What_was_challenging__c;
		check.What_was_easy__c								= surv.What_was_easy__c;
		check.How_would_you_rate_the_program__c				= surv.How_would_you_rate_the_program__c;
		check.How_could_we_improve__c						= surv.How_could_we_improve__c;
		check.Do_we_have_good_value_for_money__c			= surv.Do_we_have_good_value_for_money__c;
		check.X3_words_to_describe_Schembri_PT__c			= surv.X3_words_to_describe_Schembri_PT__c;
		check.Interested_in_a_referral_program__c			= surv.Interested_in_a_referral_program__c;
		check.Any_family_or_friends_you_could_refer__c		= surv.Any_family_or_friends_you_could_refer__c;

		//
		check.Core_Activation_Achieved_pweek__c				= surv.Core_Activation_Achieved_pweek__c;
		check.Resistance_Achieved_pweek__c					= surv.Resistance_Achieved_pweek__c;
		check.HIIT_Achieved_pweek__c						= surv.HIIT_Achieved_pweek__c;
		check.Talk_Test_Cardio_Achieved_pweek__c			= surv.Talk_Test_Cardio_Achieved_pweek__c;
		check.Metabolic_Conditioning_Achieved_pweek__c		= surv.Metabolic_Conditioning_Achieved_pweek__c;
		check.Step_Count_Achieved_pday__c					= surv.Step_Count_Achieved_pday__c;
		check.Ab_Workout_Achieved_pweek__c					= surv.Ab_Workout_Achieved_pweek__c;
		check.General_Feedback_from_Check_In__c				= surv.General_Feedback_from_Check_In__c;

		//New Field
		check.Are_you_currently_taking_medication__c		= surv.Are_you_currently_taking_medication__c;
		check.General_Feedback_from_Check_In__c				= surv.General_Feedback_from_Check_In__c;

	}
	*/

	/**
	* @description Prepares the surveys to be inserted
	* @param surveys 
	*/
	/*
	public static void beforeInsert(List<Survey__c> surveys) {

		Map<Id, Check_In__c> checkIns = getRelatedCheckIns(surveys);
		for(Survey__c surv : surveys) {
			if(checkIns.containsKey(surv.Check_In__c)) {
				mapCheckInFieldsToSurvey(checkIns.get(surv.Check_In__c), surv);
			}
		}
		update checkIns.values();

	}
	*/

	/**
	* @description Prepares the surveys to be updated
	* @param surveys 
	*/
	/*
	public static void beforeUpdate(List<Survey__c> surveys) {
	
		Map<Id, Check_In__c> checkIns = getRelatedCheckIns(surveys);
		for(Survey__c surv : surveys) {
			if(checkIns.containsKey(surv.Check_In__c)) {
				mapCheckInFieldsToSurvey(checkIns.get(surv.Check_In__c), surv);
			}
		}
		update checkIns.values();

	}
	*/

}