public class Dropbox_SharingAPI  {

	public static Dropbox_Settings__c settings = Dropbox_Settings__c.getInstance();

	private static final String API_DROPBOX_API = 'https://api.dropboxapi.com/2';

	private static final String CONTENT_DROPBOX_API = 'https://content.dropboxapi.com/2';

	public class CreateSharedLinkWithSettingsPacket {

		public String status;

		public Integer statusCode;

		public CreateSharedLinkWithSettingsResult result { get; set; }
		
		public CreateSharedLinkWithSettingsError error { get; set; }

	}

	public class CreateSharedLinkWithSettingsError_Error {

		//.tag goes here

		public CreateSharedLinkWithSettingsError_SharedLinkAlreadyExists shared_link_already_exists { get; set; }

	}
	
	public class CreateSharedLinkWithSettingsError_SharedLinkAlreadyExists {

		//.tag goes here

		public Metadata metadata { get; set; }

	}

	public class Metadata {

		public String url { get; set; }

	}

	public class CreateSharedLinkWithSettingsError {

		public String error_summary { get; set; }

		public CreateSharedLinkWithSettingsError_Error error { get; set; }

	}

	/**
	* @author Harrison Campbell
	* @description 
	*/ 
	public class CreateSharedLinkWithSettingsResult {
	
		//.tag goes here

		public String url { get; set; }

		public String name { get; set; }

		//Link permissions go here
		
		public String client_modified { get; set; }

		public String server_modified { get; set; }
		
		public String rev { get; set; }
		
		public Integer size { get; set; }
		
		public String id { get; set; }
		
		public String path_lower { get; set; }

		//team_member_info goes here

	}
	
	/**
	* @description Create shared link settings for a file on dropbox
	* @param path The Path of the File
	*/ 
	public static CreateSharedLinkWithSettingsPacket CreateSharedLinkWithSettings(String path) {

		HttpRequest request = new HttpRequest();
		request.setEndpoint(API_DROPBOX_API + '/sharing/create_shared_link_with_settings');
		request.setMethod('POST');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Content-Type', 'application/json');
		request.setBody('{"path": "' + path + '"}');
		HttpResponse response = (new Http()).send(request);
		System.debug(response.getStatus());
		System.debug(response.getStatusCode());
		System.debug(response.getBody());

		CreateSharedLinkWithSettingsPacket packet = new CreateSharedLinkWithSettingsPacket();
		packet.statusCode		= response.getStatusCode();
		packet.status			= response.getStatus();
		if(response.getStatusCode() == 200) {
			CreateSharedLinkWithSettingsResult uploadRes = (CreateSharedLinkWithSettingsResult)JSON.deserialize(response.getBody(), CreateSharedLinkWithSettingsResult.class);
			packet.result = uploadRes;
			System.debug(packet.result);
		} else {
			CreateSharedLinkWithSettingsError uploadErr = (CreateSharedLinkWithSettingsError)JSON.deserialize(response.getBody(), CreateSharedLinkWithSettingsError.class);
			packet.error = uploadErr;
		}
		
		return packet;
		
	}

	/**
	* @description Create shared link settings for a file on dropbox
	* @param path The Path of the File
	*/ 
	public static CreateSharedLinkWithSettingsResult CreateSharedLinkWithSettings(String path, String fileName) {

		HttpRequest request = new HttpRequest();
		request.setEndpoint(API_DROPBOX_API + '/sharing/create_shared_link_with_settings');
		request.setMethod('POST');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Content-Type', 'application/json');
		request.setBody('{"path": "' + path + '/' + fileName + '"}');
		HttpResponse response = (new Http()).send(request);
		System.debug(response.getStatus());
		System.debug(response.getStatusCode());
		System.debug(response.getBody());
		
		CreateSharedLinkWithSettingsResult uploadRes = (CreateSharedLinkWithSettingsResult)JSON.deserialize(response.getBody(), CreateSharedLinkWithSettingsResult.class);
		return uploadRes;
		
	}

	public static void GetSharedLinkFile(String url) {
		//Get Shared Link File
		HttpRequest request = new HttpRequest();
		request.setEndpoint(CONTENT_DROPBOX_API + '/sharing/get_shared_link_file');
		request.setMethod('POST');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Dropbox-API-Arg', '{"url": "' + url + '"}');
		HttpResponse response = (new Http()).send(request);
		System.debug(response.getStatus());
		System.debug(response.getStatusCode());
		System.debug(JSON.serializePretty(response.getBody()));
	}

}