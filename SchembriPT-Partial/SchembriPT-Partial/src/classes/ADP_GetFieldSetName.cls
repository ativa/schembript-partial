/**
* @author Harrison Campbell
* @description Used to display a picklist of Fieldsets for a specific object
*/ 
global class ADP_GetFieldSetName extends VisualEditor.DynamicPickList {

	VisualEditor.DesignTimePageContext context;


	global ADP_GetFieldSetName(VisualEditor.DesignTimePageContext context) {
		this.context = context;
	}

	global override VisualEditor.DataRow getDefaultValue() {
		List<Schema.FieldSet> fSets = getAllFieldSets(this.context.entityName);
		return new VisualEditor.DataRow(fSets.get(0).getLabel(), fSets.get(0).getName());
	}

	global override VisualEditor.DynamicPickListRows getValues() {
		VisualEditor.DynamicPickListRows dataRows = new VisualEditor.DynamicPickListRows();
		for(Schema.FieldSet fSet : getAllFieldSets(this.context.entityName)) {
			VisualEditor.DataRow newDataRow = new VisualEditor.DataRow(fSet.getLabel(), fSet.getName());
			dataRows.addRow(newDataRow);
		}
		return dataRows;
	}
	
	private List<Schema.FieldSet> getAllFieldSets(String objectName) {
		Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
		Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
		Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

		List<Schema.FieldSet> fieldSets = DescribeSObjectResultObj.FieldSets.getMap().values();
		return fieldSets;

	}

}