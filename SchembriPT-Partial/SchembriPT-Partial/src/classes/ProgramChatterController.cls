/**
* @author Harrison Campbell
* @description Controller for the ProgramChatter component
*/ 
public class ProgramChatterController {

	public class ProgramDetails {

		@AuraEnabled
		public String programId { get; set; }

	}

	/**
	* @description Get the Program Details based on the CheckIn
	* @param checkInId The Id of the Checkin
	* @return Returns the Program Details
	*/ 
	@AuraEnabled
	public static ProgramDetails GetProgramDetails(Id checkInId) {

		Check_In__c cCheckIn = [
			SELECT
				Id,
				Client_Program__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		ProgramDetails progDetails	= new ProgramDetails();
		progDetails.programId		= cCheckIn.Client_Program__c;
		return progDetails;

	}

}