/**
* @author Harrison Campbell
* @description All tools used for the Account Trigger
*/ 
public with sharing class Account_Trigger_Tools {

	/**
	* @description Create Injury Advisories for an Account
	* @param acc The Account
	* @return Returns Injury Advisories
	*/ 
	private static List<Advisory__c> createInjuriesAdvisories(Account acc, Id healthRecId) {

		List<Advisory__c> newAdvisories = new List<Advisory__c>();

		//If there are no injuries then return nothing
		if(acc.Any_Injuries__pc == null) {
			return newAdvisories;
		}
	

		String[] injuries = acc.Any_Injuries__pc.split(';');

		for(String injury : injuries) {
			
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= healthRecId;
			newAdvisory.Type__c			= 'Injuries';
			newAdvisory.Sub_Type__c		= injury;
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());

			//If this injury is other than set the description of the advisory to 
			if(injury.equals('Other')) {
				newAdvisory.Description__c = acc.If_selected_other_injury_please_specify__pc;
			}

			newAdvisories.add(newAdvisory);
		}

		return newAdvisories;

	}

	/**
	* @description Create Exercises I can't perform Advisories for an Account
	* @param acc The Account
	* @return Returns Exercises I can't perform Advisories
	*/
	private static List<Advisory__c> createExerciesCantPreformAdvisories(Account acc, Id trainingRecId) {
	
		List<Advisory__c> newAdvisories = new List<Advisory__c>();
		
		//If there are no exercises then return nothing
		if(acc.Any_Exercises_You_Can_t_Perform__pc == null) {
			return newAdvisories;
		}

		String[] exercises = acc.Any_Exercises_You_Can_t_Perform__pc.split(';');

		for(String exercise : exercises) {
			
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= trainingRecId;
			newAdvisory.Type__c			= 'Exercises I can\'t do';
			newAdvisory.Sub_Type__c		= exercise;
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			
			//If this injury is other than set the description of the advisory to 
			if(exercise.equals('Other')) {
				newAdvisory.Description__c = acc.If_other_selected_please_specify__pc;
			}

			newAdvisories.add(newAdvisory);
		}

		
		return newAdvisories;

	}

	/**
	* @description Create Illness Advisory for an Account
	* @param acc The Account
	* @return Returns Illness Advisory
	*/
	private static Advisory__c createIllAdvisory(Account acc, Id healthRecId) {
		
		Advisory__c newAdvisory		= new Advisory__c();
		newAdvisory.RecordTypeId	= healthRecId;
		newAdvisory.Type__c			= 'Medical';
		newAdvisory.Sub_Type__c		= 'Illness';
		newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
		newAdvisory.Client__c		= acc.Id;
		newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
		newAdvisory.Description__c	= acc.Specific_detail_of_any_injury_or_illness__pc;

		return newAdvisory;

	}

	/**
	* @description Create Vegan or Vegetarian Advisory for an Account
	* @param acc The Account
	* @return Returns Vegan/Vegetarian Advisory
	*/ 
	private static Advisory__c createVeganOrVegetarianAdvisory(Account acc, Id nutritionRecId) {
	
		Advisory__c newAdvisory		= new Advisory__c();
		newAdvisory.RecordTypeId	= nutritionRecId;
		newAdvisory.Type__c			= 'Dietary';
		newAdvisory.Sub_Type__c		= acc.Are_you_Vegetarian_or_Vegan__pc;
		newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
		newAdvisory.Client__c		= acc.Id;
		newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());

		return newAdvisory;

	}

	/**
	* @description Create Enjoyable Food Advisories for an Account
	* @param acc The Account
	* @return Returns Enjoyable Food Advisories
	*/ 
	private static List<Advisory__c> createEnjoyableFoodsAdvisory(Account acc, Id nutritionRecId) {
		
		List<Advisory__c> newAdvisories = new List<Advisory__c>();
		
		//If there are no foods you enjoy then return nothing
		if(acc.What_Foods_Do_You_Enjoy_Eating__pc == null) {
			return newAdvisories;
		}

		String[] enjoyableFoods = acc.What_Foods_Do_You_Enjoy_Eating__pc.split(';');

		for(String enjoyFood : enjoyableFoods) {
			Advisory__c newAdvisory			= new Advisory__c();
			newAdvisory.RecordTypeId		= nutritionRecId;
			newAdvisory.Type__c				= 'Likes';
			newAdvisory.Sub_Type__c			= enjoyFood;
			newAdvisory.Name				= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c			= acc.Id;
			newAdvisory.Start_Date__c		= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			
			//If this food is other than set the description of the advisory to 
			if(enjoyFood.equals('Other')) {
				newAdvisory.Description__c = acc.For_other_foods_you_enjoy_please_specify__pc;
			}

			newAdvisories.add(newAdvisory);
		}

		return newAdvisories;

	}

	/**
	* @description Create Disliked Food Advisories for an Account
	* @param acc The Account
	* @return Returns Disliked Food Advisories
	*/ 
	private static List<Advisory__c> createDislikedFoodAdvisories(Account acc, Id nutritionRecId) {
	
		List<Advisory__c> newAdvisories = new List<Advisory__c>();
		
		//If there are no foods you hate then return nothing
		if(acc.Are_there_any_foods_you_dislike__pc == null) {
			return newAdvisories;
		}

		String[] unlikedFoods = acc.Are_there_any_foods_you_dislike__pc.split(';');

		for(String hatedFood : unlikedFoods) {
			
			Advisory__c newAdvisory			= new Advisory__c();
			newAdvisory.RecordTypeId		= nutritionRecId;
			newAdvisory.Type__c				= 'Dislikes';
			newAdvisory.Sub_Type__c			= hatedFood;
			newAdvisory.Name				= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c			= acc.Id;
			newAdvisory.Start_Date__c		= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			
			//If this food is other than set the description of the advisory to 
			if(hatedFood.equals('Other')) {
				newAdvisory.Description__c = acc.For_other_foods_you_dislike_specify__pc;
			}

			newAdvisories.add(newAdvisory);

		}

		return newAdvisories;

	}
	
	/**
	* @description Create Comftable Exercise Advisories for an Account
	* @param acc The Account
	* @return Returns Comftable exercieses Advisories
	*/ 
	private static List<Advisory__c> createComftableExerciseAdvisories(Account acc, Id trainingRecId) {

		List<Advisory__c> newAdvisories = new List<Advisory__c>();
		
		//If there are no exercises your comftable performing then return nothing
		if(acc.What_Exercises_are_you_Comforatble_Perfo__pc == null) {
			return newAdvisories;
		}

		String[] exercises = acc.What_Exercises_are_you_Comforatble_Perfo__pc.split(';');

		for(String exercise : exercises) {
			
			Advisory__c newAdvisory = new Advisory__c();

			newAdvisory.RecordTypeId	= trainingRecId;
			newAdvisory.Type__c			= 'Exercises I can do';
			newAdvisory.Sub_Type__c		= exercise;
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());

			if(exercise.equals('Other')) {
				newAdvisory.Description__c = acc.Other_exercises_you_re_comfortable_doing__pc;
			}

			newAdvisories.add(newAdvisory);

		}

		return newAdvisories;

	}

	/**
	* @description Create Food Allergy Advisories for an Account
	* @param acc The Account
	* @return Returns Food Allergy Advisories
	*/ 
	private static List<Advisory__c> createFoodAllergiesAdvisories(Account acc, Id nutritionRecId) {
	
		String allergiesType = 'Allergies';

		List<Advisory__c> newAdvisories = new List<Advisory__c>();

		if(acc.Lactose_intolerant__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Lactose';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}
			
		if(acc.Gluten_Free__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Gluten';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}

		if(acc.Fish_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Fish';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}

		if(acc.Shellfish_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Shellfish';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}

		if(acc.Nut_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Nut';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);

			System.debug(newAdvisory);
		}

		if(acc.Corn_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Corn';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}

		if(acc.Dairy_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Dairy';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);

		}

		if(acc.Egg_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Eggs';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);

		}

		if(acc.Peanut_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Peanut';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);

		}
			
		if(acc.Meat_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Meat';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}
			
		if(acc.Soy_Allergy__pc == true) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Soy';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisories.add(newAdvisory);
		}


		//Bug here look into
		if(acc.Other_food_allergies_intollerances__pc != null) {
			Advisory__c newAdvisory		= new Advisory__c();
			newAdvisory.RecordTypeId	= nutritionRecId;
			newAdvisory.Type__c			= allergiesType;
			newAdvisory.Sub_Type__c		= 'Other';
			newAdvisory.Name			= newAdvisory.Type__c + ' ' + newAdvisory.Sub_Type__c;
			newAdvisory.Client__c		= acc.Id;
			newAdvisory.Start_Date__c	= Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
			newAdvisory.Description__c	= acc.Other_food_allergies_intollerances__pc;
			newAdvisories.add(newAdvisory);
		}

		return newAdvisories;

	}

	/**
	* @description Create Advisories for a list of accounts
	* @param accounts List of Accounts
	*/ 
	public static void createAdvisories(List<Account> accounts) {
	
		List<Advisory__c> newAdvisories = new List<Advisory__c>();

		Id healthRecordId		= null;
		Id trainingRecordId		= null;
		Id nutritionRecordId	= null;

		List<String> devNames		= new String[]{'Health', 'Training', 'Nutrition'};
		List<RecordType> recTypes	= [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN :devNames AND SobjectType = 'Advisory__c'];

		for(RecordType rType : recTypes) {
			if(rType.DeveloperName == 'Health') {
				healthRecordId = rType.Id;
			}
			if(rType.DeveloperName == 'Training') {
				trainingRecordId = rType.Id;
			}
			if(rType.DeveloperName == 'Nutrition') {
				nutritionRecordId = rType.Id;
			}

		}

		for(Account acc : accounts) {
			newAdvisories.addAll(createInjuriesAdvisories(acc, healthRecordId));
			newAdvisories.addAll(createExerciesCantPreformAdvisories(acc, trainingRecordId));
			if(acc.Have_you_been_ill_recently__pc == 'Yes') {
				newAdvisories.add(createIllAdvisory(acc, healthRecordId));
			}
			if(acc.Are_you_Vegetarian_or_Vegan__pc != null && acc.Are_you_Vegetarian_or_Vegan__pc != 'No' ) {
				newAdvisories.add(createVeganOrVegetarianAdvisory(acc, nutritionRecordId));
			}
			newAdvisories.addAll(createEnjoyableFoodsAdvisory(acc, nutritionRecordId));
			newAdvisories.addAll(createDislikedFoodAdvisories(acc, nutritionRecordId));
			newAdvisories.addAll(createComftableExerciseAdvisories(acc, trainingRecordId));

			newAdvisories.addAll(createFoodAllergiesAdvisories(acc, nutritionRecordId));

		}

		insert newAdvisories;

	}

	private static List<Account> getWooCommerceAccounts(List<Account> accounts) {

		List<Account> accs = new List<Account>();
		
		List<String> validSubStatuses = new String[] {
			'Active',
			'active',
			'On Hold',
			'On-Hold',
			'on hold',
			'on-hold'
		};
		
		for(Account acc : accounts) {
			if(validSubStatuses.contains(acc.Subscription_Status__pc)) {
				accs.add(acc);
			}
		}

		return accs;

	}

	private static Map<Id, Case> getRelatedActiveCases(List<Account> accounts) {

		//Get the Ids of the accounts
		Set<Id> accIds = new Set<Id>();
		for(Account acc : accounts) {
			accIds.add(acc.Id);
		}

		List<String> validSubStatuses = new String[] {
			'Active',
			'active',
			'On Hold',
			'On-Hold',
			'on hold',
			'on-hold'
		};

		//Get all of the cases that are active or on hold, and related to the accounts
		List<Case> cses = [
			SELECT 
				Id,
				AccountId,
				Account.Subscription_Status__pc,
				Status,
				Date_Paused__c
			FROM Case 
			WHERE (Account.Subscription_Status__pc IN :validSubStatuses) AND AccountId IN :accIds
		];

		Map<Id, Case> caseMap = new Map<Id, Case>();
		for(Case c : cses) { 
			caseMap.put(c.AccountId, c);
		}
		return caseMap;
	}

    @future
    public static void createPortalUsers(Id accountID) {
        Portal_Settings__c settings = Portal_Settings__c.getInstance();
    
        UserRole role   = [SELECT Id FROM UserRole WHERE DeveloperName = :settings.User_Role__c];
        Profile profile = [SELECT Id FROM Profile WHERE Name = :settings.User_Profile__c];
        
        Account acc = [SELECT Name, FirstName, LastName, PersonMobilePhone, PersonEmail, PersonContactId, Private_Group_ID__c FROM Account WHERE Id = :accountID];
        
        User member = new User();
                
        member.FirstName = acc.FirstName;
        member.LastName = acc.LastName;
        member.Alias = acc.FirstName.left(1) + acc.LastName.left(7);
        member.CommunityNickname = (acc.FirstName + acc.LastName).left(40);
        member.ContactId = acc.PersonContactId;
        member.Email = acc.PersonEmail;
        member.MobilePhone = acc.PersonMobilePhone;
        member.Username = acc.PersonEmail;
        member.ProfileId = profile.Id;
        member.UserRoleId = role.Id;
        member.TimeZoneSidKey = 'Australia/Sydney';
        member.LocaleSidKey = 'en_AU';
        member.EmailEncodingKey = 'ISO-8859-1';
        member.LanguageLocaleKey  = 'en_US';
        
        insert member;
    }

	/**
	* @description Handles when the Accounts are inserted in the after phase
	* @param accounts The Accounts that are being inserted, assums Account's have an Id
	*/ 
	public static void afterInsert(List<Account> accounts) {
        Map<String,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
        
        Id clientTypeID = rtMap.get('Client').getRecordTypeId();
        
        for (Account acc: accounts) {
            if ((acc.RecordTypeId == clientTypeID) && String.isNotBlank(acc.Subscription_Status__pc) && (acc.Subscription_Status__pc.toLowerCase() == 'active')) {
                createPortalUsers(acc.Id);
            }
        }
        
        //createAdvisories(accounts);
	}

}