/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-28-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-28-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@IsTest
public with sharing class ActivityTimeline_Controller_Test {
    
    @IsTest
    public static void getCaseId_FunctionalTest() {

        Contact newContact = new Contact();
        newContact.LastName = 'Test';
        insert newContact;

        Case newCase = new Case();
        insert newCase;

        Check_In__c newCheckIn          = new Check_In__c();
        newCheckIn.h_Test_Record__c     = true;
        newCheckIn.Client_Program__c    = newCase.Id;
        newCheckIn.Contact__c           = newContact.Id;
        insert newCheckIn;

        Test.startTest();
        ActivityTimeline_Controller.getCaseId(newCheckIn.Id);
        Test.stopTest();

    }
    
    @IsTest
    public static void getActivityTimeline_FunctionalTest() {
        
        Contact newContact = new Contact();
        newContact.LastName = 'Test';
        insert newContact;

        Case newCase = new Case();
        insert newCase;

        Check_In__c newCheckIn          = new Check_In__c();
        newCheckIn.h_Test_Record__c     = true;
        newCheckIn.Client_Program__c    = newCase.Id;
        newCheckIn.Contact__c           = newContact.Id;
        insert newCheckIn;

        Task newCallTask = new Task();
        newCallTask.Type                = 'Call';
        newCallTask.Subject             = '1';
        newCallTask.Description         = 'Desc';
        newCallTask.ActivityDate        = Date.today();
        newCallTask.WhoId               = newContact.Id;
        newCallTask.WhatId              = newCheckIn.Id;
        insert newCallTask;

        Task newMeetingTask = new Task();
        newMeetingTask.Type             = 'Meeting';
        newMeetingTask.Subject             = '1';
        newMeetingTask.Description         = 'Desc';
        newMeetingTask.ActivityDate        = Date.today();
        newMeetingTask.WhoId               = newContact.Id;
        newMeetingTask.WhatId              = newCheckIn.Id;
        insert newMeetingTask;

        Test.startTest();
        ActivityTimeline_Controller.getActivityTimeline(newCheckIn.Id);
        Test.stopTest();




    }
    
}