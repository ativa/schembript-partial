/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class CommunitiesLoginController {
    private static final Portal_Settings__c settings = Portal_Settings__c.getInstance();

    global CommunitiesLoginController () {}
    
    // Code we will invoke on page load.
    global PageReference forwardToAuthPage() {
    	//String startUrl = System.currentPageReference().getParameters().get('startURL');
    	
        System.currentPageReference().getParameters().put('startURL', settings.Portal_URL__c + '/apex/CommunitiesRedirect');
    	String startUrl = settings.Portal_URL__c + '/apex/CommunitiesRedirect';
    	String displayType = System.currentPageReference().getParameters().get('display');
        return Network.forwardToAuthPage(startUrl, displayType);
    }
}