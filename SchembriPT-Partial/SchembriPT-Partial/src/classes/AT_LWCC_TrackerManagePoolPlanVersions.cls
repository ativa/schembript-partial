/**
* @author Harrison Campbell
* @description The lightning component for 'trackerXManagePoolPlanVersions'
*/ 
public without sharing class AT_LWCC_TrackerManagePoolPlanVersions {

    public class PoolPlanVersion {

        @AuraEnabled
        public String id { get; set; }

        @AuraEnabled
        public String poolPlanId { get; set; }

        @AuraEnabled
        public Integer versionNumber { get; set; }

        @AuraEnabled
        public Boolean activeVersion { get; set; }

        @AuraEnabled
        public String planId { get; set; }

		@AuraEnabled
		public String planName { get; set; }

		@AuraEnabled
		public Datetime lastUpdatedDateTime { get; set; }

		@AuraEnabled
		public String clientComments { get; set; }

		@AuraEnabled
		public List<Id> targetIds { get; set; }

        public PoolPlanVersion(Pool_Plan_Version__c poolPlanVersion, List<Id> targetIds) {
            this.id						= poolPlanVersion.Id;
            this.planId					= poolPlanVersion.Reference_Plan__c;
			this.planName				= poolPlanVersion.Reference_Plan__r.Name;
            this.poolPlanId				= poolPlanVersion.Pool_Plan__c;
            this.versionNumber			= poolPlanVersion.Version__c.intValue();
            this.activeVersion			= poolPlanVersion.Active_Version__c;
			this.lastUpdatedDateTime	= poolPlanVersion.LastModifiedDate;
			this.clientComments			= poolPlanVersion.Client_Comments__c;
			this.targetIds				= targetIds;
        }

    }

	@AuraEnabled
	public static void UpdateClientComments(Id poolPlanVersionId, String clientComments) {
		Pool_Plan_Version__c ppv = [SELECT Id, Client_Comments__c FROM Pool_Plan_Version__c WHERE Id = :poolPlanVersionId];
		ppv.Client_Comments__c = clientComments;
		update ppv;
	}

    @AuraEnabled
    public static List<PoolPlanVersion> GetAllVersions(String poolPlanId) {

        Pool_Plan__c poolPlan = [
            SELECT 
                Id,
                (
                    SELECT 
                        Id,
                        Reference_Plan__c,
						Reference_Plan__r.Name,
                        Pool_Plan__c,
						Pool_Plan__r.Targets__c,
                        Version__c,
                        Active_Version__c,
						LastModifiedDate,
						Client_Comments__c
                    FROM Pool_Plan_Versions__r
                    WHERE Version__c != null
                    ORDER BY Version__c DESC
                ),
				(
					SELECT
						Id,
						Targets__c
					FROM Target_Pool_Plans__r
				)
            FROM Pool_Plan__c
            WHERE Id = :poolPlanId
        ];

        List<PoolPlanVersion> allVersions = new List<PoolPlanVersion>();

		List<Id> targetIds =  new List<Id>();
		for(Targets_Pool_Plan__c tpp : poolPlan.Target_Pool_Plans__r) {
			targetIds.add(tpp.Targets__c);
		}
		

        for(Pool_Plan_Version__c planVersion : poolPlan.Pool_Plan_Versions__r) {
            allVersions.add(new PoolPlanVersion(planVersion, targetIds));
        }
        return allVersions;

    }

    /**
    * @description Make a Pool Plan Version the active version
    * @param poolPlanVersionId The Id of the Pool Plan
    */ 
    @AuraEnabled
    public static void MakeActiveVersion(string poolPlanVersionId) {

        Pool_Plan_Version__c selectedVersion = [
            SELECT
                Id,
                Pool_Plan__c
            FROM Pool_Plan_Version__c
            WHERE Id = :poolPlanVersionId
        ];

        Pool_Plan__c poolPlan = [
            SELECT
                Id,
				(
					SELECT
						Id,
						Active_Version__c
					FROM Pool_Plan_Versions__r
				)
            FROM Pool_Plan__c
            WHERE Id = :selectedVersion.Pool_Plan__c
        ];

        for(Pool_Plan_Version__c pVersion : poolPlan.Pool_Plan_Versions__r) {
            pVersion.Active_Version__c = pVersion.Id == poolPlanVersionId;    
        }

        update poolPlan.Pool_Plan_Versions__r;

    }


	/**
	* @description Delete a Version
	* @param poolPlanVersionId The Id of the Pool Plan Version
	*/ 
	@AuraEnabled
	public static void DeleteVersion(String poolPlanVersionId) {
	
		//Delete the version and the current plan
        Pool_Plan_Version__c selectedVersion = [
            SELECT
                Id,
				Version__c,
				Active_Version__c,
                Pool_Plan__c,
				Reference_Plan__c
            FROM Pool_Plan_Version__c
            WHERE Id = :poolPlanVersionId
        ];
		Plan__c refPlan = new Plan__c(Id = selectedVersion.Reference_Plan__c);

		Id poolPlanId					= selectedVersion.Pool_Plan__c;
		Boolean isVersionDeletedActive	= selectedVersion.Active_Version__c;

		delete selectedVersion;
		delete refPlan;

		//If the version was the active version then activate the latest version
		if(isVersionDeletedActive == true) {
			Pool_Plan_Version__c latestVersion = [
				SELECT
					Id,
					Version__c,
					Active_Version__c,
					Pool_Plan__c,
					Reference_Plan__c
				FROM Pool_Plan_Version__c
				WHERE Pool_Plan__c = :poolPlanId
				ORDER BY Version__c DESC
			];
			latestVersion.Active_Version__c = true;
			update latestVersion;

		}

	}

	@AuraEnabled
	public static void SaveAsNewVersion(String poolPlanVersionId, String newPlanName) {

		Pool_Plan_Version__c ppVersion = [
			SELECT
				Id,
				Reference_Plan__c,
				Reference_Plan__r.Account__c,
				Reference_Plan__r.Check_In__c,
				Reference_Plan__r.RecordTypeId,
				Pool_Plan__c
			FROM Pool_Plan_Version__c
			WHERE Id = :poolPlanVersionId
		];

		//Query the parent pool plan, and all of the versions 
		Pool_Plan__c pPlan = [
			SELECT
				Id,
				(
					SELECT 
						Id,
						Version__c
					FROM Pool_Plan_Versions__r
					WHERE Version__c != null
					ORDER BY Version__c DESC 
				)
			FROM Pool_Plan__c
			WHERE Id = :ppVersion.Pool_Plan__c
		];


		//Create the new Plan, and query it
		Id newPlanId = AT_M_Nutrition.SaveAsNewPlan(ppVersion.Reference_Plan__c, ppVersion.Reference_Plan__r.RecordTypeId).Id;
		Plan__c newPlan = [
			SELECT 
				Id,
				Name,
				Account__c,
				Check_In__c
			FROM Plan__c
			WHERE Id = :newPlanId
		];
		newPlan.Name							= newPlanName;
		newPlan.Account__c						= ppVersion.Reference_Plan__r.Account__c;
		newPlan.Check_In__c						= ppVersion.Reference_Plan__r.Check_In__c;
		update newPlan;

		//Create a new Pool Plan Version, and make it the latest
		Pool_Plan_Version__c newLatestVersion	= new Pool_Plan_Version__c();
		newLatestVersion.Pool_Plan__c			= ppVersion.Pool_Plan__c;
		newLatestVersion.Reference_Plan__c		= newPlan.Id;
		newLatestVersion.Version__c				= pPlan.Pool_Plan_Versions__r[0].Version__c + 1;
		insert newLatestVersion;






	}


}