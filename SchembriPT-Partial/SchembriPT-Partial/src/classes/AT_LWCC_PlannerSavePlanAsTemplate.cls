/**
* @author Harrison Campbell
* @description Lightning Component for 'plannerXSavePlanAsTemplate'
*/ 
public without sharing class AT_LWCC_PlannerSavePlanAsTemplate {

	public class TemplatePlan {
		
		@AuraEnabled
		public String nutritionId { get; set; }
		
		@AuraEnabled
		public string templateName { get; set; }
		
		@AuraEnabled
		public List<TemplateBlock> templateBlocks { get; set; }

	}

	public class TemplateBlock {
		
		@AuraEnabled
		public String nutritionId { get; set; }
		
		@AuraEnabled
		public String templateName { get; set; }

		@AuraEnabled
		public String templateShortName { get; set; }

	}

	/**
	* @description Create Template Blocks based on their relating nutrition blocks
	* @param templateBlocks 
	* @return 
	*/ 
	private static Map<String, Template_Block__c> newTemplateBlocksByNutritionBlocks(Id clientUserId, List<TemplateBlock> templateBlocks) {

		RecordType tmpNutBlockType			= [SELECT Id FROM RecordType WHERE DeveloperName = 'Template_Nutrition_Block' AND SobjectType = 'Template_Block__c'];

		Map<String, Template_Block__c> newTemplateBlocks = new Map<String, Template_Block__c>();
		for(TemplateBlock tmpBlock : templateBlocks) {
			Template_Block__c newTemplateBlock		= new Template_Block__c();
			newTemplateBlock.Client__c				= clientUserId;
			newTemplateBlock.Status__c				= 'Client Specific';
			newTemplateBlock.RecordTypeId			= tmpNutBlockType.Id;
			newTemplateBlock.Name					= tmpBlock.templateName;
			newTemplateBlock.Block_Short_Name__c	= tmpBlock.templateShortName;
			newTemplateBlocks.put(tmpBlock.nutritionId, newTemplateBlock);
		}
		return newTemplateBlocks;
	}

	/**
	* @description Create Template Block Items
	* @param newTemplateBlocks Template Blocks grouped by nutrition Id
	* @return Returns the new Template Block Items
	*/ 
	private static List<Template_Block_Item__c> createTemplateBlockItems(Map<String, Template_Block__c> newTemplateBlocks) {
	
		RecordType tmpNutBlockItemType		= [SELECT Id FROM RecordType WHERE DeveloperName = 'Template_Nutrition_Block_Item' AND SobjectType = 'Template_Block_Item__c'];

		//Query the Reference Blocks
		List<Block__c> nutBlocks = [
			SELECT
				Id,
				Display_Order__c,
				(
					SELECT
						Id,
						Display_Order__c,
						Template_Item_Id__c,
						Trainer_Quantity__c,
						Client_Quantity__c,
						RecordType.DeveloperName
					FROM Plan_Items__r
					ORDER BY Display_Order__c ASC
				)
			FROM Block__c
			WHERE Id IN :newTemplateBlocks.keySet()
			ORDER BY Display_Order__c ASC
		];

		//Create the Template Item/Blocks
		List<Template_Block_Item__c> newTemplateBlockItems = new List<Template_Block_Item__c>();
		for(Block__c blk : nutBlocks) {
			for(Item__c itm : blk.Plan_Items__r) {
				Template_Block_Item__c newTemplateBlockItem = new Template_Block_Item__c();
				newTemplateBlockItem.RecordTypeId			= tmpNutBlockItemType.Id;
				newTemplateBlockItem.Template_Block__c		= newTemplateBlocks.get(blk.Id).Id;
				newTemplateBlockItem.Template_Item__c		= itm.Template_Item_Id__c;
				newTemplateBlockItem.Display_Order__c		= itm.Display_Order__c;
				if(itm.RecordType.DeveloperName == 'Nutrition_Reference_Item') {
					newTemplateBlockItem.Quantity__c		= itm.Trainer_Quantity__c;
				} else if(itm.RecordType.DeveloperName == 'Nutrition_Client_Item' || itm.RecordType.DeveloperName == 'Nutrition_Session_Item') {
					newTemplateBlockItem.Quantity__c		= itm.Client_Quantity__c;
				}
				newTemplateBlockItems.add(newTemplateBlockItem);
			}
		}

		return newTemplateBlockItems;
	}

	/**
	* @description Get Plan Details
	* @param planId The Id of the Plan
	*/ 
	@AuraEnabled
	public static AT_M_Nutrition.NutritionPlan GetPlanDetails(String planId) {
		return AT_M_Nutrition.QueryNutritionPlans(new Id[] { planId }).get(0);
	}

	/**
	* @description Save Plan as Template Plan
	* @param templatePlan The Template Data
	*/ 
	@AuraEnabled
	public static void SaveAsTemplatePlan(TemplatePlan templatePlan) {

		//Get the related Plan
		Plan__c plan = [
			SELECT
				Id,
				Account__r.SF_Community_User__pc
			FROM Plan__c
			WHERE Id = :templatePlan.nutritionId
		];
	
		RecordType tmpNutPlanRecType		= [SELECT Id FROM RecordType WHERE DeveloperName = 'Template_Nutrition_Plan' AND SobjectType = 'Template_Plan__c'];
		RecordType tmpNutPlanBlockRecType	= [SELECT Id FROM RecordType WHERE DeveloperName = 'Template_Nutrition_Plan_Block' AND SobjectType = 'Template_Plan_Block__c'];

		//Create the Template Plan
		Template_Plan__c newTemplatePlan	= new Template_Plan__c();
		newTemplatePlan.Client__c			= plan.Account__r.SF_Community_User__pc;
		newTemplatePlan.Status__c			= 'Private';
		newTemplatePlan.Name				= templatePlan.templateName;
		newTemplatePlan.RecordTypeId		= tmpNutPlanRecType.Id;
		insert newTemplatePlan;

		//Create the Template Blocks
		Map<String, Template_Block__c> newTemplateBlocks = newTemplateBlocksByNutritionBlocks(plan.Account__r.SF_Community_User__pc, templatePlan.templateBlocks);
		insert newTemplateBlocks.values();

		//Create the Template Plan/Blocks
		List<Template_Plan_Block__c> newTemplatePlanBlocks = new List<Template_Plan_Block__c>();
		for(Integer i = 0; i < templatePlan.templateBlocks.size(); i++) {
			Template_Plan_Block__c newTmpPlanBlock	= new Template_Plan_Block__c();
			newTmpPlanBlock.Name					= newTemplateBlocks.get(templatePlan.templateBlocks.get(i).nutritionId).Name;
			newTmpPlanBlock.RecordTypeId			= tmpNutPlanBlockRecType.Id;
			newTmpPlanBlock.Template_Plan__c		= newTemplatePlan.Id;
			newTmpPlanBlock.Template_Block__c		= newTemplateBlocks.get(templatePlan.templateBlocks.get(i).nutritionId).Id;
			newTmpPlanBlock.Order__c				= i;
			newTemplatePlanBlocks.add(newTmpPlanBlock);
		}
		insert newTemplatePlanBlocks;

		//Create Template Block/Items
		List<Template_Block_Item__c> newTemplateBlockItems = createTemplateBlockItems(newTemplateBlocks);
		insert newTemplateBlockItems;

	}
	
	/**
	* @description Save Blocks as Template Blocks
	* @param templateBlocks The Template Data
	*/ 
	@AuraEnabled
	public static void SaveAsTemplateBlocks(Id clientUserId, List<TemplateBlock> templateBlocks) {

		//Create the Template Blocks
		Map<String, Template_Block__c> newTemplateBlocks = newTemplateBlocksByNutritionBlocks(clientUserId, templateBlocks);
		insert newTemplateBlocks.values();
		
		//Create Template Block/Items
		List<Template_Block_Item__c> newTemplateBlockItems = createTemplateBlockItems(newTemplateBlocks);
		insert newTemplateBlockItems;

	}

}