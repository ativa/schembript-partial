@isTest 
private class CommunityWeeklyPlansControllerTest {

	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		insert testAccount;
		
		return testAccount;

	}

	private static Contact updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		testContact.Email				= 'test@tst.com';
		testContact.Phone				= '0489123345';

		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Lower Back';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';
		testProgram.Program_Start_Date__c	= Date.newInstance(2020, 3, 2);
		testProgram.Program_End_Date__c		= Date.newInstance(2020, 5, 25);
		testProgram.Check_In_Frequency__c	= '1 Week';
		testProgram.Program__c				= 'Body Revolt';

		
		//Create a Product for the case
		Programs_Products__c prod	= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 3;
		prod.Name					= 'Body Weekly';
		insert prod;
		testProgram.Programs_Products__c = prod.Id;
		insert testProgram;
				
		testProgram.Status = 'Active';
		update testProgram;

		return testProgram;
	}

	@isTest(SeeAllData=true)
	private static void getAccountID_getId() {
		/*
		//Get a user that has an asscociated account
		User u = [SELECT Contact.AccountId FROM User WHERE Contact.AccountId != null AND IsActive = true LIMIT 1];
		System.runAs(u) {
			String accountId = CommunityWeeklyPlansController.getAccountID();
			System.assertEquals(u.Contact.AccountId, accountId);
		}
		*/

	}
	
	@IsTest
	private static void queryWeeklyData_querySessionPlans() {
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		//Query CheckIn 0
		Check_In__c checkIn0 = [SELECT Id, Name, Check_In_Number__c, Status__c, Scheduled_Check_In_Date__c FROM Check_In__c];
		checkIn0.Scheduled_Check_In_Date__c = Date.newInstance(2020, 1, 20);
		update checkIn0;

		//Create a list of plans
		List<Plan__c> plans = new List<Plan__c>();
		for(Integer i = 0; i < 3; i++) {
			Plan__c newPlan = new Plan__c();
			newPlan.RecordTypeId		= planSessionRecId;
			newPlan.Session_Date__c		= Date.newInstance(2020, 1, 1 + i);
			newPlan.Name				= 'Plan ' + (i+1);
			newPlan.Check_In__c			= checkIn0.Id;
			newPlan.Account__c			= testAccount.Id;
			newPlan.Total_Fat__c		= (i+1) * 32;
			newPlan.Total_Protein__c	= (i+1) * 15;
			newPlan.Total_Carbs__c		= (i+1) * 12;
			newPlan.Total_Calories__c	= (i+1) * 13;
			newPlan.Actual_Plan_Calories__c	= (i+1) * 26;
			newPlan.Actual_Plan_Protein__c	= (i+1) * 22;
			newPlan.Actual_Plan_Carbs__c		= (i+1) * 11;
			newPlan.Actual_Plan_Fat__c		= (i+1) * 8;

			newPlan.Order__c			= i;
			plans.add(newPlan);
		}
		
		insert plans;

		Test.startTest();
		CommunityWeeklyPlansController.WeeklyPlanData planData = CommunityWeeklyPlansController.queryWeeklyData(testAccount.Id, Date.newInstance(2000,1,1), Date.newInstance(2030,1,1));
		Test.stopTest();

		//Assert that the plans are corect
		System.assertEquals(plans.size(), planData.plans.size());
		for(Integer i = 0; i < plans.size(); i++) {
			System.assertEquals(plans[i].Id,						planData.plans[i].Id);
			System.assertEquals(plans[i].Name,						planData.plans[i].Name);
			System.assertEquals(plans[i].Session_Date__c,			planData.plans[i].Session_Date__c);
			System.assertEquals(plans[i].Total_Calories__c,			planData.plans[i].Total_Calories__c);
			System.assertEquals(plans[i].Total_Protein__c,			planData.plans[i].Total_Protein__c);
			System.assertEquals(plans[i].Total_Carbs__c,			planData.plans[i].Total_Carbs__c);
			System.assertEquals(plans[i].Total_Fat__c,				planData.plans[i].Total_Fat__c);
			System.assertEquals(plans[i].Actual_Plan_Calories__c,	planData.plans[i].Actual_Plan_Calories__c);
			System.assertEquals(plans[i].Actual_Plan_Protein__c,	planData.plans[i].Actual_Plan_Protein__c);
			System.assertEquals(plans[i].Actual_Plan_Carbs__c,		planData.plans[i].Actual_Plan_Carbs__c);
			System.assertEquals(plans[i].Actual_Plan_Fat__c,		planData.plans[i].Actual_Plan_Fat__c);
		}	
		
	}

	@IsTest
	private static void queryWeeklyData_query0SessionPlans() {

		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;
		
		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		//Query CheckIn 0
		Check_In__c checkIn0 = [SELECT Id, Name, Check_In_Number__c, Status__c, Scheduled_Check_In_Date__c FROM Check_In__c];
		checkIn0.Scheduled_Check_In_Date__c = Date.newInstance(2020, 1, 20);
		update checkIn0;

		Test.startTest();
		CommunityWeeklyPlansController.WeeklyPlanData planData = CommunityWeeklyPlansController.queryWeeklyData(testAccount.Id, Date.newInstance(2000,1,1), Date.newInstance(2030,1,1));
		Test.stopTest();

		//Assert that the plans are corect
		System.assertEquals(0, planData.plans.size());
		
	}

	@IsTest
	private static void queryWeeklyData_NoCheckInFound() {
		
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		
		Test.startTest();
		CommunityWeeklyPlansController.WeeklyPlanData planData = CommunityWeeklyPlansController.queryWeeklyData(testAccount.Id, Date.newInstance(2000,1,1), Date.newInstance(2030,1,1));
		Test.stopTest();

		System.assertEquals(null, planData.checkIn);
		System.assertEquals(0, planData.plans.size());

	}

}