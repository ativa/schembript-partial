/**
* @author Harrison Campbell
* @description Testing the CommunityClientProgressController when there is over 4 Check-Ins
*/ 
@isTest 
private class CommunityClientProgressControllerTest {
	
	/**
	* @description Creates a Test User
	* @return Returns a user for testing
	*/ 
	private static User testUser() {
	     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		 User u = new User(
			Alias = 'standt',
			Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='aaa@testorg.com'
		);
		insert u;

		return u;
	}
	
	/**
	* @description Create a Test Account
	* @return Returns a Test Account
	*/ 
	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		testAccount.PersonEmail		= 'test@tst.com';
		testAccount.Phone			= '0489123345';
		testAccount.SF_Community_User__pc = UserInfo.getUserId();
		insert testAccount;
		
		return testAccount;

	}

	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		//testContact.Email				= 'test@tst.com';
		//testContact.Phone				= '0489123345';
		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		
		//Create a Product for the case
		Programs_Products__c prod			= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 12;
		prod.Name					= 'Body Revolt'; //I need to do this for the process builder
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;


		return testProgram;
	}
	
	@IsTest
	private static void queryProgress_0CheckInsIfNoneCompleted() {
	
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		
		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		List<Plan__c> newPlans = new List<Plan__c>();

		for(Integer i = 0; i < 1; i++) {
		
			String checkNum = i + '';

			Check_In__c check = [
				SELECT 
					Id, 
					Name, 
					Program__c,
					Scheduled_Check_In_Date__c,
					Check_In_Number__c, 
					Status__c,
					Thigh_Measurement__c,
					Waist_Measurement__c,
					Chest_Measurement__c,
					Hip_Measurement__c,
					Weight__c
				FROM Check_In__c 
				WHERE Check_In_Number__c = :checkNum
			];

			System.debug(check);


			//I probably should remove this
			if(check.Check_In_Number__c == '0') {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			check.Thigh_Measurement__c	= 33 + (i+1);
			check.Waist_Measurement__c	= 45 + (i+1);
			check.Chest_Measurement__c	= 56 + (i+1);
			check.Hip_Measurement__c	= 50 + (i+1);
			check.Weight__c				= 75 + (i+1);
			
			Plan__c newPlan				= new Plan__c();
			newPlan.RecordTypeId		= planSessionRecId;
			newPlan.Name				= 'Plan ' + (i+1);
			newPlan.Check_In__c			= check.Id;
			newPlan.Actual_Plan_Calories__c	= (i+1) * 13;
			newPlan.Order__c			= i;
			newPlans.add(newPlan);

			if(i < 0) {
				check.Stage__c = 'Completed';
			}
			update check;
		}
		insert newPlans;

		//Query the progress of the client
		Test.startTest();
		CommunityClientProgressController.ClientProgress progress = CommunityClientProgressController.queryProgress(testAccount.Id, Date.newInstance(Date.today().year() - 1,1,1), Date.newInstance(Date.today().year() + 1,1,1));
		Test.stopTest();

		System.assertEquals(0, progress.checkIns.size());
		
	}
	
	@IsTest
	private static void queryProgress_1CheckInsIf1Completed() {
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		autoSettings.Enable_Onboarding_Automation__c = true;
		insert autoSettings;

		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		
	
		Case currentProgram = [SELECT Id FROM Case];
		Contact cont 		= [SELECT Id FROM Contact];
		Account acc 		= [SELECT Id FROM Account];

		List<Check_In__c> allProgramCheckIns = [
			SELECT
				Id
			FROM Check_In__c
		];
		delete allProgramCheckIns;

		List<Check_In__c> newCheckIns = new List<Check_In__c>();

		for(Integer i = 0; i < 1; i++) {
			Check_In__c newCheckIn 					= new Check_In__c();
			newCheckIn.Name 						= 'Check-In' + i;
			newCheckIn.Client_Program__c 			= currentProgram.Id;
			newCheckIn.Scheduled_Check_In_Date__c 	= Date.today().addDays(i);
			newCheckIn.Check_In_Number_v2__c		= i;
			newCheckIn.Thigh_Measurement__c			= 33 + (i+1);
			newCheckIn.Waist_Measurement__c			= 45 + (i+1);
			newCheckIn.Chest_Measurement__c			= 56 + (i+1);
			newCheckIn.Hip_Measurement__c			= 50 + (i+1);
			newCheckIn.Weight__c					= 75 + (i+1);
			newCheckIn.Stage__c 					= 'Completed';
			newCheckIn.Contact__c					= cont.Id;
			newCheckIn.Account__c					= acc.Id;
			newCheckIn.h_Test_Record__c				= true;
			newCheckIns.add(newCheckIn);
		}

		insert newCheckIns;

		List<Plan__c> newPlans = new List<Plan__c>();

		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		for(Integer i = 0; i < 1; i++) {

			Plan__c newPlan					= new Plan__c();
			newPlan.RecordTypeId			= planSessionRecId;
			newPlan.Name					= 'Plan ' + (i+1);
			newPlan.Check_In__c				= newCheckIns.get(i).Id;
			newPlan.Actual_Plan_Calories__c	= (i+1) * 13;
			newPlan.Order__c				= i;
			newPlans.add(newPlan);
		}

		insert newPlans;

		/*
		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		List<Plan__c> newPlans = new List<Plan__c>();

		for(Integer i = 0; i < 2; i++) {
		
			Check_In__c check = [
				SELECT 
					Id, 
					Name, 
					Program__c,
					Scheduled_Check_In_Date__c,
					Check_In_Number__c, 
					Status__c,
					Thigh_Measurement__c,
					Waist_Measurement__c,
					Chest_Measurement__c,
					Hip_Measurement__c,
					Weight__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :i
			];

			System.debug(check);
			System.debug(check.Scheduled_Check_In_Date__c);

			check.Scheduled_Check_In_Date__c = Date.today().addDays(i);

			check.Thigh_Measurement__c	= 33 + (i+1);
			check.Waist_Measurement__c	= 45 + (i+1);
			check.Chest_Measurement__c	= 56 + (i+1);
			check.Hip_Measurement__c	= 50 + (i+1);
			check.Weight__c				= 75 + (i+1);
			
			Plan__c newPlan				= new Plan__c();
			newPlan.RecordTypeId		= planSessionRecId;
			newPlan.Name				= 'Plan ' + (i+1);
			newPlan.Check_In__c			= check.Id;
			newPlan.Actual_Plan_Calories__c	= (i+1) * 13;
			newPlan.Order__c			= i;
			newPlans.add(newPlan);

			if(i < 1) {
				check.Stage__c = 'Completed';
			}
			update check;
		}
		insert newPlans;
		*/

		//Query the progress of the client
		Test.startTest();
		CommunityClientProgressController.ClientProgress progress = CommunityClientProgressController.queryProgress(testAccount.Id, Date.newInstance(Date.today().year() - 1,1,1), Date.newInstance(Date.today().year() + 1,1,1));
		Test.stopTest();

		System.assertEquals(1, progress.checkIns.size());
		
		System.assertEquals(testAccount.Id,														progress.checkIns[0].Account__c);
		System.assertEquals(Date.today(),														progress.checkIns[0].Scheduled_Check_In_Date__c);
		System.assertEquals(34,																	progress.checkIns[0].Thigh_Measurement__c);
		System.assertEquals(46,																	progress.checkIns[0].Waist_Measurement__c);
		System.assertEquals(57,																	progress.checkIns[0].Chest_Measurement__c);
		System.assertEquals(51,																	progress.checkIns[0].Hip_Measurement__c);
		System.assertEquals(76,																	progress.checkIns[0].Weight__c);
		System.assertEquals(1,																	progress.checkIns[0].Plans__r.size());
		System.assertEquals(13,																	progress.checkIns[0].Plans__r[0].Actual_Plan_Calories__c);

	}

	@IsTest
	private static void queryProgress_0CheckIns() {
		
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		//Query the progress of the client
		Test.startTest();
		CommunityClientProgressController.ClientProgress progress = CommunityClientProgressController.queryProgress(testAccount.Id, Date.newInstance(Date.today().year() - 1, 1, 1), Date.newInstance(Date.today().year() + 1, 1, 1));
		Test.stopTest();

		System.assertEquals(0, progress.checkIns.size());
		
	}
	
	@IsTest
	private static void queryProgram_0CheckInsIfNonCompleted() {
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		
		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		List<Plan__c> newPlans = new List<Plan__c>();

		for(Integer i = 0; i < 1; i++) {
		
			String checkNum = i + '';

			Check_In__c check = [
				SELECT 
					Id, 
					Name, 
					Program__c,
					Scheduled_Check_In_Date__c,
					Check_In_Number__c, 
					Status__c,
					Thigh_Measurement__c,
					Waist_Measurement__c,
					Chest_Measurement__c,
					Hip_Measurement__c,
					Weight__c
				FROM Check_In__c 
				WHERE Check_In_Number__c = :checkNum
			];

			System.debug(check);


			//I probably should remove this
			if(check.Check_In_Number__c == '0') {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			check.Thigh_Measurement__c	= 33 + (i+1);
			check.Waist_Measurement__c	= 45 + (i+1);
			check.Chest_Measurement__c	= 56 + (i+1);
			check.Hip_Measurement__c	= 50 + (i+1);
			check.Weight__c				= 75 + (i+1);
			
			Plan__c newPlan				= new Plan__c();
			newPlan.RecordTypeId		= planSessionRecId;
			newPlan.Name				= 'Plan ' + (i+1);
			newPlan.Check_In__c			= check.Id;
			newPlan.Actual_Plan_Calories__c	= (i+1) * 13;
			newPlan.Order__c			= i;
			newPlans.add(newPlan);

			if(i < 0) {
				check.Stage__c = 'Completed';
			}
			update check;
		}
		insert newPlans;

		//Query the progress of the client

		Date queryDate = Date.newInstance(Date.today().year(), Date.today().month(), 18);

		Test.startTest();
		CommunityClientProgressController.ClientProgress progress = CommunityClientProgressController.queryProgram(testAccount.Id, queryDate);
		Test.stopTest();

		System.assertEquals(0, progress.checkIns.size());

	}

	@IsTest
	private static void queryProgram_0CheckIns() {
		
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		Date queryDate = Date.newInstance(Date.today().year(), Date.today().month(), 18);
		//Query the progress of the client
		Test.startTest();
		CommunityClientProgressController.ClientProgress progress = CommunityClientProgressController.queryProgram(testAccount.Id, queryDate);
		Test.stopTest();

		System.assertEquals(0, progress.checkIns.size());
		
	}

	@IsTest(SeeAllData=true)
	private static void getAccountID_getAccountId() {
		
		/*

		//Get a user that has an asscociated account
		User u = [SELECT Contact.AccountId FROM User WHERE Contact.AccountId != null AND IsActive = true LIMIT 1];
		System.runAs(u) {
			String accountId = CommunityClientProgressController.getAccountID(null);
			System.assertEquals(u.Contact.AccountId, accountId);
		}

		*/


	}

}