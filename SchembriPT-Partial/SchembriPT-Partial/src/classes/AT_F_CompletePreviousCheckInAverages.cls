public class AT_F_CompletePreviousCheckInAverages {

	@TestVisible
	private static Date todaysDate = Date.today();
	
	public class Request {

		@InvocableVariable(label='Check-In Id' description='The Id of the Check-In' required=true)
		public String checkInId;

	}

	/**
	* @description Query Session Plans related to a Check-In
	* @param checkInId The Id of the Check-In
	* @return Returns the Session Plan
	*/ 
	private static List<Plan__c> QueryCheckInSessionPlans(Id checkInId) {
		return [
			SELECT
				Id,
				Completed__c,
				Session_Date__c,
				Client_Plan_Protein__c,
				Client_Plan_Carbs__c,
				Client_Plan_Fat__c,
				Client_Plan_Calories__c,
				Actual_Plan_Protein__c,
				Actual_Plan_Carbs__c,
				Actual_Plan_Fat__c,
				Actual_Plan_Calories__c,
				(
					SELECT 
						Id,
						Completed__c,
						Actual_Block_Protein__c,
						Actual_Block_Carbs__c,
						Actual_Block_Fat__c,
						Actual_Block_Calories__c,
						Client_Block_Protein__c,
						Client_Block_Carbs__c,
						Client_Block_Fat__c,
						Client_Block_Calories__c
					FROM Blocks__r
				)
			FROM Plan__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Nutrition_Session_Plan'
		];
	}

	/**
	* @description Complete the 
	* @param checkInId The Id of the current Check-In
	*/ 
	private static void CompleteNutritionSessionPlansAndCalculateActuals(Id checkInId) {
	
		Check_In__c currCheckIn = [
			SELECT 
				Id,
				Calories_Consumed_average__c,
				Carbs_Consumed_average__c,
				Fat_Consumed_average__c,
				Protein_Consumed_average__c,
				Previous_Check_In__c
			FROM Check_In__c 
			WHERE Id = :checkInId
		];

		//Query the Session Plans
		List<Plan__c> pCheckInSessionPlans	= QueryCheckInSessionPlans(currCheckIn.Previous_Check_In__c);
		Integer checkInSessionPlanCount		= pCheckInSessionPlans.size();

		Integer plansBeforeToday			= 0;

		Decimal completedPlanTotalProtein	= 0;
		Decimal completedPlanTotalCarbs		= 0;
		Decimal completedPlanTotalFat		= 0;
		Decimal completedPlanTotalCalories	= 0;

		List<Plan__c> colPlansToComplete	= new List<Plan__c>();
		List<Block__c> colBlocksToComplete	= new List<Block__c>();


		for(Plan__c sessPlan : pCheckInSessionPlans) {
			if(sessPlan.Session_Date__c <= todaysDate) {
				plansBeforeToday += 1;
				//If the plan hasn't been completed yet, then set the actuals
				if(sessPlan.Completed__c == false || sessPlan.Completed__c == null) {
					Plan__c planToComplete					= sessPlan;
					planToComplete.Completed__c				= true;
					planToComplete.Actual_Plan_Protein__c	= sessPlan.Client_Plan_Protein__c;
					planToComplete.Actual_Plan_Carbs__c		= sessPlan.Client_Plan_Carbs__c;
					planToComplete.Actual_Plan_Fat__c		= sessPlan.Client_Plan_Fat__c;
					planToComplete.Actual_Plan_Calories__c	= sessPlan.Client_Plan_Calories__c;
					colPlansToComplete.add(planToComplete);

					for(Block__c sessBlock : sessPlan.Blocks__r) {
						//If the block hasn't been completed yet, then set the actuals
						if(sessBlock.Completed__c == false) {
							Block__c blockToComplete = sessBlock;
							blockToComplete.Completed__c = true;
							blockToComplete.Actual_Block_Protein__c		= blockToComplete.Client_Block_Protein__c;
							blockToComplete.Actual_Block_Carbs__c		= blockToComplete.Client_Block_Carbs__c;
							blockToComplete.Actual_Block_Fat__c			= blockToComplete.Client_Block_Fat__c;
							blockToComplete.Actual_Block_Calories__c	= blockToComplete.Client_Block_Calories__c;
							colBlocksToComplete.add(blockToComplete);
						}
					}
				}
				//Add the total values
				completedPlanTotalProtein	+= sessPlan.Client_Plan_Protein__c;
				completedPlanTotalCarbs		+= sessPlan.Client_Plan_Carbs__c;
				completedPlanTotalFat		+= sessPlan.Client_Plan_Fat__c;
				completedPlanTotalCalories	+= sessPlan.Client_Plan_Calories__c;
			}

		}

		if(colPlansToComplete.size() > 0) {	
			update colPlansToComplete;
		}
		if(colBlocksToComplete.size() > 0) {
			update colBlocksToComplete;
		}

		if(plansBeforeToday > 0) {
			currCheckIn.Protein_Consumed_average__c		= completedPlanTotalProtein		/ plansBeforeToday;
			currCheckIn.Carbs_Consumed_average__c		= completedPlanTotalCarbs		/ plansBeforeToday;
			currCheckIn.Fat_Consumed_average__c			= completedPlanTotalFat			/ plansBeforeToday;
			currCheckIn.Calories_Consumed_average__c	= completedPlanTotalCalories	/ plansBeforeToday;
		} else {
			currCheckIn.Protein_Consumed_average__c		= 0;
			currCheckIn.Carbs_Consumed_average__c		= 0;
			currCheckIn.Fat_Consumed_average__c			= 0;
			currCheckIn.Calories_Consumed_average__c	= 0;
		}

		update currCheckIn;

		//Also update the Check-In Summary
		Plan_Trigger_Tools.UpdatePlanSummaryForCheckIn(currCheckIn.Id);

	}

	@InvocableMethod(label='Complete Nutrition Session Plans And Calculate Actuals' description='' category='Ativa - Apex Flow Action')
	public static void A(List<Request> requests) {

		for(Request req : requests) {
			CompleteNutritionSessionPlansAndCalculateActuals(req.checkInId);
		}

	}



}