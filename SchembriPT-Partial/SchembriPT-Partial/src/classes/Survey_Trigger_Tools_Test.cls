/**
* @author Harrison Campbell
* @description Tests the Survey_Trigger_Tools class
*/ 
@isTest 
private class Survey_Trigger_Tools_Test {

	/*
	@TestSetup
	private static void setup() {
		
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		autoSettings.Enable_Onboarding_Automation__c = true;
		insert autoSettings;
	}
	*/

	/**
	* @description Create a Test Account
	* @return Returns a Test Account
	*/
	/* 
	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		testAccount.SF_Community_User__pc		= UserInfo.getUserId();
		insert testAccount;
		
		return testAccount;
	}
	*/

	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	/*
	private static Contact updateTestContact(Account testAccount) {


	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		testContact.Email				= 'test@tst.com';
		testContact.Phone				= '0489123345';

		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}
	*/

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	/*
	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		//Create a Product for the case
		Programs_Products__c prod	= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 3;
		prod.Name					= 'Body Weekly';
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Program_End_Date__c		= Date.newInstance(Date.today().year(), Date.today().month() + 3, 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;

		return testProgram;
	}

	private static Survey__c createTestSurvey(Id checkInId, Decimal suffix, Boolean mapFields) {
		
		Id surveyDetailedRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Survey__c' AND DeveloperName = 'Check_In_Detailed'].Id;

		Survey__c survey									= new Survey__c();
		survey.Name											= 'Survey - ' + suffix;
		survey.RecordTypeId									= surveyDetailedRecId;
		survey.Check_In__c									= checkInId;

		if(mapFields) {
			survey.Are_you_currently_taking_medication__c		= 'No';
			survey.Body_Fat_Reduction__c						= 'Yes';
			survey.Body_Recomposition__c						= 'Yes';
			survey.Post_Injury_Rehab_Recovery__c				= 'Yes';
			survey.Body_Transformation__c						= 'Yes';
			survey.Improve_Overall_Lifestyle_and_Fitness__c		= 'Yes';
			survey.Implement_Regular_Physical_Training__c		= 'Yes';
			survey.Increase_Lean_Muscle_Mass__c					= 'Yes';
			survey.Improve_Cardiovascular_Fitness__c			= 'Yes';
			survey.Bodybuilding_Competition__c					= 'Yes';
			survey.Increase_Strength__c							= 'Yes';
			survey.Learn_to_Manage_Training_Hours__c			= 'No';
			survey.Have_you_ever_cheated_on_your_diet__c		= 'A few times ' + suffix;
			survey.Future_goals__c								= 'I would like to get swole ' + suffix;		
			survey.Have_you_been_hitting_the_set_macros_cal__c	= 'I\'ve missed all of them ' + suffix;
			survey.Would_you_like_to_do_the_next_phase__c		= 'Yes';
			survey.How_do_you_relax__c							= 'Exercise' + suffix;
			survey.What_affected_your_meal_plan_consistency__c	= 'Having kids ' + suffix;
			survey.How_are_you_feeling__c						= 'Things are just average';
			survey.What_stopped_you_from_performing_cardio__c	= 'Weight Issues ' + suffix;
			survey.What_can_you_improve_on__c					= 'More Cardio ' + suffix;
			survey.How_challenging_was_the_PT_program_1_10__c	= '4';
			survey.Do_you_get_hungry_after_your_last_meal__c	= 'Yes ' + suffix;
			survey.What_interrupted_your_plans__c				= 'The Kids ' + suffix;
			survey.What_affected_your_completion_of_HIIT__c		= 'Welking ' + suffix;
			survey.What_were_you_most_fearful_of__c				= 'Beets ' + suffix;
			survey.What_made_you_start_this_program__c			= 'I just wanted too ' + suffix;
			survey.Your_biggest_motivator__c					= 'Partner';
			survey.Who_was_your_biggest_supporter__c			= 'My Mum :) ' + suffix;
			survey.How_many_sessions_did_you_do__c				= '200';
			survey.How_challenging_was_the_weight_plan__c		= '3';
			survey.What_was_challenging__c						= 'Cardio ' + suffix;
			survey.What_was_easy__c								= 'Weights ' + suffix;
			survey.How_would_you_rate_the_program__c			= '7';
			survey.How_could_we_improve__c						= 'More Cardio options ' + suffix;
			survey.Do_we_have_good_value_for_money__c			= 'Yes';
			survey.X3_words_to_describe_Schembri_PT__c			= 'Hot Cold Average';
			survey.Interested_in_a_referral_program__c			= 'Yes';
			survey.Any_family_or_friends_you_could_refer__c		= 'Nope ' + suffix;
		}

		return survey;

	}

	private static void assertCheckInFieldsWereNoMapped(Check_In__c check) {
	
		System.assertEquals(check.Body_Fat_Reduction__c, null);
		System.assertEquals(check.bod__c, null);
		System.assertEquals(check.Post_Injury_Rehab_Recovery__c, null);
		System.assertEquals(check.Body_Transformation__c, null);
		System.assertEquals(check.Improve_Overall_Lifestyle_and_Fitness__c, null);
		System.assertEquals(check.Implement_Regular_Physical_Training__c, null);
		System.assertEquals(check.Increase_Lean_Muscle_Mass__c, null);
		System.assertEquals(check.Improve_Cardiovascular_Fitness__c, null);
		System.assertEquals(check.Bodybuilding_Competition__c, null);
		System.assertEquals(check.Increase_Strength__c, null);
		System.assertEquals(check.Learn_to_Manage_Training_Hours__c, null);
		System.assertEquals(check.Have_you_ever_cheated_on_your_diet__c, null);
		System.assertEquals(check.Future_goals__c, null);		
		System.assertEquals(check.Have_you_been_hitting_the_set_macros_cal__c, null);
		System.assertEquals(check.Would_you_like_to_do_the_next_phase__c, null);
		System.assertEquals(check.How_do_you_relax__c, null);
		System.assertEquals(check.What_affected_your_meal_plan_consistency__c, null);
		System.assertEquals(check.How_are_you_feeling__c, null);
		System.assertEquals(check.What_stopped_you_from_performing_cardio__c, null);
		System.assertEquals(check.What_can_you_improve_on__c, null);
		System.assertEquals(check.How_challenging_was_the_PT_program_1_10__c, null);
		System.assertEquals(check.Do_you_get_hungry_after_your_last_meal__c, null);
		System.assertEquals(check.What_interrupted_your_plans__c, null);
		System.assertEquals(check.What_affected_your_completion_of_HIIT__c, null);
		System.assertEquals(check.What_were_you_most_fearful_of__c, null);
		System.assertEquals(check.What_made_you_start_this_program__c, null);
		System.assertEquals(check.Your_biggest_motivator__c, null);
		System.assertEquals(check.Who_was_your_biggest_supporter__c, null);
		System.assertEquals(check.How_many_sessions_did_you_do__c, null);
		System.assertEquals(check.How_challenging_was_the_weight_plan__c, null);
		System.assertEquals(check.What_was_challenging__c, null);
		System.assertEquals(check.What_was_easy__c, null);
		System.assertEquals(check.How_would_you_rate_the_program__c, null);
		System.assertEquals(check.How_could_we_improve__c, null);
		System.assertEquals(check.Do_we_have_good_value_for_money__c, null);
		System.assertEquals(check.X3_words_to_describe_Schembri_PT__c, null);
		System.assertEquals(check.Interested_in_a_referral_program__c, null);
		System.assertEquals(check.Any_family_or_friends_you_could_refer__c, null);

	}

	private static void assertCheckInFieldsWereMappedCorrectly(Check_In__c check, Decimal suffix) {
		System.assertEquals(check.Are_you_currently_taking_medication__c, 'No');
		System.assertEquals(check.Body_Fat_Reduction__c, 'Yes');
		System.assertEquals(check.bod__c, 'Yes');
		System.assertEquals(check.Post_Injury_Rehab_Recovery__c, 'Yes');
		System.assertEquals(check.Body_Transformation__c, 'Yes');
		System.assertEquals(check.Improve_Overall_Lifestyle_and_Fitness__c, 'Yes');
		System.assertEquals(check.Implement_Regular_Physical_Training__c, 'Yes');
		System.assertEquals(check.Increase_Lean_Muscle_Mass__c, 'Yes');
		System.assertEquals(check.Improve_Cardiovascular_Fitness__c, 'Yes');
		System.assertEquals(check.Bodybuilding_Competition__c, 'Yes');
		System.assertEquals(check.Increase_Strength__c, 'Yes');
		System.assertEquals(check.Learn_to_Manage_Training_Hours__c, 'No');
		System.assertEquals(check.Have_you_ever_cheated_on_your_diet__c, 'A few times ' + suffix);
		System.assertEquals(check.Future_goals__c, 'I would like to get swole ' + suffix);		
		System.assertEquals(check.Have_you_been_hitting_the_set_macros_cal__c, 'I\'ve missed all of them ' + suffix);
		System.assertEquals(check.Would_you_like_to_do_the_next_phase__c, 'Yes');
		System.assertEquals(check.How_do_you_relax__c, 'Exercise' + suffix);
		System.assertEquals(check.What_affected_your_meal_plan_consistency__c, 'Having kids ' + suffix);
		System.assertEquals(check.How_are_you_feeling__c, 'Things are just average');
		System.assertEquals(check.What_stopped_you_from_performing_cardio__c, 'Weight Issues ' + suffix);
		System.assertEquals(check.What_can_you_improve_on__c, 'More Cardio ' + suffix);
		System.assertEquals(check.How_challenging_was_the_PT_program_1_10__c, '4');
		System.assertEquals(check.Do_you_get_hungry_after_your_last_meal__c, 'Yes ' + suffix);
		System.assertEquals(check.What_interrupted_your_plans__c, 'The Kids ' + suffix);
		System.assertEquals(check.What_affected_your_completion_of_HIIT__c, 'Welking ' + suffix);
		System.assertEquals(check.What_were_you_most_fearful_of__c, 'Beets ' + suffix);
		System.assertEquals(check.What_made_you_start_this_program__c, 'I just wanted too ' + suffix);
		System.assertEquals(check.Your_biggest_motivator__c, 'Partner');
		System.assertEquals(check.Who_was_your_biggest_supporter__c, 'My Mum :) ' + suffix);
		System.assertEquals(check.How_many_sessions_did_you_do__c, '200');
		System.assertEquals(check.How_challenging_was_the_weight_plan__c, '3');
		System.assertEquals(check.What_was_challenging__c, 'Cardio ' + suffix);
		System.assertEquals(check.What_was_easy__c, 'Weights ' + suffix);
		System.assertEquals(check.How_would_you_rate_the_program__c, '7');
		System.assertEquals(check.How_could_we_improve__c, 'More Cardio options ' + suffix);
		System.assertEquals(check.Do_we_have_good_value_for_money__c, 'Yes');
		System.assertEquals(check.X3_words_to_describe_Schembri_PT__c, 'Hot Cold Average');
		System.assertEquals(check.Interested_in_a_referral_program__c, 'Yes');
		System.assertEquals(check.Any_family_or_friends_you_could_refer__c, 'Nope ' + suffix);

	}
	*/

	/**
	* @description Insert surveys with all fields mapped to checkIn
	*/ 
	/*
	@isTest
	private static void beforeInsert_insertSomeSurveysAllFieldsMappedToCheckIns() {
		
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 3; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Stage__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			if(i < 2) { check.Stage__c = 'Completed'; }

			//Create a survey for this checkIn
			surveys.add(createTestSurvey(check.Id, check.Check_In_Number_v2__c, true));

			update check;
		}

		Test.startTest();
		insert surveys;
		Test.stopTest();

		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Are_you_currently_taking_medication__c,
				Check_In_Number_v2__c,
				Body_Fat_Reduction__c,
				bod__c,
				Post_Injury_Rehab_Recovery__c,
				Body_Transformation__c,
				Improve_Overall_Lifestyle_and_Fitness__c,
				Implement_Regular_Physical_Training__c,
				Increase_Lean_Muscle_Mass__c,
				Improve_Cardiovascular_Fitness__c,
				Bodybuilding_Competition__c,
				Increase_Strength__c,
				Learn_to_Manage_Training_Hours__c,
				Have_you_ever_cheated_on_your_diet__c,
				Future_goals__c,
				Have_you_been_hitting_the_set_macros_cal__c,
				Would_you_like_to_do_the_next_phase__c,
				How_do_you_relax__c,
				What_affected_your_meal_plan_consistency__c,
				How_are_you_feeling__c,
				What_stopped_you_from_performing_cardio__c,
				What_can_you_improve_on__c,
				How_challenging_was_the_PT_program_1_10__c,
				Do_you_get_hungry_after_your_last_meal__c,
				What_interrupted_your_plans__c,
				What_affected_your_completion_of_HIIT__c,
				What_were_you_most_fearful_of__c,
				What_made_you_start_this_program__c,
				Your_biggest_motivator__c,
				Who_was_your_biggest_supporter__c,
				How_many_sessions_did_you_do__c,
				How_challenging_was_the_weight_plan__c,
				What_was_challenging__c,
				What_was_easy__c,
				How_would_you_rate_the_program__c,
				How_could_we_improve__c,
				Do_we_have_good_value_for_money__c,
				X3_words_to_describe_Schembri_PT__c,
				Interested_in_a_referral_program__c,
				Any_family_or_friends_you_could_refer__c
			FROM Check_In__c
			ORDER BY Check_In_Number_v2__c ASC
		];

		for(Check_In__c check : checkIns) {
			assertCheckInFieldsWereMappedCorrectly(check, check.Check_In_Number_v2__c);
		}

	}
	*/

	/**
	* @description Insert one survey to a CheckIn
	*/
	/*
	@isTest
	private static void beforeInsert_insertOneSurveyAllFieldsMappedToCheckIn() {
		
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 1; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Status__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			//Create a survey for this checkIn
			surveys.add(createTestSurvey(check.Id, check.Check_In_Number_v2__c, true));

			update check;
		}

		Test.startTest();
		insert surveys;
		Test.stopTest();

		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Are_you_currently_taking_medication__c,
				Check_In_Number_v2__c,
				Body_Fat_Reduction__c,
				bod__c,
				Post_Injury_Rehab_Recovery__c,
				Body_Transformation__c,
				Improve_Overall_Lifestyle_and_Fitness__c,
				Implement_Regular_Physical_Training__c,
				Increase_Lean_Muscle_Mass__c,
				Improve_Cardiovascular_Fitness__c,
				Bodybuilding_Competition__c,
				Increase_Strength__c,
				Learn_to_Manage_Training_Hours__c,
				Have_you_ever_cheated_on_your_diet__c,
				Future_goals__c,
				Have_you_been_hitting_the_set_macros_cal__c,
				Would_you_like_to_do_the_next_phase__c,
				How_do_you_relax__c,
				What_affected_your_meal_plan_consistency__c,
				How_are_you_feeling__c,
				What_stopped_you_from_performing_cardio__c,
				What_can_you_improve_on__c,
				How_challenging_was_the_PT_program_1_10__c,
				Do_you_get_hungry_after_your_last_meal__c,
				What_interrupted_your_plans__c,
				What_affected_your_completion_of_HIIT__c,
				What_were_you_most_fearful_of__c,
				What_made_you_start_this_program__c,
				Your_biggest_motivator__c,
				Who_was_your_biggest_supporter__c,
				How_many_sessions_did_you_do__c,
				How_challenging_was_the_weight_plan__c,
				What_was_challenging__c,
				What_was_easy__c,
				How_would_you_rate_the_program__c,
				How_could_we_improve__c,
				Do_we_have_good_value_for_money__c,
				X3_words_to_describe_Schembri_PT__c,
				Interested_in_a_referral_program__c,
				Any_family_or_friends_you_could_refer__c
			FROM Check_In__c
			ORDER BY Check_In_Number_v2__c ASC
		];

		for(Check_In__c check : checkIns) {
			assertCheckInFieldsWereMappedCorrectly(check, check.Check_In_Number_v2__c);
		}

	}

	@isTest
	private static void beforeInsert_insertSomeSurveysNoFieldsMappedToCheckIns() {
		
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 3; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Stage__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			if(i < 2) { check.Stage__c = 'Completed'; }

			//Create a survey for this checkIn
			surveys.add(createTestSurvey(check.Id, check.Check_In_Number_v2__c, false));

			update check;
		}

		Test.startTest();
		insert surveys;
		Test.stopTest();

		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Are_you_currently_taking_medication__c,
				Check_In_Number_v2__c,
				Body_Fat_Reduction__c,
				bod__c,
				Post_Injury_Rehab_Recovery__c,
				Body_Transformation__c,
				Improve_Overall_Lifestyle_and_Fitness__c,
				Implement_Regular_Physical_Training__c,
				Increase_Lean_Muscle_Mass__c,
				Improve_Cardiovascular_Fitness__c,
				Bodybuilding_Competition__c,
				Increase_Strength__c,
				Learn_to_Manage_Training_Hours__c,
				Have_you_ever_cheated_on_your_diet__c,
				Future_goals__c,
				Have_you_been_hitting_the_set_macros_cal__c,
				Would_you_like_to_do_the_next_phase__c,
				How_do_you_relax__c,
				What_affected_your_meal_plan_consistency__c,
				How_are_you_feeling__c,
				What_stopped_you_from_performing_cardio__c,
				What_can_you_improve_on__c,
				How_challenging_was_the_PT_program_1_10__c,
				Do_you_get_hungry_after_your_last_meal__c,
				What_interrupted_your_plans__c,
				What_affected_your_completion_of_HIIT__c,
				What_were_you_most_fearful_of__c,
				What_made_you_start_this_program__c,
				Your_biggest_motivator__c,
				Who_was_your_biggest_supporter__c,
				How_many_sessions_did_you_do__c,
				How_challenging_was_the_weight_plan__c,
				What_was_challenging__c,
				What_was_easy__c,
				How_would_you_rate_the_program__c,
				How_could_we_improve__c,
				Do_we_have_good_value_for_money__c,
				X3_words_to_describe_Schembri_PT__c,
				Interested_in_a_referral_program__c,
				Any_family_or_friends_you_could_refer__c
			FROM Check_In__c
			ORDER BY Check_In_Number_v2__c ASC
		];

		for(Check_In__c check : checkIns) {
			assertCheckInFieldsWereNoMapped(check);
		}

	}

	@isTest
	private static void beforeInsert_insertOneSurveyNoFieldsMappedToCheckIn() {
		
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 1; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Status__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			//Create a survey for this checkIn
			surveys.add(createTestSurvey(check.Id, check.Check_In_Number_v2__c, false));

			update check;
		}

		Test.startTest();
		insert surveys;
		Test.stopTest();

		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Check_In_Number_v2__c,
				Body_Fat_Reduction__c,
				bod__c,
				Post_Injury_Rehab_Recovery__c,
				Body_Transformation__c,
				Improve_Overall_Lifestyle_and_Fitness__c,
				Implement_Regular_Physical_Training__c,
				Increase_Lean_Muscle_Mass__c,
				Improve_Cardiovascular_Fitness__c,
				Bodybuilding_Competition__c,
				Increase_Strength__c,
				Learn_to_Manage_Training_Hours__c,
				Have_you_ever_cheated_on_your_diet__c,
				Future_goals__c,
				Have_you_been_hitting_the_set_macros_cal__c,
				Would_you_like_to_do_the_next_phase__c,
				How_do_you_relax__c,
				What_affected_your_meal_plan_consistency__c,
				How_are_you_feeling__c,
				What_stopped_you_from_performing_cardio__c,
				What_can_you_improve_on__c,
				How_challenging_was_the_PT_program_1_10__c,
				Do_you_get_hungry_after_your_last_meal__c,
				What_interrupted_your_plans__c,
				What_affected_your_completion_of_HIIT__c,
				What_were_you_most_fearful_of__c,
				What_made_you_start_this_program__c,
				Your_biggest_motivator__c,
				Who_was_your_biggest_supporter__c,
				How_many_sessions_did_you_do__c,
				How_challenging_was_the_weight_plan__c,
				What_was_challenging__c,
				What_was_easy__c,
				How_would_you_rate_the_program__c,
				How_could_we_improve__c,
				Do_we_have_good_value_for_money__c,
				X3_words_to_describe_Schembri_PT__c,
				Interested_in_a_referral_program__c,
				Any_family_or_friends_you_could_refer__c
			FROM Check_In__c
			ORDER BY Check_In_Number_v2__c ASC
		];

		for(Check_In__c check : checkIns) {
			assertCheckInFieldsWereNoMapped(check);
		}

	}

	@IsTest
	private static void beforeInsert_insertSurveyThatDoesntHaveACheckIn() {
		
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 1; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Status__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			//Create a survey for this checkIn, and revemove the checkin Id
			surveys.add(createTestSurvey(null, check.Check_In_Number_v2__c, true));

			update check;
		}

		Boolean exceptionCaught = false;

		try {
			Test.startTest();
			insert surveys;
			Test.stopTest();
		} catch(System.DmlException e) {
			exceptionCaught = true;
		}

		System.assert(exceptionCaught);

	}

	@IsTest
	private static void beforeUpdate_updateFields() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 1; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Status__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			//Create a survey for this checkIn
			surveys.add(createTestSurvey(check.Id, check.Check_In_Number_v2__c, false));

			update check;
		}

		insert surveys;

		surveys[0].Body_Fat_Reduction__c = 'Yes';
		
		Test.startTest();
		update surveys;
		Test.stopTest();

		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Body_Fat_Reduction__c
			FROM Check_In__c
			ORDER BY Check_In_Number_v2__c ASC
		];

		System.assertEquals(1, checkIns.size());
		System.assertEquals('Yes', checkIns[0].Body_Fat_Reduction__c);

	}

	@IsTest
	private static void beforeUpdate_updateToNullCheckIn() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		List<Survey__c> surveys = new List<Survey__c>();
		
		//Update 3 cases for testing, create plans for each of these checkIns
		for(Integer i = 0; i < 1; i++) {
			Integer checkNum = i;

			Check_In__c check = [
				SELECT
					Scheduled_Check_In_Date__c,
					Check_In_Number_v2__c, 
					Status__c
				FROM Check_In__c 
				WHERE Check_In_Number_v2__c = :checkNum
			];

			//I probably should remove this
			if(check.Check_In_Number_v2__c == 0) {
				check.Scheduled_Check_In_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), 1);
			}

			//Create a survey for this checkIn
			surveys.add(createTestSurvey(check.Id, check.Check_In_Number_v2__c, false));

			update check;
		}

		insert surveys;

		surveys[0].Check_In__c = null;
		
		Boolean exceptionCaught = false;

		try {
			Test.startTest();
			update surveys;
			Test.stopTest();
		} catch(System.DmlException e) {
			exceptionCaught = true;
		}

		System.assert(exceptionCaught);

	}
	*/

}