/**
* @author Harrison Campbell
* @description Class for the Template_Block_Item Trigger
*/ 
public with sharing class Template_Block_Item_TriggerClass {

	private static Map<Id, Template_Block__c> GetBlockItemTemplateBlocks(List<Template_Block_Item__c> tmpBlockItms) {

		Set<Id> tmpBlockIds = new Set<Id>();
		for(Template_Block_Item__c tmpBlockItm : tmpBlockItms) {
			tmpBlockIds.add(tmpBlockItm.Template_Block__c);
		}
		
		return new Map<Id, Template_Block__c>([
			SELECT
				Id,
				(
					SELECT
						Id,
						Template_Item__r.Protein__c,
						Template_Item__r.Fat__c,
						Template_Item__r.Carbs__c,
						Template_Item__r.Calories__c,
						Quantity__c,
						Template_Item__r.Item_Quantity__c
					FROM Block_Items__r
				)
			FROM Template_Block__c
			WHERE Id IN :tmpBlockIds
		]);


	}

	private static void UpdateTemplateBlockQuantities(Map<Id, Template_Block__c> tmpBlockMap) {
	
		//Update the new values for the template blocks
		for(Template_Block__c tmpBlock : tmpBlockMap.values()) {

			Decimal blockProtein	= 0;
			Decimal blockFat		= 0;
			Decimal blockCarbs		= 0;
			Decimal blockCalories	= 0;

			for(Template_Block_Item__c tmpBlkItem : tmpBlock.Block_Items__r) {

				Decimal tmpBlkItmQuantity	= (tmpBlkItem.Quantity__c != null) ? tmpBlkItem.Quantity__c : 0;
				Decimal tmpItmQuantity		= (tmpBlkItem.Template_Item__r.Item_Quantity__c != null && tmpBlkItem.Template_Item__r.Item_Quantity__c != 0) ? tmpBlkItem.Template_Item__r.Item_Quantity__c : 1;
				
				blockProtein	+= tmpBlkItem.Template_Item__r.Protein__c * tmpBlkItmQuantity / tmpItmQuantity;
				blockFat		+= tmpBlkItem.Template_Item__r.Fat__c * tmpBlkItmQuantity / tmpItmQuantity;
				blockCarbs		+= tmpBlkItem.Template_Item__r.Carbs__c * tmpBlkItmQuantity / tmpItmQuantity;
				blockCalories	+= tmpBlkItem.Template_Item__r.Calories__c * tmpBlkItmQuantity / tmpItmQuantity;
					
			}

			tmpBlock.Template_Block_Protein__c	= blockProtein;
			tmpBlock.Template_Block_Fat__c		= blockFat;
			tmpBlock.Template_Block_Carbs__c	= blockCarbs;
			tmpBlock.Template_Block_Calories__c	= blockCalories;

		}

	}

	private static void updateParentBlocks(List<Template_Block_Item__c> blockItems) {
		//Get the Template Blocks, and group the Template Block Items, by the Template Blocks, and
		//update the template blocks
		Map<Id, Template_Block__c> tmpBlockMap	= GetBlockItemTemplateBlocks(blockItems);
		updateTemplateBlockQuantities(tmpBlockMap);
		update tmpBlockMap.values();
	}


	public static void afterInsert(List<Template_Block_Item__c> blockItems) {
		updateParentBlocks(blockItems);
	}

	public static void afterUpdate(List<Template_Block_Item__c> blockItems) {
		updateParentBlocks(blockItems);
	}

	public static void afterDelete(List<Template_Block_Item__c> blockItems) {
		updateParentBlocks(blockItems);
	}

}