/**
* @author Harrison Campbell
* @description Lightning controller for the 'trackerXPlanner'
*/ 
public without sharing class AT_LWCC_TrackerPlanner {

	/**
	* @author Harrison Campbell
	* @description Holds information on a User
	*/ 
	public class UserInformation {

		/**
		* @description The Id of the user
		*/ 
		@AuraEnabled
		public String id { get; set; }
		
		/**
		* @description The name of the user
		*/ 
		@AuraEnabled
		public String name { get; set; }
		
		/**
		* @description The Id of the user account
		*/ 
		@AuraEnabled
		public String accountId { get; set; }

		/**
		* @description Create the User Information field
		* @param u The User Object
		*/ 
		public UserInformation(User u) {
			this.id			= u.Id;
			this.name		= u.Name;
			this.accountId	= u.AccountId;
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds information on the Client Plan
	*/ 
	public class PoolPlan {
		
		/**
		* @description The Id of the Pool Plan
		*/ 
		@AuraEnabled
		public String id { get; set; }
		
		/**
		 * @description The Id of the reference plan
		 */
		@AuraEnabled
		public String referencePlanId { get; set; }

		/**
		* @description The name of the reference plan
		*/ 
		@AuraEnabled
		public String referencePlanName { get; set; }

		public PoolPlan(Pool_Plan__c poolPlan) {
			this.id					= poolPlan.Id;
			this.referencePlanId	= poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Id;			
			this.referencePlanName	= poolPlan.Pool_Plan_Versions__r.get(0).Reference_Plan__r.Name;


		}
        
	}

	/**
	* @author Harrison Campbell
	* @description Holds Client Planner Information
	*/ 
	public class ClientPlannerInformation {

		/**
		* @description The Id for the Account
		*/ 
		@AuraEnabled
		public UserInformation userInfo { get; set; }

		/**
		* @description All the Client Plans
		*/ 
		@AuraEnabled
		public List<PoolPlan> clientPlans { get; set; }

		/**
		* @description All Template Pool Plans
		*/ 
		@AuraEnabled
		public List<PoolPlan> allTemplatePoolPlans { get; set; }

		/**
		* @description Holds information on the latest Check-In
		*/ 
		@AuraEnabled
		public CheckInInformation latestCheckInInfo { get; set; }

		/**
		* @description 
		*/ 
		public ClientPlannerInformation() {
			clientPlans = new List<PoolPlan>();
		}

	}

	/**
	* @author Harrison Campbell
	* @description Holds 
	*/ 
	public class CheckInInformation {

		@AuraEnabled
		public String id { get; set; }
		
		@AuraEnabled
		public String name { get; set; }
		
		@AuraEnabled
		public String stage { get; set; }

		@AuraEnabled
		public String nutritionPlanningMode { get; set; }

		@AuraEnabled
		public String guidedPlanNumberWeeks { get; set; }

		@AuraEnabled
		public Date nutritionPlanStartDate { get; set; }

		@AuraEnabled
		public Date nutritionPlanEndDate { get; set; }

		public CheckInInformation(Check_In__c checkIn) {
			this.id						= checkIn.Id;
			this.name					= checkIn.Name;
			this.stage					= checkIn.Stage__c;
			this.nutritionPlanningMode	= checkIn.Nutrition_Planning_Mode__c;
			this.guidedPlanNumberWeeks	= checkIn.Guided_Plan_Number_Weeks__c;
			this.nutritionPlanStartDate	= checkIn.Nutrition_Plan_Start_Date__c;
			this.nutritionPlanEndDate	= checkIn.Nutrition_Plan_End_Date__c;
		}

	}

	/**
	* @description Query the Client Plans related to the account
	* @param accountId The Id of the account
	*/ 
	private static List<PoolPlan> QueryAllClientPlans(string checkInId) {

		List<Pool_Plan__c> allCheckInPoolPlans = [
			SELECT
				Id,
				(
					SELECT 
						Id,
						Reference_Plan__r.Id,
						Reference_Plan__r.Name
					FROM Pool_Plan_Versions__r
				)
			FROM Pool_Plan__c
		];

		List<PoolPlan> cPlans = new List<PoolPlan>();
		for(Pool_Plan__c poolPlan : allCheckInPoolPlans) {
			cPlans.add(new PoolPlan(poolPlan));
		}
		return cPlans;
	}

	/**
	* @description Get the current user using this script
	* @param userId The Id of the user
	* @return Returns the User Salesforce Object
	*/ 
	private static User QueryClientUserBasedOnUserId(String userId) {
		return [
			SELECT
				Id,
				Name,
				AccountId
			FROM User
			WHERE Id = :userId
		];
	}

	/**
	* @description Get the current user using this script
	* @param accountId The Id of the account
	* @return Returns the User Salesforce Object
	*/ 
	private static User QueryClientUserBasedOnAccountId(String accountId) {
		return [
			SELECT
				Id,
				Name,
				AccountId
			FROM User
			WHERE AccountId = :accountId
		];
	}

	/**
	* @description Get the Latest Completed Check-In for an account
	* @param accountId The Id of the account
	* @return Return the latest completed Check-In
	*/ 
	private static Check_In__c LatestCompletedCheckIn(String accountId) {
		return [
			SELECT
				Id,
				Name,
				Check_In_Number_v2__c,
				Nutrition_Planning_Mode__c,
				Guided_Plan_Number_Weeks__c,
				Stage__c,
				Nutrition_Plan_Start_Date__c,
				Nutrition_Plan_End_Date__c
			FROM Check_In__c
			WHERE Account__c = :accountId AND Stage__c = 'Completed'
			ORDER BY Scheduled_Check_In_Date__c DESC
			LIMIT 1
		];
	}


	/**
	* @description Query the Client Planner Information
	* @param accountId The Id of the Account
	* @return Returns the Client Planner Information
	*/ 
	@AuraEnabled
	public static ClientPlannerInformation GetClientPlannerInformation(String accountId) {

		//Query the Active user (If no Account Id is provided then query the current user)
		User activeUser = null;
		if(accountId == null) {
			activeUser = QueryClientUserBasedOnUserId(UserInfo.getUserId());
		} else {
			activeUser = QueryClientUserBasedOnAccountId(accountId);
		}

        Check_In__c latestCheckIn = LatestCompletedCheckIn(activeUser.AccountId);
        
		//Client Planner Information
		ClientPlannerInformation cPlanInformation	= new ClientPlannerInformation();
		cPlanInformation.userInfo					= new UserInformation(activeUser);
		cPlanInformation.clientPlans				= QueryAllClientPlans(latestCheckIn.Id);
		cPlanInformation.latestCheckInInfo			= new CheckInInformation(latestCheckIn);

		return cPlanInformation;

	}

	/**
	* @description Delete Plan
	* @param planId The Id of the plan to delete
	*/ 
	@AuraEnabled
	public static void DeletePlan(String planId) {
		Plan__c p = [
			SELECT 
				Id 
			FROM Plan__c 
			WHERE Id = :planId
		];
		delete p;
	}

	/**
	* @description Add the Client Plan to the Template Pool
	* @param checkInId The Id of the Check-In
	* @param newPlanId The Id of the plan
	*/ 
	@AuraEnabled
	public static void AddClientPlanToPool(String checkInId, String newPlanId) {
		RecordType nutRecordType = [
			SELECT 
				Id 
			FROM RecordType 
			WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Template_Nutrition_Pool_Plan'
		]; 

		Check_In__c checkIn = [
			SELECT
				Id,
				Account__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		Pool_Plan_Version__c newTemplatePoolPlan	= new Pool_Plan_Version__c();
		newTemplatePoolPlan.Check_In__c				= checkIn.Id;
		newTemplatePoolPlan.Account__c				= checkIn.Account__c;
		newTemplatePoolPlan.Plan_Type__c			= 'Nutrition Client Plan';
		newTemplatePoolPlan.Reference_Plan__c		= newPlanId;
		newTemplatePoolPlan.RecordTypeId			= nutRecordType.Id;
		insert newTemplatePoolPlan;
	}

	/**
	* @description Get all Template Pool Plans for a Check-In
	* @param checkInId The Id of the Check-In
	* @return Returns the Client Plans related to the Check-In
	*/ 
	private static List<PoolPlan> GetAllTemplatePoolsForCheckIn(String checkInId) {

		List<Pool_Plan__c> poolPlans = [
			SELECT
				Id,
				(
					SELECT
						Id,
						Reference_Plan__c,
						Reference_Plan__r.Id,
						Reference_Plan__r.Name
					FROM Pool_Plan_Versions__r
					WHERE Active_Version__c = true
				)
			FROM Pool_Plan__c
			WHERE Check_In__c = :checkInId
		];

		List<PoolPlan> cPlans = new List<PoolPlan>();
		for(Pool_Plan__c poolPlan : poolPlans) {
			cPlans.add(new PoolPlan(poolPlan));
		}
		return cPlans;

	}

}