/**
* @author Harrison Campbell
* @description Lightning Controller for 'plannerXTemplatePool' lightning web component
*/ 
public without sharing class AT_LWCC_PlannerTemplatePool  {

	/**
	* @author Harrison Campbell
	* @description Stores information on a Template Plan
	*/ 
	public class TemplatePlan {

		/**
		* @description The Id of the Template Plan
		*/ 
		@AuraEnabled
		public String id { get; set; }
		
		/**
		* @description The Name of the Template Plan
		*/ 
		@AuraEnabled
		public String name { get; set; }
		
		/**
		* @description The total protein of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal protein { get; set; }
		
		/**
		* @description The total carbs of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal carbs { get; set; }
		
		/**
		* @description The total fat of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal fat { get; set; }
		
		/**
		* @description The total calories of the Template Plan
		*/ 
		@AuraEnabled
		public Decimal calories { get; set; }

		/**
		* @description Construct a Template Plan
		* @param tmpPlan The Template Plan
		*/ 
		public TemplatePlan(Template_Plan__c tmpPlan) {
			this.id			= tmpPlan.Id;
			this.name		= tmpPlan.Name;
			this.protein	= tmpPlan.Template_Plan_Protein__c;
			this.carbs		= tmpPlan.Template_Plan_Carbs__c;
			this.fat		= tmpPlan.Template_Plan_Fat__c;
			this.calories	= tmpPlan.Template_Plan_Calories__c;
		}

	}

	private static Check_In__c GetCheckInInformation(String checkInId) {
		return [
			SELECT
				Id,
				Account__c,
				Template_Pool_Approach__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];
	}

	private static void AddNewTemplatePools(Check_In__c checkIn, List<String> templateIds, String templatePoolApproach) {

		RecordType templatePoolItemRType	= [
			SELECT 
				Id 
			FROM RecordType 
			WHERE SobjectType = 'Pool_Plan_Version__c' AND DeveloperName = 'Template_Nutrition_Pool_Plan'
		];

        RecordType rPlanType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Reference_Plan'];
        
        Map<Id, Plan__c> refPlans = AT_M_Nutrition.CreatePlansFromTemplateIds(templateIds, rPlanType, checkIn.Id);

        //Create the Pool Plans and the first version for each of them
        Map<Id, Pool_Plan__c> newPoolPlans = new Map<Id, Pool_Plan__c>();
        for(String tmpId : templateIds) {
            Pool_Plan__c newPoolPlan    = new Pool_Plan__c();
            newPoolPlan.Account__c      = checkIn.Account__c;
            newPoolPlan.Check_In__c     = checkIn.Id;
            newPoolPlan.Source_Type__c  = templatePoolApproach;
            newPoolPlans.put(tmpId, newPoolPlan);
        }
        insert newPoolPlans.values();

        List<Pool_Plan_Version__c> newVersions = new List<Pool_Plan_Version__c>();
        for(String tmpId : templateIds) {
            Pool_Plan_Version__c newPoolPlanVersion    = new Pool_Plan_Version__c();
            newPoolPlanVersion.Reference_Plan__c        = refPlans.get(tmpId).Id;
            newPoolPlanVersion.Pool_Plan__c             = newPoolPlans.get(tmpId).Id;
            newPoolPlanVersion.Active_Version__c        = true;
            newPoolPlanVersion.Version__c               = 1;
            newVersions.add(newPoolPlanVersion);
        }
        insert newVersions;

	}

	private static void AddRemoveTemplates(Check_In__c checkIn, String[] templateIds, String templatePoolType) {

        List<Pool_Plan__c> checkInPoolPlans = [
            SELECT
                Id
            FROM Pool_Plan__c
            WHERE Check_In__c = :checkIn.Id
        ];

        List<Pool_Plan_Version__c> poolPlanVersions = new List<Pool_Plan_Version__c>();
        for(Pool_Plan__c pPlan : checkInPoolPlans) {
            poolPlanVersions.addAll(pPlan.Pool_Plan_Versions__r);
        }
        if(poolPlanVersions.size() > 0) {
            delete poolPlanVersions;
        }
        if(checkInPoolPlans.size() > 0) {
            delete checkInPoolPlans;
        }

		AddNewTemplatePools(checkIn, templateIds, templatePoolType);

	}
	
	/**
	* @description Get all of the Nutrition Template Plans in the system
	* @return Returns the Nutrition Template Plans
	*/ 
	@AuraEnabled
	public static List<TemplatePlan> GetAllNutritionTemplatePlans() {

		List<Template_Plan__c> allNutTemplatePlans = [
			SELECT
				Id,
				Name,
				Template_Plan_Protein__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Calories__c
			FROM Template_Plan__c
			WHERE RecordType.DeveloperName = 'Template_Nutrition_Plan'
			ORDER BY Name ASC
		];

		List<TemplatePlan> allTemplatePlans = new List<TemplatePlan>();
		for(Template_Plan__c tmpPlan : allNutTemplatePlans) {
			allTemplatePlans.add(new TemplatePlan(tmpPlan));
		}
		return allTemplatePlans;

	}

	/**
	* @description Get the Template Plans that have been selected
	* @return Returns the Template Plans
	*/ 
	@AuraEnabled
	public static List<TemplatePlan> GetSelectedTemplates(String checkInId) {

		List<Pool_Plan_Version__c> tmpPools = [
			SELECT
				Id,
				Template_Plan__r.Id,
				Template_Plan__r.Name,
				Template_Plan__r.Template_Plan_Protein__c,
				Template_Plan__r.Template_Plan_Carbs__c,
				Template_Plan__r.Template_Plan_Fat__c,
				Template_Plan__r.Template_Plan_Calories__c
			FROM Pool_Plan_Version__c
			WHERE Check_In__c = :checkInId AND Template_Pool_Type__c = 'Selected Templates'
		]; 
		
		List<TemplatePlan> allTemplatePlans = new List<TemplatePlan>();
		for(Pool_Plan_Version__c tmpPool : tmpPools) {
			allTemplatePlans.add(new TemplatePlan(tmpPool.Template_Plan__r));
		}
		return allTemplatePlans;
        
	}

	/**
	* @description Save the Template Ids into Template Pools
	* @param checkInId The Id of the Check-In
	* @param templatePoolApproach The approach for generating the template pools
	* @param templateIds The Ids of the Templates
	*/ 
	@AuraEnabled
	public static void SaveTemplatePools(String checkInId, String templatePoolApproach, String[] templateIds) {

		Check_In__c checkIn					= GetCheckInInformation(checkInId);
		checkIn.Template_Pool_Approach__c	= templatePoolApproach;
		update checkIn;

		if(templatePoolApproach == 'Selected Templates') {
			AddRemoveTemplates(checkIn, templateIds, 'Selected Templates');
		} else if (templatePoolApproach == 'Calorie Range'){
			AddRemoveTemplates(checkIn, templateIds, 'Calorie Range');
		}

	}

	public class TemplatePoolInformation {

		@AuraEnabled
		public String checkInNutritionPlanningMode;

		@AuraEnabled
		public List<AT_M_Nutrition.NutritionTarget> nutritionTargets;

	}

	@AuraEnabled
	public static TemplatePoolInformation GetTemplatePoolInformation(String checkInId) {

		Check_In__c checkIn = [
			SELECT
				Id,
				Nutrition_Planning_Mode__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		TemplatePoolInformation tPoolInfo		= new TemplatePoolInformation();
		tPoolInfo.checkInNutritionPlanningMode	= checkIn.Nutrition_Planning_Mode__c;
		tPoolInfo.nutritionTargets				= AT_M_Nutrition.GetCheckInTargets(checkInId);
		return tPoolInfo;
	}
	
	private static List<Template_Plan__c> p_queryTemplatePlansInCalorieRange(Targets__c nutritionTarget) {
		
		Decimal targetCalories	= nutritionTarget.Calories__c;
		Decimal diff			= targetCalories * nutritionTarget.Calorie_Percentage__c / 100;
		Decimal minCalories		= Math.max(targetCalories - diff, 0);
		Decimal maxCalories		= targetCalories + diff;

		//All of the Template Plans
		List<Template_Plan__c> allNutTemplatePlans = [
			SELECT
				Id,
				Name,
				Template_Plan_Protein__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Calories__c
			FROM Template_Plan__c
			WHERE RecordType.DeveloperName = 'Template_Nutrition_Plan' AND (Template_Plan_Calories__c >= :minCalories AND Template_Plan_Calories__c <= :maxCalories) AND Status__c = 'Public'
			ORDER BY Name ASC
		];

		return allNutTemplatePlans;

	}

	private static void p_createPoolPlans(Targets__c nutritionTarget, List<Template_Plan__c> tmpPlans, String sourceType) {

		RecordType nutRefPlanRType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Reference_Plan'];

		Check_In__c checkIn = [
			SELECT
				Id,
				Account__c
			FROM Check_In__c
			WHERE Id = :nutritionTarget.Check_In__c
		];

		//Create the Pool Plans
		Map<Id, Pool_Plan__c> newPoolPlans = new Map<Id, Pool_Plan__c>(); //Mapped by the Template Plan Ids
		List<Id> tmPlanIds = new List<Id>();
		for(Template_Plan__c tPlan : tmpPlans) {
			Pool_Plan__c newPoolPlan	= new Pool_Plan__c();
			newPoolPlan.Account__c		= checkIn.Account__c;
			newPoolPlan.Check_In__c		= checkIn.Id;
			newPoolPlan.Source_Type__c	= sourceType;
			newPoolPlans.put(tPlan.Id, newPoolPlan);

			tmPlanIds.add(tPlan.Id);
		}
		insert newPoolPlans.values();

        //Create the Target/Pool Plans
        List<Targets_Pool_Plan__c> newTargetPoolPlans = new List<Targets_Pool_Plan__c>();
        for(Pool_Plan__c pp : newPoolPlans.values()) {
            Targets_Pool_Plan__c newTPP = new Targets_Pool_Plan__c();
            newTPP.Targets__c           = nutritionTarget.Id;
            newTPP.Pool_Plan__c         = pp.Id;
            newTargetPoolPlans.add(newTPP);
        }

        insert newTargetPoolPlans;
        
		
		//Create a Nutrition Reference Plan for each active version
		Map<Id, Plan__c> newRefPlans =  AT_M_Nutrition.CreatePlansFromTemplateIds(tmPlanIds, nutRefPlanRType, checkIn.Id);


		//Create the Active Version for Each Pool Plan
		Map<Id, Pool_Plan_Version__c> newPoolPlanVersions = new Map<Id, Pool_Plan_Version__c>();
		for(Template_Plan__c tPlan : tmpPlans) {
			Pool_Plan_Version__c newPoolPlanVersion = new Pool_Plan_Version__c();
			newPoolPlanVersion.Active_Version__c	= true;
			newPoolPlanVersion.Version__c			= 1;
			newPoolPlanVersion.Pool_Plan__c			= newPoolPlans.get(tPlan.Id).Id;	
			newPoolPlanVersion.Reference_Plan__c	= newRefPlans.get(tPlan.Id).Id;		
			newPoolPlanVersions.put(tPlan.Id, newPoolPlanVersion);
		}
		insert newPoolPlanVersions.values();

	}

    private static void p_deleteRelatedPoolPlans(List<Id> targetIds) {

        //Delete all of the Pool Plans related to the Target (And Pool Plan Versions, and Rel Plans)
        List<Targets_Pool_Plan__c> relTargetPoolPlans = [
            SELECT
                Id,
                Targets__c,
                Pool_Plan__c
            FROM Targets_Pool_Plan__c
            WHERE Targets__c IN :targetIds
        ];

        List<Id> poolPlanIds = new List<Id>();
        for(Targets_Pool_Plan__c tPoolPlan : relTargetPoolPlans) {
            poolPlanIds.add(tPoolPlan.Pool_Plan__c);
        }

        List<Pool_Plan__c> relPoolPlans = [
            SELECT
                Id
            FROM Pool_Plan__c
            WHERE Id IN :poolPlanIds
        ];

        List<Pool_Plan_Version__c> relPpVersions = new List<Pool_Plan_Version__c>();
        List<Plan__c> relPlans = new List<Plan__c>();

        for(Pool_Plan__c pp : relPoolPlans) {
            relPpVersions.addAll(pp.Pool_Plan_Versions__r);

            for(Pool_Plan_Version__c ppv : pp.Pool_Plan_Versions__r) {
                relPlans.add(ppv.Reference_Plan__r);
            }

        }

        delete relPlans;
        delete relPpVersions;
        delete relPoolPlans;
        delete relTargetPoolPlans;

    }

	@AuraEnabled
	public static void UpdateCaloriePercentage(String targetId, Decimal percentage) {
		Targets__c nutritionTarget = [
			SELECT
				Id,
				Check_In__c,
				Calories__c,
				Calorie_Percentage__c
			FROM Targets__c
			WHERE Id = :targetId
		];

		nutritionTarget.Calorie_Percentage__c = percentage;
		update nutritionTarget;

        p_deleteRelatedPoolPlans(new Id[] { targetId });

		//After we have updated the Nutrition Target calorie perentage, Query all of the Template Plans related to the Target
		List<Template_Plan__c> tmpPlans = p_queryTemplatePlansInCalorieRange(nutritionTarget);
		p_createPoolPlans(nutritionTarget, tmpPlans, 'Calorie Range');

	}

	@AuraEnabled
	public static void UpdateSelectedTemplates(String targetId, List<String> templateIds) {
		Targets__c nutritionTarget = [
			SELECT
				Id,
				Check_In__c,
				Calories__c,
				Calorie_Percentage__c
			FROM Targets__c
			WHERE Id = :targetId
		];
        
		//Delete all of the Pool Plans related to the Target
        p_deleteRelatedPoolPlans(new Id[] { targetId });
      
		
		//All of the Template Plans
		List<Template_Plan__c> tmpPlans = [
			SELECT
				Id,
				Name,
				Template_Plan_Protein__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Calories__c
			FROM Template_Plan__c
			WHERE RecordType.DeveloperName = 'Template_Nutrition_Plan' AND Id IN :templateIds
			ORDER BY Name ASC
		];

		p_createPoolPlans(nutritionTarget, tmpPlans, 'Selected Templates');


	}

	@AuraEnabled
	public static void SetAsDefaultPoolPlanId(String targetId, String poolPlanId) {
      
		Targets__c target = [
			SELECT
				Id,
				Default_Target_Sub_Pool_Plan__c
			FROM Targets__c
			WHERE Id = :targetId
		];

		target.Default_Target_Sub_Pool_Plan__c = poolPlanId;
		update target;

	}

	@AuraEnabled
	public static void SetPoolType(String targetId, String newPoolType) {
		Targets__c target = [
			SELECT
				Id,
                Calorie_Percentage__c,
				Target_Pool_Type__c
			FROM Targets__c
			WHERE Id = :targetId
		];
		target.Target_Pool_Type__c = newPoolType;
		update target;


		if(target.Target_Pool_Type__c == 'Calorie Range') {
			UpdateCaloriePercentage(target.Id, target.Calorie_Percentage__c);
		} else if(target.Target_Pool_Type__c == 'Selected Templates') {

            //Query the Template/Pool Plans, Query the Pool Plans, Query the Pool Plan Versions
            //Add delete them all
            List<Targets_Pool_Plan__c> relTPoolPlans = [
                SELECT
                    Id,
                    Pool_Plan__c
                FROM Targets_Pool_Plan__c
                WHERE Targets__c = :targetId
            ];

            List<Id> poolPlanIds = new List<Id>();
            for(Targets_Pool_Plan__c tpp : relTPoolPlans) {
                poolPlanIds.add(tpp.Pool_Plan__c);
            }
            List<Pool_Plan__c> relPoolPlans = [
                SELECT
                    Id
                FROM Pool_Plan__c
                WHERE Id = :poolPlanIds
            ];
            List<Pool_Plan_Version__c> relPoolPlanVersions = new List<Pool_Plan_Version__c>();

            for(Pool_Plan__c pp : relPoolPlans) {
                relPoolPlanVersions.addAll(pp.Pool_Plan_Versions__r);
            }

            delete relPoolPlanVersions;
            delete relPoolPlans;
            delete relTPoolPlans;

		}

	}

}