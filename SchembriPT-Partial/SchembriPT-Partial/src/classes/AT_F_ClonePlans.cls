/**
* @author Harrison Campbell
* @description Flow action to clone plans to a targeted Check-In
*/ 
public without sharing class AT_F_ClonePlans {

	/**
	* @description Query the Check-In
	* @param checkInId The Id of the Check-In
	* @return Returns the Check-In
	*/ 
	private static Check_In__c QueryCheckIn(String checkInId) {
		return [
			SELECT
				Id,
				Account__c,
				Nutrition_Plan_Start_Date__c,
				Nutrition_Plan_End_Date__c
			FROM Check_In__c
			WHERE Id =: checkInId
		];
	}

	/**
	* @description Query the Plans
	* @param planIds The Ids of the plans
	* @return Returns the Plans, mapped by their Ids
	*/ 
	private static Map<Id, Plan__c> QueryPlans(List<Id> planIds) {
		return new Map<Id, Plan__c>([
			SELECT
				Id,
				Name,
				Weekdays__c,
				Template_Plan__c,
				RecordTypeId,
				Client_Plan_Protein__c,
				Client_Plan_Carbs__c,
				Client_Plan_Fat__c,
				Client_Plan_Calories__c,
				Trainer_Plan_Protein__c,
				Trainer_Plan_Carbs__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Calories__c,
				RecordType.DeveloperName
			FROM Plan__c
			WHERE Id IN :planIds
		]);
	}

	/**
	* @description Query the Blocks
	* @param planIds The Ids of the parent plans
	* @return Returns the Blocks, mapped by their Ids
	*/ 
	private static Map<Id, Block__c> QueryBlocks(List<Id> planIds) {
		return new Map<Id, Block__c>([
			SELECT
				Id,
				Name,
				Plan__c,
				Block_Short_Name__c,
				Template_Plan_Block__c,
				RecordTypeId,
				RecordType.DeveloperName,
				Display_Order__c,
				Template_Block_v2__c,
				Trainer_Block_Protein__c,
				Trainer_Block_Carbs__c,
				Trainer_Block_Fat__c,
				Trainer_Block_Calories__c,
				Client_Block_Protein__c,
				Client_Block_Carbs__c,
				Client_Block_Fat__c,
				Client_Block_Calories__c,
				Template_Block__c
			FROM Block__c
			WHERE Plan__c IN :planIds
		]);
	}

	/**
	* @description Query the Items
	* @param blockIds The Ids of the parent blocks
	* @return Returns the Items, mapped by their Ids
	*/ 
	private static Map<Id, Item__c> QueryItems(Set<Id> blockIds) {
		return new Map<Id, Item__c>([
			SELECT
				Id,
				Name,
				Block__c,
				Template_Block_Item__c,
				Template_Block_Item__r.Quantity__c,
				Template_Minimum_Quantity__c,
				Template_Measurement__c,
				Template_Quantity_Increment__c,
				RecordTypeId,
				Display_Order__c,
				Template_Item_Id__c,
				Trainer_Quantity__c,
				Trainer_Protein__c,
				Trainer_Carbs__c,
				Trainer_Fat__c,
				Trainer_Calories__c,
				Reference_Plan_Item__r.Template_Item_Id__c,
				Client_Quantity__c,
				Client_Protein__c,
				Client_Carbs__c,
				Client_Fat__c,
				Client_Calories__c
			FROM Item__c
			WHERE Block__c IN :blockIds
		]);
	}
	
	/**
	* @description Query the Template Plans
	* @param plans The Plans that are using these Template Plans
	* @return Returns the Template Plans, mapped by their Ids
	*/ 
	private static Map<Id, Template_Plan__c> QueryTemplatePlans(List<Plan__c> plans) {

		Set<Id> tempPlanIds = new Set<Id>();
		for(Plan__c p : plans) {
			tempPlanIds.add(p.Template_Plan__c);
		}

		return new Map<Id, Template_Plan__c>([
			SELECT
				Id,
				Template_Plan_Calories__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Protein__c
			FROM Template_Plan__c
			WHERE Id = :tempPlanIds
		]);
		
	}

	/**
	* @description Query the Template Blocks
	* @param blocks The Blocks that are using the Template Blocks
	* @return Returns the Template Blocks, mapped by their Ids
	*/ 
	private static Map<Id, Template_Block__c> QueryTemplateBlocks(List<Block__c> blocks) {
	
		Set<Id> tempBlkIds = new Set<Id>();
		for(Block__c p : blocks) {
			tempBlkIds.add(p.Template_Block_v2__c);
		}

		return new Map<Id, Template_Block__c>([
			SELECT
				Id,
				Template_Block_Calories__c,
				Template_Block_Carbs__c,
				Template_Block_Fat__c,
				Template_Block_Protein__c
			FROM Template_Block__c
			WHERE Id = :tempBlkIds
		]);
		
	}

	/**
	* @description Query the Template Items
	* @param items The Items that are using the Template Items
	* @return Returns the Template Items, mapped by their Ids
	*/ 
	private static Map<Id, Template_Item__c> QueryTemplateItems(List<Item__c> items) {
		
		Map<String, RecordType> allRecordTypes = QueryAllRecordTypes();
		RecordType nSessItemRType	= allRecordTypes.get('Item__c-Nutrition_Session_Item');
		RecordType nRefItemRType	= allRecordTypes.get('Item__c-Nutrition_Reference_Item');

		Set<Id> tempItemIds = new Set<Id>();
		for(Item__c i : items) {
			if(i.RecordTypeId == nRefItemRType.Id) {
				tempItemIds.add(i.Template_Item_Id__c);
			} else if(i.RecordTypeId == nSessItemRType.Id) {
				tempItemIds.add(i.Template_Item_Id__c);
			}
        }
        
        return new Map<Id, Template_Item__c>([
            SELECT
            	Id,
				Name,
				Minimum_Quantity__c,
				Measurement__c,
				Quantity_Increment__c,
				Calories__c,
				Carbs__c,
				Fat__c,
				Protein__c,
				Item_Quantity__c
            FROM Template_Item__c
            WHERE Id IN :tempItemIds
        ]);
		
	}

	/**
	* @description Query Record Type
	* @param sObjectName The SObject that contains the record type
	* @param developerName The Developer Name of the Record
	* @return Returns the Record Type
	*/ 
	private static RecordType QueryRecordType(String sObjectName, String developerName) {
		return [SELECT Id FROM RecordType WHERE SobjectType = :sObjectName AND DeveloperName = :developerName];
	}

	private static Map<String, RecordType> QueryAllRecordTypes() {
		
		List<RecordType> rTypes = [
			SELECT
				Id,
				SobjectType,
				DeveloperName
			FROM RecordType
			WHERE (SobjectType = 'Plan__c') OR (SobjectType = 'Block__c') OR (SobjectType = 'Item__c')
		];

		Map<String, RecordType> rTypeMap = new Map<String, RecordType>();

		for(RecordType rType : rTypes) {
			rTypeMap.put(rType.SobjectType + '-' + rType.DeveloperName, rType);
		}

		return rTypeMap;
	}

	private static void RollupNutritionReferenceValues(Map<Id, Plan__c> newPlans, Map<Id, Block__c> newBlocks, Map<Id, Item__c> newItems, Map<Id, List<Block__c>> planBlocks,  Map<Id, List<Item__c>> blockItems, RecordType nutritionReferencePlanRType, RecordType nutritionReferenceBlockRType, RecordType nutritionReferenceItemRType) {
	
        //For reference blocks, and plans roll up the values
		for(Block__c nBlock :  newBlocks.values()) {

			if(nBlock.RecordTypeId != nutritionReferenceBlockRType.Id) {
				continue;
			}

			Decimal bProtein	= 0;
			Decimal bCarbs		= 0;
			Decimal bFat		= 0;
			Decimal bCalories	= 0;

			Decimal bTemplateProtein	= 0;
			Decimal bTemplateCarbs		= 0;
			Decimal bTemplateFat		= 0;
			Decimal bTemplateCalories	= 0;

			if(blockItems.containsKey(nBlock.Id)) {
				for(Item__c nBItem : blockItems.get(nBlock.Id)) {
					if(nBItem.RecordTypeId == nutritionReferenceItemRType.Id) {
						bProtein			+= nBItem.Trainer_Protein__c;
						bCarbs				+= nBItem.Trainer_Carbs__c;
						bFat				+= nBItem.Trainer_Fat__c;
						bCalories			+= nBItem.Trainer_Calories__c;
						bTemplateProtein	+= nBItem.Template_Protein__c;
						bTemplateCarbs		+= nBItem.Template_Carbs__c;
						bTemplateFat		+= nBItem.Template_Fat__c;
						bTemplateCalories	+= nBItem.Template_Calories__c;
					}
				}
			}

			if(nBlock.RecordTypeId == nutritionReferenceBlockRType.Id) {
				nBlock.Trainer_Block_Protein__c		= bProtein;
				nBlock.Trainer_Block_Carbs__c		= bCarbs;
				nBlock.Trainer_Block_Fat__c			= bFat;
				nBlock.Trainer_Block_Calories__c	= bCalories;
				nBlock.Template_Block_Protein__c	= bTemplateProtein;
				nBlock.Template_Block_Carbs__c		= bTemplateCarbs;
				nBlock.Template_Block_Fat__c		= bTemplateFat;
				nBlock.Template_Block_Calories__c	= bTemplateCalories;
			} 

		}
		update newBlocks.values();
        
     	//Rollup the block values for the plans
		for(Plan__c nPlan : newPlans.values()) {
		
			if(nPlan.RecordTypeId != nutritionReferencePlanRType.Id) {
				continue;
			}

			Decimal pProtein	= 0;
			Decimal pCarbs		= 0;
			Decimal pFat		= 0;
			Decimal pCalories	= 0;
						
			Decimal pTemplateProtein	= 0;
			Decimal pTemplateCarbs		= 0;
			Decimal pTemplateFat		= 0;
			Decimal pTemplateCalories	= 0;

			if(planBlocks.containsKey(nPlan.Id)) {
				for(Block__c nPBlock : planBlocks.get(nPlan.Id)) {
					if(nPBlock.RecordTypeId == nutritionReferenceBlockRType.Id) {
						pProtein			+= nPBlock.Trainer_Block_Protein__c;
						pCarbs				+= nPBlock.Trainer_Block_Carbs__c;
						pFat				+= nPBlock.Trainer_Block_Fat__c;
						pCalories			+= nPBlock.Trainer_Block_Calories__c;
						pTemplateProtein	+= nPBlock.Template_Block_Protein__c;
						pTemplateCarbs		+= nPBlock.Template_Block_Carbs__c;
						pTemplateFat		+= nPBlock.Template_Block_Fat__c;
						pTemplateCalories	+= nPBlock.Template_Block_Calories__c;

					}
				}
			}

			if(nPlan.RecordTypeId == nutritionReferencePlanRType.Id) {
				nPlan.Trainer_Plan_Protein__c	= pProtein;
				nPlan.Trainer_Plan_Carbs__c		= pCarbs;
				nPlan.Trainer_Plan_Fat__c		= pFat;
				nPlan.Trainer_Plan_Calories__c	= pCalories;
				nPlan.Template_Plan_Protein__c	= pTemplateProtein;
				nPlan.Template_Plan_Carbs__c	= pTemplateCarbs;
				nPlan.Template_Plan_Fat__c		= pTemplateFat;
				nPlan.Template_Plan_Calories__c = pTemplateCalories;
			}

		}
		update newPlans.values();

	}

	/**
	* @description Clone plans to a target Check-In
	* @param planIds The plans to clone
	* @param tCheckInId The target Check-In Id
	*/ 
	private static Result ClonePlansToCheckIn(List<Id> planIds, Id tCheckInId) {

		//Get the Record Ids for the Plan/Block/Items
		Map<String, RecordType> allRecordTypes = QueryAllRecordTypes();
		RecordType nutritionSessionPlanRType	= allRecordTypes.get('Plan__c-Nutrition_Session_Plan');
		RecordType nutritionSessionBlockRType	= allRecordTypes.get('Block__c-Nutrition_Session_Block');
		RecordType nutritionSessionItemRType	= allRecordTypes.get('Item__c-Nutrition_Session_Item');
		RecordType nutritionReferencePlanRType	= allRecordTypes.get('Plan__c-Nutrition_Reference_Plan');
		RecordType nutritionReferenceBlockRType	= allRecordTypes.get('Block__c-Nutrition_Reference_Block');
		RecordType nutritionReferenceItemRType	= allRecordTypes.get('Item__c-Nutrition_Reference_Item');
		
		System.debug('Target Check-In Id: ' + tCheckInId);
		Check_In__c targetCheckIn = QueryCheckIn(tCheckInId);

		Map<Id, Plan__c> plansToClone		= QueryPlans(planIds);
		Map<Id, Block__c> blocksToClone		= QueryBlocks(planIds);
		Map<Id, Item__c> itemsToClone		= QueryItems(blocksToClone.keySet());

		Map<Id, Template_Plan__c> tmpPlanMap 	= QueryTemplatePlans(plansToClone.values());
        Map<Id, Template_Block__c> tmpBlockMap 	= QueryTemplateBlocks(blocksToClone.values());
        Map<Id, Template_Item__c> tmpItemMap 	= QueryTemplateItems(itemsToClone.values());

        Map<Id, List<Block__c>> planBlocks 	= new Map<Id, List<Block__c>>();
        Map<Id, List<Item__c>> blockItems 	= new Map<Id, List<Item__c>>();

        Map<Id, Plan__c> newPlans = new Map<Id, Plan__c>();

		for(Plan__c p : plansToClone.values()) {

			Plan__c newPlan					= new Plan__c();
			newPlan.Name					= p.Name;
			newPlan.RecordTypeId			= p.RecordTypeId;
			newPlan.Cloned_Plan__c			= p.Id;
			newPlan.Check_In__c				= targetCheckIn.Id;
			newPlan.Account__c				= targetCheckIn.Account__c;
			
			if(p.RecordTypeId == nutritionReferencePlanRType.Id) {			
				newPlan.Template_Plan__c			= p.Template_Plan__c;
				newPlan.Weekdays__c					= p.Weekdays__c;
				newPlan.Check_In_nr__c				= targetCheckIn.Id;
				newPlan.Account_nr__c				= targetCheckIn.Account__c;
				newPlan.Reference_Start_Date__c		= targetCheckIn.Nutrition_Plan_Start_Date__c;
				newPlan.Reference_End_Date__c		= targetCheckIn.Nutrition_Plan_End_Date__c;
			} else if(p.RecordTypeId == nutritionSessionPlanRType.Id) {
				newPlan.Check_In_ns__c				= targetCheckIn.Id;
				newPlan.Account_ns__c				= targetCheckIn.Account__c;
				
				newPlan.Trainer_Plan_Protein__c		= p.Trainer_Plan_Protein__c;
				newPlan.Trainer_Plan_Carbs__c		= p.Trainer_Plan_Carbs__c;
				newPlan.Trainer_Plan_Fat__c			= p.Trainer_Plan_Fat__c;
				newPlan.Trainer_Plan_Calories__c	= p.Trainer_Plan_Calories__c;

				newPlan.Client_Plan_Protein__c		= p.Client_Plan_Protein__c;
				newPlan.Client_Plan_Carbs__c		= p.Client_Plan_Carbs__c;
				newPlan.Client_Plan_Fat__c			= p.Client_Plan_Fat__c;
				newPlan.Client_Plan_Calories__c		= p.Client_Plan_Calories__c;

			}

			newPlans.put(p.Id, newPlan);

		}

		insert newPlans.values();

		Map<Id, Block__c> newBlocks = new Map<Id, Block__c>();
        
        for(Block__c blk : blocksToClone.values()) {
            
            Plan__c newRelPlan = newPlans.get(blk.Plan__c);
            
            Block__c newBlock 					= new Block__c();
            newBlock.Plan__c					= newRelPlan.Id;
            newBlock.Cloned_Block__c 			= blk.Id;
            newBlock.Name 						= blk.Name;
            newBlock.Block_Short_Name__c 		= blk.Block_Short_Name__c;
            newBlock.RecordTypeId 				= blk.RecordTypeId;
            newBlock.Display_Order__c 			= blk.Display_Order__c;

			if(blk.RecordTypeId == nutritionReferenceBlockRType.Id) {
				newBlock.Template_Plan_Block__c 	= blk.Template_Plan_Block__c;
				newBlock.Template_Block_v2__c 		= blk.Template_Block_v2__c;
			} else if(blk.RecordTypeId == nutritionSessionBlockRType.Id) {
				newBlock.Trainer_Block_Protein__c	= blk.Trainer_Block_Protein__c;				
				newBlock.Trainer_Block_Carbs__c		= blk.Trainer_Block_Carbs__c;
				newBlock.Trainer_Block_Fat__c		= blk.Trainer_Block_Fat__c;
				newBlock.Trainer_Block_Calories__c	= blk.Trainer_Block_Calories__c;

				newBlock.Client_Block_Protein__c	= blk.Client_Block_Protein__c;
				newBlock.Client_Block_Carbs__c		= blk.Client_Block_Carbs__c;
				newBlock.Client_Block_Fat__c		= blk.Client_Block_Fat__c;
				newBlock.Client_Block_Calories__c	= blk.Client_Block_Calories__c;

			}
            
            newBlocks.put(blk.Id, newBlock);
            
            if(planBlocks.containsKey(newRelPlan.Id) == false) {
                planBlocks.put(newRelPlan.Id, new List<Block__c>());
            }
            planBlocks.get(newRelPlan.Id).add(newBlock);
            
        }

        insert newBlocks.values();
        
        Map<Id, Item__c> newItems = new Map<Id, Item__c>();
        
        for(Item__c i : itemsToClone.values()) {
            
            Block__c newRelBlock = newBlocks.get(i.Block__c);
            
            Item__c newItem							= new Item__c();
            newItem.Block__c 						= newRelBlock.Id;
            newItem.Cloned_Item__c 					= i.Id;
            newItem.Name 							= i.Name;
            newItem.Template_Block_Item__c 			= i.Template_Block_Item__c;
            newItem.RecordTypeId 					= i.RecordTypeId;
            newItem.Display_Order__c 				= i.Display_Order__c;
            
			if(i.RecordTypeId == nutritionReferenceItemRType.Id) {
			
				Template_Item__c relTmpItem				= tmpItemMap.get(i.Template_Item_Id__c);
				
				newItem.Template_Block_Item_Quantity__c = i.Template_Block_Item__r.Quantity__c;
            
				Decimal proteinPerUnit 					= relTmpItem.Protein__c / relTmpItem.Item_Quantity__c;
				Decimal carbsPerUnit 					= relTmpItem.Carbs__c / relTmpItem.Item_Quantity__c;
				Decimal fatPerUnit 						= relTmpItem.Fat__c / relTmpItem.Item_Quantity__c;
				Decimal caloriesPerUnit					= relTmpItem.Calories__c / relTmpItem.Item_Quantity__c;

				newItem.Template_Minimum_Quantity__c 	= relTmpItem.Minimum_Quantity__c;
				newItem.Template_Measurement__c			= relTmpItem.Measurement__c;
				newItem.Template_Quantity_Increment__c	= relTmpItem.Quantity_Increment__c;
				newItem.Template_Item_Id__c				= relTmpItem.Id;
				newItem.Template_Item_Name__c			= relTmpItem.Name;
				newItem.Template_Calories__c 			= relTmpItem.Calories__c;
				newItem.Template_Carbs__c 				= relTmpItem.Carbs__c;
				newItem.Template_Fat__c 				= relTmpItem.Fat__c;
				newItem.Template_Protein__c 			= relTmpItem.Protein__c;
				newItem.Template_Quantity__c 			= relTmpItem.Item_Quantity__c;
            
				newItem.Trainer_Quantity__c = i.Trainer_Quantity__c;
				newItem.Trainer_Protein__c 	= i.Trainer_Quantity__c * proteinPerUnit;
				newItem.Trainer_Carbs__c 	= i.Trainer_Quantity__c * carbsPerUnit;
				newItem.Trainer_Fat__c 		= i.Trainer_Quantity__c * fatPerUnit;
				newItem.Trainer_Calories__c = i.Trainer_Quantity__c * caloriesPerUnit;

			} else if(i.RecordTypeId == nutritionSessionItemRType.Id) {

				newItem.Template_Minimum_Quantity__c 	= i.Template_Minimum_Quantity__c;
				newItem.Template_Measurement__c			= i.Template_Measurement__c;
				newItem.Template_Quantity_Increment__c	= i.Template_Quantity_Increment__c;
				
				newItem.Trainer_Quantity__c				= i.Trainer_Quantity__c;
				newItem.Trainer_Protein__c 				= i.Trainer_Protein__c;
				newItem.Trainer_Carbs__c 				= i.Trainer_Carbs__c;
				newItem.Trainer_Fat__c 					= i.Trainer_Fat__c;
				newItem.Trainer_Calories__c				= i.Trainer_Calories__c;

				newItem.Client_Quantity__c				= i.Client_Quantity__c;
				newItem.Client_Protein__c 				= i.Client_Protein__c;
				newItem.Client_Carbs__c 				= i.Client_Carbs__c;
				newItem.Client_Fat__c 					= i.Client_Fat__c;
				newItem.Client_Calories__c				= i.Client_Calories__c;

			}

            newItems.put(i.Id, newItem);

            if(blockItems.containsKey(newRelBlock.Id) == false) {
                blockItems.put(newRelBlock.Id, new List<Item__c>());
            }
            blockItems.get(newRelBlock.Id).add(newItem);
            
        }
        
        insert newItems.values();
        
		//Rollup the nutritional reference values
		RollupNutritionReferenceValues(newPlans, newBlocks, newItems, planBlocks, blockItems, nutritionReferencePlanRType, nutritionReferenceBlockRType, nutritionReferenceItemRType);

		Result results = new Result();
		results.clonedPlans = new List<AT_C_ClonedPlan>();
		for(Plan__c nPlan : newPlans.values()) {
			AT_C_ClonedPlan newClonedPlan = new AT_C_ClonedPlan();
			newClonedPlan.planId		= nPlan.Cloned_Plan__c;
			newClonedPlan.clonedPlanId	= nPlan.Id;
			results.clonedPlans.add(newClonedPlan);
		}
		return results;

	}

	@InvocableMethod(label='Clone Plans' description='Clone Plans to the next target Check-In' category='Ativa - Apex Flow Action')
	public static List<Result> ClonePlans(List<Request> reqs) { 
		List<Result> results = new List<Result>();
		for(Request r : reqs) {
			Result newResult = ClonePlansToCheckIn(r.planIds, r.targetCheckInId);
			results.add(newResult);
		}
		return results;
	}

	public class Request {

		@InvocableVariable(label='Target Check-In Id' description='The Check-In to clone the plans too' required=true)
		public String targetCheckInId;

		@InvocableVariable(label='Plan Ids' description='The Ids of the plans to clone' required=true)
		public List<String> planIds;

	}

	public class Result {

		@InvocableVariable(label='Cloned Plans' description='A list of cloned plans')
		public List<AT_C_ClonedPlan> clonedPlans;

	}


}