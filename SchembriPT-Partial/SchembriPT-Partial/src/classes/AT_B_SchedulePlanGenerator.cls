/**
* @author Harrison Campbell
* @description 
*/ 
global class AT_B_SchedulePlanGenerator implements Database.Batchable<Object>, Database.AllowsCallouts, Database.Stateful, Database.RaisesPlatformEvents {
	
    private final String category;

    private final String generationScenario;

    private final String sourceCheckInId;

    private final String targetCheckInId;

    private final List<String> referencePlanIds;

    private final List<AT_C_SessionPlanMap> sessionPlanMap;

	private final Integer currentIndex;

	private AT_B_SchedulePlanGenerator(String category, String generationScenario, String sourceCheckInId, String targetCheckInId, List<String> referencePlanIds, List<AT_C_SessionPlanMap> sessionPlanMap, Integer currentIndex) {
		this.category			= category;
		this.generationScenario = generationScenario;
		this.sourceCheckInId	= sourceCheckInId;
		this.targetCheckInId	= targetCheckInId;
		this.referencePlanIds	= referencePlanIds;
		this.sessionPlanMap		= sessionPlanMap;
		this.currentIndex		= currentIndex;
	}
	public AT_B_SchedulePlanGenerator(String category, String generationScenario, String sourceCheckInId, String targetCheckInId, List<String> referencePlanIds, List<AT_C_SessionPlanMap> sessionPlanMap) {
		this(category, generationScenario, sourceCheckInId, targetCheckInId, referencePlanIds, sessionPlanMap, 0);
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Iterable<Object> start(Database.BatchableContext context) {
		return new List<Object>();
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Object> scope) { }
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
		if(category == 'Nutrition Reference') {

			if(referencePlanIds != null) {
				
				if(currentIndex < referencePlanIds.size()) {
					callReferencePlanClone(category, referencePlanIds.get(currentIndex), targetCheckInId);
				}
				
				if(currentIndex + 1 < referencePlanIds.size()) {
					AT_B_SchedulePlanGenerator newBatch = new AT_B_SchedulePlanGenerator(category, generationScenario, sourceCheckInId, targetCheckInId, referencePlanIds, sessionPlanMap, currentIndex + 1);
					Database.executeBatch(newBatch);
				}

			}

		} else if(category == 'Nutrition Session') {

			if(generationScenario == 'Reset Reference & Session Plans') {
				PlannerController.CreateSessionPlansFromCheckIn(targetCheckInId);
			} else {
				if(sessionPlanMap != null && currentIndex < sessionPlanMap.size()) {
					AT_C_SessionPlanMap sessPlanMap = sessionPlanMap.get(currentIndex);
					callSessionPlanClone(category, generationScenario, sessPlanMap.sessionPlanId, sessPlanMap.targetDate, sessPlanMap.referencePlanId, sourceCheckInId, targetCheckInId);

					AT_B_SchedulePlanGenerator newBatch = new AT_B_SchedulePlanGenerator(category, generationScenario, sourceCheckInId, targetCheckInId, referencePlanIds, sessionPlanMap, currentIndex + 1);
					Database.executeBatch(newBatch);

				}
			}

		}

	}

    private void callReferencePlanClone(String category, String referencePlanId, String targetCheckInId) {

        Map<String, Object> params = new Map<String, Object>();
        params.put('inputCategory',             category);
        params.put('inputReferencePlanID',      referencePlanId);
        params.put('inputTargetCheckInID',      targetCheckInId);

        Flow.Interview.Reference_Plan_Clone_Source_to_Target_Check_In_1_plan newReferenceFlow = new Flow.Interview.Reference_Plan_Clone_Source_to_Target_Check_In_1_plan(params);
        newReferenceFlow.start();
    }

    private static void callSessionPlanClone(String cat, String genScenario, String sessPlanId, Date trgDate, String refPlanId, string srcCheckInId, String trgCheckInId) {
        
        Map<String, Object> params = new Map<String, Object>();
        params.put('inputCategory',             cat);
        params.put('inputGenerationScenario',   genScenario);
        params.put('inputSessionPlanID',        sessPlanId);
        params.put('inputTargetDate',           trgDate);
        params.put('inputReferencePlanID',      refPlanId);
        params.put('inputSourceCheckInID',      srcCheckInId);
        params.put('inputTargetCheckInID',      trgCheckInId);

        Flow.Interview.Session_Plan_Clone_Source_to_Target_Check_In_1_plan newSessFlow = new Flow.Interview.Session_Plan_Clone_Source_to_Target_Check_In_1_plan(params);
        newSessFlow.start();
    }



}