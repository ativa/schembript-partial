/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 12-17-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   12-17-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class Planner_Test {

	@testSetup
    static void init() {
        Account testAccount			= new Account();
		Id clientRecordId			= [SELECT Id FROM RecordType WHERE DeveloperName = 'Client' AND SobjectType = 'Account'].Id;
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		testAccount.PersonEmail		= 'test@tst.com';
		testAccount.Phone			= '0489123345';
		insert testAccount;

        Contact accountContact = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id];

        Contact testContact = new Contact(Id = accountContact.Id);

		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

        Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		testProgram.Program_Start_Date__c	= Date.today().addMonths(-1);
		testProgram.Check_In_Frequency__c	= '1 Week';

		//Create a Product for the case
        Programs_Products__c prod	= new Programs_Products__c();
		prod.Duration_weeks__c		= 12;
		prod.Name					= 'Body Revolt'; //I need to do this for the process builder
        prod.Onboarding_Process__c  = 'SPT Full Member';
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		insert testProgram;

		testProgram.Status = 'Active';
		update testProgram;

        Check_In__c checkIn = new Check_In__c(Client_Program__c = testProgram.Id, Account__c = testAccount.Id, Contact__c = testContact.Id, Check_In_Number__c = '1');

        insert checkIn;

        RecordType nutrition = [SELECT Id FROM RecordType WHERE SObjectType = 'Template_Item__c' AND DeveloperName = 'Nutrition'];

        Template_Item__c item = new Template_Item__c(
			Name				= 'Test', 
			Divisible__c		= false, 
			Increment__c		= 1, 
			Measurement__c		= 'Grams', 
			Item_Quantity__c	= 1, 
			Carbs__c			= 1, 
			Protein__c			= 1, 
			Fat__c				= 1, 
			RecordTypeId		= nutrition.Id
		);

        insert item;

        Template_Plan__c plan = new Template_Plan__c(Name = 'Test');

        insert plan;

        Template_Block__c block = new Template_Block__c(Name = 'Test');

        insert block;

        Template_Block_Item__c blockItem = new Template_Block_Item__c(
			Template_Block__c	= block.Id, 
			Template_Item__c	= item.Id, 
			Display_Order__c	= 0,
			Quantity__c			= 1
		);

        insert blockItem;

        Template_Plan_Block__c planBlock = new Template_Plan_Block__c(Order__c = 1, Template_Plan__c = plan.Id, Template_Block__c = block.Id);

        insert planBlock;


    }

    @isTest
    static void testTemplate() {
        Planner.getNutritionalItems();

        Planner.getFilters();
    }

    @isTest
    static void testPlans() {
		
        Template_Item__c item = [SELECT Id, Name FROM Template_Item__c LIMIT 1];

        Planner.Item itm	= new Planner.Item();
        itm.id				= item.Id;
        itm.name			= item.Name;

        Check_In__c checkIn = [SELECT Id FROM Check_In__c LIMIT 1];

		Planner.getCheckInInfo(checkIn.Id);

		Planner.getClientSummary(checkIn.Id);

		List<AT_LWCC_PlannerAddTemplatePlan.TemplatePlan> tmpPlans = AT_LWCC_PlannerAddTemplatePlan.GetTemplatePlans();

		AT_LWCC_PlannerAddTemplatePlan.SaveNewReferencePlanFromTemplate(checkIn.Id, 'New Name', tmpPlans.get(0).Id);

        Plan__c plan = [SELECT Id FROM Plan__c LIMIT 1];

		String planData = planner.getPlan(plan.Id);

		Planner.Plan p = (Planner.Plan)JSON.deserialize(planData, Planner.Plan.class);

		Planner.saveChangedBlocks(p.id, JSON.serialize(p.blocks));

		Template_Block__c templateBlock = [SELECT Id FROM Template_Block__c LIMIT 1];

		Planner.Block newBlock = planner.addBlock(plan.Id, 'New Block', 2, templateBlock.Id);

		String blockID = newBlock.Id;

        planner.renameBlock(blockID, 'New Block Name');

        List<Block__c> blocks = [SELECT Id FROM Block__c WHERE Plan__c = :plan.Id];

        planner.moveBlocks(blocks[0].Id, 1, blocks[1].Id, 2);

        List<Planner.Item> items = new List<Planner.Item>{itm};

		planner.addBlockItems(blockID, JSON.serialize(items, true));

		planner.deleteBlock(blockID);

		Planner.getTemplateBlocks();

		Plan__c tPlan = [
			SELECT
				Id
			FROM Plan__c
			LIMIT 1
		];

		Planner.DayPlan pAssignment = new Planner.DayPlan();
		pAssignment.planID = tPlan.Id;
		pAssignment.weekday = 'Monday';

		Planner.planNameExists(checkIn.Id, 'Hello');

    }

}