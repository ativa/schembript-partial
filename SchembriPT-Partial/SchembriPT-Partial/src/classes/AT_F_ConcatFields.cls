/**
* @author Harrison Campbell
* @description Concatonate two fields together, and maybe add a line break
*/ 
public without sharing class AT_F_ConcatFields  {

	@InvocableMethod(label='Concat Fields' description='Concatonate two fields together' category='Ativa - Apex Flow Action')
	public static void ConcatFields(List<Request> reqs) { 
		for(Request req : reqs) {
			String newValue = (String)req.srcObject.get(req.srcField);
			if(req.topOrBottom == 'Top') {
				if(req.addLineBreak == true || req.addLineBreak == null) {
					newValue = '\n' + newValue;
				}
				newValue = req.textToAdd + newValue;
			} else if(req.topOrBottom == 'Bottom') {
				if(req.addLineBreak == true || req.addLineBreak == null) {
					newValue = newValue + '\n';
				}
				newValue = newValue + req.textToAdd;
			}
			req.destObject.put(req.destField, newValue);
		}
	}


	/**
	* @author Harrison Campbell
	* @description The Concat Field Request
	*/ 
	public class Request {

		@InvocableVariable(label='Source Sobject' description='The Object that we are copying the field value from' required=true)
		public SObject srcObject;
		
		@InvocableVariable(label='Source Field' description='The Field API Name that we are copying (Attached to the Source SObject)' required=true)
		public String srcField;
				
		@InvocableVariable(label='Destination Sobject' description='The Object that we are saving the field value too' required=true)
		public SObject destObject;
				
		@InvocableVariable(label='Destination Field' description='The Field API Name that we are saving (Attached to the Destination SObject)' required=true)
		public String destField;

		@InvocableVariable(label='Top or Bottom' description='Is the new text being added above or below' required=true)
		public String topOrBottom;

		@InvocableVariable(label='Text to Add' description='The text to add' required=true)
		public String textToAdd;
		
		@InvocableVariable(label='Add Line Break' description='If true add a line break between the two texts' required=false)
		public Boolean addLineBreak;

	}

}