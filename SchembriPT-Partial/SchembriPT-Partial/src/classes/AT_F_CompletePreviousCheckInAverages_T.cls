/**
* @author Harrison Campbell
* @description Test the AT_F_CompletePreviousCheckInAverages flow action
*/ 
@isTest 
private class AT_F_CompletePreviousCheckInAverages_T {

	private static Contact testTool_createContact() {
		Contact newContact	= new Contact();
		newContact.LastName = 'Test';
		insert newContact;
		return newContact;
	}

	private static Check_In__c testTool_createCheckIn(Contact checkInContact) {
		Check_In__c testCheckIn			= new Check_In__c();
		testCheckIn.Contact__c			= checkInContact.Id;
		testCheckIn.Name				= 'Test Check-In';
		testCheckIn.h_Test_Record__c	= true;
		insert testCheckIn;
		return testCheckIn;
	}

	private static Plan__c testTool_createPlan(Check_In__c checkIn, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, Date sessionDate, Boolean completed, Id recTypeId) {
		Plan__c newPlan					= new Plan__c();
		newPlan.RecordTypeId			= recTypeId;
		newPlan.Check_In__c				= checkIn.Id;
		newPlan.Client_Plan_Protein__c	= protein;
		newPlan.Client_Plan_Carbs__c	= carbs;
		newPlan.Client_Plan_Fat__c		= fat;
		newPlan.Client_Plan_Calories__c	= calories;
		newPlan.Session_Date__c			= sessionDate;
		newPlan.Completed__c			= completed;

		if(completed == true) {
			newPlan.Actual_Plan_Protein__c	= protein;
			newPlan.Actual_Plan_Carbs__c	= carbs;
			newPlan.Actual_Plan_Fat__c		= fat;
			newPlan.Actual_Plan_Calories__c	= calories;
		}

		insert newPlan;
		return newPlan;
	}

	private static Block__c testTool_createBlock(Plan__c plan, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, Boolean completed, Id recTypeId) {
		Block__c newBlock = new Block__c();
		newBlock.RecordTypeId = recTypeId;
		newBlock.Plan__c = plan.Id;
		newBlock.Client_Block_Protein__c	= protein;
		newBlock.Client_Block_Carbs__c		= carbs;
		newBlock.Client_Block_Fat__c		= fat;
		newBlock.Client_Block_Calories__c	= calories;
		newBlock.Completed__c				= completed;
		
		if(completed == true) {
			newBlock.Actual_Block_Protein__c	= protein;
			newBlock.Actual_Block_Carbs__c		= carbs;
			newBlock.Actual_Block_Fat__c		= fat;
			newBlock.Actual_Block_Calories__c	= calories;
		}

		insert newBlock;
		return newBlock;
	}

	private static RecordType testTool_queryRecordType(String sObjectName, String developerName) {
		return [SELECT Id FROM RecordType WHERE SobjectType = :sObjectName AND DeveloperName = :developerName];
	}

	private static void assertTool_assertPlanActualsSet(Plan__c plan) {
		Plan__c retPlanData = [
			SELECT
				Id,
				Actual_Plan_Protein__c,
				Actual_Plan_Carbs__c,
				Actual_Plan_Fat__c,
				Actual_Plan_Calories__c,
				Completed__c
			FROM Plan__c
			WHERE Id = :plan.Id
		];
		System.assertEquals(true,							retPlanData.Completed__c);
		System.assertEquals(plan.Client_Plan_Protein__c,	retPlanData.Actual_Plan_Protein__c);
		System.assertEquals(plan.Client_Plan_Carbs__c,		retPlanData.Actual_Plan_Carbs__c);
		System.assertEquals(plan.Client_Plan_Fat__c,		retPlanData.Actual_Plan_Fat__c);
		System.assertEquals(plan.Client_Plan_Calories__c,	retPlanData.Actual_Plan_Calories__c);
	}

	private static void assertTool_assertBlockActualsSet(Block__c block) {
		Block__c retBlockData = [
			SELECT
				Id,
				Actual_Block_Protein__c,
				Actual_Block_Carbs__c,
				Actual_Block_Fat__c,
				Actual_Block_Calories__c,
				Completed__c
			FROM Block__c
			WHERE Id = :block.Id
		];
		System.assertEquals(true,							retBlockData.Completed__c);
		System.assertEquals(block.Client_Block_Protein__c,	retBlockData.Actual_Block_Protein__c);
		System.assertEquals(block.Client_Block_Carbs__c,	retBlockData.Actual_Block_Carbs__c);
		System.assertEquals(block.Client_Block_Fat__c,		retBlockData.Actual_Block_Fat__c);
		System.assertEquals(block.Client_Block_Calories__c, retBlockData.Actual_Block_Calories__c);
	}

	private static void assertTool_assertCheckInAveragesSet(Id checkInId, Decimal proteinAvg, Decimal carbsAvg, Decimal fatAvg, Decimal caloriesAvg) {
		Check_In__c checkIn = [
			SELECT
				Id,
				Protein_Consumed_average__c,
				Carbs_Consumed_average__c,
				Fat_Consumed_average__c,
				Calories_Consumed_average__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];
		System.assertEquals(proteinAvg,		checkIn.Protein_Consumed_average__c);
		System.assertEquals(carbsAvg,		checkIn.Carbs_Consumed_average__c);
		System.assertEquals(fatAvg,			checkIn.Fat_Consumed_average__c);
		System.assertEquals(caloriesAvg,	checkIn.Calories_Consumed_average__c);
	}
	
	private static void assertTool_assertPlanActualsNotSet(Plan__c plan) {
		Plan__c retPlanData = [
			SELECT
				Id,
				Actual_Plan_Protein__c,
				Actual_Plan_Carbs__c,
				Actual_Plan_Fat__c,
				Actual_Plan_Calories__c,
				Completed__c
			FROM Plan__c
			WHERE Id = :plan.Id
		];
		System.assertEquals(false,	retPlanData.Completed__c);
		System.assertEquals(null,	retPlanData.Actual_Plan_Protein__c);
		System.assertEquals(null,	retPlanData.Actual_Plan_Carbs__c);
		System.assertEquals(null,	retPlanData.Actual_Plan_Fat__c);
		System.assertEquals(null,	retPlanData.Actual_Plan_Calories__c);
	}

	private static void assertTool_assertBlockActualsNotSet(Block__c block) {
		Block__c retBlockData = [
			SELECT
				Id,
				Actual_Block_Protein__c,
				Actual_Block_Carbs__c,
				Actual_Block_Fat__c,
				Actual_Block_Calories__c,
				Completed__c
			FROM Block__c
			WHERE Id = :block.Id
		];
		System.assertEquals(false,	retBlockData.Completed__c);
		System.assertEquals(null,	retBlockData.Actual_Block_Protein__c);
		System.assertEquals(null,	retBlockData.Actual_Block_Carbs__c);
		System.assertEquals(null,	retBlockData.Actual_Block_Fat__c);
		System.assertEquals(null,	retBlockData.Actual_Block_Calories__c);
	}

	private static void assertTool_assertCheckInAveragesNotSet(Id checkInId) {
		Check_In__c checkIn = [
			SELECT
				Id,
				Protein_Consumed_average__c,
				Carbs_Consumed_average__c,
				Fat_Consumed_average__c,
				Calories_Consumed_average__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];
		System.assertEquals(0,	checkIn.Protein_Consumed_average__c);
		System.assertEquals(0,	checkIn.Carbs_Consumed_average__c);
		System.assertEquals(0,	checkIn.Fat_Consumed_average__c);
		System.assertEquals(0,	checkIn.Calories_Consumed_average__c);
	}

	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansNotCompletedBeforeTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, false, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 2), false, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, false, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 3), false, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, false, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2020, 1, 4);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesSet(nextCheckIn.Id, (100 + 200 + 300) / 3, (200 + 300 + 400) / 3, (300 + 400 + 500) / 3, (400 + 500 + 600) / 3);

	}
	
	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansCompletedBeforeTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), true, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, true, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 2), true, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, true, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 3), true, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, true, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2020, 1, 4);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesSet(nextCheckIn.Id, (100 + 200 + 300) / 3, (200 + 300 + 400) / 3, (300 + 400 + 500) / 3, (400 + 500 + 600) / 3);

	}
	
	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansSomeCompletedSomeNotCompletedBeforeTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, true, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 2), true, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, true, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 3), false, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, false, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2020, 1, 4);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesSet(nextCheckIn.Id, (100 + 200 + 300) / 3, (200 + 300 + 400) / 3, (300 + 400 + 500) / 3, (400 + 500 + 600) / 3);

	}
	
	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansNotCompletedOnTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, false, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, false, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, false, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2020, 1, 1);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesSet(nextCheckIn.Id, (100 + 200 + 300) / 3, (200 + 300 + 400) / 3, (300 + 400 + 500) / 3, (400 + 500 + 600) / 3);

	}
	
	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansCompletedOnTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), true, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, true, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 1), true, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, true, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 1), true, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, true, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2020, 1, 1);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesSet(nextCheckIn.Id, (100 + 200 + 300) / 3, (200 + 300 + 400) / 3, (300 + 400 + 500) / 3, (400 + 500 + 600) / 3);

	}

	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansSomeCompletedSomeNotCompletedOnTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, true, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 1), true, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, true, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, false, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2020, 1, 1);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesSet(nextCheckIn.Id, (100 + 200 + 300) / 3, (200 + 300 + 400) / 3, (300 + 400 + 500) / 3, (400 + 500 + 600) / 3);

	}

	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansNotCompletedAfterTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, false, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 2), false, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, false, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 3), false, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, false, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2019, 1, 1);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsNotSet(testPlan1);
		assertTool_assertPlanActualsNotSet(testPlan2);
		assertTool_assertPlanActualsNotSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsNotSet(testPlan1Block1);
		assertTool_assertBlockActualsNotSet(testPlan2Block1);
		assertTool_assertBlockActualsNotSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesNotSet(nextCheckIn.Id);

	}
	
	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansCompletedAfterTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), true, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, true, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 2), true, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, true, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 3), true, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, true, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2019, 1, 4);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesNotSet(nextCheckIn.Id);

	}

	@isTest
	private static void CompleteNutritionSessionPlansAndCalculateActuals_AllPlansSomeCompletedSomeNotCompletedAfterTodaysDate() {
		
		RecordType sessPlanRecType	= testTool_queryRecordType('Plan__c',	'Nutrition_Session_Plan');
		RecordType sessBlockRecType = testTool_queryRecordType('Block__c',	'Nutrition_Session_Block');
		
		Contact testContact		= testTool_createContact();
		Check_In__c prevCheckIn = testTool_createCheckIn(testContact);
		Check_In__c nextCheckIn = testTool_createCheckIn(testContact);
		nextCheckIn.Previous_Check_In__c = prevCheckIn.Id;
		update nextCheckIn;

		Plan__c testPlan1			= testTool_createPlan(prevCheckIn,	100, 200, 300, 400, Date.newInstance(2020, 1, 1), false, sessPlanRecType.Id);
		Block__c testPlan1Block1	= testTool_createBlock(testPlan1,	100, 200, 300, 400, true, sessBlockRecType.Id);
		Plan__c testPlan2			= testTool_createPlan(prevCheckIn,	200, 300, 400, 500, Date.newInstance(2020, 1, 2), true, sessPlanRecType.Id);
		Block__c testPlan2Block1	= testTool_createBlock(testPlan2,	200, 300, 400, 500, true, sessBlockRecType.Id);
		Plan__c testPlan3			= testTool_createPlan(prevCheckIn,	300, 400, 500, 600, Date.newInstance(2020, 1, 3), false, sessPlanRecType.Id);
		Block__c testPlan3Block1	= testTool_createBlock(testPlan3,	300, 400, 500, 600, false, sessBlockRecType.Id);

		AT_F_CompletePreviousCheckInAverages.Request req = new AT_F_CompletePreviousCheckInAverages.Request();
		req.checkInId = nextCheckIn.Id;

		Test.startTest();
		AT_F_CompletePreviousCheckInAverages.todaysDate = Date.newInstance(2019, 1, 4);
		AT_F_CompletePreviousCheckInAverages.A(new AT_F_CompletePreviousCheckInAverages.Request[]{ req });
		Test.stopTest();

		//Assert the Plan data is correct
		assertTool_assertPlanActualsNotSet(testPlan1);
		assertTool_assertPlanActualsSet(testPlan2);
		assertTool_assertPlanActualsNotSet(testPlan3);
		
		//Assert the Block data is correct
		assertTool_assertBlockActualsSet(testPlan1Block1);
		assertTool_assertBlockActualsSet(testPlan2Block1);
		assertTool_assertBlockActualsNotSet(testPlan3Block1);
		
		//Assert the Check-In data is correct
		assertTool_assertCheckInAveragesNotSet(nextCheckIn.Id);

	}


}