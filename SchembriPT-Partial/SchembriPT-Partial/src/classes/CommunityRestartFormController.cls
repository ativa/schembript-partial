/**
* @author 
* @description 
*/ 
global with sharing class CommunityRestartFormController {
    /**
    * @author Harrison Campbell
    * @description Holds details of a photo
    */ 
    public class Photo {

        @AuraEnabled 
		public Integer index;

        @AuraEnabled 
		public String	id;

        @AuraEnabled 
		public String	timestamp;

        @AuraEnabled 
		public String	name;

    }
    
    /**
    * @author Harrison Campbell
    * @description Holds the front side and back view photos of a user
    */ 
    public class PhotoSet {
        
		@AuraEnabled 
		public Photo front;

        @AuraEnabled 
		public Photo side;

        @AuraEnabled 
		public Photo back;

    }

	/**
	* @author Harrison Campbell	
	* @description Holds the portal settings of the page
	*/ 
	public class PortalSettings {

		@AuraEnabled
		public String portalURL { get; set; }

	}
    
	/**
	* @author Harrison Campbell
	* @description All of the fields used for the Restart Form
	*/ 
	global class RestartFormFields {
		
		@AuraEnabled
		public List<Survey_Question__c> measurementFields { get; set; }
		
		@AuraEnabled
		public List<Survey_Question__c> lifestyleFields { get; set; }
		
		@AuraEnabled
		public List<Survey_Question__c> trainingAndMedicalFields { get; set; }
		
		@AuraEnabled
		public List<Survey_Question__c> otherFields { get; set; }

		public RestartFormFields() {
			measurementFields			= new List<Survey_Question__c>();
			lifestyleFields				= new List<Survey_Question__c>();
			trainingAndMedicalFields	= new List<Survey_Question__c>();
			otherFields					= new List<Survey_Question__c>();
		}

	}

	/**
	* @author Harrison Campbell
	* @description The ids that relate to the records
	*/ 
	global class ParentRecords {
		
		@AuraEnabled
		public String checkInId { get; set; }

		@AuraEnabled
		public String accountId { get; set; }

		@AuraEnabled
		public String caseId { get; set; }

	}

	/**
	* @author Harrison Campbell
	* @description When the CommunityRestartForm is loaded, these are the initial values for it
	*/ 
	global class InitialValues {
		
        @AuraEnabled
		public String userID { get; set; }
        
		@AuraEnabled
		public PhotoSet photoSet { get; set; }

		@AuraEnabled
		global RestartFormFields formFields { get; set; }

		@AuraEnabled
		global ParentRecords parentRecords { get; set; }

		@AuraEnabled
		global PortalSettings portalSetting { get; set; }

	}

	/**
	* @description Get the Initial Values
	* @param checkInId The Id for CheckIn
	* @return Returns the Initial values
	*/ 
	@AuraEnabled 
	global static InitialValues getInitialValues(String userID) {
		InitialValues initValues = new InitialValues();
        
        initValues.userID = (userID == null) ? UserInfo.getUserId() : userID;
        
        
        User u = [SELECT Contact.Name, Contact.AccountId, Name FROM User WHERE Id = :initValues.userID];
        
		List<Case> activeCases = [
			SELECT
				Id
			FROM Case
			WHERE Status = 'On Hold' AND AccountId = :u.Contact.AccountId
			LIMIT 1 //This shouldn't be here
		];
		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Account__c,
				Client_Program__c,
				Photos__c
			FROM Check_In__c 
			WHERE Client_Program__c IN (SELECT Id FROM Case WHERE Status = 'On Hold' AND AccountId = :u.Contact.AccountId) AND Status__c = 'New'
			LIMIT 1 //This shouldn't be here
		];

		initValues.formFields				= getFields();
		//initValues.parentRecords			= getParentRecords(check);
		//
		if (!checkIns.isEmpty()) initValues.photoSet = getPhotos(checkIns[0]);

		initValues.parentRecords			= new ParentRecords();
		initValues.parentRecords.checkInId	= checkIns.isEmpty() ? null : checkIns[0].Id;
		initValues.parentRecords.accountId	= u.Contact.AccountId;
		initValues.parentRecords.caseId		= activeCases.isEmpty() ? null : activeCases[0].Id;

		initValues.portalSetting			= new PortalSettings();
		initValues.portalSetting.portalURL	= Portal_Settings__c.getInstance().Portal_URL__c;

		return initValues;
		
	}

	/**
	* @description Delete the photo on the CheckIn
	* @param checkInId The Id of the checkIn
	* @param versionId The version of the Id
	*/ 
	@AuraEnabled
	public static void deletePhoto(String checkInId, String versionId) {
		
		/*Check_In__c check = [
			SELECT
				Id,
				Photos__c
			FROM Check_In__c
			WHERE Id =: checkInId
			FOR UPDATE
		];

		List<Photo> photos = String.isNotBlank(check.Photos__c) ? (List<Photo>)JSON.deserialize(check.Photos__c, List<Photo>.class) : new List<Photo>();

		for(Integer i = 0; i < photos.size(); i++) {
			if(photos.get(i).id == versionId) {
				photos.remove(i);
				break;
			}
		}
		check.Photos__c = JSON.serializePretty(photos, true);*/
			
		Content.deleteFileFromVersion(versionId);
			
		//update check;

	}
	
    /**
    * @description Record the details of the photo on the CheckIn
    * @param clientID (Account) The Client Id
    * @param checkInID The Id of the CheckIn
    * @param name The name of the photo
    * @param documentID The Id of the document
    * @return The Version Id of the Photo
    */ 
    @AuraEnabled
    public static String recordPhoto(String clientID, String checkInID, String name, String documentID) {
		return CommunityHome.recordPhoto(checkInID, name, documentID);
    }

	/**
	* @description Get the Photos related to the CheckIn
	* @param check The CheckIn
	* @return Returns the photos
	*/ 
	public static PhotoSet getPhotos(Check_In__c check) {
	
		PhotoSet newSet = new PhotoSet();

		/*if(String.isNotBlank(check.Photos__c)) {
			List<Photo> photos = (List<Photo>)JSON.deserialize(check.Photos__c, List<Photo>.class);
		
			for (Photo p: photos) {
				if (p.name == 'front') newSet.front = p;
				if (p.name == 'side')  newSet.side  = p;
				if (p.name == 'back')  newSet.back  = p;
			}
		}*/
        
        Set<String> imageTypes = new Set<String>{'Photo Front', 'Photo Side', 'Photo Back'};

        List<ContentVersion> latest = [SELECT Id, Image_Type__c FROM ContentVersion WHERE (Check_In__c = :check.Id)];
        
        Integer index = 0;
        
        for (ContentVersion ver: [SELECT Id, Title, Image_Type__c, LastModifiedDate FROM ContentVersion WHERE (Check_In__c = :check.Id) AND (Image_Type__c IN :imageTypes)]) {
            Photo photo = new Photo();
            
            photo.id = ver.Id;
            photo.name = ver.Image_Type__c;
            photo.index = index++;
            photo.timestamp = String.valueOf(ver.LastModifiedDate);
            
            if (ver.Image_Type__c.contains('Side'))  newSet.side  = photo;
            if (ver.Image_Type__c.contains('Front')) newSet.front = photo;
            if (ver.Image_Type__c.contains('Back'))  newSet.back  = photo;
        }
        

		return newSet;

	} 

	/**
	* @description Get the Parent records of the CheckIn
	* @param check The CheckIn
	* @return Returns the Parent records of the CheckIn
	*/ 
	/*private static ParentRecords getParentRecords(Check_In__c check) {
		
		ParentRecords par	= new ParentRecords();
		par.accountId		= check.Account__c;
		par.caseId			= check.Client_Program__c;
		par.checkInId		= check.Id;
		return par;


	}*/

	/**
	* @description Get all of the fields used for the restart form
	* @return The fields
	*/ 
	private static RestartFormFields getFields() {

		List<Survey_Question__c> formQuestions = [
			SELECT 
				Id,
				Object_Name__c,
				Section_Name__c,
				Field_Name__c
			FROM Survey_Question__c
			WHERE Form_Name__c = 'Restart'
			ORDER BY Order__c ASC
		];

		RestartFormFields formFields = new RestartFormFields();

		for(Survey_Question__c survQuestion : formQuestions) {

			if(survQuestion.Section_Name__c == 'Measurements') {
				formFields.measurementFields.add(survQuestion);
			} else if(survQuestion.Section_Name__c == 'Lifestyle') {
				formFields.lifestyleFields.add(survQuestion);
			} else if(survQuestion.Section_Name__c == 'Training' || survQuestion.Section_Name__c == 'Medical') {
				formFields.trainingAndMedicalFields.add(survQuestion);
			} else if(survQuestion.Section_Name__c == 'Other') {
				formFields.otherFields.add(survQuestion);
			}

		}

		return formFields;



	}

}