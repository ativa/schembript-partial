@isTest 
private class AT_LWCC_PlannerNutritionGuidedPlans_T {

	/**
	* @description Get the picklist values for a given field
	* @param objectApiName The API name of the object
	* @param fieldApiName The API name of the field
	* @return Returns picklist values
	*/ 
	private static List<AT_LWCC_PlannerNutritionGuidedPlans.PicklistValue> GetPicklistValues(String objectApiName, String fieldApiName) {
        Map<String, Schema.SObjectType> gd			= Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map	= gd.get(objectApiName).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues	= field_map.get(fieldApiName).getDescribe().getPickListValues();       
		List<AT_LWCC_PlannerNutritionGuidedPlans.PicklistValue> optionlist = new List<AT_LWCC_PlannerNutritionGuidedPlans.PicklistValue>();
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(new AT_LWCC_PlannerNutritionGuidedPlans.PicklistValue(pv));
        }
        return optionlist;
	}

	/**
	* @description Query the Nutrition Target Record Type
	* @return 
	*/ 
	private static RecordType testTool_QueryNutritionTargetRecordType() {
		return [SELECT Id FROM RecordType WHERE SobjectType = 'Targets__c' AND DeveloperName = 'Nutrition'];
	}
    
    /**
     * @description Create a Test Check-In 
     */
	private static Check_In__c testTool_createCheckIn(Contact cont) {
		Check_In__c newCheckIn 		= new Check_In__c();
        newCheckIn.Contact__c 		= cont.Id;
		newCheckIn.h_Test_Record__c = true;
		insert newCheckIn;
		return newCheckIn;
	}
    
    /**
     * @description Create a Test Contact
     */
    private static Contact testTool_createContact() {
        Contact testContact = new Contact();
        testContact.LastName = 'Test Contact';
        insert testContact;
        return testContact;
    }

    /**
     * @description Create a test Target
     * @param checkIn The Check-In linked to the target
     * @param protein The protein of the target
     * @param carbs The carbs of the target
     * @param fat The fat of the target
     * @return Returns the new target
     */
	private static Targets__c testTool_createTarget(Check_In__c checkIn, Decimal protein, Decimal carbs, Decimal fat, String assignedWeeks, Id recordTypeId) {
		Targets__c newTarget 			= new Targets__c();
        newTarget.RecordTypeId			= recordTypeId;
		newTarget.Protein__c			= protein;
		newTarget.Carbs__c				= carbs;
		newTarget.Fat__c				= fat;
        newTarget.Assign_to_Weeks__c	= assignedWeeks;
		newTarget.Check_In__c			= checkIn.Id;
		insert newTarget;
		return newTarget;
	}
    
	private static AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget testTool_createNewTargetData(Id checkInId, String name, Decimal protein, Decimal carbs, Decimal fat, String[] assignedWeeks) {
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget nTarget = new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget();
		nTarget.checkInId	= checkInId;
		nTarget.name		= name;
		nTarget.protein		= protein;
		nTarget.carbs		= carbs;
		nTarget.fat			= fat;
		nTarget.assignedWeeks	= assignedWeeks;
		return nTarget;
	}

	private static AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget testTool_createOldTargetData(Id checkInId, String name, Decimal protein, Decimal carbs, Decimal fat, String assignedWeeks) {
		Targets__c newTarget = new Targets__c();
		newTarget.Check_In__c			= checkInId;
		newTarget.Display_Name__c		= name;
		newTarget.Protein__c			= protein;
		newTarget.Carbs__c				= carbs;
		newTarget.Fat__c				= fat;
		newTarget.Assign_to_Weeks__c	= assignedWeeks;
		insert newTarget;
		return new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget(newTarget);
	}

    private static void assertTool_assertTargetIsCorrect(Targets__c target, AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetData, Boolean newTarget) {
		if(newTarget == false) {
			System.assertEquals(target.Id, 				targetData.id);
		}
		System.assertEquals(target.Display_Name__c,		targetData.name);
        System.assertEquals(target.Protein__c, 			targetData.protein);
        System.assertEquals(target.Carbs__c, 			targetData.carbs);
        System.assertEquals(target.Fat__c, 				targetData.fat);
        //System.assertEquals(target.Calories__c, targetData.calories);
        System.assertEquals(target.Check_In__c,			targetData.checkInId);

        String[] assignToWeeks = target.Assign_to_Weeks__c.split(';');        
        System.assertEquals(assignToWeeks.size(), targetData.assignedWeeks.size());
        for(Integer i = 0; i < assignToWeeks.size(); i++) {
            System.assertEquals(assignToWeeks[i], targetData.assignedWeeks[i]);
        }
        
    }
    
    private static void assertTool_assertPicklistOptions(List<AT_LWCC_PlannerNutritionGuidedPlans.PicklistValue> expectedPicklistValues, List<AT_LWCC_PlannerNutritionGuidedPlans.PicklistValue> actualPicklistValues) {
        System.assertEquals(expectedPicklistValues.size(), actualPicklistValues.size());
        for(Integer i = 0; i < expectedPicklistValues.size(); i++) {
            System.assertEquals(expectedPicklistValues.get(i).label, actualPicklistValues.get(i).label);
            System.assertEquals(expectedPicklistValues.get(i).value, actualPicklistValues.get(i).value);
        }
    }
	
	@isTest
	private static void GetNutritionalTargets_GetTargets() {
		
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Week 1;Week 2', nutTargetRecType.Id);
        Targets__c targetTwo 		= testTool_createTarget(testCheckIn, 2, 4, 6, 'Week 3;Week 4;Week 5', nutTargetRecType.Id);
		Targets__c targetThree 		= testTool_createTarget(testCheckIn, 10, 31, 67, 'Week 6;Week 7', nutTargetRecType.Id);

        Test.startTest();
        List<AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget> targetData = AT_LWCC_PlannerNutritionGuidedPlans.GetNutritionalTargets(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(3, targetData.size());
        assertTool_assertTargetIsCorrect(targetOne, targetData.get(0), false);
        assertTool_assertTargetIsCorrect(targetTwo, targetData.get(1), false);
        assertTool_assertTargetIsCorrect(targetThree, targetData.get(2), false);
        
	}

	@isTest
	private static void GetNutritionalTargets_GetSingleTarget() {
		
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Week 1;Week 2', nutTargetRecType.Id);
        
        Test.startTest();
        List<AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget> targetData = AT_LWCC_PlannerNutritionGuidedPlans.GetNutritionalTargets(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(1, targetData.size());
        assertTool_assertTargetIsCorrect(targetOne, targetData.get(0), false);
        
	}
	
	@isTest
	private static void GetNutritionalTargets_GetNoTargets() {

        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
        
        Test.startTest();
        List<AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget> targetData = AT_LWCC_PlannerNutritionGuidedPlans.GetNutritionalTargets(testCheckIn.Id);
        Test.stopTest();
        
        System.assertEquals(0, targetData.size());
        
    }

    @IsTest
    private static void GetFlexiPlanData_GetTargetsWith10WeekOption() {
        
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);
		testCheckIn.Guided_Plan_Number_Weeks__c = 'Week 10';
		update testCheckIn;
		Targets__c targetOne 		= testTool_createTarget(testCheckIn, 0, 1, 2, 'Week 1;Week 2', nutTargetRecType.Id);
        Targets__c targetTwo 		= testTool_createTarget(testCheckIn, 2, 4, 6, 'Week 3;Week 4;Week 5', nutTargetRecType.Id);
		Targets__c targetThree 		= testTool_createTarget(testCheckIn, 10, 31, 67, 'Week 6;Week 7', nutTargetRecType.Id);

        Test.startTest();
        AT_LWCC_PlannerNutritionGuidedPlans.GuidedPlanData guidedPlanData = AT_LWCC_PlannerNutritionGuidedPlans.GetGuidedPlanData(testCheckIn.Id);
        Test.stopTest();
        
		//Assert the allergy options are correct
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Allergies_and_Intolerances__c'), 	guidedPlanData.allergies);
        assertTool_assertPicklistOptions(GetPicklistValues('Advisory__c','Dietary_Restrictions__c'), 		guidedPlanData.dietaryRestrictions);

		//Assert the targets are correct
        System.assertEquals(3, guidedPlanData.nutritionTargets.size());
        assertTool_assertTargetIsCorrect(targetOne,		guidedPlanData.nutritionTargets.get(0), false);
        assertTool_assertTargetIsCorrect(targetTwo,		guidedPlanData.nutritionTargets.get(1), false);
        assertTool_assertTargetIsCorrect(targetThree,	guidedPlanData.nutritionTargets.get(2), false);

		System.assertEquals(10, guidedPlanData.assignToWeekOptions.size());
		for(Integer i = 0; i < guidedPlanData.assignToWeekOptions.size(); i++) {
			System.assertEquals('Week ' + (i + 1), guidedPlanData.assignToWeekOptions.get(i));
		}

    }

	@IsTest
	private static void SaveNutritionalTargets_CreateNewTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		//Create new targets
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetOne		= testTool_createNewTargetData(testCheckIn.Id, 'Target 1', 1, 2, 3, new String[]{ 'Week 1', 'Week 2' });
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetTwo		= testTool_createNewTargetData(testCheckIn.Id, 'Target 2', 2, 3, 4, new String[]{ 'Week 3', 'Week 4', 'Week 5' });
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetThree	= testTool_createNewTargetData(testCheckIn.Id, 'Target 3', 3, 4, 5, new String[]{ 'Week 6', 'Week 7' });

		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionGuidedPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget[] { targetOne, targetTwo, targetThree }, null);
		Test.stopTest();

		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Assign_to_Weeks__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(3, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), targetOne, true);
		assertTool_assertTargetIsCorrect(databaseTargets.get(1), targetTwo, true);
		assertTool_assertTargetIsCorrect(databaseTargets.get(2), targetThree, true);
	}
	
	@IsTest
	private static void SaveNutritionalTargets_UpdatedOldTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		//Create new targets
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetOne		= testTool_createOldTargetData(testCheckIn.Id, 'Target 1', 0, 0, 0, 'Week 1;Week 2');
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetTwo		= testTool_createOldTargetData(testCheckIn.Id, 'Target 2', 0, 0, 0, 'Week 3;Week 4;Week 5');
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetThree	= testTool_createOldTargetData(testCheckIn.Id, 'Target 3', 0, 0, 0, 'Week 6;Week 7');

		//Update the values of the nutrition targets
		targetOne.protein	= 1;	targetOne.carbs		= 2;	targetOne.fat		= 3;
		targetTwo.protein	= 2;	targetTwo.carbs		= 3;	targetTwo.fat		= 4;
		targetThree.protein	= 3;	targetThree.carbs	= 4;	targetThree.fat		= 5;


		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionGuidedPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget[] { targetOne, targetTwo, targetThree }, null);
		Test.stopTest();

		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Assign_to_Weeks__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(3, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), targetOne, false);
		assertTool_assertTargetIsCorrect(databaseTargets.get(1), targetTwo, false);
		assertTool_assertTargetIsCorrect(databaseTargets.get(2), targetThree, false);
	}

	@IsTest
	private static void SaveNutritionalTargets_CreateNewTargetsAndUpdateOldTargets() {
        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget newTarget		= testTool_createNewTargetData(testCheckIn.Id, 'Target 1', 1, 2, 3, new String[]{ 'Week 1', 'Week 2' });
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget oldTarget		= testTool_createOldTargetData(testCheckIn.Id, 'Target 2', 0, 0, 0, 'Week 3;Week 4;Week 5;Week 6;Week 7');
		oldTarget.protein = 2; oldTarget.carbs = 4; oldTarget.fat = 6;
		
		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionGuidedPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget[] { newTarget, oldTarget }, null);
		Test.stopTest();
		
		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Assign_to_Weeks__c,
				Check_In__c,
				Weekdays__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(2, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), oldTarget, false);
		assertTool_assertTargetIsCorrect(databaseTargets.get(1), newTarget, true);
	}
	
	@IsTest
	private static void SaveNutritionalTargets_CreateNewTargetsWithNoCheckInId() {

        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget newTarget		= testTool_createNewTargetData(null, 'Target 1', 1, 2, 3, new String[]{ 'Week 1', 'Week 2' });
		
		Boolean exceptionCaught = false;
		Test.startTest();
		try{	        
			AT_LWCC_PlannerNutritionGuidedPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget[] { newTarget }, null);
		} catch (AT_LWCC_PlannerNutritionGuidedPlans.MissingFieldException e){
			exceptionCaught = true;
		}
		Test.stopTest();

		System.assert(exceptionCaught);
		
	}

	@IsTest
	private static void SaveNutritionalTargets_UpdateOldTargetsAndDeleteTargets() {

        RecordType nutTargetRecType = testTool_QueryNutritionTargetRecordType();
        Contact testContact			= testTool_createContact();
		Check_In__c testCheckIn 	= testTool_createCheckIn(testContact);

		//Create new targets
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetOne		= testTool_createOldTargetData(testCheckIn.Id, 'Target 1', 0, 0, 0, 'Week 1;Week 2');
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetTwo		= testTool_createOldTargetData(testCheckIn.Id, 'Target 2', 0, 0, 0, 'Week 3;Week 4;Week 5');
		AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget targetThree	= testTool_createOldTargetData(testCheckIn.Id, 'Target 3', 0, 0, 0, 'Week 6;Week 7');

		//Update the values of the nutrition targets
		targetOne.protein	= 1;	targetOne.carbs		= 2;	targetOne.fat		= 3;


		//Add them to the database
		Test.startTest();
		AT_LWCC_PlannerNutritionGuidedPlans.SaveNutritionalTargets(new AT_LWCC_PlannerNutritionGuidedPlans.NutritionalTarget[] { targetOne }, new String[] { targetTwo.id, targetThree.id });
		Test.stopTest();

		//Assert that that the new targets were added
		List<Targets__c> databaseTargets = [
			SELECT
				Id,
				Display_Name__c,
				Check_In__c,
				Weekdays__c,
				Assign_to_Weeks__c,
				Protein__c,
				Fat__c,
				Carbs__c,
				Calories__c
			FROM Targets__c
		];

		//Assert Targets are correct
		System.assertEquals(1, databaseTargets.size());
		assertTool_assertTargetIsCorrect(databaseTargets.get(0), targetOne, false);

	}

}