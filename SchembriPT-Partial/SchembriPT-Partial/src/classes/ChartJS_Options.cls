/**
* @author Harrison Campbell
* @description The options for the charts
*/ 
public class ChartJS_Options {

	@AuraEnabled
	public ChartJS_Scales scales { get; set; }

	@AuraEnabled
	public ChartJS_Legend legend { get; set; }

	@AuraEnabled
	public ChartJS_Layout layout { get; set; }

	@AuraEnabled
	public ChartJS_Plugins plugins { get; set; }

	@AuraEnabled
	public ChartJS_Title title { get; set; }

	@AuraEnabled
	public ChartJS_AnnotationPlugin annotation { get; set; }

}