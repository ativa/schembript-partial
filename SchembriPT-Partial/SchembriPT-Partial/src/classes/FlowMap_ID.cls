public class FlowMap_ID {
    
    @AuraEnabled
    public string SourceID; //Old or source ID
    
    @AuraEnabled
    public string TargetID; //New or target ID
    
    public FlowMap_ID(){}
}