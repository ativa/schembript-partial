public without sharing class AT_LC_NutritionPlannerValidation {

    public class ValidationCheck {

        /**
        * @description Is the Planner Valid
        */ 
        @AuraEnabled
        public Boolean IsValid { get; set; }

        @AuraEnabled
        public List<ValidationRule> validationRules { get; set; }

    }

    public class ValidationRule {

        @AuraEnabled
        public Boolean IsValid { get; set; }

        @AuraEnabled
        public String Message { get; set; }

    }

    private static ValidationRule p_areAllDaysAssigned(String checkInId) {

        Check_In__c checkIn = [
            SELECT
                Id,
                Nutrition_Planning_Mode__c,
				Guided_Plan_Number_Weeks__c
            FROM Check_In__c
            WHERE Id = :checkInId
        ];

		List<Targets__c> checkInTargets = [
			SELECT
				Id,
				Weekdays__c,
				Assign_to_Weeks__c
			FROM Targets__c
			WHERE Check_In__c = :checkInId
		];
		
        ValidationRule vRule = new ValidationRule();
		if(checkIn.Nutrition_Planning_Mode__c == 'Flexi Plans') {

			String[] daysToCheckFor = new String[]{
				'Monday',
				'Tuesday',
				'Wednesday',
				'Thursday',
				'Friday',
				'Saturday',
				'Sunday'
			};

			for(Targets__c target : checkInTargets) {
				if(target.Weekdays__c != null) {
					String[] weekdays = target.Weekdays__c.split(';');
					for(String wDay : weekdays) {

						for(Integer i = 0; i < daysToCheckFor.size(); i++) {
							if(daysToCheckFor[i] == wDay) {
								daysToCheckFor.remove(i);
							}
						}
						

					}
				}
			}

			if(daysToCheckFor.size() == 0) {
				vRule.IsValid = true;
				vRule.Message = 'All Days are assigned';
			} else {
				vRule.IsValid = false;
				vRule.Message = 'All Days should be assigned';
			}

		} else if(checkIn.Nutrition_Planning_Mode__c == 'Guided Plans') {

			List<String> weeksToCheckFor = new List<String>();
			Integer weekCount = Integer.valueOf(checkIn.Guided_Plan_Number_Weeks__c.split(' ').get(1));
			for(Integer i = 1; i <= weekCount; i++) {
				weeksToCheckFor.add('Week ' + i);
			}

			for(Targets__c target : checkInTargets) {
				if(target.Assign_to_Weeks__c != null) {
					String[] weeks = target.Assign_to_Weeks__c.split(';');
					for(String week : weeks) {

						for(Integer i = 0; i < weeksToCheckFor.size(); i++) {
							if(weeksToCheckFor[i] == week) {
								weeksToCheckFor.remove(i);
							}
						}
						

					}
				}
			}
			
			if(weeksToCheckFor.size() == 0) {
				vRule.IsValid = true;
				vRule.Message = 'All Weeks are assigned';
			} else {
				vRule.IsValid = false;
				vRule.Message = 'All Weeks should be assigned';
			}


		}

        return vRule;

    }

    private static ValidationRule p_checkTargetsHaveAtLeastOnePoolPlan(String checkInId) {

        //Query the targets
        Map<Id, Targets__c> checkInTargets = new Map<Id, Targets__c>([
            SELECT
                Id,
                (
                    SELECT
                        Id
                    FROM Target_Pool_Plans__r
                )
            FROM Targets__c
            WHERE Check_In__c = :checkInId
        ]);

        Boolean allTargetsHavePlans = true;

        for(Targets__c tar : checkInTargets.values()) {

            if(tar.Target_Pool_Plans__r.size() == 0) {
                allTargetsHavePlans = false;
                break;
            }
        }

        ValidationRule vRule = new ValidationRule();
        if(allTargetsHavePlans == true) {
            vRule.IsValid = true;
            vRule.Message = 'All Targets have Pool Plans Assigned';
        } else {
            vRule.IsValid = false;
            vRule.Message = 'All Targets need at least one pool plan assigned';
        }

        return vRule;

    }

    private static ValidationRule p_checkTargetsHaveDefaultPlanAssigned(String checkInId) {

        List<Targets__c> checkInTargets = [
            SELECT
                Id,
                Default_Target_Sub_Pool_Plan__c
            FROM Targets__c
            WHERE Check_In__c = :checkInId
        ];

        Boolean allTargetsHavePlans = true;

        for(Targets__c tar : checkInTargets) {
            if(tar.Default_Target_Sub_Pool_Plan__c == null) {
                allTargetsHavePlans = false;
                break;
            }
        }

        ValidationRule vRule = new ValidationRule();
        if(allTargetsHavePlans == true) {
            vRule.IsValid = true;
            vRule.Message = 'All Targets have a default plan assigned';
        } else {
            vRule.IsValid = false;
            vRule.Message = 'All Targets need a default plan assigned';
        }

        return vRule;

    }

    @AuraEnabled
    public static ValidationCheck CheckIfValid(String checkInId) {

        ValidationCheck vCheck  = new ValidationCheck();
        vCheck.validationRules  = new List<ValidationRule>();
        vCheck.IsValid          = true;
        vCheck.validationRules.add(p_areAllDaysAssigned(checkInId));
        vCheck.validationRules.add(p_checkTargetsHaveAtLeastOnePoolPlan(checkInId));
        vCheck.validationRules.add(p_checkTargetsHaveDefaultPlanAssigned(checkInId));

        for(ValidationRule vRule : vCheck.validationRules) {
            if(vRule.IsValid == false) {
                vCheck.IsValid = false;
            }
        }

        return vCheck;

    }

}