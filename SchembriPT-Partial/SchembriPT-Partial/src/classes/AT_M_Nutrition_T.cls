/**
* @author Harrison Campbell
* @description Testing for the AT_M_Nutrition class
*/ 
@IsTest
public without sharing class AT_M_Nutrition_T {


	@IsTest
	public static void QueryNutritionPlans_QuerySingleNutritionReferencePlan() {

        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1',1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1',2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2',3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c newItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newTmpItem1_1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);
        Item__c newItem2_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newTmpItem2_1.Id);
        Item__c newItem2_2              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newTmpItem2_2.Id);

		Test.startTest();
        List<AT_M_Nutrition.NutritionPlan> nutPlans = AT_M_Nutrition.QueryNutritionPlans(new Id[] { newPlan1.Id });
		Test.stopTest();

        //Assert that the Plans are correct
        System.assertEquals(1, nutPlans.size());
        AT_TestingTools.Assert_NutritionReferencePlan(newPlan1, nutPlans.get(0));
        System.assertEquals(2, nutPlans.get(0).blocks.size());
        AT_TestingTools.Assert_NutritionReferenceBlock(newBlock1, nutPlans.get(0).blocks.get(0));
        AT_TestingTools.Assert_NutritionReferenceBlock(newBlock2, nutPlans.get(0).blocks.get(1));
		System.assertEquals(1, nutPlans.get(0).blocks.get(0).items.size());
		AT_TestingTools.Assert_NutritionReferenceItem(newItem1_1, nutPlans.get(0).blocks.get(0).items.get(0));
		System.assertEquals(2, nutPlans.get(0).blocks.get(1).items.size());
		AT_TestingTools.Assert_NutritionReferenceItem(newItem2_1, nutPlans.get(0).blocks.get(1).items.get(0));
		AT_TestingTools.Assert_NutritionReferenceItem(newItem2_2, nutPlans.get(0).blocks.get(1).items.get(1));

	}
	
	@IsTest
	public static void QueryNutritionPlans_QueryOnlyNutritionReferencePlanDataNoBlocks() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);

		Test.startTest();
        List<AT_M_Nutrition.NutritionPlan> nutPlans = AT_M_Nutrition.QueryNutritionPlans(new Id[] { newPlan1.Id });
		Test.stopTest();

        //Assert that the Plans are correct
        System.assertEquals(1, nutPlans.size());
        AT_TestingTools.Assert_NutritionReferencePlan(newPlan1, nutPlans.get(0));

	}

	@IsTest
	public static void QueryNutritionPlans_QueryOnlyNutritionReferencePlanAndBlockDataNoItems() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);

		Test.startTest();
        List<AT_M_Nutrition.NutritionPlan> nutPlans = AT_M_Nutrition.QueryNutritionPlans(new Id[] { newPlan1.Id });
		Test.stopTest();

        //Assert that the Plans are correct
        System.assertEquals(1, nutPlans.size());
        AT_TestingTools.Assert_NutritionReferencePlan(newPlan1, nutPlans.get(0));
        System.assertEquals(2, nutPlans.get(0).blocks.size());
        AT_TestingTools.Assert_NutritionReferenceBlock(newBlock1, nutPlans.get(0).blocks.get(0));
        AT_TestingTools.Assert_NutritionReferenceBlock(newBlock2, nutPlans.get(0).blocks.get(1));

	}
	
	@IsTest
	public static void QueryNutritionPlans_QuerySingleNutritionClientPlan() {

        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1', 1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem1_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-2', 1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;

        //Create the Reference Plan
        Plan__c refPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn,	'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c refBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(refPlan1,		'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c refItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(refBlock1,		'Item 1_1', 20, 25, 100, 200, 200, 0, newTmpItem1_1.Id);
        Item__c refItem1_2              = AT_TestingTools.Create_NutritionReferenceItem(refBlock1,		'Item 1_2', 20, 50, 50, 25, 100, 1, newTmpItem1_2.Id);

		//Create the Client Plan
        Plan__c clientPlan1				= AT_TestingTools.Create_NutritionClientPlan(testCheckIn,		'Plan 1', 100, 200, 300, 400, refPlan1.Id);
        Block__c clientBlock1           = AT_TestingTools.Create_NutritionClientBlock(clientPlan1,		'Block 1', 'Blk 1', 75, 150, 225, 300, 0, refBlock1.Id);
        Item__c clientItem1_1           = AT_TestingTools.Create_NutritionClientItem(clientBlock1,		'Item 1_1', 20, 25, 100, 200, 200, 0, refItem1_1.Id);
        Item__c clientItem1_2           = AT_TestingTools.Create_NutritionClientItem(clientBlock1,		'Item 1_2', 20, 50, 50, 25, 100, 1, refItem1_2.Id);

		Test.startTest();
        List<AT_M_Nutrition.NutritionPlan> nutPlans = AT_M_Nutrition.QueryNutritionPlans(new Id[] { clientPlan1.Id });
		Test.stopTest();

		System.assertEquals(1, nutPlans.size());
		AT_TestingTools.Assert_NutritionClientPlan(clientPlan1, nutPlans.get(0));
		System.assertEquals(1, nutPlans.get(0).blocks.size());
		AT_TestingTools.Assert_NutritionClientBlock(clientBlock1, nutPlans.get(0).blocks.get(0));
		System.assertEquals(2, nutPlans.get(0).blocks.get(0).items.size());
		AT_TestingTools.Assert_NutritionClientItem(clientItem1_1, nutPlans.get(0).blocks.get(0).items.get(0));
		AT_TestingTools.Assert_NutritionClientItem(clientItem1_2, nutPlans.get(0).blocks.get(0).items.get(1));

	}
	
	@IsTest
	public static void QueryNutritionPlans_QueryOnlyNutritionClientPlanDataNoBlocks() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();

        //Create the Reference Plan
        Plan__c refPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn,	'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);

		//Create the Client Plan
        Plan__c clientPlan1				= AT_TestingTools.Create_NutritionClientPlan(testCheckIn,		'Plan 1', 100, 200, 300, 400, refPlan1.Id);

		Test.startTest();
        List<AT_M_Nutrition.NutritionPlan> nutPlans = AT_M_Nutrition.QueryNutritionPlans(new Id[] { clientPlan1.Id });
		Test.stopTest();

		System.assertEquals(1, nutPlans.size());
		AT_TestingTools.Assert_NutritionClientPlan(clientPlan1, nutPlans.get(0));

	}

	@IsTest
	public static void QueryNutritionPlans_QueryOnlyNutritionClientPlanAndBlockDataNoItems() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Templte Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);

        //Create the Reference Plan
        Plan__c refPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn,	'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c refBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(refPlan1,		'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);

		//Create the Client Plan
        Plan__c clientPlan1				= AT_TestingTools.Create_NutritionClientPlan(testCheckIn,		'Plan 1', 100, 200, 300, 400, refPlan1.Id);
        Block__c clientBlock1           = AT_TestingTools.Create_NutritionClientBlock(clientPlan1,		'Block 1', 'Blk 1', 75, 150, 225, 300, 0, refBlock1.Id);

		Test.startTest();
        List<AT_M_Nutrition.NutritionPlan> nutPlans = AT_M_Nutrition.QueryNutritionPlans(new Id[] { clientPlan1.Id });
		Test.stopTest();

		System.assertEquals(1, nutPlans.size());
		AT_TestingTools.Assert_NutritionClientPlan(clientPlan1, nutPlans.get(0));
		System.assertEquals(1, nutPlans.get(0).blocks.size());
		AT_TestingTools.Assert_NutritionClientBlock(clientBlock1, nutPlans.get(0).blocks.get(0));

	}

	@IsTest
	public static void CreatePlansFromTemplateIds_CreateNutritionReferencePlan() {
		
		RecordType rType = AT_TestingTools.Query_RecordType('Plan__c', 'Nutrition_Reference_Plan');

        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Template Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1', 1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1', 2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2', 3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

		Test.startTest();
		Map<Id, Plan__c> tmpPlanToPlan	= AT_M_Nutrition.CreatePlansFromTemplateIds(new Id[] { newTmpPlan.Id }, rType, testCheckIn.Id);
		Test.stopTest();

		AT_TestingTools.Assert_NutritionReferencePlanCreatedFromTemplatePlan(newTmpPlan.Id, testCheckIn.Id, testCheckIn.Account__c, tmpPlanToPlan.get(newTmpPlan.Id).Id);

	}

	@IsTest
	public static void CreatePlansFromTemplateIds_CreateNutritionClientPlan() {

		RecordType rType = AT_TestingTools.Query_RecordType('Plan__c', 'Nutrition_Client_Plan');

        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Template Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1', 1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1', 2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2', 3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

		Test.startTest();
		Map<Id, Plan__c> tmpPlanToPlan	= AT_M_Nutrition.CreatePlansFromTemplateIds(new Id[] { newTmpPlan.Id }, rType, testCheckIn.Id);
		Test.stopTest();

		AT_TestingTools.Assert_NutritionClientPlanCreatedFromTemplatePlan(newTmpPlan.Id, testCheckIn.Id, testCheckIn.Account__c, tmpPlanToPlan.get(newTmpPlan.Id).Id);

	}

	@IsTest
	public static void SaveAsNewPlan_CreateNutritionReferencePlanFromNutritionReferencePlan() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Template Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1',1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1',2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2',3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c newItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newTmpItem1_1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);
        Item__c newItem2_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newTmpItem2_1.Id);
        Item__c newItem2_2              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newTmpItem2_2.Id);

		Test.startTest();
		Plan__c newPlanId = AT_M_Nutrition.SaveAsNewPlan(newPlan1.Id, AT_TestingTools.Query_RecordType('Plan__c', 'Nutrition_Reference_Plan').Id);
		Test.stopTest();

		AT_TestingTools.Assert_NutritionReferencePlanClonedToNutritionReferencePlan(newPlan1.Id, newPlanId.Id);

	}

	@IsTest
	public static void SaveAsNewPlan_CreateNutritionClientPlanFromNutritionReferencePlan() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Template Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1',1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1',2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2',3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c newItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newTmpItem1_1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);
        Item__c newItem2_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newTmpItem2_1.Id);
        Item__c newItem2_2              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newTmpItem2_2.Id);

		Test.startTest();
		Plan__c newPlanId = AT_M_Nutrition.SaveAsNewPlan(newPlan1.Id, AT_TestingTools.Query_RecordType('Plan__c', 'Nutrition_Client_Plan').Id);
		Test.stopTest();

		AT_TestingTools.Assert_NutritionReferencePlanClonedToNutritionClientPlan(newPlan1.Id, newPlanId.Id);

	}
	
	@IsTest
	public static void SaveAsNewPlan_CreateNutritionSessionPlanFromNutritionReferencePlan() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Template Values
        Template_Plan__c newTmpPlan     = AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1',1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2  = AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1',2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2  = AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2',3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;

        //Create the Reference Plan
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c newItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newTmpItem1_1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);
        Item__c newItem2_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newTmpItem2_1.Id);
        Item__c newItem2_2              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newTmpItem2_2.Id);

		Test.startTest();
		Plan__c newPlanId = AT_M_Nutrition.SaveAsNewPlan(newPlan1.Id, AT_TestingTools.Query_RecordType('Plan__c', 'Nutrition_Session_Plan').Id);
		Test.stopTest();

		AT_TestingTools.Assert_NutritionReferencePlanClonedToNutritionSessionPlan(newPlan1.Id, newPlanId.Id);

	}
	
	@IsTest
	public static void SaveAsNewPlan_CreateNutritionClientPlanFromNutritionClientPlan() {
		
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);

        //Create the Template Values
        Template_Plan__c newTmpPlan			= AT_TestingTools.Create_NutritionTemplatePlan();
        Template_Block__c newTmpBlock1		= AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 0);
        Template_Item__c newTmpItem1_1		= AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock1, 0, 'Tmp Item 1-1',1, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Block__c newTmpBlock2		= AT_TestingTools.Create_NutritionTemplateBlock(newTmpPlan, 1);
        Template_Item__c newTmpItem2_1		= AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 0, 'Tmp Item 2-1',2, 1, 2, 3, 'Grams', 1, 1, 1).templateItem;
        Template_Item__c newTmpItem2_2		= AT_TestingTools.Create_NutritionTemplateItem(newTmpBlock2, 1, 'Tmp Item 2-2',3, 4, 5, 6, 'Slice', 5, 0, 1).templateItem;
		
        Plan__c newPlan1                = AT_TestingTools.Create_NutritionReferencePlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newTmpPlan.Id);
        Block__c newBlock1              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newTmpBlock1.Id);
        Item__c newItem1_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newTmpItem1_1.Id);
        Block__c newBlock2              = AT_TestingTools.Create_NutritionReferenceBlock(newPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newTmpBlock2.Id);
        Item__c newItem2_1              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newTmpItem2_1.Id);
        Item__c newItem2_2              = AT_TestingTools.Create_NutritionReferenceItem(newBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newTmpItem2_2.Id);

        //Create the Reference Plan
        Plan__c newClientPlan1                = AT_TestingTools.Create_NutritionClientPlan(testCheckIn, 'Plan 1', 100, 200, 300, 400, newPlan1.Id);
        Block__c newClientBlock1              = AT_TestingTools.Create_NutritionClientBlock(newClientPlan1, 'Block 1', 'Blk 1', 75, 150, 225, 300, 0, newBlock1.Id);
        Item__c newClientItem1_1              = AT_TestingTools.Create_NutritionClientItem(newClientBlock1, 'Item 1_1', 20, 75, 150, 225, 300, 0, newItem1_1.Id);
        Block__c newClientBlock2              = AT_TestingTools.Create_NutritionClientBlock(newClientPlan1, 'Block 2', 'Blk 2', 25, 50, 75, 100, 1, newBlock2.Id);
        Item__c newClientItem2_1              = AT_TestingTools.Create_NutritionClientItem(newClientBlock2, 'Item 2_1', 30, 20, 45, 70, 95, 0, newItem2_1.Id);
        Item__c newClientItem2_2              = AT_TestingTools.Create_NutritionClientItem(newClientBlock2, 'Item 2_2', 40, 5, 5, 5, 5, 1, newItem2_2.Id);

		Test.startTest();
		Plan__c newPlanId = AT_M_Nutrition.SaveAsNewPlan(newClientPlan1.Id, AT_TestingTools.Query_RecordType('Plan__c', 'Nutrition_Client_Plan').Id);
		Test.stopTest();

		AT_TestingTools.Assert_NutritionClientPlanClonedToNutritionClientPlan(newClientPlan1.Id, newPlanId.Id);

	}
	
	@IsTest
	public static void GetCheckInTargets_QueryFlexiPlanTargets() {
	
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);
		testCheckIn.Nutrition_Planning_Mode__c = 'Flexi Plans';
		update testCheckIn;

		Targets__c target1 = AT_TestingTools.Create_NutritionTarget(testCheckIn.Id, 'Target 1', 100, 200, 300, 'Monday;Tuesday', 'Week 1;Week 3');
		Targets__c target2 = AT_TestingTools.Create_NutritionTarget(testCheckIn.Id, 'Target 2', 200, 400, 600, 'Wednesday;Thursday', 'Week 2;Week 4');
		Targets__c target3 = AT_TestingTools.Create_NutritionTarget(testCheckIn.Id, 'Target 2', 300, 600, 900, 'Friday;Saturday;Sunday', 'Week 5;Week 6');

		Test.startTest();
		List<AT_M_Nutrition.NutritionTarget> nTargets = AT_M_Nutrition.GetCheckInTargets(testCheckIn.Id);
		Test.stopTest();

		System.assertEquals(3, nTargets.size());
		AT_TestingTools.Assert_NutritionFlexiPlanTarget(target1, nTargets.get(0));
		AT_TestingTools.Assert_NutritionFlexiPlanTarget(target2, nTargets.get(1));
		AT_TestingTools.Assert_NutritionFlexiPlanTarget(target3, nTargets.get(2));

	}
	
	@IsTest
	public static void GetCheckInTargets_QueryGuidedPlanTargets() {
	
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);
		testCheckIn.Nutrition_Planning_Mode__c = 'Guided Plans';
		update testCheckIn;

		Targets__c target1 = AT_TestingTools.Create_NutritionTarget(testCheckIn.Id, 'Target 1', 100, 200, 300, 'Monday;Tuesday', 'Week 1;Week 3');
		Targets__c target2 = AT_TestingTools.Create_NutritionTarget(testCheckIn.Id, 'Target 2', 200, 400, 600, 'Wednesday;Thursday', 'Week 2;Week 4');
		Targets__c target3 = AT_TestingTools.Create_NutritionTarget(testCheckIn.Id, 'Target 2', 300, 600, 900, 'Friday;Saturday;Sunday', 'Week 5;Week 6');

		Test.startTest();
		List<AT_M_Nutrition.NutritionTarget> nTargets = AT_M_Nutrition.GetCheckInTargets(testCheckIn.Id);
		Test.stopTest();

		System.assertEquals(3, nTargets.size());
		AT_TestingTools.Assert_NutritionGuidedPlanTarget(target1, nTargets.get(0));
		AT_TestingTools.Assert_NutritionGuidedPlanTarget(target2, nTargets.get(1));
		AT_TestingTools.Assert_NutritionGuidedPlanTarget(target3, nTargets.get(2));

	}

	@IsTest
	public static void GetCheckInTargets_QueryNoTargets() {
	
        Contact testContact     = AT_TestingTools.Create_Contact();
        Check_In__c testCheckIn = AT_TestingTools.Create_CheckIn(testContact);
		testCheckIn.Nutrition_Planning_Mode__c = 'Guided Plans';
		update testCheckIn;
		
		Test.startTest();
		List<AT_M_Nutrition.NutritionTarget> nTargets = AT_M_Nutrition.GetCheckInTargets(testCheckIn.Id);
		Test.stopTest();
		
		System.assertEquals(0, nTargets.size());
	}

}