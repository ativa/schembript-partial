public without sharing class AT_Q_UpdatePassword implements Queueable {

	private String userId		{ get; set; }

	private String newPassword	{ get; set; }

	public AT_Q_UpdatePassword(string userId, String newPassword) {
		this.userId			= userId;
		this.newPassword	= newPassword;
	}
	
	public void execute(QueueableContext qc) {
		System.setPassword(this.userId, this.newPassword);
	}

}