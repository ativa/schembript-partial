global without sharing class Content {
	/*@AuraEnabled
    global static Object getFile(String title, String linkedID, Object defaultValue, System.Type model) {
        List<ContentDocumentLink> templatesLink = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE (LinkedEntityId = :linkedID) AND (ContentDocument.Title = :title)];

        if (templatesLink.isEmpty()) return defaultValue;
        
        ContentDocument file = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE Id = :templatesLink[0].ContentDocumentId];
        
        ContentVersion latestVersion = [SELECT VersionData FROM ContentVersion WHERE Id = :file.LatestPublishedVersionId];
  
        if (latestVersion.VersionData == null) return defaultValue;
        
        String content = latestVersion.VersionData.toString();
     
        if (String.isBlank(content)) return defaultValue;
        
        return JSON.deserialize(content, model);
    }*/
    
    @AuraEnabled
    global static void deleteFileFromVersion(String versionID) {
        ContentVersion version = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :versionID];
        
        delete new ContentDocument(Id = version.ContentDocumentId);
    }
    
    /*@AuraEnabled
    global static Object getRawFile(String recordID) {
        ContentDocument file = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE Id = :recordID];
        
        ContentVersion latestVersion = [SELECT VersionData FROM ContentVersion WHERE Id = :file.LatestPublishedVersionId];
  
        if (latestVersion.VersionData == null) return null;
        
        String content = latestVersion.VersionData.toString();
     
        if (String.isBlank(content)) return null;
        
        return JSON.deserializeUntyped(content);
    }
    
    @AuraEnabled
    global static String getFile(String title, String linkedID) {
        List<ContentDocumentLink> templatesLink = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE (LinkedEntityId = :linkedID) AND (ContentDocument.Title = :title)];

        if (templatesLink.isEmpty()) return null;
        
        ContentDocument file = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE Id = :templatesLink[0].ContentDocumentId];
        
        ContentVersion latestVersion = [SELECT VersionData FROM ContentVersion WHERE Id = :file.LatestPublishedVersionId];
  
        if (latestVersion.VersionData == null) return null;
        
        String content = latestVersion.VersionData.toString();
     
        if (String.isBlank(content)) return null;
        
        System.debug(content);
        
        return content;
    }
    
    @AuraEnabled
    global static void saveFile(String title, String linkedID, String data) {
        ContentVersion file = new ContentVersion(Title=title, PathOnClient=(title + '.txt'), IsMajorVersion=false, VersionData=Blob.valueOf(data));
        
        List<ContentDocumentLink> links = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE (LinkedEntityId = :linkedID) AND (ContentDocument.Title = :title)];

        if (links.isEmpty()) {
            insert file;
            
            file = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :file.Id LIMIT 1];
            
            ContentDocumentLink newLink = new ContentDocumentLink(LinkedEntityId = linkedID, ContentDocumentId = file.ContentDocumentId, ShareType = 'I');
            
            insert newLink;
        } else {
            file.ContentDocumentId = links[0].ContentDocumentId;
            
            insert file;
        }
    }*/
}