/**
* @author Harrison Campbell
* @description Controller for the CommunityClientProgress Lightning Components
*/ 
public without sharing class CommunityClientProgressController {

    /**
    * @description Get the current User's AccountId
    * @return 
    */ 
    @AuraEnabled
    public static String getAccountID(String recordID) {
        if (String.isNotBlank(recordID)) {
            String prefix = recordID.left(3);
            
            if (prefix == Check_In__c.sObjectType.getDescribe().getKeyPrefix()) {
                return [SELECT Account__c FROM Check_In__c WHERE Id = :recordID].Account__c;
            }
        }
        
        return [SELECT Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()].Contact.AccountId;
    }
    
	/**
	* @author Harrison Campbell
	* @description Holds the Client Progress
	*/ 
	public class ClientProgress {

		@AuraEnabled
		public List<Check_In__c> checkIns { get; set; }

	}

	/**
	* @description Query the latest checkIns
	* @param personAccId The Person Account Id
	* @param checkInLimit The number of CheckIns we want to query
	* @return Returns the Client Progress
	*/ 
	@AuraEnabled
	public static ClientProgress queryLatestCheckIns(String personAccId, Integer checkInLimit) {
	
		

		String[] validStages = new String[] {
			'VA Review',
			'Trainer Review',
			'Super-Coach Review',
			'Completed'
		};

		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Account__c,
				Scheduled_Check_In_Date__c,
				Thigh_Measurement__c,
				Waist_Measurement__c,
				Chest_Measurement__c,
				Hip_Measurement__c,
				Weight__c,
				Calories_Consumed_average__c,
				( SELECT
					Actual_Plan_Calories__c,
					Id
				FROM Plans__r )
			FROM Check_In__c 
			WHERE Account__c = :personAccId AND Scheduled_Check_In_Date__c <= :Date.today() AND Stage__c IN :validStages
			ORDER BY Scheduled_Check_In_Date__c DESC
			LIMIT :checkInLimit
		];
		
		//Reverse the list
		List<Check_In__c> reverseCheckIns = new List<Check_In__c>();
		for(Integer i = checkIns.size() - 1; i >= 0; i--) {
			reverseCheckIns.add(checkIns[i]);
		}
		ClientProgress prog = new ClientProgress ();
		prog.checkIns		= reverseCheckIns;
		return prog;

	}

	/**
	* @description Query the program that the user is currently doing
	* @param personAccId The Id of the Person Account
	* @param queryDate The Date of the program
	* @return 
	*/ 
	@AuraEnabled
	public static ClientProgress queryProgram(String personAccId, Date queryDate) {

		Case program = [
			SELECT 
				Id, 
				Program_Start_Date__c, 
				Program_End_Date__c 
			FROM Case 
			WHERE AccountId =: personAccId AND Status = 'Active'
			LIMIT 1
		];
		return queryProgress(personAccId, program.Program_Start_Date__c, queryDate);
	}

	/**
	* @description Query the client progress
	* @param personAccId The Id of the person account
	* @param startDate The starting date of the query
	* @param endDate The end date of the query
	* @return Returns the clients progress
	*/ 
	@AuraEnabled
	public static ClientProgress queryProgress(String personAccId, Date startDate, Date endDate) {
	
		String[] validStages = new String[] {
			'VA Review',
			'Trainer Review',
			'Super-Coach Review',
			'Completed'
		};

		//Get all of the checkins for the current account
		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Account__c,
				Scheduled_Check_In_Date__c,
				Thigh_Measurement__c,
				Waist_Measurement__c,
				Chest_Measurement__c,
				Hip_Measurement__c,
				Weight__c,
				Calories_Consumed_average__c,
				( SELECT
					Actual_Plan_Calories__c,
					Id
				FROM Plans__r )
			FROM Check_In__c
			WHERE Account__c =: personAccId AND Scheduled_Check_In_Date__c >= :startDate AND Scheduled_Check_In_Date__c <= :endDate AND Stage__c IN :validStages
			ORDER By Scheduled_Check_In_Date__c ASC 
		];

		//If there are 4 or less dates then return those points
		if(checkIns.size() <= 4) {
			ClientProgress prog = new ClientProgress ();
			prog.checkIns		= checkIns;
			return prog;
		}	

		//Return 4 dates, the start date, the end date and 2 dates that are in between
		Check_In__c startCheckIn	= checkIns[0];
		Check_In__c endCheckIn		= checkIns[checkIns.size() - 1];
		
		Date scedStartDate		= startCheckIn.Scheduled_Check_In_Date__c;
		Date scedEndDate		= endCheckIn.Scheduled_Check_In_Date__c;
		Integer daysBetween		= scedStartDate.daysBetween(scedEndDate);
		Date firstDate			= Date.newInstance(scedStartDate.year(), scedStartDate.month(), scedStartDate.day() + Integer.valueOf(daysBetween * 0.33));
		Date secondDate			= Date.newInstance(scedStartDate.year(), scedStartDate.month(), scedStartDate.day() + Integer.valueOf(daysBetween * 0.66));

		System.debug(scedStartDate + ' : ' + firstDate + ' : ' + secondDate + ' : ' + scedEndDate);

		Check_In__c checkFirstDate	= null;
		Integer minFirstDiff	= 2147483647;
		Check_In__c checkSecondDate = null;
		Integer minSecondDiff	= 2147483647;

		for(Integer i = 1; i < checkIns.size() - 1; i++) {

			Integer firstDateDiff	= Math.abs(firstDate.daysBetween(checkIns[i].Scheduled_Check_In_Date__c));
			Integer secondDateDiff	= Math.abs(secondDate.daysBetween(checkIns[i].Scheduled_Check_In_Date__c));

			if(firstDateDiff < minFirstDiff) {
				minFirstDiff	= firstDateDiff;
				checkFirstDate	= checkIns[i];
			}
			if(secondDateDiff < minSecondDiff) {
				minSecondDiff	= secondDateDiff;
				checkSecondDate	= checkIns[i];
			}
		}

		List<Check_In__c> checks = new List<Check_In__c>();
		checks.add(startCheckIn);
		checks.add(checkFirstDate);
		checks.add(checkSecondDate);
		checks.add(endCheckIn);

		ClientProgress clientProgress = new ClientProgress ();
		clientProgress.checkIns = checks;
		return clientProgress;

	}

}