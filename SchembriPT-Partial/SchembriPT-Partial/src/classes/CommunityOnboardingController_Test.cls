/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-12-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-12-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class CommunityOnboardingController_Test {


	private static Account testTool_createPersonAccount() {
	
		Id pAccRecType = [SELECT Id FROM RecordType WHERE IsPersonType = true AND SobjectType = 'Account'].Id;

        Account acc	= new Account(
			RecordTypeID				= pAccRecType,
			FirstName					= 'Test',
			LastName					= 'Account',
			PersonMailingStreet			= 'test@yahoo.com',
			PersonMailingPostalCode		= '12345',
			PersonMailingCity			= 'SFO',
			PersonEmail					= 'test@yahoo.com',
			PersonHomePhone				= '1234567',
			PersonMobilePhone			= '12345678'
        );

        insert acc;
		
		return acc;
	}

	private static User testTool_createPortalUser(Account personAccount) {
	
		Account acc = [
			SELECT
				Id,
				PersonContactId
			FROM Account
			WHERE Id = :personAccount.Id
		];
		
        UserRole role			= [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'ativCustomerPersonAccount'];
		UserLicense uLicense	= [SELECT Id FROM UserLicense WHERE Name = 'Customer Community Plus'];
        Profile profile			= [SELECT Id, Name, UserLicenseId FROM Profile WHERE UserLicenseId =: uLicense.Id LIMIT 1];
        
        User u = new User(
			Alias					= 'standt', 
			Email					= 'aaaaaaa@testorg.com', 
            EmailEncodingKey		= 'UTF-8', 
			LastName				= 'Testing', 
			LanguageLocaleKey		= 'en_US', 
            LocaleSidKey			= 'en_US', 
			ProfileId				= profile.Id, 
            TimeZoneSidKey			= 'America/Los_Angeles', 
			UserName				= 'aaaaaaaa@testorg.com'
		);
		u.ContactId = acc.PersonContactId;

		insert u;

		return u;
	}

    

    @testSetup
    static void init() {
        Organization org = [SELECT IsSandbox FROM Organization LIMIT 1];
        
        String communityName = org.IsSandbox ? 'SchembriPTv2' : 'Schembri PT Client Community';
		
        UserRole role			= [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'ativCustomerPersonAccount'];
		UserLicense uLicense	= [SELECT Id FROM UserLicense WHERE Name = 'Customer Community Plus'];
        Profile profile			= [SELECT Id, Name, UserLicenseId FROM Profile WHERE UserLicenseId =: uLicense.Id LIMIT 1];
        
        Portal_Settings__c settings = new Portal_Settings__c(
			User_Role__c = role.DeveloperName, 
			User_Profile__c = profile.Name, 
			Portal_URL__c = '/', 
			Community_Name__c = communityName, 
			Subscription_Active_Status__c = 'active'
		);
        
        insert settings;
        
        RecordType client = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Client'];
        
        Account member = new Account(RecordTypeId = client.Id ,FirstName = 'test', LastName = 'test', PersonEmail = 'test@example.com');
        
        insert member;
    }
	//
    
    @isTest
    static void testGetInfo() {
        UserRole role = [SELECT Id FROM UserRole WHERE DeveloperName = 'ativCustomerPersonAccount'];
		UserLicense uLicense = [SELECT Id FROM UserLicense WHERE Name = 'Customer Community Plus'];
        Profile profile = [SELECT Id, Name, UserLicenseId FROM Profile WHERE UserLicenseId =: uLicense.Id  LIMIT 1];
        
        Account member = [SELECT FirstName, LastName, PersonEmail, PersonContactId, PersonMobilePhone FROM Account LIMIT 1];
        
        User u					= new User();
        u.FirstName				= member.FirstName;
        u.LastName				= member.LastName;
        u.Alias					= member.FirstName.left(1) + member.LastName.left(7);
        u.CommunityNickname		= (member.FirstName + member.LastName).replace(' ', '').left(40);
        u.ContactId				= member.PersonContactId;
        u.Email					= member.PersonEmail;
        u.MobilePhone			= member.PersonMobilePhone;
        u.Username				= member.PersonEmail;
        u.ProfileId				= profile.Id;
        u.UserRoleId			= role.Id;
        u.TimeZoneSidKey		= 'Australia/Sydney';
        u.LocaleSidKey			= 'en_AU';
        u.EmailEncodingKey		= 'ISO-8859-1';
        u.LanguageLocaleKey		= 'en_US';

		insert u;
        
        System.runAs(u) {
			CommunityOnboardingController.getUserInfo();
		}
    }

	@IsTest
	private static void getUserInfo_getUserWithSkipOnboardingPermission() {
		
		Account personAccount	= testTool_createPersonAccount();
		User portalUser			= testTool_createPortalUser(personAccount);

		personAccount.Skip_Onboarding_Process__c = true;
		update personAccount;

		CommunityOnboardingController.Info uInfo = null;
		Test.startTest();
		System.runAs(portalUser) {
			uInfo = CommunityOnboardingController.getUserInfo();
		}
		Test.stopTest();
		System.assertEquals(portalUser.Id,									uInfo.userId);
		System.assertEquals(personAccount.Id,								uInfo.accountID);
		//System.assertEquals(Portal_Settings__c.getInstance().Portal_URL__c, uInfo.portalURL);
		System.assertEquals(personAccount.Skip_Onboarding_Process__c,		uInfo.skipOnboarding);

	}

}