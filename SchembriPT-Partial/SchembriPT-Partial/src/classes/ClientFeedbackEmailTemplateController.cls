/**
* @author Harrison Campbell
* @description The Controller for the ClientFeedbackEmailTemplate
*/ 
public without sharing class ClientFeedbackEmailTemplateController {

	public Boolean isAllType { get; set; }

	public Boolean isSingleType { get; set; }

	public String checkInQuery { get; set; }

	public Contact cont { get; set; }
	
	public Check_In__c currentCheckIn { get; set; }

	public Check_In__c previousCheckIn { get; set; }

	/**
	* @description All of the Charts that will be displayed
	*/ 
	public ChartUrls charts { get; set; }

	/**
	* @description All of the key metrics
	*/ 
	public AllMetrics metrics { get; set; }

	/**
	* @description All urls to the photos
	*/ 
	public PhotoUrls photos { get; set; }

	/**
	* @description Return the Id of the contact
	* @return Returns the Id of the contact
	*/ 
	public String getContactId() {
		return cont.Id;
	}

	/**
	* @description Set the Id of the contact
	* @param cId The Id of the contact
	*/ 
	public void setContactId(String cId) {

		cont = [
			SELECT
				Id,
				FirstName,
				AccountId
			FROM Contact
			WHERE Id = :cId
		];

		if(cont != null && currentCheckIn != null) {
			loadCharts();
			loadMetrics();
			loadPhotos();
		}

	}

	public String getCheckInId() {
		return currentCheckIn.Id;
	}

	public void setCheckInId(String checkInId) {

		System.debug('Set CheckIn Id');

		List<String> photoTypes = new String[] {
			'Front',
			'Side',
			'Back'
		};

		currentCheckIn = [
			SELECT
				Id,
				Stage__c,
				Scheduled_Check_In_Date__c,
				Next_Check_In_Date__c,
				Contact__c,
				Super_Coach__c,
				Weight__c,
				Chest_Measurement__c,
				Waist_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,
				Starting_Weight_Difference__c,
				Starting_Chest_Difference__c,
				Starting_Waist_Difference__c,
				Starting_Hips_Difference__c,
				Starting_Thigh_Difference__c,
				Weight_Difference__c,
				Chest_Difference__c,
				Waist_Difference__c,
				Hip_Difference__c,
				Thigh_Difference__c,
				Previous_Check_In__c,
				Scheduled_Check_In_Date_email__c,
				Next_Check_In_Date_email__c,
				(
					SELECT
						Id,
						Type__c,
						Dropbox_Path__c
					FROM Photos__r
					WHERE Type__c IN :photoTypes
					ORDER BY TimeStamp__c DESC
				)
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		//If the stage is pending, then get the previous one
		if(currentCheckIn.Stage__c == 'Pending' || currentCheckIn.Stage__c == 'Client Information') {
			currentCheckIn = [
				SELECT
					Id,
					Stage__c,
					Contact__c,
					Scheduled_Check_In_Date__c,
					Next_Check_In_Date__c,
					Super_Coach__c,
					Weight__c,
					Chest_Measurement__c,
					Waist_Measurement__c,
					Hip_Measurement__c,
					Thigh_Measurement__c,
					Starting_Weight_Difference__c,
					Starting_Chest_Difference__c,
					Starting_Waist_Difference__c,
					Starting_Hips_Difference__c,
					Starting_Thigh_Difference__c,
					Weight_Difference__c,
					Chest_Difference__c,
					Waist_Difference__c,
					Hip_Difference__c,
					Thigh_Difference__c,
					Previous_Check_In__c,
					Scheduled_Check_In_Date_email__c,
					Next_Check_In_Date_email__c
				FROM Check_In__c
				WHERE Id = :currentCheckIn.Previous_Check_In__c 
				ORDER BY Scheduled_Check_In_Date__c DESC 
				LIMIT 1
			];
		}

		previousCheckIn = GetPreviousCheckIn(currentCheckIn);

		if(cont != null && currentCheckIn != null) {
			loadCharts();
			loadMetrics();
			loadPhotos();
		}

	}


	public String getFeedbackChartType() {
		if(isAllType == true) {
			return 'All';
		} else if(isSingleType == true) {
			return 'Single';
		} else {
			return '';
		}
	}

	public void setFeedbackChartType(String fType) {

		if(fType == 'Single') {
			isAllType		= false;
			isSingleType	= true;
		} else if(fType == 'All') {
			isAllType		= true;
			isSingleType	= false;
		}

	}

	private Check_In__c GetPreviousCheckIn(Check_In__c lCheckIn) {
	
		String[] photoTypes = new String[] {
			'Front',
			'Side',
			'Back'
		};

		return [
			SELECT
				Id,
				Super_Coach__c,
				Next_Check_In_Date__c,

				Weight__c,
				Chest_Measurement__c,
				Waist_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,

				Starting_Weight_Difference__c,
				Starting_Chest_Difference__c,
				Starting_Waist_Difference__c,
				Starting_Hips_Difference__c,
				Starting_Thigh_Difference__c,

				Weight_Difference__c,
				Chest_Difference__c,
				Waist_Difference__c,
				Hip_Difference__c,
				Thigh_Difference__c,

				Scheduled_Check_In_Date_email__c,
				Next_Check_In_Date_email__c,

				(
					SELECT
						Id,
						Type__c,
						Dropbox_Path__c
					FROM Photos__r
					WHERE Type__c IN :photoTypes
					Order BY Timestamp__c DESC
				)

			FROM Check_In__c
			WHERE Id = :lCheckIn.Previous_Check_In__c
		];

	}

	private void updateLineSettings(ChartJS_Chart chart) {

		ChartJS_Dataset dataset				= chart.data.datasets.get(0);
		dataset.datalabels.font.size		= '12';
		dataset.datalabels.textStrokeWidth	= 2;

		ChartJS_YAxes yAxes		= chart.options.scales.yAxes.get(0);
		yAxes.ticks.fontSize	= 16;

		ChartJS_YAxes xAxes		= chart.options.scales.xAxes.get(0);
		xAxes.ticks.fontSize	= 10;

	}
	
	private void updateBarSettings(ChartJS_Chart chart) {

		ChartJS_Dataset dataset				= chart.data.datasets.get(0);
		dataset.datalabels.font.size		= '12';
		dataset.datalabels.textStrokeWidth	= 2;

		ChartJS_YAxes yAxes		= chart.options.scales.yAxes.get(0);
		yAxes.ticks.fontSize	= 16;

		ChartJS_YAxes xAxes		= chart.options.scales.xAxes.get(0);
		xAxes.ticks.fontSize	= 10;

	}
	/**
	* @description Load Chart URLs
	*/ 
	private void loadCharts() {

		charts = new ChartUrls();
		Integer chartWidth	= 245;//350 * 0.75;
		Integer chartHeight = 175;//250 * 0.75;
	
		ClientFeedback_Chart weightChart	= ClientFeedbackCharts.GetWeightChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		updateLineSettings(weightChart.chart);
		charts.weight						= QuickChart_Adaptor.createShortURL(weightChart.chart, chartWidth * 2, chartHeight);
		
		ClientFeedback_Chart chestChart		= ClientFeedbackCharts.GetChestChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		updateLineSettings(chestChart.chart);
		charts.chest						= QuickChart_Adaptor.createShortURL(chestChart.chart, chartWidth, chartHeight);

		ClientFeedback_Chart waistChart		= ClientFeedbackCharts.GetWaistChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		updateLineSettings(waistChart.chart);
		charts.waist						= QuickChart_Adaptor.createShortURL(waistChart.chart, chartWidth, chartHeight);
		
		ClientFeedback_Chart hipsChart		= ClientFeedbackCharts.GetHipChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		updateLineSettings(hipsChart.chart);
		charts.hips							= QuickChart_Adaptor.createShortURL(hipsChart.chart, chartWidth, chartHeight);

		ClientFeedback_Chart thighChart		= ClientFeedbackCharts.GetThighChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		updateLineSettings(thighChart.chart);
		charts.thigh						= QuickChart_Adaptor.createShortURL(thighChart.chart, chartWidth, chartHeight);
	
		//600
		Integer calChartWidth	= 420;//600 * 0.75;
		Integer calChartHeight	= 525;//750 * 0.75;
	
		ClientFeedback_Chart calorieChart	= ClientFeedbackCharts.GetCalorieChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		updateBarSettings(calorieChart.chart);
		charts.calorie						= QuickChart_Adaptor.createShortURL(calorieChart.chart, calChartWidth, calChartHeight);

		ClientFeedback_Chart allChart		= ClientFeedbackCharts.GetAllChart(cont.AccountId, checkInQuery, currentCheckIn.Scheduled_Check_In_Date__c);
		charts.all							= QuickChart_Adaptor.createShortURL(allChart.chart, chartWidth * 2, calChartHeight);

	}

	/**
	* @description Load the key data for each category(weight, chest, etc.)
	* @param latest The Latest CheckIn
	*/ 
	private void loadMetrics() {

		metrics = new AllMetrics();

		metrics.weight			= new MetricType();
		metrics.weight.latest	= new MetricValue(currentCheckIn.Weight_Difference__c);
		metrics.weight.total	= new MetricValue(currentCheckIn.Starting_Weight_Difference__c);
		
		metrics.chest			= new MetricType();
		metrics.chest.latest	= new MetricValue(currentCheckIn.Chest_Difference__c);
		metrics.chest.total		= new MetricValue(currentCheckIn.Starting_Chest_Difference__c);

		metrics.waist			= new MetricType();
		metrics.waist.latest	= new MetricValue(currentCheckIn.Waist_Difference__c);
		metrics.waist.total		= new MetricValue(currentCheckIn.Starting_Waist_Difference__c);

		metrics.hips			= new MetricType();
		metrics.hips.latest		= new MetricValue(currentCheckIn.Hip_Difference__c);
		metrics.hips.total		= new MetricValue(currentCheckIn.Starting_Hips_Difference__c);

		metrics.thigh			= new MetricType();
		metrics.thigh.latest	= new MetricValue(currentCheckIn.Thigh_Difference__c);
		metrics.thigh.total		= new MetricValue(currentCheckIn.Starting_Thigh_Difference__c);
	
	}

	private void loadPhotos() {

		photos = new PhotoUrls();

		photos.hasLatestBack	= false;
		photos.hasLatestSide	= false;
		photos.hasLatestFront	= false;

		for(Photo__c p : currentCheckIn.Photos__r) {

			if(photos.hasLatestFront == true && photos.hasLatestSide == true && photos.hasLatestBack == true) {
				break;
			}

			if(photos.hasLatestFront == false && p.Type__c == 'Front') {
				photos.hasLatestFront	= true;
				photos.latestFront	= p.Dropbox_Path__c;
				continue;
			}
			
			if(photos.hasLatestSide == false && p.Type__c == 'Side') {
				photos.hasLatestSide		= true;
				photos.latestSide	= p.Dropbox_Path__c;
				continue;
			}
			
			if(photos.hasLatestBack == false && p.Type__c == 'Back') {
				photos.hasLatestBack		= true;
				photos.latestBack	= p.Dropbox_Path__c;
				continue;
			}

		}
		
		photos.hasPreviousBack	= false;
		photos.hasPreviousSide	= false;
		photos.hasPreviousFront	= false;

		for(Photo__c p : previousCheckIn.Photos__r) {

			if(photos.hasPreviousFront == true && photos.hasPreviousSide == true && photos.hasPreviousBack == true) {
				break;
			}

			if(photos.hasPreviousFront == false && p.Type__c == 'Front') {
				photos.hasPreviousFront		= true;
				photos.previousFront	= p.Dropbox_Path__c;
				continue;
			}
			
			if(photos.hasPreviousSide == false && p.Type__c == 'Side') {
				photos.hasPreviousSide		= true;
				photos.previousSide		= p.Dropbox_Path__c;
				continue;
			}
			
			if(photos.hasPreviousBack == false && p.Type__c == 'Back') {
				photos.hasPreviousBack		= true;
				photos.previousBack		= p.Dropbox_Path__c;
				continue;
			}

		}


	}

	public class ChartUrls {
		
		public String weight { get; set; }

		public String chest { get; set; }

		public String waist { get; set; }

		public String hips { get; set; }

		public String thigh { get; set; }

		public String calorie { get; set; }

		public String all { get; set; }

	}

	public class AllMetrics {

		public MetricType weight { get; set; }

		public MetricType chest { get; set; }

		public MetricType waist { get; set; }

		public MetricType hips { get; set; }

		public MetricType thigh { get; set; }

		
	}

	public class MetricType { 
	
		public MetricValue latest { get; set; }

		public MetricValue total { get; set; }


	}

	public class MetricValue {

		public String value { get; set; }

		public String valueClass { get; set; }

		public String valueStyle { get; set; }

		public MetricValue(Decimal newValue) {

			value = newValue.toPlainString();

			if(newValue == 0) {
				valueClass = 'value-nuetral';
				valueStyle = 'width: 50px; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 700; color: #000;';
			} else if(newValue > 0) {
				valueClass = 'value-increase';
				valueStyle = 'width: 50px; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 700; color: #45c445;';
			} else {
				valueClass = 'value-decrease';
				valueStyle = 'width: 50px; border: 1px solid black; text-align: center; padding: 0.5rem; font-size: 16px; font-weight: 700; color: #c42323;';
			}

		}


	}

	public class PhotoUrls {

		public Boolean hasLatestFront { get; set; }

		public Boolean hasLatestSide { get; set; }

		public Boolean hasLatestBack { get; set; }
		
		public Boolean hasPreviousFront { get; set; }

		public Boolean hasPreviousSide { get; set; }

		public Boolean hasPreviousBack { get; set; }
		
		public String latestFront { get; set; }

		public String latestSide { get; set; }
		
		public String latestBack { get; set; }
		
		public String previousFront { get; set; }

		public String previousSide { get; set; }
		
		public String previousBack { get; set; }

	}

}