public class ClientFeedback_Chart  {

	public String title { get; set; }

	public String path { get; set; }

	public ChartJS_Chart chart { get; set; }

	public Blob imgBinary { get; set; }

	public ContentVersion contVersion { get; set; }

}