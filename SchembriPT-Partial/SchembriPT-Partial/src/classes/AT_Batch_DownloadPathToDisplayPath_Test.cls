@isTest 
private class AT_Batch_DownloadPathToDisplayPath_Test {

	private static Photo__c testTool_createNewPhoto(String dPath) {
		Photo__c newPhoto			= new Photo__c();
		newPhoto.Dropbox_Path__c	= dPath;
		newPhoto.Type__c			= 'Front';
		newPhoto.Source__c			= 'Dropbox';
		insert newPhoto;
		return newPhoto;
	}

	private static Photo__c testTool_refreshPhoto(Photo__c p) {
		return [
			SELECT 
				Id,
				Dropbox_Path__c 
			FROM Photo__c 
			WHERE Id = :p.Id
		];
	}

	@isTest
	private static void batch_testUpdatingPhotos() {

		String path1 = 'https://www.dropbox.com/s/c5gu29t2d2spmxi/Kathleen%20Petheram%20-%20DSC_0292.JPG?dl=0';
		Photo__c p1 = testTool_createNewPhoto(path1);
		
		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();
		
		p1 = testTool_refreshPhoto(p1);
		System.assertEquals('https://www.dropbox.com/s/c5gu29t2d2spmxi/Kathleen%20Petheram%20-%20DSC_0292.JPG?raw=1',	p1.Dropbox_Path__c);

	}

	@IsTest
	private static void batch_updatePhoto2() {

		String path2 = 'https://www.dropbox.com/s/c5gu29t2d2spmxi/Kathleen%20Petheram%20-%20DSC_0292.JPG?raw=1';
		Photo__c p2 = testTool_createNewPhoto(path2);
		
		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();

		p2 = testTool_refreshPhoto(p2);
		System.assertEquals('https://www.dropbox.com/s/c5gu29t2d2spmxi/Kathleen%20Petheram%20-%20DSC_0292.JPG?raw=1',	p2.Dropbox_Path__c);

	}
	
	@IsTest
	private static void batch_updatePhoto3() {

		String path3 = null;
		Photo__c p3 = testTool_createNewPhoto(path3);
		
		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();
		
		p3 = testTool_refreshPhoto(p3);
		System.assertEquals(null,	p3.Dropbox_Path__c);

	}
	
	@IsTest
	private static void batch_updatePhoto4() {
	
		String path4	= 'aaadl=0aaa';
		Photo__c p4		= testTool_createNewPhoto(path4);
		
		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();
		
		p4 = testTool_refreshPhoto(p4);
		System.assertEquals('aaadl=0aaa',	p4.Dropbox_Path__c);

	}
	
	@IsTest
	private static void batch_updatePhoto5() {
	
		String path5	= 'aaaraw=1aaa';
		Photo__c p5		= testTool_createNewPhoto(path5);
		
		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();
		
		p5 = testTool_refreshPhoto(p5);
		System.assertEquals('aaaraw=1aaa',	p5.Dropbox_Path__c);

	}
	
	@IsTest
	private static void batch_updatePhoto6() {
	
		String path6	= 'dl=0';
		Photo__c p6		= testTool_createNewPhoto(path6);
		
		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();
		
		p6 = testTool_refreshPhoto(p6);
		System.assertEquals('raw=1',	p6.Dropbox_Path__c);

	}
	
	@IsTest
	private static void batch_updatePhoto7() {
	
		String path7 = 'raw=1';
		Photo__c p7	 = testTool_createNewPhoto(path7);

		Test.startTest();
		AT_Batch_DownloadPathToDisplayPath dp2dp = new AT_Batch_DownloadPathToDisplayPath();
		Database.executeBatch(dp2dp);
		Test.stopTest();
		
		p7 = testTool_refreshPhoto(p7);
		System.assertEquals('raw=1',	p7.Dropbox_Path__c);

	}
}