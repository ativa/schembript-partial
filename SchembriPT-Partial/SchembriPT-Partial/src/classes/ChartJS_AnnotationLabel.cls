/**
* @author Harrison Campbell
* @description 
*/ 
public class ChartJS_AnnotationLabel  {

	public Boolean enabled { get; set; }

	public String content { get; set; }
	
}