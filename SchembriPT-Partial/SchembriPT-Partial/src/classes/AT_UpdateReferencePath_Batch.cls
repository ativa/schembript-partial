/**
* @author Harrison Campbell
* @description Trim the Reference path of photos to a certain folder in the path
*/ 
global class AT_UpdateReferencePath_Batch implements Database.Batchable<SObject> {
	
	private final String startingFolder;

	global AT_UpdateReferencePath_Batch(String startingFolder) {
		this.startingFolder = startingFolder;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, Name, Reference__c, t_Reference_Trimmed__c, Dropbox_Path__c FROM Photo__c');
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Photo__c> scope) {
	

		for(Photo__c p : scope) {
			if(p.Reference__c != null) {
				String trimmedReference = p.Reference__c.substringAfter(startingFolder);
				if(String.isNotEmpty(trimmedReference)) {
					p.Reference__c = '/' + startingFolder + trimmedReference;
				}
			}	
		}

		update scope;

	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}
}