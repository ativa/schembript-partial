/**
* @author Harrison Campbell
* @description Test the CollaborationGroupMember_Trigger_Tools class
*/ 
@isTest 
private class CollaborationGroupMember_TT_Test {


	/**
	* @description Creates a Test User
	* @return Returns a user for testing
	*/ 
	private static User testUser() {
	     Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
		 User u = new User(
			Alias = 'standt', 
			Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='aaa@testorg.com'
		);
		insert u;

		return u;
	}
	
	/**
	* @description Creates a Test User
	* @return Returns a user for testing
	*/ 
	private static User testUser2() {
	     Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
		 User u = new User(
			Alias = 'standt2', 
			Email='standarduser2@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing2', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='bbb@testorg.com'
		);
		insert u;

		return u;
	}
	
	/**
	* @description Creates a Test User
	* @return Returns a user for testing
	*/ 
	private static User testUser3() {
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
		User u = new User(
			Alias = 'standt3', 
			Email='standarduser3@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing3', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='ccc@testorg.com'
		);
		insert u;

		return u;
	}

	/**
	* @description Insert a few members to groups
	*/
	@isTest(SeeAllData=true)
	private static void beforeInsert_InsertMembersWithGroupAndGroupSettings() {
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

		User marikaUser = testUser();
		User lukeUser	= testUser2();
		User creationUser = testUser3();
		
		//Create some collaboration groups
		CollaborationGroup collGroup1	= new CollaborationGroup();
		collGroup1.Name					= 'Test Collaboration Group 1';
		collGroup1.CollaborationType	= 'Public';
		CollaborationGroup collGroup2	= new CollaborationGroup();
		collGroup2.Name					= 'Test Collaboration Group 2';
		collGroup2.CollaborationType	= 'Public';

		System.runAs(creationUser) {
			insert collGroup1;
			insert collGroup2;
		}

		List<CollaborationGroupMember> groupMembers = new List<CollaborationGroupMember>();
		
		CollaborationGroupMember marikaGroupMember	= new CollaborationGroupMember();
		marikaGroupMember.CollaborationGroupId		= collGroup1.Id;
		marikaGroupMember.MemberId					= marikaUser.Id;
		groupMembers.add(marikaGroupMember);
		
		CollaborationGroupMember lukeGroupMember	= new CollaborationGroupMember();
		lukeGroupMember.CollaborationGroupId		= collGroup2.Id;
		lukeGroupMember.MemberId					= lukeUser.Id;
		groupMembers.add(lukeGroupMember);

		Test.startTest();
		insert groupMembers;
		Test.stopTest();

		//Assert that the collaboration group members had their notification frequency updated
		marikaGroupMember	= [SELECT Id, CollaborationGroupId, MemberId, NotificationFrequency FROM CollaborationGroupMember WHERE Id =:marikaGroupMember.Id];
		lukeGroupMember		= [SELECT Id, CollaborationGroupId, MemberId, NotificationFrequency FROM CollaborationGroupMember WHERE Id =:lukeGroupMember.Id];

		System.assertEquals('W', marikaGroupMember.NotificationFrequency);
		System.assertEquals('D', lukeGroupMember.NotificationFrequency);

		List<ConnectApi.Subscription> following = ConnectApi.ChatterUsers.getFollowings(null, marikaUser.Id).following;
		System.assertEquals(1, following.size());
		System.assertEquals(UserInfo.getName(), following[0].subject.name);
		System.assertEquals('User', following[0].subject.type);
		
		List<ConnectApi.Subscription> following2 = ConnectApi.ChatterUsers.getFollowings(null, lukeUser.Id).following;
		System.assertEquals(0, following2.size());


	}

	/**
	* @description Insert one member to a group
	*/
	@IsTest(SeeAllData=true)
	private static void beforeInsert_InsertOneMemberWithGroupAndGroupSetting() {
		
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;

		User lukeUser	= testUser2();
		User creationUser = testUser3();


		CollaborationGroup collGroup2 = new CollaborationGroup();
		collGroup2.Name					= 'Test Collaboration Group 1';
		collGroup2.CollaborationType	= 'Public';

		System.runAs(creationUser) {
			insert collGroup2;
		}
		

		List<CollaborationGroupMember> groupMembers = new List<CollaborationGroupMember>();
		
		CollaborationGroupMember lukeGroupMember	= new CollaborationGroupMember();
		lukeGroupMember.CollaborationGroupId		= collGroup2.Id;
		lukeGroupMember.MemberId					= lukeUser.Id;
		groupMembers.add(lukeGroupMember);

		Test.startTest();
		insert groupMembers;
		Test.stopTest();

		//Assert that the collaboration group members had their notification frequency updated
		lukeGroupMember		= [SELECT Id, CollaborationGroupId, MemberId, NotificationFrequency FROM CollaborationGroupMember WHERE Id =:lukeGroupMember.Id];

		System.assertEquals('W', lukeGroupMember.NotificationFrequency);
		
		List<ConnectApi.Subscription> following = ConnectApi.ChatterUsers.getFollowings(null, lukeUser.Id).following;
		System.assertEquals(1, following.size());
		System.assertEquals(UserInfo.getName(),	following[0].subject.name);
		System.assertEquals('User',		following[0].subject.type);
		
	}

	/**
	* @description Insert a member to a group that doesn't have a group setting
	*/ 
	@IsTest
	private static void beforeInsert_InsertMembersWithGroupAndNoGroupSetting() { 
		
		User lukeUser	= [SELECT Id FROM User WHERE Id = '0052w000000FJP0AAO'];

		CollaborationGroup collGroup2 = new CollaborationGroup();
		collGroup2.Name = 'No Collaboration Members';
		collGroup2.CollaborationType = 'Public';
		insert collGroup2;
		

		List<CollaborationGroupMember> groupMembers = new List<CollaborationGroupMember>();
		
		CollaborationGroupMember lukeGroupMember	= new CollaborationGroupMember();
		lukeGroupMember.CollaborationGroupId		= collGroup2.Id;
		lukeGroupMember.MemberId					= lukeUser.Id;
		groupMembers.add(lukeGroupMember);

		Test.startTest();
		insert groupMembers;
		Test.stopTest();

		//Assert that the collaboration group members had their notification frequency updated
		lukeGroupMember		= [SELECT Id, CollaborationGroupId, MemberId, NotificationFrequency FROM CollaborationGroupMember WHERE Id =:lukeGroupMember.Id];

		System.assertEquals('N', lukeGroupMember.NotificationFrequency);

	}

}