public class ChartJS_Scales  {

	@AuraEnabled
	public ChartJS_YAxes[] yAxes { get; set; }
	
	@AuraEnabled
	public ChartJS_YAxes[] xAxes { get; set; }

}