/**
* @author Harrison Campbell
* @description Add Template Item to blocks
*/ 
public without sharing class AT_LWCC_PlannerAddTemplateItem  {

	public class KeyValuePair {

		@AuraEnabled
		public String label;

		@AuraEnabled
		public String value;

		public KeyValuePair(String label, String value) {
			this.label = label;
			this.value = value;
		}

	}

	public class TemplateInformation {
	
		@AuraEnabled
		public List<KeyValuePair> allAllergies { get; set; }
		
		@AuraEnabled
		public List<KeyValuePair> allDietRestrictions { get; set; }
		
		@AuraEnabled
		public List<TemplateItem> allTemplateItems { get; set; }

	}

	public class TemplateItem {

		/**
		* @description The Id of the Template Item
		*/ 
		@AuraEnabled
		public String itemId { get; set; }
		
		/**
		* @description The name of the Template Item
		*/ 
		@AuraEnabled
		public String itemName { get; set; }
		
		/**
		* @description The Quantity of the Template Item
		*/ 
		@AuraEnabled
		public Decimal itemQuantity { get; set; }
		
		/**
		* @description Is this in Grams, Units, etc.
		*/ 
		@AuraEnabled
		public String itemMeasurement { get; set; }
		
		/**
		* @description Total Protein for the item
		*/ 
		@AuraEnabled
		public Decimal itemProtein { get; set; } 
		
		/**
		* @description Total Carbs for the item
		*/ 
		@AuraEnabled
		public Decimal itemCarbs { get; set; } 
		
		/**
		* @description Total Fat for the item
		*/ 
		@AuraEnabled
		public Decimal itemFat { get; set; } 
		
		/**
		* @description Total Calories for the item
		*/ 
		@AuraEnabled
		public Decimal itemCalories { get; set; } 

		/**
		* @description The people with the following allergies can't consume this
		*/ 
		@AuraEnabled 
		public List<String> allergies { get; set; }
		
		/**
		* @description The people with the following dietary restrictions can't consume this
		*/ 
		@AuraEnabled
		public List<String> dietRestrictions { get; set; }
		
		@AuraEnabled
		public Decimal quantityIncrement { get; set; }

		@AuraEnabled
		public Decimal minimumQuantity { get; set; }

		/**
		* @description Convert a multi-select value to a String List
		* @param multiSelect The multi-select value
		* @return Returns the list of multi-select values
		*/ 
		private List<String> convertMultiSelectToList(String multiSelect) {

			if(String.isEmpty(multiSelect)) {
				return new List<String>();
			}

			return multiSelect.split(';');
		}

		public TemplateItem(Template_Item__c tmpItem) {
			this.itemId				= tmpItem.Id;
			this.itemName			= tmpItem.Name;
			this.itemQuantity		= tmpItem.Item_Quantity__c;
			this.itemMeasurement	= tmpItem.Measurement__c;
			this.itemProtein		= tmpItem.Protein__c;
			this.itemCarbs			= tmpItem.Carbs__c;
			this.itemFat			= tmpItem.Fat__c;
			this.itemCalories		= tmpItem.Calories__c;
			this.quantityIncrement	= tmpItem.Quantity_Increment__c;
			this.minimumQuantity	= tmpItem.Minimum_Quantity__c;
			this.allergies			= convertMultiSelectToList(tmpItem.Allergy__c);
			this.dietRestrictions	= convertMultiSelectToList(tmpItem.Restriction__c);
		}

	}

	private static List<TemplateItem> GetAllTemplateItems() {

		List<TemplateItem> tItems = new List<TemplateItem>();

		for(Template_Item__c tItem : [
			SELECT
				Id,
				Name,
				Minimum_Quantity__c,
				Quantity_Increment__c,
				Item_Quantity__c,
				Measurement__c,
				Protein__c,
				Carbs__c,
				Fat__c,
				Calories__c,
				Allergy__c,
				Restriction__c
			FROM Template_Item__c
			WHERE RecordType.DeveloperName = 'Nutrition'
		]) {
			tItems.add(new TemplateItem(tItem));
		}

		return tItems;
	}

	private static List<KeyValuePair> GetPicklistValues(String objName, String fieldName) {
 
		Schema.SObjectType s = Schema.getGlobalDescribe().get(objName) ;
		Schema.DescribeSObjectResult r = s.getDescribe() ;
		Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
		Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();

		List<KeyValuePair> picklistValues = new List<KeyValuePair>();
		for( Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()){
			picklistValues.add(new KeyValuePair(pickListVal.getLabel(), pickListVal.getValue()));
		}  
		return picklistValues;

	}

	@AuraEnabled
	public static TemplateInformation GetNutritionalTemplateItemInformation() {
		TemplateInformation tmpInfo		= new TemplateInformation();
		tmpInfo.allTemplateItems		= GetAllTemplateItems();
		tmpInfo.allAllergies			= GetPicklistValues('Template_Item__c','Allergy__c');
		tmpInfo.allDietRestrictions		= GetPicklistValues('Template_Item__c','Restriction__c');
		return tmpInfo;
	}

}