public without sharing class ChatterUtil {
    private static final Portal_Settings__c settings = Portal_Settings__c.getInstance();
    public static Boolean disableTrigger = false;
    
	@AuraEnabled
    public static Check_In__c getCheckInMemberName(String checkInID) {
        return [SELECT Name, Account__r.Name FROM Check_In__c WHERE Id = :checkInID];
    }
    
    @AuraEnabled
    public static void postFeedback(String checkInID, String text) {
        Check_In__c checkIn = [SELECT Name, Account__c FROM Check_In__c WHERE Id = :checkInID];
        
        User member = [SELECT Id, CommunityNickname FROM User WHERE Contact.AccountId = :checkIn.Account__c];
        
        Network net = [SELECT Id, Name FROM Network WHERE Name = :settings.Community_Name__c];
        
        Id communityID = postFeedItem('Feedback for ' + checkIn.Name + ':', null, text, member.Id, net.Id);
        Id internalID = postFeedItem('Feedback for ' + checkIn.Name + ':', null, text, checkIn.Id, null);
        
        if (!Test.isRunningTest()) {
            Feed_Link__c link = new Feed_Link__c(Internal_ID__c = internalID, Community_ID__c = communityID);
            insert link;
        }
    }
    
    public static Id postFeedItem(String title, String mention, String body, String subject, String networkID) {
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        if (String.isNotBlank(title)) {
            ConnectApi.TextSegmentInput titleSegmentInput = new ConnectApi.TextSegmentInput();
            titleSegmentInput.text = title + '\n';
            messageBodyInput.messageSegments.add(titleSegmentInput);
        }
        
        if (String.isNotBlank(mention)) {
            mentionSegmentInput.id = mention;
            messageBodyInput.messageSegments.add(mentionSegmentInput);
        }
        
        textSegmentInput.text = ' ' + body;
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = subject;
        
        if (!Test.isRunningTest()) {
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(networkID, feedItemInput);
            return feedElement.Id;
        } else {
            return null;
        }
    }
}