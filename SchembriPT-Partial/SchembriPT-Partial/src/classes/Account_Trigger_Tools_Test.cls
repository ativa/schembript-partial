/**
* @author Harrison Campbell
* @description Test the Account_Trigger_Tools class
*/ 
@isTest 
private class Account_Trigger_Tools_Test {

	@testSetup
    static void init() {
        Organization org = [SELECT IsSandbox FROM Organization LIMIT 1];
        
        String communityName = org.IsSandbox ? 'SchembriPTv2' : 'Schembri PT Client Community';
        
        UserRole role			= [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'ativCustomerPersonAccount'];
		UserLicense uLicense	= [SELECT Id FROM UserLicense WHERE Name = 'Customer Community Plus'];
        Profile profile			= [SELECT Id, Name, UserLicenseId FROM Profile WHERE UserLicenseId =: uLicense.Id LIMIT 1];
        
        Portal_Settings__c settings = new Portal_Settings__c(
			User_Role__c					= role.DeveloperName, 
			User_Profile__c					= profile.Name, 
			Portal_URL__c					= '/', 
			Community_Name__c				= communityName, 
			Subscription_Active_Status__c	= 'active'
		);
        
        insert settings;

		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		insert autoSettings;
    }
    
    /**
	* @description Create a Test Account
	* @return Returns a Test Account
	*/ 
	private static Account createTestAccount() {
		
		Account testAccount			= new Account();
		testAccount.FirstName		= '[Test]';
		testAccount.LastName		= '[Account]';
		insert testAccount;
		
		return testAccount;
	}
	
	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		testContact.Email				= 'test@tst.com';
		testContact.Phone				= '0489123345';

		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		System.debug('Case SOQL Limit');
		System.debug('Start ' + Limits.getQueries());

		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		//Create a Product for the case
		Programs_Products__c prod	= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 3;
		prod.Name					= 'Body Weekly';
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Program_End_Date__c		= Date.newInstance(Date.today().year(), Date.today().month() + 3, 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;

		
		System.debug('End ' + Limits.getQueries());

		return testProgram;
	}

	private static void assertAdvisoryIsCorrect(Account acc, String adType, String adSubType, String otherDesc, RecordType adRecType, Advisory__c advis) {

		System.assertEquals(adRecType.Id,																				advis.RecordTypeId);
		System.assertEquals(adType,																						advis.Type__c);
		System.assertEquals(adSubType,																					advis.Sub_Type__c);
		System.assertEquals(adRecType.Name + ' - ' + adType + ' - ' + adSubType,																	advis.Name);
		System.assertEquals(acc.Id,																						advis.Client__c);
		System.assertEquals(Date.newInstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day()),	advis.Start_Date__c);

		if(adSubType == 'Other') {
			System.assertEquals(otherDesc, advis.Description__c);
		}

	}

	private static void assertAdvisoriesAreCorrect(Account acc, String adType, List<String> adSubTypes, String otherDesc, Id adRecType) {
		
		List<Advisory__c> advisories = [
			SELECT 
				Id,
				RecordTypeId,
				Type__c,
				Sub_Type__c,
				Name,
				Client__c,
				Start_Date__c,
				Description__c
			FROM Advisory__c 
			WHERE Client__c = :acc.Id AND Type__c = :adType
		];

		RecordType advisoryRecType = [
			SELECT
				Id,
				Name
			FROM RecordType
			WHERE Id = :adRecType
		];

		System.assertEquals(adSubTypes.size(), advisories.size());
		for(Integer i = 0; i < adSubTypes.size(); i++) {
			assertAdvisoryIsCorrect(acc, adType, adSubTypes[i], otherDesc, advisoryRecType, advisories[i]);
		}

	}

	@isTest
	private static void createAdvisories_insertAllAdvisories() {
		
		Account testAcc	= new Account();
		testAcc.FirstName	= '[Test]';
		testAcc.LastName	= '[Account]';

		//Set Injury Advisories
		testAcc.Any_Injuries__pc								= 'Lower Back;Other';
		testAcc.If_selected_other_injury_please_specify__pc		= 'Hurt my Arms';
		
		//Set Exercises Can't Perform Advisories
		testAcc.Any_Exercises_You_Can_t_Perform__pc				= 'Deadlift;Other';
		testAcc.If_other_selected_please_specify__pc			= 'Climbers';

		//Set Illness Advisories
		testAcc.Have_you_been_ill_recently__pc					= 'Yes';
		testAcc.Specific_detail_of_any_injury_or_illness__pc	= 'Extreme Heart Palpatations';

		//Set Vegan Advisory
		testAcc.Are_you_Vegetarian_or_Vegan__pc					= 'Vegan';

		//Create Enjoyable foods Advisories
		testAcc.What_Foods_Do_You_Enjoy_Eating__pc				= 'Dairy;Other';
		testAcc.For_other_foods_you_enjoy_please_specify__pc	= 'Taffy';

		//Create disliked food advisories
		testAcc.Are_there_any_foods_you_dislike__pc				= 'Eggs;Other';
		testAcc.For_other_foods_you_dislike_specify__pc			= 'Cucumber';

		//Create Comftable Exercises Advisories
		testAcc.What_Exercises_are_you_Comforatble_Perfo__pc	= 'Lunges;Other';
		testAcc.Other_exercises_you_re_comfortable_doing__pc	= 'Punches';

		//Create Food Allergy Advisories
		testAcc.Lactose_intolerant__pc							= true;
		testAcc.Gluten_Free__pc									= true;
		testAcc.Shellfish_Allergy__pc							= true;
		testAcc.Nut_Allergy__pc									= true;
		testAcc.Corn_Allergy__pc								= true;
		testAcc.Dairy_Allergy__pc								= true;
		testAcc.Egg_Allergy__pc									= true;
		testAcc.Peanut_Allergy__pc								= true;
		testAcc.Meat_Allergy__pc								= true;
		testAcc.Soy_Allergy__pc									= true;
		testAcc.Other_food_allergies_intollerances__pc			= 'Clamping';

		Test.startTest();
		insert testAcc;
		CommunityOnboardingController.addAdvisories(testAcc.Id);
		Test.stopTest();

		testAcc = [
			SELECT
				Id,
				CreatedDate
			FROM Account
			WHERE Id = :testAcc.Id
		];

		Id healthRecordId			= [SELECT Id FROM RecordType WHERE DeveloperName = 'Health' AND SobjectType = 'Advisory__c'].Id;
		Id trainingRecordId			= [SELECT Id FROM RecordType WHERE DeveloperName = 'Training' AND SobjectType = 'Advisory__c'].Id;
		Id nutritionRecordId		= [SELECT Id FROM RecordType WHERE DeveloperName = 'Nutrition' AND SobjectType = 'Advisory__c'].Id;

		//Assert Injury Advisories
		assertAdvisoriesAreCorrect(testAcc, 'Injuries', new String[]{ 'Lower Back', 'Other' }, 'Hurt my Arms', healthRecordId);
		
		//Assert Exercises Advisories
		assertAdvisoriesAreCorrect(testAcc, 'Exercises I can\'t do', new String[]{ 'Deadlift','Other' }, 'Climbers', trainingRecordId);

		//Assert Illness Advisory
		assertAdvisoriesAreCorrect(testAcc, 'Medical', new String[]{ 'Illness' }, 'Extreme Heart Palpatations', healthRecordId);
		
		//Assert Vegan/Vegatarian Advisory
		assertAdvisoriesAreCorrect(testAcc, 'Dietary', new String[]{ 'Vegan' }, null, nutritionRecordId);
		
		//Assert Like Advisories
		assertAdvisoriesAreCorrect(testAcc, 'Likes', new String[]{ 'Dairy','Other' }, 'Taffy', nutritionRecordId);
		
		//Assert Dislike Advisories
		assertAdvisoriesAreCorrect(testAcc, 'Dislikes', new String[]{ 'Other','Eggs' }, 'Cucumber', nutritionRecordId);
		
		//Assert Comftable Advisories
		assertAdvisoriesAreCorrect(testAcc, 'Exercises I can do', new String[]{ 'Lunges', 'Other' }, 'Punches', trainingRecordId);
		
		//Assert Food Allergy Advisories
		assertAdvisoriesAreCorrect(testAcc, 'Allergies', new String[]{ 'Lactose', 'Gluten', 'Shellfish', 'Nut', 'Corn', 'Dairy', 'Eggs', 'Peanut', 'Meat', 'Soy', 'Other' }, 'Clamping', nutritionRecordId);

	}

	@isTest
	private static void afterInsert_insertNoAdvisories() {

		//Create test accounts with no advisories
		Account testAcc			= new Account();
		testAcc.FirstName		= '[Test]';
		testAcc.LastName		= '[Account]';

		Test.startTest();
		insert testAcc;
		CommunityOnboardingController.addAdvisories(testAcc.Id);
		Test.stopTest();

		List<Advisory__c> advisories = [SELECT Id, Client__c FROM Advisory__c WHERE Client__c =: testAcc.Id];
		System.assertEquals(0, advisories.size());

	}

	@IsTest
	private static void afterUpdate_updateAccountWithNoPrograms() {
		
		Account testAccount			= createTestAccount();

		testAccount.Subscription_Status__pc = 'Active';

		Test.startTest();
		update testAccount;
		Test.stopTest();

		//If it didn't break then we were successful

	}

	@IsTest
	private static void afterUpdate_updateAccountToOnHoldVariation1() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testAccount.Subscription_Status__pc = 'On Hold';
		
		Test.startTest();
		update testAccount;
		Test.stopTest();

		testAccount = [
			SELECT
				Id,
				Subscription_Status__pc
			FROM Account
		];


		testProgram = [
			SELECT
				Id,
				Status, 
				On_Hold_Reason__c,
				Date_Paused__c
			FROM Case
			WHERE Id = :testProgram.Id
		];

		System.assertEquals('On Hold',		testProgram.Status);
		System.assertEquals('Non Payment',	testProgram.On_Hold_Reason__c);
		System.assertEquals(Date.today(),	testProgram.Date_Paused__c);

	}

	@IsTest
	private static void afterUpdate_updateAccountToOnHoldVariation2() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testAccount.Subscription_Status__pc = 'On-Hold';

		Test.startTest();
		update testAccount;
		Test.stopTest();

		testAccount = [
			SELECT
				Id,
				Subscription_Status__pc
			FROM Account
		];


		testProgram = [
			SELECT
				Id,
				Status, 
				On_Hold_Reason__c,
				Date_Paused__c
			FROM Case
			WHERE Id = :testProgram.Id
		];

		System.assertEquals('On Hold', testProgram.Status);
		System.assertEquals('Non Payment', testProgram.On_Hold_Reason__c);
		System.assertEquals(Date.today(),	testProgram.Date_Paused__c);

	}

	@IsTest
	private static void afterUpdate_updateAccountToOnHoldVariation3() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testAccount.Subscription_Status__pc = 'on hold';

		Test.startTest();
		update testAccount;
		Test.stopTest();

		testAccount = [
			SELECT
				Id,
				Subscription_Status__pc
			FROM Account
		];


		testProgram = [
			SELECT
				Id,
				Status, 
				On_Hold_Reason__c,
				Date_Paused__c
			FROM Case
			WHERE Id = :testProgram.Id
		];

		System.assertEquals('On Hold', testProgram.Status);
		System.assertEquals('Non Payment', testProgram.On_Hold_Reason__c);
		System.assertEquals(Date.today(),	testProgram.Date_Paused__c);

	}

	@IsTest
	private static void afterUpdate_updateAccountToOnHoldVariation4() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testAccount.Subscription_Status__pc = 'on-hold';

		Test.startTest();
		update testAccount;
		Test.stopTest();

		testAccount = [
			SELECT
				Id,
				Subscription_Status__pc
			FROM Account
		];


		testProgram = [
			SELECT
				Id,
				Status, 
				On_Hold_Reason__c,
				Date_Paused__c
			FROM Case
			WHERE Id = :testProgram.Id
		];

		System.assertEquals('On Hold', testProgram.Status);
		System.assertEquals('Non Payment', testProgram.On_Hold_Reason__c);
		System.assertEquals(Date.today(),	testProgram.Date_Paused__c);

	}

	@IsTest
	private static void afterUpdate_updateAccountToOnHoldAndBackToActiveVariation1() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testAccount.Subscription_Status__pc = 'On Hold';
		update testAccount;

		testAccount.Subscription_Status__pc = 'Active';

		Test.startTest();
		update testAccount;
		Test.stopTest();

		testProgram = [
			SELECT
				Id,
				Status, 
				On_Hold_Reason__c,
				Date_Paused__c
			FROM Case
			WHERE Id = :testProgram.Id
		];

		System.assertEquals('Active',	testProgram.Status);
		System.assertEquals(null,		testProgram.On_Hold_Reason__c);
		System.assertEquals(null,		testProgram.Date_Paused__c);

	}
	
	@IsTest
	private static void afterUpdate_updateAccountToOnHoldAndBackToActiveVaration2() {
	
		//Create an account, contact, and case (Should also create a checkIn for us)
		Account testAccount		= createTestAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);

		testAccount.Subscription_Status__pc = 'On Hold';
		update testAccount;

		testAccount.Subscription_Status__pc = 'active';

		Test.startTest();
		update testAccount;
		Test.stopTest();

		testProgram = [
			SELECT
				Id,
				Status, 
				On_Hold_Reason__c,
				Date_Paused__c
			FROM Case
			WHERE Id = :testProgram.Id
		];

		System.assertEquals('Active',	testProgram.Status);
		System.assertEquals(null,		testProgram.On_Hold_Reason__c);
		System.assertEquals(null,		testProgram.Date_Paused__c);

	}


    @isTest
    static void insertClient() {
        Map<String,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
    
    	Id clientTypeID = rtMap.get('Client').getRecordTypeId();
        
        Account acc = new Account(FirstName = 'test',
                                  LastName = 'test',
                    			  PersonMobilePhone = '12345',
            					  PersonEmail = 'test@example.com',
                                  RecordTypeId = clientTypeID,
                                  Subscription_Status__pc = 'active');
        
        insert acc;
    }
}