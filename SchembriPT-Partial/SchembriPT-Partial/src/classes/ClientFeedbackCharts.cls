/**
* @author Harrison Campbell
* @description Gets Charts from Quickchart.io that displays the clients progress
*/ 
public without sharing class ClientFeedbackCharts {

	private static final String DATE_FORMAT = 'MMMdd-yy';
	//private static final String DATE_FORMAT = 'yyyy-mm-dd';


	private static List<List<Check_In__c>> splitList(List<Check_In__c> checkIns) {

		List<Check_In__c> lList = new List<Check_In__c>();
		List<Check_In__c> rList = new List<Check_In__c>();

		Integer middleIndex = checkIns.size() / 2;

		for(Integer i = 0; i < middleIndex; i++) {
			lList.add(checkIns.get(i));
		}
		for(Integer i = middleIndex; i < checkIns.size(); i++) {
			rList.add(checkIns.get(i));
		}

		List<List<Check_In__c>> lists = new List<List<Check_In__c>>();
		lists.add(lList);
		lists.add(rList);
		return lists;

	}

	private static List<List<Check_In__c>> segmentCheckIns(List<Check_In__c> checkIns) {

		List<List<Check_In__c>> lvl1_List	= splitList(checkIns);

		List<Check_In__c> l1_L				= lvl1_List.get(0);
		List<Check_In__c> l1_R				= lvl1_List.get(1);

		List<List<Check_In__c>> lvl2_L_List = splitList(l1_L);
		List<List<Check_In__c>> lvl2_R_List = splitList(l1_R);

		List<Check_In__c> l2_LL = lvl2_L_List.get(0);
		List<Check_In__c> l2_LR = lvl2_L_List.get(1);
		List<Check_In__c> l2_RL = lvl2_R_List.get(0);
		List<Check_In__c> l2_RR = lvl2_R_List.get(1);

		List<List<Check_In__c>> lvl3_LL_List = splitList(l2_LL);
		List<List<Check_In__c>> lvl3_LR_List = splitList(l2_LR);
		List<List<Check_In__c>> lvl3_RL_List = splitList(l2_RL);
		List<List<Check_In__c>> lvl3_RR_List = splitList(l2_RR);

		List<Check_In__c> l3_LLL = lvl3_LL_List.get(0);
		List<Check_In__c> l3_LLR = lvl3_LL_List.get(1);
		List<Check_In__c> l3_LRL = lvl3_LR_List.get(0);
		List<Check_In__c> l3_LRR = lvl3_LR_List.get(1);
		
		List<Check_In__c> l3_RLL = lvl3_RL_List.get(0);
		List<Check_In__c> l3_RLR = lvl3_RL_List.get(1);
		List<Check_In__c> l3_RRL = lvl3_RR_List.get(0);
		List<Check_In__c> l3_RRR = lvl3_RR_List.get(1);

		List<List<Check_In__c>> checks = new List<List<Check_In__c>>();
		checks.addAll(splitList(l3_LLL));
		checks.addAll(splitList(l3_LLR));
		checks.addAll(splitList(l3_LRL));
		checks.addAll(splitList(l3_LRR));
		checks.addAll(splitList(l3_RLL));
		checks.addAll(splitList(l3_RLR));
		checks.addAll(splitList(l3_RRL));
		checks.addAll(splitList(l3_RRR));
		return checks;

	}

	private static List<Check_In__c> GetLatest3(Id accountId, Date latestDate) {
		
		//Get all of the checkins for the current account
		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Scheduled_Check_In_Date__c,
				Weight__c,
				Waist_Measurement__c,
				Chest_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,
				Calories_Consumed_average__c,
				Sign_Up_Weight__c,
				Sign_Up_Waist__c,
				Sign_Up_Chest__c,
				Sign_Up_Hips__c,
				Sign_Up_Thighs__c,
				Sign_up_Calories_Plan_Average__c
			FROM Check_In__c
			WHERE Account__c = :accountId AND Scheduled_Check_In_Date__c != null AND Scheduled_Check_In_Date__c <= :latestDate
			ORDER BY Scheduled_Check_In_Date__c ASC
		];

		//If the number of checkins is 5 or less, then return the checkins
		if(checkIns.size() <= 5) {
			return checkIns;
		}

		List<Check_In__c> latest3CheckIns = new List<Check_In__c>();
		latest3CheckIns.add(checkIns.get(0));
		latest3CheckIns.add(checkIns.get(checkIns.size() - 4));
		latest3CheckIns.add(checkIns.get(checkIns.size() - 3));
		latest3CheckIns.add(checkIns.get(checkIns.size() - 2));
		latest3CheckIns.add(checkIns.get(checkIns.size() - 1));

		return latest3CheckIns;

	}

	
	/**
	* @description Get Check-Ins that represent the Client's entire progress
	* @param accountID 
	* @param latestDate 
	* @return 
	*/
	/* 
	private static List<Check_In__c> GetAllTime(Id accountID, Date latestDate) {
	
		//Get all of the checkins for the current account
		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Scheduled_Check_In_Date__c,
				Weight__c,
				Waist_Measurement__c,
				Chest_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,
				Calories_Consumed_average__c,
				Sign_Up_Weight__c,
				Sign_Up_Waist__c,
				Sign_Up_Chest__c,
				Sign_Up_Hips__c,
				Sign_Up_Thighs__c,
				Sign_up_Calories_Plan_Average__c
			FROM Check_In__c
			WHERE Account__c = :accountId AND Scheduled_Check_In_Date__c != null AND Scheduled_Check_In_Date__c <= :latestDate
			ORDER BY Scheduled_Check_In_Date__c ASC
		];

		//If there are 4 or less dates then return those points
		if(checkIns.size() <= 16) {
			return checkIns;
		}	

		//Return 4 dates, the start date, the end date and 2 dates that are in between
		Check_In__c startCheckIn	= checkIns[0];

		//Remove the Checkins from the list
		checkIns.remove(0);
		
		List<List<Check_In__c>> sortedLists = segmentCheckIns(checkIns);


		List<Check_In__c> checks = new List<Check_In__c>();
		checks.add(startCheckIn);
		
		Integer index = 0;

		for(List<Check_In__c> segment : sortedLists) {

			if(index > 0) {
				checks.add(segment.get(segment.size() - 1));	
			}
			index += 1;

		}

		return checks;
		
	}
	*/

	private static List<Check_In__c> GetAllTime(Id accountID, Date latestDate, Integer checkInLength) {
	
		//Get all of the checkins for the current account
		List<Check_In__c> checkIns = [
			SELECT
				Id,
				Scheduled_Check_In_Date__c,
				Weight__c,
				Waist_Measurement__c,
				Chest_Measurement__c,
				Hip_Measurement__c,
				Thigh_Measurement__c,
				Calories_Consumed_average__c,
				Sign_Up_Weight__c,
				Sign_Up_Waist__c,
				Sign_Up_Chest__c,
				Sign_Up_Hips__c,
				Sign_Up_Thighs__c,
				Sign_up_Calories_Plan_Average__c
			FROM Check_In__c
			WHERE Account__c = :accountId AND Scheduled_Check_In_Date__c != null AND Scheduled_Check_In_Date__c <= :latestDate
			ORDER BY Scheduled_Check_In_Date__c ASC
		];

		//If the number of Check-Ins returned is less than or equal to the Check-In length we are querying then return the Check-Ins
		if(checkIns.size() <= checkInLength) {
			return checkIns;
		}	

		//Get the length in days between the first and last date
		Date firstDate	= checkIns.get(0).Scheduled_Check_In_Date__c;
		Date endDate	= checkIns.get(checkIns.size() - 1).Scheduled_Check_In_Date__c;

		Integer daysBetween = firstDate.daysBetween(endDate);

		Decimal intervalDays = daysBetween / (checkInLength - 1);

		Date dayToCheck = Date.newInstance(firstDate.year(), firstDate.month(), firstDate.day());
		
		List<Check_In__c> retCheckIns = new List<Check_In__c>();

		for(Integer i = 0; i < checkIns.size(); i++) {

			Check_In__c currCheckIn = checkIns.get(i);

			if(currCheckIn.Scheduled_Check_In_Date__c >= dayToCheck) {
				retCheckIns.add(currCheckIn);
				dayToCheck = Date.newInstance(firstDate.year(), firstDate.month(), firstDate.day());
				dayToCheck = dayToCheck.addDays( Integer.valueOf(intervalDays) * retCheckIns.size());
			}
			
						
		}

		/*
		//Return 4 dates, the start date, the end date and 2 dates that are in between
		Check_In__c startCheckIn	= checkIns[0];
		checkIns.remove(0);
		Check_In__c endCheckIn		= checkIns[checkIns.size() - 1];
		checkIns.remove(checkIns.size() - 1);

		Integer segmentSize = checkIns.size() / (checkInLength - 2);

		retCheckIns.add(startCheckIn);
		for(Integer i = segmentSize; i < checkIns.size(); i += segmentSize) {
			retCheckIns.add(checkIns.get(i));

			if(retCheckIns.size() == checkInLength - 1) {
				break;
			}

		}
		retCheckIns.add(endCheckIn);
		*/

		return retCheckIns;

	}

	/**
	* @description Get the Account
	* @param accountID The Id of the account
	* @return Returns the Account
	*/ 
	private static Account GetAccount(Id accountID) {
		return [
			SELECT
				Id,
				Date_Signed_Up__pc,
				Starting_Weight__pc,
				Starting_Chest__pc,
				Starting_Hips__pc,
				Starting_Thigh__pc,
				Starting_Waist__pc
			FROM Account
			WHERE Id = :accountID
		];

	}

	/**
	* @description Get the completed Checkins
	* @param accountId The Id of the Account
	* @return Returns the completed checkins, ordered by the Scheduled CheckIn date
	*/ 
	public static List<Check_In__c> GetCompletedCheckIns(Id accountId, String range, Date latestDate) {

		List<Check_In__c> compCheckIns = new List<Check_In__c>();

		if(range == 'All Time') {
			compCheckIns = GetAllTime(accountId, latestDate, 16);
		} else if(range == 'All Time X 12') {
			compCheckIns = GetAllTime(accountId, latestDate, 12); 
		} else if(range == 'All Time X 8') {
			compCheckIns = GetAllTime(accountId, latestDate, 8); 
		} else if(range == 'Latest 3') {
			compCheckIns = GetLatest3(accountId, latestDate);
		}

		System.debug('Completed CheckIns');
		for(Check_In__c check : compCheckIns) {
			System.debug(check);
		}

		return compCheckIns;

	} 
	
	private static ChartJS_Options getBarChartOptions() {
	
		ChartJS_Options chartOptions = new ChartJS_Options();
		
		//Add the Options
		chartOptions											= new ChartJS_Options();
		chartOptions.scales										= new ChartJS_Scales();
		chartOptions.scales.yAxes								= new ChartJS_YAxes[] { new ChartJS_YAxes() };
		chartOptions.scales.yAxes.get(0).ticks					= new ChartJS_Ticks();
		chartOptions.scales.yAxes.get(0).ticks.beginAtZero		= true;
		chartOptions.scales.yAxes.get(0).ticks.fontSize			= 8;
		chartOptions.scales.yAxes.get(0).ticks.stepSize			= 500;
		chartOptions.scales.yAxes.get(0).scaleLabel				= new ChartJS_ScaleLabel();
		chartOptions.scales.yAxes.get(0).scaleLabel.fontSize	= 2;
		chartOptions.scales.yAxes.get(0).scaleLabel.fontColor	= 'rgba(255,0,0,1)';
		
		chartOptions.scales.xAxes								= new ChartJS_YAxes[] { new ChartJS_YAxes() };
		chartOptions.scales.xAxes.get(0).ticks					= new ChartJS_Ticks();
		chartOptions.scales.xAxes.get(0).ticks.fontSize			= 16;
		chartOptions.scales.xAxes.get(0).ticks.autoSkip			= false;
		chartOptions.scales.xAxes.get(0).scaleLabel				= new ChartJS_ScaleLabel();
		chartOptions.scales.xAxes.get(0).scaleLabel.fontSize	= 2;
		chartOptions.scales.xAxes.get(0).scaleLabel.fontColor	= 'rgba(255,0,0,1)';
		
		chartOptions.legend			= new ChartJS_Legend();
		chartOptions.legend.display = false;
		
		chartOptions.layout					= new ChartJS_Layout();
		chartOptions.layout.padding			= new ChartJS_Padding();
		chartOptions.layout.padding.bottom	= 0;
		chartOptions.layout.padding.top		= 10;
		chartOptions.layout.padding.left	= 0;
		chartOptions.layout.padding.right	= 0;

		return chartOptions;

	}

	private static ChartJS_Options getLineChartOptions(String titleTxt) {

		ChartJS_Options chartOptions = new ChartJS_Options();
		
		//Add the Options
		chartOptions											= new ChartJS_Options();
		chartOptions.scales										= new ChartJS_Scales();
		chartOptions.scales.yAxes								= new ChartJS_YAxes[] { new ChartJS_YAxes() };
		chartOptions.scales.yAxes.get(0).ticks					= new ChartJS_Ticks();
		chartOptions.scales.yAxes.get(0).ticks.beginAtZero		= false;
		chartOptions.scales.yAxes.get(0).ticks.fontSize			= 8;
		chartOptions.scales.yAxes.get(0).ticks.stepSize			= 10;
		chartOptions.scales.yAxes.get(0).scaleLabel				= new ChartJS_ScaleLabel();
		chartOptions.scales.yAxes.get(0).scaleLabel.fontSize	= 10;
		chartOptions.scales.yAxes.get(0).scaleLabel.fontColor	= 'rgba(255,0,0,1)';
		
		chartOptions.scales.xAxes								= new ChartJS_YAxes[] { new ChartJS_YAxes() };
		chartOptions.scales.xAxes.get(0).ticks					= new ChartJS_Ticks();
		chartOptions.scales.xAxes.get(0).ticks.beginAtZero		= false;
		chartOptions.scales.xAxes.get(0).ticks.fontSize			= 16;

		chartOptions.scales.xAxes.get(0).ticks.autoSkip			= false;
		//chartOptions.scales.xAxes.get(0).ticks.maxRotation		= 0;

		chartOptions.scales.xAxes.get(0).scaleLabel				= new ChartJS_ScaleLabel();
		chartOptions.scales.xAxes.get(0).scaleLabel.fontSize	= 2;
		chartOptions.scales.xAxes.get(0).scaleLabel.fontColor	= 'rgba(255,0,0,1)';

		chartOptions.legend			= new ChartJS_Legend();
		chartOptions.legend.display = false;

		chartOptions.layout					= new ChartJS_Layout();
		chartOptions.layout.padding			= new ChartJS_Padding();
		chartOptions.layout.padding.bottom	= 0;
		chartOptions.layout.padding.top		= 10;
		chartOptions.layout.padding.left	= 0;
		chartOptions.layout.padding.right	= 0;

		chartOptions.plugins						= new ChartJS_Plugins();
		chartOptions.plugins.datalabels				= new ChartJS_DataLabel();
		chartOptions.plugins.datalabels.font		= new ChartJS_Font();
		chartOptions.plugins.datalabels.font.size	= '2';

		chartOptions.title				= new ChartJS_Title();
		chartOptions.title.display		= true;
		chartOptions.title.fontSize		= 10;
		chartOptions.title.padding		= 3;
		chartOptions.title.text			= titleTxt;

		return chartOptions;

	}

	private static ChartJS_DataLabel lineDataLabel() {
	
		ChartJS_DataLabel lineDatalabel = new ChartJS_DataLabel();
		lineDatalabel.anchor			= 'end';
		lineDatalabel.align				= 'end';
		lineDatalabel.display			= true;
		lineDatalabel.font				= new ChartJS_Font();
		lineDatalabel.font.size			= '8';
		lineDatalabel.offset			= 2;
		lineDatalabel.textStrokeWidth	= 1;
		lineDatalabel.textStrokeColor	= 'rgba(33,33,33,1)';
		lineDatalabel.textShadowBlur	= 1;
		lineDatalabel.textShadowColor	= 'rgba(100,100,100,1)';
		lineDatalabel.color				= 'rgba(255,255,255,1)';

		return lineDatalabel;
	}

	private static ChartJS_DataLabel barDataLabel() {
		
		ChartJS_DataLabel bDataLabel = new ChartJS_DataLabel();
		bDataLabel.anchor				= 'end';
		bDataLabel.align				= 'end';
		bDataLabel.display			= true;
		bDataLabel.font				= new ChartJS_Font();
		bDataLabel.font.size		= '14';
		//barDataLabel.clamp				= true;
		bDataLabel.textStrokeWidth	= 3;
		bDataLabel.textStrokeColor	= 'rgba(33,33,33,1)';
		bDataLabel.textShadowBlur		= 3;
		bDataLabel.textShadowColor	= 'rgba(100,100,100,1)';
		bDataLabel.color				= 'rgba(255,255,255,1)';
		return bDataLabel;


	}
	

	public static ClientFeedback_Chart GetWeightChart(Id accountId, String range, Date latestDate) {

		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);

		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'line';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];
		
		//Add the Options
		newChart.options = getLineChartOptions('Weight (kg)');


		ChartJS_Dataset dataSet = new ChartJS_Dataset();
		dataSet.fill			= false;
		dataSet.label			= 'Weight(kg)';
		dataSet.borderColor		= 'rgba(56,33,25,1)';
		dataSet.data			= new Decimal[0];
		dataSet.datalabels		= lineDataLabel();

		newChart.data.datasets = new ChartJS_Dataset[] { dataSet };
		
		Decimal minValue = 2147483647.0;
		Decimal maxValue = -2147483647.0;

		Integer index = 0;

		for(Check_In__c cIn : completedCheckIns) {

			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));

			Decimal weight = 0;

			if(index == 0) {
				weight = (cIn.Sign_Up_Weight__c != null) ? cIn.Sign_Up_Weight__c : weight;
			} else { 
				weight = (cIn.Weight__c != null) ? cIn.Weight__c : weight;
			}
			
			dataSet.data.add(weight);
			minValue = Math.min(weight, minValue);
			maxValue = Math.max(weight, maxValue);

			index += 1;

		}

		minValue -= 10;
		maxValue += 10;
		
		minValue /= 10;
		maxValue /= 10;

		minValue = minValue.setScale(0, RoundingMode.FLOOR);
		maxValue = maxValue.setScale(0, RoundingMode.CEILING);

		minValue *= 10;
		maxValue *= 10;
		
		newChart.options.scales.yAxes.get(0).ticks.min = minValue;
		newChart.options.scales.yAxes.get(0).ticks.max = maxValue;

		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'Weight Chart';
		newFeedbackChart.path	= '/' + accountId + '/WeightChart' + Date.today().format() + '.jpeg';

		return newFeedbackChart;

	}

	public static ClientFeedback_Chart GetWaistChart(Id accountId, String range, Date latestDate) {
	
		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);

		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'line';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];
		
		ChartJS_Dataset dataSet = new ChartJS_Dataset();
		dataSet.fill			= false;
		dataSet.label			= 'Waist(cm)';
		dataSet.borderColor		= 'rgba(56,33,25,1)';
		dataSet.data			= new Decimal[0];
		dataSet.datalabels		= lineDataLabel();
		newChart.data.datasets	= new ChartJS_Dataset[] { dataSet };
		
		newChart.options = getLineChartOptions('Waist (cm)');
		
		Decimal minValue = 2147483647.0;
		Decimal maxValue = -2147483647.0;

		Integer index = 0;

		for(Check_In__c cIn : completedCheckIns) {

			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));
			
			Decimal waist = 0;

			if(index == 0) {
				waist = (cIn.Sign_Up_Waist__c != null) ? cIn.Sign_Up_Waist__c : waist;
			} else { 
				waist = (cIn.Waist_Measurement__c != null) ? cIn.Waist_Measurement__c : waist;
			}
			
			dataSet.data.add(waist);
			minValue = Math.min(waist, minValue);
			maxValue = Math.max(waist, maxValue);

			index += 1;

		}

		minValue -= 10;
		maxValue += 10;

		minValue /= 10;
		maxValue /= 10;

		minValue = minValue.setScale(0, RoundingMode.FLOOR);
		maxValue = maxValue.setScale(0, RoundingMode.CEILING);

		minValue *= 10;
		maxValue *= 10;
		
		newChart.options.scales.yAxes.get(0).ticks.min = minValue;
		newChart.options.scales.yAxes.get(0).ticks.max = maxValue;
		
		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'Waist Chart';
		newFeedbackChart.path	= '/' + accountId + '/WaistChart' + Date.today().format() + '.jpeg';

		return newFeedbackChart;

	}

	public static ClientFeedback_Chart GetChestChart(Id accountId, String range, Date latestDate) {
	
		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);

		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'line';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];
		
		ChartJS_Dataset dataSet = new ChartJS_Dataset();
		dataSet.fill			= false;
		dataSet.label			= 'Chest(cm)';
		dataSet.borderColor		= 'rgba(56,33,25,1)';
		dataSet.data			= new Decimal[0];
		dataSet.datalabels		= lineDataLabel();
		newChart.data.datasets	= new ChartJS_Dataset[] { dataSet };
		
		newChart.options = getLineChartOptions('Chest (cm)');
		
		Decimal minValue = 2147483647.0;
		Decimal maxValue = -2147483647.0;

		Integer index = 0;

		for(Check_In__c cIn : completedCheckIns) {
			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));

			Decimal chest = 0;

			if(index == 0) {
				chest = (cIn.Sign_Up_Chest__c != null) ? cIn.Sign_Up_Chest__c : chest;
			} else { 
				chest = (cIn.Chest_Measurement__c != null) ? cIn.Chest_Measurement__c : chest;
			}
			
			dataSet.data.add(chest);
			minValue = Math.min(chest, minValue);
			maxValue = Math.max(chest, maxValue);

			index += 1;

		}
		
		minValue -= 10;
		maxValue += 10;
		
		minValue /= 10;
		maxValue /= 10;

		minValue = minValue.setScale(0, RoundingMode.FLOOR);
		maxValue = maxValue.setScale(0, RoundingMode.CEILING);

		minValue *= 10;
		maxValue *= 10;
		
		newChart.options.scales.yAxes.get(0).ticks.min = minValue;
		newChart.options.scales.yAxes.get(0).ticks.max = maxValue;

		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'Chest Chart';
		newFeedbackChart.path	= '/' + accountId + '/ChestChart' + Date.today().format() + '.jpeg';

		return newFeedbackChart;

	}

	public static ClientFeedback_Chart GetHipChart(Id accountId, String range, Date latestDate) {
	
		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);

		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'line';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];
		
		ChartJS_Dataset dataSet = new ChartJS_Dataset();
		dataSet.fill			= false;
		dataSet.label			= 'Hip(cm)';
		dataSet.borderColor		= 'rgba(56,33,25,1)';
		dataSet.data			= new Decimal[0];
		dataSet.datalabels		= lineDataLabel();
		newChart.data.datasets	= new ChartJS_Dataset[] { dataSet };
		
		newChart.options = getLineChartOptions('Hip (cm)');
		
		Decimal minValue = 2147483647.0;
		Decimal maxValue = -2147483647.0;

		Integer index = 0;

		for(Check_In__c cIn : completedCheckIns) {
			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));

			Decimal hips = 0;

			if(index == 0) {
				hips = (cIn.Sign_Up_Hips__c != null) ? cIn.Sign_Up_Hips__c : hips;
			} else { 
				hips = (cIn.Hip_Measurement__c != null) ? cIn.Hip_Measurement__c : hips;
			}
			
			dataSet.data.add(hips);
			minValue = Math.min(hips, minValue);
			maxValue = Math.max(hips, maxValue);

			index += 1;

		}
		
		minValue -= 10;
		maxValue += 10;
		
		minValue /= 10;
		maxValue /= 10;

		minValue = minValue.setScale(0, RoundingMode.FLOOR);
		maxValue = maxValue.setScale(0, RoundingMode.CEILING);

		minValue *= 10;
		maxValue *= 10;
		
		newChart.options.scales.yAxes.get(0).ticks.min = minValue;
		newChart.options.scales.yAxes.get(0).ticks.max = maxValue;

		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'Hip Chart';
		newFeedbackChart.path	= '/' + accountId + '/HipChart' + Date.today().format() + '.jpeg';

		return newFeedbackChart;

	}

	public static ClientFeedback_Chart GetThighChart(Id accountId, String range, Date latestDate) {
	
		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);

		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'line';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];
		
		ChartJS_Dataset dataSet = new ChartJS_Dataset();
		dataSet.fill			= false;
		dataSet.label			= 'Thigh(cm)';
		dataSet.borderColor		= 'rgba(56,33,25,1)';
		dataSet.data			= new Decimal[0];
		dataSet.datalabels		= lineDataLabel();
		newChart.data.datasets	= new ChartJS_Dataset[] { dataSet };
		
		newChart.options = getLineChartOptions('Thigh (cm)');
		
		Decimal minValue = 2147483647.0;
		Decimal maxValue = -2147483647.0;

		Integer index = 0;

		for(Check_In__c cIn : completedCheckIns) {
			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));
			
			Decimal thigh = 0;

			if(index == 0) {
				thigh = (cIn.Sign_Up_Thighs__c != null) ? cIn.Sign_Up_Thighs__c : thigh;
			} else { 
				thigh = (cIn.Thigh_Measurement__c != null) ? cIn.Thigh_Measurement__c : thigh;
			}
			
			dataSet.data.add(thigh);
			minValue = Math.min(thigh, minValue);
			maxValue = Math.max(thigh, maxValue);

			index += 1;

		}
		
		minValue -= 10;
		maxValue += 10;
		
		minValue /= 10;
		maxValue /= 10;

		minValue = minValue.setScale(0, RoundingMode.FLOOR);
		maxValue = maxValue.setScale(0, RoundingMode.CEILING);

		minValue *= 10;
		maxValue *= 10;
		
		newChart.options.scales.yAxes.get(0).ticks.min = minValue;
		newChart.options.scales.yAxes.get(0).ticks.max = maxValue;

		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'Thigh Chart';
		newFeedbackChart.path	= '/' + accountId + '/ThighChart' + Date.today().format() + '.jpeg';

		return newFeedbackChart;

	}

	public static ClientFeedback_Chart GetCalorieChart(Id accountId, String range, Date latestDate) {
	
		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);
		
		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'bar';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];

		newChart.options = getBarChartOptions();
		
		ChartJS_Dataset dataSet = new ChartJS_Dataset();
		dataSet.fill			= false;
		dataSet.label			= 'Calories';
		dataSet.borderColor		= 'black';
		dataSet.backgroundColor = '#bdbdbd';
		dataSet.data			= new Decimal[0];
		dataSet.datalabels		= barDataLabel();
		newChart.data.datasets	= new ChartJS_Dataset[] { dataSet };
		
		Decimal maxValue = -2147483647.0;

		Integer index = 0;

		for(Check_In__c cIn : completedCheckIns) {
			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));

			Decimal calories = 0;

			if(index == 0) {
				calories = (cIn.Calories_Consumed_average__c != null) ? cIn.Calories_Consumed_average__c : calories;
			} else { 
				calories = (cIn.Calories_Consumed_average__c != null) ? cIn.Calories_Consumed_average__c : calories;
			}
			
			dataSet.data.add(calories);
			maxValue = Math.max(calories, maxValue);

			index += 1;

		}
		

		
		Integer mValue		= Integer.valueOf(maxValue);
		Integer remainder	= Math.mod(mValue, 500);
		Integer newMValue	=  Integer.valueOf(mValue - remainder + 1000);

		/*
		maxValue += 1000;
		maxValue /= 1000;
		maxValue = maxValue.setScale(0, RoundingMode.CEILING);
		maxValue *= 1000;
		*/

		newChart.options.scales.yAxes.get(0).ticks.max = newMValue;

		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'Calorie Chart';
		newFeedbackChart.path	= '/' + accountId + '/CalorieChart' + Date.today().format() + '.jpeg';

		
		return newFeedbackChart;

	}

	public static ClientFeedback_Chart GetAllChart(Id accountId, String range, Date latestDate) {
	
		//Query all of the Checkins that are completed for the client
		List<Check_In__c> completedCheckIns = GetCompletedCheckIns(accountId, range, latestDate);
		
		ChartJS_Chart newChart		= new ChartJS_Chart();
		newChart.type				= 'line';
		newChart.data				= new ChartJS_Data();
		newChart.data.labels		= new String[0];
		
		newChart.options = getLineChartOptions('All');

		ChartJS_Dataset weightDataset	= new ChartJS_Dataset();
		weightDataset.fill				= false;
		weightDataset.label				= 'Weight';
		weightDataset.borderColor		= 'rgba(56,33,25,1)';
		weightDataset.data				= new Decimal[0];
		weightDataset.datalabels		= lineDataLabel();

		ChartJS_Dataset chestDataset	= new ChartJS_Dataset();
		chestDataset.fill				= false;
		chestDataset.label				= 'Chest';
		chestDataset.borderColor		= 'rgba(133,192,249,1)';
		chestDataset.data				= new Decimal[0];
		chestDataset.datalabels		= lineDataLabel();

		ChartJS_Dataset waistDataset	= new ChartJS_Dataset();
		waistDataset.fill				= false;
		waistDataset.label				= 'Waist';
		waistDataset.borderColor		= 'rgba(245,121,58,1)';
		waistDataset.data				= new Decimal[0];
		waistDataset.datalabels		= lineDataLabel();

		ChartJS_Dataset hipDataset	= new ChartJS_Dataset();
		hipDataset.fill				= false;
		hipDataset.label			= 'Hip';
		hipDataset.borderColor		= 'red';
		hipDataset.data				= new Decimal[0];
		hipDataset.datalabels		= lineDataLabel();

		ChartJS_Dataset thighDataset	= new ChartJS_Dataset();
		thighDataset.fill				= false;
		thighDataset.label				= 'Thigh';
		thighDataset.borderColor		= 'rgba(92,202,68,1)';
		thighDataset.data				= new Decimal[0];
		thighDataset.datalabels			= lineDataLabel();

		newChart.data.datasets			= new ChartJS_Dataset[] { weightDataset, chestDataset, waistDataset, hipDataset, thighDataset };

		System.debug('WEIGHT CHART');

		for(Check_In__c cIn : completedCheckIns) {

			System.debug(cIn.Scheduled_Check_In_Date__c + ' ' + cIn.Id);

			Datetime nDT = Datetime.newInstance(cIn.Scheduled_Check_In_Date__c.year(), cIn.Scheduled_Check_In_Date__c.month(), cIn.Scheduled_Check_In_Date__c.day());
			newChart.data.labels.add(nDT.format(DATE_FORMAT));

			weightDataset.data.add(cIn.Weight__c);
			chestDataset.data.add(cIn.Chest_Measurement__c);
			waistDataset.data.add(cIn.Waist_Measurement__c);
			hipDataset.data.add(cIn.Hip_Measurement__c);
			thighDataset.data.add(cIn.Thigh_Measurement__c);

		}
		
		ClientFeedback_Chart newFeedbackChart = new ClientFeedback_Chart();
		newFeedbackChart.chart	= newChart;
		newFeedbackChart.title	= 'All Chart';
		newFeedbackChart.path	= '/' + accountId + '/AllChart' + Date.today().format() + '.jpeg';

		
		return newFeedbackChart;

	}


	public static void GetChartData(List<ClientFeedback_Chart> feedbackCharts) {

		Http hp = new Http();

		List<ContentVersion> versionsToAdd = new List<ContentVersion>();

		for(ClientFeedback_Chart cfChart : feedbackCharts) {
		
			HttpRequest req = new HttpRequest();
			req.setEndpoint(QuickChart_Adaptor.getImageURL(cfChart.chart));
			req.setMethod('GET');
			req.setHeader('Content-Type', 'image/jpeg');
			req.setTimeout(60000);

			HttpResponse res = hp.send(req);
			cfChart.imgBinary = res.getBodyAsBlob();

			ContentVersion newImageVersion	= new ContentVersion();
			newImageVersion.VersionData			= cfChart.imgBinary;
			newImageVersion.Title				= cfChart.title;
			newImageVersion.ContentLocation		= 's';
			newImageVersion.PathOnClient			= cfChart.path;
			cfChart.contVersion = newImageVersion;

			versionsToAdd.add(newImageVersion);

		}
		
		insert versionsToAdd; 
		
		//Update the ContentVersions with the ContentDocumentId
		Map<Id, ContentVersion> versionMap = new Map<Id, ContentVersion>([
			SELECT
				Id,
				ContentDocumentId,
				VersionData,
				Title,
				ContentLocation,
				PathOnClient
			FROM ContentVersion
			WHERE Id IN :(new Map<Id, ContentVersion>(versionsToAdd)).KeySet()
		]);

		for(ClientFeedback_Chart cfChart : feedbackCharts) {
			ContentVersion newVersion = versionMap.get(cfChart.contVersion.Id);
			cfChart.contVersion = newVersion;
		}




	}

}