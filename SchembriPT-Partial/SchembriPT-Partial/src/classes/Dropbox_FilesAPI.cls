public class Dropbox_FilesAPI {

	private static Dropbox_Settings__c settings = Dropbox_Settings__c.getInstance();
	
	private static final String API_DROPBOX_API = 'https://api.dropboxapi.com/2';

	private static final String CONTENT_DROPBOX_API = 'https://content.dropboxapi.com/2';

	/**
	* @author Harrison Campbell
	* @description Returns a result from the '/files/upload' api call
	*/ 
	public class UploadResult {

		public String name { get; set; }

		public String id { get; set; }

		public String client_modified { get; set; }
		
		public String server_modified { get; set; }
		
		public String rev { get; set; }
		
		public Integer size { get; set; }
		
		public String path_lower { get; set; }
		
		public String path_display { get; set; }

		//sharing_info class here

		public Boolean is_downloadable { get; set; }
		
		//property_groups class here

		public Boolean has_explicit_shared_members { get; set; }

		public String content_hash { get; set; }

	}

	public class ListFolderPacket {

		public List<Metadata> entries { get; set; }

	}

	public class Metadata {

		public String name				{ get; set; }
		public String id				{ get; set; }
		public String client_modified	{ get; set; }
		public String server_modified   { get; set; }
		public String rev				{ get; set; }
		public Integer size				{ get; set; }
		public String path_lower		{ get; set; }
		public String path_display		{ get; set; }

	}

	/**
	* @description Upload a file to Dropbox
	* @param path The Path of the File
	*/ 
	public static UploadResult Upload(String fullPath, Blob binaryData) {
		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setEndpoint(CONTENT_DROPBOX_API + '/files/upload');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Dropbox-API-Arg', '{"path": "' + fullPath + '","mode": "overwrite","autorename": true,"mute": false,"strict_conflict": false}');
		request.setHeader('Content-Type', 'application/octet-stream');
		request.setBodyAsBlob(binaryData);
		HttpResponse response = (new Http()).send(request);
		System.debug(response.getStatus());
		System.debug(response.getStatusCode());
		System.debug(response.getBody());

		UploadResult uploadRes = (UploadResult)JSON.deserialize(response.getBody(), UploadResult.class);
		return uploadRes;
		
	}
	
	public static ListFolderPacket ListFolder(String path) {
		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setEndpoint(API_DROPBOX_API + '/files/list_folder');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Content-Type', 'application/json');
		request.setBody('{"path": "' + path + '"}');
		
		HttpResponse response = (new Http()).send(request);
		System.debug(response.getStatus());
		System.debug(response.getStatusCode());
		System.debug(response.getBody());

		ListFolderPacket listFolderPacket = (ListFolderPacket)JSON.deserialize(response.getBody(), ListFolderPacket.class);
		return listFolderPacket;

	}

	public static void GetTemporaryLink(String path, String fileName) {

		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setEndpoint(API_DROPBOX_API + '/files/get_temporary_link');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Content-Type', 'application/json');
		request.setBody('{"path": "/GottaGoFast.png"}');
		HttpResponse response = (new Http()).send(request);
		System.debug(response.getStatus());
		System.debug(response.getStatusCode());
		System.debug(response.getBody());
	}

	public static Map<String, Object> CreateFolder(String path, Boolean autorename) {

		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setEndpoint(API_DROPBOX_API + '/files/create_folder_v2');
		request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
		request.setHeader('Content-Type', 'application/json');

		Map<String, Object> body = new Map<String, Object>();
		if(path != null) {
			body.put('path',		path);
		}
		if(autorename != null) {
			body.put('autorename',	autorename); 
		}
		request.setBody(JSON.serialize(body));

		HttpResponse response = (new Http()).send(request);
		return (Map<String, Object>)JSON.deserializeUntyped(response.getBody());

	}

    public static void Search(String query) {

        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(API_DROPBOX_API + '/files/search_v2');
        request.setHeader('Authorization', String.format('Bearer {0}', new String[]{ settings.Authorization_Code__c }));
        request.setHeader('Content-Type', 'application/json');

        Map<String, Object> body = new Map<String, Object>();
        if(query != null) {
            body.put('query',        query);
        }
        request.setBody(JSON.serialize(body));
        HttpResponse response = (new Http()).send(request);
        System.debug(response.getStatus());
        System.debug(response.getStatusCode());
        System.debug(response.getBody());


    }

}