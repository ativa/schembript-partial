/**
* @author Harrison Campbell
* @description Exception that is thrown for SchembriPT
*/ 
public class AT_SchembriPTException extends Exception { }