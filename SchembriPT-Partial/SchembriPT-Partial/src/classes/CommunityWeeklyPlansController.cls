public without sharing class CommunityWeeklyPlansController {

    @AuraEnabled
    public static String getAccountID() {
		User u = [SELECT Contact.AccountId, Name FROM User WHERE Id = :UserInfo.getUserId()];

		System.debug('Returning account ID: ' + u.Contact.AccountId);

        return u.Contact.AccountId;
    }

	public class WeeklyPlanData {

		@AuraEnabled
		public Check_In__c checkIn { get; set; }

		@AuraEnabled
		public List<Plan__c> plans { get; set; }

	}

	/**
	* @description Query the weekly data for the checkin
	* @param personAccId The Id of the Person Account
	* @param startDate The start of the date range
	* @param endDate The end of the date range
	* @return Returns the Weekly data
	*/
	@AuraEnabled
	public static WeeklyPlanData queryWeeklyData(String personAccId, Date startDate, Date endDate) {

		//Check_In__c checkIn = checkIns[0];

		Id planSessionRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Session_Plan'].Id;

		//Get all of the session plans that relate to the check-in
		List<Plan__c> sessionPlans = [
			SELECT
				Id,
				Name,
				Session_Date__c,
				Total_Calories__c,
				Total_Protein__c,
				Total_Carbs__c,
				Total_Fat__c,
				Actual_Plan_Calories__c,
				Actual_Plan_Protein__c,
				Actual_Plan_Carbs__c,
				Actual_Plan_Fat__c,
				Ref_Trainer_Total_Calories__c,
				Ref_Trainer_Total_Protein__c,
				Ref_Trainer_Total_Carbs__c,
				Ref_Trainer_Total_Fat__c
			FROM Plan__c
			WHERE Account__c = :personAccId AND Session_Date__c >= :startDate AND Session_Date__c <= :endDate AND RecordTypeId =: planSessionRecId
			ORDER BY Session_Date__c ASC
		];



		WeeklyPlanData planData = new WeeklyPlanData();
		//planData.checkIn		= checkIn;
		planData.plans			= sessionPlans;
		return planData;

	}

}