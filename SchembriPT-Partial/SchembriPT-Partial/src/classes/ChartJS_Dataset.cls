/**
* @author Harrison Campbell
* @description Displays the data in a set
*/ 
public class ChartJS_Dataset {

	/**
	* @description The label of the dataset
	*/ 
	@AuraEnabled
	public String label { get; set; }

	/**
	* @description Is this dataset used for a line, bar, or other graph type (Used for mixed charts)
	*/ 
	@AuraEnabled
	public String type { get; set; }

	/**
	* @description The data in numbers to display
	*/ 
	@AuraEnabled
	public Decimal[] data { get; set; }

	@AuraEnabled
	public String[] labels { get; set; }

	/**
	* @description The border color of each point
	*/ 
	@AuraEnabled
	public String borderColor { get; set; }


	/**
	* @description Should the Inside if the chart be full
	*/ 
	@AuraEnabled
	public Boolean fill { get; set; }
	
	/**
	* @description Sets options for displaying labels on the chart
	*/ 
	@AuraEnabled
	public ChartJS_DataLabel datalabels { get; set; }

	@AuraEnabled
	public Integer borderWidth { get; set; }

	@AuraEnabled
	public String backgroundColor { get; set; }
	
}