/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-27-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-27-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest 
private class ClientProgressSingleChartsController_T {

	private static Automation_Settings__c testTool_createAutomationSettings() {
	
		Automation_Settings__c autoSettings = Automation_Settings__c.getInstance();
		autoSettings.Enable_Automation__c = true;
		autoSettings.Enable_Onboarding_Automation__c = true;
		insert autoSettings;

		return autoSettings;

	}

	private static Account testTool_createPersonAccount() {
	
		Id pAccRecType = [SELECT Id FROM RecordType WHERE IsPersonType = true AND SobjectType = 'Account'].Id;

        Account acc	= new Account(
			RecordTypeID				= pAccRecType,
			FirstName					= 'Test',
			LastName					= 'Account',
			PersonMailingStreet			= 'test@yahoo.com',
			PersonMailingPostalCode		= '12345',
			PersonMailingCity			= 'SFO',
			PersonEmail					= 'test@yahoo.com',
			PersonHomePhone				= '1234567',
			PersonMobilePhone			= '12345678',
			SF_Community_User__pc		= UserInfo.getUserId()
        );

        insert acc;
		
		return acc;
	}

	private static User testTool_createPortalUser(Account personAccount) {
	
		Account acc = [
			SELECT
				Id,
				PersonContactId
			FROM Account
			WHERE Id = :personAccount.Id
		];

		Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Plus User - CUSTOM']; 
        User u = new User(
			Alias = 'standt', 
			Email='aaaaaaa@testorg.com', 
            EmailEncodingKey='UTF-8', 
			LastName='Testing', 
			LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
			ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
			UserName='aaaaaaaa@testorg.com'
		);
		u.ContactId = acc.PersonContactId;
		insert u;

		return u;
	}

	/**
	* @description Update the related test contact
	* @param testAccount The related Test Account
	* @return Returns the contact that was updated
	*/ 
	private static Contact updateTestContact(Account testAccount) {
	
		Contact testContact = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId = :testAccount.Id];
		//testContact.Email				= 'test@tst.com';
		//testContact.Phone				= '0489123345';
		testContact.Any_Exercises_You_Can_t_Perform__c				= 'Deadlift';
		testContact.Any_Injuries__c									= 'Chest';
		testContact.Any_additional_info_you_deem_relevant__c		= 'Additional Info';
		testContact.Are_there_any_foods_you_dislike__c				= 'Chicken';
		testContact.Are_you_Vegetarian_or_Vegan__c					= 'Vegan';
		testContact.Are_you_currently_breastfeeding__c				= 'No';
		testContact.Are_you_taking_any_medication__c				= 'No';
		testContact.Body_Fat_Reduction__c							= 'Yes';
		testContact.Body_Recomposition__c							= 'Yes';
		testContact.Body_Transformation__c							= 'Yes';
		testContact.Bodybuilding_Competition__c						= 'Yes';
		testContact.Do_you_drink_alcohol__c							= 'Yes';
		testContact.Do_you_smoke__c									= 'Yes';
		testContact.Do_you_take_any_recreational_drugs__c			= 'Yes';
		testContact.What_Exercises_are_you_Comforatble_Perfo__c		= 'Squat';
		testContact.For_other_foods_you_enjoy_please_specify__c		= 'Other Food';
		testContact.Have_you_been_ill_recently__c					= 'Yes';
		testContact.Hospitalised_recently__c						= 'No';
		testContact.How_active_are_you_most_days__c					= 'Moderate';
		testContact.How_long_have_you_been_going_to_the_gym__c		= '3 years';
		testContact.How_many_drinks_per_week__c						= '20';
		testContact.How_many_times_do_you_do_drugs_per_week__c		= '10';
		testContact.If_other_selected_please_specify__c				= 'aaa';
		testContact.If_selected_other_injury_please_specify__c		= 'bbb';
		testContact.Implement_Regular_Physical_Training__c			= 'Yes';
		testContact.Improve_Cardiovascular_Fitness__c				= 'Yes';
		testContact.Improve_Overall_Lifestyle_and_Fitness__c		= 'No';
		testContact.Increase_Lean_Muscle_Mass__c					= 'No';
		testContact.Increase_Strength__c							= 'No';
		testContact.Learn_to_Manage_Training_Hours__c				= 'Yes';
		testContact.Other_activities_you_enjoy_doing__c				= 'ccc';
		testContact.Other_exercises_you_re_comfortable_doing__c		= 'ddd';
		testContact.Other_fitness_goals__c							= 'eee';
		testContact.For_other_foods_you_dislike_specify__c			= 'fff';
		testContact.Post_Injury_Rehab_Recovery__c					= 'Yes';
		testContact.Potential_Heart_Concerns__c						= 'No';
		testContact.Pregnant_or_given_birth_in_last_12_month__c		= 'No';
		testContact.Specific_detail_of_any_injury_or_illness__c		= 'ggg';
		testContact.Surgery_recently__c								= 'Yes';
		testContact.What_Foods_Do_You_Enjoy_Eating__c				= 'Chicken';
		testContact.What_time_of_day_do_you_train__c				= '8am';
		testContact.Your_ideal_training_week_Cardio__c				= 'hhh';
		testContact.Your_ideal_training_week_HIIT__c				= 'iii';
		testContact.Your_ideal_training_week_Resistance__c			= 'jjj';

		update testContact;

		return testContact;

	}

	/**
	* @description Create a test program
	* @param testAccount The Test Account
	* @param testContact The Test Contact
	* @return Return the Case
	*/ 
	private static Case createTestCase(Account testAccount, Contact testContact) {
	
		Case testProgram					= new Case();
		testProgram.AccountId				= testAccount.Id;
		testProgram.ContactId				= testContact.Id;
		testProgram.Status					= 'New';

		testProgram.Program_Start_Date__c	= Date.newInstance(Date.today().year(), Date.today().month(), 1);
		testProgram.Check_In_Frequency__c	= '1 Week';
		
		//Create a Product for the case
		Programs_Products__c prod			= new Programs_Products__c(); 
		prod.Duration_weeks__c		= 12;
		prod.Name					= 'Body Revolt'; //I need to do this for the process builder
		insert prod;
		testProgram.Programs_Products__c = prod.Id;

		insert testProgram;	
				
		testProgram.Status = 'Active';
		update testProgram;


		return testProgram;
	}

	private static void testTool_createCheckIns(Case currentProgram, Contact cont, Integer checkInCount) {

		List<Check_In__c> allProgramCheckIns = [
			SELECT
				Id
			FROM Check_In__c
			WHERE Client_Program__c = :currentProgram.Id
		];
		delete allProgramCheckIns;

		List<Check_In__c> newCheckIns = new List<Check_In__c>();

		for(Integer i = 0; i < checkInCount; i++) {
			Check_In__c newCheckIn 					= new Check_In__c();
			newCheckIn.Name 						= 'Check-In' + i;
			newCheckIn.Client_Program__c 			= currentProgram.Id;
			newCheckIn.Scheduled_Check_In_Date__c 	= Date.today().addDays(i);
			newCheckIn.Check_In_Number_v2__c		= i;
			newCheckIn.Thigh_Measurement__c			= 33 + (i+1);
			newCheckIn.Waist_Measurement__c			= 45 + (i+1);
			newCheckIn.Chest_Measurement__c			= 56 + (i+1);
			newCheckIn.Hip_Measurement__c			= 50 + (i+1);
			newCheckIn.Weight__c					= 75 + (i+1);
			newCheckIn.Stage__c 					= 'Completed';
			newCheckIn.Contact__c					= cont.Id;
			newCheckIn.h_Test_Record__c				= true;
			newCheckIns.add(newCheckIn);
		}

		insert newCheckIns;

	}
	
	
	@isTest
	private static void InitProgressCharts_functionalTest() {

		Automation_Settings__c autoSettings = testTool_createAutomationSettings();

		Account testAccount		= testTool_createPersonAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		
		testTool_createCheckIns(testProgram, testContact, 2);

		Check_In__c check = [
			SELECT
				Id
			FROM Check_In__c
			LIMIT 1
		];

		Test.startTest();
		ClientProgressSingleChartsController.ProgressInit pInit = ClientProgressSingleChartsController.InitProgressCharts(check.Id, 'All Time');
		ClientProgressSingleChartsController.ProgressInit pInit2 = ClientProgressSingleChartsController.InitProgressCharts(check.Id, 'Latest 3');
		Test.stopTest();

	}
	
	@isTest
	private static void GetData_functionalTest() {

		Automation_Settings__c autoSettings = testTool_createAutomationSettings();

		Account testAccount		= testTool_createPersonAccount();
		Contact testContact		= updateTestContact(testAccount);
		Case testProgram		= createTestCase(testAccount, testContact);
		
		testTool_createCheckIns(testProgram, testContact, 2);

		Check_In__c check = [
			SELECT
				Id
			FROM Check_In__c
			LIMIT 1
		];

		Test.startTest();
		ClientProgressSingleChartsController.ProgressData pInit = ClientProgressSingleChartsController.GetData(check.Id, 'All Time');
		ClientProgressSingleChartsController.ProgressData pInit2 = ClientProgressSingleChartsController.GetData(check.Id, 'Latest 3');
		Test.stopTest();

	}
}