@isTest 
private without sharing class AT_F_ClonePlans_T {

	private static Contact testTool_createContact() {
		Contact testContact = new Contact();
		testContact.LastName = 'Test';
		insert testContact;
		return testContact;
	}

	private static Check_In__c testTool_createCheckIn(Contact testContact, Date nutPlanStartDate, Date nutPlanEndDate) {
		Check_In__c testCheckIn						= new Check_In__c();
		testCheckIn.h_Test_Record__c				= true;
		testCheckIn.Name							= 'Test Check-In';
		testCheckIn.Contact__c						= testContact.Id;
		testCheckIn.Nutrition_Plan_Start_Date__c	= nutPlanStartDate;
		testCheckIn.Nutrition_Plan_End_Date__c		= nutPlanEndDate;
		insert testCheckIn;
		return testCheckIn;
	}

	private static Template_Plan__c testTool_createTemplatePlan(String planName, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, Id planRTypeId) {
		Template_Plan__c tmpPlan			= new Template_Plan__c();
		tmpPlan.RecordTypeId				= planRTypeId;
		tmpPlan.Name						= planName;
		tmpPlan.Template_Plan_Protein__c	= protein;
		tmpPlan.Template_Plan_Carbs__c		= carbs;
		tmpPlan.Template_Plan_Fat__c		= fat;
		tmpPlan.Template_Plan_Calories__c	= calories;
		insert tmpPlan;
		return tmpPlan;
	}

	private static Template_Block__c testTool_createTemplateBlock(Template_Plan__c tempPlan, String blockName, Decimal protein, Decimal carbs, Decimal fat, Decimal calories, Integer blockOrder, Id blockRTypeId, Id planBlockRTypeId) {

		Template_Block__c templateBlock				= new Template_Block__c();
		templateBlock.RecordTypeId					= blockRTypeId;
		templateBlock.Name							= blockName;
		templateBlock.Template_Block_Protein__c		= protein;
		templateBlock.Template_Block_Carbs__c		= carbs;
		templateBlock.Template_Block_Fat__c			= fat;
		templateBlock.Template_Block_Calories__c	= calories;
		insert templateBlock;
	
		Template_Plan_Block__c tmpPlanBlock = new Template_Plan_Block__c();
		tmpPlanBlock.RecordTypeId			= planBlockRTypeId;
		tmpPlanBlock.Template_Plan__c		= tempPlan.Id;
		tmpPlanBlock.Template_Block__c		= templateBlock.Id;
		tmpPlanBlock.Order__c				= blockOrder;
		insert tmpPlanBlock;

		return templateBlock;
		
	}

	private static Template_Item__c testTool_createTemplateItem(Template_Block__c tempBlock, String itemName, Decimal tempQuantity, Decimal tempBlkItemQuantity, String measurement, Decimal quantityIncrement, Decimal minimumQuantity, Decimal protein, Decimal carbs, Decimal fat, Integer itemOrder, Id itemRTypeId, Id blockItemRTypeId) {
		
		Template_Item__c templateItem				= new Template_Item__c();
		templateItem.RecordTypeId					= itemRTypeId;
		templateItem.Name							= itemName;
		templateItem.Quantity_Increment__c			= quantityIncrement;
		templateItem.Minimum_Quantity__c			= minimumQuantity;
		templateItem.Measurement__c					= measurement;
		templateItem.Protein__c						= protein;
		templateItem.Carbs__c						= carbs;
		templateItem.Item_Quantity__c				= tempQuantity;
		templateItem.Fat__c							= fat;
		insert templateItem;

		Template_Block_Item__c templateBlockItem	= new Template_Block_Item__c();
		templateBlockItem.RecordTypeId				= blockItemRTypeId;
		templateBlockItem.Template_Item__c			= templateItem.Id;
		templateBlockItem.Template_Block__c			= tempBlock.Id;
		templateBlockItem.Quantity__c				= tempBlkItemQuantity;
		insert templateBlockItem;
		return templateItem;
	}

	private static Plan__c testTool_createNutritionReferencePlan(Check_In__c checkIn, Template_Plan__c tempPlan, string planName, String weekdays) {
	
		RecordType nutPlanRecType = AT_TestingTools.Query_recordType('Plan__c', 'Nutrition_Reference_Plan');

		Plan__c newPlan				= new Plan__c();
		newPlan.RecordTypeId		= nutPlanRecType.Id;
		newPlan.Name				= planName;
		newPlan.Weekdays__c			= weekdays;
		newPlan.Template_Plan__c	= tempPlan.Id;
		insert newPlan;
		return newPlan;

	}

	private static Block__c testTool_createNutritionReferenceBlock(Plan__c nutPlan, Template_Block__c tempBlock, String blockName, String blockShortName, Integer order) {
		RecordType nutBlockRecType = AT_TestingTools.Query_recordType('Block__c', 'Nutrition_Reference_Block');
		Block__c newBlock				= new Block__c();
		newBlock.RecordTypeId			= nutBlockRecType.Id;
		newBlock.Plan__c				= nutPlan.Id;
		newBlock.Template_Block_v2__c	= tempBlock.Id;
		newBlock.Name					= blockName;
		newBlock.Block_Short_Name__c	= blockShortName;
		newBlock.Display_Order__c		= order;
		insert newBlock;
		return newBlock;
	}

	private static Item__c testTool_createNutritionReferenceItem(Block__c nutBlock, Template_Item__c tempItem, String itemName, Integer quantity, Integer order) {
		RecordType nutItemRecType	= AT_TestingTools.Query_recordType('Item__c', 'Nutrition_Reference_Item');
		Item__c newItem				= new Item__c();
		newItem.Trainer_Quantity__c	= quantity;
		newItem.Block__c			= nutBlock.Id;
		newItem.RecordTypeId		= nutItemRecType.Id;
		newItem.Name				= itemName;
		newItem.Display_Order__c	= order;
		newItem.Template_Item_Id__c = tempItem.Id;
		insert newItem;
		return newItem;
	}
	
	private static void assertTool_assertPlanIsCloned(Check_In__c targetCheckIn, Plan__c planData, Template_Plan__c tempPlan) {

		List<Plan__c> clonedPlans = [
			SELECT
				Id,
				RecordTypeId,
				Name,
				Weekdays__c,
				Template_Plan__c,
				Check_In__c,
				Reference_Start_Date__c,
				Reference_End_Date__c,
				Account__c,
				Check_In_nr__c,
				Account_nr__c,
				Template_Plan_Protein__c,
				Template_Plan_Carbs__c,
				Template_Plan_Fat__c,
				Template_Plan_Calories__c,
				Trainer_Plan_Protein__c,
				Trainer_Plan_Carbs__c,
				Trainer_Plan_Fat__c,
				Trainer_Plan_Calories__c
			FROM Plan__c
			WHERE Check_In__c = :targetCheckIn.Id
		];
		Plan__c clonedPlan = clonedPlans.get(0);


		System.assertEquals(planData.Name,				clonedPlan.Name);
		System.assertEquals(planData.RecordTypeId,		clonedPlan.RecordTypeId);
		System.assertEquals(planData.Weekdays__c,		clonedPlan.Weekdays__c);
		System.assertEquals(planData.Template_Plan__c,	clonedPlan.Template_Plan__c);

		System.assertEquals(targetCheckIn.Id,							clonedPlan.Check_In__c);
		System.assertEquals(targetCheckIn.Nutrition_Plan_Start_Date__c, clonedPlan.Reference_Start_Date__c);
		System.assertEquals(targetCheckIn.Nutrition_Plan_End_Date__c,	clonedPlan.Reference_End_Date__c);
		System.assertEquals(targetCheckIn.Account__c,					clonedPlan.Account__c);
		System.assertEquals(targetCheckIn.Id,							clonedPlan.Check_In_nr__c);
		System.assertEquals(targetCheckIn.Account__c,					clonedPlan.Account_nr__c);
		System.assertEquals(tempPlan.Template_Plan_Protein__c,			clonedPlan.Template_Plan_Protein__c);
		System.assertEquals(tempPlan.Template_Plan_Carbs__c,			clonedPlan.Template_Plan_Carbs__c);
		System.assertEquals(tempPlan.Template_Plan_Fat__c,				clonedPlan.Template_Plan_Fat__c);
		System.assertEquals(tempPlan.Template_Plan_Calories__c,			clonedPlan.Template_Plan_Calories__c);

		System.assertEquals(tempPlan.Template_Plan_Protein__c,			clonedPlan.Trainer_Plan_Protein__c);
		System.assertEquals(tempPlan.Template_Plan_Carbs__c,			clonedPlan.Trainer_Plan_Carbs__c);
		System.assertEquals(tempPlan.Template_Plan_Fat__c,				clonedPlan.Trainer_Plan_Fat__c);
		System.assertEquals(tempPlan.Template_Plan_Calories__c,			clonedPlan.Trainer_Plan_Calories__c);

	}

	@isTest
	private static void ClonePlans_CloneNutritionReferencePlan() {
		Contact testContact				= testTool_createContact();
		Check_In__c testCheckIn1		= testTool_createCheckIn(testContact, Date.newInstance(2020, 1, 1), Date.newInstance(2020, 2, 1));
		Check_In__c testCheckIn2		= testTool_createCheckIn(testContact, Date.newInstance(2020, 2, 1), Date.newInstance(2020, 3, 1));
		
		RecordType tmpPlanRecType		= AT_TestingTools.Query_recordType('Template_Plan__c',			'Template_Nutrition_Plan');
		RecordType tmpPlanBlockRecType	= AT_TestingTools.Query_recordType('Template_Plan_Block__c',	'Template_Nutrition_Plan_Block');
		RecordType tmpBlockRecType		= AT_TestingTools.Query_recordType('Template_Block__c',			'Template_Nutrition_Block');
		RecordType tmpBlockItemRecType	= AT_TestingTools.Query_recordType('Template_Block_Item__c',	'Template_Nutrition_Block_Item');
		RecordType tmpItemRecType		= AT_TestingTools.Query_recordType('Template_Item__c',			'Nutrition');

		Template_Plan__c tmpPlan1		= testTool_createTemplatePlan('Test Template Plan', 100, 100, 100, 1700, tmpPlanRecType.Id);
		Template_Block__c tmpBlock1		= testTool_createTemplateBlock(tmpPlan1, 'Test Template Block 1', 100, 100, 100, 100, 0, tmpBlockRecType.Id, tmpPlanBlockRecType.Id);
		Template_Item__c tmpItem1		= testTool_createTemplateItem(tmpBlock1, 'Test Template Item 1', 1, 1, 'Grams', 1, 0, 100, 100, 100, 0, tmpItemRecType.Id, tmpBlockItemRecType.Id);

		Plan__c nutPlan					= testTool_createNutritionReferencePlan(testCheckIn1, tmpPlan1, 'Test Nutrition Plan', 'Monday');
		Block__c nutBlock				= testTool_createNutritionReferenceBlock(nutPlan, tmpBlock1, 'Test Nutrition Block', 'TNB', 0);
		Item__c nutItem					= testTool_createNutritionReferenceItem(nutBlock, tmpItem1, 'Test Nutrition Item', 1, 0);

		Test.startTest();
		AT_F_ClonePlans.Request req = new AT_F_ClonePlans.Request();
		req.targetCheckInId = testCheckIn2.Id;
		req.planIds = new List<String>();
		req.planIds.add(nutPlan.Id);
		AT_F_ClonePlans.ClonePlans(new AT_F_ClonePlans.Request[] { req });
		Test.stopTest();

		assertTool_assertPlanIsCloned(testCheckIn2, nutPlan, tmpPlan1);


	}
}