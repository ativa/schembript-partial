public without sharing class Template_Item_TriggerClass {

	private static List<Template_Block_Item__c> GetTemplateBlockItems(List<Template_Item__c> tmpItems) {
	
		RecordType nutritionRecType = [
			SELECT 
				Id 
			FROM RecordType 
			WHERE SobjectType = 'Template_Item__c' AND DeveloperName = 'Nutrition'
		];

		Set<Id> tmpItemIds = new Set<Id>();
		for(Template_Item__c tmpItem : tmpItems) {
			if(tmpItem.RecordTypeId == nutritionRecType.Id) {
				tmpItemIds.add(tmpItem.Id);
			}
		}

		return [
			SELECT
				Id,
				Template_Block__c
			FROM Template_Block_Item__c
			WHERE Template_Item__c IN :tmpItemIds
		];
			
	}
	
	public static void afterInsert(List<Template_Item__c> tmpItems) {
		List<Template_Block_Item__c> blockItems = GetTemplateBlockItems(tmpItems);
		update blockItems;
	}

	public static void afterUpdate(List<Template_Item__c> tmpItems) {
		List<Template_Block_Item__c> blockItems = GetTemplateBlockItems(tmpItems);
		update blockItems;
	}

	public static void afterDelete(List<Template_Item__c> tmpItems) {
		List<Template_Block_Item__c> blockItems = GetTemplateBlockItems(tmpItems);
		update blockItems;
	}


}