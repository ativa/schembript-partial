/**
* @author Harrison Campbell
* @description Styles the font
*/ 
public class ChartJS_Font  {

	@AuraEnabled
	public String size { get; set; }

}