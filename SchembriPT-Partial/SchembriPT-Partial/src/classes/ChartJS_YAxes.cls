public class ChartJS_YAxes {

	/**
	* @description Controls the axis global visibility (visible when true, hidden when false). When display: 'auto', the axis is visible only if at least one associated dataset is visible.
	*/ 
	public Object display { get; set; }


	public ChartJS_Ticks ticks { get; set; }

	public ChartJS_ScaleLabel scaleLabel { get; set; }

}