public without sharing class AT_LWCC_TrackerTargetTable {

    public class CheckInInformation {

        @AuraEnabled
        public String nutritionPlanningMode { get; set; }

        @AuraEnabled
        public List<AT_M_Nutrition.NutritionTarget> targets { get; set; }

        @AuraEnabled
        public List<AT_M_Nutrition.NutritionPoolPlan> unassignedPoolPlans { get; set; }

		/**
		 * @description A List of Pool Plans grouped by each target
		 */
		@AuraEnabled
		public Map<String, List<AT_M_Nutrition.NutritionPoolPlan>> targetPoolPlans { get; set; } 

        public CheckInInformation(Check_In__c checkIn) {
            this.nutritionPlanningMode = checkIn.Nutrition_Planning_Mode__c;
        } 

    }    

    @AuraEnabled
    public static CheckInInformation GetData(String checkInId) {

        CheckInInformation checkInInfo  = new CheckInInformation([SELECT Id, Nutrition_Planning_Mode__c FROM Check_In__c WHERE Id = :checkInId]);
        checkInInfo.targets             = GetCheckInTargets(checkInId);

        List<AT_M_Nutrition.NutritionPoolPlan> poolPlansWithNoTargets = new List<AT_M_Nutrition.NutritionPoolPlan>();
        for(AT_M_Nutrition.NutritionPoolPlan npp : AT_M_Nutrition.GetCheckInPoolPlans(checkInId)) {
            if(npp.parentTargetIds.size() == 0) {
                poolPlansWithNoTargets.add(npp);
            }
        }
        checkInInfo.unassignedPoolPlans = poolPlansWithNoTargets;

        return checkInInfo;

    }

    @AuraEnabled
    public static void UnassignPoolPlanFromTarget(Id poolPlanId, Id targetId) {

        Targets_Pool_Plan__c tpp = [
            SELECT 
                Id 
            FROM Targets_Pool_Plan__c 
            WHERE Pool_Plan__c = :poolPlanId AND Targets__c = :targetId
        ];
        delete tpp;
    }

	/**
	* @description Has the plan name been used
	* @param planName The name of the plan
	* @param checkInId The Id of the checkIn
	* @return Returns true if the plan name is used, false otherwise
	*/ 
	@AuraEnabled
	public static Boolean IsPlanNameUsed(String planName, String checkInId) {
		List<Plan__c> plans = [SELECT Id FROM Plan__c WHERE Check_In__c = :checkInId AND Name = :planName];
		return plans.size() > 0;
	}


	private static Map<String, Integer> weekdayToIntMap = new Map<String, Integer>{
		'Mon'	=> 0,
		'Tue'	=> 1,
		'Wed'	=> 2,
		'Thu'	=> 3,
		'Fri'	=> 4,
		'Sat'	=> 5,
		'Sun'	=> 6
	};

	private static Map<String, Integer> weekToIntMap = new Map<String, Integer>{
		'Wk1'	=> 0,
		'Wk2'	=> 1,
		'Wk3'	=> 2,
		'Wk4'	=> 3,
		'Wk5'	=> 4,
		'Wk6'	=> 5,
		'Wk7'	=> 6,
		'Wk8'	=> 7,
		'Wk9'	=> 8,
		'Wk10'	=> 9,
		'Wk11'	=> 10,
		'Wk12'	=> 11
	};

	@AuraEnabled
	public static void SaveClientComments(Id poolPlanVersionId, String clientComments) {
		Pool_Plan_Version__c ppv = [SELECT Id, Client_Comments__c FROM Pool_Plan_Version__c WHERE Id =: poolPlanVersionId];
		ppv.Client_Comments__c = clientComments;
		update ppv;
	}

    /**
    * @description Query Targets for the Check-In
    * @param checkInId The Id of the Check-In
    * @return Returns the Check-In targets
    */ 
    @AuraEnabled
    public static List<AT_M_Nutrition.NutritionTarget> GetCheckInTargets(String checkInId) {

		Check_In__c checkIn = [SELECT Id, Nutrition_Planning_Mode__c FROM Check_In__c WHERE Id = :checkInId];

        List<AT_M_Nutrition.NutritionTarget> nTargets = AT_M_Nutrition.GetCheckInTargets(checkInId);
		Map<Integer, AT_M_Nutrition.NutritionTarget> orderedMap = new Map<Integer, AT_M_Nutrition.NutritionTarget>();
		List<AT_M_Nutrition.NutritionTarget> noAssignedValues = new List<AT_M_Nutrition.NutritionTarget>();

		if(checkIn.Nutrition_Planning_Mode__c == 'Flexi Plans' || checkIn.Nutrition_Planning_Mode__c == 'Carb-Cycle') {

			//Abbreviate the values
			for(AT_M_Nutrition.NutritionTarget nTarget : nTargets) {
				if(nTarget.assignedTo != null) {
					for(Integer i = 0; i < nTarget.assignedTo.size(); i++) {
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Monday',		'Mon');
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Tuesday',	'Tue');
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Wednesday',	'Wed');
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Thursday',	'Thu');
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Friday',		'Fri');
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Saturday',	'Sat');
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Sunday',		'Sun');
					}
					orderedMap.put(weekdayToIntMap.get(nTarget.assignedTo[0]), nTarget);
				} else {
					noAssignedValues.add(nTarget);
				}
			}

		} else if(checkIn.Nutrition_Planning_Mode__c == 'Guided Plans') {
			//Abbreviate the values
			for(AT_M_Nutrition.NutritionTarget nTarget : nTargets) {
				if(nTarget.assignedTo != null) {
					for(Integer i = 0; i < nTarget.assignedTo.size(); i++) {
						nTarget.assignedTo[i] = nTarget.assignedTo[i].replace('Week ',		'Wk');
					}
					orderedMap.put(weekToIntMap.get(nTarget.assignedTo[0]), nTarget);
				} else {
					noAssignedValues.add(nTarget);
				}
			}
		}

		List<AT_M_Nutrition.NutritionTarget> orderedTargets = new List<AT_M_Nutrition.NutritionTarget>();

		for(Integer i = 0; i < 12; i++ ) {
			if(orderedMap.containsKey(i)) {
				orderedTargets.add(orderedMap.get(i));
			}
		}
		orderedTargets.addAll(noAssignedValues);

        return orderedTargets;
    }

	
	@AuraEnabled
	public static void SaveAsNewPoolPlan(String poolPlanVersionId, String newPlanName, List<Id> targetIds) {

		Pool_Plan_Version__c currVersion = [
			SELECT
				Id,
				Pool_Plan__c,
				Reference_Plan__c,
				Reference_Plan__r.Check_In__c,
				Reference_Plan__r.Account__c
			FROM Pool_Plan_Version__c
			WHERE Id = :poolPlanVersionId
		];
  
		RecordType rPlanType = [
            SELECT 
                Id 
            FROM RecordType 
            WHERE SobjectType = 'Plan__c' AND DeveloperName = 'Nutrition_Client_Plan'
        ];

		//Create a new Pool Plan
		Pool_Plan__c newPoolPlan	= new Pool_Plan__c();
		//newPoolPlan.Targets__c		= currVersion.Pool_Plan__r.Targets__c;
		newPoolPlan.Check_In__c		= currVersion.Reference_Plan__r.Check_In__c;
		newPoolPlan.Account__c		= currVersion.Reference_Plan__r.Account__c;
		insert newPoolPlan;

		//Create the new Plan
		Id newPlanId = AT_M_Nutrition.SaveAsNewPlan(currVersion.Reference_Plan__c, rPlanType.Id).Id;


		Plan__c newPlan = [
			SELECT
				Id,
				Name,
				Account__c,
				Check_In__c
			FROM Plan__c
			WHERE Id = :newPlanId
		];
		newPlan.Name			= newPlanName;
		newPlan.Account__c		= currVersion.Reference_Plan__r.Account__c;
		newPlan.Check_In__c		= currVersion.Reference_Plan__r.Check_In__c;
		update newPlan;

		//Create a new Pool Plan Version
		Pool_Plan_Version__c newPoolPlanVersion = new Pool_Plan_Version__c();
		newPoolPlanVersion.Active_Version__c	= true;
		newPoolPlanVersion.Version__c			= 1;
		newPoolPlanVersion.Pool_Plan__c			= newPoolPlan.Id;
		newPoolPlanVersion.Reference_Plan__c	= newPlan.Id;
		insert newPoolPlanVersion;

        //Assign
        List<Targets_Pool_Plan__c> newTargetsPoolPlan = new List<Targets_Pool_Plan__c>();
        for(Id targetId : targetIds) {
            Targets_Pool_Plan__c nPP    = new Targets_Pool_Plan__c();
            nPP.Targets__c              = targetId;
            nPP.Pool_Plan__c            = newPoolPlan.Id;
            newTargetsPoolPlan.add(nPP);
        }

        insert newTargetsPoolPlan;

	}

    /**
    * @description Delete a Pool Plan
    * @param poolPlanId The Id of the Pool Plan
    */ 
    @AuraEnabled
    public static void DeletePoolPlan(Id poolPlanId) {

        Pool_Plan__c pPlan = [
            SELECT 
                Id
            FROM Pool_Plan__c
            WHERE Id = :poolPlanId
        ];

        delete pPlan;

    }


    /**
    * @description Assign a Pool Plan to a Set of Targets
    * @param poolPlanId The Id of the Pool Plan
    * @param targetIds The Set of Ids for Targets
    */ 
    @AuraEnabled
    public static void AssignPoolPlanToTargets(Id poolPlanId, List<Id> targetIds) {

        //Delete the link between the pool plan, and the targets
        List<Targets_Pool_Plan__c> tpp = [
            SELECT
                Id
            FROM Targets_Pool_Plan__c
            WHERE Pool_Plan__c =: poolPlanId
        ];
        delete tpp;

        //Add the Pool Plan to the Targets
        List<Targets_Pool_Plan__c> newTpps = new List<Targets_Pool_Plan__c>();
        for(Id tId : targetIds) {
            Targets_Pool_Plan__c newTpp = new Targets_Pool_Plan__c();
            newTpp.Pool_Plan__c = poolPlanId;
            newTpp.Targets__c = tId;
            newTpps.add(newTpp);
        }
        insert newTpps;

    }

	
    @AuraEnabled
    public static Id SaveNewVersion(Id poolPlanId, Id newPlanId) {

        Pool_Plan__c pPlan = [
            SELECT
                Id,
                Account__c,
                Check_In__c,
                (
                    SELECT
                        Id,
                        Version__c
                    FROM Pool_Plan_Versions__r
                    ORDER BY Version__c DESC NULLS LAST
                )
            FROM Pool_Plan__c
            WHERE Id = :poolPlanId
        ];

        Plan__c newPlan = [
            SELECT
                Id
            FROM Plan__c
            WHERE Id = :newPlanId
        ];
        newPlan.Account__c  = pPlan.Account__c;
        newPlan.Check_In__c = pPlan.Check_In__c;
        update newPlan;

        //Add the new version
        Pool_Plan_Version__c newVersion = new Pool_Plan_Version__c();
        newVersion.Pool_Plan__c         = pPlan.Id;
        newVersion.Version__c           = pPlan.Pool_Plan_Versions__r.get(0).Version__c + 1;
        newVersion.Reference_Plan__c    = newPlan.Id;
        newVersion.Active_Version__c    = true;
        insert newVersion;

        //Make sure that all of the previous versions aren't active
        for(Pool_Plan_Version__c v : pPlan.Pool_Plan_Versions__r) {
            v.Active_Version__c = false;
        }

        update pPlan.Pool_Plan_Versions__r;

		return newVersion.Id;

    }


}