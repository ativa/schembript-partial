/**
* @author Harrison Campbell
* @description 
*/ 
public without sharing class AT_LWCC_PlannerAssignToDays {

	/**
	* @description Get the Nutritional Reference Plans for the Check-In
	* @param checkInId The Id of the Check-In
	* @return Returns the Nutritional Reference Plans
	*/ 
	@AuraEnabled
	public static List<Plan__c> GetNutritionReferencePlans(string checkInId) { 

		List<Plan__c> nutRefPlans = [
			SELECT
				Id,
				Name,
				Weekdays__c
			FROM Plan__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Nutrition_Reference_Plan'
		];

		return nutRefPlans;
	}


	/**
	* @description Update the Reference Plans Weekdays
	* @param checkInId The Id of the Check-In that contain the Weekdays
	* @param selectedDays A map of the day to plan Id (e.g. 'Monday' => <Plan__c.Id>)
	*/ 
	@AuraEnabled
	public static void AssignDaysToReferencePlans(string checkInId, Map<String, String> selectedDays) {
		
		//Group the selected days by the Ids
		Map<String, List<String>> selDays = new Map<String, List<String>>();
		for(String day : selectedDays.keySet()) {
			String planId = selectedDays.get(day);

			if(!selDays.containsKey(planId)) {
				selDays.put(planId, new List<String>());
			}
			selDays.get(planId).add(day);
		}



		//Query all of the Nutrition Plans for the Check-In, clear the weekdays, and then reassign them
		List<Plan__c> nutritionPlans = [
			SELECT
				Id,
				Weekdays__c,
				(
					SELECT
						Id,
						Pool_Plan__c,
						Pool_Plan__r.Targets__c,
						Pool_Plan__r.Targets__r.Weekdays__c
					FROM Pool_Plan_Versions__r
				)
			FROM Plan__c
			WHERE Check_In__c = :checkInId AND RecordType.DeveloperName = 'Nutrition_Reference_Plan'
		];

        //Get the Pool Plan Ids related to the Plans...
        List<Id> poolPlanIds = new List<Id>();
        for(Plan__c p : nutritionPlans) {
            for(Pool_Plan_Version__c ppv : p.Pool_Plan_Versions__r) {
                poolPlanIds.add(ppv.Pool_Plan__c);
            }
        }

        //Group the Targets/Pool Plan by Pool Plan Id
        List<Targets_Pool_Plan__c> relTargetPoolPlans = [
            SELECT
                Id,
                Pool_Plan__c,
                Targets__r.Weekdays__c
            FROM Targets_Pool_Plan__c
            WHERE Pool_Plan__c IN :poolPlanIds
        ];

        Map<Id, List<Targets_Pool_Plan__c>> relTargetPoolPlansMap = new Map<Id, List<Targets_Pool_Plan__c>>();
        for(Targets_Pool_Plan__c tpp : relTargetPoolPlans) {
            if(!relTargetPoolPlansMap.containsKey(tpp.Pool_Plan__c)) {
                relTargetPoolPlansMap.put(tpp.Pool_Plan__c, new List<Targets_Pool_Plan__c>());
            }
            relTargetPoolPlansMap.get(tpp.Pool_Plan__c).add(tpp);
        }

        List<Targets__c> targetsToUpdate = new List<Targets__c>();
        for(Plan__c nutPlan : nutritionPlans) {
            nutPlan.Weekdays__c = null;

            Id relPoolPlanId = nutPlan.Pool_Plan_Versions__r.get(0).Pool_Plan__c;

            if(selDays.containsKey(nutPlan.Id)) {
                for(String day : selDays.get(nutPlan.Id)) {
                    if(nutPlan.Weekdays__c == null) {
                        nutPlan.Weekdays__c = day;
                        //Loop through the related Targets, and update the weekdays
                        for(Targets_Pool_Plan__c tpp : relTargetPoolPlansMap.get(relPoolPlanId)) {
                            tpp.Targets__r.Weekdays__c = day;
                            targetsToUpdate.add(tpp.Targets__r);
                        }
                    } else {
                        nutPlan.Weekdays__c += ';' + day;
                        //Loop through the related Targets, and update the weekdays
                        for(Targets_Pool_Plan__c tpp : relTargetPoolPlansMap.get(relPoolPlanId)) {
                            tpp.Targets__r.Weekdays__c += ';' + day;
                        }
                    }
                }
            }


        }

		update nutritionPlans;
		update targetsToUpdate;
		
		Check_In__c currCheckIn = [
			SELECT
				Id,
				Nutrition_Plan_WIP__c,
				Nutrition_Assign_to_Days_Changed__c
			FROM Check_In__c
			WHERE Id = :checkInId
		];

		//Update the states of the Check-In
		currCheckIn.Nutrition_Plan_WIP__c				= false;
		currCheckIn.Nutrition_Assign_to_Days_Changed__c = true;
		update currCheckIn;

        //Update the Summary
        Plan_Trigger_Tools.UpdatePlanSummaryForCheckIn(currCheckIn.Id);

	}

}