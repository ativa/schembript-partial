public without sharing class AT_Q_UpdateUser implements Queueable {
	
		public String userAction;

		public String userId;

		public String firstName;
		
		public String lastName;
		
		public String alias;
		
		public String email;
		
		public String username;
		
		public String nickname;

		public String roleId;
		
		public String profileId;
		
		public Boolean active;
		
		public String mobile;
		
		public Boolean salesforceContentUser;
		
		public String timeZone;
		
		public String locale;
		
		public String emailEncoding;
		
		public String language;
		
		public String contactId;
		
		public String password;

		public String licenseName;

		public AT_Q_UpdateUser(String userAction, String userId, String firstName, String lastName, String alias, String email, String username, String nickname, String roleId, String profileId, Boolean active, String mobile, Boolean salesforceContentUser, String timeZone, String locale, String emailEncoding, String language, String contactId, String password, String licenseName) {
			this.userAction = userAction;
			this.userId = userId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.alias = alias;
			this.email = email;
			this.username = username;
			this.nickname = nickname;
			this.roleId = roleId;
			this.profileId = profileId;
			this.active = active;
			this.mobile = mobile;
			this.salesforceContentUser = salesforceContentUser;
			this.timeZone = timeZone;
			this.locale = locale;
			this.emailEncoding = emailEncoding;
			this.language = language;
			this.contactId = contactId;
			this.password = password;
			this.licenseName = licenseName;
		}

		public void execute(QueueableContext qc) {
			if(userAction == 'Create') {
				User newUser = new User();
				if(firstName != null) { newUser.FirstName = firstName; }
				if(lastName != null) { newUser.LastName = lastName; }
				if(alias != null) { newUser.Alias = alias; }
				if(email != null) { newUser.Email = email; }
				if(username != null) { newUser.Username = username; }
				if(nickname != null) { newUser.CommunityNickname = nickname; }
				if(roleId != null) { newUser.UserRoleId = roleId; }
				if(profileId != null) { newUser.ProfileId = profileId; }
				if(active != null) { newUser.IsActive = active; }
				if(mobile != null) { newUser.MobilePhone = mobile; }
				if(salesforceContentUser != null) { newUser.UserPermissionsSFContentUser = salesforceContentUser; }
				if(timeZone != null) { newUser.TimeZoneSidKey = timeZone; }
				if(locale != null) { newUser.LocaleSidKey = locale; }
				if(emailEncoding != null) { newUser.EmailEncodingKey = emailEncoding; }
				if(language != null) { newUser.LanguageLocaleKey = language; }
				if(contactId != null) { newUser.ContactId = contactId; }
				insert newUser;
				if(password != null) { System.enqueueJob(new AT_Q_UpdatePassword(newUser.Id, password)); }
			} else {
				User newUser = [SELECT Id, FirstName, LastName, Alias, Email, Username, CommunityNickname, UserRoleId, ProfileId, IsActive, MobilePhone, UserPermissionsSFContentUser, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, LanguageLocaleKey, ContactId FROM User WHERE Id = :userId];
				if(firstName != null) { newUser.FirstName = firstName; }
				if(lastName != null) { newUser.LastName = lastName; }
				if(alias != null) { newUser.Alias = alias; }
				if(email != null) { newUser.Email = email; }
				if(username != null) { newUser.Username = username; }
				if(nickname != null) { newUser.CommunityNickname = nickname; }
				if(roleId != null) { newUser.UserRoleId = roleId; }
				if(profileId != null) { newUser.ProfileId = profileId; }
				if(active != null) { newUser.IsActive = active; }
				if(mobile != null) { newUser.MobilePhone = mobile; }
				if(salesforceContentUser != null) { newUser.UserPermissionsSFContentUser = salesforceContentUser; }
				if(timeZone != null) { newUser.TimeZoneSidKey = timeZone; }
				if(locale != null) { newUser.LocaleSidKey = locale; }
				if(emailEncoding != null) { newUser.EmailEncodingKey = emailEncoding; }
				if(language != null) { newUser.LanguageLocaleKey = language; }
				if(contactId != null) { newUser.ContactId = contactId; }
				update newUser;
				if(password != null) { System.enqueueJob(new AT_Q_UpdatePassword(newUser.Id, password)); }
			}
		}


}